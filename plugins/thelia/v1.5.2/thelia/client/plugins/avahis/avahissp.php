<?php 
header("Content-type: text/html; charset=utf-8");
//header("Content-type: text/plain; charset=utf-8" );
//header("Content-Type: application/csv-tab-delimited-table");
//header("Content-disposition: filename=avahis.csv");

ini_set('display_errors', '0');
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Variable.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Produit.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Produitdesc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Image.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Cnx.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Rubriquedesc.class.php");
	// inclure les classes caracteristique et déclinaison
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Rubrique.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Caracteristiquedesc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Caracval.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Caracdispdesc.class.php");
	// déclinaison
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Declidispdesc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Stock.class.php");
	// devise
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Devise.class.php");
	include_once(realpath(dirname(__FILE__)) . "/classes/Parametre_avahis.class.php");
	include("function.php");
		
	$parametre_avahis = new Parametre_avahis();
	
	$resst = "";
	$delimiter = ";"; // delimiteur
	$res="";
	// on recherche la langue par défaut 
	$lang = new Lang();
	$lang->charger_defaut();
	$langCd= $lang->id;
	
	$urlsite = new Variable();
	$urlsite->charger("urlsite");
	
	//ancienne référence d'oririgine
	//$res = "Reference_ecommersant;Titre;Description;Categorie;Prix_ttc;Prix_barre;Frais_de_livraison;Quantite_en_stock;URL_produit;URL_image;Poids;Devise;";
	
	// nouvelle référence de champs :
	$res = "ID_PRODUCT;REFERENCE_PRODUCT;PRICE_TTC;ETAT_PROMO;PRICE_REDUCTION;ETAT_NEW;QUANTITY;ACTIVE;DEVISE;TITRE_DECLINAISON;";
	$res = str_replace(" ","_",$res);
	$res = stripAccents($res);
	$res =  strtoupper($res) ;
	$res .= "\r\n";
	
	// on charge la table produit dans $result
	$produit = new Produit();
	$query = "select * from $produit->table where ligne = '1'";
	$result = mysql_query($query,$produit->link);
	
	//concaténations des valeurs des attributs
	while($data = mysql_fetch_object($result))
	{
		// on charge la table stock suivant l'id produit
		$stock= new Stock();
		//var_dump($data);
		$query_decli = "select * from $stock->table where produit = " . $data->id." "; 
		$result_decli = mysql_query($query_decli, $stock->link);
		//$row_stock= mysql_fetch_object($result_decli);
		
		// on charge le nombre en stock pour la déclinaison de ce produit
		$query_nbdecli ="SELECT SUM(  `valeur` )as nbdecli FROM  $stock->table WHERE produit =" . $data->id."";
		$result_nbdecli = mysql_query($query_nbdecli, $stock->link);
		$row_nbdecli = mysql_fetch_object($result_nbdecli);
		
		//on charge la table produitdesc suivant l'id produit.	
		$produitdesc = new Produitdesc();
		$produitdesc->charger($data->id,$langCd);
		
		// on charge la table rubriquedesc suivant l'id produit.
		$rubriquedesc = new Rubriquedesc();
		$rubriquedesc->charger($data->rubrique,$langCd);
		
		// on charge la table rubrique suivant l'id produit.
		$rubrique = new Rubrique();
		$rubrique->charger($data->rubrique);
		
		// on charge la devise du ecommerçant
		$devise = new Devise();
		$query_devise = "select * from $devise->table where defaut= '1' limit 0,1";
		$result_devise = mysql_query($query_devise, $devise->link);
		$row_devise= mysql_fetch_object($result_devise);
	
		// on charge la table image suivant l'id produit
		$image = new Image();
		$query_img = "select * from $image->table where produit=" . $data->id . " limit 0,1";
		$resul_img = mysql_query($query_img, $image->link);
		$row_img = mysql_fetch_object($resul_img);
		
		// on charge le nombre d'itération de ce produit dans la table parametre_avahis
		$nbIteProd = $parametre_avahis->charger_nbrec($data->id,'product_id');
		
		// ecriture des enregistrement produit
		
		if($nbIteProd==1){ // si le produit est dans les paramétre de l'ecommerçant pour l'export alors
			if ($row_nbdecli->nbdecli==0){ // si pas de déclinaison
			
				// id_produit
				$res .= ($data->id); 
				$res .= $delimiter;

				// ref 
				$ref=str_replace(";","",$data->ref);
				$ref=str_replace(" ","",$data->ref);
				$res .= trim($ref); // concaténe $res avec ref de la table Produitdesc
				$res .= $delimiter;
								
				// price
				$res .= $data->prix;
				$res .= $delimiter;
				
				// etat_promo
				$res .= $data->promo;
				$res .= $delimiter;
						
				// prix_promo
				$res .= $data->prix2;
				$res .= $delimiter;
				
				//etat_new
				$res .= $data->nouveaute;
				$res .= $delimiter;
				
				// stock_principal
				$res .= $data->stock;
				$res .= $delimiter;
				
				//etat_ligne
				$res .= $data->ligne;
				$res .= $delimiter;
				
				// devise
				$res .=  trim(utf8_encode($row_devise->code));
				$res .= $delimiter;	
				
				// declinaison
				$res .=  "0"; // pas de déclinaison
				$res .= $delimiter;	
				
				$res .= "\r\n"; // fin de ligne et nouvelle ligne
			}
			else{ // pour les déclinaisons
				while($row_stock= mysql_fetch_object($result_decli)){
					// on charge la table declidispdesc avec ses titres suivant l'id declidisp donné
					$declidispdesc = new Declidispdesc() ;
					$query_declidispdesc = "select * from $declidispdesc->table where declidisp=".$row_stock->declidisp." AND lang=".$langCd." "; 
					$result_declidispdesc = mysql_query($query_declidispdesc, $declidispdesc->link);
					$row_titredecli= mysql_fetch_object($result_declidispdesc);	
					
					// id_produit
					$res .= trim($data->id); 
					$res .= $delimiter;
					
					// ref 
					$ref=str_replace(";","",$data->ref);
					$ref=str_replace(" ","",$data->ref);
					$res .= trim($ref); // concaténe $res avec ref de la table Produitdesc
					$res .= $delimiter;
						
					// price
					$res .= ($data->prix)+($row_stock->surplus);
					$res .= $delimiter;
			
					// etat_promo
					$res .= $data->promo;
					$res .= $delimiter;
							
					// prix_promo
					$res .= ($data->prix2)+($row_stock->surplus);
					$res .= $delimiter;
					
					//etat_new
					$res .= $data->nouveaute;
					$res .= $delimiter;
					
					// stock_principal
					$res .= $row_stock->valeur; // a refaire pour chaque déclinaison de produit
					$res .= $delimiter;
					
					//etat_ligne
					$res .= $data->ligne;
					$res .= $delimiter;
					
					// devise
					$res .=  trim(utf8_encode($row_devise->code));
					$res .= $delimiter;	
					
					// déclinaison
					$titre = $row_titredecli->titre;
					$titre = str_replace($delimiter," ",$titre);
					
					$res .= $titre ;//trim(utf8_encode($row_titredecli->titre)); 
					$res .= $delimiter;	
										
					$res .= "\r\n"; // fin de ligne et nouvelle ligne	
				}	
			}
		}

	}
	echo $res;
?>
