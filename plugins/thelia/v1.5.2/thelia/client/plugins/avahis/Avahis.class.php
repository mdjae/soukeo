<?php
@include_once(realpath(dirname(__FILE__)) . "/../../../classes/PluginsClassiques.class.php");
@include_once(realpath(dirname(__FILE__)) . "/../../../classes/Variable.class.php");

class Avahis extends PluginsClassiques {


	var $id;
	var $commande;
	var $panier;

	var $table="avahis";
	var $bddvars = array("id", "commande", "panier");		

	
	
	function Avahis() {
		$this->PluginsClassiques();	
	}
	
	function charger($commande){
		return $this->getVars("select * from $this->table where commande=\"$commande\"");
	}	
	 
	function action(){
		global $res, $fond;
	
		include_once(realpath(dirname(__FILE__)) . "/config.php");
	 
		if(! isset($_SESSION['panier_avahis']))
			$_SESSION['panier_avahis'] = uniqid();
			 
		$res = str_replace("#avahis_PANIER", $_SESSION['panier_avahis'], $res);	
		$res = str_replace("#avahis_ID", $idclient, $res);	
	
		if($fond == "merci.html" || $fond == "cheque.html")
			unset($_SESSION['panier_avahis']);
	}
	
	function aprescommande($commande){
		$this->commande = $commande->id;
		$this->panier = $_SESSION['panier_avahis'];
		$this->add();
	}
	
	function init(){
		$cnx = new Cnx();
		$query_avahis = "CREATE TABLE IF NOT EXISTS parametre_avahis(
 `id_parametre` INT NOT NULL AUTO_INCREMENT ,
 `parametre_nom` TEXT NOT NULL ,
 `parametre_valeur` TEXT NOT NULL ,
PRIMARY KEY (  `id_parametre` )
) DEFAULT CHARSET = utf8 ENGINE = MYISAM";
		$resul_avahis = mysql_query($query_avahis, $cnx->link);
	}
}