<?php

include_once (realpath(dirname(__FILE__)) . "/../../../../fonctions/autoload.php");

class Parametre_avahis extends Baseobj {

	public $id_parametre;
	public $parametre_nom;
	public $parametre_valeur;

	const TABLE = "parametre_avahis";
	public $table = self::TABLE;

	public $bddvars = array("id_parametre", "parametre_nom", "parametre_valeur");

	public function __construct($parametre_nom = "") {
		parent::__construct();

		//if($ref != "")
		$this -> charger($parametre_nom);
	}

	public function charger($parametre_nom) {
		return $this -> getVars("select * from $this->table where parametre_nom=\"$parametre_nom\"");
	}

	function charger_id($id_parametre) {
		return $this -> getVars("select * from $this->table where id_parametre=\"$id_parametre\"");
	}

	/**
	 * Pour obtenir le nombre d'occurence pour une valeur donné a un nom donnée
	 * ex : combien il y a d'occurence pour pour un produit donné
	 * charger_nbrec(1,product_id)
	 * @param $valeur de type char la valeur recherché
	 * @param $nom de type char le type qu'on veut recherché
	 * @return résultat de type int le nombre d'occurence de la valeur recherché
	 */
	function charger_nbrec($valeur, $nom) {
		$resul = $this -> query("select count(*) as nb from $this->table where parametre_valeur='" . $valeur . "' AND parametre_nom ='" . $nom . "'");
		return $resul ? $this -> get_result($resul, 0, "nb") : 0;
	}

	/*
	 * Pour obtenir la valeur d'un parametre en un seul appel: Parametre_avahis::lire("nomvariable")
	 */
	static $_cache = array();
	public static function lire($parametre_nom, $defaut = "") {

		if (!isset(self::$_cache[$parametre_nom])) {
			$var = new Variable($parametre_nom);

			self::$_cache[$parametre_nom] = empty($var -> id_parametre) ? $defaut : $var -> parametre_valeur;
		}

		return self::$_cache[$parametre_nom];
	}

	public function add() {
		unset(self::$_cache[$this -> parametre_nom]);

		return parent::add();

	}

	public function maj() {
		unset(self::$_cache[$this -> parametre_nom]);

		return parent::maj();

	}

	public function delete() {
		unset(self::$_cache[$this -> parametre_nom]);

		parent::delete();

	}

	/**
	 * Pour mettre a jour la valeur d'un parametre en un seul appel: Parametre_avahis::ecrire("nomParametre", valeur)
	 */
	public static function ecrire($parametre_nom, $parametre_valeur, $creer_si_inexistante = false) {

		$var = new Variable($parametre_nom);

		if ($creer_si_inexistante && !$var -> charger($parametre_nom)) {

			$var -> parametre_nom = $parametre_nom;
			$var -> parametre_valeur = $parametre_valeur;

			$var -> add();
		} else {
			$var -> parametre_valeur = $parametre_valeur;

			$var -> maj();
		}
	}

	/*public function delete(){

	 if (! empty($this->id)) {

	 $this->delete_cascade('Image', 'produit', $this->id);
	 $this->delete_cascade('Document', 'produit', $this->id);
	 $this->delete_cascade('Stock', 'produit', $this->id);
	 $this->delete_cascade('Accessoire', 'produit', $this->id);
	 $this->delete_cascade('Accessoire', 'accessoire', $this->id);
	 $this->delete_cascade('Caracval', 'produit', $this->id);
	 $this->delete_cascade('Exdecprod', 'produit', $this->id);
	 $this->delete_cascade('Produitdesc', 'produit', $this->id);

	 parent::delete();
	 }
	 }*/
}
?>