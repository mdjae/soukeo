Passerelle vers l'agrégateur de produit Avahis.

Veuillez simplement glisser le répertoire avahis dans le dossier client/plugins/ de votre Thelia.
Activez-le ensuite dans le menu Configuration/Gestion des plugins dans votre interface d'administration

L'url à fournir aux services Avahis sera de la forme suivante : http://www.monsite.com/client/plugins/avahis/avahis.php


