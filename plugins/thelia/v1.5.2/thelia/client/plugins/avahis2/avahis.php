<?php header("Content-type: text/plain; charset=utf-8");?>
<?php
	header("Content-Type: application/csv-tab-delimited-table");
	header("Content-disposition: filename=avahis.csv");
?>
<?php
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Variable.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Produit.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Produitdesc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Image.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Cnx.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Rubriquedesc.class.php");

	$delimiter = ";"; // delimiteur
	
	$urlsite = new Variable();
	$urlsite->charger("urlsite");

	$res = "Identifiant_unique;Titre;Description;Categorie;Prix_ttc;Prix_barre;Frais_de_livraison;Quantite_en_stock;URL_produit;URL_image;Poids;Devise;";
	
	$caracteristique = new Caracteristiquedesc();
	
	$caract = "select * from $caracteristique->table ";
	$result = mysql_query($caract,$caracteristique->link);
	while($data = mysql_fetch_object($result) ){
		$resst  .= $data->chapo.";";
	}
	
	$res .= $resst;
	$res .= "\r\n";
	
	$produit = new Produit();
	
	$query = "select * from $produit->table";
	$result = mysql_query($query,$produit->link);

	while($data = mysql_fetch_object($result))
	{
		$produitdesc = new Produitdesc();
		$produitdesc->charger($data->id);
		$rubrique = new Rubriquedesc();
		$rubrique->charger($data->rubrique);

		$image = new Image();
		$query_img = "select * from $image->table where produit=" . $produitdesc->produit . " limit 0,1";
		$resul_img = mysql_query($query_img, $image->link);
		$row_img = mysql_fetch_object($resul_img);
		

		$res .= trim($data->ref);
		$res .= $delimiter;
		
		$titre = trim(utf8_encode($produitdesc->titre));
		$titre = str_replace(";"," ",$titre);
		$res .= $titre;
		$res .= $delimiter;
		
		$description = trim(strip_tags(utf8_encode($produitdesc->chapo)));
		$description = str_replace(";"," ",$description);
		$res .= $description;
		$res .= $delimiter;
		
		$rub = trim(utf8_encode($rubrique->titre));
		$rub = str_replace(";"," ",$rub);
		$res .= $rub;
		$res .= $delimiter;
		
		$res .= $data->prix2;
		$res .= $delimiter;
		
		$res .= $data->prix;
		$res .= $delimiter;

		$res .= "Voir sur le site";
		$res .= $delimiter;
				
		$res .= $data->stock;
		$res .= $delimiter;
		
		$res .= $urlsite->valeur."/produit.php?ref=".$data->ref."&id_rubrique=".$data->rubrique;
		$res .= $delimiter;
		
		$res .= $urlsite->valeur."/fonctions/redimlive.php?type=produit&nomorig=" . $row_img->fichier . "&width=300";
		$res .= $delimiter;
		
		$res .= $data->poids;
		$res .= $delimiter;
		
		$res .= "EUR;";
		
		//recup val specifique pour le produit 
		$caract = "select * from $caracteristique->table ";
		$result = mysql_query($caract,$caracteristique->link);
		
		while($datacac = mysql_fetch_object($result) ){
			$caracval = new Caracval();	
			$caracval->charger($data->id,$datacac->id);
		}

		$res .= "\r\n"; // fin de ligne et nouvelle ligne
	}
	echo $res;
?>
