<?php
	include_once(realpath(dirname(__FILE__)) . "/../../../fonctions/authplugins.php");

	autorisation("duplicomp");

	include_once(realpath(dirname(__FILE__)) . "/Duplicomp.class.php");
	
	if($_REQUEST['action1'] == "dupliquer1"){
		$test = new Produit();
		if(! $test->charger($_REQUEST['nouvelle_ref1'])){
			$produit = new Produit();
			if($produit->charger($_REQUEST['courante_ref1'])){
				$duplicomp = new Duplicomp();				
				$duplicomp->dupliquer($produit, $_REQUEST['nouvelle_ref1']);
				?><script type="text/javascript">
					alert("Duplication correcte");
					location="parcourir.php?parent=<?php echo $produit->rubrique; ?>";
				</script><?php
			}				
			else{		
				?><script type="text/javascript">
					alert("Le produit n'existe pas");
				</script><?php
			}
				
		}			
		else{				
			?><script type="text/javascript">
				alert("Ref non disponible");
			</script><?php	
		}			
	}
?>
<form action="#" method="post">
	<div class="entete">
		<div class="titre" style="cursor:pointer" onclick="$('#pliantdupli').show('slow');">DUPLICATION COMPLETE DU PRODUIT</div>
	</div>
	<div class="blocs_pliants_prod" id="pliantdupli">				
		<ul class="lignesimple">
			<li class="cellule_designation" style="width:128px; padding:5px 0 0 5px; background-image:url(gfx/degrade_ligne1.png); background-repeat: repeat-x;">Ref Courante</li>
			<li class="cellule" style="width:450px; padding: 5px 0 0 5px; background-image:url(gfx/degrade_ligne1.png); background-repeat: repeat-x;"><input type="text" name="courante_ref1" value="<?php echo $_GET['ref']; ?>"</li>	
		</ul>			
		<ul class="lignesimple">
			<div class="cellule_designation" style="height:30px; width:128px;">Nouvelle Ref</div>
			<div class="cellule">
				<input style="margin-top:-3px;" type="text" name="nouvelle_ref1" style="width:219px;" class="form" />			
			</div>
		</ul>
		<ul class="lignesimple">
			<div class="cellule_designation" style="height:30px; width:128px;">&nbsp;</div>
			<div class="cellule" style="height:30px; border-bottom: 1px dotted #9DACB6"><input style="margin-top:-3px;" type="submit" value="Dupliquer"></div>
		</ul>
		<div class="bloc_fleche" style="cursor:pointer" onclick="$('#pliantdupli').hide();"><img src="gfx/fleche_accordeon_up.gif"></div>
	</div>
	<input type="hidden" name="action1" value="dupliquer1" />
</form>