Ce plugin permet de dupliquer intégralement une fiche produit.

Veuillez simplement glisser le répertoire duplicomp dans le dossier client/plugins/ de votre Thelia.
Vous pouvez aussi utiliser l'assistant d'import de plugins dans Configuration/Gestion des plugins.
Activez-le ensuite dans le menu Configuration/Gestion des plugins dans votre interface d'administration.