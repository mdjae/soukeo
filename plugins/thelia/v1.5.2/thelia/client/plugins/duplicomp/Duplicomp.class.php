<?php
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/PluginsClassiques.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Produit.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Produitdesc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Image.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Imagedesc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Variable.class.php");	
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Document.class.php");	
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Documentdesc.class.php");	
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Caracval.class.php");	
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Accessoire.class.php");	
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Contenuassoc.class.php");
	include_once(realpath(dirname(__FILE__)) . "/../../../classes/Stock.class.php");

	class Duplicomp extends PluginsClassiques {
		
		function Duplicomp(){
			$this->PluginsClassiques();
		}
		
		function init(){
		
		}
		
		function destroy(){

		}
		
		function dupliquer($produit, $nouvelle_ref){
			if(!empty($nouvelle_ref) && !empty($produit)){
				$unProd = new Produit();
				if(!$unProd->charger($nouvelle_ref)){
					$urlsite = new Variable();
					$urlsite->charger("urlsite");
					$url_doc = $urlsite->valeur;
					$url_doc .= "/client/document/";
					$url_image = $urlsite->valeur;
					$url_image .= "/client/gfx/photos/produit/";
					
					$query_dup = "select max(classement) as maxclassement from $produit->table where rubrique=\"" . $produit->rubrique . "\"";
					$resul_dup = mysql_query($query_dup, $produit->link);
					$classement = mysql_result($resul_dup, 0, "maxclassement");			
					
					$image = new Image();
					$query_img = "select * from $image->table where produit=$produit->id";
					$resul_img = mysql_query($query_img);
		
					$document = new Document();
					$query_doc = "select * from $document->table where produit=$produit->id";
					$resul_doc = mysql_query($query_doc);
					
					$caracval = new Caracval();
					$query_caracval = "select * from $caracval->table where produit=$produit->id";
					$resul_caracval = mysql_query($query_caracval);
					
					$accessoire = new Accessoire();
					$query_accessoire = "select * from $accessoire->table where produit=$produit->id";
					$resul_accessoire = mysql_query($query_accessoire);
					
					$contenuassoc = new Contenuassoc();
					$query_contenuassoc = "select * from $contenuassoc->table where objet=$produit->id";
					$resul_contenuassoc = mysql_query($query_contenuassoc);
					
					$produitdesc = new Produitdesc();
					$query_prod = "select * from $produitdesc->table where produit=$produit->id";
					$resul_prod = mysql_query($query_prod);
					
					$stock = new Stock();
					$query_stock = "select * from $stock->table where produit=$produit->id";
					$resul_stock = mysql_query($query_stock);
					
					$newproduit = new Produit();
					
					// Duplication du produit
					$newproduit = $produit;
					$newproduit->id = "";
					$newproduit->ref = $nouvelle_ref;
					$newproduit->classement = $classement + 1;
					
					// On r�cup�re l'id du produit qui vient d'�tre dupliqu�
					$lastid = $newproduit->add();
					
					// Duplication des descriptions produits pour les diff�rentes langues
					while($produitdesc = mysql_fetch_object($resul_prod)){
						$newproduitdesc = new Produitdesc();
						$newproduitdesc->charger($produitdesc->produit, $produitdesc->lang);
						$newproduitdesc->id = "";
						$newproduitdesc->produit = $lastid;
						$newproduitdesc->add();					
					}
					
					// Duplication des caract�ristiques associ�es
					while($caracval = mysql_fetch_object($resul_caracval)){
						$newcaracval = new Caracval();
						$newcaracval->charger($caracval->produit, $caracval->caracteristique);
						$newcaracval->id = "";
						$newcaracval->produit = $lastid;
						$newcaracval->add();
					}
					
					// Duplication des accessoires associ�s
					while($accessoire = mysql_fetch_object($resul_accessoire)){
						$newaccessoire = new Accessoire();
						$newaccessoire->charger($accessoire->id);
						$newaccessoire->id = "";
						$newaccessoire->produit = $lastid;
						$newaccessoire->add();
					}
					
					// Duplication des contenus associ�s
					while($contenuassoc = mysql_fetch_object($resul_contenuassoc)){
						$newcontenuassoc = new Contenuassoc();
						$newcontenuassoc->charger($contenuassoc->id);
						$newcontenuassoc->id = "";
						$newcontenuassoc->objet = $lastid;
						$newcontenuassoc->add();
					}
					
					// Duplication des d�clinaisons
					while($stock = mysql_fetch_object($resul_stock)){
						$newstock = new Stock();
						$newstock->charger($stock->declidisp, $stock->produit);
						$newstock->id = "";
						$newstock->produit = $lastid;
						$newstock->add();
					}
					
					// Duplication du ou des documents attach�s et de leur description
					while($document = mysql_fetch_object($resul_doc)){
						$newdoc = new Document();
						$newdoc->charger($document->id);					
						$newdoc->id = "";
						$newdoc->produit = $lastid;
						$notfound = 1;
						do{
							if(file_exists("../client/document/" . $notfound . $document->fichier)){
								$notfound++;
							}
							else{
								$newdoc->fichier = $notfound . $newdoc->fichier;
								copy("../client/document/" . $document->fichier,"../client/document/" . $newdoc->fichier);
								$notfound = 0;
							}
						}while($notfound);
						$lastid1 = $newdoc->add();
						
						$documentdesc = new Documentdesc();
						$query_doc_desc = "select * from $documentdesc->table where document=$document->id";
						$resul_doc_desc = mysql_query($query_doc_desc);
						while($documentdesc = mysql_fetch_object($resul_doc_desc)){
							$newdocumentdesc = new Documentdesc();
							$newdocumentdesc->charger($documentdesc->document, $documentdesc->lang);
							$newdocumentdesc->id = "";
							$newdocumentdesc->document = $lastid1;
							$newdocumentdesc->add();
						}
					}
					
					// Duplication du ou des images attach�es
					while($image = mysql_fetch_object($resul_img)){
						$newimage = new Image();
						$newimage->charger($image->id);					
						$newimage->id = "";
						$newimage->produit = $lastid;
						$notfound = 1;
						do{
							if(file_exists("../client/gfx/photos/produit/" . $notfound . $image->fichier)){
								$notfound++;
							}
							else{
								$newimage->fichier = $notfound . $newimage->fichier;
								copy("../client/gfx/photos/produit/" . $image->fichier,"../client/gfx/photos/produit/" . $newimage->fichier);
								$notfound = 0;
							}
						}while($notfound);					
						$lastid2 = $newimage->add();	
						
						$imagedesc = new Imagedesc();
						$query_img_desc = "select * from $imagedesc->table where image=$image->id";
						$resul_img_desc = mysql_query($query_img_desc);
						while($imagedesc = mysql_fetch_object($resul_img_desc)){
							$newimgdesc = new Imagedesc();
							$newimgdesc->charger($imagedesc->image, $imagedesc->lang);
							$newimgdesc->id = "";
							$newimgdesc->image = $lastid2;
							$newimgdesc->add();
						}
					}
				}
			}
		}
	}
?>