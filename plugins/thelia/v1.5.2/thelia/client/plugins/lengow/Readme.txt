Passerelle vers l'agrégateur de comparateur de prix lengow.

Veuillez simplement glisser le répertoire lengow dans le dossier client/plugins/ de votre Thelia.
Activez-le ensuite dans le menu Configuration/Gestion des plugins dans votre interface d'administration

L'url à fournir aux services Lengow sera de la forme suivante : http://www.monsite.com/client/plugins/lengow/lengow.php

N'oubliez pas d'éditer le fichier config.php et d'indiquer votre numéro de client Lengow.

Si vous souhaitez tracker les ventes provenant de Lengow veuillez ajouter les tags suivants :

a) sur la page commande.html (choix du mode de paiement)
<img src="https://tracking.lengow.com/lead.php?idClient=#LENGOW_ID&price=#PANIER_TOTPORT&idCommande=#LENGOW_PANIER" alt="" />

b) sur la page merci.html (remerciement, fin de commande)
<img src="https://tracking.lengow.com/leadValidation.php?idClient=#LENGOW_ID&idCommande=#PANIER_TOTPORT" alt="" />