<?php
include_once(realpath(dirname(__FILE__)) . "/../../../classes/PluginsClassiques.class.php");
include_once(realpath(dirname(__FILE__)) . "/../../../classes/Variable.class.php");

class Lengow extends PluginsClassiques {


	var $id;
	var $commande;
	var $panier;

	var $table="lengow";
	var $bddvars = array("id", "commande", "panier");		

	
	
	function Lengow() {
		$this->PluginsClassiques();	
	}
	
	function charger($commande){
		return $this->getVars("select * from $this->table where commande=\"$commande\"");
	}	
	
	function action(){
		global $res, $fond;
	
		include_once(realpath(dirname(__FILE__)) . "/config.php");
	
		if(! isset($_SESSION['panier_lengow']))
			$_SESSION['panier_lengow'] = uniqid();
			
		$res = str_replace("#LENGOW_PANIER", $_SESSION['panier_lengow'], $res);	
		$res = str_replace("#LENGOW_ID", $idclient, $res);	
	
		if($fond == "merci.html" || $fond == "cheque.html")
			unset($_SESSION['panier_lengow']);
	}
	
	function aprescommande($commande){
		$this->commande = $commande->id;
		$this->panier = $_SESSION['panier_lengow'];
		$this->add();
	}
	
	function init(){
		$cnx = new Cnx();
		$query_lengow = "CREATE TABLE `lengow` (
		  `id` int(11) NOT NULL auto_increment,
		  `commande` int(11) NOT NULL,
		  `panier` text NOT NULL,
		  PRIMARY KEY  (`id`)
		)";
		$resul_lengow = mysql_query($query_lengow, $cnx->link);
	}
}