<?php
	include_once(realpath(dirname(__FILE__)) . "/../../../fonctions/authplugins.php");
	autorisation("lengow");
?>
<?php
	include_once(realpath(dirname(__FILE__)) . "/Lengow.class.php");

	$tmpcmd = new Commande();
	$tmpcmd->charger_ref($_REQUEST['ref']);
	
	$lengow = new Lengow();
	$lengow->charger($tmpcmd->id);
?>

<div class="bordure_bottom" style="margin:0 0 10px 0;">
	<div class="entete_liste_client">
		<div class="titre">LENGOW</div>
	</div>
	<ul class="ligne_claire_BlocDescription">
		<li class="designation" style="width:290px;">Id du panier Lengow</li>
		<li><?php echo $lengow->panier; ?></li>
	</ul>
</div>
