<div id="contenu_int">
  	
  	<style type="text/css">
  		#contenu_int table {
  			width: 100%;
  		}
  	</style>
  	
	<fieldset style="margin : 5px; background-color: white;">
		<legend>Paramétrage du module myposeo, suivi de positionnement en ligne</legend>

<?php
	
	// Inclusion fichier myposeo
	include_once(realpath(dirname(__FILE__)) . "/myposeo.class.php");
	
	// Inclusion fichier API myposeo
	include_once(realpath(dirname(__FILE__)) . "/MyposeoAPI.php");
	
	// Obtention du numéro de version
	$version = new Variable();
	$version -> charger("version");
	
	// Intanciation de la classe d'API
	$myposeoAPI = new MyposeoAPI();
	$myposeoAPI -> addClient( 'thelia' );
	$myposeoAPI -> addParam( 'thelia_version', substr($version->valeur, 0, 1) . "." . substr($version->valeur, 1, 1) . "." . substr($version->valeur, 2, 1) );
	
	// Définition du mode de connexion (connexion par jeton par défaut)
	$myposeoAPI -> setConnexionMode( MyposeoAPI::CONNEXION_KEY );
	
	// Définition de la clé de connexion que vous trouverez dans votre compte
	$myposeoAPI -> setKey( $variableKeyMyposeo->valeur );
?>

<?php if( !extension_loaded('curl') ) : ?>
	
	<div>ATTENTION : L'extension cURL doit être activée.</div>

<?php endif; ?>

<?php if( !(bool)ini_get('allow_url_fopen') ) : ?>
	
	<div>ATTENTION : La configuration PHP 'allow_url_fopen' doit être activée.</div>

<?php endif; ?>

<?php if( isset( $_GET['getKey'] ) ) {

		// Récupération de la clé de l'utilisateur
		$valid = $myposeoAPI->logIn( $_POST['login'], $_POST['password'] );
		
		// Si l'authentification a réussi, on enregistre la clé
		if( $valid ) {
			
			$variableKeyMyposeo->valeur = $myposeoAPI->getKey();
			$variableKeyMyposeo->maj();
		}
	}
?>

<?php if( !isset( $_GET['url'] ) ) : ?>

	<?php if( $variableKeyMyposeo->valeur != "" && $myposeoAPI->keyIsValid() ) : ?>
	
		<br />
		<p style="font-weight: bold;"><img src="https://www.myposeo.com/img/logo-thelia-mini.png" />myposeo - Liste des URLs :</p>
		<br />
		<?php
		
			// Définition du format (format xml par défaut)
			$myposeoAPI -> setFormat( MyposeoAPI::XML );
			
			// On récupère la liste des URLs du compte
			$listeURLs = $myposeoAPI->getURLs();
			
			$dom = new DomDocument();
			$dom -> loadXML( $listeURLs );
			
			foreach( $dom->getElementsByTagName('url') as $url ) {
				
				echo '<a href="./module.php?nom=myposeo&url=' . $url->getAttribute("id") . '">' . $url->firstChild->nodeValue . "</a><br />";
			}
		
		?>
		
	<?php else : ?>
	
		<?php if( $variableKeyMyposeo->valeur != "" ) : ?>
		
			<p style="color: red;">La clé de l'API myposeo n'est pas valide, merci de la vérifier.</p>
		
		<?php endif; ?>
		
		<?php
		
			// Si la clé est vide on lui propose de se connecter à son compte myposeo
			if( isset( $valid ) && !$valid ) :
		?>
		
			<p style="color: red;">Les identifiants ne sont pas corrects.</p>
			
		<?php endif; ?>
		
		<form action="./module.php?nom=myposeo&getKey" method="post">
			<label style="width:250px; display:block; float:left; margin-right:20px;text-align:right;">Email : </label><div style="display:block; float:left;"><input type="text" value="<?php echo ( isset( $_POST['login'] ) ? $_POST['login'] : '' )?>" name="login" /></div>
			<div style="clear:both; height:0.5em;"></div>
			<label style="width:250px; display:block; float:left; margin-right:20px;text-align:right;">Mot de passe : </label><div style="display:block; float:left;"><input type="password" value="<?php echo ( isset( $_POST['password'] ) ? $_POST['password'] : '' )?>" name="password" /></div>
			<div style="clear:both; height:0.5em;"></div>
			<div style="display:block; float:left; margin-left: 270px;"><input type="submit" value="Récupérer ma clé myposeo" /></div>
			<div style="clear:both; height:0.5em;"></div>
		</form>
	
	<?php endif; ?>
	
<?php else : ?>
		
	<?php 
	
	// Passage en format HTML
	$myposeoAPI -> setFormat( MyposeoAPI::HTML );
	
	// Appel du flux HTML correspondant à l'id de l'URL
	echo $myposeoAPI->getPositioningURLForPlugins( $_GET['url'] );
	
	?>

<?php endif; ?>

	</fieldset>
</div>