<?php

include_once(realpath(dirname(__FILE__)) . "/../../../classes/PluginsClassiques.class.php");
include_once("myposeo_var.php");

class Myposeo extends PluginsClassiques
{	
	public function Myposeo()
    {
		$this->PluginsClassiques();	
	}
 	
 	/**
 	* Initialisation du plugin
 	*/
 	public function init()
    {
    	
       // Clé d'identification donnée par Myposeo
       $variableAPIMyposeo = new Variable();
       $variableAPIMyposeo->nom = "cle_api_myposeo";
       $variableAPIMyposeo->protege = 1;
       $variableAPIMyposeo->add();
    }
	
	/**
	* Suppression du plugin
	*/
	public function destroy()
    {
    	
      	$variableAPIMyposeo = new Variable();
      	$variableAPIMyposeo -> charger("cle_api_myposeo");
      	$variableAPIMyposeo -> delete();
	}	

}

?>