<?php

class MyposeoAPI {
	
	// Définition de l'URL
	public static $URL_API	= 'https://www.myposeo.com/api';
	
	// Définition des constantes de format
	const XML				= 'xml';
	const HTML				= 'html';
	const JSON				= 'json';
	const SOAP				= 'soap';
		
	/**
	 * Format de retour
	 * @var String
	 */
	private $format			= 'xml';
		
	// Définition du mode de connexion
	const CONNEXION_KEY		= 'key';
	const CONNEXION_TOKEN	= 'token';
	
	/**
	 * Mode de connexion
	 * @var String
	 */
	private $connexion_mode	= 'token';
	
	/**
	 * Jeton de connexion si le compte est configuré pour la connexion à l'API par jeton
	 * @var String
	 */
	private $token			= null;
	
	/**
	 * Clé de l'API si le compte est configuré pour la connexion par clé
	 * @var String
	 */
	private $key			= null;
	
	/**
	 * Login de l'utilisateur
	 * @var String
	 */
	private $login			= null;
	
	/**
	 * Mot de passe de l'utilisateur
	 * @var String
	 */
	private $password		= null;
	
	/**
	 * Client (identification plugin)
	 * @var String
	 */
	private $client			= "";
	
	/**
	 * Paramètres à passer à l'API
	 * @var Array
	 */
	private $params			= array();
	
	/**
	 * Constructeur
	 * @param String $format : format de retour
	 */
	public function __construct( $format = MyposeoAPI::XML ) {
		
		$this->format		= $format;
		
		MyposeoAPI::$URL_API	= "https://www.myposeo.com/api";
	}
	
	/**
	 * Ajout d'un client pour un plugin
	 * @param String $client : Client qui permet d'identifier le plugin afin de rendre le bon rendu
	 */
	public function addClient( $client ) {
		
		$this->client	= $client;
	}

	/**
	 *  Ajout d'un paramètre à l'API
	 * @param $key : clé
	 * @param $value : valeur
	 */
	public function addParam( $key, $value ) {
		
		$this->params[ $key ]	= $value;
	}
	
	/**
	 * Méthode de connexion afin de récupérer le jeton de connexion
	 * @param String $login : votre login
	 * @param String $password : votre mot de passe
	 */
	public function logIn( $login, $password ) {
		
		// On enregistre les infos de l'utilisateur
		$this->login		= $login;
		$this->password		= $password;
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/login/xml' . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, '&login=' . $login . '&password=' . $password );
		
		$xml 	= curl_exec( $c );
		curl_close( $c );
		
		$dom 	= new DOMDocument();
		$dom	-> loadXML( $xml );
		
		if( intval( $dom->getElementsByTagName('code')->item(0)->nodeValue ) == -1 ) return false;
		
		$cle 	= $dom->getElementsByTagName('key')->item(0)->nodeValue;
		$token 	= $dom->getElementsByTagName('token')->item(0)->nodeValue;
		
		// Récupération des informations
		$this->key		= $cle;
		$this->token	= $token;
		
		return true;
	}
	
	/**
	 * Vérification de la validité de la clé
	 */
	public function keyIsValid() {
		
		// Vérification que la clé ne soit pas vide
		if( $this->key == "" || $this->key == null ) return false;
		
		// Connexion à l'API
		$c 		= curl_init();
		
		// Vérification de la clé en ligne		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/verify/key' . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$xml 	= curl_exec( $c );
		curl_close( $c );
		
		// Si le XML est vide on ne va pas plus loin
		if( $xml == "" ) return false;
		
		// On regarde si la clé est valide
		$dom 	= new DOMDocument();
		$dom	-> loadXML( $xml );
		return intval( $dom->getElementsByTagName('code')->item(0)->nodeValue ) == 1;
	}
	
	/**
	* Retourne la liste des URLs du compte
	*/
	public function getURLs() {
	
		// Connexion à l'API
		$c 		= curl_init();
				
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/urls/' . $this->format . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}
	
	/**
	 * Méthode qui permet de récupérer les informations de son compte myposeo
	 */
	public function getAccountInformations() {
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/account/' . $this->format . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		
		// Si le jeton n'est pas nul, on peut ajouter ses informations
		if( $this->token != null ) {
			
			curl_setopt( $c, CURLOPT_POST, true );
			curl_setopt( $c, CURLOPT_POSTFIELDS, '&token=' . $this->token );
		}
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}

	/**
	 * Méthode qui permet de récupérer les informations d'un suivi de positionnement
	 * @param Date $date : date du suivi demandée
	 */
	public function getPositioning( $date = null ) {
		
		// Si la date est nulle, on inscrit la date du jour par défaut
		if( $date == null ) $date	= date('Y-m-d');
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/positioning/' . $this->format . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, '&date=' . $date . ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}
	
	/**
	 * Méthode qui permet de récupérer les informations d'une URL
	 * @param int $idURL : id de l'url du compte dont on souhaite récupérer les informations
	 */
	public function getPositioningURL( $idURL ) {
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/url/' . $this->format . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, '&url=' . $idURL . ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}

	/**
	 * Méthode qui permet de récupérer les informations d'un suivi de positionnement pour une URL
	 * @param int $idURL : id de l'URL
	 * @param Date $date : date du suivi demandée
	 */
	public function getPositioningURLForPlugins( $idURL, $date = null ) {
		
		// Si la date est nulle, on inscrit la date du jour par défaut
		if( $date == null ) $date	= date('Y-m-d');
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/url/format/html' . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, '&url=' . $idURL . '&date=' . $date . ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}

	/**
	 * Méthode qui permet de récupérer les informations des concurrents d'une URL
	 * @param int $idURL : id de l'url du compte dont on souhaite récupérer les informations
	 */
	public function getConcurrents( $idURL ) {
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/concurrents/' . $this->format . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, '&url=' . $idURL . ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}

	/**
	 * Méthode qui permet de récupérer les informations des mots clés d'une URL
	 * @param int $idURL : id de l'url du compte dont on souhaite récupérer les informations
	 */
	public function getKeywords( $idURL ) {
		
		// Connexion à l'API
		$c 		= curl_init();
		
		curl_setopt( $c, CURLOPT_URL, MyposeoAPI::$URL_API . '/keywords/' . $this->format . '?myposeo_client=' . $this->client . $this->_getParamsString() );
		curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $c, CURLOPT_CONNECTTIMEOUT, 3 );
		curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $c, CURLOPT_POST, true );
		curl_setopt( $c, CURLOPT_POSTFIELDS, '&url=' . $idURL . ( ( $this->connexion_mode == MyposeoAPI::CONNEXION_KEY ) ? '&key=' . $this->_getKey() : '&token=' . $this->_getToken() ) );
		
		$datas 	= curl_exec( $c );
		curl_close( $c );
		
		// On retourne les informations
		return $datas;
	}
	
	/**
	 * Accesseur au token de connexion
	 */
	public function getToken() {
		
		return $this->token;
	}
	
	/**
	 * Accesseur de la clé
	 */
	public function getkey() {
		
		return $this->key;
	}
	
	/**
	 * Méthode qui permet de renseigner la clé de l'API
	 * @param String $cle : clé privée de l'API
	 */
	public function setKey( $cle ) {
		
		$this->key		= $cle;
	}
	/**
	 * Méthode qui permet de changer le mode de connexion
	 * @param String $mode : mode de connexion
	 */
	public function setConnexionMode( $mode ) {
		
		$this->connexion_mode	= $mode;
	}

	/**
	 * Méthode qui permet de renseigner le format souhaité
	 * @param String $format : format de retour
	 */
	public function setFormat( $format ) {
		
		$this->format	= $format;
	}
	
	/**
	 * Méthode provée de récupération de jeton
	 */
	private function _getToken() {
		
		// On vérifie que le login et le mot de passe existe
		if( $this->login == null || $this->password == null )
			throw new Exception( "Vous devez vous indentifiez pour vous connecter aux différents services." );
		
		// On récupère le jeton
		$this->logIn( $this->login, $this->password );
		
		// On retourne le jeton
		return $this->token;
	}

	/**
	 * Méthode provée de récupération de clé
	 */
	private function _getKey() {
		
		// On vérifie que le login et le mot de passe existe
		if( $this->key == null )
			throw new Exception( "Vous devez vous indentifiez pour obtenir votre clé ou la renseigner manuellement." );
		
		// On retourne la clé
		return $this->key;
	}
	
	/**
	 * Retourne la liste des paramètres sous forme de chaines de caractères
	 */
	private function _getParamsString() {
		
		$str	= "";
		foreach( $this->params as $key => $param ) {
			
			$str .= "&" . $key . "=" . $param;
		}
		
		return $str;
	}
}

?>