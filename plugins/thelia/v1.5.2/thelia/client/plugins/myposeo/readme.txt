Installation et utilisation du plugin myposeo pour Thélia

Le module myposeo dashboard pour Thélia vous permet dafficher, dans le back office de votre site ou de votre site Thélia les résultats de positionnement de votre compte myposeo.

Installation : 

Depuis le back-office de Thélia, charger l'archive depuis le menu "AJOUTER UN PLUGIN" puis activer le module.

Dans la gestion des variables, renseignez le champ "cle_api_myposeo" avec la clé de votre API myposeo.
Si vous ne connaissez pas votre clé, rendez vous sur le module myposeo ou vous aurez la possibilité de la récupérer automatiquement en inscrivant vos identifiants de compte myposeo.

Pour toutes questions sur le module, vou pouvez contacter le support myposeo depuis votre compte et l'onglet "Support".