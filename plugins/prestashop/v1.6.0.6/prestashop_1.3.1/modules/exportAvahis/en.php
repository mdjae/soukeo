<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{exportAvahis}prestashop>exportAvahis_82b5a57b2835fad00a30bafb01d328b7'] = 'Informations : ';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_3e9914add26f0592374434cbdb44cce5'] = 'Avahis : export your Catalogue to the Avahis Marketplace';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_3491e2eac6304c9d5dcee94c58c30313c'] = 'Avahis : export your Catalogue';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_574e0b7aece74215db65b0ee26ab58da'] = 'Avahis : export your Catalogue to the Avahis Marketplace';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_0a739b221caaa9ed4dbc0ef59edc359a'] = 'Avahis est une solution SaaS permettant à un e-commerçant d\'optimiser ses catalogues produits vers les comparateurs de prix, régies d\'affiliation & cashbacks, places de marché mais aussi liens sponsorisés, réseaux sociaux et blogs.';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_7078c3438bb46fdab36587050053cacf'] = 'Le principe est que la solution récupère le catalogue produits du marchand, configure, optimise et tracke les informations des campagnes marchandes afin de restituer à l\'e-commerçant les statistiques sous forme de tableaux de bords et graphiques.';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_4116f2e6b08ae5aae73ed7f399cf45ee'] = 'Ce processus permet aux e-commerçants d\'optimiser leurs flux et leurs coûts d\'acquisition sur chaque support de diffusion.';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_9a71923a8dbd55e9f3e4b2225ed7f2dd'] = 'Exportez votre catalogue produits vers les comparateurs de prix, places de marché, régies d\'affiliation, réseaux sociaux, blogs et liens sponsorisés.';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_4cc7b2483088b9f51e2b7d11603d7bd7'] = 'Etes vous certain de vouloir désinstaller le module Avahis?';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_1b5e4f9b07ad87029a7f94ce4006b1da'] = 'Le Module Avahis ne doit pas être renomé';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_365589d7ae6168a24ff2de3c924149ed'] = 'Solution Avahis';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_2c251c08b2589ac8a4dc78150b423417'] = 'Choisissez vos catégories et produits à exporter : ';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_fcd47c11a73886a4fe3840db0ba9d6ad'] = 'Arbre des catégories : ';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_deb10517653c255364175796ace3553f'] = 'Produit';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_629709b86fd09c134ba328eeeeef63cc'] = 'Vous êtes client chez Avahis? Veuillez saisir votre identifiant : ';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_b01bcbd0500cda4c67a727b6cd61754e'] = 'Votre identifiant doit être un entier > 0';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_a27373b8e74f6483f3c6615caf08ec79'] = 'Votre Group ID doit être un entier > 0';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_bc74aa3ba771ee7e1adf49a9430083ff'] = 'Choisissez la langue à utiliser dans l\'export : ';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_5e9df908eafa83cb51c0a3720e8348c7'] = 'Cocher tous les produits';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_9747d23c8cc358c5ef78c51e59cd6817'] = 'Décocher tous les produits';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_07e5a123264f67e171d0493ba8774875'] = 'En indiquant votre identifiant Avahis ainsi que l\'id de groupe, vous pourrez ainsi placer le tag de conversion <a href="http://www.Avahis.fr/tagcapsule.html" target="_blank" style="color:black;text-decoration:underline">TagCapsule</a> automatiquement sur l\'ensemble des pages de votre boutique.';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_c2410ff87789464f6533bafeb9f79273'] = 'Où trouvez mes identifiants de groupe et client ?';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_394dcde2383771cc4b12c88f9edafa86'] = 'Vous pouvez visualiser vos identifiants sur votre page Compte Avahis';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_7edc03a48f430c6b36c9efab3fc43965'] = 'id client';
$_MODULE['<{exportAvahis}prestashop>exportAvahis_323e237c481315f14a50e7fe99b34121'] = 'id de groupe';

