<?php
class exportAvahis extends Module
{
	public function __construct()
	{
		$this->name = 'exportAvahis';
		$this->tab = 'export';
		$this->version = '1.4.5';
		$this->author = 'Soukeo';

		parent::__construct();

		$this->displayName = $this->l('Avahis');
		$this->description = $this->l('Exportez votre catalogue sur la market-place Avahis de Avahis.');
		$this->confirmUninstall = $this->l('Êtes-vous sûr de vouloir désinstaller le module Avahis ?');
	}

	public function install()
	{
		if(!parent::install()
		|| !$this->registerHook('leftColumn')
		|| !Configuration::updateValue('MOD_EXPORT_SOUKEO_URL', constant('__PS_BASE_URI__').DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'exportAvahis'.DIRECTORY_SEPARATOR.'logo-mini.jpg'))
			return false;
		return true;
	}

	public function uninstall()
	{
		$sql = "DROP TABLE `".constant('_DB_PREFIX_')."parametre_avahis`";
		Db::getInstance()->ExecuteS($sql);
		if(!parent::uninstall() || !Configuration::deleteByName('MOD_EXPORT_SOUKEO_URL'))
			return false;
		return true;
	}


	public function getContent()
	{
		$html = '';
		$exportAvahis_url = 'http://'.$_SERVER['HTTP_HOST'].constant('__PS_BASE_URI__').'modules/exportAvahis/export.php';
		$exportFullAvahis_url = 'http://'.$_SERVER['HTTP_HOST'].constant('__PS_BASE_URI__').'modules/exportAvahis/export.php?mode=full';
		$html .= '<h2>'.$this->l('Avahis : Export de votre catalogue').' (v'.$this->version.')</h2>
    	<fieldset>
			<legend>'.$this->l('Informations  :').'</legend><b>'.$this->l("Avahis est la place de marché électronique de l'Océan Indien.").'</b><br />	
			<br />'.$this->l("Intégrez notre module Prestashop pour synchroniser le catalogue de votre site e-commerce chez Avahis. Mettez gratuitement vos produits en vente chez Avahis.").
			"<br />".$this->l("Nous accueillons tous les e-commerçants de La Réunion, Maurice, Madagascar et Mayotte soucieux de développer leur visibilité et leur chiffre d'affaire.").' 
			<br />'.$this->l("Gérez vos ventes, vos stocks et vos catalogues depuis votre backoffice Prestashop.").'
			<br />'.$this->l("Bénéficiez des mises à jour et des services premium d'Avahis.").'
			<br clear="all" />
			<br clear="all" />
			<a href="http://www.avahis.fr" target="_blank"><div style="background-color:#FFFFFF;text-align:center;padding:10px;border:1px solid #DDD"><img src="http://'.$_SERVER['HTTP_HOST'].constant('__PS_BASE_URI__').'modules/exportAvahis/logo_600x300.jpg" alt="'.$this->l('avahis Solution').'" border="0" /></div></a>
		</fieldset>
		
		<br clear="all" />
		<fieldset>
			<legend>'.$this->l('URL de votre catalogue produit :').'</legend>
        	<a href="'.$exportAvahis_url.'" target="_blank" style="font-family:Courier">'.$exportAvahis_url.'</a>
        		
		</fieldset>
		
		<br clear="all" />';
		
		return $html.$this->displayForm();
	}

	public function secureDir($dir)
	{
		define('_SOUKEO_DIR_',DIRECTORY_SEPARATOR.'exportAvahis');
		define('MSG_ALERT_MODULE_NAME',$this->l('Module Avahis should not be renamed'));

		if($dir != _SOUKEO_DIR_)
		{
			echo utf8_decode(MSG_ALERT_MODULE_NAME);
			exit();
		}
	}

	public function netoyage_html($CatList)
	{
		$pattern = '@<[\/\!]*?[^<>]*?>@si'; //nettoyage du code HTML
		$CatList = preg_replace($pattern, ' ', $CatList); 
		$CatList = preg_replace('/[\s]+/', ' ', $CatList); //nettoyage des espaces multiples
		
		$CatList = trim ($CatList);
		$CatList = str_replace("&nbsp;"," ",$CatList);
		$CatList = str_replace("|"," ",$CatList);
		$CatList = str_replace("&#39;","' ",$CatList);
		$CatList = str_replace("&#150;","-",$CatList);
		$CatList = str_replace(chr(9)," ",$CatList);
		$CatList = str_replace(chr(10)," ",$CatList);
		$CatList = str_replace(chr(13)," ",$CatList);
		return $CatList;
	}

	public function displayForm()
	{
		$output = '';
		ob_start();
		include('formAvahis.php');
		$output = ob_get_clean();
		return $output;
		ob_end_clean();
	}
}
?>
