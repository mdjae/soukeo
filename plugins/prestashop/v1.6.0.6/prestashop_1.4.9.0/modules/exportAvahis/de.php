<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{exportavahis}prestashop>exportavahis_82b5a57b2835fad00a30bafb01d328b7'] = 'Informationen: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_f153d92c82352821bc659e5b8c753288'] = 'URL des Produktkatalogs: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_0392e77d19d8f4c2f2cd5c93ce4dc87c'] = 'URL des Produktkatalogs mit Varianten: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_574e0b7aece74215db65b0ee26ab58da'] = 'avahis: Export des Katalogs';
$_MODULE['<{exportavahis}prestashop>exportavahis_0a739b221caaa9ed4dbc0ef59edc359a'] = 'avahis ist eine SaaS-Lösung , die E-Händlern ermöglicht das Produktkatalog für die Preisvergleicher, Affiliate-Dienste, Marketplace oder Cashback-Seite zu optimieren.';
$_MODULE['<{exportavahis}prestashop>exportavahis_7078c3438bb46fdab36587050053cacf'] = 'Die Lösung integriert das Katalog, konfiguriert, optimiert und verfolgt alle Informationen der Kampagnen und stellt Statistiken-Dashboards und Grafike zur Verfügung.';
$_MODULE['<{exportavahis}prestashop>exportavahis_4116f2e6b08ae5aae73ed7f399cf45ee'] = 'Dieser Vorgang ermöglicht den Online-Händlern die Feeds und die Kosten für alle Plattforme zu optimieren.';
$_MODULE['<{exportavahis}prestashop>exportavahis_9a71923a8dbd55e9f3e4b2225ed7f2dd'] = 'Exportieren Sie das Produktkatalog zur avahis.';
$_MODULE['<{exportavahis}prestashop>exportavahis_4cc7b2483088b9f51e2b7d11603d7bd7'] = 'Möchten Sie das Modul avahis deinstallieren?';
$_MODULE['<{exportavahis}prestashop>exportavahis_1b5e4f9b07ad87029a7f94ce4006b1da'] = 'Das Modul avahis kann nicht umbenannt werden';
$_MODULE['<{exportavahis}prestashop>exportavahis_365589d7ae6168a24ff2de3c924149ed'] = 'Lösung avahis';
$_MODULE['<{exportavahis}prestashop>exportavahis_2c251c08b2589ac8a4dc78150b423417'] = 'Wählen Sie die Kategorie und Produkte aus, die Sie exportieren möchten: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_fcd47c11a73886a4fe3840db0ba9d6ad'] = 'Kategoriebaum: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{exportavahis}prestashop>exportavahis_deb10517653c255364175796ace3553f'] = 'Produkt';
$_MODULE['<{exportavahis}prestashop>exportavahis_629709b86fd09c134ba328eeeeef63cc'] = 'Sie sind schon Kunde bei avahis? Bitte geben Sie hier Ihre Benutzerdaten ein: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_b01bcbd0500cda4c67a727b6cd61754e'] = 'Ihr Benutzername soll eine ganze Zahl sein > 0';
$_MODULE['<{exportavahis}prestashop>exportavahis_a27373b8e74f6483f3c6615caf08ec79'] = 'Ihre Gruppe-ID soll eine ganze Zahl sein > 0';
$_MODULE['<{exportavahis}prestashop>exportavahis_bc74aa3ba771ee7e1adf49a9430083ff'] = 'Bitte die Sprache für den Export auswählen: ';
$_MODULE['<{exportavahis}prestashop>exportavahis_5e9df908eafa83cb51c0a3720e8348c7'] = 'Kreuzen Sie alle Produkte ein';
$_MODULE['<{exportavahis}prestashop>exportavahis_9747d23c8cc358c5ef78c51e59cd6817'] = 'Ankreuzen für alle Produkte rückgängig machen';
$_MODULE['<{exportavahis}prestashop>exportavahis_07e5a123264f67e171d0493ba8774875'] = 'Um das Conversion-Tag auf der Bestellungsbestätigungsseite einzubauen, geben Sie hier Ihre avahis-Benutzerdaten.';
$_MODULE['<{exportavahis}prestashop>exportavahis_c2410ff87789464f6533bafeb9f79273'] = 'Wo kann ich meine Kunden- und Gruppen-ID finden?';
$_MODULE['<{exportavahis}prestashop>exportavahis_394dcde2383771cc4b12c88f9edafa86'] = 'Sie können Ihre Benutzerdaten auf Ihrem Konto finden.';
$_MODULE['<{exportavahis}prestashop>exportavahis_7edc03a48f430c6b36c9efab3fc43965'] = 'Kunden-ID';
$_MODULE['<{exportavahis}prestashop>exportavahis_323e237c481315f14a50e7fe99b34121'] = 'Gruppen-ID';

