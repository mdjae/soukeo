<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_82b5a57b2835fad00a30bafb01d328b7'] = 'Informations : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_f153d92c82352821bc659e5b8c753288'] = 'URL de votre catalogue produit : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_0392e77d19d8f4c2f2cd5c93ce4dc87c'] = 'URL de votre catalogue produit avec les déclinaisons : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_574e0b7aece74215db65b0ee26ab58da'] = 'Soukeo : export de votre catalogue produits';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_0a739b221caaa9ed4dbc0ef59edc359a'] = 'Soukeo est une solution SaaS permettant à un e-commerçant d\'optimiser ses catalogues produits vers les comparateurs de prix, régies d\'affiliation & cashbacks, places de marché mais aussi liens sponsorisés, réseaux sociaux et blogs.';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_7078c3438bb46fdab36587050053cacf'] = 'Le principe est que la solution récupère le catalogue produits du marchand, configure, optimise et tracke les informations des campagnes marchandes afin de restituer à l\'e-commerçant les statistiques sous forme de tableaux de bords et graphiques.';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_4116f2e6b08ae5aae73ed7f399cf45ee'] = 'Ce processus permet aux e-commerçants d\'optimiser leurs flux et leurs coûts d\'acquisition sur chaque support de diffusion.';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_9a71923a8dbd55e9f3e4b2225ed7f2dd'] = 'Exportez votre catalogue produits vers les comparateurs de prix, places de marché, régies d\'affiliation, réseaux sociaux, blogs et liens sponsorisés.';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_4cc7b2483088b9f51e2b7d11603d7bd7'] = 'Etes vous certain de vouloir désinstaller le module Soukeo?';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_1b5e4f9b07ad87029a7f94ce4006b1da'] = 'Le Module Soukeo ne doit pas être renomé';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_365589d7ae6168a24ff2de3c924149ed'] = 'Solution Soukeo';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_2c251c08b2589ac8a4dc78150b423417'] = 'Choisissez vos catégories et produits à exporter : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_fcd47c11a73886a4fe3840db0ba9d6ad'] = 'Arbre des catégories : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_deb10517653c255364175796ace3553f'] = 'Produit';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_629709b86fd09c134ba328eeeeef63cc'] = 'Vous êtes client chez Soukeo? Veuillez saisir votre identifiant : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_b01bcbd0500cda4c67a727b6cd61754e'] = 'Votre identifiant doit être un entier > 0';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_a27373b8e74f6483f3c6615caf08ec79'] = 'Votre Group ID doit être un entier > 0';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_bc74aa3ba771ee7e1adf49a9430083ff'] = 'Choisissez la langue à utiliser dans l\'export : ';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_5e9df908eafa83cb51c0a3720e8348c7'] = 'Cocher tous les produits';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_9747d23c8cc358c5ef78c51e59cd6817'] = 'Décocher tous les produits';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_07e5a123264f67e171d0493ba8774875'] = 'En indiquant votre identifiant Soukeo ainsi que l\'id de groupe, vous pourrez ainsi placer le tag de conversion <a href="http://www.Soukeo.fr/tagcapsule.html" target="_blank" style="color:black;text-decoration:underline">TagCapsule</a> automatiquement sur l\'ensemble des pages de votre boutique.';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_c2410ff87789464f6533bafeb9f79273'] = 'Où trouvez mes identifiants de groupe et client ?';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_394dcde2383771cc4b12c88f9edafa86'] = 'Vous pouvez visualiser vos identifiants sur votre page Compte Soukeo';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_7edc03a48f430c6b36c9efab3fc43965'] = 'id client';
$_MODULE['<{exportSoukeo}prestashop>exportSoukeo_323e237c481315f14a50e7fe99b34121'] = 'id de groupe';

