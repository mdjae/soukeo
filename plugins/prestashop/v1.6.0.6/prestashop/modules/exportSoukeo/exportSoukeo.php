<?php
class exportSoukeo extends Module
{
	public function __construct()
	{
		$this->name = 'exportSoukeo';
		$this->tab = 'export';
		$this->version = '1.1';
		$this->author = 'Soukeo';

		parent::__construct();

		$this->displayName = $this->l('Soukeo');
		$this->description = $this->l('Exportez votre catalogue sur la market-place Avahis de Soukeo.');
		$this->confirmUninstall = $this->l('Êtes-vous sûr de vouloir désinstaller le module Soukeo ?');
	}

	public function install()
	{
		if(!parent::install()
		|| !$this->registerHook('leftColumn')
		|| !Configuration::updateValue('MOD_EXPORT_SOUKEO_URL', constant('__PS_BASE_URI__').DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'exportSoukeo'.DIRECTORY_SEPARATOR.'logo-mini.jpg'))
			return false;
		return true;
	}

	public function uninstall()
	{
		$sql = "DROP TABLE `".constant('_DB_PREFIX_')."parametre_soukeo`";
		Db::getInstance()->ExecuteS($sql);
		if(!parent::uninstall() || !Configuration::deleteByName('MOD_EXPORT_SOUKEO_URL'))
			return false;
		return true;
	}


	public function getContent()
	{
		$html = '';
		$exportSoukeo_url = 'http://'.$_SERVER['HTTP_HOST'].constant('__PS_BASE_URI__').'modules/exportSoukeo/export.php';
		$exportFullSoukeo_url = 'http://'.$_SERVER['HTTP_HOST'].constant('__PS_BASE_URI__').'modules/exportSoukeo/export.php?mode=full';
		$html .= '<h2>'.$this->l('Soukeo : Export de votre catalogue').' (beta v'.$this->version.')</h2>
    	<fieldset>
			<legend>'.$this->l('Informations  :').'</legend>'.$this->l("Soukeo est la place de marché de l'Océan Indien .").'<br />	
			<br />'.$this->l('Version beta. Ce module a pour but de récupérer les produits des catégories que vous avez séléctionné dans votre catalogue.').' 
			<br />'.$this->l('Vos produits seront ensuite, suite à validation, présent dans votre espace e-commerçant sur la place de marché Avahis de Soukeo.').'
			<br clear="all" />
			<br clear="all" />
			<a href="http://www.soukeo.fr" target="_blank"><div style="background-color:#FFFFFF;text-align:center;padding:10px;border:1px solid #DDD"><img src="http://'.$_SERVER['HTTP_HOST'].constant('__PS_BASE_URI__').'modules/exportSoukeo/logo_mini.jpg" alt="'.$this->l('soukeo Solution').'" border="0" /></div></a>
		</fieldset>
		
		<br clear="all" />
		<fieldset>
			<legend>'.$this->l('URL de votre catalogue produit :').'</legend>
        	<a href="'.$exportSoukeo_url.'" target="_blank" style="font-family:Courier">'.$exportSoukeo_url.'</a>
        		
		</fieldset>
		
		<br clear="all" />';
		
		return $html.$this->displayForm();
	}

	public function secureDir($dir)
	{
		define('_SOUKEO_DIR_',DIRECTORY_SEPARATOR.'exportSoukeo');
		define('MSG_ALERT_MODULE_NAME',$this->l('Module Soukeo should not be renamed'));

		if($dir != _SOUKEO_DIR_)
		{
			echo utf8_decode(MSG_ALERT_MODULE_NAME);
			exit();
		}
	}

	public function netoyage_html($CatList)
	{
		$pattern = '@<[\/\!]*?[^<>]*?>@si'; //nettoyage du code HTML
		$CatList = preg_replace($pattern, ' ', $CatList); 
		$CatList = preg_replace('/[\s]+/', ' ', $CatList); //nettoyage des espaces multiples
		
		$CatList = trim ($CatList);
		$CatList = str_replace("&nbsp;"," ",$CatList);
		$CatList = str_replace("|"," ",$CatList);
		$CatList = str_replace("&#39;","' ",$CatList);
		$CatList = str_replace("&#150;","-",$CatList);
		$CatList = str_replace(chr(9)," ",$CatList);
		$CatList = str_replace(chr(10)," ",$CatList);
		$CatList = str_replace(chr(13)," ",$CatList);
		return $CatList;
	}

	public function displayForm()
	{
		$output = '';
		ob_start();
		include('formSoukeo.php');
		$output = ob_get_clean();
		return $output;
		ob_end_clean();
	}
}
?>
