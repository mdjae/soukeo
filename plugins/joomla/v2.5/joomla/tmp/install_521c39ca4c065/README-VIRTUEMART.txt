Installation procedure is described here:

http://docs.virtuemart.net/tutorials/30-installation-migration-upgrade-vm-2/80-installation-of-virtuemart-2

or

http://dev.virtuemart.net/projects/virtuemart/wiki/Installation_of_VirtueMart_2_with_provided_packages

if you still have trouble, please visit

http://forum.virtuemart.net/index.php?board=115.0

Install first the core, then the aio!