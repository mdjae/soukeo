<?php
/* Require the config */
$separator = '|';
$champTitre  = 'ID_PRODUCT'.$separator.'REFERENCE_PRODUCT'.$separator.'CATEGORY'.$separator.'NAME_PRODUCT'.$separator.'DESCRIPTION'.$separator;
$champTitre .= 'DESCRIPTION_SHORT'.$separator.'PRICE_PRODUCT'.$separator.'WEIGHT'.$separator.'WEIGHT_UNIT'.$separator.'LENGHT'.$separator;
$champTitre .= 'WIDHT'.$separator.'HEIGHT'.$separator.'LWH_UNIT'.$separator.'META_DESCRIPTION'.$separator.'META_KEYWORD'.$separator;
$champTitre .= 'META_TITLE'.$separator.'IMAGE_PRODUCT'.$separator.'THUMBNAIL'.$separator.'ETAT_PROMO'.$separator.'PRICE_REDUCTION'.$separator;
$champTitre .= 'SPECIAL_PRODUCT'.$separator.'QUANTITY'.$separator.'AVAILABLE_PRODUCT'.$separator.'DEVISE'.$separator.'MANUFACTURER'.$separator;
$champTitre .= 'PRODUCT_PRICE_PUBLISH_UP'.$separator.'PRODUCT_PRICE_PUBLISH_DOWN'.$separator.'PRICE_QUANTITY_START'.$separator.'PRICE_QUANTITY_END';
$champTitre .="\n\r";
"TITRECHAMPERSO1;TITRECHAMPPERSONNALISE2;CAPACITE;COULEUR;TAILLE ";

//Console::logSpeed('virtuemart start');
ini_set('display_errors', 1);
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

error_reporting(E_ALL ^ E_NOTICE);
define( '_JEXEC', 1 );
define('JPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );

require_once (JPATH_BASE.DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
require_once(JPATH_BASE.DS.'configuration.php' );
require_once(JPATH_LIBRARIES.DS.'loader.php' );
require_once ( JPATH_LIBRARIES.DS.'joomla'.DS.'factory.php' );
require_once (JPATH_LIBRARIES . '/import.php');
require_once (JPATH_LIBRARIES . '/cms.php');
//require_once(JPATH_LIBRARIES.DS.'joomla'.DS.'database'.DS.'table.php' );
jimport('joomla.database.table');
jimport('joomla.application.menu');
jimport('joomla.environment.uri');
jimport('joomla.utilities.utility');
jimport('joomla.event.dispatcher');
jimport('joomla.utilities.arrayhelper');
jimport('joomla.form.formfield');
jimport('joomla.application.component.modellist');
jimport('joomla.application.component.controller');
jimport('joomla.application.component.modellist');
jimport('joomla.application.component.view');

//if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
require_once(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'shopfunctions.php');
require_once(JPATH_VM_ADMINISTRATOR . DS . 'tables' . DS . 'categories.php');
require_once(JPATH_VM_ADMINISTRATOR . DS . 'elements' . DS . 'vmelements.php');

VmConfig::loadConfig();

vmRam('Start');
vmSetStartTime('Start');

VmConfig::loadJLang('com_virtuemart', true);

if(VmConfig::get('shop_is_offline',0)){
	$_controller = 'virtuemart';
	require (JPATH_VM_SITE.DS.'controllers'.DS.'virtuemart.php');
	JRequest::setVar('view', 'virtuemart');
	$task='';
	$basePath = JPATH_VM_SITE;
} else {

	/* Front-end helpers */
	if(!class_exists('VmImage')) require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'image.php'); //dont remove that file it is actually in every view except the state view
	if(!class_exists('shopFunctionsF'))require(JPATH_VM_SITE.DS.'helpers'.DS.'shopfunctionsf.php'); //dont remove that file it is actually in every view

	/* Loading jQuery and VM scripts. */
	//vmJsApi::jPrice();    //in create button
	vmJsApi::jQuery();
	vmJsApi::jSite();
	vmJsApi::cssSite();

	$_controller = JRequest::getWord('view', JRequest::getWord('controller', 'virtuemart')) ;
	$trigger = 'onVmSiteController';
// 	$task = JRequest::getWord('task',JRequest::getWord('layout',$_controller) );		$this makes trouble!
	$task = JRequest::getWord('task','') ;

	if (($_controller == 'product' || $_controller == 'category') && ($task == 'save' || $task == 'edit') ) {
		$app = JFactory::getApplication();

		vmJsApi::js('vmsite');
		vmJsApi::jQuery(FALSE);
		if(!class_exists('Permissions')) require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'permissions.php');
		if	(Permissions::getInstance()->check("admin,storeadmin")) {
			$jlang =JFactory::getLanguage();
			$jlang->load('com_virtuemart', JPATH_ADMINISTRATOR, null, true);
			$basePath = JPATH_VM_ADMINISTRATOR;
			$trigger = 'onVmAdminController';
		} else {
			$app->redirect('index.php?option=com_virtuemart', jText::_('COM_VIRTUEMART_RESTRICTED_ACCESS') );
		}

	} elseif($_controller) {
			$basePath = JPATH_VM_SITE;
	}
}

/* Create the controller name */
$_class = 'VirtuemartController'.ucfirst($_controller);

if (file_exists($basePath.DS.'controllers'.DS.$_controller.'.php')) {
	if (!class_exists($_class)) {
		require ($basePath.DS.'controllers'.DS.$_controller.'.php');
	}
}
else {
	// try plugins
	JPluginHelper::importPlugin('vmextended');
	$dispatcher = JDispatcher::getInstance();
	$dispatcher->trigger($trigger, array($_controller));
}


if (class_exists($_class)) {
    $controller = new $_class();

	// try plugins
	JPluginHelper::importPlugin('vmuserfield');
	$dispatcher = JDispatcher::getInstance();
	$dispatcher->trigger('plgVmOnMainController', array($_controller));

    /* Perform the Request task */
    $controller->execute($task);

    //Console::logSpeed('virtuemart start');
    vmTime($_class.' Finished task '.$task,'Start');
    vmRam('End');
    vmRamPeak('Peak');
    /* Redirect if set by the controller */
    $controller->redirect();
} else {
    vmDebug('VirtueMart controller not found: '. $_class);
    $mainframe = Jfactory::getApplication();
    $mainframe->redirect(JRoute::_ ('index.php?option=com_virtuemart&view=virtuemart', FALSE));
}