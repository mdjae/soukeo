<?php
ini_set('display_errors', 1);
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

error_reporting(E_ALL ^ E_NOTICE);
define( '_JEXEC', 1 );
define('JPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );

require_once (JPATH_BASE.DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
// configuration.php le cas ou le répertoire a été mit en écriture total 777.
require_once(JPATH_BASE.DS.'configuration.php' );
require_once(JPATH_LIBRARIES.DS.'loader.php' );
require_once ( JPATH_LIBRARIES.DS.'joomla'.DS.'factory.php' );

// il faudrat amélioré dans le cas ou l'installation fait que le l'e-commerçant
// a dut crée le fichier database, et a voir si il est bien dans le répertoir include ou en racine 
//require_once (JPATH_BASE.DS.'includes'.DS.'database.php' );

//database connection
$jconfig = new JConfig();
$db_error = "Erreur connexion BDD.";
$db_config = mysql_connect( $jconfig->host, $jconfig->user, $jconfig->password ) or die( $db_error );
mysql_select_db( $jconfig->db, $db_config ) or die( $db_error );
$query  = "SELECT * FROM ".$jconfig->dbprefix."virtuemart_products ";
//$query  .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_product_categories as pc ON p.product_id= pc.virtuemart_product_id ";
//$query  .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_categories as c ON pc.category_id= c.virtuemart_category_id ";
$query  .= "WHERE published='1'";
// echo 'request: '.$query.'<br>';
$result = mysql_query($query);
if (!$result) {
	
   echo $jconfig->dbprefix.': Impossible d\'exécuter la requete : ' . mysql_error();
   exit;
}

$separator = '|';
echo 'id'.$separator.'sku'.$separator.'categorie'.$separator.'titre'.$separator.'prix'.$separator.'prixDiscount'.$separator.'delais_livraison'.$separator.'stock'.$separator.'description'.$separator.'image'.$separator.'url'.$separator.'poid'.$separator.'devise'.$separator.'frais_de_port'."\n";

while ($row = mysql_fetch_assoc($result)) 
{
	$sql_tva = 'SELECT tax_rate FROM '.$jconfig->dbprefix.'virtuemart_tax_rate WHERE tax_rate_id='.$row['product_tax_id'];
	$result_tva = mysql_query($sql_tva);
	while ($row_tva = mysql_fetch_assoc($result_tva)) 
	{
		$tva = $row_tva['tax_rate'];
	}
	$id = $row['product_id'];
	$sku = utf8_encode($row['product_sku']);
	$categorie = utf8_encode(stripslashes($row['category_name']));
	$titre = utf8_encode(stripslashes($row['product_name']));
	$sql_prix = 'SELECT product_price, product_currency FROM '.$jconfig->dbprefix.'virtuemart_product_price WHERE product_id='.$row['product_id'];
	$result_prix = mysql_query($sql_prix);
	while ($row_prix = mysql_fetch_assoc($result_prix)) 
	{
		$prix = $row_prix['product_price']*(1+$tva);
		$prixHT = $row_prix['product_price'];
		$prix = number_format($prix, 2);
		$devise = $row_prix['product_currency'];
	}
	$today = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('y'));
	$sql_prix_discount = 'SELECT amount, is_percent, end_date, start_date FROM '.$jconfig->dbprefix.'virtuemart_product_discount WHERE discount_id='.$row['product_discount_id'];
	$result_prix_discount = mysql_query($sql_prix_discount);
	$prixDiscount = 0;
	while($row_prix_discount = mysql_fetch_assoc($result_prix_discount)) 
	{
		$montant = $row_prix_discount['amount'];
		$end_date = $row_prix_discount['end_date'];
		$start_date = $row_prix_discount['start_date'];
		$is_percent = $row_prix_discount['is_percent'];
		if($start_date < $today)
		{
			if($end_date > $today || $end_date==0)
			//if($end_date > $today)
			{
				if($is_percent == 1)
				{
					$prixDiscount = ($prixHT - ($prixHT*$montant)/100);
					$prixDiscount = $prixDiscount *(1+$tva);
					$prixDiscount = number_format($prixDiscount, 2);
				}
				else
				{
					$prixDiscount = ($prixHT*(1+$tva))- $montant;
					$prixDiscount = number_format($prixDiscount, 2);
				}
			}
			else
			{
				$prixDiscount ='';
			}
		}
		else
		{
			$prixDiscount ='';
		}
	echo "prix: ".$prix."\n";
	echo "prixHT: ".$prixHT."\n";
	echo "today: ".$today."\n";
	echo "start_date: ".$start_date."\n";
	echo "end_date: ".$end_date."\n";
	echo "prixDiscount: ".$prixDiscount."\n";
	}
	$delais_livraison = $row['product_availability'];
	$stock = $row['product_in_stock'];
	$description = str_replace("\r", '', $row['product_desc']);
	$description = utf8_encode(str_replace("\n", '', $description));
	$image = 'http://'.$_SERVER['SERVER_NAME'].'/components/com_virtuemart/shop_image/product/'.$row['product_full_image'];
	$url = 'http://'.$_SERVER['SERVER_NAME'].'/index.php?page=shop.product_details&product_id='.$id;
	$poid = $row['product_weight'];
	$sql_fdp = 'SELECT zone_cost FROM '.$jconfig->dbprefix.'virtuemart_zone_shipping WHERE zone_name="Default"';
	$result_fdp = mysql_query($sql_fdp);
	while($row_fdp = mysql_fetch_assoc($result_fdp)) 
	{
		$frais_de_port = $row_fdp['zone_cost'];
	}
	echo '"'.$id.'"'.$separator.'"'.$sku.'"'.$separator.'"'.$categorie.'"'.$separator.'"'.$titre.'"'.$separator.'"'.$prix.'"'.$separator.'"'.$prixDiscount.'"'.$separator.'"'.$delais_livraison.'"'.$separator.'"'.$stock.'"'.$separator.'"'.$description.'"'.$separator.'"'.$image.'"'.$separator.'"'.$url.'"'.$separator.'"'.$poid.'"'.$separator.'"'.$devise.'"'.$separator.'"'.$frais_de_port.'"'."\n";
}
?>