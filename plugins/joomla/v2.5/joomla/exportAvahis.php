<?php
header('Content-Type: text/html; charset=utf-8');
//header("Content-type: text/plain; charset=utf-8" );
//header("Content-Type: application/csv-tab-delimited-table");
//header("Content-disposition: filename=avahis.csv");

ini_set('display_errors', 0);
set_time_limit(0);

error_reporting(E_ALL ^ E_NOTICE);
define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname(__FILE__) );
//urlracine = (realpath(dirname(__FILE__)) . "".DS."..".DS."..".DS."..".DS);
//define('JPATH_BASE', $urlracine );


require_once (JPATH_BASE.DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

// configuration.php le cas ou le répertoire a été mit en écriture total 777.
// présent déja dans framework.php

//require_once JPATH_CONFIGURATION.'configuration.php'; 
require_once(JPATH_BASE.DS.'configuration.php' );
require_once(JPATH_LIBRARIES.DS.'loader.php' );
require_once (JPATH_LIBRARIES.DS.'joomla'.DS.'factory.php' );
//require_once (JPATH_BASE.DS.'includes'.DS.'database.php' );

//require_once(JPATH_LIBRARIES.DS.'joomla'.DS.'database'.DS.'table.php' );
//require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php') ;
//require_once(JPATH_BASE.DS.'components' . DS . 'com_virtuemart'.DS.'virtuemart.php');

$parents= array();

//database connection
$jconfig = new JConfig();
$db_error = "Erreur connexion BDD.";
$db_config = mysql_connect( $jconfig->host, $jconfig->user, $jconfig->password ) or die( $db_error );
mysql_select_db( $jconfig->db, $db_config ) or die( $db_error );

/*
* requéte pour virtuemart 1
* 
*/
$query1  = "SELECT p.*, c.category_name, c.category_id FROM ".$jconfig->dbprefix."vm_product as p ";
$query1  .= "LEFT JOIN ".$jconfig->dbprefix."vm_product_category_xref as pc ON p.product_id= pc.product_id ";
$query1  .= "LEFT JOIN ".$jconfig->dbprefix."vm_category as c ON pc.category_id= c.category_id ";
$query1  .= "WHERE product_publish='Y'";
$result1 = mysql_query($query1);
// echo 'request1: '.$query1.'<br>';


/*
* requéte pour virtuemart 2
*/
$query2  = "SELECT p.*,pp.*,pff.*,c.* FROM ".$jconfig->dbprefix."virtuemart_products as p ";
$query2  .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_products_fr_fr as pff ON pff.virtuemart_product_id = p.virtuemart_product_id ";
$query2  .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_product_prices as pp ON pp.virtuemart_product_id = p.virtuemart_product_id ";
$query2  .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_product_categories as pc ON pc.virtuemart_product_id= p.virtuemart_product_id ";
$query2  .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_categories_fr_fr as c ON c.virtuemart_category_id= pc.virtuemart_category_id ";
$query2  .= "WHERE p.published='1'GROUP BY p.virtuemart_product_id";

$result2 = mysql_query($query2);

/* Test de versions de virtuemart
*
* 
*/
if ($result2!=null) {// si la table virtuemart_products existe
	$versVm='2';	//alors virtuemart version 2 est installé
	afficherProduitVmVers2($result2);
}
else{//sinon on verifie bien que virtuemart v1 est installé
	if ($result1!=null) {// si vm_product exite
		$versVm='1';// 	alors virtuemar version 1 est installé
		afficherProduitVmVers1($result1);
	}
	else{// sinon pas de virtuemart installé
		echo 'Impossible d\'exécuter la requete : ' . mysql_error();//envoie de message d'erreur
		echo "\r Vous n'avez peut étre pas installer virtuemart";
		exit; // sortie du programme
	}
}
/**
 * Affiche les produits pour virtuemart version 1
 */
function afficherProduitVmVers1($result){
	global $parents ;	
	global $jconfig ;
	
	// nom du répertoire de joomla
	$string =$_SERVER['REQUEST_URI'];
	$url1[] = str_getcsv($string ,'/');
	$nomRepertoire= $url1['0']['1'];
	$versVm= '1';
	$separator = ';';
	$parents = initTableParent($versVm) ;
	
		$champTitre  = 	'ID_PRODUCT'.$separator.'REFERENCE_PRODUCT'.$separator.'CATEGORY'.$separator.'NAME_PRODUCT'.$separator.'DESCRIPTION'.$separator.
					'DESCRIPTION_SHORT'.$separator.'PRICE_PRODUCT'.$separator.'PRICE_TTC'.$separator.'PRICE_REDUCTION'.$separator.'REDUCTION_FROM'.$separator.'REDUCTION_TO'.$separator.
					'WEIGHT'.$separator.'WEIGHT_UNIT'.$separator.'LENGHT'.$separator.'WIDHT'.$separator.'HEIGHT'.$separator.'LWH_UNIT'.$separator.
					'META_DESCRIPTION'.$separator.'META_KEYWORD'.$separator.'META_TITLE'.$separator.'IMAGE_PRODUCT'.$separator.'THUMBNAIL'.$separator.
					'ETAT_PROMO'.$separator.'SPECIAL_PRODUCT'.$separator.'QUANTITY'.$separator.'AVAILABLE_PRODUCT'.$separator.'DEVISE'.$separator.'MANUFACTURER'.$separator.
					'PRODUCT_PRICE_PUBLISH_UP'.$separator.'PRODUCT_PRICE_PUBLISH_DOWN'.$separator.'PRICE_QUANTITY_START'.$separator.'PRICE_QUANTITY_END';
	
	// implémentation des attibuts
	// les contstantes :
	$champTitre  .= 	$separator.'ATTRIBUTE'.$separator.'CUSTOM_ATTRIBUTE';
	// Les dynaques :
	$sql_titre_attribute = "SELECT attribute_name FROM ".$jconfig->dbprefix."vm_product_attribute ";
	$sql_titre_attribute .= "GROUP BY attribute_name";
	//echo ".\r.$sql_titre_attribute.\r";
	$result_titre_attribute = mysql_query($sql_titre_attribute) ;
	$enregistrementAttribut = '';
	while ($row_titre_attribute = mysql_fetch_assoc($result_titre_attribute)){
		$titre = strtoupper(stripAccents(filtre_string($row_titre_attribute['attribute_name']))) ;
		$champTitre  .= $separator.$titre;
	}

	$champTitre .="\r\n";
	echo $champTitre ;
		
	while ($row = mysql_fetch_assoc($result)) 
	{
		//$COLOR = "test";
		$sql_tva = 'SELECT tax_rate FROM '.$jconfig->dbprefix.'vm_tax_rate WHERE tax_rate_id='.$row['product_tax_id'];
		$result_tva = mysql_query($sql_tva);
		while ($row_tva = mysql_fetch_assoc($result_tva)) 
		{
			$tva = $row_tva['tax_rate'];
		}
		$ID_PRODUCT = $row['product_id'];
		$REFERENCE_PRODUCT = utf8_encode($row['product_sku']);
		
		// category :
				
		if($row['product_parent_id']!=0){ //si ce produit a un parent 
			$CATEGORY = $parents[$row['product_parent_id']]['category'];
			$sql_product_categories  = 'SELECT category_id FROM '.$jconfig->dbprefix.'vm_product_category_xref ' ;
			$sql_product_categories .= "WHERE product_id = '".$row['product_parent_id']."'";
			//echo "\r\n".$sql_product_categories."\r\n";
			$result_product_categories = mysql_query($sql_product_categories);
			$row_product_categories = mysql_fetch_array($result_product_categories);
			$idCateg = $row_product_categories['category_id'];//on recherche l'id catégorie par rapport a l'id parent
			//var_dump($idCateg);
		}
		else{
			$CATEGORY = filtre_string($row['category_name']); 
			$idCateg = $row['category_id'] ;
		}
		
		$sql_arboCateg  = 'SELECT c.category_name, cx.*  FROM '.$jconfig->dbprefix.'vm_category as c ';
		$sql_arboCateg .= 'LEFT JOIN '.$jconfig->dbprefix.'vm_category_xref as cx ON cx.category_child_id = c.category_id ';
		$sql_arboCateg .= "WHERE c.category_id ='".$idCateg."'"; 
		//echo "\n$sql_arboCateg\n"; 
		$result_arboCateg = mysql_query($sql_arboCateg);
		$row_arboCateg = mysql_fetch_array($result_arboCateg);
		
		if ($row_arboCateg['category_parent_id']!=(0||null)){ // on recherche les catégories qui non pas de parent et non null
			while ($row_arboCateg['category_parent_id']!=0){ // tant que cette catégorie a un parent on continu
				$idCateg = 	$row_arboCateg['category_parent_id'] ;
				$sql_arboCateg  = 'SELECT c.category_name, cx.*  FROM '.$jconfig->dbprefix.'vm_category as c ';
				$sql_arboCateg .= 'LEFT JOIN '.$jconfig->dbprefix.'vm_category_xref as cx ON cx.category_child_id = c.category_id ';
				$sql_arboCateg .= "WHERE c.category_id ='".$idCateg."'"; 
				$result_category = mysql_query($sql_arboCateg);
				$row_category 	 = mysql_fetch_array($result_category);
							
				$CATEGORY = $row_category['category_name']." > ".$CATEGORY ;
						
				$sql_arboCateg  = 'SELECT c.category_name, cx.*  FROM '.$jconfig->dbprefix.'vm_category as c ';
				$sql_arboCateg .= 'LEFT JOIN '.$jconfig->dbprefix.'vm_category_xref as cx ON cx.category_child_id = c.category_id ';
				$sql_arboCateg .= "WHERE c.category_id ='".$idCateg."'"; 
				$result_arboCateg = mysql_query($sql_arboCateg);
				$row_arboCateg = mysql_fetch_array($result_arboCateg);
				$idCateg = $row_arboCateg['category_parent_id'] ;
			}
		}
		
		
		$NAME_PRODUCT = filtre_string($row['product_name']);
		$sql_prix  = 'SELECT product_price, product_currency FROM '.$jconfig->dbprefix.'vm_product_price '; 
		$sql_prix .= 'WHERE product_id='.$row['product_id'];
		$result_prix = mysql_query($sql_prix);
		$REDUCTION_TO = "";
		$REDUCTION_FROM ="";
		while ($row_prix = mysql_fetch_assoc($result_prix)) 
		{
			$prix = $row_prix['product_price']*(1+$tva);
			$PRICE_PRODUCT = $row_prix['product_price'];
			$PRICE_TTC = number_format($prix, 2);
			$DEVISE = $row_prix['product_currency'];
		}
		$today = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('y'));
		$sql_prix_discount = 'SELECT amount, is_percent, end_date, start_date FROM '.$jconfig->dbprefix.'vm_product_discount WHERE discount_id='.$row['product_discount_id'];
		$result_prix_discount = mysql_query($sql_prix_discount);
		$PRICE_REDUCTION = 0;
		while($row_prix_discount = mysql_fetch_assoc($result_prix_discount)) 
		{
			$montant = $row_prix_discount['amount'];
			$REDUCTION_TO = $row_prix_discount['end_date'];
			$REDUCTION_FROM = $row_prix_discount['start_date'];
			$is_percent = $row_prix_discount['is_percent'];
			if($REDUCTION_FROM < $today)
			{
				if($REDUCTION_TO > $today || $REDUCTION_TO==0)
				//if($REDUCTION_TO > $today)
				{
					if($is_percent == 1)
					{
						$PRICE_REDUCTION = ($PRICE_PRODUCT - ($PRICE_PRODUCT*$montant)/100);
						$PRICE_REDUCTION = $PRICE_REDUCTION *(1+$tva);
						$PRICE_REDUCTION = number_format($PRICE_REDUCTION, 2);
					}
					else
					{
						$PRICE_REDUCTION = ($PRICE_PRODUCT*(1+$tva))- $montant;
						$PRICE_REDUCTION = number_format($PRICE_REDUCTION, 2);
					}
				}
				else
				{
					$PRICE_REDUCTION ='';
				}
			}
			else
			{
				$PRICE_REDUCTION ='';
			}
		// echo "prix: ".$prix."\n";
		// echo "prixHT: ".$PRICE_PRODUCT."\n";
		// echo "today: ".$today."\n";
		// echo "start_date: ".$start_date."\n";
		// echo "end_date: ".$REDUCTION_TO."\n";
		// echo "prixDiscount: ".$PRICE_REDUCTION."\n";
		}
		$REDUCTION_TO = date('d/m/Y H:i', $REDUCTION_TO);
		$REDUCTION_FROM = date('d/m/Y H:i', $REDUCTION_FROM); 

		$SPECIAL_PRODUCT 	= $row['product_special']; 
		$AVAILABLE_PRODUCT = $row['product_availability'];
		$QUANTITY = $row['product_in_stock'];
		
		// si le produit a un parent
		if($row['product_parent_id']!=0){
		$DESCRIPTION = $parents[$row['product_parent_id']]['desc']; 
		}
		else{
			$DESCRIPTION = filtre_string($row['product_desc']);
		}
		
		$DESCRIPTION_SHORT= filtre_string($row['product_s_desc']);
		
		
		$IMAGE_PRODUCT ="";
		$THUMBNAIL="";
		if ($row['product_full_image'])
		$IMAGE_PRODUCT = 'http://'.$_SERVER['SERVER_NAME'].'/'.$nomRepertoire.'/components/com_virtuemart/shop_image/product/'.$row['product_full_image'];
		if ($row['product_thumb_image'])
		$THUMBNAIL = 'http://'.$_SERVER['SERVER_NAME'].'/'.$nomRepertoire.'/components/com_virtuemart/shop_image/product/'.$row['product_thumb_image'];
		
		$URL_PRODUCT = 'http://'.$_SERVER['SERVER_NAME'].'/'.$nomRepertoire.'/index.php?page=shop.product_details&product_id='.$id;
		
		// Poid et dimension :
		
		$WEIGHT 		= verifNumNC($row['product_weight']);
		$WEIGHT_UNIT	= verifStringNC($row['product_weight_uom']);
		$LENGHT			= verifNumNC($row['product_length']);
		$WIDHT 			= verifNumNC($row['product_width']);
		$HEIGHT 		= verifNumNC($row['product_height']);	
		$LWH_UNIT 		= verifStringNC($row['product_lwh_uom']);
		
		//date de publicaiton du prix du produit :
		$PRODUCT_PRICE_PUBLISH_UP 	= $row['product_price_vdate'];
		$PRODUCT_PRICE_PUBLISH_DOWN = $row['product_price_edate'];
		//$PRICE_QUANTITY_START 		= $row['price_quantity_start'];
		//$PRICE_QUANTITY_END 		= $row['price_quantity_end'];
		
		//attribut
		// statique
		$ATTRIBUTE = filtre_string($row['attribute']);
		$CUSTOM_ATTRIBUTE = filtre_string($row['custom_attribute']);
		//dynamique 
		$sql_table_attribute  = "SELECT * FROM ".$jconfig->dbprefix."vm_product_attribute ";	
		$sql_table_attribute .= "WHERE product_id='".$row['product_id']."'";
		$result_table_attribute = mysql_query($sql_table_attribute);
		while ($row_table_attribute = mysql_fetch_assoc($result_table_attribute)){
			$titre = strtoupper(stripAccents(filtre_string($row_table_attribute['attribute_name']))) ;
			//global $$titre ;
			$$titre = filtre_string($row_table_attribute['attribute_value']);
		}
		
		// manufacturer
		$sql_manufacturer  = "SELECT m.mf_name FROM  ".$jconfig->dbprefix."vm_manufacturer as m ";
		$sql_manufacturer .= "LEFT JOIN ".$jconfig->dbprefix."vm_product_mf_xref as mx ON m.manufacturer_id = mx.manufacturer_id ";
		$sql_manufacturer .= "WHERE mx.product_id='".$row['product_id']."'"; 
		$result_manufacturer = mysql_query($sql_manufacturer);
		//var_dump($sql_manufacturer);	
		while ($row_manufacturer = mysql_fetch_assoc($result_manufacturer)){
			$MANUFACTURER = filtre_string($row_manufacturer['mf_name']);
		}
		/*
		$sql_fdp = 'SELECT zone_cost FROM '.$jconfig->dbprefix.'vm_zone_shipping WHERE zone_name="Default"';
		$result_fdp = mysql_query($sql_fdp);
		while($row_fdp = mysql_fetch_assoc($result_fdp)) 
		{
			$frais_de_port = $row_fdp['zone_cost'];
		}
		*/
		$enregistrement = '"'.$ID_PRODUCT.'"'.$separator.'"'.$REFERENCE_PRODUCT.'"'.$separator.'"'.$CATEGORY.'"'.$separator.'"'.$NAME_PRODUCT.'"'.$separator.'"'.$DESCRIPTION.'"'.$separator.
			 '"'.$DESCRIPTION_SHORT.'"'.$separator.'"'.$PRICE_PRODUCT.'"'.$separator.'"'.$PRICE_TTC.'"'.$separator.'"'.$PRICE_REDUCTION.'"'.$separator.'"'.$REDUCTION_FROM.'"'.$separator.'"'.$REDUCTION_TO.'"'.$separator.
			 '"'.$WEIGHT.'"'.$separator.'"'.$WEIGHT_UNIT.'"'.$separator.'"'.$LENGHT.'"'.$separator.'"'.$WIDHT.'"'.$separator.'"'.$HEIGHT.'"'.$separator.'"'.$LWH_UNIT.'"'.$separator.
			 '"'.$META_DESCRIPTION.'"'.$separator.'"'.$META_KEYWORD.'"'.$separator.'"'.$META_TITLE.'"'.$separator.'"'.$IMAGE_PRODUCT.'"'.$separator.'"'.$THUMBNAIL.'"'.$separator.
			 '"'.$ETAT_PROMO.'"'.$separator.'"'.$SPECIAL_PRODUCT.'"'.$separator.'"'.$QUANTITY.'"'.$separator.'"'.$AVAILABLE_PRODUCT.'"'.$separator.'"'.$DEVISE.'"'.$separator.'"'.$MANUFACTURER.'"'.$separator.
			 '"'.$PRODUCT_PRICE_PUBLISH_UP.'"'.$separator.'"'.$PRODUCT_PRICE_PUBLISH_DOWN.'"'.$separator.'"'.$PRICE_QUANTITY_START.'"'.$separator.'"'.$PRICE_QUANTITY_END.'"' .$separator.
			 '"'.$ATTRIBUTE.'"'.$separator.'"'.$CUSTOM_ATTRIBUTE.'"';
	
		
		
		//attribut dynamique :
		$result_titre_attribute = mysql_query($sql_titre_attribute) ;
		//mysql_data_seek($result_titre_attribute); // ne fonctionne pas
		$enregistrementAttribut = '';
		while ($row_titre_attribute = mysql_fetch_assoc($result_titre_attribute)){
			$titre = strtoupper(stripAccents(filtre_string($row_titre_attribute['attribute_name']))) ;
			$enregistrementAttribut .= $separator.'"'.$$titre.'"';
		}
		$enregistrement .=$enregistrementAttribut;
		
		$enregistrement .="\r\n" ;
		if(!verifParents($row['product_id'])&&$CATEGORY)
		echo $enregistrement;
		$enregistrement="";
	}			
}	
/**
 * Affiche les produits pour virtuemart version 2
 */	
function afficherProduitVmVers2($result){
	global $parents ;	
	$jconfig = new JConfig();
	$db_error = "Erreur connexion BDD.";
	$db_config = mysql_connect( $jconfig->host, $jconfig->user, $jconfig->password ) or die( $db_error );
	mysql_select_db( $jconfig->db, $db_config ) or die( $db_error );		
	
	// nom du répertoire de joomla
	$string =$_SERVER['REQUEST_URI'];
	$url1[] = str_getcsv($string ,'/');
	$nomRepertoire= $url1['0']['1'];
	$versVm='2';
	$separator = ';';
	$parents = initTableParent($versVm) ;
	//var_dump($parents); 	
	//requéte pour les attributs
	$sql_nom_attribut  = "SELECT custom_title FROM ".$jconfig->dbprefix."virtuemart_customs ";
	$sql_nom_attribut .= "WHERE virtuemart_custom_id>'2' AND published='1'";
	$result_nom_attribut = mysql_query($sql_nom_attribut);
	//echo "\r\n$sql_nom_attribut\r\n";
	
	
		$champTitre  = 	'ID_PRODUCT'.$separator.'REFERENCE_PRODUCT'.$separator.'CATEGORY'.$separator.'NAME_PRODUCT'.$separator.'DESCRIPTION'.$separator.
					'DESCRIPTION_SHORT'.$separator.'PRICE_PRODUCT'.$separator.'PRICE_TTC'.$separator.'PRICE_REDUCTION'.$separator.'REDUCTION_FROM'.$separator.'REDUCTION_TO'.$separator.
					'WEIGHT'.$separator.'WEIGHT_UNIT'.$separator.'LENGHT'.$separator.'WIDHT'.$separator.'HEIGHT'.$separator.'LWH_UNIT'.$separator.
					'META_DESCRIPTION'.$separator.'META_KEYWORD'.$separator.'META_TITLE'.$separator.'IMAGE_PRODUCT'.$separator.'THUMBNAIL'.$separator.
					'ETAT_PROMO'.$separator.'SPECIAL_PRODUCT'.$separator.'QUANTITY'.$separator.'AVAILABLE_PRODUCT'.$separator.'DEVISE'.$separator.'MANUFACTURER'.$separator.
					'PRODUCT_PRICE_PUBLISH_UP'.$separator.'PRODUCT_PRICE_PUBLISH_DOWN'.$separator.'PRICE_QUANTITY_START'.$separator.'PRICE_QUANTITY_END'.$separator.
					'ATTRIBUTE'.$separator.'CUSTOM_ATTRIBUTE';
	
	// titre des attributs
	while($row_nom_attribut = mysql_fetch_array($result_nom_attribut)){
		$nomAttribut = strtoupper(stripAccents(filtre_string($row_nom_attribut['custom_title'])));	
		$champTitre .= $separator.$nomAttribut;
	}
	
	$champTitre .="\r\n";
	echo $champTitre ;
	
	while ($row = mysql_fetch_assoc($result)) 
	{
		/*$sql_tva = 'SELECT tax_rate FROM '.$jconfig->dbprefix.'virtuemart_tax_rate WHERE tax_rate_id='.$row['product_tax_id'];
		$result_tva = mysql_query($sql_tva);
		while ($row_tva = mysql_fetch_assoc($result_tva)) 
		{
			$tva = $row_tva['tax_rate'];
		}*/
		$ID_PRODUCT = $row['virtuemart_product_id'];
		$REFERENCE_PRODUCT = utf8_encode($row['product_sku']);
		
		// category :
		
		
		if($row['product_parent_id']!=0){ //si ce produit a un parent 
			$CATEGORY = $parents[$row['product_parent_id']]['category'];
			
			$sql_product_categories  = 'SELECT virtuemart_category_id FROM '.$jconfig->dbprefix.'virtuemart_product_categories ' ;
			$sql_product_categories .= "WHERE virtuemart_product_id = '".$row['product_parent_id']."'";
			$result_product_categories = mysql_query($sql_product_categories);
			$row_product_categories = mysql_fetch_array($result_product_categories);
			$idCateg = $row_product_categories['virtuemart_category_id'];//on recherche l'id catégorie par rapport a l'id parent
			//echo "\r\n".$sql_product_categories."\r\n";
			//var_dump($idCateg);
		}
		else{
		$CATEGORY = filtre_string($row['category_name']); // a revoir avec l'implémentation des catégorie racine
		$idCateg = $row['virtuemart_category_id'] ;
		}
		
		$nb = 0 ;
		$sql_arboCateg  = 'SELECT * FROM '.$jconfig->dbprefix.'virtuemart_category_categories ';
		$sql_arboCateg .= "WHERE id='".$idCateg."'"; 
		$result_arboCateg = mysql_query($sql_arboCateg);
		$row_arboCateg = mysql_fetch_array($result_arboCateg);
		//echo "\n$sql_arboCateg\n"; 
		
		
		//var_dump($row_arboCateg);
		
		if ($row_arboCateg['category_parent_id']!=0){ // si la catégorie est placé a la racine
			while ($row_arboCateg['category_parent_id']!=0){
				$sql_categories_fr_fr	 = 'SELECT category_name FROM '.$jconfig->dbprefix.'virtuemart_categories_fr_fr ';
				$sql_categories_fr_fr	 .= "WHERE virtuemart_category_id=".$row_arboCateg['category_parent_id'].""; 
				$result_categories_fr_fr = mysql_query($sql_categories_fr_fr);
				$row_categories_fr_fr 	 = mysql_fetch_array($result_categories_fr_fr);
							
				$CATEGORY = $row_categories_fr_fr[category_name]." > ".$CATEGORY ;
						
				$idCateg = $row_arboCateg['category_parent_id'] ;
				$sql_arboCateg  = 'SELECT * FROM '.$jconfig->dbprefix.'virtuemart_category_categories ';
				$sql_arboCateg .= "WHERE id='".$idCateg."'"; 
				$result_arboCateg = mysql_query($sql_arboCateg);
				$row_arboCateg = mysql_fetch_array($result_arboCateg);
				$nb++ ;
				if ($nb ==6)
				$row_arboCateg['category_parent_id'] = 0;
			}
		}
		
		
		
		
		$NAME_PRODUCT = utf8_encode(stripslashes($row['product_name']));
		/*
		$sql_prix = 'SELECT product_price, product_currency FROM '.$jconfig->dbprefix.'virtuemart_product_prices WHERE virtuemart_product_id='.$row['virtuemart_product_id'];
		$result_prix = mysql_query($sql_prix);
		while ($row_prix = mysql_fetch_assoc($result_prix)) 
		{
			$prix = $row_prix['product_price']*(1+$tva);
			$prixHT = $row_prix['product_price'];
			$prix = number_format($prix, 2);
			$devise = $row_prix['product_currency'];
		}
		*/
		$PRICE_PRODUCT = $row['product_price'];
		$PRICE_PRODUCT = number_format($PRICE_PRODUCT, 2);
		/*
		$today = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('y'));
		$sql_prix_discount = 'SELECT amount, is_percent, end_date, start_date FROM '.$jconfig->dbprefix.'virtuemart_product_discount WHERE discount_id='.$row['product_discount_id'];
		$result_prix_discount = mysql_query($sql_prix_discount);
		$prixDiscount = 0;
		while($row_prix_discount = mysql_fetch_assoc($result_prix_discount)) 
		{
			$montant = $row_prix_discount['amount'];
			$REDUCTION_TO = $row_prix_discount['end_date'];
			$start_date = $row_prix_discount['start_date'];
			$is_percent = $row_prix_discount['is_percent'];
			if($start_date < $today)
			{
				if($REDUCTION_TO > $today || $REDUCTION_TO==0)
				//if($REDUCTION_TO > $today)
				{
					if($is_percent == 1)
					{
						$prixDiscount = ($prixHT - ($prixHT*$montant)/100);
						$prixDiscount = $prixDiscount *(1+$tva);
						$prixDiscount = number_format($prixDiscount, 2);
					}
					else
					{
						$prixDiscount = ($prixHT*(1+$tva))- $montant;
						$prixDiscount = number_format($prixDiscount, 2);
					}
				}
				else
				{
					$prixDiscount ='';
				}
			}
			else
			{
				$prixDiscount ='';
			}
		// echo "prix: ".$prix."\n";
		// echo "prixHT: ".$prixHT."\n";
		// echo "today: ".$today."\n";
		// echo "start_date: ".$start_date."\n";
		// echo "end_date: ".$REDUCTION_TO."\n";
		// echo "prixDiscount: ".$prixDiscount."\n";
		}
		*/
		$prixDiscount = $row['product_override_price'];
		$PRICE_REDUCTION = number_format($prixDiscount, 2);
		$AVAILABLE_PRODUCT = str_replace(array("\r\n","\n","\r",".gif"),'', $row['product_availability']);
		$QUANTITY = $row['product_in_stock'];
		
		$DESCRIPTION = filtre_string($row['product_desc']);
		$DESCRIPTION_SHORT= filtre_string($row['product_s_desc']);
		
		
		$sqlMedia ='SELECT m.* FROM '.$jconfig->dbprefix.'virtuemart_product_medias as pm '; 
		$sqlMedia .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_medias as m ON m.virtuemart_media_id = pm.virtuemart_media_id ";
		$sqlMedia .='WHERE pm.virtuemart_product_id='.$row['virtuemart_product_id'];
		//echo"\n$sqlMedia\n";
		
		$result_media = mysql_query($sqlMedia);
		$IMAGE_PRODUCT ="";
		$THUMBNAIL="";
		while ($row_media = mysql_fetch_assoc($result_media)) 
		{
			if ($row_media['file_url'])	
			$IMAGE_PRODUCT = 'http://'.$_SERVER['SERVER_NAME'].'/'.$nomRepertoire.'/'.$row_media['file_url'];
			if($row_media['file_url_thumb'])
			$THUMBNAIL = 'http://'.$_SERVER['SERVER_NAME'].'/'.$nomRepertoire.'/'.$row_media['file_url_thumb'];
		}
		//$url = 'http://'.$_SERVER['SERVER_NAME'].'/index.php?page=shop.product_details&product_id='.$id;
		
		
		// Poid et dimension :
		
		$WEIGHT 		= verifNumNC($row['product_weight']);
		$WEIGHT_UNIT	= verifStringNC($row['product_weight_uom']);
		$LENGHT			= verifNumNC($row['product_length']);
		$WIDHT 			= verifNumNC($row['product_width']);
		$HEIGHT 		= verifNumNC($row['product_height']);	
		$LWH_UNIT 		= verifStringNC($row['product_lwh_uom']);
		
		// META Avoir probléme avec le produit id = 15 les données meta son nul alors qu'il ne le dervait pas
		//if($row['virtuemart_product_id']=='15')var_dump($row);
		$META_DESCRIPTION 	= filtre_string($row['metadesc']);
		$META_KEYWORD 		= filtre_string($row['metakey']);//str_replace(array("\r\n","\n","\r"),'',$row['metakey']);
		$META_TITLE 		= filtre_string($row['customtitle']);
		
		$ETAT_PROMO 		= $row['override']; 
		$SPECIAL_PRODUCT 	= $row['product_special']; 
		
		//fabricant-fournisseur :
		$sql_manufacturer 	=  'SELECT m.mf_name FROM '.$jconfig->dbprefix.'virtuemart_manufacturers_fr_fr as m ';
		$sql_manufacturer 	.= 'INNER JOIN '.$jconfig->dbprefix.'virtuemart_product_manufacturers as pm ON pm.virtuemart_manufacturer_id= m.virtuemart_manufacturer_id ';
		$sql_manufacturer 	.= "WHERE pm.virtuemart_product_id=".$row['virtuemart_product_id'];
		$result_manufacturer = mysql_query($sql_manufacturer);
		$MANUFACTURER = "" ;
		while($row_manufacturer = mysql_fetch_assoc($result_manufacturer)) {
			$MANUFACTURER = $row_manufacturer['mf_name'];
		}	
		
		//devise :
		$sl_devise_vendor =  'SELECT c.currency_name FROM '.$jconfig->dbprefix.'virtuemart_currencies as c ';
		$sl_devise_vendor .= 'INNER JOIN '.$jconfig->dbprefix.'virtuemart_vendors as v ON c.virtuemart_currency_id = v.vendor_currency ' ;
		$sl_devise_vendor .= 'INNER JOIN '.$jconfig->dbprefix.'virtuemart_products as p ON p.virtuemart_vendor_id = v.virtuemart_vendor_id ' ;
		$sl_devise_vendor .= "WHERE p.virtuemart_product_id=".$row['virtuemart_product_id']; 
		$result_devise_vendor = mysql_query($sl_devise_vendor);
		//echo"\n$sl_devise_vendor\n";	
		$DEVISE="";
		while($row_devise_vendor = mysql_fetch_assoc($result_devise_vendor)) {
			if ($row_devise_vendor['currency_name']){//si le vendeur as définit une devise par défaut	
				$DEVISE= $row_devise_vendor['currency_name'];		
			}
			else{
				$sl_devise_product =  'SELECT c.currency_name FROM '.$jconfig->dbprefix.'virtuemart_currencies as c ';
				$sl_devise_product .= 'INNER JOIN '.$jconfig->dbprefix.'virtuemart_product_prices as pp ON pp.product_currency = c.virtuemart_currency_id ' ;
				$sl_devise_product .= "WHERE pp.virtuemart_product_id=".$row['virtuemart_product_id']; 
				$result_devise_devise_product = mysql_query($sl_devise_product);
				while($row_devise_product = mysql_fetch_assoc($result_devise_devise_product)) {
					$DEVISE= $row_devise_product['currency_name'];
				}
			}
		}
		
		//date de publicaiton du prix du produit :
		$PRODUCT_PRICE_PUBLISH_UP 	= $row['product_price_publish_up'];
		$PRODUCT_PRICE_PUBLISH_DOWN = $row['product_price_publish_down'];
		$PRICE_QUANTITY_START 		= $row['price_quantity_start'];
		$PRICE_QUANTITY_END 		= $row['price_quantity_end'];
		
		/*
		$sql_fdp = 'SELECT zone_cost FROM '.$jconfig->dbprefix.'virtuemart_zone_shipping WHERE zone_name="Default"';
		$result_fdp = mysql_query($sql_fdp);
		while($row_fdp = mysql_fetch_assoc($result_fdp)) 
		{
			$frais_de_port = $row_fdp['zone_cost'];
		}
		 
		*/
		

	
		
		$enregistrement = '"'.$ID_PRODUCT.'"'.$separator.'"'.$REFERENCE_PRODUCT.'"'.$separator.'"'.$CATEGORY.'"'.$separator.'"'.$NAME_PRODUCT.'"'.$separator.'"'.$DESCRIPTION.'"'.$separator.
			 '"'.$DESCRIPTION_SHORT.'"'.$separator.'"'.$PRICE_PRODUCT.'"'.$separator.'"'.$PRICE_TTC.'"'.$separator.'"'.$PRICE_REDUCTION.'"'.$separator.'"'.$REDUCTION_FROM.'"'.$separator.'"'.$REDUCTION_TO.'"'.$separator.
			 '"'.$WEIGHT.'"'.$separator.'"'.$WEIGHT_UNIT.'"'.$separator.'"'.$LENGHT.'"'.$separator.'"'.$WIDHT.'"'.$separator.'"'.$HEIGHT.'"'.$separator.'"'.$LWH_UNIT.'"'.$separator.
			 '"'.$META_DESCRIPTION.'"'.$separator.'"'.$META_KEYWORD.'"'.$separator.'"'.$META_TITLE.'"'.$separator.'"'.$IMAGE_PRODUCT.'"'.$separator.'"'.$THUMBNAIL.'"'.$separator.
			 '"'.$ETAT_PROMO.'"'.$separator.'"'.$SPECIAL_PRODUCT.'"'.$separator.'"'.$QUANTITY.'"'.$separator.'"'.$AVAILABLE_PRODUCT.'"'.$separator.'"'.$DEVISE.'"'.$separator.'"'.$MANUFACTURER.'"'.$separator.
			 '"'.$PRODUCT_PRICE_PUBLISH_UP.'"'.$separator.'"'.$PRODUCT_PRICE_PUBLISH_DOWN.'"'.$separator.'"'.$PRICE_QUANTITY_START.'"'.$separator.'"'.$PRICE_QUANTITY_END.'"'.$separator.
			 '"'.$ATTRIBUTE.'"'.$separator.'"'.$CUSTOM_ATTRIBUTE.'"';
	
		// Attribut
		$sql_nom_attribut  = "SELECT virtuemart_custom_id FROM ".$jconfig->dbprefix."virtuemart_customs ";
		$sql_nom_attribut .= "WHERE virtuemart_custom_id>'2' AND published='1'";
		$result_nom_attribut = mysql_query($sql_nom_attribut);	
		while($row_nom_attribut = mysql_fetch_array($result_nom_attribut)){
			//$nomAttribut = strtoupper(stripAccents(filtre_string($row_nom_attribut['custom_title'])));	
			//$champTitre .= $separator.$nomAttribut;
			$sql_product_customfields  = 'SELECT * FROM '.$jconfig->dbprefix.'virtuemart_product_customfields ';
			$sql_product_customfields .= "WHERE virtuemart_product_id='".$row['virtuemart_product_id']."' AND virtuemart_custom_id='".$row_nom_attribut['virtuemart_custom_id']."'"; 
			$result_product_customfields = mysql_query($sql_product_customfields);
			$attribut= "";
			$product_id="";
			//var_dump($row_nom_attribut);
			while($row_product_customfields = mysql_fetch_array($result_product_customfields)){
				if ($product_id!=$row_product_customfields['virtuemart_product_id']){ // pour la premiére itération le produit n'est pas la même
					$attribut= filtre_string($row_product_customfields['custom_value']);	
				}
				else{ // si pour les autres itération le produit est le même on concaténe les valeur avec un " > "entre 
					$attribut.= " > ".filtre_string($row_product_customfields['custom_value']);
				}
				
				$product_id = $row_product_customfields['virtuemart_product_id'];
			
			}
			$enregistrement =	$enregistrement.$separator.'"'.$attribut.'"' ;
			
		}
		$enregistrement .="\r\n" ;
		if (($CATEGORY&&!$row['product_parent_id'])) // si ce produit a une catégory, hérité du parent ou pas, et si il n'est pas parent alors
		echo $enregistrement;
		$enregistrement="";
		
	}
}
/** 
	 * \ initialise une tableParent contenant tous les parent leur id et leur description
	 * @return array $tableParent
	 * */ 
	function initTableParent($versVm){
		global $jconfig ;
		//$jconfig = new JConfig();
		//$db_error = "Erreur connexion BDD.";
		//$db_config = mysql_connect( $jconfig->host, $jconfig->user, $jconfig->password ) or die( $db_error );
		//mysql_select_db( $jconfig->db, $db_config ) or die( $db_error );	
	
	
		// recherche des parents 
		$sql_parents = afficherSqlParents($versVm);
		$result_parents = mysql_query($sql_parents);
		while ($row_parents = mysql_fetch_assoc($result_parents)) {
			$sql_desc_parents = afficherSqlDescParents($versVm, $row_parents['product_parent_id']);
			//var_dump($sql_desc_parents );
			$result_desc_parents = mysql_query($sql_desc_parents);
			while ($row_desc_parents = mysql_fetch_assoc($result_desc_parents)){
				$tableParent[$row_parents['product_parent_id']]= array('desc' => filtre_string($row_desc_parents['product_desc']),
																		'category' => filtre_string($row_desc_parents['category_name']));
				//var_dump($row_parents['product_parent_id']);
			}
		}
		return $tableParent ;
	}
/**
 * Selon la version de VM retourne la requéte de recherche d'un tableau de produit parent
 * @param string $versVm la version de virtuemart
 * @return string $sql_parents la requéte sql attendut
 */
 
 function afficherSqlParents($versVm){
	global $jconfig ;
	if($versVm == "1"){
		$sql_parents  = "SELECT product_parent_id FROM ".$jconfig->dbprefix."vm_product ";
		$sql_parents .= "GROUP BY product_parent_id";
		return $sql_parents ;	
	}
	elseif($versVm =="2"){
		$sql_parents  = "SELECT product_parent_id FROM ".$jconfig->dbprefix."virtuemart_products ";
		$sql_parents .= "GROUP BY product_parent_id";	
		return $sql_parents ;
	}
	else{
		return "Cette version n'est pas encore prise en charge";
	}
 }
 
 /**
  * Selon la vers de VM retoune la requéte de recherche de la description et la catégory d'un produit parent donné
  *  @param string $versVm la version de virtuemart
  *  @param string $product_parent_id l'id du produit parent
  * @return string $sql_parents la requéte sql attendut
  */
	function afficherSqlDescParents($versVm,$product_parent_id){
		//var_dump($versVm);
	  	global $jconfig ;	
	  	if($versVm  == '1'){
	  		$sql_desc_parents  = "SELECT p.product_desc, c.category_name FROM ".$jconfig->dbprefix."vm_product as p ";
			$sql_desc_parents .= "LEFT JOIN ".$jconfig->dbprefix."vm_product_category_xref as pc ON p.product_id= pc.product_id ";
			$sql_desc_parents .= "LEFT JOIN ".$jconfig->dbprefix."vm_category as c ON pc.category_id= c.category_id ";
			$sql_desc_parents .= "WHERE p.product_id ='".$product_parent_id."'";
			return $sql_desc_parents ;
		}
		elseif($versVm == '2'){
			$sql_desc_parents  = "SELECT p.product_desc, c.category_name FROM ".$jconfig->dbprefix."virtuemart_products_fr_fr as p ";
			$sql_desc_parents .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_product_categories as pc ON p.virtuemart_product_id= pc.virtuemart_product_id ";
			$sql_desc_parents .= "LEFT JOIN ".$jconfig->dbprefix."virtuemart_categories_fr_fr as c ON pc.virtuemart_category_id= c.virtuemart_category_id ";
			$sql_desc_parents .= "WHERE p.virtuemart_product_id ='".$product_parent_id."'";		
			return $sql_desc_parents ;
		}
		else{
			return "Cette version n'est pas encore prise en charge";
		}		
	}
/** 
 * vérifie si ce produit est un parent
 * @param string $idProduit l'id produit a vérifié
 * @return boolean retourne true si il est parent
 * */
function verifParents($idProduit){
	global $parents ;
	//var_dump($parents);
	foreach($parents as $x=>$x_value)
	{
		if ($idProduit==$x){
			$isbnLivreTrouver=$x ;
			return TRUE ;
		}
				
	}
	//echo "<BR>corespondance isbn ko" ;
	return FALSE ;
}
/**
 * Enléve les caractéres \n\r les balises et encode en UTF8
 * @param string a filtré
 * @return string filtré
 */
 function filtre_string($string){
	$stringFiltrer=	str_replace(array("\r\n","\n","\r"),'', $string);
	$stringFiltrer=	str_replace(array(";"),'>', $stringFiltrer);
	$stringFiltrer = utf8_encode(str_replace(array("\r\n","\n","\r"),'', strip_tags($stringFiltrer)));
	return $stringFiltrer;
	
}
/**
 * Enléve les accents
 * @param string avant filtrage des accents
 * @return string sans accents
 */
function stripAccents($texte) {
		$texte = str_replace(
			array(
				'à', 'â', 'ä', 'á', 'ã', 'å',
				'î', 'ï', 'ì', 'í', 
				'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
				'ù', 'û', 'ü', 'ú', 
				'é', 'è', 'ê', 'ë', 
				'ç', 'ÿ', 'ñ',
				'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
				'Î', 'Ï', 'Ì', 'Í', 
				'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
				'Ù', 'Û', 'Ü', 'Ú', 
				'É', 'È', 'Ê', 'Ë', 
				'Ç', 'Ÿ', 'Ñ' 
			),
			array(
				'a', 'a', 'a', 'a', 'a', 'a', 
				'i', 'i', 'i', 'i', 
				'o', 'o', 'o', 'o', 'o', 'o', 
				'u', 'u', 'u', 'u', 
				'e', 'e', 'e', 'e', 
				'c', 'y', 'n', 
				'A', 'A', 'A', 'A', 'A', 'A', 
				'I', 'I', 'I', 'I', 
				'O', 'O', 'O', 'O', 'O', 'O', 
				'U', 'U', 'U', 'U', 
				'E', 'E', 'E', 'E', 
				'C', 'Y', 'N' 
			),$texte);
		return $texte;
	}
/**
 * Vérifie si la valeur test est null
 * @param String $valTest 
 * @return String retourne sa valeur ou nc si elle est null
 */
function verifStringNC($valTest){
		
	if($valTest!=null){
		return $valTest;
	}
	else{
		return "nc";
	}
	
}
/**
 * Vérifie si la valeur test est null
 * @param String $valTest 
 * @return String retourne sa valeur ou nc si elle est null
 */
function verifNumNC($valTest){
		
	if($valTest>0){
		return $valTest;
	}
	else{
		return "nc";
	}
	
}
?>