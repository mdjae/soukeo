<?php
include "lib/config.php";
$page="univers_1"

?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content">
	<div class="contenu">
	
	<div class="row-fluid">
	
		<!-- Begin Menu Secondaire -->
		<div class="span3">
			<div class="bloc secondary">
				<header><span><i class="fa fa-home fa-fw fa-lg"></i></span><span class="title">Mode et vêtements</span> <span class="right hidden-desktop reduct-list"><i class="fa fa-2x fa-list"></i></span></header>
					<ul>
						<li class="active"><a href="#">Femmes</a></li>
						<li><a href="#">hommes</a></li>
						<li><a href="#">Filles</a></li>
						<li><a href="#">Garçon</a></li>
						<li><a href="#">hommes</a></li>
						<li><a href="#">Filles</a></li>
						<li><a href="#">Garçon</a></li>
						<li><a href="#">hommes</a></li>
						<li><a href="#">Filles</a></li>
						<li><a href="#">Garçon</a></li>
					</ul>
			</div>
		</div>
		<!-- Fin Menu Secondaire -->
		
		<div class="span9 well-right">
		
			<!-- Begin Bloc soldes -->
			<div class="row-fluid sales visible-desktop">
				<div class="span9"><img src="img/soldes.jpg" alt="soldes" width="100%" height="auto"></div>
				<div class="span3" style="margin-top: 60px;">
					<p class="strong">Voir toutes les soldes</p>
					<ul>
						<li>> <a href="#" class="underline xsmall">soldes Femmes</a></li>
						<li>> <a href="#" class="underline xsmall">soldes Hommes</a></li>
						<li>> <a href="#" class="underline xsmall">soldes Bébé (-3 ans)</a></li>
						<li>> <a href="#" class="underline xsmall">soldes Enfants</a></li>
					</ul>
				</div>
			</div>
			<!-- Fin Bloc soldes -->
			
			<!-- Begin Bloc microboutique -->
			<div class="row-fluid">
				<div class="bloc bloc-grid">
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>					

					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>						
					
				</div>
			</div>
			<!-- Fin Bloc microboutique -->
			
		</div>
	</div>
	
			<!-- Begin #addtionnal -->
		<div class="row-fluid" id="additional">
		<div class="well">
			<div class="span4"><a href="#"><img src="img/promo1.jpg"><div class="badge bg-orange badge-bottom"><span>43 €</span><span>A PARTIR<br>DE</span></div></a></div>
			<div class="span4"><a href="#"><img src="img/promo2.jpg"><div class="badge"><span>143 €</span><span>A PARTIR<br>DE</span></div></a></div>
			<div class="span4"><a href="#"><img src="img/promo3.jpg"><div class="badge bg-green badge-right"><span>943 €</span><span>A PARTIR<br>DE</span></div></a></div>
		</div>
		</div>
		<!-- End #additionnal -->
	
	
	<!-- Begin bloc selection -->
	<div class="row-fluid selection visible-desktop">
		<div class="span12">
		<div class="bloc bloc-grid border-top border-bottom bloc-width-title">
			<div class="content-bloc" id="slider-our-selection">
				<div class="pag"></div>
			<div class="row-fluid slides">
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">1. Le nom du jouet détailllé sur 2 lignes, Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">2. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/vin.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">3. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/vin2.jpg"><span class="tag bg-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">4. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">5. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">6. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">7. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">8. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">9. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">10. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
						<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">11. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">12. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">13. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">14. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				
				
				</div>
			</div>
						<script type="text/javascript">
$(document).ready(function() {		
		 $('#slider-our-selection .slides').carouFredSel({
	 items		  : {
		visible	: 5,
		minimun	: 6
	  },
	  pagination : {
	  container : "#slider-our-selection .pag",
	  anchorBuilder : true
	  },
	  height: 220,
	  direction		: "left",
	  responsive : true,
        scroll : {
            items           : 5,
            duration        : 1000,                         
            pauseOnHover    : true,
			fx 				: "slide",
			direction		: "left"
        }                   
    });	

});
</script>
			
		</div>
		</div>
	</div>
	<!-- Fin bloc selection -->
	
	<div class="well">
		<div class="row-fluid">
					
			<div class="span4">
			<!-- Begin blog -->
				<div class="bloc-list bloc border">
					<div class="top-bar bg-dark text-center"><span class="xlarge uppercase white">En direct du <span class="bold white">blog</span></span></div>
					<div class="offered">
						<img src="http://silicon-village.fr/images/remi.png" class="circle right">

						<div>
							<p class="bold"><span class="large">Rémi</span>, responsable de l'univers</p>
							<p><a href="#" class="large gray bold">Livres</a>, <a href="#" class="large gray bold">DVD</a>, <a href="#" class="large gray bold">Blue-Ray</a></p>
							<p><span class="small bold">Passionné de Cinéma philosophie, BD et arts graphiques.</span></p>
						</div>
						
					</div>
					<ul>
						<li><a href="#"><img src="img/blog2.jpg" class="border" ></a><p><a class="bold" href="#">Saint-Valentin 2014: les tendances pour elle et lui !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">14/03</span></p></li>
						<li><a href="#"><img src="img/blog1.jpg" class="border" ></a><p><a class="bold" href="#">C’est l’été ! Collection plage et maillots de bain 2014 - test sur 3 grandes lignes</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">14/03</span></p></li>
						<li><a href="#"><img src="img/blog1.jpg" class="border" ></a><p><a class="bold" href="#">Les meilleurs ateliers-créateurs de l’île de la Réunion !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">13/03</span></p></li>
						<li><a href="#"><img src="img/blog2.jpg" class="border" ></a><p><a class="bold" href="#">Saint-Valentin 2014: les tendances pour elle et lui !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">10/03</span></p></li>
						
					</ul>
					<div class="bottom-bar bg-white text-center border-top"><a class="btn btn-small" href="<?php echo $url; ?>">voir plus d'article <span class="large">Livres, DVD et Blue-Ray</span></a></div>
		
			</div>
		<!-- Fin blog -->
		
		</div>
		
			
			
		<div class="span8">
			
		<div class="bloc bloc-grid topics">
			<div class="row-fluid border-bottom">			
			<div class="box span3">
				<a href="#" class="large title">Chemisiers et blouses</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Chemisiers et blouses</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Jeans</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Jeans</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Jupes</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Jupes</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Lingeries</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Lingeries</a></p>
			</div>			
			</div>
			
			<div class="row-fluid border-bottom">			
			<div class="box span3">
				<a href="#" class="large title">Chemisiers et blouses</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Chemisiers et blouses</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Jeans</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Jeans</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Jupes</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Jupes</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Lingeries</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Lingeries</a></p>
			</div>			
			</div>
			
			
			<div class="row-fluid">			
			<div class="box span3">
				<a href="#" class="large title">Chemisiers et blouses</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Chemisiers et blouses</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Jeans</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Jeans</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Jupes</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Jupes</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Lingeries</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="small bold underline white">Tous Lingeries</a></p>
			</div>			
			</div>
			
			

			</div>
			</div>
		
		
		
		
		</div>
		
		</div>
		
		
		
		<!-- Begin bloc topicsLarge -->
		<div class="row-fluid topicsLarge">
		<div class="well">
		<div class="span3 box">
			<div class="top-bar border-bottom bg-white"><span class="large">Vêtements femmes</span></div>
			<div class="content-bloc">
				<div class="right"><img src="img/product/robe.png"></div>
				<ul>
					<li><a href="">Grandes tailles</a></li>
					<li><a href="">T-shirt & tops</a></li>
					<li><a href="">Jeans</a></li>
					<li><a href="">Shorts</a></li>
					<li><a href="">Vêtements de place</a></li>
				</ul>
				<a href="">> voir tout</a> 
			</div>
		</div>
				<div class="span3 box">
			<div class="top-bar border-bottom bg-white"><span class="large">Vêtements hommes</span></div>
			<div class="content-bloc">
				<div class="right"><img src="img/product/pull.png"></div>
				<ul>
					<li><a href="">Grandes tailles</a></li>
					<li><a href="">T-shirt & tops</a></li>
					<li><a href="">Jeans</a></li>
					<li><a href="">Shorts</a></li>
					<li><a href="">Vêtements de place</a></li>
				</ul>
				<a href="">> voir tout</a> 
			</div>
		</div>
				<div class="span3 box">
			<div class="top-bar border-bottom bg-white"><span class="large">Vêtements femmes</span></div>
			<div class="content-bloc">
				<div class="right"><img src="img/product/robe.png"></div>
				<ul>
					<li><a href="">Grandes tailles</a></li>
					<li><a href="">T-shirt & tops</a></li>
					<li><a href="">Jeans</a></li>
					<li><a href="">Shorts</a></li>
					<li><a href="">Vêtements de place</a></li>
				</ul>
				<a href="">> voir tout</a> 
			</div>
		</div>
				<div class="span3 box">
			<div class="top-bar border-bottom bg-white"><span class="large">Vêtements hommes</span></div>
			<div class="content-bloc">
				<div class="right"><img src="img/product/pull.png"></div>
				<ul>
					<li><a href="">Grandes tailles</a></li>
					<li><a href="">T-shirt & tops</a></li>
					<li><a href="">Jeans</a></li>
					<li><a href="">Shorts</a></li>
					<li><a href="">Vêtements de place</a></li>
				</ul>
				<a href="">> voir tout</a> 
			</div>
		</div>
		
		
		</div>
		</div>
		<!-- End bloc topicsLarge -->
		
		
		
		<div class="well">
			<div class="row-fluid">
		
						<!-- Begin bloc top vente -->
			<div class="span4">
				<div class="bloc-list bloc border">
					<div class="top-bar"><span class="uppercase"><span class="bold">Le top</span> des ventes</span></div>
					<ul>
						<li><a href="#"><img src="img/product/product1.jpg"></a><p><span class="xlarge bold">1. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/product1.jpg"></a><p><span class="xlarge bold">2. </span><a href="#" class="bold blue">Le nom du jouet détaillé</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/vin.jpg"></a><p><span class="xlarge bold">3. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/vin2.jpg"></a><p><span class="xlarge bold">4. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les </a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/product1.jpg"></a><p><span class="xlarge bold">5. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						
					</ul>
		
		</div>
		
		</div>
		<!-- Fin bloc top vente -->
		
		<div class="span8">
		
			<!-- Begin bloc marque -->
		<div class="row-fluid">
				<div class="top-bar text-center bg-white"><span class="xlarge uppercase dark">Toutes <span class="bold dark">les marques</span></span></div>
				<div class="bloc-grid bloc brandLarge visible-desktop border">
					<div class="content-bloc">
					<ul>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/nike.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hm.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hugoboss.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/jules.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/nike.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hm.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hugoboss.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/jules.jpg"></a></li>						
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						</ul>
					</div>
			</div>
			</div>
		<!-- Fin bloc marque -->
		
		
		
		
<!-- Begin serviceBar -->
				<div class="serviceBar bloc border">
					<div class="top-bar top-bar-center bg-green"><span class="bold white uppercase">Payez en 3 fois à partir de 100 € d’achats</span><a href="#" class="underline xsmall white">> voir toutes les conditions</a></div>
					<div class="row-fluid no-margin">
						<div>
						<p class="uppercase bold small">Paiement sécurisé</p>
							<p><i class="fa fa-lock fa-4x"></i></p>
							<div>
							
							<p>A partir de 48h en express, du commerçant à votre domicile</p>
							<a href="#" class="xsmall underline bold">En savoir plus</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">Paiement en 3 fois</p>
							<p><i class="fa fa-credit-card fa-3x"></i></p>
							<div>							
							<p>Service à partir de 100 € d’achat</p>
							<a href="#" class="xsmall underline bold">voir le détail des conditions</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">livraison facile et rapide</p>
							<p><i class="fa fa-truck fa-3x"></i></p>
							<div>
							
							<p>A partir de 48h en express, du commerçant à votre domicile</p>
							<a href="#" class="xsmall underline bold">En savoir plus</a>
							</div>
						</div>
					</div>
					<div class="bottom-bar bottom-bar-center"><span class="small">Contactez notre service client au</span> <span class="large bold">0693 911 859</span><span class="small"> du lundi à vendredi de 9h à 18h.</span></div>
		</div>
	<!-- Fin serviceBar -->	
		
		</div>
		
		</div>
		</div>
		
		
		
		
		<!-- Begin bloc promotion -->
	<div class="row-fluid bloc bloc-grid promotion">
		<div class="span12 content-bloc">
			<div class="top-bar "><span class="uppercase large">chutes de prix ! <span class="bold">toutes nos promotions</span></span> <a href="" class="underline">voir plus de promos Mode et vêtements</a></div>
			<div class="row-fluid">
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin2.jpg"><span class="tag bg-green">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">949.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue dark">- 15 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Fin bloc promotion -->	
		
		
	
	
	
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>	