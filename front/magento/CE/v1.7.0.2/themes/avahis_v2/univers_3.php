<?php
include "lib/config.php";
$page = "univers_3";

?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content">
	<div class="contenu">
	
	
	<div class="row-fluid">
	
	<!-- Begin Menu Secondaire -->
		<div class="span3">
			<div class="bloc secondary">
				<header><span class="xsmall"><i class="fa fa-chevron-left fa-fw"></i></span> <a href="#"><span class="title">Photo et caméscopes <br><span class="xsmall">retour</span></span></a> <span class="right hidden-desktop reduct-list"><i class="fa fa-2x fa-list"></i></span></header>
					<ul>
						<li class="niv-sup selected"><a href="#">Appareil photo numérique</a>
							<ul>
								<li><a href="#">Compact et bridges (49)</a></li>
								<li><a href="#">Reflex numériques (4)</a></li>
								<li><a href="#">Flash(152)</a></li>
							</ul>
						</li>
						<li class="niv-sup"><a href="#">Accessoires</a></li>
						<li class="niv-sup"><a href="#">Caméras de surveillance</a></li>
						<li class="niv-sup"><a href="#">Caméscopes</a></li>
						<li class="niv-sup"><a href="#">Flash</a></li>
						<li class="niv-sup"><a href="#">Jumelles, téléscopes et optique</a></li>
						<li class="niv-sup"><a href="#">Objectifs</a></li>

					</ul>
			</div>
			<!-- End Menu Secondaire -->
			
			<!-- Begin Menu liveBlog -->
			<div class="bloc border liveBlog">
				<div class="top-bar text-center"><span class="orange uppercase bold">En direct du blog</span></div>
				<div class="content-bloc bg-light-gray">
					<ul class="border-bottom">
						<li><a href="">Notre sélection d'appareils photo numériques</a></li>
						<li><a href="">Tout l'équipement du photographe semi-professionnel</a></li>
					</ul>
					
					<ul>
						<li><a href="" class="bold">Toutes mes promotions</a></li>
						<li><a href="" class="bold">Toutes les nouveautés</a></li>
					</ul>
					
				</div>
			
			</div>
			<!-- End Menu liveBlog -->
			
			
			
			<div class="bloc brandSmall">
				<div class="top-bar bg-white text-center"><span class="orange uppercase bold">Toutes nos marques</span></div>
				<div class="content-bloc">
					<ul>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/nike.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hm.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hugoboss.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/jules.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/nike.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						</ul>
				
				
				</div>
				
			</div>
			
			
			<div class="content-bloc">
			
			<div class="bloc border bg-light-gray">
				<div class="top-bar"><span class="green bold uppercase">Trier par prix</span></div>
				<div class="content-bloc">
					<ul>
						<li><input type="checkbox"><span class="xsmall bold"> 20 € à 50€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 50 € à 10€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 100 € à 200€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 200 € et plus <span class="gray">(457)</span></span></li>
						<li><span class="xsmall bold"> de <input type="text"> € à <input type="text"> €</span></li>
					</ul>
				</div>
				
			</div>
			
			<div class="groupBloc">
			
			<div class="top-bar bg-yellow"><span class="uppercase bold">Téléviseurs</span></div>
			
			<div class="bloc border-bottom">
				<div class="top-bar bg-white"><span class="uppercase orange bold">TV par taille</span></div>
				<div class="content-bloc">
				<ul>
						<li><input type="checkbox"><span class="xsmall">jusqu'à 59 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">61-76 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">77-99 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">102-114 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">114 cm et +</span></li>
					</ul>
				</div>
			</div>
			
			<div class="bloc border-bottom">
				<div class="top-bar bg-white"><span class="uppercase orange bold">TV par taille</span></div>
				<div class="content-bloc">
				<ul>
						<li><input type="checkbox"><span class="xsmall">jusqu'à 59 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">61-76 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">77-99 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">102-114 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">114 cm et +</span></li>
					</ul>
				</div>
			</div>
			
			<div class="bloc">
				<div class="top-bar bg-white"><span class="uppercase orange bold">TV par technologie</span></div>
				<div class="content-bloc">
				<ul>
						<li><input type="checkbox"><span class="xsmall">TV 3D</span></li>
						<li><input type="checkbox"><span class="xsmall">TV connectée à internet</span></li>
						<li><input type="checkbox"><span class="xsmall">TV LED</span></li>
						<li><input type="checkbox"><span class="xsmall">TV LCD</span></li>
						<li><input type="checkbox"><span class="xsmall">Toutes les TV par technologie</span></li>
					</ul>
				</div>
			</div>
			
			
			
			
		</div>	



<div class="bloc border bg-light-gray">
				<div class="top-bar"><span class="green bold uppercase">Trier par vendeur</span></div>
				<div class="content-bloc">
					<ul>
						<li><input type="checkbox"><span class="xsmall bold"> netShopRun <span class="gray">(250)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> Mobile974 <span class="gray">(200)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> Avahis.com <span class="gray">(150)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> Autre vendeur <span class="gray">(457)</span></span></li>
					</ul>
				</div>
				
			</div>


		
			
		</div>
		
		</div>
		
		
		
		<div class="span9 well-right">
		
				<h1>Photo et caméscopes</h1>
			
			<!-- Begin bloc rubric -->
			<div class="row-fluid rubric">
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
				</div>
			
			
			</div>
		<!-- End bloc rubric -->
		
		
		
		<!-- Begin bloc selection -->
	<div class="row-fluid selection visible-desktop">
		<div class="span12">
		<div class="bloc bloc-grid border bloc-width-title">
		<div class="top-bar "><span class="uppercase bg-white xlarge">les plus vues</span></div>
			<div class="content-bloc" id="slider-our-selection">
				<div class="pag"></div>
			<div class="row-fluid slides">
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">1. Le nom du jouet détailllé sur 2 lignes, Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">2. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/vin.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">3. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/vin2.jpg"><span class="tag bg-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">4. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">5. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">6. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">7. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">8. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">9. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">10. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
						<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">11. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">12. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"></a>
					<p class="text-left"><a href="#" class="blue">13. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
				</div>
				<div class="box span2 slide">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-blue">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">14. Le nom du jouet détailllé sur 2 lignes</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
				</div>
				
				
				</div>
			</div>
						<script type="text/javascript">
$(document).ready(function() {		
		 $('#slider-our-selection .slides').carouFredSel({
	 items		  : {
		visible	: 4,
		minimun	: 5
	  },
	  pagination : {
	  container : "#slider-our-selection .pag",
	  anchorBuilder : true
	  },
	  height: 220,
	  direction		: "left",
	  responsive : true,
        scroll : {
            items           : 4,
            duration        : 1000,                         
            pauseOnHover    : true,
			fx 				: "slide",
			direction		: "left"
        }                   
    });	

});
</script>
			
		</div>
		</div>
	</div>
	<!-- Fin bloc selection -->
		
		
		<div class="result">
		
		<!-- Begin breadcrumb -->
			<div id="breadcrumb" class="breadcrumb">
		<ul>
			<li><a href="#">High - Tech</a></li> >
			<li><a href="#">Photo et caméscopes</a></li> >
			<li><a href="#">Appareil photo numériques</a></li>
		</ul>
	</div>
		<!-- End breadcrumb -->
		
		
			<!-- Begin allPager -->
		<div class="allPager">
			<div class="left">
				<span class="bold uppercase">Résultats</span>
				<span class="dark bold"> 1 à 20</span> <span>sur 86</span>		
			</div>
			
			<div class="right">
				<span class="bold uppercase">Résultats par page</span>
				<select><option>10</option><option>20</option></select>
			</div>
			
			<div class="center">
				<a href="" class="left">< page précédente</a>
				<div class="pager left">
					<ul>
						<li class="selected"><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li><a href="">5</a></li>
						<li>...</li>
						<li><a href="">23</a></li>
					</ul>
				</div>
				<a href="" class="left">page suivante ></a>
				
			</div>
			
		</div>
			<!-- End allPager -->
		
		<div>
			<div class="left">
				<span class="uppercase bold gray xsmall">Type d'affichage</span>
				<a href="" class="xsmall gray"><i class="fa fa-th fa-lg fa-fw gray"></i> Grille</a>
				<a href="" class="xsmall gray"><i class="fa fa-align-justify fa-lg fa-fw gray"></i> Liste</a>
			</div>
			
			<div class="right">
				<span class="uppercase bold gray xsmall">trier</span>
				<select><option>Du moins cher au plus cher</option><option>Du plus cher au moins cher</option></select>
			</div>
			
		</div>
		
		
			<!-- Begin resultList list -->
		<div class="resultList list">
			<article class="productBox">
				<a href="" class="left"><img src="img/product/appareil-photo.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir - max 60 car. voir sur deux lignes. et blabla bla lorem ipsum... au final, un maximum de 160 caractères</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>
			
			<article class="productBox featured">
				<a href="" class="left"><img src="img/product/appareil-photo.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>		
			<article class="productBox">
				<a href="" class="left"><img src="img/product/vin.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>		
			<article class="productBox">
				<a href="" class="left"><img src="img/product/appareil-photo.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>
			<article class="productBox">
				<a href="" class="left"><img src="img/product/vin2.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>			
		
		
		</div>
		<!-- End resultList list -->
		
		
		
		
		<!-- Begin resultList grid -->
		<div class="resultList grid">
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>
	
			<article class="productBox featured">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>	
				<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/vin.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>	
				<article class="productBox ">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox featured">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/vin2.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>				
		</div>
		<!-- End resultList grid -->
		
		
					<!-- Begin allPager -->
		<div class="allPager">
			<div class="left">
				<span class="bold uppercase">Résultats</span>
				<span class="dark bold"> 1 à 20</span> <span>sur 86</span>		
			</div>
			
			<div class="right">
				<span class="bold uppercase">Résultats par page</span>
				<select><option>10</option><option>20</option></select>
			</div>
			
			<div class="center">
				<a href="" class="left">< page précédente</a>
				<div class="pager left">
					<ul>
						<li class="selected"><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li><a href="">5</a></li>
						<li>...</li>
						<li><a href="">23</a></li>
					</ul>
				</div>
				<a href="" class="left">page suivante ></a>
				
			</div>
			
		</div>
			<!-- End allPager -->
		
			
		
		</div>
		
		
		</div>
		
			
		
</div>
	
	<div class="well">
	<!-- Begin Bloc microboutique -->
			<div class="row-fluid">
				<div class="bloc bloc-grid">
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>					

					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>						
					
				</div>
			</div>
			<!-- Fin Bloc microboutique -->
	</div>
	
	
	
	
	
	
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>	