<?php
include "lib/config.php";
$page = "univers_4";

?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content">
	<div class="contenu">
	
	
		<div class="row-fluid">
	
		<div class="span3">
		
		<div class="bloc secondary">
			<header><span class="xsmall"><i class="fa fa-chevron-left fa-fw"></i></span> <a href="#"><span class="title">Appareil photo numérique<br><span class="xsmall">retour</span></span></a> <span class="right hidden-desktop reduct-list"><i class="fa fa-2x fa-list"></i></span></header>
		</div>
		
			<div class="content-bloc">
			
			<div class="bloc border bg-light-gray">
				<div class="top-bar"><span class="green bold uppercase">Trier par prix</span></div>
				<div class="content-bloc">
					<ul>
						<li><input type="checkbox"><span class="xsmall bold"> 20 € à 50€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 50 € à 10€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 100 € à 200€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 200 € et plus <span class="gray">(457)</span></span></li>
						<li><span class="xsmall bold"> de <input type="text"> € à <input type="text"> €</span></li>
					</ul>
				</div>
				
					<div class="top-bar"><span class="green bold uppercase">Toutes les marques</span></div>
				<div class="content-bloc">
					<ul class="maxHeight bg-white border">
						<li><input type="checkbox"><span class="xsmall bold"> 20 € à 50€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 50 € à 10€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 100 € à 200€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 200 € et plus <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 20 € à 50€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 50 € à 10€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 100 € à 200€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 200 € et plus <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 50 € à 10€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 100 € à 200€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 200 € et plus <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 20 € à 50€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 50 € à 10€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 100 € à 200€ <span class="gray">(457)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> 200 € et plus <span class="gray">(457)</span></span></li>						
					</ul>
				</div>
				
				
			</div>
			
			
			
			<div class="groupBloc">
			
			<div class="top-bar bg-yellow"><span class="uppercase bold">Appareil photo reflex</span></div>
			
			<div class="bloc border-bottom">
				<div class="top-bar bg-white"><span class="uppercase orange bold">TV par taille</span></div>
				<div class="content-bloc">
				<ul>
						<li><input type="checkbox"><span class="xsmall">jusqu'à 59 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">61-76 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">77-99 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">102-114 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">114 cm et +</span></li>
					</ul>
				</div>
			</div>
			
			<div class="bloc border-bottom">
				<div class="top-bar bg-white"><span class="uppercase orange bold">TV par taille</span></div>
				<div class="content-bloc">
				<ul>
						<li><input type="checkbox"><span class="xsmall">jusqu'à 59 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">61-76 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">77-99 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">102-114 cm</span></li>
						<li><input type="checkbox"><span class="xsmall">114 cm et +</span></li>
					</ul>
				</div>
			</div>
			
			<div class="bloc">
				<div class="top-bar bg-white"><span class="uppercase orange bold">TV par technologie</span></div>
				<div class="content-bloc">
				<ul>
						<li><input type="checkbox"><span class="xsmall">TV 3D</span></li>
						<li><input type="checkbox"><span class="xsmall">TV connectée à internet</span></li>
						<li><input type="checkbox"><span class="xsmall">TV LED</span></li>
						<li><input type="checkbox"><span class="xsmall">TV LCD</span></li>
						<li><input type="checkbox"><span class="xsmall">Toutes les TV par technologie</span></li>
					</ul>
				</div>
			</div>
			
			
			
			
		</div>	



<div class="bloc border bg-light-gray">
				<div class="top-bar"><span class="green bold uppercase">Trier par vendeur</span></div>
				<div class="content-bloc">
					<ul>
						<li><input type="checkbox"><span class="xsmall bold"> netShopRun <span class="gray">(250)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> Mobile974 <span class="gray">(200)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> Avahis.com <span class="gray">(150)</span></span></li>
						<li><input type="checkbox"><span class="xsmall bold"> Autre vendeur <span class="gray">(457)</span></span></li>
					</ul>
				</div>
				
			</div>


		
			
		</div>
		
		</div>
		
		
		
		<div class="span9 well-right">
		
		
		<div class="result">
		
		<!-- Begin breadcrumb -->
			<div id="breadcrumb" class="breadcrumb">
		<ul>
			<li><a href="#">High - Tech</a></li> >
			<li><a href="#">Photo et caméscopes</a></li> >
			<li><a href="#">Appareil photo numériques</a></li>
		</ul>
	</div>
		<!-- End breadcrumb -->
		
		
			<!-- Begin allPager -->
		<div class="allPager">
			<div class="left">
				<span class="bold uppercase">Résultats</span>
				<span class="dark bold"> 1 à 20</span> <span>sur 86</span>		
			</div>
			
			<div class="right">
				<span class="bold uppercase">Résultats par page</span>
				<select><option>10</option><option>20</option></select>
			</div>
			
			<div class="center">
				<a href="" class="left">< page précédente</a>
				<div class="pager left">
					<ul>
						<li class="selected"><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li><a href="">5</a></li>
						<li>...</li>
						<li><a href="">23</a></li>
					</ul>
				</div>
				<a href="" class="left">page suivante ></a>
				
			</div>
			
		</div>
			<!-- End allPager -->
		
		<div>
			<div class="left">
				<span class="uppercase bold gray xsmall">Type d'affichage</span>
				<a href="" class="xsmall gray"><i class="fa fa-th fa-lg fa-fw gray"></i> Grille</a>
				<a href="" class="xsmall gray"><i class="fa fa-align-justify fa-lg fa-fw gray"></i> Liste</a>
			</div>
			
			<div class="right">
				<span class="uppercase bold gray xsmall">trier</span>
				<select><option>Du moins cher au plus cher</option><option>Du plus cher au moins cher</option></select>
			</div>
			
		</div>
		
		
			<!-- Begin resultList list -->
		<div class="resultList list">
			<article class="productBox">
				<a href="" class="left"><img src="img/product/appareil-photo.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir - max 60 car. voir sur deux lignes. et blabla bla lorem ipsum... au final, un maximum de 160 caractères</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>
			
			<article class="productBox featured">
				<a href="" class="left"><img src="img/product/appareil-photo.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>		
			<article class="productBox">
				<a href="" class="left"><img src="img/product/vin.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>		
			<article class="productBox">
				<a href="" class="left"><img src="img/product/appareil-photo.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>
			<article class="productBox">
				<a href="" class="left"><img src="img/product/vin2.jpg"> <span class="tag tag-bottom bg-green bold white">livraison 72h</span></a>
				<div class="desc">
				<div class="right">
					<span class="right"><span class="small gray">à partir de</span> <span class="xlarge pink bold"> 62.97 €</span></span><br><br>
					<span class="green bold right xsmall"><i class="fa fa-check fa-fw green"></i> En stock</span>
				</div>
				
				<a href=""><h2>Montre Casio G-SHOCK Shock Resist Noir</h2></a>
				<p class="small gray">22O caractères : ipsum dolor sit amet, consectetur adipiscing elit. Quisque dapibus dictum interdum. Etiam laoreet malesuada dolor at elementum. Aenean in eros tincidunt neque porttitor suscipit. Integer tristique sagittis posuere, [...]</p>
				
				<div>
				<div class="breadcrumb">
					<span class="xsmall bold"> ><a href=""> dans la categorie</a> > <a> Dernier niveau</a></span>
				</div>	
					<div class="right">
						<a href="" class="btn gray btn-ico-left btn-small"><i class="fa fa-shopping-cart fa-lg fa-fw gray"></i> ajouter au panier</a>
						<a href="" class="btn btn-primary btn-ico-left uppercase"><i class="fa fa-search fa-lg fa-fw"></i> Voir la fiche</a>
					
					</div>
				</div>
				
				</div>
				
			</article>			
		
		
		</div>
		<!-- End resultList list -->
		
		
		
		
		<!-- Begin resultList grid -->
		<div class="resultList grid">
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>
	
			<article class="productBox featured">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>	
				<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/vin.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>	
				<article class="productBox ">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox featured">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/appareil-photo.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>			
			<article class="productBox">
				<a href="">
					<h2>Montre Casio G-SHOCK Shock Resist Noir - max 60</h2>
					<span class="right"><span class="xsmall gray">à partir de</span> <span class="xlarge orange bold"> 62.97 €</span></span><br><br>
					
					<img src="img/product/vin2.jpg">
				
					<span class="view"><i class="fa fa-search fa-fw fa-lg white"></i> Voir le produit</span>
				</a>
			</article>				
		</div>
		<!-- End resultList grid -->
		
		
					<!-- Begin allPager -->
		<div class="allPager">
			<div class="left">
				<span class="bold uppercase">Résultats</span>
				<span class="dark bold"> 1 à 20</span> <span>sur 86</span>		
			</div>
			
			<div class="right">
				<span class="bold uppercase">Résultats par page</span>
				<select><option>10</option><option>20</option></select>
			</div>
			
			<div class="center">
				<a href="" class="left">< page précédente</a>
				<div class="pager left">
					<ul>
						<li class="selected"><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li><a href="">5</a></li>
						<li>...</li>
						<li><a href="">23</a></li>
					</ul>
				</div>
				<a href="" class="left">page suivante ></a>
				
			</div>
			
		</div>
			<!-- End allPager -->
		
			
		
		</div>
		
		
		
			<!-- Begin serviceBar -->
				<div class="serviceBar bloc border">
					<div class="top-bar top-bar-center bg-dark"><span class="bold white uppercase">Payez en 3 fois sans frais des 100 € d’achats</span><a href="#" class="underline xsmall white">> voir toutes les conditions</a></div>
					<div class="row-fluid no-margin">
						<div>
						<p class="uppercase bold small">Livraison facile et rapide</p>
							<p><i class="fa fa-truck fa-3x"></i></p>
							<div>
							
							<p>A partir de 48h en express, du commerçant à votre domicile</p>
							<a href="#" class="xsmall underline bold">En savoir plus</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">Votre Service client !</p>
							<p><i class="fa fa-phone-square fa-3x"></i></p>
							<div>							
							<p>Contactez nous par téléphone ou par mail, nous vous répondrons au plus vite </p>
							<a href="#" class="xsmall underline bold">Nous contacter</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">satisfait ou remboursé</p>
							<p><i class="fa fa-reply-all fa-3x"></i></p>
							<div>
							
							<p>Nos commerçants reprenent vos produits sous 15 jours.</p>
							<a href="#" class="xsmall underline bold">Les conditions de retour</a>
							</div>
						</div>
					</div>
					<div class="bottom-bar bottom-bar-center"><span class="small">service client du lundi au vendredi de 9h à 18h au</span> <span class="large bold">0693 911 859</span><span class="small"></span></div>
		</div>
	<!-- Fin serviceBar -->	
		
		
		
		
		</div>
		
			
		
</div>
	
		<div class="well">
	<!-- Begin Bloc microboutique -->
			<div class="row-fluid">
				<div class="bloc bloc-grid">
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>					

					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span3 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>						
					
				</div>
			</div>
			<!-- Fin Bloc microboutique -->
	</div>
	
	
	
	
	
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>	