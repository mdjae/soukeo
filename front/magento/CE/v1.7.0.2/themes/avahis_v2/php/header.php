<header id="header">
<div id="headline">
		<span class="welcome visible-desktop">Place de marché de <span class="strong">La Réunion</span></span>	
		<span class="contact-us"><i class="fa fa-phone fa-lg"></i> service client au <span class="strong">0693 911 859</span></span>
		<ul class="hidden-phone">
			<li><a href="#">promotions</a></li>
			<li><a href="#">nouveautés</a></li>
			<li><a href="#">aide</a></li>
			<li><a href="#">vendez sur Avahis</a></li>
		</ul>
</div>

<h1 id="logo" class="logo" itemscope="" itemtype="http://schema.org/Organization">
	<a itemprop="url" href="http://www.avahis.fr" title="Retour à l'accueil  ">
		<img itemprop="logo" src="img/logo.png" alt="Avahis" width="170" height="auto">
	</a>
</h1>	

<form class="search" id="searchForm" method="POST" action="">
	<fieldset>
		<input id="search" name="search-field" type="search" placeholder="Chercher un produit" dir="ltr" required="">
		<button id="search_btn" class="btn btn-primary btn-search btn-ico-left" type="submit"><i class="fa fa-search"></i> chercher</button>
	</fieldset>
</form>

<div id="toolbar">
	<ul>
		<li class="visible-phone"><a href="#nav"><i class="fa fa-bars fa-2x"></i> <span>Rayon</span></a></li>
		<li class="hidden">
            <a href="#" class="accountLink ">
                 <i class="fa fa-user"></i> <span>Mon compte</span>
            </a>
			<div class="box-shadow-hover hidden-phone hidden-tablet accountBloc">
				<div class="well">
				<ul>
					<li><a href="#">Actif quand connecter</a></li>
					<li><a href="#">Lien 2</a></li>
					<li><a href="#">Lien 3</a></li>
					<li><a href="#">Lien 4</a></li>
				</ul>
				<a href="#" class="blue right xsmall underline">> Déconnection</a>
				</div>
			</div>
         </li>
		 <li>
			<a href="http://www.avahis.com/customer/account/" class="connectLink">
				<i class="fa fa-lock"></i> <span>Me connecter</span>
			</a>
			<div class="well box-shadow-hover hidden-phone hidden-tablet connectBloc">
			<div>
				<div>
					<p class="large bold">Déja inscrit sur Avahis.com ?</p>
					<p><span class="bold">Connectez-vous</span> en entrant vos identifiants.</p>
					<form>
						<label for="email">Adresse email</label><input type="texte" id="email">
						<label for="pass">Mot de passe</label><input type="texte" id="pass">
						<p><label></label><a href="#" class="blue underline small">mot de passe perdu</a></p>
						<label></label><button class="btn btn-primary btn-ico-right right">connexion <i class="fa fa-lock fa-lg"></i></button/>
					</form>
				</div>
				
				<div>
					<p class="large bold">Rejoignez Avahis.com !</p>
					<p>Créez un compte pour passer vos commandes et bénéficier des avantages réservés aux membres</p>
					<a href="#" class="btn">créer un compte</a>
				</div>
			</div>
			
			<div>
				<p class="large bold">Accès professionnel</p>
				<a href="#" class="btn right">accès professionnels</a>
				<p><span class="bold">Rejoignez les commerçants</span> sur Avahis.com et profitez de nombreux avantages ! <a href="#" class="blue underline small">en savoir plus</a></p>
			</div>
			
			</div>
		</li> 
		<li>
            <a href="http://www.avahis.com/checkout/cart/" class="cartLink ">
                 <i class="fa fa-shopping-cart"></i> <span>Mon panier</span><span class="number">3</span>
            </a>
			<div class="box-shadow-hover hidden-phone hidden-tablet cartBloc">
				<div class="well">
				<p class="large"><span class="bold">3 produits</span> dans votre panier :</p>
				</div>
				<ul>
					<li>
						<a href="#"><i class="fa fa-times"></i></a>
						<a href="#"><img src="img/product/portable.jpg"></a>
						<p><a href="#">Montre Casio G-SHOCK Shock Resist Noir</a> <span class="gray">(x 1)</span><br>
						<span class="red">64.99</span></p>						
					</li>
					<li>
						<a href="#"><i class="fa fa-times"></i></a>
						<a href="#"><img src="img/product/portable.jpg"></a>
						<p><a href="#">Montre Casio G-SHOCK Shock Resist Noir</a> <span class="gray">(x 1)</span><br>
						<span class="red">64.99</span></p>						
					</li>
					<li>
						<a href="#"><i class="fa fa-times"></i></a>
						<a href="#"><img src="img/product/portable.jpg"></a>
						<p><a href="#">Montre Casio G-SHOCK Shock Resist Noir</a> <span class="gray">(x 1)</span><br>
						<span class="red">64.99</span></p>						
					</li>					
				</ul>
				<div class="well">
				<p class="left"><span class="bold">montant total</span><br> de votre commande :</p>
				<p class="right"><span class="xlarge bold red">194.97 €</span><br><span class="gray">* hors frais d'envoi</span></p>
				<a href="#" class="btn btn-primary btn-ico-right center">passer ma commande <i class="fa fa-shopping-cart"></i></a>
				</div>
			</div>
         </li>
  
	</ul>
</div>
	
</header>