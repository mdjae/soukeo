<nav role="navigation" id="nav" class="primary">
		
		<header class="hidden-desktop">
			<span class="strong">Navigation produits</span>
<a href="#container" class="hidden-tablet mean-top"><i class="fa fa-arrow-up"></i> Retour en haut</a>
</header>
			<ul id="navigation" class="nav">
				<!--<li style="width: 30px;"class="home visible-desktop"><a href="#"><i class="fa fa-home fa-lg"></i></a></li> -->
				<li class="btn-ssmenu"><a href="">Informatique Télèphones</a>
					<ul class="submenu">					
						<div><li class="btn-ssmenu"><a href="#"><h2>titre1 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
							
						</li>
						<li class="btn-ssmenu">
						<a href="#"><h2>titre2 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>	
						</li>
						</div>
						
						<div><li class="btn-ssmenu"><a href="#"><h2>titre3 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
						</li>							
						</div>
						<div><li class="btn-ssmenu"><a href="#"><h2>titre4 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
							</li>
						</div>
						<div><li class="btn-ssmenu"><a href="#"><h2>titre5 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
						</li>
						<li class="btn-ssmenu">
						<a href="#"><h2>titre6 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>	
						</li>
						</div>
						<div><li class="btn-ssmenu"><a href="#"><h2>titre7 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
						</li>							
						</div>											
					</ul>
				</li>
				<li class="btn-ssmenu"><a href="">High-tech Hi-fi, photo</a>
				<ul class="submenu">					
						<div><li class="btn-ssmenu"><a href="#"><h2>titre1 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
							
						</li>
						<li>
						<a href="#"><h2>titre2 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>	
						</li>
						<li>
						<a href="#"><h2>titre2-1 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>	
						</li>
						</div>
						
						<div><li class="btn-ssmenu"><a href="#"><h2>titre3 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
						</li>							
						</div>
						<div><li class="btn-ssmenu"><a href="#"><h2>titre4 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
							</li>
						</div>
						<div><li><a href="#"><h2>titre5 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>
						</li>
						<li>
						<a href="#"><h2>titre6 Sous-menu1</h2></a>
							<ul>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
								<li><a href="#">Lien sous menu2</a></li>
							</ul>	
						</li>
						</div>	
						<div class="visible-desktop additionnal-submenu">
							<a href="#"><h2>Liens complémentaires</h2></a>
							<a href="#"><img src="img/slide1.png" width="150" height="auto"></a><br>
							<a href="#" class="blue underline xsmall">> un lien externe</a><br>
							<a href="#"><img src="img/slide1.png" width="150" height="auto"></a>
						</div>						
					</ul>
				</li>
				<li><a href="">Jeux vidéos</a></li>
				<li><a href="">Maison, Déco, Cuisine</a></li>
				<li><a href="">Bricoloage Jardin</a></li>
				<li><a href="">Enfants, Bébés Jeux et jouets</a></li>
				<li><a href="">Vêtements chaussures</a></li>
				<li><a href="">Parfum Beauté, santé</a></li>
				<li><a href="">Bureau Papeterie</a></li>				
				<li><a href="">Livres, DVD, Blueray</a></li>
				<li><a href="">Musique et instruments</a></li>
				<li><a href="">Alimentaire Animalerie</a></li>
				<li class="plusLink visible-desktop"><a href="">Plus de<br><span class="bold">catégories...</span></a></li>
				<div class="box-shadow-hover">
					<li class="btn-ssmenu"><a href="">Auto, moto, GPS</a>
						<ul class="submenu">
							<li class="btn-ssmenu"><a href="#"><h2>titre sousmenu</h2></a>
							<ul>
								<li><a href="#">lien sous menu hidden</a></li>
							</ul>
							</li>
						</ul>
					</li>
					<li><a href="">Instruments de musique et Sono</a></li>
					<li><a href="">Tourisme Sport, Loisirs</a></li>
					<li><a href="">Services</a></li>
					<a href="#" class="blue right xsmall underline visible-desktop">> toutes les catégories</a>
				</div>
			</ul>
		</nav>