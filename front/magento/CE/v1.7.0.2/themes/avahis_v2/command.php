<?php
include "lib/config.php";
$page = "Command";

?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>" class="command">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content">
	<div class="contenu">
	
	
	
		<div class="row-fluid">
	
	<!-- Begin Menu Tertiaire -->
		<div class="span3">
			<div class="bloc tertiary">
				
					<ul>
						<li class="selected"><a href="#"><span>1</span> Mon panier</a></li>
						<li><a href="#"><span>2</span> Mode de commande</a></li>
						<li><a href="#"><span>3</span> Adresse de facturation</a></li>
						<li><a href="#"><span>4</span> Adresse de livraison</a></li>
						<li><a href="#"><span>5</span> Mode de livraison</a></li>
						<li><a href="#"><span>6</span> Mode de paiement</a></li>
						<li><a href="#">Validation</a></li>
					</ul>
				
			</div>
		</div>
		<!-- Fin Menu Tertiaire -->
	
	
		<div class="span9 processCommand">
		<div class="well border box-shadow">
			<div class="row-fluid">
				
				
				<div class="left">
					<p class="large">Vous avez choisi <span class="bold large">3 articles</span></p>
					<p class="large">pour un montant total de <span class="bold large">96.70 € *</span><br>
					<span class="xsmall">* frais de port estimés</span></p>
				</div>
				
				
				<div class="helpBloc right visible-desktop">
					<p class="medium-title bold">Besoin d'aide ?</p>
					<p>Contactez-nous au <br> <span class="bold">0693 911 859</span></p>
				</div>
				
				<div class="text-center" style="margin-top: 100px;">
					<a href="#" class="btn btn-primary btn-ico-right"> passer ma commande <i class="fa fa-shopping-cart fa-lg"></i></a>
				</div>
			</div>
			
			
			
			<!-- Begin Tableau --> 
			<div class="row-fluid">
			<table>
				<thead>
						<th>Vendeur</th>
						<th>Description</th>
						<th>Prix unitaire</th>
						<th>Quantité</th>
						<th>Total</th>
						<th></th>
				</thead>
				
				<tbody>
					<tr>
						<td><a href="#" class="imgLink"><img src="img/commercants/phytorun.jpg"></a></td>
						<td>
						<a class="bold blue xsmall visible-phone visible-tablet ">> Vendu par Phytorun</a>
							<a href="#" class="imgLink"><img src="img/product/portable.jpg" class="border"></a>
							<p>Tshirt à manches longues Court Tee JACK & JONES<br>
							<span class="bold">Taille : L</span><br>
							<span class="bold green">En stock</span></p>
							
						</td>
						<td><span>29.90 €</span></td>
						<td><select><option>1</option><option>2</option></select></td>
						<td><span class="bold">29.90 €</span></td>
						<td><a href="#" class="trash"><i class="fa fa-trash-o fa-lg fa-fw"></i></a></td>
					</tr>
					<tr>
						<td><a href="#" class="imgLink"><img src="img/commercants/phytorun.jpg"></a></td>
						<td>
						<a class="bold blue xsmall visible-phone visible-tablet ">> Vendu par Phytorun</a>
							<a href="#" class="imgLink"><img src="img/product/portable.jpg" class="border"></a>
							<p>Tshirt à manches longues Court Tee JACK & JONES<br>
							<span class="bold">Taille : L</span><br>
							<span class="bold green">En stock</span></p>
						</td>
						<td><span>29.90 €</span></td>
						<td><select class="select"><option>1</option><option>2</option></select></td>
						<td><span class="bold">29.90 €</span></td>
						<td><a href="#" class="trash"><i class="fa fa-trash-o fa-lg fa-fw"></i></a></td>
					</tr>
					<tr>
						<td colspan="6"  class="shipping"><span><i class="fa fa-truck fa-fw fa-lg fa-flip-horizontal"></i> frais de livraison estimés : <span class="bold">7 €</span></span></td>
					</tr>
					<tr>
						<td><a href="#" class="imgLink"><img src="img/commercants/phytorun.jpg"></a></td>
						<td>
						<a class="bold blue xsmall visible-phone visible-tablet ">> Vendu par Phytorun</a>
							<a href="#" class="imgLink"><img src="img/product/portable.jpg" class="border"></a>
							<p>Tshirt à manches longues Court Tee JACK & JONES<br>
							<span class="bold">Taille : L</span><br>
							<span class="bold green">En stock</span></p>
						</td>
						<td><span>29.90 €</span></td>
						<td><select><option>1</option><option>2</option></select></td>
						<td><span class="bold">29.90 €</span></td>
						<td><a href="#" class="trash"><i class="fa fa-trash-o fa-lg fa-fw"></i></a></td>
					</tr>
					<tr>
						<td colspan="6"  class="shipping"><span><i class="fa fa-truck fa-fw fa-lg fa-flip-horizontal"></i> frais de livraison estimés : <span class="bold">offerts</span></span></td>
					</tr>
					
				</tbody>							
			</table>
		</div>
		<!-- End Tableau --> 
			
			
			
		<div class="row-fluid">
			<div class="span6 border">
				<div class="bloc">
					<div class="top-bar"><i class="fa fa-lg fa-fw fa-barcode"></i><span class="bold"> Vous avez un bon de réduction ?</span></div>
					<div class="content-bloc">
						<form>
						<input type="text" placeholder="saisissez votre code ici..."><button class="btn btn-small">Appliquer</button>
						</form>
					</div>
				</div>
			</div>
			
		<div class="span6 border retailBilling">
		<div class="bloc">
					<div class="top-bar"><i class="fa fa-lg fa-fw fa-thumb-tack"></i><span class="bold"> Détail de ma facturation</span></div>
					<div class="content-bloc">
						<p>Total article <span class="right bold large">89.70 €</span></p>
						<p>Frais de livraison <span class="right bold large">7 €</span></p>
						<p class="pink uppercase bold">Montant de ma commande <span class="right bold pink xlarge">89.70 €</span></p>
					</div>
				</div>
			</div>
		</div>		
			
		
		<div class="row-fluid">
			<a href="#" class="btn btn-purchase"><span>Continuer mes achats</span><br> et retourner au catalogue</a>		
			<a href="#" class="btn btn-primary btn-ico-right right btnMyOrder"> passer ma commande <i class="fa fa-shopping-cart fa-lg"></i></a>
		
		</div>
			
		
		</div>
	

	</div>
	</div>
	
	
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>	