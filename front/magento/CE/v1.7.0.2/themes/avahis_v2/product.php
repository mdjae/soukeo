<?php
include "lib/config.php";
$page = "product";

?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>" class="<?php echo $page; ?>">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content no-margin-top">
	<div class="contenu">
	
	<?php include 'php/breadcrumb.php'; ?>
	
	<div class="ribbon visible-desktop"><div class="corner" style="border-color: transparent transparent #0099da transparent;"></div><a href="#" class="bg-blue small"><span class="small white">retour</span> <span class="bold white">Ordinateur portable</span></a></div>
	
	<div class="well">
	
		<div class="title"><h1>MSI S12T 3M-011FR - AMD E1-2100 4 Go 500 Go 11.6" LED Tactile Wi-Fi N/Bluetooth Webcam Windows 8 64 bits </h1> <span class="green"><i class="fa fa-check green"></i> en cours d'approvisement</span></div>

		<div class="row-fluid">

			<!-- begin fiche produit -->
			<div class="span9">
				<div class="image">
					<a href="#"><img src="img/product/portable.jpg" width="300" height="300"></a>
					
					<div class="thumbs visible-desktop">
						<a href="#" class="thumb"><img src="img/product/portable.jpg" width="50" height="50"></a>
						<a href="#" class="thumb"><img src="img/product/portable.jpg" width="50" height="50"></a>
					</div>
					<div class="social"><a href="#">social</a></div>
				</div>
				
				<div class="text">
						<p class="large">à partir de <span class="line-through gray">481.30 €</span> <span class="large bold red">450.00 €</span><span class="label bg-orange">promotion</span></p>
						<p><span><i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star-half-o yellow"></i> <i class="fa fa-star-o yellow"> </i></span> 2 avis <a href="#" class="blue underline">voir les avis</a></p>
						<p>Parfait pour les golfeurs, musiciens, joueurs de squash, de tennis, d'escrime, de basket, de badminton, de tir à l'arc, de criquet, etc.<br>
S'utilise pour rééduquer et soulager le syndrome du canal carpien et les TMS.<br>
La Powerball Neon a 6 LED haute intensité qui brillent lorsque le Powerball tourne.<br>
The compteur fonctionne avec deux piles boutons d'une durée de vie approximative de 3 ans.<br>
La lueur du Powerball est générée par une bobine d'induction dans le rotor<br>
Plus il tourne vite, plus il brille !<br>
							<a href="#" class="blue underline">> en savoir plus</a></p>
							
							<p><span class="border" style="display:inline-block; padding: 1px 3px;"><img src="img/marque/msi.jpg"></span><br>
							<a href="#" class="blue underline">tous les produits MSI</a></p>
				</div>
			
			</div>
			<!-- Fin fiche produit -->
			
			<!-- begin bloc droit vendeurs -->
			<div class="span3 border bg-light-gray box-shadow">
				<div class="content-bloc text-center">
					<p><span class="red bold">450.00 €</span> + livraison <span class="bold">48h</span><br>
					Vendu par <span class="bold">NetshopRun</span></p>
					 <p> quantité: <select><option>1</option><option>2</option></select></p>
					 <a href="#" class="btn btn-primary btn-ico-circle add-cart"><span><i class="fa fa-shopping-cart fa-lg"></i></span> ajouter au panier</a>
					 <p><a href="#" class="underline">ajouter à ma liste d'envies</a><br><a href="#" class="underline">acheter directement</a></p>
				</div>
				<div class="bg-pink text-center">
					<div class="content-bloc">
					<p class="white bold no-margin">Paiement CB en 3 fois sans frais ! <br>
					<a href="#" class="white underline">voir les conditions</a></p>
					</div>
				</div>
				<div class="content-bloc seller">
					<span class="small"><span class="bold">3 vendeurs</span> proposent ce produit: </span>
					<ul>
						<li>
							<a href="#" class="btn btn-small btn-ico-left"><i class="fa fa-fw fa-shopping-cart fa-2x"></i> choisir ce vendeur</a>
							<div><p><span class="bold">NetshopRun avec un titre sur 2 lignes</span><br><span class="red">481.30 €</span><span class="small"> + livraison <span class="bold">48h</span></span></p></div>
						</li>
						<li>
							<a href="#" class="btn btn-small btn-ico-left"><i class="fa fa-fw fa-shopping-cart fa-2x"></i>choisir ce vendeur</a>
							<div><p><span class="bold">Mobile974</span><br><span class="red">481.30 €</span><span class="small"> + livraison <span class="bold">48h</span></span></p></div>
						</li>
						<li>
							<a href="#" class="btn btn-small btn-ico-left"><i class="fa fa-fw fa-shopping-cart fa-2x"></i>choisir ce vendeur</a>
							<div><p><span class="bold">reunion Informatique</span><br><span class="red">481.30 €</span><span class="small"> + livraison <span class="bold">7 jours</span></span></p></div>
						</li>
						
					</ul>
				</div>
				
			</div>
			<!-- fin bloc droit vendeurs -->
			
		</div>
	</div>
	
	<div class="well-right">
		<div class="row-fluid">
			<div class="span9">
			<div class="row-fluid border box-shadow">
				<div class="bloc" id="slider-description">
					<div class="bg-light-gray pag">
						<ul>
							<li>Description du produit</li>
							<li>Fiche technique</li>
							<li>Avis des utilisateurs</li>
						</ul>
					</div>
					<div class="content-bloc slides">
						<div class="slide"><p><img src="img/text.jpg" style="width: 100%; height: 750px;"></p></div>
						<div class="slide"><p>la fiche technique</p></div>
						<div class="slide"><p>l'avis des utilisateurs</p></div>
					</div>
					
				</div>
									<script type="text/javascript">
$(document).ready(function() {
      $('#slider-description .slides').carouFredSel({
	  items		  : {
		visible	: 1
	  },
	  pagination : {
	  container : "#slider-description .pag ul",
	  anchorBuilder : false
	  },
	  responsive : true,
        scroll : {
            items           : 1,
            duration        : 1000,  
			fx 				: "crossfade"
        },
		auto : {
			play			: false
		}		
    });
	 });
	</script>
				</div>
				
				<div class="row-fluid sawAndAllSeller box-shadow border bg-light-gray">
					<div class="span5">
					<div class="bloc bloc-grid bloc-width-title saw">
						<div class="top-bar"><span class="bg-smoke gray uppercase xlarge bold">vous regardez</span></div>
						<div class="content-bloc">
							<div>
							<a href="#"><img src="img/product/portable.jpg" width="70" height="70"></a>
							<p>quantité: <select><option>1</option><option>2</option></select></p>
							</div>
							<div>
								<p class="xsmall">MSI S12T 3M-011FR - AMD E1-2100 4 Go 500 Go 11.6" LED Tactile Wi-Fi N/Bluetooth Webcam Windows 8 64 bits</p>
								<p><a href="#" class="underline small">ajouter à ma liste d’envies</a><br> <a href="#" class="underline small">acheter directement</a></p>
							</div>
						</div>
					</div>
					
					</div>
					
					<div class="span7">
					<div class="bloc bloc-grid bloc-width-title allSeller">
						<div class="top-bar"><span class="bg-yellow gray uppercase xlarge bold">tous les vendeurs pour ce produit</span></div>
						<div class="content-bloc">
							<ul>
								<li>
									<a href="#"><img src="img/commercants/phytorun.jpg"></a>
								<a href="#" class="btn btn-primary btn-small">ajouter au panier</a>
									<div>
										<p><a href="#" class="bold">NetShop Run avec un grand titre sur 2 lignes pour test</a> <span class="small"><i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star-half-o yellow"></i> <i class="fa fa-star-o yellow"> </i></span><br>
										<span><span class="red bold">450.00 €</span> <span class="small">+ livraison à partir de <span class="large bold">7 €</span></span></span></p>
										
									</div>
										
								</li>
								<li>
									<a href="#"><img src="img/commercants/phytorun.jpg"></a>
								<a href="#" class="btn btn-primary btn-small">ajouter au panier</a>
									<div>
										<p><a href="#" class="bold">NetShop Run</a> <span class="small"><i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star-half-o yellow"></i> <i class="fa fa-star-o yellow"> </i></span><br>
										<span><span class="red bold">450.00 €</span> <span class="small">+ livraison à partir de <span class="large bold">7 €</span></span></span></p>
										
									</div>
										
								</li>
								<li>
									<a href="#"><img src="img/commercants/phytorun.jpg"></a>
								<a href="#" class="btn btn-primary btn-small">ajouter au panier</a>
									<div>
										<p><a href="#" class="bold">NetShop Run</a> <span class="small"><i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star yellow"></i> <i class="fa fa-star-half-o yellow"></i> <i class="fa fa-star-o yellow"> </i></span><br>
										<span><span class="red bold">450.00 €</span> <span class="small">+ livraison à partir de <span class="large bold">7 €</span></span></span></p>
										
									</div>
										
								</li>
							</ul>
						</div>
					</div>
					</div>
				</div>
				
			</div>
			
			<div class="span3 border serviceBar visible-desktop">
			
				<div class="top-bar bg-gray text-center"><span class="uppercase bold white small">Paiement en 3 fois</span></div>
				<div class="content-bloc"><i class="fa fa-fw fa-credit-card fa-2x dark"></i><p class="xsmall">Service à partir de 100 € d’achat, voir <a href="#" class="underline">le détail des conditions</a></p></div>
			
				<div class="top-bar bg-gray text-center"><span class="uppercase bold white small">Paiement sécurisé</span></div>
				<div class="content-bloc"><i class="fa fa-lock fa-3x dark"></i><p class="xsmall">A partir de 48h en express, du commerçant à votre domicile <a href="#" class="underline">En savoir plus</a></p></div>
				
				<div class="top-bar bg-gray text-center"><span class="uppercase bold white small">livraison facile et rapide</span></div>
				<div class="content-bloc"><i class="fa fa-truck fa-2x dark"></i><p class="xsmall">A partir de 48h en express, du commerçant à votre domicile <a href="#" class="underline">En savoir plus</a></p></div>
				
				<div class="top-bar bg-gray text-center"><span class="uppercase bold white small">votre service client !</span></div>
				<div class="content-bloc"><i class="fa fa-phone-square fa-2x dark"></i><p class="xsmall">Le service client est ouvert de lundi à vendredi, de 9h à 18h.<br><br> Contactez nous par téléphone au 0693 911 859 ou <a href="#" class="underline">par mail</a>, nos conseillers vous répondront du lundi au plus vite !</p></div>
				
				<div class="top-bar bg-gray text-center"><span class="uppercase bold white small">satisfait ou remboursé</span></div>
				<div class="content-bloc"><i class="fa fa-mail-reply fa-2x dark"></i><p class="xsmall">Nos commerçants reprenent vos produits sous 15 jours. <a href="#" class="underline">Les conditions de retour</a></p></div>
				
			</div>
		</div>
	</div>
	
	<div class="well">
		<!-- Begin bloc promotion -->
	<div class="row-fluid bloc bloc-grid bloc-width-title">
		<div class="span12 content-bloc">
			<div class="top-bar top-bar "><span class="uppercase xlarge bg-yellow">nous vous conseillons...</span></div>
			<div class="row-fluid promotion ">
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-green">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">949.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue dark">- 15 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Fin bloc promotion -->	
	</div>
	
	
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>	