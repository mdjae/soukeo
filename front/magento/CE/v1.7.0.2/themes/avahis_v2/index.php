<?php
include "lib/config.php";
$page="home"

?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content">
	<div class="contenu">
	
		<div class="row-fluid visible-desktop">
			<div class="span12">
				
				<!-- Begin #slider-home -->
				<div id="slider-home">
				<div class="pag">
					<ul>
						<li><a href="#">Les boutons</a></li>
						<li><a href="#">Les boutons styles</a></li>
						<li><a href="#">Vendez sur Avahis</a></li>
						<li><a href="#">Paiment sécurisé et livraison sous 72h</a></li>
					</ul>
					</div>
				<div class="slides">
													
						<div class="slide">
						<div style="padding-top: 5px;">
							
								<p class="text-center xlarge bold">Le style normal des bouton !</p>
								<span>Normal</span> <button class="btn left">vendre sur AVAHIS</button> <button class="btn hover left ">vendre sur AVAHIS</button>  <button class="btn active left ">vendre sur AVAHIS</button> <br><br><br>
								<span>Primaire</span><button class="btn left btn-primary">vendre sur AVAHIS</button> <button class="btn hover left btn-primary ">vendre sur AVAHIS</button>  <button class="btn active left btn-primary">vendre sur AVAHIS</button> <br><br><br>
								<span>Secondaire</span><button class="btn left btn-secondary">vendre sur AVAHIS</button> <button class="btn hover left btn-secondary ">vendre sur AVAHIS</button>  <button class="btn active left btn-secondary">vendre sur AVAHIS</button> 

							
						</div>
						</div>
						
						<div class="slide">
						<div style="padding-top: 5px;">
							
								<p class="text-center xlarge bold">Les bouton avec icones et autres styles !</p>
								<span>Icone</span><button class="btn left btn-primary btn-ico-right">vendre sur AVAHIS <i class="fa fa-lg fa-shopping-cart"></i></button> <button class="btn left btn-primary btn-ico-left"><i class="fa fa-lg fa-lock"></i> vendre sur AVAHIS</button> <button class="btn left btn-secondary btn-ico-right">0692 811 945 <i class="fa fa-fw fa-lg fa-phone"></i></button> <button class="btn left btn-ico-right">Vendre sur AVAHIS <i class="fa fa-fw fa-lg fa-truck"></i></button><br><br><br><br>
								<span>Large</span><button class="btn left btn-large">vendre sur AVAHIS</button><button class="btn left btn-primary btn-large">vendre sur AVAHIS</button><button class="btn left btn-secondary btn-large">vendre sur AVAHIS</button><br><br><br><br><br>
								<span>Petit</span><button class="btn left btn-small">vendre sur AVAHIS</button><button class="btn left btn-primary btn-small">vendre sur AVAHIS</button><button class="btn left btn-secondary btn-small">vendre sur AVAHIS</button>
							
						</div>
						</div>
						
					<div class="slide">
						<div>
							<p class="left content-img"><img src="img/slide1.png"></p>
							<div class="content-slide left">
								<p class="text-center xlarge bold">Vendre sur Internet, c'est facile avec Avahis !</p>
								<p class="blue italic text-center large">"Depuis que je vend sur internet, c'est comme si j'avais une boutique à tous les coins de rue de l'île ! "</p>
								<p class="bold">Emilie a sauté le pas en mars 2014: elle a crée son compte professionnel sur Avahis, et mis en vente les objets de son magasin Des bulles pour bébé à Saint-Pierre.<br>
								<span class="bold large">Vous aussi,</span> <a href="#" class="blue large underline">vendez sur Avahis.com</a> !</p>
								<button class="btn btn-secondary right">vendre sur AVAHIS</button>
								
							</div>
						</div>
						</div>
						
						
						<div class="slide">
						<div>
							<div class="content-slide" style="width: 90%; margin: 0 auto;">
								<p class="text-center xlarge bold">c'est facile avec Avahis !</p>
								<p class="blue italic text-center large">"Depuis que je vend sur internet, c'est comme si j'avais une boutique à tous les coins de rue de l'île ! "</p>
								<p class="bold">Emilie a sauté le pas en mars 2014<br>
								<span class="bold large">Vous aussi,</span> <a href="#" class="blue large underline">vendez sur Avahis.com</a> !</p>
								<button class="btn btn-secondary right">vendre sur AVAHIS</button>
								
							</div>
							
						</div>
						</div>
					
					
				</div>
				</div> 
					<script type="text/javascript">
$(document).ready(function() {
      $('#slider-home .slides').carouFredSel({
	  items		  : {
		visible	: 1
	  },
	  pagination : {
	  container : "#slider-home .pag ul",
	  anchorBuilder : false
	  },
	  height: 230,
	  direction		: "left",
	  responsive : true,
        scroll : {
            items           : 1,
            duration        : 1000,                         
            pauseOnHover    : true,
			fx 				: "cover-fade",
			direction		: "left"
        }
    });
	 });
	</script>
				<!-- Fin #slider-home -->
				
			</div>
		</div>
		
		<!-- Begin #addtionnal -->
		<div class="row-fluid border-top border-bottom bg-light-gray" id="additional">
		<div class="well">
			<div class="span4"><a href="#"><img src="img/promo1.jpg"><div class="badge bg-orange badge-bottom"><span>43 €</span><span>A PARTIR<br>DE</span></div></a></div>
			<div class="span4"><a href="#"><img src="img/promo2.jpg"><div class="badge"><span>143 €</span><span>A PARTIR<br>DE</span></div></a></div>
			<div class="span4"><a href="#"><img src="img/promo3.jpg"><div class="badge bg-green badge-right"><span>943 €</span><span>A PARTIR<br>DE</span></div></a></div>
		</div>
		</div>
		<!-- End #additionnal -->
		
<div class="well">		
		<div class="row-fluid">
		<!-- Begin blog -->
			<div class="span4">
				<div class="bloc-list bloc border">
					<div class="top-bar"><span class="large uppercase">En direct du <span class="bold">blog</span></span><a href="#" class="blue xsmall underline">> voir tous les articles</a></div>
					<ul>
						<li><a href="#"><img src="img/blog1.jpg" class="border" ></a><p><a class="bold" href="#">Les meilleurs ateliers-créateurs de l'île de la Réunion !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">15/03</span></p></li>
						<li><a href="#"><img src="img/blog2.jpg" class="border" ></a><p><a class="bold" href="#">Saint-Valentin 2014: les tendances pour elle et lui !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">14/03</span></p></li>
						<li><a href="#"><img src="img/blog1.jpg" class="border" ></a><p><a class="bold" href="#">C’est l’été ! Collection plage et maillots de bain 2014 - test sur 3 grandes lignes</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">14/03</span></p></li>
						<li><a href="#"><img src="img/blog1.jpg" class="border" ></a><p><a class="bold" href="#">Les meilleurs ateliers-créateurs de l’île de la Réunion !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">13/03</span></p></li>
						<li><a href="#"><img src="img/blog2.jpg" class="border" ></a><p><a class="bold" href="#">Saint-Valentin 2014: les tendances pour elle et lui !</a></p><p><span class="small gray">Mode et vêtement</span><span class="small gray right">10/03</span></p></li>
						
					</ul>
		
			</div>
		
		
		</div>
		<!-- Fin blog -->
		
		<!-- Begin bloc commerçant -->
		<div class="span8">
		<!--
		<div class="univers row-fluid bloc bloc-grid">
		<div class="top-bar uppercase"><span class="xlarge"><span class="bold">les commerçants</span> du mois</span><a href="#" class="xsmall" style="text-decoration: underline;">voir tous les commerçants</a></div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Univers sur 2 lignes</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			<div class="box span2">
				<article>
					<h3>Jeux & jouets</h3>
					<p><img src="http://www.avahis.com/media/catalog/category/_hightech_1.jpg" alt="img"></p>
					<ul>
						<li><a href="#">Jeux en bois<a>, <a href="#">Jeux de société<a>, <a href="#">Jeux de carte<a></li>
						<li><a href="#">Déguisements<a></li>
					</ul>
					<a href="#">> Jeux et Jouets</a>
				</article>
			</div>
			
		</div>	-->
		<div class=" bloc bloc-grid border-top bloc-width-title">
		<div class="top-bar uppercase"><span class="xlarge bg-white"><span class="bold">les commerçants</span> du mois</span><a href="#" class="xsmall underline">voir tous les commerçants</a></div>
			<div class="row-fluid">
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun</a></p>
			</div>
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg" height="200" width="200"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun sur 3 lignes plus grand encore et un de plus</a></p>
			</div>
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun</a></p>
			</div>
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a></p>
				<p><a href="#" class="bold">Phytorun</a></p>
			</div>
		</div>
		<div class="row-fluid">
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun</a></p>
			</div>
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun</a></p>
			</div>
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun</a></p>
			</div>
			<div class="box span3">
				<a href="#"><img src="img/commercants/phytorun.jpg" height="200" width="200"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="bold">Phytorun sur 3 lignes plus grand encore et un de plus</a></p>
			</div>
		</div>	
	
		</div>
		</div>
		<!-- Fin bloc commerçant -->

		
		</div>
		
		
	<div class="row-fluid visible-desktop">
	<!-- Begin newsletter -->
	<div class="span4">
				<div class="newsletter bloc border">
					<div class="top-bar top-bar-center"><span class="bold">La newsletter</span> d'Avahis.com</div>
					<div class="content-bloc">
					<p>Restez informé des promotions et des actualités d’Avahis.com en vous inscrivant à notre newsletter :</p>
					<div>
						<form>
							<input type="text" placeholder="entrez votre adresse email">
							<button class="btn btn-primary">inscription</button>
						</form>
					</div>
					</div>		
		</div>
	</div>
	<!-- Fin newsletter -->
	
	<!-- Begin serviceBar -->
	<div class="span8">
				<div class="serviceBar bloc border">
					<div class="top-bar top-bar-center bg-green"><span class="bold white uppercase">Payez en 3 fois à partir de 100 € d’achats</span><a href="#" class="underline xsmall white">> voir toutes les conditions</a></div>
					<div class="row-fluid no-margin">
						<div>
						<p class="uppercase bold small">Paiement sécurisé</p>
							<p><i class="fa fa-lock fa-4x"></i></p>
							<div>
							
							<p>A partir de 48h en express, du commerçant à votre domicile</p>
							<a href="#" class="xsmall underline bold">En savoir plus</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">Paiement en 3 fois</p>
							<p><i class="fa fa-credit-card fa-3x"></i></p>
							<div>							
							<p>Service à partir de 100 € d’achat</p>
							<a href="#" class="xsmall underline bold">voir le détail des conditions</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">livraison facile et rapide</p>
							<p><i class="fa fa-truck fa-3x"></i></p>
							<div>
							
							<p>A partir de 48h en express, du commerçant à votre domicile</p>
							<a href="#" class="xsmall underline bold">En savoir plus</a>
							</div>
						</div>
					</div>
					<div class="bottom-bar bottom-bar-center"><span class="small">Contactez notre service client au</span> <span class="large bold">0693 911 859</span><span class="small"> du lundi à vendredi de 9h à 18h.</span></div>
		</div>
	</div>
	<!-- Fin serviceBar -->	
		
	</div>
	
	</div>
	
	
	<!-- Begin bloc promotion -->
	<div class="row-fluid bloc bloc-grid promotion">
		<div class="span12 content-bloc ">
			<div class="top-bar top-bar "><span class="uppercase">chutes de prix ! <span class="bold">toutes nos promotions</span></span></div>
			<div class="row-fluid  ">
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin.jpg"><span class="tag bg-green">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">949.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin2.jpg"><span class="tag tag-right tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue dark">- 15 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Fin bloc promotion -->
	
	<div class="well">
	<div class="row-fluid">
			<!-- Begin bloc top vente -->
			<div class="span4">
				<div class="bloc-list bloc border">
					<div class="top-bar"><span class="uppercase"><span class="bold">Le top</span> des ventes</span></div>
					<ul>
						<li><a href="#"><img src="img/product/product1.jpg"></a><p><span class="xlarge bold">1. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/product1.jpg"></a><p><span class="xlarge bold">2. </span><a href="#" class="bold blue">Le nom du jouet détaillé</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/vin.jpg"></a><p><span class="xlarge bold">3. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/vin2.jpg"></a><p><span class="xlarge bold">4. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les </a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						<li><a href="#"><img src="img/product/product1.jpg"></a><p><span class="xlarge bold">5. </span><a href="#" class="bold blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes</a></p><p class="text-right"><span class="small gray">à partir de </span><span class="orange xlarge bold">49.00 €</span></p></li>
						
					</ul>
		
		</div>
		
		</div>
		<!-- Fin bloc top vente -->
		
		<!-- Begin bloc univers -->
		<div class="span8">
		<div class=" bloc bloc-grid">
		
			<div class="row-fluid border-bottom">
			<div class="box span3">
				<a href="#" class="large title">Informatique</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">High-tech</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Tout pour la maison</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Auto / moto</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
		</div>
		
		<div class="row-fluid border-bottom" style="padding-top: 15px;">
			<div class="box span3">
				<a href="#" class="large title">Parfums, Beauté et Santé</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Bricolage et jardinage</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Bébé et puériculture</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
			<div class="box span3">
				<a href="#" class="large title">Alimentaire</a>
				<a href="#"><img src="img/product/product1.jpg"></a>
				<p><a href="#" class="blue bold small underline ">bien-être,</a> <a href="#" class="blue bold small underline "> alimentaire, </a> <a href="#" class="blue bold small underline ">santé, </a> <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
				<p><a href="#" class="small bold">> Tous informatique</a></p>
			</div>
		</div>	
	
		</div>
		</div>
		<!-- Fin bloc univers -->
		
		</div>
	
	
		<!-- Begin bloc sous-univers 1 -->
		<div class="row-fluid  border-bottom bloc-grid bloc-small" style=" padding-bottom:10px;">
	
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>	
		</div>
		<!-- Fin bloc sous-univers 1 -->
	
		<!-- Begin bloc sous-univers 2 -->
		<div class="row-fluid bloc-grid bloc-small">
	
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>
			<div class="box span2">
				<a href="#" class="title">Bureau et papeterie</a>
				<p><a href="#" class="bold small">bien-être,</a> <a href="#" class="bold small"> alimentaire, </a> <a href="#" class="bold small">santé, </a> <a href="#" class="bold small">huiles essentielles</a></p>
				<p><a href="#" class="small bold blue underline ">> Tous Bureau et papeterie</a></p>
			</div>	
		</div>
		<!-- Fin bloc sous-univers 2  -->
		
		
		
		
		<div class="row-fluid visible-desktop">
		
		<!-- Begin slider partner -->
		<div class="span8">
			<div class="bloc bloc-grid border bloc-width-title partner">
			<div class="top-bar"><span class="uppercase xlarge bold bg-white">nos partenaires</span></div>
			<div id="slider-partner">
				<div class="pag">
					<ul>
						<li><a href="#"><i class="fa fa-circle"></i></a></li>
						<li><a href="#"><i class="fa fa-circle"></i></a></li>
						<li><a href="#"><i class="fa fa-circle"></i></a></li>
					</ul>
				</div>
				<div class="slides">
			<div class="slide content-bloc">
				<div class="span5">
					<p class="italic text-center myriad-pro" style="margin-top: 20px;"><span class="xlarge">« J’ai eu le plaisir d’accompagner l’aventure Avahis.com depuis fin 2012.<br>
					Ensemble, nous proposons de nouveaux services, plus rapides, plus sécurisés, qui rapprochent les commerçants  et les réunionnais. »</span></p>
				</div>
				<div class="span4">
				<p class="text-center uppercase myriad-pro"><span class="bold large-title">Franz Raybois</span><br> <span class="large">directeur général</span> <br><span class="xlarge bold">chronopost Réunion</span></p>
				<p class="text-center"><img src="img/partenaire/chronopost.jpg"></p>
				</div>
				<div class="span3">
				<p class="text-center"><img src="img/partenaire/raybois.jpg" class="circle" style="width: 100%; height: auto;"></p>
				</div>
			</div>
			
						<div class="slide content-bloc">
				<div class="span5">
					<p class="italic text-center myriad-pro" style="margin-top: 20px;"><span class="xlarge">« J’ai eu le plaisir d’accompagner l’aventure Avahis.com depuis fin 2012.<br>
					Ensemble, nous proposons de nouveaux services, plus rapides, plus sécurisés, qui rapprochent les commerçants  et les réunionnais. »</span></p>
				</div>
				<div class="span4">
				<p class="text-center uppercase xlarge myriad-pro"><span class="bold">Rémi Voluer</span><br> <span class="small">directeur associé</span> <br><span class="small bold">Avahis Réunion</span></p>
				<p class="text-center"><img src="img/logo.png"></p>
				</div>
				<div class="span3">
				<p class="text-center"><img src="http://silicon-village.fr/images/remi.png" class="circle" style="width: 100%; height: auto;"></p>
				</div>
			</div>

			<div class="slide content-bloc">
				<div class="span5">
					<p class="italic text-center myriad-pro" style="margin-top: 20px;"><span class="xlarge">« J’ai eu le plaisir d’accompagner l’aventure Avahis.com depuis fin 2012.<br>
					Ensemble, nous proposons de nouveaux services, plus rapides, plus sécurisés, qui rapprochent les commerçants  et les réunionnais. »</span></p>
				</div>
				<div class="span4">
				<p class="text-center uppercase large myriad-pro"><span class="bold">Shanti Meralli-Ballou</span><br> <span class="small">Président Directeur Général</span> <br><span class="small bold">Avahis Réunion</span></p>
				<p class="text-center"><img src="img/logo.png"></p>
				</div>
				<div class="span3">
				<p class="text-center"><img src="http://silicon-village.fr/images/shanti.png" class="circle" style="width: 100%; height: auto;"></p>
				</div>
			</div>			
			
			</div>
			</div>
				<script type="text/javascript">
$(document).ready(function() {	
	 $('#slider-partner .slides').carouFredSel({
	 items		  : {
		visible	: 1
	  },
	  pagination : {
	  container : "#slider-partner .pag ul",
	  anchorBuilder : false
	  },
	  height: 220,
	  direction		: "left",
	  responsive : true,
        scroll : {
            items           : 1,
            duration        : 1000,                         
            pauseOnHover    : true,
			fx 				: "cover-fade",
			direction		: "left"
        }                   
    });
	 });
	</script>
			
			<div class="bottom-bar bg-smoke"><span><img src="img/partenaires.png" style="width: 100%; height"></span> </div> </div>
			
			
		</div>
		<!-- Fin slider partner -->
		
		<!-- Begin bloc facebook -->
		<div class="span4">
		
			<div class="fb-like-box fb_iframe_widget" data-href="http://www.facebook.com/Avahis" data-width="300" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=375088622565841&amp;color_scheme=light&amp;header=true&amp;height=350&amp;href=http%3A%2F%2Fwww.facebook.com%2FAvahis&amp;locale=fr_FR&amp;sdk=joey&amp;show_border=true&amp;show_faces=true&amp;stream=false&amp;width=300"><span style="vertical-align: bottom; width: 300px; height: 350px;"><iframe name="f67fb8c4" width="300px" height="350px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:like_box Facebook Social Plugin" src="http://www.facebook.com/plugins/like_box.php?app_id=375088622565841&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FLEdxGgtB9cN.js%3Fversion%3D40%23cb%3Dff5f7de48%26domain%3Dwww.avahis.com%26origin%3Dhttp%253A%252F%252Fwww.avahis.com%252Ff14a5aa604%26relation%3Dparent.parent&amp;color_scheme=light&amp;header=true&amp;height=350&amp;href=http%3A%2F%2Fwww.facebook.com%2FAvahis&amp;locale=fr_FR&amp;sdk=joey&amp;show_border=true&amp;show_faces=true&amp;stream=false&amp;width=300" class="" style="border: none; visibility: visible; width: 300px; height: 350px;"></iframe></span></div>
		</div>
		<!-- Fin bloc facebook -->
		
		</div>
		
	
		
	</div>
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>
