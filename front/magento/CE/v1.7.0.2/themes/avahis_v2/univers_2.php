<?php
include "lib/config.php";
$page = "univers_2";
?>

<!DOCTYPE html>
<html lang="fr">

<?php include "php/head.php"; ?>

<body id="<?php echo $page; ?>">

<div id="container">

<?php include 'php/headline.php'; ?>

	
<div id="wrapper">

<?php include 'php/header.php'; ?>


	<?php include 'php/nav.php'; ?>	





<section class="content">
	<div class="contenu">
	
	<div class="row-fluid">
	
	<!-- Begin Menu Secondaire -->
		<div class="span3">
			<div class="bloc secondary">
				<header><span class="xsmall"><i class="fa fa-chevron-left fa-fw"></i></span> <a href="#"><span class="title">Mode et vêtements <br><span class="xsmall">retour</span></span></a> <span class="right hidden-desktop reduct-list"><i class="fa fa-2x fa-list"></i></span></header>
					<ul>
						<li class="niv-sup selected"><a href="#">Femmes</a>
							<ul>
								<li><a href="#">Top sans manches (49)</a></li>
								<li><a href="#">Top avec manches (4)</a></li>
								<li><a href="#">Pantalon (152)</a></li>
								<li><a href="#">Top sans manches (49)</a></li>
								<li><a href="#">Top avec manches (4)</a></li>
								<li><a href="#">Pantalon (152)</a></li>
								<li><a href="#">Top sans manches (49)</a></li>
								<li><a href="#">Top avec manches (4)</a></li>
								<li><a href="#">Pantalon (152)</a></li>
								<li><a href="#">Top sans manches (49)</a></li>
								<li><a href="#">Top avec manches (4)</a></li>
								<li><a href="#">Pantalon (152)</a></li>
							</ul>
						</li>
						<li class="niv-sup"><a href="#">hommes</a></li>
						<li class="niv-sup"><a href="#">Filles</a></li>
						<li class="niv-sup"><a href="#">Garçon</a></li>

					</ul>
			</div>
			<!-- End Menu Secondaire -->
			
			<!-- Begin Menu liveBlog -->
			<div class="bloc border liveBlog">
				<div class="top-bar text-center"><span class="orange uppercase bold">En direct du blog</span></div>
				<div class="content-bloc bg-light-gray">
					<ul class="border-bottom">
						<li><a href="">Notre sélection d'appareils photo numériques</a></li>
						<li><a href="">Tout l'équipement du photographe semi-professionnel</a></li>
					</ul>
					
					<ul>
						<li><a href="" class="bold">Toutes mes promotions</a></li>
						<li><a href="" class="bold">Toutes les nouveautés</a></li>
					</ul>
					
				</div>
			
			</div>
			<!-- End Menu liveBlog -->
			
			
		</div>
		
		
	<div class="span9 well-right">
		
			<h1>Photo et caméscopes</h1>
			
			<!-- Begin bloc rubric -->
			<div class="row-fluid rubric">
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
					<p><a href="#" class="blue bold small underline ">lien 1</a>, <a href="#" class="blue bold small underline ">lien 2 </a>, <a href="#" class="blue bold small underline ">lien 3 </a>, <a href="#" class="blue bold small underline ">lien 4</a></p>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
					<p><a href="#" class="blue bold small underline ">lien 1</a>, <a href="#" class="blue bold small underline ">lien 2 </a>, <a href="#" class="blue bold small underline ">lien 3 </a>, <a href="#" class="blue bold small underline ">lien 4</a></p>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
					<p><a href="#" class="blue bold small underline ">lien 1</a>, <a href="#" class="blue bold small underline ">lien 2 </a>, <a href="#" class="blue bold small underline ">lien 3 </a>, <a href="#" class="blue bold small underline ">lien 4</a></p>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
					<p><a href="#" class="blue bold small underline ">lien 1</a>, <a href="#" class="blue bold small underline ">lien 2 </a>, <a href="#" class="blue bold small underline ">lien 3 </a>, <a href="#" class="blue bold small underline ">lien 4</a></p>
				</div>
				<div class="box span3">			
					<a href="#" class="border"><img src="img/product/appareil-photo.jpg" ></a>
					<a href="#" class="title">Compacts et bridge</a>
					<p><a href="#" class="blue bold small underline ">lien 1</a>, <a href="#" class="blue bold small underline ">lien 2 </a>, <a href="#" class="blue bold small underline ">lien 3 </a>, <a href="#" class="blue bold small underline ">lien 4</a></p>
				</div>
			
			
			</div>
		<!-- End bloc rubric -->
		
		<!-- Begin Bloc soldes -->
			<div class="row-fluid sales visible-desktop">
				<div class="span9"><img src="img/soldes.jpg" alt="soldes" width="100%" height="auto"></div>
				<div class="span3" style="margin-top: 60px;">
					<p class="strong">Voir toutes les soldes</p>
					<ul>
						<li>> <a href="#" class="underline xsmall">soldes Femmes</a></li>
						<li>> <a href="#" class="underline xsmall">soldes Hommes</a></li>
						<li>> <a href="#" class="underline xsmall">soldes Bébé (-3 ans)</a></li>
						<li>> <a href="#" class="underline xsmall">soldes Enfants</a></li>
					</ul>
				</div>
			</div>
			<!-- Fin Bloc soldes -->	
		
	<!-- Begin #addtionnal -->
		<div class="row-fluid" id="additional">
			<div class="span4"><a href="#"><img src="img/promo1.jpg"></a></div>
			<div class="span4"><a href="#"><img src="img/promo2.jpg"></a></div>
			<div class="span4"><a href="#"><img src="img/promo3.jpg"></a></div>
		</div>
		<!-- End #additionnal -->
		
		
		
		
		
		
		
		
		
		
	<!-- Begin serviceBar -->
<!--
				<div class="serviceBar bloc border visible-desktop">
					<div class="top-bar top-bar-center bg-dark"><span class="xlarge bold"><span class="uppercase white">Payez en 3 fois sans frais </span><span class="white">dès 100 € d’achats </span></span><a href="#" class="underline xsmall white">> voir toutes les conditions</a></div>
					<div class="row-fluid no-margin">
						<div>
						<p class="uppercase bold small">Livraison facile et rapide</p>
							<p><i class="fa fa-truck fa-3x dark"></i></p>
							<div>
							
							<p>A partir de 48h en express, du commerçant à votre domicile</p>
							<a href="#" class="xsmall underline bold">En savoir plus</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">VOTRE Service client !</p>
							<p><i class="fa fa-phone-square fa-3x dark"></i></p>
							<div>							
							<p>Contactez nous par téléphone ou par mail, nous vous répondrons au plus vite</p>
							<a href="#" class="xsmall underline bold">Nous contacter</a>
							</div>
						</div>
						<div>
						<p class="uppercase bold small">satisfait ou remboursé</p>
							<p><i class="fa fa-mail-reply-all fa-3x dark"></i></p>
							<div>
							
							<p>Nos commerçants reprenent vos produits sous 15 jours.</p>
							<a href="#" class="xsmall underline bold">Les conditions de retour</a>
							</div>
						</div>
					</div>
					<div class="bottom-bar bottom-bar-center"><span class="small">service client du lundi au vendredi de 9h à 18h au </span> <span class="large bold">0693 911 859</span><span class="small"></span></div>
		</div> -->

	<!-- Fin serviceBar -->	
		
		</div>
	
	
	
	</div>
	
	
	
	
	<!-- Begin bloc promotion -->
	<div class="row-fluid bloc bloc-grid promotion">
		<div class="span12 content-bloc ">
			<div class="top-bar top-bar "><span class="uppercase">nos meilleurs ventes <span class="bold">appareil photo</span></span></div>
			<div class="row-fluid  ">
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin.jpg"><span class="tag bg-green">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">949.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin2.jpg"><span class="tag tag-right tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue dark">- 15 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Fin bloc promotion -->
	
	
	
	
	
	<div class="well">
	<div class="row-fluid">
		<div class="span8">
			<!-- Begin Bloc microboutique -->
			
				<div class="bloc bloc-grid">
				<div class="row-fluid">
					<div class="box span4 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span4 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>					

					<div class="box span4 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
															
				</div>
				
								<div class="row-fluid">
					<div class="box span4 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
					
					<div class="box span4 border">
						<a href="#" class="bold xlarge myriad-pro">Phytorun</a>
						<p><a href="#" class="blue bold small underline ">bien-être</a>, <a href="#" class="blue bold small underline "> alimentaire</a>, <a href="#" class="blue bold small underline ">santé</a>, <a href="#" class="blue bold small underline ">huiles essentielles</a></p>
						<a href="#"><img src="img/commercants/phytorun.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Phytorun</a></div>				
					</div>					

					<div class="box span4 border">
						<a href="#" class="bold xlarge myriad-pro">Ckomca</a>
						<p><a href="#" class="blue bold small underline ">bijoux</a>, <a href="#" class="blue bold small underline "> accessoire de mode</a></p>
						<a href="#"><img src="img/commercants/ckomca.jpg"></a>
						<div class="bottom-bar text-center"><a href="#" class="small">Ckomca</a></div>				
					</div>
															
				</div>
				
			</div>
			<!-- Fin Bloc microboutique -->
		</div>
		
		<div class="span4">
		
		<!-- Begin bloc marque -->
		<div class="row-fluid">
				<div class="bloc-grid bloc brand visible-desktop">
					<div class="top-bar text-right"><span class="xlarge uppercase bg-green dark">Toutes <span class="bold dark">nos marques</span></span></div>
					<div class="content-bloc bg-light-gray">
					<ul>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/nike.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hm.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hugoboss.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/jules.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/gucci.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/lacoste.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/nike.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hm.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hugoboss.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/jules.jpg"></a></li>						
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/apple.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/celio.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hm.jpg"></a></li>
						<li><a href="#"><img src="http://www.distrijob.fr/logos/imgnew/hugoboss.jpg"></a></li>
						</ul>
					</div>
			</div>
			</div>
		<!-- Fin bloc marque -->
	
		
		</div>
	
	</div>
	
	</div>
	
	
	<!-- Begin bloc promotion -->
	<div class="row-fluid bloc bloc-grid promotion">
		<div class="span12 content-bloc ">
			<div class="top-bar top-bar "><span class="uppercase">nos meilleures ventes <span class="bold">caméscopes</span></span></div>
			<div class="row-fluid  ">
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">549.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-right">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin.jpg"><span class="tag bg-green">- 5 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">949.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/vin2.jpg"><span class="tag tag-right tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag bg-blue dark">- 15 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
				<div class="span2 box">
					<a href="#"><img src="img/product/product1.jpg"><span class="tag tag-bottom">- 25 %</span></a>
					<p class="text-left"><a href="#" class="blue">Le nom du jouet détaillé sur 2 lignes pour faire les test...</a></p>
					<p class="bold right"><span class="xsmall">à partir de </span><span class="xlarge pink">49.00 €</span></p>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Fin bloc promotion -->
	
	
	
</div>
</section>


</div>

<!-- Begin footer -->
<?php include 'php/footer.php'; ?>
<!-- Fin footer -->


</div>


</body>
</html>	