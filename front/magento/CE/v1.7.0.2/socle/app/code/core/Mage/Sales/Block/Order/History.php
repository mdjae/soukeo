<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales order history block
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Mage_Sales_Block_Order_History extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('sales/order/history.phtml');

        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->setOrder('created_at', 'desc')
        ;

        $this->setOrders($orders);

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getViewUrl($order)
    {
        return $this->getUrl('*/*/view', array('order_id' => $order->getId()));
    }

    public function getTrackUrl($order)
    {
        return $this->getUrl('*/*/track', array('order_id' => $order->getId()));
    }

    public function getReorderUrl($order)
    {
        return $this->getUrl('*/*/reorder', array('order_id' => $order->getId()));
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
    
        public function commandeCloturable($idCommande) 
    {
        //Récupération des objets bons de commande pour vérification du statut
        $orderPos = Mage::getModel('udpo/po')->getCollection();
        $orderPos->addAttributeToFilter('order_id', $idCommande);
        //Si un ou plusieurs PO ont été générés pour la commande
        if (is_array($orderPos->getData()) && count($orderPos->getData()) > 0) {
            //On  les parcourre pour vérifier leur statut
            foreach ($orderPos as $orderPo) {
                //Si le statut du PO n'est pas "Expédié", la commande n'est pas clôturable    
                if ($orderPo->getData('udropship_status') != 1) {
                    return false;
                    break;    
                }
            }
        //Sinon, la commande n'est pas clôturable, car les PO n'ont pas encore été générés
        } else {
            return false; 
            break;
        }
        
        //Récupération des objets bons de livraison correspondants pour vérification du statut
        $orderShipments = Mage::getModel('sales/order_shipment')->getCollection();
        $orderShipments->addAttributeToFilter('order_id', $idCommande);
        //Si un ou plusieurs bons de livraison ont été générés
        if (is_array($orderShipments->getData()) && count($orderShipments->getData()) > 0) {
            //On les parcourre pour vérifier leur statut
            foreach($orderShipments as $orderShipment) {
                //Si le statut n'est pas expédié, la commande n'est pas clôturable    
                if ($orderShipment->getData('udropship_status') != 1) {
                    return false;
                    break;    
                }
            }
        //Sinon, la commande n'est pas clôturable, car les bons de livraison n'ont pas encore été générés
        } else {
            return false;
            break;
        }
        
        //Si on arrive jusqu'ici, c'est que tous les bons de commande et de livraison ont été générés et qu'ils ont tous le statut expédié,  la commande est clôturable
        return true;
    }
    
}
