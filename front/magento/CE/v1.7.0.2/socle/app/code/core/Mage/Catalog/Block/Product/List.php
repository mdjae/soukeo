<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();
            
            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }
        return $this->_productCollection;
    }

    //Fonction personnalisée permettant de récupérer la collection constituée de l'ensemble des produits commercialisés par le vendeur identifié
    //Comprenant les fiches produit dont il est le Dropship Vendor et les fiches produit dont Soukéo est le Dropship Vendor
    public function getVendorProductCollection()
    {
        $v = Mage::getSingleton('udropship/session')->getVendor();
        $r = Mage::app()->getRequest();
        $res = Mage::getSingleton('core/resource');
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToFilter('entity_id', array('in'=>array_keys($v->getAssociatedProducts())));
        $conn = $collection->getConnection();
        if (Mage::helper('udropship')->isUdmultiAvailable()) {
            $collection->getSelect()->joinLeft(
                array('uvp' => $res->getTableName('udropship/vendor_product')),
                $conn->quoteInto('uvp.product_id=e.entity_id AND uvp.vendor_id=?', $v->getId())
            );
        }
        $this->_vendorCollection = $collection;           
        return $this->_vendorCollection;        
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml()
    {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection)
    {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code)
    {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate()
    {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }
    
    //Fonction personnalisée permettant de construire le tableau contenant 
    // -> les ID des différentes catégories retrouvées dans la collection passée en paramètre
    // -> le décompte de produits pour chacun de ces ID
    public function listerCategories($collection)
    {
        //Récupération des données, constituées d'un tableau contenant lui même des tableaux pour chaque produit et les catégories qui lui sont associées
        $listeDesProduitsEtDeLeursCategories = $collection->getData();
        //Mise en place d'un tableau permettant de lister les différents ID des catégories rencontrés et d'incrémenter la quantité pour chaque produit correspondant à cet ID catégorie
        $listeCategories = array();
        //On parcourre le 1er niveau du tableau des données pour récupérer chaque tableau correspondant à une association IDProduit / IDCatégorie
        foreach ($listeDesProduitsEtDeLeursCategories as $tableauDesDonnees) {
            //Et on récupère pour chaque tableau l'ID de la catégorie
            $idCategorie = $tableauDesDonnees['category_id'];
            $listeCategories[$idCategorie] += 1;
         }
        //Le tableau comporte maintenant la liste de tous les Id catégories rencontrés dans la collection constituant la recherche
        //Et le compte du nombre de produits correspondant pour chaque catégorie
        //Il faut maintenant récupérer pour chaque ID catégorie le libellé de la catégorie correspondant
        //On parcourre le tableau contenant les ID des catégories et quantités associées
        foreach ($listeCategories as $idCat => $nbProd) {
            //On récupère l'objet catégorie correspondant à l'ID de la catégorie en cours en chargeant la collection des catégories filtrée sur l'ID de la catégorie en cours
           $categorie = Mage::getModel('catalog/category')->getCollection()->addAttributetoSelect('name')->addAttributeToFilter('entity_id', $idCat);
           //Puis on extrait le libellé de la catégorie 
            $categoryDataTable = $categorie->getData();
            $categoryData = $categoryDataTable[0];
            $nomCategorie = $categoryData['name'];
            //Et on met en place les données nécessaires dans le tableau, à savoir ID catégorie (clé du tableau associatif), libellé de la catégorie et nopmbre de produits correspondant
            $donneesCategories[$idCat] = array(   'nom' => $nomCategorie, 
                                                                           'qty'    => $nbProd );
        }
        return $donneesCategories;
    }

    //Fonction personnalisée permettant de récupérer la catégorie pour un produit à partir de son ID
    public function getCategoryForProduct($idProduit)
    {
        $res = Mage::getSingleton('core/resource');
        $collection =   Mage::getModel('catalog/category')->getCollection();
        //echo $collection->getSelect();
        $conn = $collection->getConnection();
        $collection->addAttributeToSelect(array('name', 'url_path'));
        $collection->getSelect()
                          ->join(   array('ccp' => $res->getTableName('catalog_category_product')),
                                       $conn->quoteInto('ccp.category_id=main_table.entity_id WHERE ccp.product_id=?', $idProduit)
                          );
        return $collection;
    }
    
        //Fonction personnalisée ajoutée par Magali le 25/11/2013 pour vérification : le produit est featured? à partir de son ID
    public function isFeatured($productID) {
        $featuredBlocks = Mage::getModel('awfeatured/blocks')->getCollection();
        foreach($featuredBlocks as $block) {
             if ($block->getData('block_id') == 'featured_products_by_category') {
                $featured = $block;
            }            
        }
        $listeProduits = $featured->getData('automation_data');
        $listeProduits = explode(',', $listeProduits);
        //La liste de produits se trouve mainetnant dans un tableau
        //Pour le premier produit, il faut retirer toutes les infos avant l'ID du produit, c'est à dire tout ce qui se trouve avant la dernière double quote
        $listeProduits[0] = substr(strrchr($listeProduits[0], '"'), 1);
        //Pour le dernier produit, il faut extraire toutes les infos après l'ID du produit, c'est à dire tout ce qui se trouve après la première occurence de " 
        $position = strpos($listeProduits[count($listeProduits)-1], '"');
        $listeProduits[count($listeProduits)-1] = substr($listeProduits[count($listeProduits)-1],  0, $position);
        foreach ($listeProduits as $produit) {
            if ($productID == $produit) {
                return true;
            }
        }
        return false;
    }
        
}
