<?php

class Unirgy_DropshipMicrositePro_Model_Registration extends Unirgy_DropshipMicrosite_Model_Registration
{
    protected $_regFields;
    public function getRegFields()
    {
        if (null === $this->_regFields) {
            $this->_regFields = array();
            $columnsConfig = Mage::getStoreConfig('udsignup/form/fieldsets');
            if (!is_array($columnsConfig)) {
                $columnsConfig = Mage::helper('udropship')->unserialize($columnsConfig);
                if (is_array($columnsConfig)) {
                foreach ($columnsConfig as $fsConfig) {
                if (is_array($fsConfig)) {
                    foreach (array('top_columns','bottom_columns','left_columns','right_columns') as $colKey) {
                    if (isset($fsConfig[$colKey]) && is_array($fsConfig[$colKey])) {
                        $requiredFields = (array)@$fsConfig['required_fields'];
                        foreach ($fsConfig[$colKey] as $fieldCode) {
                            $field = Mage::helper('udmspro/protected')->getRegistrationField($fieldCode);
                            if (!empty($field)) {
                                if (in_array($fieldCode, $requiredFields)) {
                                    $field['required'] = true;
                                } else {
                                    $field['required'] = false;
                                    if (!empty($field['class'])) {
                                        $field['class'] = str_replace('required-entry', '', $field['class']);
                                    }
                                }
                                $this->_regFields[$fieldCode] = $field;
                            }
                        }
                    }}
                }}}
            }
        }
        return $this->_regFields;
    }
    public function attachLabelVars()
    {
        foreach ($this->getRegFields() as $name=>$rf) {
            switch ($rf['type']) {
                case 'statement_po_type': case 'payout_po_status_type': case 'notify_lowstock':
                case 'select': case 'multiselect': case 'checkboxes':
                    $srcModel = $rf['source_model'];
                    $source = Mage::getSingleton($srcModel);
                    if (is_callable(array($source, 'setPath'))) {
                        $source->setPath(!empty($rf['source']) ? $rf['source'] : $name);
                    }
                    if ($rf['type']=='multiselect') {
                        $values = array_map('trim', explode(',', $this->getData($name)));
                    } else {
                        $values = $this->getData($name);
                    }
                    $values = array_filter((array)$values);
                    if (!empty($values) && is_callable(array($source, 'getOptionLabel'))) {
                        $lblValues = array();
                        foreach ($values as $value) {
                            $lblValues[] = $source->getOptionLabel($value);
                        }
                        $lblValues = implode(', ', $lblValues);
                        $this->setData($name.'_label', $lblValues);
                    }
                    break;
            }
        }
    }

    public function validate()
    {
        $hlp = Mage::helper('umicrosite');
        $dhlp = Mage::helper('udropship');
        extract($this->getData());

        $hasPasswordField = false;
        foreach ($this->getRegFields() as $rf) {
            if (!empty($rf['required'])
                && !$this->getData($rf['name'])
                && !in_array($rf['type'], array('image','file'))
                && !in_array($rf['name'], array('payout_paypal_email'))
            ) {
                Mage::throwException($hlp->__('Formulaire de demande d\'inscription incomplet'));
            }
            $hasPasswordField = $hasPasswordField || in_array($rf['name'], array('password_confirm','password'));
            if ($rf['name']=='password_confirm'
                && $this->getData('password') != $this->getData('password_confirm')
            ) {
                Mage::throwException($hlp->__('Les deux mots de passe saisis ne concordent pas.'));
            }
        }

        $this->setStreet(@$street1."\n".@$street2);
        $this->initPassword(@$password);
        $this->initUrlKey(@$url_key);
        $this->setRemoteIp($_SERVER['REMOTE_ADDR']);
        $this->setRegisteredAt(now());
        $this->setStoreId(Mage::app()->getStore()->getId());
        $dhlp->processCustomVars($this);
        $this->attachLabelVars();

        return $this;
    }
    public function initPassword($password=null)
    {
        if (empty($password)) {
            $password = $this->generatePassword();
        }
        $this->setPasswordEnc(Mage::helper('core')->encrypt($password));
        $this->setPasswordHash(Mage::helper('core')->getHash($password, 2));
        $this->unsPassword();
        return $this;
    }
    public function formatUrlKey($str)
    {
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', Mage::helper('catalog/product_url')->format($str));
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }
    public function initUrlKey($urlKey=null)
    {
        if (empty($urlKey)) {
            $urlKey = $this->formatUrlKey($this->getData('vendor_name'));
        }
        $this->setData('url_key', $urlKey);
        return $this;
    }
    protected function generatePassword()
    {
        return Mage::helper('udmspro')->processRandomPattern('[AN*6]');
    }

    protected function _beforeSave()
    {
        parent::_beforeSave();
        Mage::helper('udropship')->processCustomVars($this);
    }
}