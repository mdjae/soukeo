<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Model_MostViewed extends Mage_Core_Model_Abstract
{
	/**
	 * XML configuration paths
	 */
	const XML_PATH_NB_DAYS_EXPIRY		= 'ayalinemostviewed/general/nb_days_expiry';
	const XML_PATH_SIDEBAR_NB_DAYS 		= 'ayalinemostviewed/sidebar/nb_days';
	const XML_PATH_GENERAL_SHOW_OUT_OF_STOCK	=	'ayalinemostviewed/general/show_out_of_stock';

	protected function _construct() {
		$this->_init('ayalinemostviewed/mostViewed');
	}

	public function getNbDaysExpiry($storeId=null) {
		return Mage::getStoreConfig(self::XML_PATH_NB_DAYS_EXPIRY, $storeId);
	}

	public function cleanOld($storeIds=null) {
		if($storeIds instanceof Mage_Cron_Model_Schedule) {
			$storeIds = null;
		}
		
		if(is_null($storeIds)) {
			$storeIds = Mage::app()->getStores(true);
		} elseif(!is_array($storeIds)) {
			$storeIds = array($storeIds);
		}

		foreach($storeIds as $store) {
			$storeId = $store->getId();
				
			$date = new Zend_Date();
			$nbDaysExpiry = $this->getNbDaysExpiry($storeId);
			$date->sub($nbDaysExpiry, Zend_Date::DAY);
				
			$this->_getResource()->cleanOlderThan($storeId, $date->get('YYYY-MM-dd'));
		}
	}

	/**
	 * Increase most viewed count for the given parameters.
	 *
	 * @param int $productId
	 * @param int $storeId
	 * @param string $date
	 * @param int $nbViewed
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 *
	 * @return bool : true if updates
	 */
	public function increaseMostViewedCount($productId, $storeId, $date, $nbViewed=1, $zdFormat='YYYY-MM-dd HH:mm:ss') {
		$zd = new Zend_Date($date, $zdFormat);

		$session = Mage::getSingleton('customer/session');
		if(is_null($session->getData('ayalinemostviewed_informations'))) {
			$session->setData('ayalinemostviewed_informations', new Varien_Object());
		}
		$informations = $session->getData('ayalinemostviewed_informations');
		if(is_null($informations->getData('store_' . $storeId))) {
			$informations->setData('store_' . $storeId, new Varien_Object());
		}
		$storeInformations = $informations->getData('store_' . $storeId);

		if(is_null($storeInformations->getData('product_' . $productId))) {
			// To avoid multiple increases on the same product for one user
			$this->_getResource()->increaseMostViewedCount($productId, $storeId, $zd->get('YYYY-MM-dd'), $nbViewed);
			$storeInformations->setData('product_' . $productId, true);
		}
	}

	/**
	 * Decrease most viewed count for the given parameters.
	 * If no count if found for these parameters, do nothing.
	 *
	 * @param int $productId
	 * @param int $storeId
	 * @param string $date
	 * @param int $nbViewed
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 *
	 * @return bool : true if updates
	 */
	public function decreaseMostViewedCount($productId, $storeId, $date, $nbViewed, $zdFormat='YYYY-MM-dd HH:mm:ss') {
		$zd = new Zend_Date($date, $zdFormat);

		return $this->_getResource()->decreaseMostViewedCount($productId, $storeId, $zd->get('YYYY-MM-dd'), $nbViewed);
	}

	/**
	 *
	 * Prepare collection with most_viewed_count attribute
	 *
	 * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
	 * @param boolean $strict : get also the products already viewed or not
	 * @param int $nbDays : number of days (inclusive)
	 * @param int $store : store id (default : current store)
	 * @param string $alias : the name of the alias
	 *
	 * @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 */
	public function addToProductCollection($collection, $strict = false, $nbDays = null, $store = null, $alias = 'most_viewed'){
		/** Manage strict Param **/
		if($strict === true){
			$join = 'joinInner';
		}else{
			$join = 'joinLeft';
		}

		/** Manage store Param **/
		if(!$store){
			$store = Mage::app()->getStore()->getId();
		}

		/** Manage nbDays Param **/
		if($nbDays == null){
			$nbDays = Mage::getStoreConfig(self::XML_PATH_SIDEBAR_NB_DAYS, $store);
		}

		$joinConditions = array('e.entity_id = amvi.product_id',
								"amvi.store_id = '".$store."'",
								"amvi.day = '".$nbDays."'");

		$collection
			->getSelect()
			->$join(
				array('amvi' => $collection->getTable('ayalinemostviewed/most_viewed_index')),
				implode(' AND ', $joinConditions),
				array($alias => 'amvi.nb_viewed')
			)
		;

		if(!Mage::getStoreConfig(self::XML_PATH_GENERAL_SHOW_OUT_OF_STOCK)) {
			$collection->getSelect()
				->joinInner(
					array('ciss' => $collection->getTable('cataloginventory/stock_status')),
					"ciss.product_id = e.entity_id AND ciss.website_id = '".Mage::app()->getStore($store)->getWebsiteId()."'",
					array()
				)
				->where('ciss.stock_status = ?', Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK)
			;
		}

		return $collection;
	}
}
