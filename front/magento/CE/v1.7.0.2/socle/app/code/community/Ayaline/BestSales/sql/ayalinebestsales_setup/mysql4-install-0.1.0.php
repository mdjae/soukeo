<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('ayalinebestsales/best_sales')};
CREATE TABLE {$this->getTable('ayalinebestsales/best_sales')} (
  `best_sales_id` int(10) unsigned NOT NULL auto_increment,
  `product_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `sales_date` date NOT NULL default '0000-00-00',
  `sales_count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`best_sales_id`),
  UNIQUE KEY `UK_AYALINE_BESTSALES` (`product_id`,`store_id`,`sales_date`),
  CONSTRAINT `FK_AYALINE_BESTSALES_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES {$this->getTable('catalog/product')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AYALINE_BESTSALES_STORE` FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core_store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


");
