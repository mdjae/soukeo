<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_Adminhtml_System_Config_Source_ListMode
{
	public function toOptionArray() {
		return array(
			array('value'=>'grid', 'label'=>Mage::helper('ayalinebestsales')->__('Grid')),
			array('value'=>'list', 'label'=>Mage::helper('ayalinebestsales')->__('List')),
		);
	}
}
