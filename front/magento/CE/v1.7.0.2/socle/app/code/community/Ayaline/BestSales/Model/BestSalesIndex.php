<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_BestSalesIndex extends Mage_Core_Model_Abstract {

	protected function _construct() {
		$this->_init('ayalinebestsales/bestSalesIndex');
	}

	/**
	 * Création de la table d'index des produits les vendus
	 *
	 * @param array(Mage_Core_Model_Store) $stores
	 * @param bool $withDefault
	 */
	public function indexBestSales($stores = null, $withDefault = true) {
		if((is_array($stores) && empty($stores)) || is_null($stores) || $stores instanceof Mage_Cron_Model_Schedule) {
			$stores = Mage::app()->getStores();
		} elseif(!is_array($stores)) {
			$stores = array($stores);
		}

		try {
			$this->_getResource()->truncateIdxTable();

			foreach($stores as $_store) {
				if($_store instanceof Mage_Core_Model_Store) {
					$storeId = $_store->getStoreId();
				} else {
					$storeId = $_store;
				}
				try {
					$this->_getResource()->indexBestSales($storeId);
				} catch(Exception $e) {
					$notificationObject = new Varien_Object(array(
						'title'			=>	Mage::helper('ayalinebestsales')->__('An error occurred while indexing "%s" to the store %s', Mage::helper('ayalinebestsales')->__('Best sales'), $_store->getName()),
						'date'			=>	date('Y-m-d H:i:s'),
						'url'			=>	'#',
						'description'	=>	$e->getMessage().'<br />'.Mage::helper('ayalinecore')->__('See exception.log'),
					));
					Mage::helper('ayalinecore')->addCustomAdminNotification($notificationObject);
					Mage::logException($e);
				}
			}

			$this->_getResource()->renameTables();
				
		} catch(Exception $e) {
			$notificationObject = new Varien_Object(array(
				'title'			=>	Mage::helper('ayalinemostviewed')->__('An error occurred while indexing "%s"', Mage::helper('ayalinebestsales')->__('Best sales')),
				'date'			=>	date('Y-m-d H:i:s'),
				'url'			=>	'#',
				'description'	=>	$e->getMessage().'<br />'.Mage::helper('ayalinecore')->__('See exception.log'),
			));
			Mage::helper('ayalinecore')->addCustomAdminNotification($notificationObject);
			Mage::logException($e);
		}
	}

}