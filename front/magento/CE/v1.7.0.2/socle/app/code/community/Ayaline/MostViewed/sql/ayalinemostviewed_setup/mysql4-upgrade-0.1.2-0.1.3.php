<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ayalinemostviewed/most_viewed_index_idx')}` (
	`index_id`		INT(10)					NOT NULL	AUTO_INCREMENT ,
	`product_id`	INT(10)		UNSIGNED	NOT NULL ,
	`day`			SMALLINT(5)	UNSIGNED	NOT NULL ,
	`store_id`		SMALLINT(5)	UNSIGNED	NOT NULL ,
	`nb_viewed`		INT(10)		UNSIGNED	NOT NULL ,
	PRIMARY KEY (`index_id`) ,

	INDEX `AYALINE_MOST_VIEWED_INDEX_IDX_DAY_IDX` (`day` ASC) ,
	INDEX `AYALINE_MOST_VIEWED_INDEX_IDX_PRODUCT_ID_IDX` (`product_id` ASC) ,
	INDEX `AYALINE_MOST_VIEWED_INDEX_IDX_STORE_ID_IDX` (`store_id` ASC) ,

	UNIQUE INDEX `AYALINE_MOST_VIEWED_INDEX_IDX_UNIQUE_KEY` (`product_id` ASC, `day` ASC, `store_id` ASC) ,

	CONSTRAINT `FK_AYALINE_MOST_VIEWED_INDEX_IDX_PRODUCT_ID`
		FOREIGN KEY (`product_id` )
		REFERENCES `{$installer->getTable('catalog/product')}` (`entity_id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE,

	CONSTRAINT `FK_AYALINE_MOST_VIEWED_INDEX_IDX_STORE_ID`
		FOREIGN KEY (`store_id` )
		REFERENCES `{$installer->getTable('core/store')}` (`store_id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE)

ENGINE = InnoDB;

");

$installer->endSetup();
