<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPo
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */
 
class Unirgy_DropshipPo_Model_Pdf_PoItems_Default extends Mage_Sales_Model_Order_Pdf_Items_Abstract
{
    /**
     * Draw item line
     *
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $lines  = array();
        
        //POUR AFFICHAGHE DU PRIX TTC DANS LE PDF DE LA COMMANDE TELECHARGE DEPUIS LE BACK-OFFICE COMMERCANT
        //Le prix stocké au niveau de l'item est HT, il faut donc récupérer le prix TTC grâce à l'ID du vendeur et l'ID du produit dans la table udropship_vendor_product
        $vendorID = $_SESSION['vendorID'];
        $productID = $item->getData('product_id');
        $product = Mage::getModel('udropship/vendor_product')   ->getCollection()
                                                                                                        ->addFieldToFilter('vendor_id', $vendorID)
                                                                                                        ->addFieldToFilter('product_id', $productID);
        $productData = $product->getData();
        $productData = $productData[0];
        $prixTTC = $productData['vendor_price'];
        
       // draw Product name
        $lines[0] = array(array(
            'text' => Mage::helper('core/string')->str_split($item->getName(), 50, true, true),
            'feed' => 35,
            'font_size' => 8,        
        ));

        // draw SKU
        $lines[0][] = array(
            'text'  => Mage::helper('core/string')->str_split($this->getSku($item), 25),
            'feed'  => 245,
            'font_size' => 8,
        );

        // draw QTY
        $lines[0][] = array(
            'text'  => $item->getQty()*1,
            'feed'  => 475,
            'font_size' => 8,
        );

        // draw Price
        $lines[0][] = array(
            //'text'  => $order->getBaseCurrency()->formatTxt($item->getBaseCost()),
            'text'  => $order->getBaseCurrency()->formatTxt($prixTTC),
            'feed'  => 420,
            'font_size' => 8,
            'font'  => 'bold',
            'align' => 'right'
        );

        // draw Subtotal
        $lines[0][] = array(
            //'text'  => $order->getBaseCurrency()->formatTxt($item->getBaseCost()*$item->getQty()),
            'text'  => $order->getBaseCurrency()->formatTxt($prixTTC*$item->getQty()),
            'feed'  => 565,
            'font_size' => 8,
            'font'  => 'bold',
            'align' => 'right'
        );
        $_SESSION['totalTTC'] += $prixTTC*$item->getQty();

/*        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = array(
                    'text' => Mage::helper('core/string')->str_split(strip_tags($option['label']), 70, true, true),
                    'font' => 'italic',
                    'feed' => 35
                );

                if ($option['value']) {
                    $_printValue = isset($option['print_value']) ? $option['print_value'] : strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        $lines[][] = array(
                            'text' => Mage::helper('core/string')->str_split($value, 50, true, true),
                            'feed' => 40
                        );
                    }
                }
            }
        }
*/
        $lineBlock = array(
            'lines'  => $lines,
            'height' => 5
        );

        $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $this->setPage($page);
    }
}