<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("

ALTER TABLE `{$installer->getTable('ayalinemostviewed/most_viewed_index')}` DROP FOREIGN KEY `FK_AYALINE_MOST_VIEWED_INDEX_PRODUCT_ID`;
ALTER TABLE `{$installer->getTable('ayalinemostviewed/most_viewed_index')}` DROP FOREIGN KEY `FK_AYALINE_MOST_VIEWED_INDEX_STORE_ID`;

ALTER TABLE `{$installer->getTable('ayalinemostviewed/most_viewed_index')}` ADD 
	CONSTRAINT `FK_AYALINE_MOST_VIEWED_INDEX_PRODUCT_ID`
		FOREIGN KEY (`product_id` )
		REFERENCES `{$installer->getTable('catalog/product')}` (`entity_id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
;

ALTER TABLE `{$installer->getTable('ayalinemostviewed/most_viewed_index')}` ADD 
	CONSTRAINT `FK_AYALINE_MOST_VIEWED_INDEX_STORE_ID`
		FOREIGN KEY (`store_id` )
		REFERENCES `{$installer->getTable('core/store')}` (`store_id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
;

");

$installer->endSetup();
