<?php

/**
 * Payline contracts status resource model 
 */

class Monext_Payline_Model_Mysql4_Contract_Status extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct() 
    {
        $this->_init('payline/contract_status', 'id');
    }

	/**
	 * Update contract status by scope
	 * @param type $ids
	 * @param type $status
	 * @param type $website
	 * @param type $store
	 * @return type 
	 */
	public function updateContractStatus($ids,$status,$website_code,$store_code)
	{
		if(!is_array($ids)) {
			$ids = array($ids);
		}
		
		$pointOfSell = $this->getPointOfSell($ids);
		$otherContracts = $this->getContractsNotIn($pointOfSell);
		$storeIds = array();
		$websiteId = null;
		$isDefaultLevel = false;
		$isWebsiteLevel = false;
		$isStoreViewLevel= false;
		
		if(!$store_code) {
			if($website_code) {
				$isWebsiteLevel = true;
				$website = Mage::getModel('core/website')->load($website_code);
				$websiteId = $website->getId();
				$storeIds = $website->getStoreIds();
			} else {
				$isDefaultLevel = true;
			}
		} else {
			$isStoreViewLevel = true;
			$storeIds = Mage::getModel('core/store')->load($store_code)->getId();
			$storeIds = array($storeIds);
		}
		
		$connection = $this->_getWriteAdapter();	
		$connection->beginTransaction();
		$fields = array();
		switch($status) {
			case 0:
				$fields['is_primary'] = 1; $fields['is_secondary'] = 0;		
				break;
			case 1:
				$fields['is_primary'] = 0; $fields['is_secondary'] = 1;
				break;
			case 2:
				$fields['is_primary'] = 1; $fields['is_secondary'] = 1;		
				break;
			case 3:
				$fields['is_primary'] = 0; $fields['is_secondary'] = 0;		
				break;
			default :
				$fields['is_primary'] = 0; $fields['is_secondary'] = 0;		
		}
		
		if($isDefaultLevel) {
			$conditions = array();
			$conditions[] = $connection->quoteInto('contract_id in (?)', $ids);
			$connection->delete($this->getTable('payline/contract_status'),$conditions);
			
			$where = $connection->quoteInto('id in (?)', $ids);
			$connection->update($this->getTable('payline/contract'), $fields, $where);
			
			$this->resetContractStatus($connection, 0, $otherContracts, $websiteId, $storeIds);
			
			$count = Mage::getModel('payline/contract')->getCollection()->addFieldToFilter('is_primary',1)->getSize();
		} else  {
			$conditions = 'contract_id in ('.implode(',',$ids).') AND (';
			if($isWebsiteLevel) $conditions .= 'website_id = '. $websiteId . ' OR ';
			$conditions .= 'store_id in (' . implode(',',$storeIds) . '))';				
			$connection->delete($this->getTable('payline/contract_status'),$conditions);

			foreach ($ids as $id) {
				if($isWebsiteLevel) {
					$data = array(
							'contract_id' => $id,
							'website_id' => $websiteId,
							'is_primary' => $fields['is_primary'],
							'is_secondary' => $fields['is_secondary']
						);
					$connection->insert($this->getTable('payline/contract_status'),$data);
				}
				foreach ($storeIds as $storeId) {
					$data = array(
						'contract_id' => $id,
						'store_id' => $storeId,
						'is_primary' => $fields['is_primary'],
						'is_secondary' => $fields['is_secondary']
					);
					
					$connection->insert($this->getTable('payline/contract_status'),$data);
				}
			} 
			
			$this->resetContractStatus($connection, ($isWebsiteLevel ? 2 : 3), $otherContracts, $websiteId, $storeIds);
			
			if($isWebsiteLevel) {
				$count= Mage::getModel('payline/contract_status')->getCollection()
										->addFieldToFilter('is_primary',1)
										->addFieldToFilter('store_id',$storeIds)
										->getSize();	
			} else {
				$count = Mage::getModel('payline/contract')->getCollection()->addFilterStatus(true,$storeId)->getSize();
			}
		}	

		//at least one contract must be primary
		if(!$count) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payline')->__('At leat one contract must be primary'));
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payline')->__('Please set a primary contract beforefor this point of sell'));
			return;
		}
		
		$connection->commit();	
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('payline')->__('Contracts modified successfully'));
	}
	
	/**
	 * Reset contract status for contracts that are not in $pointOfSell
	 * 
	 * @param type $connection
	 * @param type $level
	 * @param type $ids
	 * @param type $websiteId
	 * @param type $storeIds 
	 */
	public function resetContractStatus($connection,$level,$ids,$websiteId,$storeIds) {
		$fields = array();
		$fields['is_primary'] = 0;
		$fields['is_secondary'] = 0;		
		
		if($level == 0) {
			$conditions = array();
			$conditions[] = $connection->quoteInto('contract_id in (?)', $ids);
			$connection->delete($this->getTable('payline/contract_status'),$conditions);
			
			$where = $connection->quoteInto('id in (?)', $ids);
			$connection->update($this->getTable('payline/contract'), $fields, $where);
		} else  {
			$conditions = 'contract_id in ('.implode(',',$ids).') AND (';
			if($level == 2) $conditions .= 'website_id = '. $websiteId . ' OR ';
			$conditions .= 'store_id in (' . implode(',',$storeIds) . '))';
			$connection->delete($this->getTable('payline/contract_status'),$conditions);

			foreach ($ids as $id) {
				if($level == 2) {
					$data = array(
							'contract_id' => $id,
							'website_id' => $websiteId,
							'is_primary' => $fields['is_primary'],
							'is_secondary' => $fields['is_secondary']
						);
					$connection->insert($this->getTable('payline/contract_status'),$data);
				}
				foreach ($storeIds as $storeId) {
					$data = array(
						'contract_id' => $id,
						'store_id' => $storeId,
						'is_primary' => $fields['is_primary'],
						'is_secondary' => $fields['is_secondary']
					);
					
					$connection->insert($this->getTable('payline/contract_status'),$data);
				}
			} 
		}
	}
	
	
	/**
	 * Get contract ids of contracts not int $pointOfSell
	 * @param string $pointOfSell
	 * @return array 
	 */
	public function getContractsNotIn($pointOfSell) {
		$read = $this->_getReadAdapter();
		
		$select = $read->select()
			->distinct()
			->from($this->getTable('payline/contract'),array('id'))
			->where('point_of_sell != ?', $pointOfSell);				
				
		$result = $select->query();
		$row = $result->fetchAll();
		$res = array();
		foreach($row as $r) {
			$res[] = $r['id'];
		}
		return $res;
	}
	
	
	/**
	 * Get the point of sell of contracts
	 * @param array $contract_ids
	 * @return string 
	 */
	public function getPointOfSell($contract_ids) {
		$read = $this->_getReadAdapter();

		$select = $read->select()
			->distinct()
			->from($this->getTable('payline/contract'),array('point_of_sell'))
			->where('id in (?)', $contract_ids);
				
				
		$result = $select->query();
		$row = $result->fetchAll();
		return $row[0]['point_of_sell'];
	}
	
}
?>
