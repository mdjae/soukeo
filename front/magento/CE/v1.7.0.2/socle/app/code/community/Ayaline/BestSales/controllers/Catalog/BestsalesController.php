<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Catalog_BestsalesController extends Mage_Core_Controller_Front_Action {

	protected function _construct() {
		$this->_realModuleName = 'Ayaline_BestSales';
	}

	public function indexAction() {
		if(!Mage::helper('ayalinebestsales')->listIsActive()) {
			$this->_forward('cms/index/noRoute');
			return;
		}
		$this->loadLayout();
		$this->renderLayout();
	}
}
