<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Model_Adminhtml_System_Config_Source_ListMode
{
	public function toOptionArray() {
		return array(
			array('value'=>'grid', 'label'=>Mage::helper('ayalinemostviewed')->__('Grid')),
			array('value'=>'list', 'label'=>Mage::helper('ayalinemostviewed')->__('List')),
		);
	}
}
