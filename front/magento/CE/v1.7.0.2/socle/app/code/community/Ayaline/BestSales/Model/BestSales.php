<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_BestSales extends Mage_Core_Model_Abstract
{
	/**
	 * XML configuration paths
	 */
	const XML_PATH_NB_DAYS_EXPIRY		= 'ayalinebestsales/general/nb_days_expiry';
	const XML_PATH_DECREASE_IF_CANCELED	= 'ayalinebestsales/general/decrease_if_canceled';
	const XML_PATH_SIDEBAR_NB_DAYS 		= 'ayalinebestsales/sidebar/nb_days';
	const XML_PATH_GENERAL_SHOW_OUT_OF_STOCK	=	'ayalinebestsales/general/show_out_of_stock';

	protected function _construct() {
		$this->_init('ayalinebestsales/bestSales');
	}

	public function getNbDaysExpiry($storeId=null) {
		return Mage::getStoreConfig(self::XML_PATH_NB_DAYS_EXPIRY, $storeId);
	}

	public function decreaseIfCanceled($storeId=null) {
		return (bool)Mage::getStoreConfig(self::XML_PATH_DECREASE_IF_CANCELED, $storeId);
	}

	public function cleanOld($storeIds=null) {
		if($storeIds instanceof Mage_Cron_Model_Schedule) {
			$storeIds = null;
		}
		
		if(is_null($storeIds)) {
			$storeIds = Mage::app()->getStores(true);
		} elseif(!is_array($storeIds)) {
			$storeIds = array($storeIds);
		}

		foreach($storeIds as $store) {
			$storeId = $store->getId();

			$date = new Zend_Date();
			$nbDaysExpiry = $this->getNbDaysExpiry($storeId);
			$date->sub($nbDaysExpiry, Zend_Date::DAY);

			$this->_getResource()->cleanOlderThan($storeId, $date->get('YYYY-MM-dd'));
		}
	}

	/**
	 * Increase sales count for the given parameters.
	 *
	 * @param int $productId
	 * @param int $storeId
	 * @param string $date
	 * @param int $qty
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 *
	 * @return bool : true if updates
	 */
	public function increaseSalesCount($productId, $storeId, $date, $qty, $zdFormat='YYYY-MM-dd HH:mm:ss') {
		$zd = new Zend_Date($date, $zdFormat);

		$this->_getResource()->increaseSalesCount($productId, $storeId, $zd->get('YYYY-MM-dd'), $qty);
	}

	/**
	 * Decrease sales count for the given parameters.
	 * If no count if found for these parameters, do nothing.
	 *
	 * @param int $productId
	 * @param int $storeId
	 * @param string $date
	 * @param int $qty
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 *
	 * @return bool : true if updates
	 */
	public function decreaseSalesCount($productId, $storeId, $date, $qty, $zdFormat='YYYY-MM-dd HH:mm:ss') {
		$zd = new Zend_Date($date, $zdFormat);

		return $this->_getResource()->decreaseSalesCount($productId, $storeId, $zd->get('YYYY-MM-dd'), $qty);
	}

	/**
	 *
	 * Prepare collection with sales_count attribute
	 *
	 * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
	 * @param boolean $strict : get also the products already viewed or not
	 * @param int $nbDays : number of days before today
	 * @param int $store : store id (default : current store)
	 * @param string $alias : the name of the alias
	 *
	 * @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
	 */
	public function addToProductCollection($collection, $strict = false, $nbDays = null, $store = null, $alias = 'best_sales'){
		/** Manage strict Param **/
		if($strict === true){
			$join = 'joinInner';
		}else{
			$join = 'joinLeft';
		}

		/** Manage store Param **/
		if(!$store){
			$store = Mage::app()->getStore()->getId();
		}

		/** Manage nbDays Param **/
		if($nbDays == null){
			$nbDays = Mage::getStoreConfig(self::XML_PATH_SIDEBAR_NB_DAYS, $store);
		}

		$joinConditions = array('e.entity_id = absi.product_id',
								"absi.store_id = '".$store."'",
								"absi.day = '".$nbDays."'");

		$collection
			->getSelect()
			->$join(
				array('absi' => $collection->getTable('ayalinebestsales/best_sales_index')),
				implode(' AND ', $joinConditions),
				array($alias => 'absi.nb_sales')
			)
		;

		if(!Mage::getStoreConfig(self::XML_PATH_GENERAL_SHOW_OUT_OF_STOCK)) {
			$collection->getSelect()
				->joinInner(
					array('ciss' => $collection->getTable('cataloginventory/stock_status')),
					"ciss.product_id = e.entity_id AND ciss.website_id = '".Mage::app()->getStore($store)->getWebsiteId()."'",
					array()
				)
				->where('ciss.stock_status = ?', Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK)
			;
		}

		return $collection;
	}

}
