<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Model_Mysql4_MostViewedIndex extends Mage_Core_Model_Mysql4_Abstract {

	const XML_PATH_GENERAL_NB_DAYS_EXPIRY		=	'ayalinemostviewed/general/nb_days_expiry';

	protected function _construct() {
		$this->_init('ayalinemostviewed/most_viewed_index', 'index_id');
	}

	/**
	 * Table d'index
	 *
	 * @param int $storeId
	 */
	public function indexMostViewed($storeId) {
		$indexSize = $this->_getIndexSize($storeId);

		for($i = 0; $i <= $indexSize; $i++) {
			$date = new Zend_Date();
			$date->subDay($i);
			
			/* @var $select Varien_Db_Select */
			$select = $this->_getReadAdapter()->select()
				->from(
					$this->getTable('ayalinemostviewed/most_viewed'), 
					array('product_id', 'day' => new Zend_Db_Expr($i), 'store_id', 'nb_viewed' => 'SUM(most_viewed_count)'))
				->where('most_viewed_date >= ?', $date->toString('y-MM-dd'))
				->where('store_id = ?', $storeId)
				->group('product_id')
			;
			
			$query = $select->insertFromSelect($this->getTable('ayalinemostviewed/most_viewed_index_idx'), array('product_id', 'day', 'store_id', 'nb_viewed'));
			$this->_getWriteAdapter()->query($query);
		}
	}

	public function truncateIdxTable() {
		$this->_getWriteAdapter()->truncate($this->getTable('ayalinemostviewed/most_viewed_index_idx'));
	}
	
	public function renameTables() {
		$sql = "
			RENAME TABLE 
				`{$this->getTable('ayalinemostviewed/most_viewed_index_idx')}` TO `ayalinemostviewed_most_viewed_index_tmp`,
				`{$this->getMainTable()}` TO `{$this->getTable('ayalinemostviewed/most_viewed_index_idx')}`,
				`ayalinemostviewed_most_viewed_index_tmp` TO `{$this->getMainTable()}`;
		";
		
		$this->_getWriteAdapter()->query($sql);
	}
	

	protected function _getIndexSize($storeId) {
		$indexSize = Mage::getStoreConfig(self::XML_PATH_GENERAL_NB_DAYS_EXPIRY, $storeId);
		return $indexSize-1;
	}
	
	protected function _getRow($productId, $day, $storeId) {
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable())
			->where('product_id = ?', $productId)
			->where('day = ?', $day)
			->where('store_id = ?', $storeId)
		;
		return $this->_getReadAdapter()->fetchRow($select);
	}

}