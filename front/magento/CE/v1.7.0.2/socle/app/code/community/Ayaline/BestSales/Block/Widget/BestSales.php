<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Block_Widget_BestSales extends Ayaline_BestSales_Block_Sidebar implements Mage_Widget_Block_Interface {
	
	protected function _construct() {
		parent::_construct();
		$this->setCount($this->getData('products_count'));		//	nb products in template
		$this->setColumnCount($this->getData('column_count'));	//	if grid template, number of columns
	}
    
}