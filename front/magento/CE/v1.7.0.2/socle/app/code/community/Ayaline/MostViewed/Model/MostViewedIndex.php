<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Model_MostViewedIndex extends Mage_Core_Model_Abstract {

	protected function _construct() {
		$this->_init('ayalinemostviewed/mostViewedIndex');
	}

	/**
	 * Création de la table d'index des produits les plus consultés
	 *
	 * @param array(Mage_Core_Model_Store) $stores
	 * @param bool $withDefault
	 */
	public function indexMostViewed($stores = null) {
		if((is_array($stores) && empty($stores)) || is_null($stores) || $stores instanceof Mage_Cron_Model_Schedule) {
			$stores = Mage::app()->getStores();
		} elseif(!is_array($stores)) {
			$stores = array($stores);
		}

		try {
			$this->_getResource()->truncateIdxTable();
		
			foreach($stores as $_store) {
				if($_store instanceof Mage_Core_Model_Store) {
					$storeId = $_store->getStoreId();
				} else {
					$storeId = $_store;
				}
				try {
					$this->_getResource()->indexMostViewed($storeId);
				} catch(Exception $e) {
					$notificationObject = new Varien_Object(array(
						'title'			=>	Mage::helper('ayalinemostviewed')->__('An error occurred while indexing %s to the store %s', Mage::helper('ayalinemostviewed')->__('Most viewed'), $_store->getName()),
						'date'			=>	date('Y-m-d H:i:s'),
						'url'			=>	'#',
						'description'	=>	$e->getMessage().'<br />'.Mage::helper('ayalinecore')->__('See exception.log'),
					));
					Mage::helper('ayalinecore')->addCustomAdminNotification($notificationObject);
					Mage::logException($e);
				}
			}
		
			$this->_getResource()->renameTables();
			
		} catch(Exception $e) {
			$notificationObject = new Varien_Object(array(
				'title'			=>	Mage::helper('ayalinemostviewed')->__('An error occurred while indexing %s', Mage::helper('ayalinemostviewed')->__('Most viewed')),
				'date'			=>	date('Y-m-d H:i:s'),
				'url'			=>	'#',
				'description'	=>	$e->getMessage().'<br />'.Mage::helper('ayalinecore')->__('See exception.log'),
			));
			Mage::helper('ayalinecore')->addCustomAdminNotification($notificationObject);
			Mage::logException($e);
		}
	}

}