<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Model_Observer extends Mage_Core_Model_Abstract
{
	/**
	 * Event : controller_action_postdispatch_catalog_product_view
	 *
	 * @param Varien_Event_Observer $observer
	 * @return Ayaline_BestSales_Model_Observer
	 */
	public function increaseProductView(Varien_Event_Observer $observer) {
		$product = Mage::registry('current_product');
		
		$zd = new Zend_Date();
		Mage::getSingleton('ayalinemostviewed/mostViewed')->increaseMostViewedCount($product->getId(), $product->getStoreId(), $zd->get('YYYY-MM-dd'));
	}
}

