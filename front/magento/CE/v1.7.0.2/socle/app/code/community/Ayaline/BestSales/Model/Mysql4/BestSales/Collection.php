<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_Mysql4_BestSales_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_storeFilterApplied = false;
	protected $_useStrictMode = false;
	
	public function _construct() {
		$this->_init('ayalinebestsales/bestSales');
	}
	
	/**
	 * Returns a new product collection after joining with current collection
	 * Only $fields are added to product collection select
	 * 
	 * @param array $fields
	 * @param string $sortOrder
	 * @param string $sortDirection
	 */
	public function getProductCollection($fields=array(), $sortOrder='sales_count', $sortDirection='desc') {
		if(sizeof($fields) == 0) {
			$fields = array('sales_count' => new Zend_Db_Expr('SUM(sales_count)'));
		}
		
		/* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product */
		$collection = Mage::getModel('catalog/product')->getCollection();
		
		$bestSalesCollection = clone($this);
		$bestSalesCollection->load();
		
		if($this->_useStrictMode) {
			$collection->getSelect()
				->joinInner(
					array('abs' => $bestSalesCollection->getSelect()),
					'e.entity_id = abs.product_id',
					$fields
				);
		} else {
			$collection->getSelect()
				->joinLeft(
					array('abs' => $bestSalesCollection->getSelect()),
					'e.entity_id = abs.product_id',
					$fields
				);
		}
		$collection->getSelect()
			->group('e.entity_id')
			->order("$sortOrder $sortDirection")
		;
		
		return $collection;
	}
	
	protected function _beforeLoad() {
		if(!$this->_storeFilterApplied) {
			$this->addStoreFilter(Mage::app()->getStore()->getId());
		}
		return parent::_beforeLoad();
	}
	
	public function useStrictMode($strict=true) {
		$this->_useStrictMode = $strict;
		return $this;
	}
	
	public function addLastDaysFilter($nbDays, $strict=false) {
		$date = new Zend_Date();
		$date->sub($nbDays, Zend_Date::DAY);
		$this->addAfterDateFilter($date->get('YYYY-MM-dd'), $strict);
		return $this;
	}
	
	/**
	 * @param string $date
	 * @param bool $strict
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 */
	public function addAfterDateFilter($date, $strict=false, $zdFormat='YYYY-MM-dd') {
		$operand = $strict ? 'gt' : 'gteq';
		
		return $this->_addDateFilter($date, $zdFormat, $operand);
	}
	
	/**
	 * @param string $date
	 * @param bool $strict
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 */
	public function addBeforeDateFilter($date, $strict=false, $zdFormat='YYYY-MM-dd') {
		$operand = $strict ? 'lt' : 'lteq';
		
		return $this->_addDateFilter($date, $zdFormat, $operand);
	}
	
	/**
	 * @param string $date
	 * @param string $zdFormat : date format for $date (see Zend_Date)
	 */
	public function addDateFilter($date, $zdFormat='YYYY-MM-dd') {
		return $this->_addDateFilter($date, $zdFormat, 'eq');
	}
	
	protected function _addDateFilter($date, $zdFormat, $operand) {
		$zd = new Zend_Date($date, $zdFormat);
		
		$this->addFieldToFilter('main_table.sales_date', array($operand => $zd->get('YYYY-MM-dd')));
		
		return $this;
	}
	
	/**
	 * Add Products Filter
	 *
	 * @param int|array $productId
	 * @return Ayaline_BestSales_Model_Mysql4_BestSales_Collection
	 */
	public function addProductFilter($productId) {
		if(!is_array($productId)) {
			$productId = array($productId);
		}
		
		$this->addFieldToFilter('main_table.product_id', array('in' => $productId));
		
		return $this;
	}
	
	/**
	 * Add Stores Filter
	 *
	 * @param int|array $storeId
	 * @return Ayaline_BestSales_Model_Mysql4_BestSales_Collection
	 */
	public function addStoreFilter($storeId, $withAdmin = true) {
		if(!is_array($storeId)) {
			$storeId = array($storeId);
		}
		
		if($withAdmin) {
			$storeId[] = 0;
		}
		
		$this->addFieldToFilter('main_table.store_id', array('in' => $storeId));
		
		$this->_storeFilterApplied = true;
		
		return $this;
	}
}
