<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Block_Sidebar extends Mage_Catalog_Block_Product_Abstract
{
	const XML_PATH_NB_DAYS			= 'ayalinebestsales/sidebar/nb_days';
	const XML_PATH_COUNT			= 'ayalinebestsales/sidebar/count';
	const XML_PATH_TRUNCATE_NAME	= 'ayalinebestsales/sidebar/truncate_name';
	
	protected $_nbDays = null;
	protected $_count = null;
	protected $_useStrictMode = true;
	
	/**
	 * Number of days used to calculate the best sales
	 * Should be called in layouts
	 */
	public function setNbDays($nbDays) {
		$this->_nbDays = $nbDays;
	}
	
	public function getNbDays() {
		if($this->_nbDays) {
			return $this->_nbDays;
		} else {
			return Mage::getStoreConfig(self::XML_PATH_NB_DAYS);
		}
	}
	
	/**
	 * Number of products displayed in sidebar
	 * Should be called in layouts
	 */
	public function setCount($count) {
		$this->_count = $count;
	}
	
	public function getCount() {
		if($this->_count) {
			return $this->_count;
		} else {
			return Mage::getStoreConfig(self::XML_PATH_COUNT);
		}
	}
	
	/**
	 * Define the mode. If not strict, products never viewed are still loadeds
	 * Should be called in layouts
	 */
	public function setUseStrictMode($strict) {
		$this->_useStrictMode = $strict;
	}
	
	public function getUseStrictMode() {
		return (bool)$this->_useStrictMode;
	}
	
	/**
	 * Return best sales products collection
	 * 
	 * @return Mage_Catalog_Model_Resource_Product_Collection
	 */
	public function getProductCollection() {
		$productCollection = Mage::getResourceModel('ayalinebestsales/bestSales_collection')
			->addLastDaysFilter($this->getNbDays())
			->useStrictMode($this->getUseStrictMode())
			->getProductCollection()
			->addStoreFilter(Mage::app()->getStore()->getId())
			->addAttributeToSelect('name')
			->addAttributeToFilter('status', array('eq' => 1))
			->setPageSize($this->getCount())
		;
		
		if($category = Mage::registry('current_category')) {
			$productCollection->addCategoryFilter($category);
		}
		
		$productCollection = $this->_addProductAttributesAndPrices($productCollection);
		
		Mage::dispatchEvent('ayalinebestsales_sidebar_prepare_collection', array('collection' => $productCollection));
		
		return $productCollection;
	}
	
	public function getProductName($product) {
		$name = $this->helper('catalog/output')->productAttribute($product, $product->getName(), 'name');
		
		if($nb = Mage::getStoreConfig(self::XML_PATH_TRUNCATE_NAME)) {
			$useless = '';
			return Mage::helper('core/string')->truncate($name, $nb, '...', $useless, false);
		}
		
		return $name;
	}
}
