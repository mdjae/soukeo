<?php

/* SOUKEO SAS
 * @author : Silicon Village Team (mamode@silicon-village.fr)
 * @package : Avahis
 */

class Soukeo_ChronopostGames_Model_Observer {

    //private $_dir = '/opt/soukeo/export/chronopei/customers/data.csv';
    // private $_file = 'data.csv';
    private $_idChrono = '53';
   

    public function DB($sql) {
        
        $resource = Mage::getSingleton('core/resource');
        $cnx = $resource->getConnection('chronopostgames_write');
        $res = $cnx->query($sql);

        return $res;
    }

    /**
     * Appel aux collection et retourne de données
     * @param 
     * @return array 
     */
    public function accessData($model, $condition, $field, $value) {
        $cltn = Mage::getModel($model)
                ->getCollection()
                ->addFieldToFilter($field, array($condition => $value));
        $tmp = $cltn->getData();

        return $tmp[0];
    }

    public function token() {
        $d = uniqid(rand(100), true);
        return $d;
    }

    /**
     * Detection de l'event
     * @param Varien_Event_Observer $observer observer object
     * @return boolean
     */
    public function chronopostGamesProcess($observer) {
        // Infos de commande
        $orderIds = $observer->getData('order_ids');
        $order = Mage::getModel('sales/order')->load($orderIds);

        $customer_email = $order->getData('customer_email');
        $customer_name = $order->getData('customer_firstname') . ' ';
        $customer_name .= $order->getData('customer_lastname');
        $mtt = $order->getData('grand_total');
        Mage::log('[ChronopostGames] - Mtt ' . $mtt, null, 'soukeo.log');

        $order_id = $order->getId();
        Mage::log('[ChronopostGames] - order_id ' . $order_id, null, 'soukeo.log');


        
        $_sessionData = Mage::getSingleton('udropship/session');
        $vendor_id = $_sessionData->getVendorId();
        
        //$poData = $this->accessData('udpo/po', 'eq', 'order_id', $orderId);
        $cltn = Mage::getModel('udpo/po')
                ->getCollection()
                ->addFieldToFilter('order_id', array('eq' => $order_id))
                ->addFieldToFilter('udropship_vendor', array('eq' => $vendor_id));

        $tmp = $cltn->getData();
        $poData = $tmp[0];
        

        if (!empty($poData) && $poData != null) {

            $poVendorId = $poData['udropship_vendor'];
            Mage::log('[ChronopostGames] - poVendorId ' . $poVendorId, null, 'soukeo.log');

            //COMMANDE
            $cmd_id = $order->getData('increment_id');
            $bon_cmd = $poData['increment_id'];

            //Shipping Adresse
            $shipping_address_id = $order->getShippingAddress()->getId();
            $shipping_address = Mage::getModel('sales/order_address')->load($shipping_address_id);

            $shipping_tel = $order->getShippingAddress()->getData('telephone');
            $shipping_street = $order->getShippingAddress()->getStreetFull();

            $city = $order->getShippingAddress()->getCity();
            $postcode = $order->getShippingAddress()->getPostcode();
            $country_name = $this->getCountryName($order->getShippingAddress()->getCountry_id());



            //Billing Adresse
            $tel = $order->getBillingAddress()->getData('telephone');
            $street = $order->getBillingAddress()->getStreetFull();

            $city = $order->getBillingAddress()->getCity();
            $postcode = $order->getBillingAddress()->getPostcode();
            $country_name = $this->getCountryName($order->getBillingAddress()->getCountry_id());

            //SHipping method
            $shipping_method = $poData['udropship_method_description'];

            // Lien download bon de commande
            $bc_link = $_SERVER['DOCUMENT_ROOT'] ."/udpo/vendor/udpoPdf/udpo_id/$order_id/";




            if ($mtt >= 55 && $poVendorId == $this->_idChrono) {
                Mage::log('[ChronopostGames] - Cmd_id ' . $cmd_id, null, 'soukeo.log');
                Mage::log('[ChronopostGames] - Mtt ' . $mtt, null, 'soukeo.log');
                Mage::log('[ChronopostGames] - poVendorId ' . $poVendorId, null, 'soukeo.log');

                // Insert des données dans DB jeux
                $lastname = $order->getData('customer_lastname');
                $firstname = $order->getData('customer_firstname');
                $tk = $this->token();

                $qSelect = "SELECT `mail` FROM `soukflux`.`chronopei` WHERE `mail`='" . $customer_email . "'";

                $qInsert = "INSERT INTO `soukflux`.`chronopei` (`id` ,`nom` ,`prenom` ,`date_naissance` ,`mail` ,`telephone` ,
                    `adresse` ,`code_postal` ,`ville` ,`news` ,`reponse1` ,`reponse2` ,`reponse3` , `bonne_reponse` ,
                    `date_participation` ,`IP` ,`token` ,`a_participer`)
                    VALUES (NULL,'" . $lastname . "','" . $firstname . "' ,'','" . $customer_email . "','" . $tel . "','" . $street . "','" . $postcode . "','" . $city . "','','','','','','','','" . $tk . "','0')";

                $qInsertOther = "INSERT INTO `soukflux`.`chronopei_other` (`id` ,`nom` ,`prenom` ,`date_naissance` ,`mail` ,`telephone` ,
                    `adresse` ,`code_postal` ,`ville` ,`news` ,`reponse1` ,`reponse2` ,`reponse3` , `bonne_reponse` ,
                    `date_participation` ,`IP` ,`token` ,`a_participer`)
                    VALUES (NULL,'" . $lastname . "','" . $firstname . "' ,'','" . $customer_email . "','" . $tel . "','" . $street . "','" . $postcode . "','" . $city . "','','','','','','','','" . $tk . "','0')";

                // try {
                $res = $this->DB($qSelect);
                $count = count($res->fetchAll());
                Mage::log("[ChronopostGames] - count $count", null, 'soukeo.log');

                if ($count == 0) {
                    
                    $res = $this->DB($qInsert);
                    Mage::log('[ChronopostGames] - Ok', null, 'soukeo.log');


                    $mail_client = "Bonjour $customer_name,<br/><br/>
                    Vous avez acheté 55€ de produits Chronopost sur la boutique Chronopost d'Avahis.com.<br/>
                    Cet achat vous offre la possibilité de participer au grand jeu Chronopost et d'avoir la chance de gagner<br/>
                    un vol aller-retour Réunion/Paris/Réunion <br/>
                    ou Paris/Réunion/Paris pour 2 personnes avec Air Austral.<br/>
                    Pour participer cliquez sur le lien suivant :<br/><br/>

                    <a target='_blank' href='http://www.jeuchronopei.avahis.com/?t=$tk'> www.jeuchronopei.avahis.com/?t=$tk </a><br/><br/>
                    Chronopost vous souhaite bonne chance.<br/>
                    Pour joindre Chronopost notre N° à tarif spécial : <b>0 825 801 801</b>
                    <br/><hr/><br/>
                    <center><b>Information importante<b><br/><br/>
                    En raison de la forte affluence de colis en décembre et donc 
                    des limites de capacités de chargement des compagnies aériennes, 
                    un bon de réservation Chronopéi vous sera remis à la livraison 
                    de votre commande.Ce bon de réservation devra être remis 
                    avec le colis lors du dépôt en bureau de poste ou en agence Chronopost.
                    Tous les Prêt-à-Expédier qui vous sont livrés à domicile doivent 
                    être déposés par vos soins en bureau de poste ou en agence Chronopost.<center>";


                    $this->sendMail($mail_client, 'Gagner un billet d\'avion avec Chronopost',$customer_email);
                    $this->sendMail($mail_client,'Une nouvelle commande Chronopei sur Avahis','mamode@silicon-village.fr');
                    
                } else {

                    $res = $this->DB($qInsertOther);
                    Mage::log('[ChronopostGames] - Other', null, 'soukeo.log');
                }
            }
        } else {
            Mage::log('[ChronopostGames] - Commande' . $cmd_id . ' repond au critere mais absence de PO', null, 'soukeo.log');
            return false;
        }
    }

    /**
     * Gestion du label country par rapport au code pays
     * @param $country_id string
     * @return string
     */
    public function getCountryName($country_id) {

        switch ($country_id) {
            case 'RE':
                $countryname = 'REUNION';
                return $countryname;
                break;
            case 'YT':
                $countryname = 'MAYOTTE';
                return $countryname;
                break;
            case 'MU':
                $countryname = 'MAURICE';
                return $countryname;
                break;
            case 'MG':
                $countryname = 'MADAGASCAR';
                return $countryname;
                break;

            default:
                break;
        }
    }

    /**
     * Envoi du mail et pj
     * @param 
     * @return boolean
     */
    public function sendMail($mailTpl, $subject, $to) {

        try {

            $msg = new Zend_Mail('UTF-8');
            $msg->setBodyHtml($mailTpl);
            $msg->setFrom('contact@avahis.com', 'AVAHIS');
            $msg->addTo($to, 'Chronopost');
            $msg->setSubject($subject);

            $msg->send();

            Mage::log('[ChronopostGames] - Envoi du mail ', null, 'soukeo.log');

            return true;
        } catch (Exception $e) {
            //$order->addStatusHistoryComment('Soukeo_ShippingLabel: Exception occurred during action. Exception message: ' . $e->getMessage(), false);
            Mage::log("[ChronopostGames] - Error sending mail", null, 'soukeo.log');
            return false;
        }
    }

}

?>
