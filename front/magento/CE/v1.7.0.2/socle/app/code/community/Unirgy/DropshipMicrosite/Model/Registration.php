<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipMicrosite
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

class Unirgy_DropshipMicrosite_Model_Registration extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('umicrosite/registration');
        parent::_construct();
    }
/*
    protected function _afterLoad()
    {
        parent::_afterLoad();
        Mage::helper('udropship')->loadCustomData($this);
    }
*/
    public function validate()
    {
        $hlp = Mage::helper('umicrosite');
        $dhlp = Mage::helper('udropship');
        extract($this->getData());
        
        $_isQuickRegister = Mage::getStoreConfig('udropship/microsite/allow_quick_register');

        if (!isset($vendor_name) || !isset($telephone) || !isset($email) ||
            !isset($password) || !isset($password_confirm)
        ) {
            Mage::throwException($hlp->__('IFormulaire incomplet'));
        }
        if (!$_isQuickRegister) {
            if (!isset($carrier_code) || !isset($url_key)
                || !isset($street1) || !isset($city) || !isset($country_id)
            ) {
                Mage::throwException($hlp->__('Formulaire incomplet'));
            }
        }
        if ($password!=$password_confirm) {
            Mage::throwException($hlp->__('Les deux mots de passe saisis ne sont pas identiques'));
        }
        $collection = Mage::getModel('udropship/vendor')->getCollection()
            ->addFieldToFilter('email', $email);
        foreach ($collection as $dup) {
            if (Mage::getStoreConfig('udropship/vendor/unique_email')) {
                Mage::throwException($dhlp->__('Un vendeur est déjà enregistré avec cette adresse e-mail.'));
            }
            if (Mage::helper('core')->validateHash($password, $dup->getPasswordHash())) {
                Mage::throwException($dhlp->__('Un vendeur est déjà enregistré avec cette adresse e-mail et ce mot de passe.'));
            }
        }
        if (isset($url_key)) {
            $vendor = Mage::getModel('udropship/vendor')->load($url_key, 'url_key');
            if ($vendor->getId()) {
                Mage::throwException($hlp->__('Ce nom de domaine est déjà réservé, nous vous remercions de bien vouloir en choisir un autre.'));
            }
            if (Mage::helper('udropship')->isUrlKeyReserved($url_key)) {
                Mage::throwException(Mage::helper('udropship')->__('Cette URL est déjà réservée, nous vous remercions de bien vouloir en choisir une autre.'));
            }
        }
        if (Mage::getStoreConfig('udropship/vendor/unique_vendor_name')) {
            $collection = Mage::getModel('udropship/vendor')->getCollection()
                ->addFieldToFilter('vendor_name', $vendor_name);
            foreach ($collection as $dup) {
                Mage::throwException(Mage::helper('udropship')->__('Un vendeur a déjà enregistré sa boutique sous ce nom, nous vous remercions de bien vouloir en choisir un autre.'));
            }
        }
        $this->setStreet(@$street1."\n".@$street2);
        $this->setPasswordEnc(Mage::helper('core')->encrypt($password));
        $this->setPasswordHash(Mage::helper('core')->getHash($password, 2));
        $this->unsPassword();
        $this->setRemoteIp($_SERVER['REMOTE_ADDR']);
        $this->setRegisteredAt(now());
        $this->setStoreId(Mage::app()->getStore()->getId());

        return $this;
    }

    protected function _afterSave()
    {
        if ($this->_inAfterSave) {
            return;
        }
        $this->_inAfterSave = true;

        parent::_afterSave();

        if (!empty($_FILES)) {
            $baseDir = Mage::getConfig()->getBaseDir('media').DS.'registration'.DS.$this->getId();
            Mage::getConfig()->createDirIfNotExists($baseDir);
            foreach ($_FILES as $k=>$img) {
                if (empty($img['tmp_name']) || empty($img['name']) || empty($img['type'])) {
                    continue;
                }
                if (!@move_uploaded_file($img['tmp_name'], $baseDir.DS.$img['name'])) {
                    Mage::throwException('Error while uploading file: '.$img['name']);
                }
                $this->setData($k, 'registration/'.$this->getId().'/'.$img['name']);
            }
            $this->save();
        }
        $this->_inAfterSave = false;
    }

    public function toVendor()
    {
        $vendor = Mage::getModel('udropship/vendor')->load(Mage::getStoreConfig('udropship/microsite/template_vendor'));
        $carrierCode = $this->getCarrierCode() ? $this->getCarrierCode() : $vendor->getCarrierCode();
        $vendor->getShippingMethods();
        $vendor->unsetData('vendor_name');
        $vendor->unsetData('url_key');
        $vendor->unsetData('email');
        $vendor->addData($this->getData());
        $vendor->setCarrierCode($carrierCode);
        Mage::helper('udropship')->loadCustomData($vendor);
        $vendor->setPassword(Mage::helper('core')->decrypt($this->getPasswordEnc()));
        $vendor->unsVendorId();
        $shipping = $vendor->getShippingMethods();
        foreach ($shipping as $sId=>&$_s) {
            foreach ($_s as &$s) {
                if ($s['carrier_code']==$vendor->getCarrierCode()) {
                    $s['carrier_code'] = null;
                }
            }
        }
        unset($_s);
        unset($s);
        $vendor->setShippingMethods($shipping);
        return $vendor;
    }
}