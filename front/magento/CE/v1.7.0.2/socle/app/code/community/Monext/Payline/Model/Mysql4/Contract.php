<?php

/**
 * Payline contracts resource model 
 */

class Monext_Payline_Model_Mysql4_Contract extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct() 
    {
        $this->_init('payline/contract', 'id');
    }
	
	/**
	 * set primary = 0 and secondary = 0 for contracts that are not in $pointOfSell
	 * @param type $pointOfSell 
	 */
	public function removePrimaryAndSecondaryNotIn($pointOfSell)
	{
		$connection = $this->_getWriteAdapter();	
		$connection->beginTransaction();
		$fields = array();
		$fields['is_primary'] = 0;
		$fields['is_secondary'] = 0;
		$where = $connection->quoteInto('point_of_sell != ?', $pointOfSell);
		$connection->update($this->getTable('payline/contract'), $fields, $where);
		$connection->commit();
	}
}
?>
