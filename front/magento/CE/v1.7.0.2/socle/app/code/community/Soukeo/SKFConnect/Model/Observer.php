<?php

/* SOUKEO SAS
 * @author : Silicon Village Team (mamode@silicon-village.fr)
 * @package : Avahis
 */

class Soukeo_SKFConnect_Model_Observer {

    public function DB($sql) {

        $resource = Mage::getSingleton('core/resource');
        if ($_SERVER['HTTP_HOST'] == 'www.avahis.com') {
            $cnx = $resource->getConnection('skfconnect_write');
        } else {
            $cnx = $resource->getConnection('skfconnectdev_write');
        }

        $res = $cnx->query($sql);

        return $res;
    }

    /**
     * Appel aux collection et retourne de données
     * @param 
     * @return array 
     */
    public function buildSql($data, $tbl, $type) {

        foreach ($data as $k => $v) {
            $col[] = "`$k`";
            $v = addslashes($v);
            $val[] = "'$v'";
        }

        $colstr = implode(",", $col);
        $valstr = implode(",", $val);


        switch ($type) {
            case 'insert':
                $q = "INSERT INTO `$tbl` ($colstr) VALUES ($valstr) ON DUPLICATE KEY UPDATE $col[0]=VALUES($col[0]) ";
                break;
            case 'insert_brut':
                $q = "INSERT INTO `$tbl` ($colstr) VALUES ($valstr)";
                break;
            case 'delete':
                break;
            case 'update':
                break;
            default:
                break;
        }


        return $q;
    }

    /**
     * Appel aux collection et retourne de données
     * @param 
     * @return array 
     */
    public function accessData($model, $condition, $field, $value) {
        $cltn = Mage::getModel($model)
                ->getCollection()
                ->addFieldToFilter($field, array($condition => $value));
        $tmp = $cltn->getData();

        return $tmp[0];
    }

    /**
     * Envoi du mail et pj
     * @param 
     * @return boolean
     */
    public function sendMail($mailTpl, $subject, $to) {

        try {

            $msg = new Zend_Mail('UTF-8');
            $msg->setBodyHtml($mailTpl);
            $msg->setFrom('contact@avahis.com', 'AVAHIS');
            $msg->addTo($to, 'Chronopost');
            $msg->setSubject($subject);

            $msg->send();

            Mage::log('[SKFConnect] - Envoi du mail ', null, 'soukeo.log');

            return true;
        } catch (Exception $e) {
            //$order->addStatusHistoryComment('Soukeo_ShippingLabel: Exception occurred during action. Exception message: ' . $e->getMessage(), false);
            Mage::log("[SKFConnect] - Error sending mail", null, 'soukeo.log');
            return false;
        }
    }

    /**
     * Recuperer les informations de facturation 
     * de la commande
     * @param $order int
     * @return array
     */
    public function getOrderInfo($order) {

        $line['order_id'] = $order->getData('entity_id');
        $line['order_state'] = $order->getData('state');
        $line['order_status'] = $order->getData('status');
        $line['coupon_code'] = $order->getData('coupon_code');
        $line['coupon_rule_name'] = $order->getData('coupon_rule_name');
        $line['shipping_description'] = $order->getData('shipping_description');

        $line['order_store_id'] = $order->getData('store_id');
        $line['order_customer_id'] = $order->getData('customer_id');
        $line['customer_group_id'] = $order->getData('customer_group_id');
        $line['recipient_name'] = $order->getData('customer_lastname').' '.$order->getData('customer_firstname');    // Mtt cmd dans la monnaie d'origine
        
        $line['base_shipping_amount'] = $order->getData('base_shipping_amount');
        $line['base_shipping_incl_tax'] = $order->getData('base_shipping_incl_tax');
        
        // Mtt cmd avnat les taxes
        $line['base_grand_total'] = $order->getData('base_grand_total');
        $line['base_subtotal'] = $order->getData('base_subtotal');
        $line['base_tax_amount'] = $order->getData('base_tax_amount');
        $line['discount_amount'] = $order->getData('discount_amount');
        // Mtt cmd dans la monnaie de paiement
        $line['grand_total'] = $order->getData('grand_total');
        $line['tax_amount'] = $order->getData('tax_amount');
        
        
        $line['billing_address_id'] = $order->getData('billing_address_id');
        $line['shipping_address_id'] = $order->getData('shipping_address_id');

        $line['weight'] = $order->getData('weight');
        $line['increment_id'] = $order->getData('increment_id');
        $line['quote_id'] = $order->getData('quote_id');
        $line['store_currency_code'] = $order->getData('store_currency_code');

        $line['udropship_status'] = $order->getData('udropship_status');
        $line['udropship_shipping_details'] = $order->getData('udropship_shipping_details');
        $line['date_created'] = $order->getData("created_at");
        $line['date_updated'] = $order->getData("updated_at");
        $line['litiges'] = 0;

        return $line;
    }

    /**
     * Recuperer les informations de chaque ligne de la commande
     * @param $order int
     * @return array
     */
    function getOrderLineDetails($order) {

        $lines = array();
        $items = $order->getAllVisibleItems();
        
        foreach ($items as $item) {
            $line = array();
            var_dump($line);
            $line['item_id'] = $item->getData('item_id');
            $line['sku'] = $item->getData('sku');
            $line['vendor_sku'] = $item->getData('sku');
            $line['udropship_vendor'] = $item->getData('udropship_vendor');
            $line['order_id'] = $item->getData('order_id');
            $line['increment_id'] = $item->getData('increment_id');
            $line['attribute_set_id'] = '';
            $line['name'] = $item->getData('name');
            $line['base_row_total'] = $item->getData('base_row_total');
            $line['base_row_total_incl_tax'] = $item->getData('base_row_total_incl_tax');
            $line['base_tax_amount'] = $item->getData('base_tax_amount');
            $line['row_weight'] = $item->getData('row_weight');
            $line['qty_ordered'] = $item->getData('qty_ordered');
           
            //$_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$line['sku']);
           // $taxClassId = $_product->getTaxClassId();
            
            $line['tax_class_id'] = 0;
            $line['date_created'] = $item->getData('created_at');
            $line['date_updated'] = $item->getData('updated_at');
            $line['litiges'] = 0;
            $lines[] = $line;
        }

        return $lines;
    }
    
    
    
    
    function getHistoryLineDetails($order) {

        $lines = array();

        	$items  = Mage::getModel('sales/order_status_history')->getCollection()
                ->addFieldToFilter('parent_id', $order->getId());
                
        foreach ($items as $item) {
            $line = array();

            $line['history_id'] = $item->getData('entity_id');
            $line['order_id'] = $item->getData('parent_id');
            $line['is_customer_notified'] = $item->getData('is_customer_notified');
            $line['is_visible_on_front'] = $item->getData('is_visible_on_front');
            $line['comment'] = $item->getData('comment');
            $line['status'] = $item->getData('status');
            $line['created_at'] = $item->getData('created_at');
            $line['entity_name'] = $item->getData('entity_name');

            $lines[] = $line;
        }

        return $lines;
    }
    
    
    
    public function getPO($order) {
        $order_id = $order->getId();
        //$udsd = json_decode($order->getData('udropship_shipping_details'),true);
        
        //foreach($udsd['methods'] as $k => $v){
            $cltn = Mage::getModel('udpo/po')
                        ->getCollection()
                        ->addFieldToFilter('order_id', array('eq' => $order_id));
                        //->addFieldToFilter('udropship_vendor', array('eq' => $k));

                $po = $cltn->getData();
                //$po = $tmp[0];
                foreach($po as $v) {

                    Mage::log('[SKFConnect] - Insert en base soukflux', null, 'soukeo.log');

                    $line = array();
                    $line['po_id'] = $v['entity_id'];
                    $line['vendor_id'] = $v['udropship_vendor'];
                    $line['increment_id'] = $v['increment_id'];
                    $line['order_id'] = $v['order_id'];
                    $line['customer_id'] = $v['customer_id'];
                    $line['udropship_status'] = $v['udropship_status'];
                    $line['store_id'] = $v['store_id'];
                    $line['total_weight'] = $v['total_weight'];
                    $line['total_qty'] = $v['total_qty'];
                    $line['shipping_address_id'] = $v['shipping_address_id'];
                    $line['billing_address_id'] = $v['billing_address_id'];
                    $line['base_total_value'] = $v['base_total_value'];
                    $line['base_shipping_amount'] = $v['base_shipping_amount'];
                    $line['base_tax_amount'] = $v['base_tax_amount'];
                    $line['udropship_method'] = $v['udropship_method'];
                    $line['udropship_method_description'] = $v['udropship_method_description'];
                    $line['date_created'] = $v['created_at'];
                    $line['date_updated'] = $v['updated_at'];
                    $line['date_updated'] = 0;
                    $lines[] = $line;
                }
        //}
        
        return $lines;
    }

    
    
    
    
    public function getShipping($order) {
        $shipping_amount = $order->getShippingAmount();
        $shipping_method = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod();

        return array(
            "method" => $shipping_method,
            "shipping_amount" => $shipping_amount,
            "tax_amount" => $order->getData("shipping_tax_amount"),
            "tracking_number" => '',
            "date_created" => $order->getData("created_at"),
            "date_updated" => $order->getData("updated_at")
        );
    }




    public function getCustomer($order) {
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId()); 

            return array(
               "customer_id" => $order->getCustomerId(),
               "name" => $customer->getName(),
               "company" => $customer->getCompany(),
               "firstname" => $customer->getFirstname(),
               "lastname" => $customer->getLastname(),
                "email" => $customer->getEmail(),
               "phone" => $customer->getTelephone(),
               "date_created" => $customer->getData("created_at"),
               "date_updated" => $customer->getData("updated_at")
         );

    }

    /**
     * Recuperer les informations de facturation 
     * de la commande
     * @param $order 
     * @return array
     */
    public function getAddressBilling($order) {
        //$billingAddress = $order->getBillingAddress();
        $adr_id = $order->getData('billing_address_id');
        $billingAddress = Mage::getModel('sales/order_address')->load($adr_id);

        return array(
            "address_id" => $billingAddress ? $order->getData('billing_address_id') : '',
            "street1" => $billingAddress ? $billingAddress->getStreet(1) : '',
            "street2" => $billingAddress ? $billingAddress->getStreet(2) : '',
            "postcode" => $billingAddress ? $billingAddress->getData("postcode") : '',
            "city" => $billingAddress ? $billingAddress->getData("city") : '',
            "region_code" => $billingAddress ? $billingAddress->getRegionCode() : '',
            "country" => $billingAddress ? $billingAddress->getCountry() : '',
            //"date_created" => $billingAddress->getData("created_at"),
            //"date_updated" => $billingAddress->getData("updated_at"),
            "address_type" => 'billing'
        );
    }

    /**
     * Recuperer les informations de livraison de la commande
     * @param $order 
     * @return array
     */
    public function getAddressShipping($order) {
        //$shippingAddress =  $order->getShippingAddress();
        $adr_id = $order->getData('shipping_address_id');
        $shippingAddress = Mage::getModel('sales/order_address')->load($adr_id);
        return array(
            "address_id" => $shippingAddress ? $order->getData('shipping_address_id') : '',
            "street1" => $shippingAddress ? $shippingAddress->getStreet(1) : '',
            "street2" => $shippingAddress ? $shippingAddress->getStreet(2) : '',
            "postcode" => $shippingAddress ? $shippingAddress->getData("postcode") : '',
            "city" => $shippingAddress ? $shippingAddress->getData("city") : '',
            "region_code" => $shippingAddress ? $shippingAddress->getRegionCode() : '',
            "country" => $shippingAddress ? $shippingAddress->getCountry() : '',
            //"date_created" => $shippingAddress->getData("created_at"),
            //"date_updated" => $shippingAddress->getData("updated_at"),
            "address_type" => 'shipping'
        );
        
    }

    /**
     * Recuperer les informations de livraison de la commande
     * @param $order 
     * @return array
     */
    public function getInvoice($order) {
        $incrementId = $order->getData('increment_id');
        $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($incrementId);
    }

    /**
     * Recuperer les informations de livraison de la commande
     * @param $order int
     * @return array
     */
    public function getPayment($order) {
        $method_title = $order->getPayment()->getMethodInstance()->getTitle();
        $payment = $order->getPayment();

        return array(

            "base_shipping_amount" => $payment ? $payment->getData("base_shipping_amount") : '',
            "shipping_amount" => $payment ? $payment->getData("shipping_amount") : '',
            "base_amount_ordered" => $payment ? $payment->getData("base_amount_ordered") : '',
            "amount_ordered" => $payment ? $payment->getData("amount_ordered") : '',
            "base_amount_canceled" => $payment ? $payment->getData("base_amount_canceled") : '',
            "cc_exp_month" => $payment ? $payment->getData("cc_exp_month") : '',
            "cc_ss_start_year" => $payment ? $payment->getData("cc_ss_start_year") : '',
            "echeck_bank_name" => $payment ? $payment->getData("echeck_bank_name") : '',
            "method" => $payment ? $payment->getData("method") : '',
            "cc_debug_request_body" => $payment ? $payment->getData("cc_debug_request_body") : '',
            "cc_secure_verify" => $payment ? $payment->getData("cc_secure_verify") : '',
            "protection_eligibility" => $payment ? $payment->getData("protection_eligibility") : '',
            "cc_approval" => $payment ? $payment->getData("cc_approval") : '',
            "cc_last4" => $payment ? $payment->getData("cc_last4") : '',
            "cc_status_description" => $payment ? $payment->getData("cc_status_description") : '',
            "echeck_type" => $payment ? $payment->getData("echeck_type") : '',
            "cc_debug_response_serialized" => $payment ? $payment->getData("cc_debug_response_serialized") : '',
            "cc_ss_start_month" => $payment ? $payment->getData("cc_ss_start_month") : '',
            "echeck_account_type" => $payment ? $payment->getData("echeck_account_type") : '',
            "last_trans_id" => $payment ? $payment->getData("last_trans_id") : '',
            "cc_cid_status" => $payment ? $payment->getData("cc_cid_status") : '',
            "cc_owner" => $payment ? $payment->getData("cc_owner") : '',
            "cc_type" => $payment ? $payment->getData("cc_type") : '',
            "po_number" => $payment ? $payment->getData("po_number") : '',
            "cc_exp_year" => $payment ? $payment->getData("cc_exp_year") : '',
            "cc_status" => $payment ? $payment->getData("cc_status") : '',
            "echeck_routing_number" => $payment ? $payment->getData("echeck_routing_number") : '',
            "account_status" => $payment ? $payment->getData("account_status") : '',
            "cc_ss_issue" => $payment ? $payment->getData("cc_ss_issue") : '',
            "check_account_name" => $payment ? $payment->getData("check_account_name") : '',
            "cc_avs_status" => $payment ? $payment->getData("cc_avs_status") : '',
            "cc_number_enc" => $payment ? $payment->getData("cc_number_enc") : '',
            "cc_trans_id" => $payment ? $payment->getData("cc_trans_id") : ''

        );
    }

    /**
     * Detection de l'event
     * @param Varien_Event_Observer $observer observer object
     * @return boolean
     */
    public function SKFProcess($observer) {

        $orderIds = $observer->getData('order_ids');
        $order = Mage::getModel('sales/order')->load($orderIds);


        $lines = $this->getOrderLineDetails($order);
        $history_lines = $this->getHistoryLineDetails($order);
        $po_lines = $this->getPO($order);
        $shippingDetails = $this->getAddressShipping($order);
        $billingDetails = $this->getAddressBilling($order);
        $generalInfo = $this->getOrderInfo($order);
        $paymentInfo = $this->getPayment($order);
        $shippingInfo = $this->getShipping($order);
        $customerInfo = $this->getCustomer($order);

        Mage::log('[SKFConnect] - Insert en base soukflux', null, 'soukeo.log');


       $sqlGI = $this->buildSql($generalInfo, 'skf_orders', 'insert_brut');
        $res = $this->DB($sqlGI,$generalInfo);

       //if(!empty($billingDetails['address_id']) && $billingDetails['address_id'] != 0 ){
            $sqlBD = $this->buildSql($billingDetails, 'skf_address', 'insert_brut');
            $res = $this->DB($sqlBD,$billingDetails);
            Mage::log('[SKFConnect] - Insert en base soukflux'.$sqlBD, null, 'soukeo.log');
       //}
        
       //if(!empty($shippingDetails['address_id']) && $shippingDetails['address_id'] != 0 ){
            $sqlSD = $this->buildSql($shippingDetails, 'skf_address', 'insert_brut');
            $res = $this->DB($sqlSD,$shippingDetails);
            Mage::log('[SKFConnect] - Insert en base soukflux'.$sqlSD, null, 'soukeo.log');
       //}

        
        $sqlPI = $this->buildSql($paymentInfo, 'skf_payments', 'insert');
        $res = $this->DB($sqlPI,$paymentInfo);
        
        
//        $data = $this->buildSql($shippingInfo, 'skf_shippings', 'insert');
//        $res = $this->DB($data);
                
        //if(!empty($customerInfo['customer_id']) && $customerInfo['customer_id'] != 0 ){ 
            $sqlCI = $this->buildSql($customerInfo, 'skf_customers', 'insert');
            $res = $this->DB($sqlCI,$customerInfo);
       // }

        // Traitement des lignes de la commande

        foreach ($lines as $line) {
            $sqlOI= $this->buildSql($line, 'skf_order_items', 'insert_brut');
            $res = $this->DB($sqlOI,$line);
        }
        
        foreach ($po_lines as $po_line) {
            if(!empty($po_line['po_id']) && $po_line['po_id'] != 0 ){
                
                $sqlPOL = $this->buildSql($po_line, 'skf_po', 'insert_brut');
                $res = $this->DB($sqlPOL,$po_line);
            }
        }
        
        foreach ($history_lines as $history_line) {
                
                $sqlOH = $this->buildSql($history_line, 'skf_order_history', 'insert_brut');
                $res = $this->DB($sqlOH,$history_line);
                
            }

    }
    
    
    
    
    
    public function historiqueAvahis($order_id){
        
        $order = Mage::getModel('sales/order')->load($order_id);


        $lines = $this->getOrderLineDetails($order);
        $history_lines = $this->getHistoryLineDetails($order);
        $po_lines = $this->getPO($order);
        $shippingDetails = $this->getAddressShipping($order);
        $billingDetails = $this->getAddressBilling($order);
        $generalInfo = $this->getOrderInfo($order);
        $paymentInfo = $this->getPayment($order);
        $shippingInfo = $this->getShipping($order);
        $customerInfo = $this->getCustomer($order);

        Mage::log('[SKFConnect] - Insert en base soukflux', null, 'soukeo.log');


       $sqlGI = $this->buildSql($generalInfo, 'skf_orders', 'insert_brut');
        $res = $this->DB($sqlGI);

      if(!empty($billingDetails['address_id']) && $billingDetails['address_id'] != 0 ){
            $sqlBD = $this->buildSql($billingDetails, 'skf_address', 'insert_brut');
            $res = $this->DB($sqlBD);
            Mage::log('[SKFConnect] - Insert en base soukflux'.$sqlBD, null, 'soukeo.log');
       }
        
       if(!empty($shippingDetails['address_id']) && $shippingDetails['address_id'] != 0 ){
            $sqlSD = $this->buildSql($shippingDetails, 'skf_address', 'insert_brut');
            $res = $this->DB($sqlSD);
            Mage::log('[SKFConnect] - Insert en base soukflux'.$sqlSD, null, 'soukeo.log');
       }

        
        $sqlPI = $this->buildSql($paymentInfo, 'skf_payments', 'insert');
        $res = $this->DB($sqlPI);
        
        
//        $data = $this->buildSql($shippingInfo, 'skf_shippings', 'insert');
//        $res = $this->DB($data);
                
        //if(!empty($customerInfo['customer_id']) && $customerInfo['customer_id'] != 0 ){ 
            $sqlCI = $this->buildSql($customerInfo, 'skf_customers', 'insert');
            $res = $this->DB($sqlCI);
        //}

        // Traitement des lignes de la commande

        foreach ($lines as $line) {
            $sqlOI= $this->buildSql($line, 'skf_order_items', 'insert_brut');
            $res = $this->DB($sqlOI);
        }
        
        foreach ($po_lines as $po_line) {
            if(!empty($po_line['po_id']) && $po_line['po_id'] != 0 ){
                
                $sqlPOL = $this->buildSql($po_line, 'skf_po', 'insert_brut');
                $res = $this->DB($sqlPOL);
            }
        }
        
         foreach ($history_lines as $history_line) {
                
                $sqlOH = $this->buildSql($history_line, 'skf_order_history', 'insert_brut');
                $res = $this->DB($sqlOH);
                
            }

    }

}

?>
