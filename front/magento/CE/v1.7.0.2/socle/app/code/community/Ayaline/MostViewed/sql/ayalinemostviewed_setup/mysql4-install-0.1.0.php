<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('ayalinemostviewed/most_viewed')};
CREATE TABLE {$this->getTable('ayalinemostviewed/most_viewed')} (
  `most_viewed_id` int(10) unsigned NOT NULL auto_increment,
  `product_id` int(10) unsigned NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  `most_viewed_date` date NOT NULL default '0000-00-00',
  `most_viewed_count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`most_viewed_id`),
  UNIQUE KEY `UK_AYALINE_MOSTVIEWED` (`product_id`,`store_id`,`most_viewed_date`),
  CONSTRAINT `FK_AYALINE_MOSTVIEWED_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES {$this->getTable('catalog/product')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_AYALINE_MOSTVIEWED_STORE` FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core_store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


");
