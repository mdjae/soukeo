<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Model_Mysql4_MostViewed extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct() {
		$this->_init('ayalinemostviewed/most_viewed', 'most_viewed_id');
	}
	
	public function increaseMostViewedCount($productId, $storeId, $date, $nbViewed) {
		if(!$this->_getRow($productId, $storeId, $date)) {
			$this->_getWriteAdapter()->insert($this->getMainTable(), array(
				'product_id'	=> $productId,
				'store_id'		=> $storeId,
				'most_viewed_date'	=> $date,
				'most_viewed_count'	=> $nbViewed,
			));
		} else {
			$conditions = array();
			$conditions[] = $this->_getWriteAdapter()->quoteInto('product_id = ?', $productId);
			$conditions[] = $this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId);
			$conditions[] = $this->_getWriteAdapter()->quoteInto('most_viewed_date = ?', $date);
			
			$this->_getWriteAdapter()->update($this->getMainTable(), array('most_viewed_count' => new Zend_Db_Expr('most_viewed_count + ' . $nbViewed)), $conditions);
		}
	}
	
	public function decreaseMostViewedCount($productId, $storeId, $date, $nbViewed) {
		if(!$this->_getRow($productId, $storeId, $date)) {
			return false;
		}
		
		$conditions = array();
		$conditions[] = $this->_getWriteAdapter()->quoteInto('product_id = ?', $productId);
		$conditions[] = $this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId);
		$conditions[] = $this->_getWriteAdapter()->quoteInto('most_viewed_date = ?', $date);
		
		$this->_getWriteAdapter()->update($this->getMainTable(), array('most_viewed_count' => new Zend_Db_Expr('most_viewed_count - ' . $nbViewed)), $conditions);
		
		return true;
	}
	
	public function cleanOlderThan($storeId, $date) {
		$this->_getWriteAdapter()->delete(
			$this->getMainTable(),
			array(
				$this->_getWriteAdapter()->quoteInto('most_viewed_date < ?', $date),
				$this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId),
			)
		);
	}
	
	protected function _getRow($productId, $storeId, $date) {
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable())
			->where('product_id = ?', $productId)
			->where('store_id = ?', $storeId)
			->where('most_viewed_date = ?', $date)
		;
		return $this->_getReadAdapter()->fetchRow($select);
	}
}
