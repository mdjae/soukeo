<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_Mysql4_BestSalesIndex extends Mage_Core_Model_Mysql4_Abstract {

	const XML_PATH_GENERAL_NB_DAYS_EXPIRY		=	'ayalinebestsales/general/nb_days_expiry';

	protected function _construct() {
		$this->_init('ayalinebestsales/best_sales_index', 'index_id');
	}


	/**
	 * Table d'index
	 *
	 * @param int $storeId
	 */
	public function indexBestSales($storeId) {
		$indexSize = $this->_getIndexSize($storeId);

		for($i = 0; $i <= $indexSize; $i++) {
			$date = new Zend_Date();
			$date->subDay($i);

			$select = $this->_getReadAdapter()->select()
				->from(
					$this->getTable('ayalinebestsales/best_sales'),
					array('product_id', 'day' => new Zend_Db_Expr($i), 'store_id', 'nb_sales' => 'SUM(sales_count)'))
				->where('sales_date >= ?', $date->toString('y-MM-dd'))
				->where('store_id = ?', $storeId)
				->group('product_id')
			;
				
			$query = $select->insertFromSelect($this->getTable('ayalinebestsales/best_sales_index_idx'), array('product_id', 'day', 'store_id', 'nb_sales'));
			$this->_getWriteAdapter()->query($query);
		}

	}


	public function truncateIdxTable() {
		$this->_getWriteAdapter()->truncate($this->getTable('ayalinebestsales/best_sales_index_idx'));
	}

	public function renameTables() {
		$sql = "
			RENAME TABLE 
				`{$this->getTable('ayalinebestsales/best_sales_index_idx')}` TO `ayalinebestsales_best_sales_index_tmp`,
				`{$this->getMainTable()}` TO `{$this->getTable('ayalinebestsales/best_sales_index_idx')}`,
				`ayalinebestsales_best_sales_index_tmp` TO `{$this->getMainTable()}`;
		";

		$this->_getWriteAdapter()->query($sql);
	}

	protected function _getIndexSize($storeId) {
		$indexSize = Mage::getStoreConfig(self::XML_PATH_GENERAL_NB_DAYS_EXPIRY, $storeId);
		return $indexSize-1;
	}

	protected function _getRow($productId, $day, $storeId) {
		$select = $this->_getReadAdapter()->select()
		->from($this->getMainTable())
		->where('product_id = ?', $productId)
		->where('day = ?', $day)
		->where('store_id = ?', $storeId)
		;
		return $this->_getReadAdapter()->fetchRow($select);
	}
}