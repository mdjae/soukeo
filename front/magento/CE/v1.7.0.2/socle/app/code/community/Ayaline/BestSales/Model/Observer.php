<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_Observer extends Mage_Core_Model_Abstract
{
	/**
	 * Event : sales_order_save_before
	 *
	 * @param Varien_Event_Observer $observer
	 * @return Ayaline_BestSales_Model_Observer
	 */
	public function beforeSaveOrder(Varien_Event_Observer $observer) {
		$order = $observer->getEvent()->getOrder();
		if(!$order->getId()) {
			// The order is going to be saved
			Mage::register('ayalinebestsales_process_count', 1);
		}
	}

	/**
	 * Event : sales_order_save_after
	 *
	 * @param Varien_Event_Observer $observer
	 * @return Ayaline_BestSales_Model_Observer
	 */
	public function afterSaveOrder(Varien_Event_Observer $observer) {
		$order = $observer->getEvent()->getOrder();

		if(Mage::registry('ayalinebestsales_process_count') && $order->getId()) {
			// Increase sales counts
			/* @var $item Mage_Sales_Model_Order_Item */
			foreach($order->getAllVisibleItems() as $item) {
				if($item->getParentItem()) {
					continue;
				}

				$_productId = $item->getProductId();
				if(Mage::helper('core')->isModuleOutputEnabled('OrganicInternet_SimpleConfigurableProducts')) {	//	SCP ?
					if($item->getProductOptionByCode('info_buyRequest') && array_key_exists('cpid', $item->getProductOptionByCode('info_buyRequest'))) {	//	if it's a configurable product
						$infoBuyRequest = $item->getProductOptionByCode('info_buyRequest');
						$_productId = $infoBuyRequest['cpid'];	//	parent product id
					}
				}
				Mage::getSingleton('ayalinebestsales/bestSales')->increaseSalesCount($_productId, $item->getStoreId(), $item->getCreatedAt(), $item->getQtyOrdered());
			}
		}

		Mage::unregister('ayalinebestsales_process_count');

		return $this;
	}

	/**
	 * Decrease sales count
	 *
	 * Event : sales_order_item_cancel
	 *
	 * @param Varien_Event_Observer $observer
	 * @return Ayaline_BestSales_Model_Observer
	 */
	public function cancelOrderItem(Varien_Event_Observer $observer) {
		if(!Mage::getSingleton('ayalinebestsales/bestSales')->decreaseIfCanceled()) {
			return $this;
		}
		/* @var $item Mage_Sales_Model_Order_Item */
		$item = $observer->getEvent()->getItem();
		if($item->getParentItem()) {
			return $this;
		}

		$_productId = $item->getProductId();
		if(Mage::helper('core')->isModuleOutputEnabled('OrganicInternet_SimpleConfigurableProducts')) {	//	SCP ?
			if($item->getProductOptionByCode('info_buyRequest') && array_key_exists('cpid', $item->getProductOptionByCode('info_buyRequest'))) {	//	if it's a configurable product
				$infoBuyRequest = $item->getProductOptionByCode('info_buyRequest');
				$_productId = $infoBuyRequest['cpid'];	//	parent product id
			}
		}

		Mage::getSingleton('ayalinebestsales/bestSales')->decreaseSalesCount($_productId, $item->getStoreId(), $item->getCreatedAt(), $item->getQtyOrdered());

		return $this;
	}

}
