<?php

class Unirgy_DropshipMicrositePro_Model_Url extends Unirgy_DropshipMicrosite_Model_Url
{
    public function getRouteUrl($routePath = null, $routeParams = null)
    {
        $this->unsetData('route_params');

        if (isset($routeParams['_direct'])) {
            if (is_array($routeParams)) {
                $this->setRouteParams($routeParams, false);
            }
            return $this->getBaseUrl() . $routeParams['_direct'];
        }

        if (!is_null($routePath)) {
            $this->setRoutePath($routePath);
        }
        if (is_array($routeParams)) {
            $this->setRouteParams($routeParams, false);
        }

        $vendorPortalUrl = Mage::getStoreConfig('udropship/admin/vendor_portal_url');
        $rootPath = $this->getRoutePath($routeParams);
        $baseUrl = $this->getBaseUrl();
        if ($vendorPortalUrl) {
            $found = false;
            $rootPath = $this->getRoutePath($routeParams);
            foreach (array('sellers','udpo','udprod','umicrosite','udropship') as $pref) {
                if (0 === strpos($rootPath, $pref.'/vendor/')) {
                    $rootPath = substr($rootPath, strlen($pref.'/vendor/'));
                    $found = true;
                    break;
                }
            }
            if ($found) {
                $baseUrl = $vendorPortalUrl;
            }
        }
        $url = $baseUrl . $rootPath;

        return $url;
    }
}