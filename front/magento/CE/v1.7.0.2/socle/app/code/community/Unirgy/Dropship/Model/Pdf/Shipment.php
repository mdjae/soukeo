<?php

class Unirgy_Dropship_Model_Pdf_Shipment extends Mage_Sales_Model_Order_Pdf_Shipment
{
    protected function insertLogo(&$page, $store = null)
    {
        if (Mage::helper('udropship')->compareMageVer('1.7.0.0', '1.12.0', '>=')) {
            return $this->insertLogo17($page, $store);
        } else {
            return $this->insertLogoLT17($page, $store);
        }
    }
    protected function insertLogoLT17(&$page, $store = null)
    {
        $image = Mage::getStoreConfig('sales/identity/logo', $store);
        if ($image) {
            $image = Mage::getStoreConfig('system/filesystem/media', $store) . '/sales/store/logo/' . $image;
            if (is_file($image)) {
                $image = Zend_Pdf_Image::imageWithPath($image);
                $page->drawImage($image, 25, 800, 125, 825);
            }
        }
        //return $page;
    }
    protected function insertLogo17(&$page, $store = null)
    {
        $this->y = $this->y ? $this->y : 800;
        $image = Mage::getStoreConfig('sales/identity/logo', $store);
        if ($image) {
            $image = Mage::getBaseDir('media') . '/sales/store/logo/' . $image;
            if (is_file($image)) {
                $image       = Zend_Pdf_Image::imageWithPath($image);
                //$page->drawImage($image, 25, 800, 125, 825);
                $page->drawImage($image, 25, 780, 125, 825);
            }
        }
    }
    protected function _setFontRegular($object, $size = 8)
    {
        if (!$this->getUseFont()) {
            return parent::_setFontRegular($object, $size);
        }
        $font = Zend_Pdf_Font::fontWithName(constant('Zend_Pdf_Font::FONT_HELVETICA'));
        $object->setFont($font, $size);
        return $font;
    }

    protected function _setFontBold($object, $size = 8)
    {
        if (!$this->getUseFont()) {
            return parent::_setFontBold($object, $size);
        }
        $font = Zend_Pdf_Font::fontWithName(constant('Zend_Pdf_Font::FONT_HELVETICA_BOLD'));
        $object->setFont($font, $size);
        return $font;
    }

    protected function _setFontItalic($object, $size = 8)
    {
        if (!$this->getUseFont()) {
            return parent::_setFontItalic($object, $size);
        }
        $font = Zend_Pdf_Font::fontWithName(constant('Zend_Pdf_Font::FONT_HELVETICA_ITALIC'));
        $object->setFont($font, $size);
        return $font;
    }

    protected $_currentShipment;

    public function getPdf($shipments = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $pdf = new Zend_Pdf();
        if (method_exists($this, '_setPdf')) $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);
        foreach ($shipments as $shipment) {
            if ($shipment->getStoreId()) {
                Mage::app()->getLocale()->emulate($shipment->getStoreId());
            }
            $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
            $pdf->pages[] = $page;

            $this->_currentShipment = $shipment;

            $order = $shipment->getOrder();

            /* Add image */
            $this->insertLogo($page, $shipment->getStore());

            /* Add address */
            $this->insertAddress($page, $shipment->getStore());

            /* Add head */
            $this->insertOrder($page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID, $order->getStoreId()));

            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->_setFontBold($page, $size=10);           
            //$page->drawText(Mage::helper('sales')->__('Bon de livraison # ') . $shipment->getIncrementId(), 35, 780, 'UTF-8');
            $page->drawText(Mage::helper('sales')->__('Bon de livraison # ') . $shipment->getIncrementId(), 35, 740, 'UTF-8');
            
            //Insertion du nom vu vendeur
            $vendorID = $shipment->getData('udropship_vendor');
            $vendorData = Mage::getModel('udropship/vendor')->getcollection()->addFieldToFilter('vendor_id', $vendorID)->getData();
            $vendorData = $vendorData[0];
            $vendorName = $vendorData['vendor_name'];
            $page->drawText(Mage::helper('sales')->__('Vendeur : ') . strtoupper($vendorName), 285, 740, 'UTF-8');
            $this->_setFontRegular($page);

            /* Add table */
            $page->setFillColor(new Zend_Pdf_Color_Html('#d9d9d9'));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);


            /* Add table head */
            //Insertion du cadre de l'en-tête de tableau des articles
            //$page->drawRectangle(25, $this->y, 570, $this->y-15);
            $page->drawRectangle(25, 510, 570, 495);
            $this->y = 510;
            $this->y -=10;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $page->drawText(Mage::helper('sales')->__('Quantité'), 35, $this->y, 'UTF-8');
            $page->drawText(Mage::helper('sales')->__('Article'), 100, $this->y, 'UTF-8');
            $page->drawText(Mage::helper('sales')->__('Référence'), 520, $this->y, 'UTF-8');

            $this->y -=15;

            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            /* Add body */
            foreach ($shipment->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }

                if ($this->y<65+10*count($this->getCustomTextArray($shipment))) {
                    $this->drawCustomText($shipment, $page);
                    $page = $this->newPage(array('table_header' => true));
                }

                /* Draw item */
                $page = $this->_drawItem($item, $page, $order);
                //Insertion d'un cadre autour de la ligne produit
                $page->drawLine(25, $this->y, 25, $this->y+15);
                $page->drawLine(25, $this->y, 570, $this->y);
                $page->drawLine(570, $this->y, 570, $this->y+15);
                $this->y -= 10;
            }
            $this->y -= 10;
            $this->drawCustomText($shipment, $page);
        }

        //Insertion d'un cadre pour info : CECI N'EST PAS UNE FACTURE 
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.75));       
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, 150, 570, 110);
        $page->setFillColor(new Zend_Pdf_Color_Rgb(255, 0, 0));
        $this->_setFontBold($page, $size=10);
        $page->drawText(Mage::helper('sales')->__('ATTENTION'), 270, 138, 'UTF-8');
        $this->_setFontRegular($page, $size=10);
        $page->drawText(Mage::helper('sales')->__('Ceci n\'est pas une facture'), 240, 128, 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('La facture correspondant à ce Bon de Livraison devra vous-être remise par le vendeur'), 120, 118, 'UTF-8');

        //Insertion des mentions légales
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));       
        $this->_setFontBold($page, $size=8);
        $page->drawText(Mage::helper('sales')->__('Le site Avahis.com est édité par SOUKEO '), 240, 45, 'UTF-8');
        $this->_setFontRegular($page, $size=8);
        $page->drawText(Mage::helper('sales')->__('SARL au capital social de 100€ - RCS de Saint-Denis de la Réunion n°791 331 267'), 190, 35, 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('Siège social : 117 rue Monthyon - 97400 SAINT-DENIS'), 220, 25, 'UTF-8');
        
        $this->_afterGetPdf();

        if ($shipment->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }
        return $pdf;
    }

    protected function insertOrder(&$page, $order, $putOrderId = true)
    {
        /* @var $order Mage_Sales_Model_Order */
        $page->setFillColor(new Zend_Pdf_Color_Html('#bbbbbb'));

        //Insertion du cadre contenant le n°de bon de livraison, de commande et la date de commande
        //$page->drawRectangle(25, 790, 570, 755);
        $page->drawRectangle(25, 750, 570, 715);       
        
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);

        if (Mage::helper('udropship')->isUdpoActive()) {
            $po = Mage::helper('udpo')->getShipmentPo($this->_currentShipment);
        }

        if ($putOrderId) {
            //$page->drawText(Mage::helper('sales')->__('Order # ').$order->getRealOrderId(), 35, 770, 'UTF-8');
            $page->drawText(Mage::helper('sales')->__('Order # ').$order->getRealOrderId(), 35, 730, 'UTF-8');
            if (!empty($po)) {
                //$page->drawText(Mage::helper('udpo')->__('Bon de commande # ').$po->getIncrementId(), 135, 770, 'UTF-8');
                $page->drawText(Mage::helper('udpo')->__('Bon de commande # ').$po->getIncrementId(), 285, 730, 'UTF-8');
            }
        }
        //$page->drawText(Mage::helper('sales')->__('Order Date: ') . date( 'D M j Y', strtotime( $order->getCreatedAt() ) ), 35, 760, 'UTF-8');
        //$page->drawText(Mage::helper('sales')->__('Order Date: ') . date( 'D M j Y', strtotime( $order->getCreatedAt() ) ), 35, 720, 'UTF-8');
        //$page->drawText(Mage::helper('sales')->__('Date de commande : ') . Mage::helper('core')->formatDate($order->getCreatedAtStoreDate(), 'medium', false), 35, 760, 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('Date de commande : ') . Mage::helper('core')->formatDate($order->getCreatedAtStoreDate(), 'medium', false), 35, 720, 'UTF-8');
        
        if (!empty($po)) {
            //$page->drawText(Mage::helper('udpo')->__('Date d\'édition du bon de commande : ') . Mage::helper('core')->formatDate($po->getCreatedAtStoreDate(), 'medium', false), 135, 760, 'UTF-8');
            $page->drawText(Mage::helper('udpo')->__('Date d\'édition du bon de commande : ') . Mage::helper('core')->formatDate($po->getCreatedAtStoreDate(), 'medium', false), 285, 720, 'UTF-8');

        }

        $page->setFillColor(new Zend_Pdf_Color_Html('#d9d9d9'));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        //Insertion du cadre autour du texte "VENDU A"
        //$page->drawRectangle(25, 755, 275, 730);
        $page->drawRectangle(25, 705, 275, 680);
        //Insertion du cadre autour du texte "LIVRE A"
        //$page->drawRectangle(275, 755, 570, 730);
        $page->drawRectangle(275, 705, 570, 680);

        /* Calculate blocks info */

        /* Billing Address */
        $billingAddress = $this->_formatAddress(
            Mage::helper('udropship')->formatCustomerAddress($order->getBillingAddress(), 'pdf', $this->_currentShipment->getUdropshipVendor())
        );

        /* Payment */
        $paymentInfo = Mage::helper('payment')->getInfoBlock($order->getPayment())
            ->setIsSecureMode(true)
            ->toPdf();
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);
        foreach ($payment as $key=>$value){
            if (strip_tags(trim($value))==''){
                unset($payment[$key]);
            }
        }
        reset($payment);

        /* Shipping Address and Method */
        if (!$order->getIsVirtual()) {
            /* Shipping Address */
            $shippingAddress = $this->_formatAddress(
                Mage::helper('udropship')->formatCustomerAddress($order->getShippingAddress(), 'pdf', $this->_currentShipment->getUdropshipVendor())
            );

            $shippingMethod  = $order->getShippingDescription();
        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontBold($page);
        //Insertion du texte "VENDU A"
        //$page->drawText(Mage::helper('sales')->__('SOLD TO:'), 35, 740 , 'UTF-8');
        $page->drawText(Mage::helper('sales')->__('SOLD TO:'), 35, 690 , 'UTF-8');

        if (!$order->getIsVirtual()) {
            //Insertion du texte "LIVRE A"
            //$page->drawText(Mage::helper('sales')->__('SHIP TO:'), 285, 740 , 'UTF-8');
            $page->drawText(Mage::helper('sales')->__('SHIP TO:'), 285, 690 , 'UTF-8');
        }
        else {
            $page->drawText(Mage::helper('sales')->__('Payment Method:'), 285, 740 , 'UTF-8');
        }

        if (!$order->getIsVirtual()) {
            $y = 730 - (max(count($billingAddress), count($shippingAddress)) * 10 + 5);
        }
        else {
            $y = 730 - (count($billingAddress) * 10 + 5);
        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        //Insertion du cadre autour de l'adresse de livraison et de l'adresse de facturation
        //$page->drawRectangle(25, 730, 570, $y);
        $page->drawRectangle(25, 680, 570, 590);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);
        //$this->y = 720;
        $this->y = 670;

        foreach ($billingAddress as $value){
            if ($value!=='') {
                //Insertion de l'adresse de facturation
                $page->drawText(strip_tags(ltrim($value)), 35, $this->y, 'UTF-8');
                $this->y -=10;
            }
        }

        if (!$order->getIsVirtual()) {
            //$this->y = 720;
            $this->y = 670;
            foreach ($shippingAddress as $value){
                if ($value!=='') {
                    //Insertion de l'adresse de livraison    
                    $page->drawText(strip_tags(ltrim($value)), 285, $this->y, 'UTF-8');
                    $this->y -=10;
                }
            }

            $page->setFillColor(new Zend_Pdf_Color_Html('#d9d9d9'));
            $page->setLineWidth(0.5);
            //Insertion du cadre autour du texte "Mode de paiement"
            //$page->drawRectangle(25, $this->y, 275, $this->y-25);
            $page->drawRectangle(25, 580, 275, 555);
            //Insertion du cadre autour du texte "Mode de livraison"
            //$page->drawRectangle(275, $this->y, 570, $this->y-25);
            $page->drawRectangle(275, 580, 570, 555);
            
            $this->y = 580;
            $this->y -=10;
            $this->_setFontBold($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            //Insertion du texte "Mode de paiement"
            $page->drawText(Mage::helper('sales')->__('MODE DE PAIEMENT :'), 35, $this->y, 'UTF-8');
            //Insertion du texte "Mode de livraison"
            $page->drawText(Mage::helper('sales')->__('MODE DE LIVRAISON :'), 285, $this->y , 'UTF-8');

            $this->y -=10;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));

            $this->_setFontRegular($page);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            $paymentLeft = 35;
            $yPayments   = $this->y - 15;
        }
        else {
            $yPayments   = 720;
            $paymentLeft = 285;
        }

        foreach ($payment as $value){
            if (trim($value)!=='') {
                //Insertion du mode de paiement    
                $page->drawText(strip_tags(trim($value)), $paymentLeft, $yPayments, 'UTF-8');
                $yPayments -=10;
            }
        }

        if (!$order->getIsVirtual()) {
            $this->y -=15;

            //Insertion du mode de livraison
            $page->drawText($shippingMethod, 285, $this->y, 'UTF-8');

            $yShipments = $this->y;

            $curVendor = Mage::helper('udropship')->getVendor($this->_currentShipment->getUdropshipVendor());
            if (!$curVendor->getHidePackingslipAmount()) {
                $totalShippingChargesText = "(" . Mage::helper('sales')->__('Total Shipping Charges') . " " . $order->getBaseCurrency()->formatTxt($order->getBaseShippingAmount()) . ")";

                $page->drawText($totalShippingChargesText, 285, $yShipments-7, 'UTF-8');
            }
            $yShipments -=10;
            $tracks = $order->getTracksCollection();
            if (count($tracks)) {
                $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                $page->setLineWidth(0.5);
                $page->drawRectangle(285, $yShipments, 510, $yShipments - 10);
                $page->drawLine(380, $yShipments, 380, $yShipments - 10);
                //$page->drawLine(510, $yShipments, 510, $yShipments - 10);

                $this->_setFontRegular($page);
                $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                //$page->drawText(Mage::helper('sales')->__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Title'), 290, $yShipments - 7, 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Number'), 385, $yShipments - 7, 'UTF-8');

                $yShipments -=17;
                $this->_setFontRegular($page, 6);
                foreach ($order->getTracksCollection() as $track) {

                    $CarrierCode = $track->getCarrierCode();
                    if ($CarrierCode!='custom')
                    {
                        $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($CarrierCode);
                        $carrierTitle = $carrier->getConfigData('title');
                    }
                    else
                    {
                        $carrierTitle = Mage::helper('sales')->__('Custom Value');
                    }

                    //$truncatedCarrierTitle = substr($carrierTitle, 0, 35) . (strlen($carrierTitle) > 35 ? '...' : '');
                    $truncatedTitle = substr($track->getTitle(), 0, 45) . (strlen($track->getTitle()) > 45 ? '...' : '');
                    //$page->drawText($truncatedCarrierTitle, 285, $yShipments , 'UTF-8');
                    $page->drawText($truncatedTitle, 300, $yShipments , 'UTF-8');
                    $page->drawText($track->getNumber(), 395, $yShipments , 'UTF-8');
                    $yShipments -=7;
                }
            } else {
                $yShipments -= 7;
            }

            $currentY = min($yPayments, $yShipments);

            // replacement of Shipments-Payments rectangle block
            $page->drawLine(25, $this->y + 15, 25, $currentY);
            $page->drawLine(25, $currentY, 570, $currentY);
            $page->drawLine(570, $currentY, 570, $this->y + 15);

            $this->y = $currentY;
            $this->y -= 15;            
        }
    }

    public function getSpdTextWidth($string, $page)
    {
        $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
        $characters = array();
        for ($i = 0; $i < strlen($drawingString); $i++) {
            $characters[] = (ord($drawingString[$i++]) << 8) | ord($drawingString[$i]);
        }
        $font = $page->getFont();
        $glyphs = $font->glyphNumbersForCharacters($characters);
        $widths = $font->widthsForGlyphs($glyphs);
        $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $page->getFontSize();
        return $stringWidth;
    }

    protected $_customTextArr = array();
    protected function getCustomTextArray($shipment)
    {
        if (!isset($this->_customTextArr[$shipment->getId()])) {
            $customText = Mage::getStoreConfig('udropship/vendor/packingslip_custom_text', $shipment->getStoreId());
            $curVendor = Mage::helper('udropship')->getVendor($shipment->getUdropshipVendor());
            $vUsePSCT = $curVendor->getData('use_packingslip_custom_text');
            if ($vUsePSCT==1) {
                $customText = $curVendor->getData('packingslip_custom_text');
            } elseif ($vUsePSCT==0) {
                $customText = '';
            }
            $_customTextArr = preg_split("/\r\n|\r|\n/", $customText);
            $customTextArr = array();
            foreach ($_customTextArr as $_cti) {
                if (($_cti = trim($_cti))) {
                    $customTextArr[] = $_cti;
                }
            }
            $this->_customTextArr[$shipment->getId()] = $customTextArr;
        }
        return $this->_customTextArr[$shipment->getId()];
    }

    protected function drawCustomText($shipment, $page)
    {
        $this->_setFontBold($page);
        foreach ($this->getCustomTextArray($shipment) as $i => $__cti) {
            if (($_cti = preg_replace('/\[%\s*bold\s*%\]/', '', $__cti))!=$__cti) {
                $this->_setFontBold($page);
            } else {
                $this->_setFontRegular($page);
            }
            $_ctiWidth = $this->getSpdTextWidth($_cti, $page);
            $page->drawText($_cti, 35+(545-$_ctiWidth)/2, $this->y-(30+$i*10), 'UTF-8');
        }
        $this->_setFontRegular($page);
    }
}
