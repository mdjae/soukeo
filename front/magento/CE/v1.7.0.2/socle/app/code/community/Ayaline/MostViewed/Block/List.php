<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */

class Ayaline_MostViewed_Block_List extends Mage_Catalog_Block_Product_List {
	
	protected $_collection;
	
	public function _construct(){
		parent::_construct();		
		$this->_collection = $this->_getProductCollection();
	}
	
	public function getListName() {
		return Mage::helper('ayalinemostviewed')->__('Most viewed');
	}
	
	protected function _getProductCollection() {
		if(!$this->_collection) {
			/* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
			$collection = parent::_getProductCollection();
			
			$collection = Mage::getSingleton('ayalinemostviewed/mostViewed')
				->addToProductCollection($collection, true, $this->_getLastXDays() - 1)
			;
			$collection->getSelect()->order('most_viewed DESC');
			
			$this->_collection = $collection;
		}
		
		return $this->_collection;
	}
	
	protected function _getLastXDays() {
		return Mage::getStoreConfig('ayalinemostviewed/list/size');
	}
	
	protected function _beforeToHtml() {
		parent::_beforeToHtml();
		
		$this->getToolbarBlock()->setData('_current_grid_mode', Mage::getStoreConfig('ayalinemostviewed/list/display_mode'));
		
		return $this;
	}
}
