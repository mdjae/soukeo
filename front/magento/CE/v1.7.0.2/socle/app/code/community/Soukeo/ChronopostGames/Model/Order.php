<?php
/* SOUKEO SAS
 * @atuhor : Silicon Village Team (mamode@silicon-village.fr)
 * @package : Avahis
 */

class Soukeo_ChronopostGames_Model_Order extends Mage_Sales_Model_Order{
 
    public function setState($state, $status = false, $comment = '', $isCustomerNotified = null){
        Mage::dispatchEvent('sales_order_status_change', array('order' => $this, 'state' => $state, 'status' => $status, 'comment' => $comment, 'isCustomerNotified' => $isCustomerNotified));
        Mage::log('[ShippingLabel] - '.date('d-m-Y H:i:s T'), null, 'soukeo.log');
        
        return parent::setState($state, $status, $comment, $isCustomerNotified);
        
    }

}

?>
