<?php

class Unirgy_Dropship_Block_Vendor_Preferences extends Mage_Core_Block_Template
{

	
    public function getFieldsets()
    {
        $hlp = Mage::helper('udropship');

        $visible = Mage::getStoreConfig('udropship/vendor/visible_preferences');
        $visible = $visible ? explode(',', $visible) : false;

        $fieldsets = array();
        foreach (Mage::getConfig()->getNode('global/udropship/vendor/fieldsets')->children() as $code=>$node) {
            if ($node->modules && !$hlp->isModulesActive((string)$node->modules)
                || $node->hide_modules && $hlp->isModulesActive((string)$node->hide_modules)
                || $node->is('hidden')
            ) {
                continue;
            }
            $fieldsets[$code] = array(
                'position' => (int)$node->position*10,
                'legend' => (string)$node->legend,
            );
        }
        foreach (Mage::getConfig()->getNode('global/udropship/vendor/fields')->children() as $code=>$node) {
            if (empty($fieldsets[(string)$node->fieldset]) || $node->is('disabled')) {
                continue;
            }
            if ($node->modules && !$hlp->isModulesActive((string)$node->modules)
                || $node->hide_modules && $hlp->isModulesActive((string)$node->hide_modules)
            ) {
                continue;
            }
            if ($visible && !in_array($code, $visible)) {
                continue;
            }
            $type = $node->type ? (string)$node->type : 'text';
            $type = ($type == 'wysiwyg' && !$hlp->isWysiwygAllowed()) ? 'textarea' : $type;

            $field = array(
                'position' => (int)$node->position,
                'type' => $type,
                'name' => $node->name ? (string)$node->name : $code,
                'label' => (string)$node->label,
                'class' => (string)$node->class,
                'note' => (string)$node->note,
            );
            switch ($type) {
            case 'select': case 'multiselect': case 'checkboxes':
                $source = Mage::getSingleton($node->source_model ? (string)$node->source_model : 'udropship/source');
                if (is_callable(array($source, 'setPath'))) {
                    $source->setPath($node->source ? (string)$node->source : $code);
                }
                $field['options'] = $source->toOptionArray();
                break;
            }
            $fieldsets[(string)$node->fieldset]['fields'][$code] = $field;
        }

        $fieldsets['account'] = array(
            'position' => 0,
            'legend' => 'Informations du Compte',
            'fields' => array(
                'vendor_name' => array(
                    'position' => 1,
                    'name' => 'vendor_name',
                    'type' => 'text',
                    'label' => 'Nom de l\'entreprise',
                ),
                'civilite' => array(
                    'position' => 2,
                    'name' => 'civilite',
                    'type' => 'select',
                    'label' => 'Civilité du contact principal',
                    'options' => Mage::getSingleton('udropship/source')->setPath('civilite')->toOptionArray(),
                ),                
                'vendor_attn' => array(
                    'position' => 3,
                    'name' => 'vendor_attn',
                    'type' => 'text',
                    'label' => 'Nom du contact principal',
                ),
                'vendor_attn2' => array(
                    'position' => 4,
                    'name' => 'vendor_attn2',
                    'type' => 'text',
                    'label' => 'Prénom du contact principal',
                ),
                'email' => array(
                    'position' => 5,
                    'name' => 'email',
                    'type' => 'text',
                    'label' => 'Adresse e-mail',
                ),
                'password' => array(
                    'position' => 6,
                    'name' => 'password',
                    'type' => 'text',
                    'label' => 'Mot de passe',
                ),
                'telephone' => array(
                    'position' => 7,
                    'name' => 'telephone',
                    'type' => 'text',
                    'label' => 'Téléphone principal',
                ),
                'telephone2' => array(
                    'position' => 8,
                    'name' => 'telephone2',
                    'type' => 'text',
                    'label' => 'Téléphone secondaire',
                ),
            ),
        );

        //$countries = Mage::getModel('adminhtml/system_config_source_country')->toOptionArray();
        $countries  = Mage::getSingleton('udmspro/source')->setPath('country_id')->toOptionArray();
        
        $countryId = Mage::registry('vendor_data') ? Mage::registry('vendor_data')->getCountryId() : null;
        if (!$countryId) {
            $countryId = Mage::getStoreConfig('general/country/default');
        }

        $regionCollection = Mage::getModel('directory/region')
            ->getCollection()
            ->addCountryFilter($countryId);

        $regions = $regionCollection->toOptionArray();

        if ($regions) {
            $regions[0]['label'] = $hlp->__('Please select state...');
        } else {
            $regions = array(array('value'=>'', 'label'=>''));
        }

        $fieldsets['shipping_origin'] = array(
            'position' => 10,
            'legend' => 'Adresse d\'Expédition des Marchandises',
            'fields' => array(
                'street' => array(
                    'position' => 1,
                    'name' => 'street',
                    'type' => 'textarea',
                    'label' => 'N° et rue',
                ),
                'city' => array(
                    'position' => 2,
                    'name' => 'city',
                    'type' => 'text',
                    'label' => 'Ville',
                ),
                'zip' => array(
                    'position' => 3,
                    'name' => 'zip',
                    'type' => 'text',
                    'label' => 'Code postal',
                ),
                'country_id' => array(
                    'position' => 4,
                    'name' => 'country_id',
                    'type' => 'select',
                    'label' => 'Country',
                    'options' => $countries,
                ),
            ),
        );

        $fieldsets['billing_address'] = array(
            'position' => 20,
            'legend' => 'Adresse de Facturation',
            'fields' => array(
                'billing_use_shipping' => array(
                    'position' => -1,
                    'name' => 'billing_use_shipping',
                    'type' => 'select',
                    'label' => 'Identique à l\'adresse d\'expédition',
                    'options' => Mage::getSingleton('udropship/source')->setPath('billing_use_shipping')->toOptionArray(),
                    'depend_select' => 1,
                    'field_config' => array(
                        'depend_fields' => array(
                            'billing_vendor_attn' => '0',
                            'billing_street' => '0',
                            'billing_city' => '0',
                            'billing_zip' => '0',
                            'billing_country_id' => '0',
                            'billing_region_id' => '0',
                            'billing_email' => '0',
                            'billing_telephone' => '0',
                            'billing_fax' => '0',
                        )
                    )
                ),
                'billing_vendor_attn' => array(
                    'position' => 0,
                    'name' => 'billing_vendor_attn',
                    'type' => 'text',
                    'label' => 'Nom et prénom du contact facturation',
                    'note'  => 'Laisser vide pour utiliser le nom du contact principal'
                ),
                'billing_street' => array(
                    'position' => 1,
                    'name' => 'billing_street',
                    'type' => 'textarea',
                    'label' => 'N° et rue',
                ),
                'billing_city' => array(
                    'position' => 2,
                    'name' => 'billing_city',
                    'type' => 'text',
                    'label' => 'City',
                ),
                'billing_zip' => array(
                    'position' => 3,
                    'name' => 'billing_zip',
                    'type' => 'text',
                    'label' => 'Code postal',
                ),
                'billing_country_id' => array(
                    'position' => 4,
                    'name' => 'billing_country_id',
                    'type' => 'select',
                    'label' => 'Country',
                    'options' => $countries,
                ),
                'billing_email' => array(
                    'position' => 6,
                    'name' => 'billing_email',
                    'type' => 'text',
                    'label' => 'Adresse e-mail',
                    'note'  => 'Laisser vide pour utiliser l\'adresse e-mail par défaut'
                ),
                'billing_telephone' => array(
                    'position' => 7,
                    'name' => 'billing_telephone',
                    'type' => 'text',
                    'label' => 'Téléphone',
                    'note'  => 'Laisser vide pour utiliser le n° de téléphone par défaut'
                ),
                'billing_fax' => array(
                    'position' => 8,
                    'name' => 'billing_fax',
                    'type' => 'text',
                    'label' => 'Fax'
                ),
            ),
        );
		
		 $fieldsets['entreprise_info'] = array(
            'position' => 30,
            'legend' => 'Informations du Vendeur',
            'fields' => array(
               	'formjuridique' => array(
                    'position' => 0,
                    'name' => 'formjuridique',
                    'type' => 'text',
                    'label' => 'Forme juridique',
                ),
                'raisonsocial' => array(
                    'position' => 1,
                    'name' => 'raisonsocial',
                    'type' => 'text',
                    'label' => 'Raison sociale',
                ),
                'siret' => array(
                    'position' => 2,
                    'name' => 'siret',
                    'type' => 'text',
                    'label' => 'N° de Siret',
                ),
                  'capital' => array(
                    'position' => 3,
                    'name' => 'capital',
                    'type' => 'text',
                    'label' => 'Capital',
                ),
                  'siteinternet' => array(
                    'position' => 4,
                    'name' => 'siteinternet',
                    'type' => 'text',
                    'label' => 'Site internet',
                ),
                  'tvaintra' => array(
                    'position' => 5,
                    'name' => 'tvaintra',
                    'type' => 'text',
                    'label' => 'Numéro de tva intracommunautaire',
                ),
                'logo' => array(
                    'position' => 6,
                    'name' => 'logo',
                    'type' => 'image',
                    'label' => 'Votre logo',			
                ),
            ),
        );
		
            $fieldsets['entreprise_rib'] = array(
                'position' => 40,
                'legend' => 'Informations Bancaires',
                'fields' => array(
               	    'ribdomiciliation' => array(
                        'position' => 0,
                        'name' => 'ribdomiciliation',
                        'type' => 'text',
                        'label' => 'Domiciliation',
                    ),
                    'ribcodebanque' => array(
                        'position' => 0,
                        'name' => 'ribcodebanque',
                        'type' => 'text',
                        'label' => 'Code banque',
                    ),
                    'ribcodeguichet' => array(
                        'position' => 1,
                        'name' => 'ribcodeguichet',
                        'type' => 'text',
                        'label' => 'Code guichet',
                    ),
                    'ribnumcompte' => array(
                        'position' => 1,
                        'name' => 'ribnumcompte',
                        'type' => 'text',
                        'label' => 'Numéro de compte',
                    ),
                    'ribcle' => array(
                        'position' => 1,
                        'name' => 'ribcle',
                        'type' => 'text',
                        'label' => 'Clé rib',
                    ),
                    'ribiban' => array(
                        'position' => 1,
                        'name' => 'ribiban',
                        'type' => 'text',
                        'label' => 'IBAN',
                    ),
                    'ribbic' => array(
                        'position' => 1,
                        'name' => 'ribbic',
                        'type' => 'text',
                        'label' => 'BIC',
                    ),                
				    'ribtitulaire' => array(
                        'position' => 1,
                        'name' => 'ribtitulaire',
                        'type' => 'text',
                        'label' => 'Titulaire',
                    ),				
                ),
            );
            
        Mage::dispatchEvent('udropship_vendor_front_preferences', array(
            'fieldsets'=>&$fieldsets
        ));

        uasort($fieldsets, array($hlp, 'usortByPosition'));
        foreach ($fieldsets as $k=>$v) {
            if (empty($v['fields'])) {
                continue;
            }
            uasort($v['fields'], array($hlp, 'usortByPosition'));
        }

        return $fieldsets;
    }

    public function getDependSelectJs($htmlId, $field)
    {
        $html = '';
        $fc = (array)@$field['field_config'];
        if (isset($fc['depend_fields']) && ($dependFields = (array)$fc['depend_fields'])
            || isset($fc['hide_depend_fields']) && ($hideDependFields = (array)$fc['hide_depend_fields'])
        ) {
            if (!empty($dependFields)) {
                foreach ($dependFields as &$dv) {
                    $dv = $dv!='' ? explode(',', $dv) : array('');
                }
                unset($dv);
                $dfJson = Zend_Json::encode($dependFields);
            } else {
                $dfJson = '{}';
            }
            if (!empty($hideDependFields)) {
                foreach ($hideDependFields as &$dv) {
                    $dv = $dv!='' ? explode(',', $dv) : array('');
                }
                unset($dv);
                $hideDfJson = Zend_Json::encode($hideDependFields);
            } else {
                $hideDfJson = '{}';
            }
            $html .=<<<EOT
<script type="text/javascript">
document.observe("dom:loaded", function() {
	var df = \$H($dfJson);
	var hideDf = \$H($hideDfJson);
	var enableDisable = function (pair, flag) {
        if ($(pair.key) && (trElem = $(pair.key).up("tr"))) {
            if (flag == (\$A(pair.value).indexOf($('{$htmlId}').value) != -1)) {
                trElem.show()
                trElem.select('select').invoke('enable')
                trElem.select('input').invoke('enable')
                trElem.select('textarea').invoke('enable')
            } else {
                trElem.hide()
                trElem.select('select').invoke('disable')
                trElem.select('input').invoke('disable')
                trElem.select('textarea').invoke('disable')
            }
        }
    }
	var syncDependFields = function() {
		df.each(function(pair){
			enableDisable(pair, true);
		});
		hideDf.each(function(pair){
			enableDisable(pair, false);
		});
	}
    $('{$htmlId}').observe('change', syncDependFields)
    syncDependFields()
})
</script>
EOT;
        }
        return $html;
    }
}