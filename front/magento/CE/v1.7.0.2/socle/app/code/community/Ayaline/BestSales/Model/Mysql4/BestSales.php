<?php
/**
 * created : 07/04/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
class Ayaline_BestSales_Model_Mysql4_BestSales extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct() {
		$this->_init('ayalinebestsales/best_sales', 'best_sales_id');
	}
	
	public function increaseSalesCount($productId, $storeId, $date, $qty) {
		if(!$this->_getRow($productId, $storeId, $date)) {
			$this->_getWriteAdapter()->insert($this->getMainTable(), array(
				'product_id'	=> $productId,
				'store_id'		=> $storeId,
				'sales_date'	=> $date,
				'sales_count'	=> $qty,
			));
		} else {
			$conditions = array();
			$conditions[] = $this->_getWriteAdapter()->quoteInto('product_id = ?', $productId);
			$conditions[] = $this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId);
			$conditions[] = $this->_getWriteAdapter()->quoteInto('sales_date = ?', $date);
			
			$this->_getWriteAdapter()->update($this->getMainTable(), array('sales_count' => new Zend_Db_Expr('sales_count + ' . $qty)), $conditions);
		}
	}
	
	public function decreaseSalesCount($productId, $storeId, $date, $qty) {
		if(!$this->_getRow($productId, $storeId, $date)) {
			return false;
		}
		
		$conditions = array();
		$conditions[] = $this->_getWriteAdapter()->quoteInto('product_id = ?', $productId);
		$conditions[] = $this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId);
		$conditions[] = $this->_getWriteAdapter()->quoteInto('sales_date = ?', $date);
		
		$this->_getWriteAdapter()->update($this->getMainTable(), array('sales_count' => new Zend_Db_Expr('sales_count - ' . $qty)), $conditions);
		
		return true;
	}
	
	public function cleanOlderThan($storeId, $date) {
		$this->_getWriteAdapter()->delete(
			$this->getMainTable(),
			array(
				$this->_getWriteAdapter()->quoteInto('sales_date < ?', $date),
				$this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId),
			)
		);
	}
	
	protected function _getRow($productId, $storeId, $date) {
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable())
			->where('product_id = ?', $productId)
			->where('store_id = ?', $storeId)
			->where('sales_date = ?', $date)
		;
		return $this->_getReadAdapter()->fetchRow($select);
	}
}
