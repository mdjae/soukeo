<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Catalog_MostviewedController extends Mage_Core_Controller_Front_Action {

	protected function _construct() {
		$this->_realModuleName = 'Ayaline_MostViewed';
	}

	public function indexAction() {
		if(!Mage::helper('ayalinemostviewed')->listIsActive()) {
			$this->_forward('cms/index/noRoute');
			return;
		}
		$this->loadLayout();
		$this->renderLayout();
	}
}
