<?php
/**
 * created : 27/05/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("
CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ayalinebestsales/best_sales_index')}` (
	`index_id`		INT(10)					NOT NULL	AUTO_INCREMENT ,
	`product_id`	INT(10)		UNSIGNED	NOT NULL ,
	`day`			SMALLINT(5)	UNSIGNED	NOT NULL ,
	`store_id`		SMALLINT(5)	UNSIGNED	NOT NULL ,
	`nb_sales`		INT(10)		UNSIGNED	NOT NULL ,
	PRIMARY KEY (`index_id`) ,

	INDEX `AYALINE_BEST_SALES_INDEX_DAY_IDX` (`day` ASC) ,
	INDEX `AYALINE_BEST_SALES_INDEX_PRODUCT_ID_IDX` (`product_id` ASC) ,
	INDEX `AYALINE_BEST_SALES_INDEX_STORE_ID_IDX` (`store_id` ASC) ,

	UNIQUE INDEX `AYALINE_BEST_SALES_INDEX_UNIQUE_KEY` (`product_id` ASC, `day` ASC, `store_id` ASC) ,

	CONSTRAINT `FK_AYALINE_BEST_SALES_INDEX_PRODUCT_ID`
		FOREIGN KEY (`product_id` )
		REFERENCES `{$installer->getTable('catalog/product')}` (`entity_id` )
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,

	CONSTRAINT `FK_AYALINE_BEST_SALES_INDEX_STORE_ID`
		FOREIGN KEY (`store_id` )
		REFERENCES `{$installer->getTable('core/store')}` (`store_id` )
		ON DELETE NO ACTION
		ON UPDATE NO ACTION)

ENGINE = InnoDB;

");

$installer->endSetup();
