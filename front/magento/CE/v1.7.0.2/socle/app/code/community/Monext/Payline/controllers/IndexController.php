<?php

/**
 * This controller manage all payline payment
 * cptAction, directAction, nxAction & walletAction are called just after the checkout validation
 * the return/notify/cancel are the urls called by Payline
 * An exception for notifyAction : it's not directly called by Payline, since it couldn't work in a local environment; it's then called by the returnAction.
 * @author fague
 *
 */
class Monext_Payline_IndexController extends Mage_Core_Controller_Front_Action {
	/* @var $order Mage_Sales_Model_Order */
	private $order;

	protected function _getCustomerSession() {
		return Mage::getSingleton('customer/session');
	}

	/**
	 * Add a transaction to the current order, depending on the payment type (Auth or Auth+Capture)
	 * @param string $transactionId
	 * @param string $paymentAction
	 * @return null
	 */
	private function addTransaction($transactionId, $paymentAction) {
		if (version_compare(Mage::getVersion(), '1.4', 'ge')) {
			/* @var $payment Mage_Payment_Model_Method_Abstract */
			$payment = $this -> order -> getPayment();
			if (!$payment -> getTransaction($transactionId)) {// if transaction isn't saved yet
				$transaction = Mage::getModel('sales/order_payment_transaction');
				$transaction -> setTxnId($transactionId);
				$transaction -> setOrderPaymentObject($this -> order -> getPayment());
				if ($paymentAction == '100') {

				} else if ($paymentAction == '101') {
					$transaction -> setTxnType(Mage_Sales_Model_Order_Payment_Transaction::TYPE_PAYMENT);
				}
				$transaction -> save();
				$this -> order -> sendNewOrderEmail();
			}
		} else {
			$this -> order -> getPayment() -> setLastTransId($transactionId);
			$this -> order -> sendNewOrderEmail();
		}
	}

	/**
	 *
	 * Set the order's status to the provided status (must be part of the cancelled state)
	 * Reinit stocks & redirect to checkout
	 * @param string $cancelStatus
	 */
	private function cancelOrder($cancelStatus, $resCode = '', $message = '') {
		$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $cancelStatus, $message, false);
		$this -> updateStock();
		$this -> order -> save();

		$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
	}

	/**
	 * Check if the customer is logged, and if it has a wallet
	 * If not & if there is a walletId in the result from Payline, we save it
	 */
	public function saveWallet($walletId) {
		if (!Mage::getStoreConfig('payment/payline_common/automate_wallet_subscription')) {
			return;
		}
		$customer = Mage::getSingleton('customer/session');
		if ($customer -> isLoggedIn()) {
			$customer = Mage::getModel('customer/customer') -> load($customer -> getId());
			if (!$customer -> getWalletId()) {
				$customer -> setWalletId($walletId);
				$customer -> save();
			}
		}
	}

	/**
	 *
	 * Initialise the requests param array
	 * @return array
	 */
	private function init() {
		$array = array();

		$_session = Mage::getSingleton('checkout/session');

		$this -> order = Mage::getModel('sales/order') -> loadByIncrementId($_session -> getLastRealOrderId());
		$_numericCurrencyCode = Mage::helper('payline') -> getNumericCurrencyCode($this -> order -> getBaseCurrencyCode());

		// PAYMENT
		$array['payment']['amount'] = round($this -> order -> getBaseGrandTotal() * 100);
		$array['payment']['currency'] = $_numericCurrencyCode;

		// ORDER
		//
		$array['order']['ref'] = substr($this -> order -> getRealOrderId(), 0, 50);
		$array['order']['amount'] = $array['payment']['amount'];
		$array['order']['currency'] = $_numericCurrencyCode;

		$billingAddress = $this -> order -> getBillingAddress();

		// BUYER
		$buyerLastName = substr($this -> order -> getCustomerLastname(), 0, 50);
		if ($buyerLastName == null || $buyerLastName == '') {
			$buyerLastName = substr($billingAddress -> getLastname(), 0, 50);
		}
		$buyerFirstName = substr($this -> order -> getCustomerFirstname(), 0, 50);
		if ($buyerFirstName == null || $buyerFirstName == '') {
			$buyerFirstName = substr($billingAddress -> getFirstname(), 0, 50);
		}
		$array['buyer']['lastName'] = Mage::helper('payline') -> encodeString($buyerLastName);
		$array['buyer']['firstName'] = Mage::helper('payline') -> encodeString($buyerFirstName);

		$email = $this -> order -> getCustomerEmail();
		$pattern = '/\+/i';
		$charPlusExist = preg_match($pattern, $email);
		if (strlen($email) <= 50 && Zend_Validate::is($email, 'EmailAddress') && !$charPlusExist) {
			$array['buyer']['email'] = Mage::helper('payline') -> encodeString($email);
		} else {
			$array['buyer']['email'] = '';
		}
		$array['buyer']['customerId'] = Mage::helper('payline') -> encodeString($email);

		// ADDRESS
		$array['address']['name'] = Mage::helper('payline') -> encodeString(substr($billingAddress -> getName(), 0, 100));
		$array['address']['street1'] = Mage::helper('payline') -> encodeString(substr($billingAddress -> getStreet1(), 0, 100));
		$array['address']['street2'] = Mage::helper('payline') -> encodeString(substr($billingAddress -> getStreet2(), 0, 100));
		$array['address']['cityName'] = Mage::helper('payline') -> encodeString(substr($billingAddress -> getCity(), 0, 40));
		$array['address']['zipCode'] = substr($billingAddress -> getPostcode(), 0, 12);
		//The $billing->getCountry() returns a 2 letter ISO2, should be fine
		$array['address']['country'] = $billingAddress -> getCountry();
		$forbidenCars = array(' ', '.', '(', ')', '-');
		$phone = str_replace($forbidenCars, '', $billingAddress -> getTelephone());
		$regexpTel = '/^\+?[0-9]{1,14}$/';
		if (preg_match($regexpTel, $phone)) {
			$array['address']['phone'] = $phone;
		} else {
			$array['address']['phone'] = '';
		}
		return $array;
	}

	/**
	 * Add payment transaction to the order, reinit stocks if needed
	 * @param $res array result of a request
	 * @param $transactionId
	 * @return boolean (true=>valid payment, false => invalid payment)
	 */
	private function updateOrder($res, $transactionId, $paymentType = 'CPT') {
		Mage::helper('payline/logger') -> log("[updateOrder] Mise à jour commande " . $this -> order -> getIncrementId() . " (mode $paymentType) avec la transaction $transactionId");
		$orderOk = false;

		if ($res['result']['code']) {

			if ($res['result']['code'] == '00000' || $res['result']['code'] == '02500' || $res['result']['code'] == '04003') {// transaction OK
				$orderOk = true;

				if ($paymentType == 'NX') {
					Mage::helper('payline/logger') -> log("[updateOrder] Cas du paiement NX");
					if (isset($res['billingRecordList']['billingRecord'][0])) {
						$code_echeance = $res['billingRecordList']['billingRecord'][0] -> result -> code;
						if ($code_echeance == '00000' || $code_echeance == '02501') {
							Mage::helper('payline/logger') -> log("[updateOrder] première échéance paiement NX OK");
							$orderOk = true;
						} else {
							Mage::helper('payline/logger') -> log("[updateOrder] première échéance paiement NX refusée, code " . $code_echeance);
							$orderOk = false;
						}
					} else {
						Mage::helper('payline/logger') -> log("[updateOrder] La première échéance de paiement est à venir");
					}
				}

				$this -> order -> getPayment() -> setCcTransId($transactionId);
				if (isset($res['payment']) && isset($res['payment']['action'])) {
					$paymentAction = $res['payment']['action'];
				} else {
					$paymentAction = Mage::getStoreConfig('payment/Payline' . $paymentType . '/payline_payment_action');
				}
				$this -> addTransaction($transactionId, $paymentAction);
			} else {
				$this -> updateStock();
			}
		}
		$this -> order -> save();
		return $orderOk;
	}

	/**
	 * Reinit stocks
	 */
	private function updateStock() {
		if (Mage::getStoreConfig(Mage_CatalogInventory_Model_Stock_Item::XML_PATH_CAN_SUBTRACT) == 1) {// le stock a été décrémenté à la commande
			// ré-incrémentation du stock
			$items = $this -> order -> getAllItems();
			if ($items) {
				foreach ($items as $item) {
					$quantity = $item -> getQtyOrdered();
					// get Qty ordered
					$product_id = $item -> getProductId();
					// get its ID
					$stock = Mage::getModel('cataloginventory/stock_item') -> loadByProduct($product_id);
					// Load the stock for this product
					$stock -> setQty($stock -> getQty() + $quantity);
					// Set to new Qty
					$stock -> save();
					// Save
					continue;
				}
			}
		}
	}

	/**
	 * Initialize the cpt payment request
	 */
	public function cptAction() {
		//Check if wallet is sendable
		//Must be done before call to Payline helper initialisation
		$expiredWalletId = false;
		if (Mage::getSingleton('customer/session') -> isLoggedIn()) {
			$customer = Mage::getSingleton('customer/session') -> getCustomer();
			$customer = Mage::getModel('customer/customer') -> load($customer -> getId());
			if ($customer -> getWalletId() && !Mage::getModel('payline/wallet') -> checkExpirationDate()) {
				$expiredWalletId = true;
			}
		}

		$array = $this -> init();
		/* @var $paylineSDK PaylineSDK */
		$helperPayline = Mage::helper('payline');
		$paylineSDK = $helperPayline -> initPayline('CPT', $array['payment']['currency']);
		$paymentMethod = $this -> order -> getPayment() -> getCcType();
		$array['payment']['action'] = Mage::getStoreConfig('payment/PaylineCPT/payline_payment_action');
		if ($paymentMethod) {
			// debut ajout FSZ 15/11/2012
			Mage::helper('payline/logger') -> log('[cptAction] order ' . $array['order']['ref'] . ' - customer selected contract ' . $paymentMethod);
			if ($paymentMethod == 'LEETCHI') {
				Mage::helper('payline/logger') -> log('[cptAction] order ' . $array['order']['ref'] . ' - LEETCHI selected => payment action is forced to 101');
				$array['payment']['action'] = '101';
			}
			// fin ajout FSZ 15/11/2012
			$array['payment']['contractNumber'] = $paymentMethod;
			$array['contracts'] = array($paymentMethod);
		} else {
			$array['payment']['contractNumber'] = $helperPayline -> contractNumber;
		}
		$array['payment']['mode'] = 'CPT';

		//second contracts
		$array['secondContracts'] = explode(';', $helperPayline -> secondaryContractNumberList);

		//If wallet isn't sendable...
		if ($expiredWalletId) {
			$helperPayline -> walletId = null;
		}

		// PRIVATE DATA
		$privateData = array();
		$privateData['key'] = "orderRef";
		$privateData['value'] = substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $array['order']['ref']), 0, 255);
		$paylineSDK -> setPrivate($privateData);

		//ORDER DETAILS (optional)
		$items = $this -> order -> getAllItems();
		if ($items) {
			if (count($items) > 100)
				$items = array_slice($items, 0, 100);
			foreach ($items as $item) {
				$itemPrice = round($item -> getPrice() * 100);
				if ($itemPrice > 0) {
					$product = array();
					$product['ref'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getName()), 0, 50));
					$product['price'] = round($item -> getPrice() * 100);
					$product['quantity'] = round($item -> getQtyOrdered());
					$product['comment'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getDescription()), 0, 255));
					$paylineSDK -> setItem($product);
				}
				continue;
			}
		}

		//WALLET
		if (Mage::getStoreConfig('payment/PaylineCPT/send_wallet_id')) {
			if (!isset($array['buyer']['walletId'])) {
				if (isset($helperPayline -> walletId)) {
					$array['buyer']['walletId'] = $helperPayline -> walletId;
				}
			}
			if ($helperPayline -> canSubscribeWallet()) {
				//If the wallet is new (registered during payment), we must save it in the private data since it's not sent back by default
				if ($helperPayline -> isNewWallet) {
					if ($helperPayline -> walletId) {
						$paylineSDK -> setPrivate(array('key' => 'newWalletId', 'value' => $helperPayline -> walletId));
					}
				}
			}
		}

		// EXECUTE
		try {
			$result = $paylineSDK -> doWebPayment($array);
		} catch(Exception $e) {
			Mage::logException($e);
			$this -> updateStock();
			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			$msgLog = 'Unknown PAYLINE ERROR (payline unreachable?)';
			Mage::helper('payline/logger') -> log('[cptAction] ' . $this -> order -> getIncrementId() . ' ' . $msgLog);
			$this -> _redirect('checkout/onepage');
			return;
		}
		// RESPONSE
		$initStatus = Mage::getStoreConfig('payment/payline_common/init_order_status');
		if (isset($result) && is_array($result) && $result['result']['code'] == '00000') {
			$this -> order -> setState(Mage_Sales_Model_Order::STATE_NEW, $initStatus, '', false);
			$this -> order -> save();
			header("location:" . $result['redirectURL']);
			exit();
		} else {//Payline error
			$this -> updateStock();
			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			if (isset($result) && is_array($result)) {
				$msgLog = 'PAYLINE ERROR : ' . $result['result']['code'] . ' ' . $result['result']['shortMessage'] . ' (' . $result['result']['longMessage'] . ')';
			} elseif (isset($result) && is_string($result)) {
				$msgLog = 'PAYLINE ERROR : ' . $result;
			} else {
				$msgLog = 'Unknown PAYLINE ERROR';
			}
			$this -> order -> setState(Mage_Sales_Model_Order::STATE_NEW, $initStatus, $msgLog, false);
			$this -> order -> save();
			Mage::helper('payline/logger') -> log('[cptAction] ' . $this -> order -> getIncrementId() . ' ' . $msgLog);
			$this -> _redirect('checkout/onepage');
			return;
		}
	}

	/**
	 * Initialize & process the direct payment request
	 */
	public function directAction() {
		$array = $this -> init();
		$paylineSDK = Mage::helper('payline') -> initPayline('DIRECT', $array['payment']['currency']);

		//PAYMENT
		$array['payment']['action'] = Mage::getStoreConfig('payment/PaylineDIRECT/payline_payment_action');
		$array['payment']['mode'] = 'CPT';

		$contract = Mage::getModel('payline/contract') -> load($_SESSION['payline_ccdata'] -> cc_type);
		$array['payment']['contractNumber'] = $contract -> getNumber();

		//ORDER
		$array['order']['date'] = date("d/m/Y H:i");

		//PRIVATE DATA
		$privateData1 = array();
		$privateData1['key'] = 'orderRef';
		$privateData1['value'] = substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $array['order']['ref']), 0, 255);
		$paylineSDK -> setPrivate($privateData1);

		//ORDER DETAILS (optional)
		$items = $this -> order -> getAllItems();
		if ($items) {
			if (count($items) > 100)
				$items = array_slice($items, 0, 100);
			foreach ($items as $item) {
				$itemPrice = round($item -> getPrice() * 100);
				if ($itemPrice > 0) {
					$product = array();
					$product['ref'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getName()), 0, 50));
					$product['price'] = round($item -> getPrice() * 100);
					$product['quantity'] = round($item -> getQtyOrdered());
					$product['comment'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getDescription()), 0, 255));
					$paylineSDK -> setItem($product);
				}
				continue;
			}
		}

		// CARD INFO
		$array['card']['number'] = $_SESSION['payline_ccdata'] -> cc_number;
		$array['card']['cardholder'] = $_SESSION['payline_ccdata'] -> cc_owner;
		$array['card']['type'] = $contract -> getContractType();
		$array['card']['expirationDate'] = $_SESSION['payline_ccdata'] -> cc_exp_month . $_SESSION['payline_ccdata'] -> cc_exp_year;
		$array['card']['cvx'] = $_SESSION['payline_ccdata'] -> cc_cid;

		// Customer IP
		$array['buyer']['ip'] = Mage::helper('core/http') -> getRemoteAddr();

		//3D secure
		$array['3DSecure'] = array();

		//BANK ACCOUNT DATA
		$array['BankAccountData'] = array();

		//version
		$array['version'] = Monext_Payline_Helper_Data::VERSION;
		// OWNER
		$array['owner']['lastName'] = Mage::helper('payline') -> encodeString($_SESSION['payline_ccdata'] -> cc_owner);

		try {
			$author_result = $paylineSDK -> doAuthorization($array);
		} catch(Exception $e) {
			Mage::logException($e);
			$this -> updateStock();
			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			$msgLog = 'Unknown PAYLINE ERROR (payline unreachable?)';
			Mage::helper('payline/logger') -> log('[directAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$this -> _redirect('checkout/onepage');
			return;
		}
		// RESPONSE
		$failedOrderStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
		if (isset($author_result) && is_array($author_result) && $author_result['result']['code'] == '00000') {
			$array_details = array();
			$array_details['orderRef'] = $this -> order -> getRealOrderId();
			$array_details['transactionId'] = $author_result['transaction']['id'];
			$array_details['startDate'] = '';
			$array_details['endDate'] = '';
			$array_details['transactionHistory'] = '';
			$array_details['version'] = Monext_Payline_Helper_Data::VERSION;
			$detail_result = $paylineSDK -> getTransactionDetails($array_details);

			if ($this -> updateOrder($detail_result, $detail_result['transaction']['id'], 'DIRECT')) {
				$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/success/";
				if ($detail_result['result']['code'] == '04003') {
					$newOrderStatus = Mage::getStoreConfig('payment/payline_common/fraud_order_status');
				} else {
					$newOrderStatus = Mage::getStoreConfig('payment/payline_common/new_order_status');
				}
				Mage::helper('payline') -> setOrderStatus($this -> order, $newOrderStatus);

				$array['wallet']['lastName'] = $array['buyer']['lastName'];
				$array['wallet']['firstName'] = $array['buyer']['firstName'];
				$array['wallet']['email'] = $array['buyer']['email'];
				Mage::helper('payline') -> createWalletForCurrentCustomer($paylineSDK, $array);
				Mage::helper('payline') -> automateCreateInvoiceAtShopReturn('DIRECT', $this -> order);

				$this -> order -> save();
				Mage_Core_Controller_Varien_Action::_redirectSuccess($redirectUrl);
			} else {
				$msgLog = 'Error during order update (#' . $this -> order -> getIncrementId() . ')' . "\n";
				$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $failedOrderStatus, $msgLog, false);
				$this -> order -> save();

				$msg = Mage::helper('payline') -> __('Error during payment');
				Mage::getSingleton('core/session') -> addError($msg);
				Mage::helper('payline/logger') -> log('[directAction] ' . $this -> order -> getIncrementId() . $msgLog);
				$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
				return;
			}
		} else {
			if (isset($author_result) && is_array($author_result)) {
				$msgLog = 'PAYLINE ERROR : ' . $author_result['result']['code'] . ' ' . $author_result['result']['shortMessage'] . ' (' . $author_result['result']['longMessage'] . ')';
			} elseif (isset($author_result) && is_string($author_result)) {
				$msgLog = 'PAYLINE ERROR : ' . $author_result;
			} else {
				$msgLog = 'Unknown PAYLINE ERROR';
			}

			$this -> updateStock();
			$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $failedOrderStatus, $msgLog, false);
			$this -> order -> save();

			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			Mage::helper('payline/logger') -> log('[directAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
			return;
		}
	}

	/** Initialisize a WALLET payment request
	 *
	 */
	/**
	 * Initialize & process a wallet direct payment request
	 */
	public function walletAction() {
		$array = $this -> init();
		$paylineSDK = Mage::helper('payline') -> initPayline('WALLET', $array['payment']['currency']);

		//PAYMENT
		$array['payment']['action'] = Mage::getStoreConfig('payment/PaylineWALLET/payline_payment_action');
		$array['payment']['mode'] = 'CPT';

		//Get the wallet contract number from card type
		$wallet = Mage::getModel('payline/wallet') -> getWalletData();
		$contract = Mage::getModel('payline/contract') -> getCollection() -> addFilterStatus(true, Mage::app() -> getStore() -> getId()) -> addFieldToFilter('contract_type', $wallet['card']['type']) -> getFirstItem();

		$array['payment']['contractNumber'] = $contract -> getNumber();

		//ORDER
		$array['order']['date'] = date("d/m/Y H:i");

		//PRIVATE DATA
		$privateData1 = array();
		$privateData1['key'] = 'orderRef';
		$privateData1['value'] = substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $array['order']['ref']), 0, 255);
		$paylineSDK -> setPrivate($privateData1);

		//ORDER DETAILS (optional)
		$items = $this -> order -> getAllItems();
		if ($items) {
			if (count($items) > 100)
				$items = array_slice($items, 0, 100);
			foreach ($items as $item) {
				$itemPrice = round($item -> getPrice() * 100);
				if ($itemPrice > 0) {
					$product = array();
					$product['ref'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getName()), 0, 50));
					$product['price'] = round($item -> getPrice() * 100);
					$product['quantity'] = round($item -> getQtyOrdered());
					$product['comment'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getDescription()), 0, 255));
					$paylineSDK -> setItem($product);
				}
				continue;
			}
		}

		$customerId = Mage::getSingleton('customer/session') -> getId();
		$customer = Mage::getModel('customer/customer') -> load($customerId);
		$walletId = $customer -> getWalletId();
		$array['walletId'] = $walletId;
		$array['cardInd'] = '';

		try {
			$author_result = $paylineSDK -> doImmediateWalletPayment($array);
		} catch(Exception $e) {
			Mage::logException($e);
			$this -> updateStock();
			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			$msgLog = 'Unknown PAYLINE ERROR (payline unreachable?) during wallet payment';
			Mage::helper('payline/logger') -> log('[walletAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$this -> _redirect('checkout/onepage');
			return;
		}
		// RESPONSE
		$failedOrderStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
		if (isset($author_result) && is_array($author_result) && $author_result['result']['code'] == '00000') {
			$array_details = array();
			$array_details['orderRef'] = $this -> order -> getRealOrderId();
			$array_details['transactionId'] = $author_result['transaction']['id'];
			$array_details['startDate'] = '';
			$array_details['endDate'] = '';
			$array_details['transactionHistory'] = '';
			$array_details['version'] = Monext_Payline_Helper_Data::VERSION;
			$detail_result = $paylineSDK -> getTransactionDetails($array_details);

			if (is_array($detail_result) && $this -> updateOrder($detail_result, $detail_result['transaction']['id'], 'WALLET')) {
				$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/success/";
				if ($detail_result['result']['code'] == '04003') {
					$newOrderStatus = Mage::getStoreConfig('payment/payline_common/fraud_order_status');
				} else {
					$newOrderStatus = Mage::getStoreConfig('payment/payline_common/new_order_status');
				}
				Mage::helper('payline') -> setOrderStatus($this -> order, $newOrderStatus);
				Mage::helper('payline') -> automateCreateInvoiceAtShopReturn('WALLET', $this -> order);
				$this -> order -> save();
				Mage_Core_Controller_Varien_Action::_redirectSuccess($redirectUrl);
			} else {
				$msgLog = 'Error during order update (#' . $this -> order -> getIncrementId() . ')';
				$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $failedOrderStatus, $msgLog, false);
				$this -> order -> save();
				$msg = Mage::helper('payline') -> __('Error during payment');
				Mage::getSingleton('core/session') -> addError($msg);
				Mage::helper('payline/logger') -> log('[walletAction] ' . $this -> order -> getIncrementId() . $msgLog);
				$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
				return;
			}

		} else {
			$this -> updateStock();
			if (isset($author_result) && is_array($author_result)) {
				$msgLog = 'PAYLINE ERROR during doImmediateWalletPayment: ' . $author_result['result']['code'] . ' ' . $author_result['result']['shortMessage'] . ' (' . $author_result['result']['longMessage'] . ')';
			} elseif (isset($author_result) && is_string($author_result)) {
				$msgLog = 'PAYLINE ERROR during doImmediateWalletPayment: ' . $author_result;
			} else {
				$msgLog = 'Unknown PAYLINE ERROR during doImmediateWalletPayment';
			}

			$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $failedOrderStatus, $msgLog, false);
			$this -> order -> save();

			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			Mage::helper('payline/logger') -> log('[walletAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
			return;
		}
	}

	/**
	 * Initialize the NX payment request
	 */
	public function nxAction() {

		//Check if wallet is sendable
		//Must be done before call to Payline helper initialisation
		$expiredWalletId = false;
		if (Mage::getSingleton('customer/session') -> isLoggedIn()) {
			$customer = Mage::getSingleton('customer/session') -> getCustomer();
			$customer = Mage::getModel('customer/customer') -> load($customer -> getId());
			if ($customer -> getWalletId() && !Mage::getModel('payline/wallet') -> checkExpirationDate()) {
				$expiredWalletId = true;
			}
		}

		$array = $this -> init();
		$helperPayline = Mage::helper('payline');
		$paylineSDK = $helperPayline -> initPayline('NX', $array['payment']['currency']);

		//If wallet isn't sendable...
		if ($expiredWalletId) {
			Mage::helper('payline') -> walletId = null;
		}

		$nx = Mage::getStoreConfig('payment/PaylineNX/billing_occurrences');
		$array['payment']['mode'] = 'NX';
		$array['payment']['action'] = 101;
		$array['payment']['contractNumber'] = $helperPayline -> contractNumber;
		$array['recurring']['amount'] = round($array['payment']['amount'] / $nx);
		$array['recurring']['firstAmount'] = $array['payment']['amount'] - ($array['recurring']['amount'] * ($nx - 1));
		$array['recurring']['billingCycle'] = Mage::getStoreConfig('payment/PaylineNX/billing_cycle');
		$array['recurring']['billingLeft'] = $nx;
		$array['recurring']['billingDay'] = '';
		$array['recurring']['startDate'] = '';

		//second contracts
		$array['secondContracts'] = explode(';', $helperPayline -> secondaryContractNumberList);

		// PRIVATE DATA
		$privateData = array();
		$privateData['key'] = "orderRef";
		$privateData['value'] = substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $array['order']['ref']), 0, 255);
		$paylineSDK -> setPrivate($privateData);

		//ORDER DETAILS (optional)
		$items = $this -> order -> getAllItems();
		if ($items) {
			if (count($items) > 100)
				$items = array_slice($items, 0, 100);
			foreach ($items as $item) {
				$itemPrice = round($item -> getPrice() * 100);
				if ($itemPrice > 0) {
					$product = array();
					$product['ref'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getName()), 0, 50));
					$product['price'] = round($item -> getPrice() * 100);
					$product['quantity'] = round($item -> getQtyOrdered());
					$product['comment'] = Mage::helper('payline') -> encodeString(substr(str_replace(array("\r", "\n", "\t"), array('', '', ''), $item -> getDescription()), 0, 255));
					$paylineSDK -> setItem($product);
				}
				continue;
			}
		}

		//WALLET
		if (Mage::getStoreConfig('payment/PaylineCPT/send_wallet_id')) {
			if (!isset($array['buyer']['walletId'])) {
				if (isset($helperPayline -> walletId)) {
					$array['buyer']['walletId'] = $helperPayline -> walletId;
				}
			}
			if ($helperPayline -> canSubscribeWallet()) {
				//If the wallet is new (registered during payment), we must save it in the private data since it's not sent back by default
				if ($helperPayline -> isNewWallet) {
					if ($helperPayline -> walletId) {
						$paylineSDK -> setPrivate(array('key' => 'newWalletId', 'value' => $helperPayline -> walletId));
					}
				}
			}
		}

		// EXECUTE
		try {
			$result = $paylineSDK -> doWebPayment($array);
		} catch(Exception $e) {
			Mage::logException($e);
			$this -> updateStock();
			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::getSingleton('core/session') -> addError($msg);
			$msgLog = 'Unknown PAYLINE ERROR (payline unreachable?)';
			Mage::helper('payline/logger') -> log('[nxAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$this -> _redirect('checkout/onepage');
			return;
		}
		// RESPONSE
		$initStatus = Mage::getStoreConfig('payment/payline_common/init_order_status');
		if (isset($result) && is_array($result) && $result['result']['code'] == '00000') {
			$this -> order -> setState(Mage_Sales_Model_Order::STATE_NEW, $initStatus, '', false);
			$this -> order -> save();
			header("location:" . $result['redirectURL']);
			exit();
		} else {
			$this -> updateStock();
			if (isset($result) && is_array($result)) {
				$msgLog = 'PAYLINE ERROR : ' . $result['result']['code'] . ' ' . $result['result']['shortMessage'] . ' (' . $result['result']['longMessage'] . ')';
			} elseif (isset($result) && is_string($result)) {
				$msgLog = 'PAYLINE ERROR : ' . $result;
			} else {
				$msgLog = 'Unknown PAYLINE ERROR';
			}
			$this -> order -> setState(Mage_Sales_Model_Order::STATE_NEW, $initStatus, $msgLog, false);
			$this -> order -> save();
			$msg = Mage::helper('payline') -> __('Error during payment');
			Mage::helper('payline/logger') -> log('[nxAction] ' . $this -> order -> getIncrementId() . $msgLog);
			Mage::getSingleton('core/session') -> addError($msg);
			$this -> _redirect('checkout/onepage');
			return;

		}
	}

	/**
	 * Action called on the customer's return form the Payline website.
	 */
	public function cptReturnAction() {
		$res = Mage::helper('payline') -> initPayline('CPT') -> getWebPaymentDetails(array('token' => $_GET['token'], 'version' => Monext_Payline_Helper_Data::VERSION));

		$this -> _getCustomerSession() -> setWebPaymentDetails($res);

		if (isset($res['privateDataList']['privateData']['value'])) {
			$orderRef = $res['privateDataList']['privateData']['value'];
		} else {
			foreach ($res['privateDataList']['privateData'] as $privateDataList) {
				if ($privateDataList -> key == 'orderRef') {
					$orderRef = $privateDataList -> value;
				}
			}
		}

		if (!isset($orderRef)) {
			$msgLog = 'Couldn\'t find order increment id in cpt payment result';
			Mage::helper('payline/logger') -> log('[cptNotifAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
			return;
		}
		$this -> order = Mage::getModel('sales/order') -> loadByIncrementId($orderRef);

		//If order is still new, notifAction haven't been called yet
		if ($this -> order -> getState() == Mage_Sales_Model_Order::STATE_NEW) {
			//	Mage::helper('udpo/protected')->splitOrderToPos($this->order);
			Mage_Core_Controller_Varien_Action::_redirectSuccess($this -> cptNotifAction());
		}

		if ($res['result']['code'] == '00000' || $res['result']['code'] == '04003') {
			Mage::helper('udpo/protected') -> splitOrderToPos($this -> order);
			$this -> _redirect('checkout/onepage/success');
		} else {
			Mage::getSingleton('core/session') -> addError(Mage::helper('payline') -> __('Votre paiement a été refusé'));
			$this -> _redirectUrl($this -> _getPaymentRefusedRedirectUrl());
		}
	}

	/**
	 * Action called on the customer's return form the Payline website.
	 */
	public function nxReturnAction() {
		Mage_Core_Controller_Varien_Action::_redirectSuccess($this -> nxNotifAction());
	}

	/**
	 * Save CPT payment result, called by the bank when the transaction is done
	 */
	public function cptNotifAction() {
		$res = $this -> _getCustomerSession() -> getWebPaymentDetails(true);
		if (empty($res)) {
			$res = Mage::helper('payline') -> initPayline('CPT') -> getWebPaymentDetails(array('token' => $_GET['token'], 'version' => Monext_Payline_Helper_Data::VERSION));
		}

		if (isset($res['privateDataList']['privateData']['value'])) {
			$orderRef = $res['privateDataList']['privateData']['value'];
		} else {
			foreach ($res['privateDataList']['privateData'] as $privateDataList) {
				if ($privateDataList -> key == 'orderRef') {
					$orderRef = $privateDataList -> value;
				}
			}
		}
		if (!isset($orderRef)) {
			$msgLog = 'Couldn\'t find order increment id in cpt payment result';
			Mage::helper('payline/logger') -> log('[cptNotifAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/";
		}
		$this -> order = Mage::getModel('sales/order') -> loadByIncrementId($orderRef);
		$payment = $this -> order -> getPayment();
		if ($payment -> getBaseAmountPaid() != $payment -> getBaseAmountOrdered()) {
			if ($res['result']['code'] == '04003') {
				$newOrderStatus = Mage::getStoreConfig('payment/payline_common/fraud_order_status');
			} else {
				$newOrderStatus = Mage::getStoreConfig('payment/payline_common/new_order_status');
			}
			$failedOrderStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
			if (is_array($res) && $this -> updateOrder($res, $res['transaction']['id'], 'CPT')) {
				$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/success/";
				Mage::helper('payline') -> setOrderStatus($this -> order, $newOrderStatus);
				if (isset($res['privateDataList']['privateData'][1]) && $res['privateDataList']['privateData'][1] -> key == "newWalletId" && $res['privateDataList']['privateData'][1] -> value != '') {
					$this -> saveWallet($res['privateDataList']['privateData'][1] -> value);
				}
				Mage::helper('payline') -> automateCreateInvoiceAtShopReturn('CPT', $this -> order);
			} else {
				if (isset($res) && is_array($res)) {
					$msgLog = 'PAYLINE ERROR : ' . $res['result']['code'] . ' ' . $res['result']['shortMessage'] . ' (' . $res['result']['longMessage'] . ')';
				} elseif (isset($res) && is_string($res)) {
					$msgLog = 'PAYLINE ERROR : ' . $res;
				} else {
					$msgLog = 'Error during order update (#' . $this -> order -> getIncrementId() . ')';
				}

				if (is_array($res) && $res['result']['code'] == '02304') {
					$abandonedStatus = Mage::getStoreConfig('payment/payline_common/resignation_order_status');
					$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $abandonedStatus, $msgLog, false);
				} else {
					$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $failedOrderStatus, $msgLog, false);
				}

				Mage::helper('payline/logger') -> log('[cptNotifAction] ' . $this -> order -> getIncrementId() . $msgLog);
				$redirectUrl = $this -> _getPaymentRefusedRedirectUrl();
			}
			$this -> order -> save();
		} else {

			$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/success/";
		}
		return $redirectUrl;
	}

	/**
	 * Save NX payment result, called by the bank when the transaction is done
	 */
	public function nxNotifAction() {

		$res = Mage::helper('payline') -> initPayline('NX') -> getWebPaymentDetails(array('token' => $_GET['token'], 'version' => Monext_Payline_Helper_Data::VERSION));
		if (isset($res['privateDataList']['privateData']['value'])) {
			$orderRef = $res['privateDataList']['privateData']['value'];
		} else {
			foreach ($res['privateDataList']['privateData'] as $privateDataList) {
				if ($privateDataList -> key == 'orderRef') {
					$orderRef = $privateDataList -> value;
				}
			}
		}
		if (!isset($orderRef)) {
			$msgLog = 'Référence commande introuvable dans le résultat du paiement Nx';
			Mage::helper('payline/logger') -> log('[nxNotifAction] ' . $this -> order -> getIncrementId() . ' ' . $msgLog);
			$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/";
		}
		$this -> order = Mage::getModel('sales/order') -> loadByIncrementId($orderRef);

		if ($res['result']['code'] == '04003') {
			$newOrderStatus = Mage::getStoreConfig('payment/payline_common/fraud_order_status');
		} else {

			$newOrderStatus = Mage::getStoreConfig('payment/payline_common/new_order_status');
		}
		$failedOrderStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');

		if (isset($res['billingRecordList']['billingRecord'])) {
			$size = sizeof($res['billingRecordList']['billingRecord']);
		} else {
			$size = 0;
		}
		$billingRecord = false;
		for ($i = 0; $i < $size; $i++) {
			if ($res['billingRecordList']['billingRecord'][$i] -> status == 1) {
				$txnId = $res['billingRecordList']['billingRecord'][$i] -> transaction -> id;
				if (!$this -> order -> getTransaction($txnId)) {
					$billingRecord = $res['billingRecordList']['billingRecord'][$i];
				}
			}
		}

		if ($billingRecord && $this -> updateOrder($res, $billingRecord -> transaction -> id, 'NX')) {
			$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/success/";
			Mage::helper('payline') -> setOrderStatus($this -> order, $newOrderStatus);
				Mage::helper('udpo/protected')->splitOrderToPos($this->order);
			if (isset($res['privateDataList']['privateData'][1]) && $res['privateDataList']['privateData'][1] -> key == "newWalletId" && $res['privateDataList']['privateData'][1] -> value != '') {
				$this -> saveWallet($res['privateDataList']['privateData'][1] -> value);
			}
			$payment = $this -> order -> getPayment();
			if ($payment -> getBaseAmountPaid() != $payment -> getBaseAmountOrdered()) {
				Mage::helper('payline') -> automateCreateInvoiceAtShopReturn('NX', $this -> order);
			}
		} else {
			if (isset($res) && is_array($res)) {
				$msgLog = 'PAYLINE ERROR : ' . $res['result']['code'] . ' ' . $res['result']['shortMessage'] . ' (' . $res['result']['longMessage'] . ')';
			} elseif (isset($res) && is_string($res)) {
				$msgLog = 'PAYLINE ERROR : ' . $res;
			} else {
				$msgLog = 'Error during order update (#' . $this -> order -> getIncrementId() . ')';
			}

			if (is_array($res) && ($res['result']['code'] == '02304')) {
				$abandonedStatus = Mage::getStoreConfig('payment/payline_common/resignation_order_status');
				$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $abandonedStatus, $msgLog, false);
			} else {
				$this -> order -> setState(Mage_Sales_Model_Order::STATE_CANCELED, $failedOrderStatus, $msgLog, false);
			}

			Mage::helper('payline/logger') -> log('[nxNotifAction] ' . $this -> order -> getIncrementId() . $msgLog);
			Mage::getSingleton('core/session') -> addError(Mage::helper('payline') -> __('Votre paiement a été refusé'));
			$redirectUrl = $this -> _getPaymentRefusedRedirectUrl();
		}
		$this -> order -> save();
		return $redirectUrl;

	}

	/**
	 * Cancel a CPT payment request /order
	 */
	public function cptCancelAction() {
		$res = Mage::helper('payline') -> initPayline('CPT') -> getWebPaymentDetails(array('token' => $_GET['token'], 'version' => Monext_Payline_Helper_Data::VERSION));

		if (isset($res['privateDataList']['privateData']['value'])) {
			$orderRef = $res['privateDataList']['privateData']['value'];
		} else {
			foreach ($res['privateDataList']['privateData'] as $privateDataList) {
				if ($privateDataList -> key == 'orderRef') {
					$orderRef = $privateDataList -> value;
				}
			}
		}
		if (!isset($orderRef)) {
			$msgLog = 'Couldn\'t find order increment id in cpt payment cancel result';
			Mage::helper('payline/logger') -> log('[cptCancelAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/";
			$this -> _redirect($redirectUrl);
			return;
		}
		$this -> order = Mage::getModel('sales/order') -> loadByIncrementId($orderRef);
		$msg = '';
		if (is_string($res)) {
			$msg = 'PAYLINE ERROR : ' . $res;
			Mage::helper('payline/logger') -> log('[cptCancelAction] ' . $this -> order -> getIncrementId() . ' ' . $msg);
			$cancelStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
		} elseif (substr($res['result']['code'], 0, 2) == '01' || substr($res['result']['code'], 0, 3) == '021') {
			//Invalid transaction or error during the process on Payline side
			//No error display, the customer is already told on the Payline side
			Mage::getSingleton('core/session') -> addError(Mage::helper('payline') -> __('Votre paiement a été réfusé'));
			$msg = 'PAYLINE ERROR : ' . $res['result']['code'] . ' ' . $res['result']['shortMessage'] . ' (' . $res['result']['longMessage'] . ')';
			Mage::helper('payline/logger') -> log('[cptCancelAction] ' . $this -> order -> getIncrementId() . $msg);
			$cancelStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
		} else {
			Mage::getSingleton('core/session') -> addError(Mage::helper('payline') -> __('Votre paiement a été annulé'));
			$msg = 'PAYLINE INFO : ' . $res['result']['code'] . ' ' . $res['result']['shortMessage'] . ' (' . $res['result']['longMessage'] . ')';
			//Transaction cancelled by customer
			$cancelStatus = Mage::getStoreConfig('payment/payline_common/canceled_order_status');
		}
		$this -> cancelOrder($cancelStatus, $res['result']['code'], $msg);
	}

	/**
	 * Cancel a NX payment request /order
	 */
	public function nxCancelAction() {
		$res = Mage::helper('payline') -> initPayline('NX') -> getWebPaymentDetails(array('token' => $_GET['token'], 'version' => Monext_Payline_Helper_Data::VERSION));
		if (isset($res['privateDataList']['privateData']['value'])) {
			$orderRef = $res['privateDataList']['privateData']['value'];
		} else {
			foreach ($res['privateDataList']['privateData'] as $privateDataList) {
				if ($privateDataList -> key == 'orderRef') {
					$orderRef = $privateDataList -> value;
				}
			}
		}
		if (!isset($orderRef)) {
			$msgLog = 'Couldn\'t find order increment id in nx payment cancel result';
			Mage::helper('payline/logger') -> log('[nxCancelAction] ' . $this -> order -> getIncrementId() . $msgLog);
			$redirectUrl = Mage::getBaseUrl() . "checkout/onepage/";
		}
		$this -> order = Mage::getModel('sales/order') -> loadByIncrementId($orderRef);

		if (is_string($res)) {
			$msg = 'PAYLINE ERROR : ' . $res;
			Mage::helper('payline/logger') -> log('[nxCancelAction] ' . $this -> order -> getIncrementId() . ' ' . $msg);
			$cancelStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
		} elseif (substr($res['result']['code'], 0, 2) == '01' || substr($res['result']['code'], 0, 3) == '021') {
			//Invalid transaction or error during the process on Payline side
			//No error display, the customer is already told on the Payline side
			Mage::getSingleton('core/session') -> addError(Mage::helper('payline') -> __('Votre paiement a été refusé'));
			$msg = 'PAYLINE ERROR : ' . $res['result']['code'] . ' ' . $res['result']['shortMessage'] . ' (' . $res['result']['longMessage'] . ')';
			Mage::helper('payline/logger') -> log('[nxCancelAction] ' . $this -> order -> getIncrementId() . $msg);
			$cancelStatus = Mage::getStoreConfig('payment/payline_common/failed_order_status');
		} else {
			Mage::getSingleton('core/session') -> addError(Mage::helper('payline') -> __('Votre paiement a été annulé'));
			$msg = 'PAYLINE INFO : ' . $res['result']['code'] . ' ' . $res['result']['shortMessage'] . ' (' . $res['result']['longMessage'] . ')';
			//Transaction cancelled by customer
			$cancelStatus = Mage::getStoreConfig('payment/payline_common/canceled_order_status');
		}
		$this -> cancelOrder($cancelStatus, $res['result']['code'], $msg);
	}

	protected function _getPaymentRefusedRedirectUrl() {
		$option = Mage::getStoreConfig('payment/payline_common/return_payment_refused');
		switch($option) {
			case Monext_Payline_Model_Datasource_Return::CART_EMPTY :
				$url = Mage::getUrl('checkout/onepage');
				break;
			case Monext_Payline_Model_Datasource_Return::HISTORY_ORDERS :
				$url = Mage::getUrl('sales/order/history');
				break;
			case Monext_Payline_Model_Datasource_Return::CART_FULL :
				$url = Mage::getUrl('sales/order/reorder', array('order_id' => $this -> order -> getId()));
				break;
			default :
				$url = Mage::getUrl('checkout/onepage');
		}

		return $url;
	}

}
