<?php
/**
 * created : 29/08/2011
 * 
 * @category Ayaline
 * @package Ayaline_MostViewed
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_MostViewed
 */
class Ayaline_MostViewed_Block_Widget_MostViewed extends Ayaline_MostViewed_Block_Sidebar implements Mage_Widget_Block_Interface {
	
	protected function _construct() {
		parent::_construct();
		$this->setCount($this->getData('products_count'));		//	nb products in template
		$this->setColumnCount($this->getData('column_count'));	//	if grid template, number of columns
	}
    
}