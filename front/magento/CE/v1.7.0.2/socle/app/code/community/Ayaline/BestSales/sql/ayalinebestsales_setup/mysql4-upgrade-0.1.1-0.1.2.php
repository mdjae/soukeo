<?php
/**
 * created : 27/05/2011
 * 
 * @category Ayaline
 * @package Ayaline_BestSales
 * @author aYaline
 * @copyright Ayaline - 2012 - http://magento-shop.ayaline.com
 * @license http://shop.ayaline.com/magento/fr/conditions-generales-de-vente.html
 */

/**
 * 
 * @package Ayaline_BestSales
 */
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();

$installer->run("

ALTER TABLE `{$installer->getTable('ayalinebestsales/best_sales_index')}` DROP FOREIGN KEY `FK_AYALINE_BEST_SALES_INDEX_PRODUCT_ID`;
ALTER TABLE `{$installer->getTable('ayalinebestsales/best_sales_index')}` DROP FOREIGN KEY `FK_AYALINE_BEST_SALES_INDEX_STORE_ID`;

ALTER TABLE `{$installer->getTable('ayalinebestsales/best_sales_index')}` ADD 
	CONSTRAINT `FK_AYALINE_BEST_SALES_INDEX_PRODUCT_ID`
		FOREIGN KEY (`product_id` )
		REFERENCES `{$installer->getTable('catalog/product')}` (`entity_id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
;

ALTER TABLE `{$installer->getTable('ayalinebestsales/best_sales_index')}` ADD 
	CONSTRAINT `FK_AYALINE_BEST_SALES_INDEX_STORE_ID`
		FOREIGN KEY (`store_id` )
		REFERENCES `{$installer->getTable('core/store')}` (`store_id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
;

");

$installer->endSetup();
