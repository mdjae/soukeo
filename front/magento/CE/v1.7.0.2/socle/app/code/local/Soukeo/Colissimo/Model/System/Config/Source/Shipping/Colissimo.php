<?php

class Soukeo_Colissimo_Model_System_Config_Source_Shipping_Colissimo{
	
	public function toOptionArray(){
		
		return array(
				array('value' => '', 'label'=> Mage::helper('adminhtml')->__('None')),
				array('value' => 'order', 'label' => Mage::helper('adminhtml')->__('Per Order')),
				array('value' => 'item', 'label' => Mage::helper('adminhtml')->__('Per Item')),
				);
		
	}
	
	
	
}

?>