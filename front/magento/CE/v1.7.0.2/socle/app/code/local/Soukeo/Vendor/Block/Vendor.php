<?php


class Mage_Catalog_Block_Product_View_Vendor extends Mage_Core_Block_Template
{
    protected $_product = null;

    function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::registry('product');
        }
        return $this->_product;
    }
	
	function getVendor(){
		
		$assocVendor = Mage::getModel('udropship/vendor_product')->getCollection()
					->addProductFilter($this->getProduct()->getId())->setOrder('vendor_price', 'ASC'); ;
	  	$assocVendor->getSelect()->join(array('p' => $assocVendor->getTable('catalog/product')), 'p.entity_id=main_table.product_id');
		
		
		return $assocVendor->getdata();
	
	}
	
	
}
