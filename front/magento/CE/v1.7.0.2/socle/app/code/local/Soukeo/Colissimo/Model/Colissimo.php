<?php

class Soukeo_Colissimo_Model_Colissimo
	extends Mage_Shipping_Model_Carrier_Abstract
	implements Mage_Shipping_Model_Carrier_Interface 

	{
		
		protected $_code ='colissimo';
		protected $_isFixed = true;
		protected $_colissimoFlatRate = 5.90;
		
		public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
			
			$method = Mage::getModel('shipping/rate_result_method');
			$result = Mage::getModel('shipping/rate_result');
			
			$method->setCarrier($this->_code);
			$method->setCarrierTitle('La Poste - Reunion');
			$method->setMethod($this->_code);
			$method->setMethodTitle('Colissimo');
			
			
			$customer = $this->getCustomer();
			$orderQty = 0;
			
			if($customer->getId()){
				
			/* *
			 * recupere le count des anciennes commande du customer	
			 * Si oui et que pas de coupon de reduction les frais sont offert
			 * 
			 */
			 
				$orderQty = count(Mage::getResourceModel('sales/order_collection')
									->addAttributeToFilter('customer_id', $customer->getId()));
				$shippingAmount = $orderQty && !$this->getQuote()->getCouponCode() ? 0 : $_colissimoFlatRate;			
			}
			
			$method->setCost($shippingAmount);
			$method->setPrice($shippingAmount);
			
			$result->append($method);
			return $result;
			
		}
		
		
		// Retourne la traduction par le helper 
		public function getAllowedMethods(){
			
			//return array('colissimo' => Mage::helper('colissimo') -> __('Colissimo'));
			return array('colissimo' => 'Colissimo');
		}
		
		//Data de session sur la comamnde
		public function getQuote(){
			return Mage::getSingleton("checkout/session")->getQuote();
		}
		
		//Date de session sur le costumer
		public function getCustomer(){
			return Mage::helper('customer')->getCustomer();
		}
		
	}





?>
