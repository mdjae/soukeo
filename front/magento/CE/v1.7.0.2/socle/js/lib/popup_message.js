
// Affiche une popup a chaque message de validation ou autre

var j = jQuery.noConflict();
j(document).ready(function () {
	
	
	j(".messages li ul").append("<li class='close_button'> <button class='close light_action'>Fermer</button></li>");
	
	var messages = j(".messages");
	
	j(".messages").remove();

	if ( messages.html() ) {
		j("body").append("<div id='popup'><div class='popup-bg'></div><div class='popup'><div class='close-popup'></div><ul>"+messages.html()+"</ul></div></div>" ); 	
	//	j('#popup').delay(3000).fadeOut(400);
	}


	j(".close").click(function() {		
		j('#popup').fadeOut(200);
	})	
	
	j(".close-popup").click(function() {		
		j('#popup').fadeOut(200);
	})
	
	j(".popup-bg").click(function() {		
		j('#popup').fadeOut(200);
	})
});
		