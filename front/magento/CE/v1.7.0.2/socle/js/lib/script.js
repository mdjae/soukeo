
var j = jQuery.noConflict();
j(document).ready(function () {
	
	
	
	// Modifie le header au scroll de la page
	
	var menu = j('header#header > #navigation > #mainmenu > nav > .submenu');
	var menu_home = j('header.ishome > #navigation > #mainmenu > nav > .submenu');
	var menu_product = j('.product > .container > header > #navigation > #mainmenu > nav > .submenu');
	var menu_home_s = j('#header-small > #mainmenu > nav > .submenu');
	var menu_product_s = j('.product > .container > #header-small > #mainmenu > nav > .submenu');


	//var breadcrumbs = j('#ariane');
	var navigation = j('#header #navigation'); 
	var header_small = j("#header-small");
	

	
	// Ajoute le contenu a l identificant header-small
	j( ".Hleft" ).clone().appendTo( "#header-small" );
	j( "#searchbox" ).clone().appendTo( "#header-small" );
	j( "#connexion" ).clone().appendTo( "#header-small" );
	j( "#quicklinks" ).clone().appendTo( "#header-small" );	
	//j( "#navigation" ).clone().appendTo( "#header-small" ); // avec le fil d'ariane
	j( "#mainmenu" ).clone().appendTo( "#header-small" ); // sans le fil d'ariane
	

	// le menu est visible au chargment de la page que pour la page d accueil et la page produit
	//menu_home.css({"visibility": "visible"});
	//menu_product.css({"visibility": "visible"});	
	
	
	menu_home.hide(0).delay(1000).fadeIn(400);
	//menu_product.hide(0).delay(1000).fadeIn(400);
	

// cache ou affiche le sous menu principal
 j('.mainmenu').hover(
	function(e) {		
		//j(this).children().children().css("visibility", "visible");
		//j(this).children().children().stop();
		j(this).children().children().stop(true, true).slideDown(400);
	
	}, function(e) {
		//j(this).children().children().css("visibility", "hidden");
		//j(this).children().children().stop();
		j(this).children().children().stop(true, true).delay(500).slideUp(400);	

	});
	
	
	
	
	
	/*
// cache ou affiche le sous sous menu principal
 j('.submenu > ul > li').hover(
	function(e) {	
		
		j( ".menuslide"  ).empty();
		j(this).children().clone().appendTo( ".menuslide" );
		j( ".menuslide" ).children().css("display", "block");	
		j( ".menuslide" ).children().stop(true, true).animate({left: 230});
		
		//var $lefty = j(this).children();
		//j(this).children().css("display", "block")	
		//j(this).children().stop(true, true).animate({left: 130});	
	}, function(e) {
		//var $lefty = j(this).children();
		//j(this).children().css("display", "none")		
		//j(this).children().stop(true, true).animate({left: 10});	
	
		//j( ".menuslide" ).children().stop(true, true).animate({left: -500});
	//j( ".menuslide"  ).empty();
	});

 */

j(window).scroll(function(){ // On ajoute une fonction quand on défile dans le site

		// On récupère la position de la barre de défilement par rapport à notre fenêtre
		var scrollTop = j(window).scrollTop();

		if(scrollTop <= 100){ // Si on inferieur a 100px
				//header_small.css("visibility", "hidden");
				//menu_home.css({"visibility": "visible"});
				//menu_product.css({"visibility": "visible"});
				header_small.fadeOut(400);
				//menu_home.fadeIn(400);
				//menu_product.fadeIn(400);
				//menu_home_s.fadeIn(400);
				//menu_product_s.fadeIn(400);
				//  console.log( "en haut" );
				 
			}
		else{
			// Sinon, on cache la div si celle ci est visible.
				//header_small.css("visibility", "visible");
				//menu_home.css({"visibility": "hidden"});
				//menu_product.css({"visibility": "hidden"});
				header_small.fadeIn(400);
				menu_home.fadeOut(400);
				menu_product.fadeOut(400);
				menu_home_s.fadeOut(400);
				menu_product_s.fadeOut(400);
				//console.log( " descendu " );

		}
	});
	
       // On lance l'évènement scroll un première fois au chargement de la page
	//
	j(window).scroll();
	
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	
	
	
	// Tabulations pour les pages de niveau 1, meilleurs ventes et les plus vendues 
	
	var best_sales = j('#best_sales');
	best_sales.css("display", "none");

	var most_viewed = j('#most_viewed'); 
	
	
	j("#tabs_best_sales").on("click",  function() {
		j(".top_ventes ul li").removeClass( "active" )
		j(this).addClass("active");
		
		most_viewed.css("display", "none");
		best_sales.css("display", "block");
		
	})
	
	j("#tabs_most_viewed").on("click",  function() {
			
		j(".top_ventes ul li").removeClass( "active" )
		j(this).addClass("active");

		best_sales.css("display", "none");
		most_viewed.css("display", "block");
		
	})
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	
	
		// Tabulations pour les pages produits vendeurs
	
	j('#YT_partenaires').removeClass("active");
	j('#MU_partenaires').removeClass("active");
	j('#MG_partenaires').removeClass("active");
	j('#FR_partenaires').removeClass("active");
	
	j('#YT_partenaires_content').css("display", "none");
	j('#MU_partenaires_content').css("display", "none");
	j('#MG_partenaires_content').css("display", "none");
	j('#FR_partenaires_content').css("display", "none");
	
	/////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	
	// Modification de la publicité par univers	
	j('#pub-univers').clone().appendTo( ".bloc-pub-univers" );
	j('#pub-univers').clone().appendTo( ".bloc-pub-univers_s" );
	
	
j(window).scroll(function(){ // On ajoute une fonction quand on défile dans le site

		// On récupère la position de la barre de défilement par rapport à notre fenêtre
		var scrollTop = j(window).scrollTop();

		if(scrollTop <= 5){ // Si on dépassé les 300 pixels
				j('.bloc-pub-univers').css({"top": "160px"}); // modifie la hauteur pour un meilleur affichage de la pub par univers
				j('.bloc-pub-univers_s').css({"top": "160px"});
			}
		else{
			// Sinon, on cache la div si celle ci est visible.
				j('.bloc-pub-univers').css({"top": "80px"});   // modifie la hauteur pour un meilleur affichage de la pub par univers
				j('.bloc-pub-univers_s').css({"top": "80px"});
		}
	});	
	
	
	
	// On calcule la taille de l 'ecran au chargement
	var hauteur_fenetre = j(window).height() - 80; // 80 est la hauteur de header small
	j('.bloc-pub-univers img').css({height: hauteur_fenetre, width: "auto" });
	j('.bloc-pub-univers_s img').css({height: hauteur_fenetre, width: "auto" });
		

		// On calcule la taille de l 'ecran au a chaque redimensionnment du navigateur	
	j(window).resize(function(){ 
		
		var hauteur_fenetre = j(window).height() - 80; // 80 est la hauteur de header small		
		j('.bloc-pub-univers img').css({height: hauteur_fenetre, width: "auto" });
		j('.bloc-pub-univers_s img').css({height: hauteur_fenetre, width: "auto" });
		
		return false 
	});
	
	
	
	
	
	
	
		/////////////////////////////////////////////////////////////////////////////////////////
	
	
	// Modification de la publicité 

j(window).scroll(function(){ // On ajoute une fonction quand on défile dans le site

		// On récupère la position de la barre de défilement par rapport à notre fenêtre
		var scrollTop = j(window).scrollTop();

		if(scrollTop <= 5){ // Si on dépassé les 300 pixels
				j('.bloc-publicite').css({"top": "160px"}); // modifie la hauteur pour un meilleur affichage de la pub par univers
			 
			}
		else{
			// Sinon, on cache la div si celle ci est visible.
				j('.bloc-publicite').css({"top": "80px"});   // modifie la hauteur pour un meilleur affichage de la pub par univers

		}
	});	
	
	
	
	// On calcule la taille de l 'ecran au chargement
	/* var hauteur_fenetre = j(window).height() - 80; // 80 est la hauteur de header small
	j('.bloc-publicite img').css({height: hauteur_fenetre, width: "auto" });
		
		// On calcule la taille de l 'ecran au a chaque redimensionnment du navigateur	
	j(window).resize(function(){ 
		
		var hauteur_fenetre = j(window).height() - 80; // 80 est la hauteur de header small		
		j('.bloc-publicite img').css({height: hauteur_fenetre, width: "auto" });
		
		return false 
	});
	*/
	
	
			/////////////////////////////////////////////////////////////////////////////////////////


	
	//Ajout du logo paiement dans la section commande
	//NE FONCTIONNE PAS
/*j("#shipping-method-buttons-container button").on("click",  function() {
		j( "#p_method_PaylineCPT" ).after( "<p> wefwe fwefe</p>" );
	
});
	
	*/
	
			/////////////////////////////////////////////////////////////////////////////////////////
//Ajout d'un scroll vers les commentaires au clic sur le bouton

	j("#to_comment").on("click",  function() {
				
		j(".nav_onglet").children().removeClass("active");
		j(".onglet_detail").children().css("display","none");
		j("#product_tabs_description_tabbed_contents").css("display","none");
		j("#product_tabs_additional_tabbed_contents").css("display","none");
		j("#product_tabs_tags_tabbed_contents").css("display","none");
		
		
		
		
		j("#product_tabs_review_tabbed").addClass("active");
		j("#product_tabs_review_tabbed_contents").css("display","block");
	
		
		
		
		j("body, html").animate({
   		 scrollTop: j("#product_tabs_review_tabbed").offset().top-85
			});
		
	});





//j(".afp-slider-item").clone().appendTo(".afp-slides-container");


	
});




	