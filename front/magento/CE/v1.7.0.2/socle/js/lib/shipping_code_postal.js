// Fonction pour la section "Information de livraison"
// Verifie l'etat du code postal pour la reunion et mayotte et valide si il est bon
var j = jQuery.noConflict();
j(document).ready(function () {
		
	// init	
	var reg_codeP_RE = '^(974[0-9][0-9])$';
	var reg_codeP_YT = '^(976[0-9][0-9])$';
	var pays = j("#opc-shipping .validate-select"); 
	var codeP = j("#opc-shipping .validate-zip-international");


//j("#shipping-buttons-container button").prop('disabled',true);
// supprime la valeur de onclick pour empecher l 'envoi du formulaire
j("#shipping-buttons-container button").attr( "onclick", "" );	
	
	
j("#shipping-buttons-container button").on("click", function(e) {	
	codeP.removeClass("validation-failed");
	codeP.next().remove();
	
	// choix Reunion
	if ( pays.val() == "RE") {
		   // console.log( "pays: " + pays.val() ); 
		
		// verif le code postal
		if ( codeP.val().match(reg_codeP_RE) ) {
			 //   console.log( " bon code postal" );
			     return shipping.save();

		 }
		 else {
		 	codeP.addClass("validation-failed");
		 	codeP.after( '<div class="validation-advice" id="advice-required-entry-shipping:postcode" style="">Le code postal ne convient pas pour la Réunion.</div>' );
		 //	 console.log( " mauvais code postal" );
		 }
	}
   
   //choix Mayotte
   else if ( pays.val() == "YT") {
   console.log( "pays: " + pays.val() ); 
		
		// verif le code postal
		if ( codeP.val().match(reg_codeP_YT) ) {
			//    console.log( " bon code postal" );
			     return shipping.save();

		 }
		 else {
			e.stopPropagation()
		 	codeP.addClass("validation-failed");
		 	codeP.after( '<div class="validation-advice" id="advice-required-entry-shipping:postcode" style="">Le code postal ne convient pas pour Mayotte.</div>' );
		 //	 console.log( " mauvais code postal" );
		 }
	} 
	
   //autre choix
   else {
     //console.log( "pas reunion et pas mayotte" );
	 return shipping.save();

   }
   
return false;

});

});

	