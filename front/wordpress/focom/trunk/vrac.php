<!DOCTYPE html>
<!--[if IE 8 ]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>
<meta charset="utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Revusion - Flat Design HTML5</title>

<!-- ////////////////////////////////// -->
<!-- //     Retina Bookmark Icon     // -->
<!-- ////////////////////////////////// -->
<link rel="apple-touch-icon-precomposed" href="apple-icon.png"/>

<!-- ////////////////////////////////// -->
<!-- //     Retina Bookmark Icon     // -->
<!-- ////////////////////////////////// -->
<link rel="apple-touch-icon-precomposed" href="apple-icon.png"/>

<!-- ////////////////////////////////// -->
<!-- //      Stylesheets Files       // -->
<!-- ////////////////////////////////// -->
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/media.css"/>
<link rel="stylesheet" href="css/revolution.css" media="screen"/>
<link rel="stylesheet" href="css/media-slideshow.css" media="screen"/>
<link rel="stylesheet" href="css/noscript.css" media="screen,all" id="noscript"/>

<!-- ////////////////////////////////// -->
<!-- //     Google Webfont Files     // -->
<!-- ////////////////////////////////// -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400"/>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:400,300,200"/>

<!-- ////////////////////////////////// -->
<!-- //        Favicon Files         // -->
<!-- ////////////////////////////////// -->
<link rel="shortcut icon" href="images/favicon.ico"/>

<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>
<body>
	<!-- header start here -->
	<header>
    
    	<!-- top info start here -->
    	<div id="top-info">
        	<div class="row">
            	<div class="twelve column">
                    <div class="phonemail-area">
                        <i class="icon-phonealt"></i><span>(621) 555 6789</span>
                        <i class="icon-emailalt"></i><span>hello@revusion.net</span>
                    </div>
                    <div id="top-socials">
                        <ul class="socials-list">                                
                            <li><a href="#"><i class="social-twitter"></i></a></li>
                            <li><a href="#"><i class="social-facebook"></i></a></li>
                        </ul>
                    </div>
                    <div class="flag-area">
                    	<span>Language</span>
                        <a href="#"><img src="images/flags/id.gif" alt=""/></a>
                        <a href="#"><img src="images/flags/us.gif" alt=""/></a>
                    </div>
                </div>
            </div>
        </div>
<!-- top info end here -->      
        <div id="header-wrapper">
        	<div class="row">
	            <a href="index.html"><img id="logo" src="images/logo_big.png" alt="main-logo" class="retina"/></a>
            </div>
		</div>


<!-- navigation start here -->
        <div class="wrapper100" id="menu_container">
            <div class="row">            
                    <nav id="mainmenu">
                        <ul id="menu">
                            <li class="dropdown selected"><a href="services.html">Services</a>
                                <ul> 
                                    <li><a href="services_fonctionnement.html">Comprendre le fonctionnement de soukeo.fr</a></li>
                                    <li><a href="services_developpez_vos_ventes.html">Développez vos ventes</a></li>
                                    <li><a href="services_couts_dacquisition.html">Maîtrisez vos coûts d'acquistion client</a></li>
                                    <li><a href="services_interface_personnalisee.html">Interface personnalisé & supports</a></li>
                                    <li><a href="services_categories_disponibles.html">Catégories disponibles à la vente sur soukeo.</a></li></ul></li>
                            <li class="dropdown"><a href="nous_connaitre.html">Mieux nous connaître</a>
                                <ul> 
                                    <li><a href="nous_connaitre_qui_sommes_nous.html">Qui sommes-nous ?</a></li>
                                    <li><a href="nous_connaitre_emplois_et_stages.html">Emplois et stages</a></li>
                                    <li><a href="nous_connaitre_communiques_de_presse.html">Communiqués de presse</a></li></ul>
                <li class="dropdown"><a href="solutions_ecommerce.html">Solutions e-commerce</a>
                          <ul>
                                    <li><a href="solutions_ecommerce_creation_et_gestion_catalogue.html">Création et gestion de catalogue produit</a></li>
                                    <li><a href="solutions_ecommerce_expediez.html">Expédiez par soukeo.fr</a></li>
                                    <li><a href="solutions_ecommerce_data_et_marketing.html">Data & marketing</a></li>                                        	
                                </ul>
                            </li>
                            <li class="dropdown"><a href="ressources.html">Ressources</a>
                          <ul>
                                    <li><a href="ressources_faq.html">Foire aux questions</a></li>
                                    <li><a href="ressources_supports_technique.html">Support technique</a></li>
                                    <li><a href="ressources_support_commercial.html">Support commercial</a></li>                                        	
                                    <li><a href="ressources_fomation_a_distance.html">Formation à distance</a></li>
                                    <li><a href="ressources_formation_collective.html">Formation collective</a></li>                                        	
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact</a></li>                                
                        </ul>
                    </nav>
	            </div>
			</div>                        
<!-- navigation end here -->  
    </header>


<!-- header end here -->
 
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="#">Pages</a></li>
                    <li class="current-page"><a href="#">FAQ</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Foire aux questions</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->

    <section id="content-wrapper">          

        <div class="row grey-column">
        	<div class="twelve column">
                <h4>Ce que dise nos clients</h4>                     
            </div>                         
            
            <div class="four column">
                <!-- begin of testimonials 1 -->                    
                <div class="testi-container">                        	
                    <div class="testi-text">
                        <blockquote>
                            <p>I searched all over and looked at many different company websites to decide which one to use and yours was by far the best. The designs are amazing.</p>
                        </blockquote>                                                         
                    </div>
                    <div class="clear"></div>                                                                      
                </div>
                <div class="testi-baloon"></div>
                <div class="testi-image">
                    <img src="images/sample_images/team1.jpg" alt="" />                                                                      
                </div>
                <div class="testi-name">Mario Benneti<br/><span class="company-name">Marketing &ndash; <span class="text-info">Arrow Tech Inc</span></span></div>	                                  
            </div>
                                
            <div class="four column">
                <!-- begin of testimonials 2 -->                                       
                <div class="testi-container">                        	
                    <div class="testi-text">
                        <blockquote>
                            <p>Endymion are always helpful if we ever have a problem, with no faults or interruptions in the service. Overall, we are happy. Keep up the good work.</p>
                        </blockquote>                              
                    </div>                        
                    <div class="clear"></div>                                                                     
                </div>
                <div class="testi-baloon"></div>
                <div class="testi-image">
                    <img src="images/sample_images/team2.jpg" alt="" />                                              
                </div>                    
                <div class="testi-name">Sarah Waterson<br/><span class="company-name">Marketing &ndash; <span class="text-info">Northern Inc</span></span></div>	 
            </div>
            
            <div class="four column">
                <!-- begin of testimonials 3 -->                                       
                <div class="testi-container">                        	
                    <div class="testi-text">
                        <blockquote>
                            <p>Cygnus is by far the best company I have used to handle my Web site and software development needs. They are professional and attentive.</p>
                        </blockquote>                              
                    </div>                        
                    <div class="clear"></div>                                                                     
                </div>
                <div class="testi-baloon"></div>
                <div class="testi-image">
                    <img src="images/sample_images/team3.jpg" alt="" />                                              
                </div>                    
                <div class="testi-name">Don Kuzenko<br/><span class="company-name">Marketing &ndash; <span class="text-info">Gold Coast Inc</span></span></div>	 
            </div>
        </div>

        <div class="row grey-column">
        	<div class="twelve column">
            	<h4>Les derniers commerçants inscrits</h4>
            
                <ul id="content-carousel">
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p1.jpg" alt="Post Image">
                                    <!-- <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p1.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div>--> 
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 1</h6>
                            <h6 class="subheader">Description here</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p2.jpg" alt="Post Image">
                                    <!--<div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p2.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> -->
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 2</h6>
                            <h6 class="subheader">Description</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p3.jpg" alt="Post Image">
                                    <!--<div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p3.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> -->
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 3</h6>
                            <h6 class="subheader">Description</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p4.jpg" alt="Post Image">
                                    <!--<div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p4.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> -->
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 4</h6>
                            <h6 class="subheader">Description</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p5.jpg" alt="Post Image">
                                    <!--<div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p5.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> -->
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 5</h6>
                            <h6 class="subheader">Some description here</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p6.jpg" alt="Post Image">
                                    <!--<div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p6.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> -->
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 6</h6>
                            <h6 class="subheader">Some description here</h6>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
            


        <div class="row margin-top2">
        	<div class="seven column">
            	<h3>Soukéo est une place de marché électronique basée à l'île de la Réunion.</h3>
                <p class="lead"> Que vous soyez e-commerçants ou commerçants nous sommes votre partenaire et nous vous aidons à vendre plus et mieux.Chaque jour, plusieurs milliers d'internautes se rendent sur Soukeo.fr.</p>                
                <p class="lead">Vous aussi, commencez à vendre sur Soukeo.fr dès aujourd'hui !</p>                
                
                <a class="button medium blue" href="#">Vendre sur Soukéo !</a>
            </div>
            <div class="five column">
            	<img src="images/soukeo.gif" style="margin:0 auto; width:400px text-align:center;">
            </div>
        </div>        
   </section>      
    
      
    <!-- content section start here -->
    <section id="content-wrapper">          
        

        <div class="row">        	
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-html"></i>
                </div>
                <h4>Semantic Code</h4>
                <p class="text-overflow">Reprehenderit et quine voluptate velitesse quami nihil molestiae dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat</p>
            </div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-barchart"></i>
                </div>
                <h4>Professional Design</h4>
                <p class="text-overflow">Reprehenderit et quine voluptate velitesse quami nihil molestiae dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat</p>  
            </div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-websitebuilder"></i>
                </div>
                <h4>Easy to Customize</h4>
                <p class="text-overflow">Reprehenderit et quine voluptate velitesse quami nihil molestiae dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat</p>  
            </div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-flaskfull"></i>
                </div>
                <h4>Latest Technology</h4>
                <p class="text-overflow">Reprehenderit et quine voluptate velitesse quami nihil molestiae dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat</p>  
            </div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-map"></i>
                </div>
                <h4>Custom Google Map</h4>
                <p class="text-overflow">Reprehenderit et quine voluptate velitesse quami nihil molestiae dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat</p>  
            </div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-shoppingbag"></i>
                </div>
                <h4>Complete Features</h4>
                <p class="text-overflow">Reprehenderit et quine voluptate velitesse quami nihil molestiae dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat</p>  
            </div> 
        </div>
        

        
        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text">
                        <h3>Efficiently unleash cross-media information</h3>
                        <p>Completely synergize resource sucking relationships via premier niche markets.</p>
                    </div>
                    <div class="promo-button">
                        <a href="#" class="button medium blue">Try it free today</a>
                    </div>
                </div>                                   
            </div>
        </div>
        
        <div class="row">     
            <div class="twelve column text-center">              
            	<h3 class="smallmargin-bottom">Vendre sur <span class="highlight">Soukéo</span></h3>
                <p>Soukéo est une place de marché électronique basée à l'île de la Réunion. Que vous soyez e-commerçants ou commerçants nous sommes votre partenaire et nous vous aidons à vendre plus et mieux.Chaque jour, plusieurs milliers d'internautes se rendent sur Soukeo.fr.</p>                
                <p>Vous aussi, commencez à vendre sur Soukeo.fr dès aujourd'hui !</p>
            </div>
            
            <div class="twelve column">
            	<hr/>
            </div>                                       
        </div>
        
        <div class="row">        	
            <div class="six column mobile-two">
            	<h4>Semantic Coding</h4>
                <i class="flat-config float-right"></i>
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae consequatur velillum quile dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat non proident sunt culpa qui officia</p>                
            </div>
            
            <div class="six column mobile-two">
            	<h4>Fully Responsive</h4>
                <i class="flat-responsive float-right"></i>
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae consequatur velillum quile dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat non proident sunt culpa qui officia</p>
            </div>
            
            <div class="six column mobile-two">
            	<h4>Competitve Price</h4>
                <i class="flat-money float-right"></i>
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae consequatur velillum quile dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat non proident sunt culpa qui officia</p>
            </div>
            
            <div class="six column mobile-two">
            	<h4>Flexible Template</h4>
                <i class="flat-arrow float-right"></i>
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae consequatur velillum quile dolorem eum fugiat quo voluptas nulla pariatur occaecat cupidatat non proident sunt culpa qui officia</p>
            </div> 
        </div>
        
        <div class="row grey-column">
        	<div class="twelve column">
            	<h4>Les derniers commerçants inscrits</h4>
            
                <ul id="content-carousel">
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p1.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p1.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 1</h6>
                            <h6 class="subheader">Description here</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p2.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p2.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 2</h6>
                            <h6 class="subheader">Description</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p3.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p3.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 3</h6>
                            <h6 class="subheader">Description</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p4.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p4.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Commerçant 4</h6>
                            <h6 class="subheader">Description</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p5.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p5.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Work Title 5</h6>
                            <h6 class="subheader">Some description here</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p6.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p6.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Work Title 6</h6>
                            <h6 class="subheader">Some description here</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p7.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p7.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Work Title 7</h6>
                            <h6 class="subheader">Some description here</h6>
                        </div>
                    </li>
                    
                    <li>
                    	<div class="teaser">                                                
                            <div class="teaser-preview-box">
                                    
                                <div class="lightbox-item"> 
                                    <img src="images/portfolio_thumb/p8.jpg" alt="Post Image">
                                    <div class="lightbox-item-overlay-content"> 
                                        <a class="preview fancybox" href="images/portfolio_big/p8.jpg" data-fancybox-group="gallery" title="Some of project description here"><i class="icon-search"></i></a>
                                        <a class="permalink" href="portfolio-single.html"><i class="icon-link"></i></a>
                                    </div> 
                                </div>  
                                    
                            </div>
                            <h6>Work Title 8</h6>
                            <h6 class="subheader">Some description here</h6>
                        </div>
                    </li>                     
                                                           
                </ul>
            </div>
        </div>
        
        <div class="row">
        	<div class="twelve column">
                <ul class="tabs-left">
                    <li><a href="#flat-design">Flat Design</a></li>
                    <li><a href="#fully-responsive">Fully Responsive</a></li>
                    <li><a href="#latest-technology">Latest Technology</a></li>
                    <li><a href="#retina-support">Retina Support</a></li>                
                </ul>
                <div class="tab_container-left">
                    <div id="flat-design" class="tab_content-left"> 
                        <h4>Flat and Minimalist Design</h4>
                        <img src="images/sample_images/img_sample.png" alt="" class="img-left imghide-mobile"/>                          
                        <p>Lorem ipsum dolor amet, consectetur adipite scinelit vestibulum vel quam sitare amet odio ultricies dapbus acer vitae.</p>
                        <ul class="disc text-overflow">
                            <li>Exercitationem ullam corporis</li>
                            <li>Doloremque laudantium totames</li>
                            <li>Perspiciatis ndeomniste error</li>
                            <li>Corporis suscipit laboriosam</li>
                        </ul>
                        <p>Sgue duis nulla nunc dignissim ullamcorper minima veniam quis nostrum exercitationem ullam corporis suscipit laboriosam.</p>                               
                    </div>
                                                        
                    <div id="fully-responsive" class="tab_content-left">                            
                        <h4>Fully Responsive</h4>
                        <img src="images/sample_images/img_sample.png" alt="" class="img-left imghide-mobile"/>                          
                        <p>Lorem ipsum dolor amet, consectetur adipite scinelit vestibulum vel quam sitare amet odio ultricies dapbus acer vitae.</p>
                        <ul class="disc text-overflow">
                            <li>Exercitationem ullam corporis</li>
                            <li>Doloremque laudantium totames</li>
                            <li>Perspiciatis ndeomniste error</li>
                            <li>Corporis suscipit laboriosam</li>
                        </ul>
                        <p>Sgue duis nulla nunc dignissim ullamcorper minima veniam quis nostrum exercitationem ullam corporis suscipit laboriosam.</p>                                        
                    </div>
                                                        
                    <div id="latest-technology" class="tab_content-left">                            
                        <h4>Latest Technology</h4>
                        <img src="images/sample_images/img_sample.png" alt="" class="img-left imghide-mobile"/>                          
                        <p>Lorem ipsum dolor amet, consectetur adipite scinelit vestibulum vel quam sitare amet odio ultricies dapbus acer vitae.</p>
                        <ul class="disc text-overflow">
                            <li>Exercitationem ullam corporis</li>
                            <li>Doloremque laudantium totames</li>
                            <li>Perspiciatis ndeomniste error</li>
                            <li>Corporis suscipit laboriosam</li>
                        </ul>
                        <p>Sgue duis nulla nunc dignissim ullamcorper minima veniam quis nostrum exercitationem ullam corporis suscipit laboriosam.</p>                                     
                    </div>
                    <div id="retina-support" class="tab_content-left">                            
                        <h4>Retina Support</h4>
                        <img src="images/sample_images/img_sample.png" alt="" class="img-left imghide-mobile"/>                          
                        <p>Lorem ipsum dolor amet, consectetur adipite scinelit vestibulum vel quam sitare amet odio ultricies dapbus acer vitae.</p>
                        <ul class="disc text-overflow">
                            <li>Exercitationem ullam corporis</li>
                            <li>Doloremque laudantium totames</li>
                            <li>Perspiciatis ndeomniste error</li>
                            <li>Corporis suscipit laboriosam</li>
                        </ul>
                        <p>Sgue duis nulla nunc dignissim ullamcorper minima veniam quis nostrum exercitationem ullam corporis suscipit laboriosam.</p>                                    
                    </div>                                                                 
                </div>
            </div>
            
    	<div class="row">
        	<div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-download"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>
                <a href="#" class="button small blue">More Info</a>
            </div>
            
            <div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-map"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>  
                <a href="#" class="button small blue">More Info</a>
            </div>
            
            <div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-lock"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>
                <a href="#" class="button small blue">More Info</a>  
            </div>
            
            <div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-clock"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>
                <a href="#" class="button small blue">More Info</a>  
            </div>
        </div>
        
        <div class="row margin-top2">
        	<div class="seven column">
            	<h3>Proactively envisioned multimedia based expertise and cross-media growth strategies, revolutionize global sources through ideas.</h3>
                <p class="lead">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui</p>
                
                <div class="note">
                	<div class="row">
                    
                    	<div class="six column mobile-two">
                        	<address>
                              <strong>Revusion, Inc.</strong><br>
                              <i class="icon-home"></i>Jend Sudirman Lot 25, INA<br/>
                              <i class="icon-phonealt"></i>(621) 555 6789<br/>
                              <i class="icon-emailalt"></i>hello@Revusion.net
                            </address>
                        </div>
                        <div class="six column mobile-two">
                        	<address>
                              <strong>Business Hours</strong><br>
                              <i class="icon-time"></i>Monday - Friday : 9AM to 5PM<br/>
                              <i class="icon-time"></i>Saturday : 10AM to 2PM<br/>
                            </address>
                        </div>
                    
                    </div>
                </div>
            </div>
            
            <div class="five column">
            	<img src="images/sample_images/services_img2.png" alt="" class="img-left imghide-tablet imghide-mobile"/>
            </div>
        </div>       

        <div class="row">        	
            <div class="three column">
            	<div class="note-folded blue">
                	<h4>Why Must Choose Revusion</h4>
                    <p>Coluptatem set nesciunt neque porrola quisquam estel quile dolorem ipsut corporis eti laboriosam commodil consequatur</p>
                </div>                
            </div>
            
            <div class="nine column">
            	<div class="row">
                	<div class="four column mobile-two">
                    	<div class="flatborder-alt">
                            <i class="flat-arrow small"></i>
                        </div>
                        <h4>Flexibel</h4>
                        <p class="text-overflow">Reprehenderit qui in ea voluptate velit esse quam nihil molestiae eum consequatur vel</p>
                    </div>
                    
                    <div class="four column mobile-two">
                    	<div class="flatborder-alt">
                            <i class="flat-photo small"></i>
                        </div>
                        <h4>Awesome</h4>
                        <p class="text-overflow">Reprehenderit qui in ea voluptate velit esse quam nihil molestiae eum consequatur vel</p>
                    </div>
                    
                    <div class="four column mobile-two">
                    	<div class="flatborder-alt">
                            <i class="flat-letter small"></i>
                        </div>
                        <h4>Elegants</h4>
                        <p class="text-overflow">Reprehenderit qui in ea voluptate velit esse quam nihil molestiae eum consequatur vel</p>
                    </div>
                    
                    <div class="four column mobile-two">
                    	<div class="flatborder-alt">
                            <i class="flat-responsive small"></i>
                        </div>
                        <h4>Responsive</h4>
                        <p class="text-overflow">Reprehenderit qui in ea voluptate velit esse quam nihil molestiae eum consequatur vel</p>
                    </div>
                    
                    <div class="four column mobile-two">
                    	<div class="flatborder-alt">
                            <i class="flat-wallet small"></i>
                        </div>
                        <h4>Efficient</h4>
                        <p class="text-overflow">Reprehenderit qui in ea voluptate velit esse quam nihil molestiae eum consequatur vel</p>
                    </div>
                    
                    <div class="four column mobile-two">
                    	<div class="flatborder-alt">
                            <i class="flat-compas small"></i>
                        </div>
                        <h4>Compatible</h4>
                        <p class="text-overflow">Reprehenderit qui in ea voluptate velit esse quam nihil molestiae eum consequatur vel</p>
                    </div>
                </div>
            </div>
        </div>
        
        
                
        
        
        <div class="row">
        	<div class="twelve column">
            	<h4 class="smallmargin-bottom">Nos partenaires</h4>
                
                <ul class="client-box">
                    <li><img src="images/sample_images/client1.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client2.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client3.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client4.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client5.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client6.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client7.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client8.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client9.png" alt="" class="retina" /></li>
                    <li><img src="images/sample_images/client10.png" alt="" class="retina" /></li>                 
                </ul>
            </div>
        </div>
                          
            <div class="twelve column">
            	<hr/>
            </div>
        </div>
        
        <div class="row grey-column">
        	<div class="twelve column">
                <h4>Ce que dise nos clients</h4>                     
            </div>                         
            
            <div class="four column">
                <!-- begin of testimonials 1 -->                    
                <div class="testi-container">                        	
                    <div class="testi-text">
                        <blockquote>
                            <p>I searched all over and looked at many different company websites to decide which one to use and yours was by far the best. The designs are amazing.</p>
                        </blockquote>                                                         
                    </div>
                    <div class="clear"></div>                                                                      
                </div>
                <div class="testi-baloon"></div>
                <div class="testi-image">
                    <img src="images/sample_images/team1.jpg" alt="" />                                                                      
                </div>
                <div class="testi-name">Mario Benneti<br/><span class="company-name">Marketing &ndash; <span class="text-info">Arrow Tech Inc</span></span></div>	                                  
            </div>
                                
            <div class="four column">
                <!-- begin of testimonials 2 -->                                       
                <div class="testi-container">                        	
                    <div class="testi-text">
                        <blockquote>
                            <p>Endymion are always helpful if we ever have a problem, with no faults or interruptions in the service. Overall, we are happy. Keep up the good work.</p>
                        </blockquote>                              
                    </div>                        
                    <div class="clear"></div>                                                                     
                </div>
                <div class="testi-baloon"></div>
                <div class="testi-image">
                    <img src="images/sample_images/team2.jpg" alt="" />                                              
                </div>                    
                <div class="testi-name">Sarah Waterson<br/><span class="company-name">Marketing &ndash; <span class="text-info">Northern Inc</span></span></div>	 
            </div>
            
            <div class="four column">
                <!-- begin of testimonials 3 -->                                       
                <div class="testi-container">                        	
                    <div class="testi-text">
                        <blockquote>
                            <p>Cygnus is by far the best company I have used to handle my Web site and software development needs. They are professional and attentive.</p>
                        </blockquote>                              
                    </div>                        
                    <div class="clear"></div>                                                                     
                </div>
                <div class="testi-baloon"></div>
                <div class="testi-image">
                    <img src="images/sample_images/team3.jpg" alt="" />                                              
                </div>                    
                <div class="testi-name">Don Kuzenko<br/><span class="company-name">Marketing &ndash; <span class="text-info">Gold Coast Inc</span></span></div>	 
            </div>
        </div>
                 
    </section>
    <!-- content section end here -->
    
    <footer>   
        <div class="row">
        	<div class="three column">
            	<div id="logo-footer"><a href="index.html"><img src="images/logo_footer.png" alt="main-logo" class="retina"/></a></div>
            	<p class="copyright">&copy; 2013 Soukeo - Ile de la Réunion - tous droits réservés</p>
                <ul class="social-list circle-social">
                	<li><a href="#"><i class="social-facebook"></i></a></li>
                    <li><a href="#"><i class="social-twitter"></i></a></li>
                    <li><a href="#"><i class="social-dribbble"></i></a></li>
                    <li><a href="#"><i class="social-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="three column mobile-five">
            	<h4>Services</h4>
            	<ul class="no-bullet">
                    <li><a href="services_fonctionnement.html">Comprendre le fonctionnement de soukeo.fr</a></li>
                    <li><a href="services_developpez_vos_ventes.html">Développez vos ventes</a></li>
                    <li><a href="services_couts_dacquisition.html">Maîtrisez vos coûts d'acquistion client</a></li>
                    <li><a href="services_interface_personnalisee.html">Interface personnalisé & supports</a></li>
                    <li><a href="services_categories_disponibles.html">Catégories disponibles à la vente sur soukeo.</a></li>
                </ul>
            </div>
            <div class="three column mobile-five">
            	<h4>Nous connaître</h4>
            	<ul class="no-bullet">
                    <li><a href="nous_connaitre_qui_sommes_nous.html">Qui sommes-nous ?</a></li>
                    <li><a href="nous_connaitre_emplois_et_stages.html">Emplois et stages</a></li>
                    <li><a href="nous_connaitre_communiques_de_presse.html">Communiqués de presse</a></li>
                </ul>
            </div>
            <div class="three column mobile-five">
            	<h4>Ressources</h4>
            	<ul class="no-bullet">
                    <li><a href="ressources_faq.html">Foire aux questions</a></li>
                    <li><a href="ressources_supports_technique.html">Support technique</a></li>
                    <li><a href="ressources_support_commercial.html">Support commercial</a></li>                                        	
                    <li><a href="ressources_fomation_a_distance.html">Formation à distance</a></li>
                    <li><a href="ressources_formation_collective.html">Formation collective</a></li>         
                </ul>
            </div>	           
        </div>
    </footer>     

<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/jquery.themepunch.plugins.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/superfish.js"></script>
<script src="js/mediaelement-and-player.min.js"></script>
<script src="js/jquery.flexisel.js"></script>
<script src="js/jquery.fancybox.js?v=2.0.6"></script>
<script src="js/jquery.fancybox-media.js?v=1.0.3"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/accordion-functions.js" ></script>
<script src="js/theme-functions.js"></script>
<script src="js/jquery.retina.js"></script>

<script>$('#noscript').remove();</script>
</body>
</html>