<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>



<!-- header end here -->
 
    
    
     <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="ressources.php">Ressources</a></li>
                    <li class="current-page"><a href="#">FAQ</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Foire aux questions</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->
      
    <!-- content section start here -->
    <section id="content-wrapper">
                
        <div class="row">        	
            <div class="six column">
            	<h3> Questions commerciales</h3>               

                <div class="accordion-title">Qui peut vendre sur Soukeo.fr ?</div>
                <div class="accordion-content">
                    <p>Toute entreprise qui possède un numero de SIRET , qu'elle soit une SARL ou une entreprise individuelle.</p>
                    <p>Les associations peuvent également vendre sur soukéo, et pour les soutenir dans leurs actions Soukéo ne leur facturera aucun abonnement mensuel.</p> </div>
                <div class="accordion-title">Quels documents doit fournir le vendeur ?</div>
                <div class="accordion-content">
                    <p>Les documents à fournir sont une copie de la carte d'identité du gérant, un rib, et un extrait kbis.</p></div>                    
                    

                <div class="accordion-title">Qui détermine le prix de vente des produits ?</div>
                <div class="accordion-content">
                    <p>Le commerçant est le seul à déterminer son prix de vente, toutefois Soukeo demande à ses participants de repsecter la parité entre son éventuel site internet et la place de marché.</p>
                    <p>Le vendeur s'engage dans les conditions générales à ne pas vendre plus cher sur la place de marché que dans son magasin physique ou virtuel.</p> </div>
                <div class="accordion-title">Qui peut répondre à mes problématiques techniques ?</div>
                <div class="accordion-content">
                    <p>Le service technique est présent pour répondre aux demande qui n'auraient pas trouvé de réponse dans la FAQ et dans l'assistance technique en ligne.</p></div>
                <div class="accordion-title">Qui gère les fiches-produits ?</div>
                <div class="accordion-content">
                    <p>Les fiches-produits sont gérées par Soukéo.</p>
                    <p>Si le produit existe dans la base de données Soukeo, le vendeur ne renseigne que le prix et le stock dont il dispose.</p>
                    <p>Dans le cas contraire une nouvelle fiche produit sera crée manuellement par le vendeur.</p> </div>                                        
                <div class="accordion-title">Qui s'occupe du paiement ?</div>
                <div class="accordion-content">
                    <p>Le paiement est intégralement géré par Soukéo avec ses partenaires (Société Générale ou La Banque Postale). </p>
                    <p>Vous ne vous occupez que de préparer votre commande, Soukéo s'occupe du reste.</p> </div>
                <div class="accordion-title">Ou se trouve votre magasin ?</div>
                <div class="accordion-content">
                    <p>La place de marché soukéo est présente uniquement sur internet , cependant vous pouvez nous joindre par mail ou par téléphone pour toute demande de renseignements.</p></div>
                <div class="accordion-title">Combien cela me coute de m'inscrire chez Soukeo ?</div>
                <div class="accordion-content">
                    <p>Si vous possédez déjà en boutique en ligne votre inscription est gratuite sur le site de soukéo, si vous êtes commercant l'abonnement mensuel est de 49 euros HT</p>
                    <p>Retrouvez le détail de notre grille tarifaire sur <a href="solutions_ecommerce_combien_ca_coute.php">la page dédiée.</a></p> </div>
                <div class="accordion-title">Comment vendre sur Soukeo ?</div>
                <div class="accordion-content">
                    <p>Vendre sur Soukéo est simple : inscrivez vous en tant que vendeur , mettez vos produits en ligne, vendez dans tout l'Océan Indien.</p> </div>
                <div class="accordion-title">Comment expédier mes colis</div>
                <div class="accordion-content">
                    <p>Selon vos préférences et la demande de l'acheteur vous devrez, au choix :</p>
                    <ul class="arrow">
                    	<li class="arrow">confier le colis à un de nos partenaire qui l'enlèvera au lieu de stockage</li>
                        <li>soit expédier le colis par la poste </li>
                        <li> soit assurer la livraison vous-même.</li></ul></div>
                <div class="accordion-title">Comment promouvoir mes produits ?</div>
                <div class="accordion-content">
                    <p>Soukéo est un espace pensé pour faciliter le commerce en ligne, vous êtes libre d'organiser vos campagnes de promotions de façon automatisée et à l'avance en relation avec notre service commercial.</p></div>
                <div class="accordion-title">Comment sont gérés mes paiements ? Combien coûte vos services</div>
                <div class="accordion-content">
                    <p>Afin de garantire une satisfaction client maximale, Soukéo garde le montant de la transaction jusqu’à la fin du délais de rétractation du client (15 jours), puis rétribue le commerçant déduction faîte de notre commission.</p>
                    <p>En dehors de l'abonnement (gratuit si vous disposez déja d'un site e-commerce), Soukéo prélève une commission variable par catégorie de produits. </div>
                <div class="accordion-title">Qui est responsable du contrat de vente ?</div>
                <div class="accordion-content">
                    <p>Soukéo est uniquement un intermédiaire de vente. Le vendeur mandate donc soukéo pour effectuer la vente en son nom.</p>
                    <p> Le vendeur est le seul responsable en cas de défaillance du produit vendu. Soukéo n'intervient qu'en tant que facilitateur de l'acte de vente.</p></div>
                <div class="accordion-title">Comment gérer les retours produits ?</div>
                <div class="accordion-content">
                    <p>Les litiges sont gérés entre le vendeur et le client, via l'interface de Soukéo. En cas de désaccord, Soukéo sez réserve le droit d'intervenir en tant qu'intermédiaire pour la résolution du litige.</p></div>
                <div class="accordion-title">Combien de temps suis-je engagé ?</div>
                <div class="accordion-content">
                    <p>Il n'y a pas d'engagement sur la durée, vous êtes libre d'arrêter la collaboration avec la plate forme à tout moment. </p> </div>
                 <div class="accordion-title">Quelle protection contre la fraude à la carte bancaire ?</div>
                <div class="accordion-content">
                    <p>Notre partenariat avec Payline (opérateur bancaire international) nous permet de garantir les transactions bancaires contre la fraude et l'utilisation abusive.</p>
                    <p>Soukéo à mis en place des critères stricts en matière de lutte contre la fraude avec le dispositif 3D secure, le cryptage des données bancaires, des pages hautement sécurisées via le protocole SSL v3 256bits.</p>
                    <p>Enfin, chaque action est historisée permettant une tracabilité globale des ordres sur le compte vendeur.</p></div>                            	    
            </div>

            <div class="six column">
            	<h3> Questions techniques</h3>
                <h4>Non disponible pour le moment</h4>               
            </div>


        </div>    
                 
 

     
<!--    	<div class="row">
        	<div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-download"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>
                <a href="#" class="button small blue">More Info</a>
            </div>
            
            <div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-map"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>  
                <a href="#" class="button small blue">More Info</a>
            </div>
            
            <div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-lock"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>
                <a href="#" class="button small blue">More Info</a>  
            </div>
            
            <div class="three column text-center mobile-two">
            	<div class="flatborder-bottom">                           
                	<i class="flat-clock"></i>
                </div>    
                <h4>Semantic Coding</h4> 
                <p>Reprehenderit quine voluptate velitesse quami nihil molestiae voluptas nulla pariatur occaecat</p>
                <a href="#" class="button small blue">More Info</a>  
            </div>
        </div>


      	<hr/>-->
                

        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text">
                        <h3>Pas de réponse ?</h3>
                        <p>Contactez notre support client, nous répondrons à votre demande.</p>
                    </div>
                    <div class="promo-button">
                        <a href="ressources_support_technique.php" class="button medium blue">Contacter le support technique</a>
                        <a href="ressources_support_commercial.php" class="button medium blue">Contacter le support commercial</a>                        
                    </div>
                </div>                                   
            </div>
        </div>                 
                 
    </section>
 
 
 
 
 
    <!-- content section end here -->

 <?php echo $vue->getFooter(); ?>   

<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>