  $(document).ready(function() {		
  $('#buttonsend').click( function() {
		
		var civ 	= $('#civilite').val(); //ok
		var name    = $('#name').val(); //ok
		var prenom 	= $('#prenom').val(); //

		var email   = $('#email').val(); //ok
		var message = $('#message').val(); //ok
		
		var entreprise 	= $('#entreprise').val();
		var adresse		= $('#adresse').val();
		var codepostal	= $('#codepostal').val();
		var ville 		= $('#ville').val();
		var pays 		= $('#pays').val();
		var tel 		= $('#telephone').val();
		var siret 		= $('#siret').val();
		var siteweb		= $('#siteweb').val();
		var ecom 	= $('#ecommerce').val();
		var ecom_tec 	= $('#ecommerce_techno').val();
		
		
		
		$('.loading').fadeIn('fast');
		
		if (name != ""  && email != ""  && prenom != "" )
			{

				$.ajax(
					{
						url: './inc/scripts/sendform.php',
						type: 'POST',
						data: "name=" + name  + "&email=" + email + "&cle=inscription" + "&civ=" + civ+ 
							  "&prenom=" + prenom + "&entreprise=" + entreprise + "&adresse=" + adresse + "&codepostal=" + codepostal + "&ville=" + ville +
							  "&pays=" + pays+ "&tel=" + tel + "&siret=" + siret+ "&siteweb=" + siteweb+ "&ecom=" + ecom+ "&message=" + message +
							  "&ecom_tec=" + ecom_tec,
						success: function(result) 
						{
							$('.loading').fadeOut('fast');
							if(result == "email_error") {
								$('#email').css({"background":"#FFFCFC","border":"1px solid #ffadad"}).next('.require').text(' !');
							} else {
								$('#name, #subject, #email, #message').val("","Name","Subject","Email","Message");
								$('<div class="success-contact">Votre message a bien été envoyé . Merci !  </div>').insertBefore('#contact-form-area, #contact-form-area2');
								$('.success-contact').fadeOut(10000, function(){ $(this).remove(); });
							}
						}
					}
				);
				return false;
				
			} 
		else 
			{
				$('.loading').fadeOut('fast');
				if( name == "","Name") $('#name').css({"background":"#FFFCFC","border":"1px solid #ffadad"});
				if( prenom == "","Prenom") $('#prenom').css({"background":"#FFFCFC","border":"1px solid #ffadad"});
				if(email == "","Email" ) $('#email').css({"background":"#FFFCFC","border":"1px solid #ffadad"});
				return false;
			}
	});
	
		$('#name, #subject, #email, #prenom').focus(function(){
			$(this).css({"border":"none","background":"#fafafa","border":"1px solid #ccc"});
		});
      	
		});