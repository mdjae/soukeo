<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
    
    
       
     <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="solutions_ecommerce.php">Solutions e-commerce</a></li>
                    <li class="current-page"><a href="#">Création et gestion de catalogue</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Création et gestion de catalogue</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->
      
    <!-- content section start here -->
    <section id="content-wrapper">
    	<div class="row">
            <h3 class="column twelve">Augmentez votre visibilité et attirez de nouveaux clients !</h3>
        	<div class="column seven">
                <p class="lead">Vos fiches ainsi que votre catalogue produits doivent être performants.</p>
                <ul class="arrow">
                    <li>Vous n’avez pas le temps de créer, structurer vos catalogues et bases de données produits ?</li>
                    <li>Vous ne savez pas comment créer votre catalogue produit ? </li>
                    <li>Vous n’avez pas de base de données produits structurée ?</li>
                    <li>Vous n’avez pas de fichier de gestion de stock ?</li></ul>
                <p class="lead">Nous nous occupons de tout et vous aiderons à créer votre catalogue produit. Nous pouvons également gérer vos catalogues pour vous. Ce service a un coût, fonction du nombre de vos produits mis en vente.</p>
                <p>Comment-faire ?</p>
                <p>Faites-nous parvenir un email, expliquez-nous votre problématique et nous reviendrons vers vous afin de trouver la meilleur solution.</p>
                <a href="contact.php" class="orange medium button">Contactez-nous !</a></div>
            
			<div class="five column">
            	<img src="images/visu_gestion_catalogue.jpg" ></div>    
		</div>
	</section>





















    <!-- content section end here -->
  <?php echo $vue->getFooter(); ?>   

<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>