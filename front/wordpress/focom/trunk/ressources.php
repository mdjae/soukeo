<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
    
    
    
       <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li class="current-page"><a href="#">Ressources</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Ressources</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
            
    
       <div id="categories" class="row">        	
            <div class="six column mobile-two">
            	<h4>Foire aux questions</h4>
                <i class="icon-sortbynameascending-atoz"></i>
                <p>Si vous n'avez pas trouvé de réponse à vos questions sur le site, essayez la Foire Aux Questions. Elle évolue chaque jour en fonction des demandes de nos clients.</p>  
                <a href="ressources_faq.php" class="button small orange">en savoir plus</a>                                  
            </div>
            <div class="six column mobile-two">
            	<h4>Support technique</h4>
                <i class="icon-debug"></i>
                <p>A chaque problème sa solution, contactez notre service technique en cas de difficulté technique comme l'import des fiches produits.</p>  
                <a href="ressources_supports_technique.php" class="button small orange">en savoir plus</a>                                  
            </div>
            <hr />     
            <div class="six column mobile-two">
            	<h4>Support commercial</h4>
                <i class="icon-shoppingcartalt"></i>
                <p>Vous avez des questions d'ordre commerciales, vous souhaitez monter un partenariat fort : contactez la direction commerciale.</p>  
                <a href="ressources_support_commercial.php" class="button small orange">en savoir plus</a>                                  
            </div>
            <div class="six column mobile-two">
            	<h4>Formation à distance</h4>
                <i class="icon-podcast"></i>
                <p>Retrouvez l'integralité de nos supports de formation à l'utilisation de Soukéo. Des tutoriels, des vidéos et plein de ressources pour vous aider à utiliser efficacement la market place.</p>  
                <a href="ressources_fomation_a_distance.php" class="button small orange">en savoir plus</a>                                  
            </div>
            <hr />                 
            <div class="six column mobile-two">
            	<h4>Formation collective</h4>
                <i class="icon-groups-friends"></i>
                <p>Venez nous rencontrer lors des petits déjeunés Soukéo. Nous en organisons chaque mois à travers l'île, à destination des commerçants et de ceux qui souhaitent découvrir les possibilités offertes par Soukéo.</p>  
                <a href="ressources_formation_collective.php" class="button small orange">en savoir plus</a>                                  
            </div>
            


        </div>
                
    
    






	</section>
    <!-- content section end here -->   
    
    
    























    <!-- content section end here -->
    
 <?php echo $vue->getFooter(); ?>   

<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>