<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
 
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="services.php">Services</a></li>                    
                    <li class="current-page"><a href="#">Développez vos ventes</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Développez vos ventes</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     

       <div class="row margin-top2">   
        	<div class="seven column">
	            <h3 class="">Des milliers d’internautes réunionnais visitent chaque jour soukeo.fr.<br/> Profitez-en !</h3>    
                <p class="">Elargissez votre audience et augmentez votre chiffre d’affaire tout en maitrisant votre rentabilité.</p>
                <p>Développez vos ventes avec un nouveau marché !</p>
   <a href="inscription.php" class=" button orange medium ">Inscrivez-vous</a>
            <div class="note" style="margin-right:30px;margin-top: 80px;">
            
				<h5>Quelques chiffres du e-commerce à la Réunion</h5> 
                <p class="italic">(* source TIC TRACK Ipsos Region Reunion – 4eme Trim 2010</p>
                <ul class="arrow">
                    <li>72% des réunionnais disposent d’un abonnement Internet à leur domicile*</li>
                    <li>20% des réunionnais déclare effectuer des achats en ligne chaque mois, soit entre 90 à 110 000 acheteurs chaque mois*</li>
                    <li>Le e-commerce réunionnais génère 280 millions d’euros chaque année (uniquement produit, hors services/immatériel)</li></ul>
            </div>
                   
            </div>
            <div class="five column">
            	<img src="images/visu_developper_vos_ventes.jpg" style="margin:0 auto; width:400px; float:right; text-align:center;">
            </div>
            
            
			<div class="column five note-folded orange">
                <h4>280 millions d’euros !</h4>
                <p >C’est l’estimation de ce que les réunionnais ont dépensé en e-commerce en 2012 (hors services, abonnements récurrents, applications mobiles,....).</p>
                <p>Ne passez pas à coté de ce marché ! Ne ratez plus ces ventes !</p></div>
		</div>

        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text">
                        <h3>Mettre en vente mes produits maintenant !</h3>
                        <p>Comment maîtriser mes coûts d’acquisition client ?</p>
                    </div>
                    <div class="promo-button">
                        <a href="services_couts_dacquisition.php" class="button medium blue">en savoir plus</a>
                    </div>
                </div>                                   
            </div>
        </div>

	</section>










    <!-- content section end here -->
    
  <?php echo $vue->getFooter(); ?>   
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>