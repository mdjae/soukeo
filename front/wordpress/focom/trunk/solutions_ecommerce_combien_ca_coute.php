<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
    
    
       
     <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="solutions_ecommerce.php">Solutions e-commerce</a></li>
                    <li class="current-page"><a href="#">Combien ça coute ?</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Combien ça coute ?</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->
      
    <!-- content section start here -->
    <section id="content-wrapper">
           
        <div class="row">        	
            <div class="twelve column">
            	<h3 class="smallmargin-bottom">Vendez sur internet, sans engagement et sans surprise</h3>
                <p>Commencez à vendre en quelques minutes : choisissez votre offre et inscrivez-vous !</p>
                
                <ul class="pricing-grid three-up">                            
                    <li class="pricing-column">
                        <h6 class="pricing-title">E-commerçant</h6>
                        <div class="pricing-price">
                            <h1>Gratuit !<span class="permonth">&nbsp;</span></h1>
                            <p>Vous disposez déja d'un site de vente en ligne.</p> 
                        </div>                    
                        <ul class="pricing-content">
                            <li>Synchronisation avec les plateformes e-commerce : <strong>prestashop, magento,...</strong></li>
                            <li>Nombre de produits <strong>illimité</strong></li>                                 
                            <li>Accès au Backoffice avec plus de 50 fonctionnalités</li>
                            <li>Mode de livraison personnalisé</li>
                            <li>Tarifs bancaires et livraison négociés</li>
                            <li>Sécurisation des transactions</li></ul>                    
                        <div class="pricing-button">
                            <a href="inscription.php" class="button medium grey">Inscription</a>
                        </div>
                    </li>
                    <li class="pricing-column blue-plan featured-plan">
                        <h6 class="pricing-title">Commercant</h6>
                        <div class="pricing-price">
                            <h1>49.90<span class="dollar">€</span><span class="permonth">/mois</span></h1>
                            <p>Vendez vos produits sur Internet !</p> 
                        </div>                    
                        <ul class="pricing-content">
                            <li>Gestion de votre catalogue et de vos stocks chez Soukéo</li>
                            <li>Nombre de produits <strong>illimité</strong></li>                                 
                            <li>Accès au Backoffice avec plus de 50 fonctionnalités</li>
                            <li>Livraison par Soukéo</li>
                            <li>Tarifs bancaires et livraison négociés</li>
                            <li>Sécurisation des transactions</li></ul>                    
                        <div class="pricing-button">
                            <a href="inscription.php" class="button medium blue">Inscription</a>
                        </div>
                    </li>
                    <li class="pricing-column">
                        <h6 class="pricing-title">Premium</h6>
                        <div class="pricing-price">
                            <h1 style="font-size:35px; line-height:60px;">Nous consulter<span class="permonth">&nbsp;</span></h1>
                            <p>Votre force de vente sur Internet</p> 
                        </div>                    
                        <ul class="pricing-content">
                            <li>Valorisation des produits sur Soukéo</li>
                            <li>Campagne de génération de ventes à la performance</li>
                            <li>CRM et push-marketing ciblé</li>
                            <li>Opération promotionnelles exceptionnelles</li>
                            <li>Analyse comportementale des acheteurs et clients</li>
                        </ul>                    
                        <div class="pricing-button">
                            <a href="inscription.php" class="button medium grey">Inscription</a>
                        </div>
                    </li>                                    
                </ul>
                
                <hr/>             
            </div>
            
            <div class="twelve column">
            	<h4 class="smallmargin-bottom2">Des questions ?</h4>
            </div>
            
            <div class="six column mobile-two">
            	<i class="icon-ok-sign icon-margin"></i><strong>Achetez un an, payez 10 mois !</strong>
                <p>Engagez-vous chez Soukéo pour 12 mois et bénéficiez d'une remise. Cette option vous sera proposée lors du paiement en ligne.</p>
            </div>
            <div class="six column mobile-two">
            	<i class="icon-ok-sign icon-margin"></i><strong>Puis arreter mon abonnement quand je le désire?</strong>
                <p>L'offre de soukéo est sans durée d'engagement vous êtes libre de suspendre votre abonnement quand vous le souhaitez.</p>
            </div>
            
            <div class="twelve column">
            	<h4>Nous acceptons</h4>
                <div class="note">
                	<ul class="pricing-payment">
                        <li><img src="images/sample_images/payment/payment-visa.png" alt="" class="retina" /></li>
                        <li><img src="images/sample_images/payment/payment-mastercard.png" alt="" class="retina" /></li>
                        <li><img src="images/sample_images/payment/payment-amex.png" alt="" class="retina" /></li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>    
                 
    </section>





















    <!-- content section end here -->
    
   
 <?php echo $vue->getFooter(); ?>  
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>