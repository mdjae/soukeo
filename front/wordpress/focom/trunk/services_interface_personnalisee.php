<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
 
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="services.php">Services</a></li>                    
                    <li class="current-page"><a href="#">Interface personnalisé & supports</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Interface personnalisé & supports</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
            

	<div class="row">

        <h3>Soukeo.fr propose des services inédits et intuitifs que vous adopterez en quelques heures.</h3>
        <p class="lead">Extraits des fonctionnalités de votre interface de gestion :</p>
	</div>        


       <div class="row">        	

        <div class="row">    
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-businesscardalt"></i>
                </div>
                <h4>Gestion de votre compte client</h4>
                <ul class="text-overflow arrow">
                	<li>Mon compte</li>
                    <li>Options de l’abonnement</li>
                    <li>Animer ma boutique</li>
                    <li>Statistiques</li></ul></div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-barcode"></i>
                </div>
                <h4>Gestion des catalogues</h4>
                <ul class="text-overflow arrow">
                	<li>Synchronisation avec les plateformes e-commerce</li>
                    <li>Mise à jour du catalogue</li>
                    <li>Edition manuelle d’une fiche produit</li></ul></div>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-dropbox"></i>
                </div>
                <h4>Gestion des Stocks</h4>
                <ul class="text-overflow arrow">
                	<li>Edition manuelle des stocks</li>
                    <li>Alertes stocks</li></ul>
            </div>
            
            <hr class="margin-top2"/>
            
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-coupon"></i>
                </div>
                <h4>Gestion des promotions</h4>
                <ul class="text-overflow arrow">
                	<li>Opérations commerciales</li>
                    <li>Mise en valeur des produits</li></ul></div> 
                    
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-invoice"></i>
                </div>
                <h4>Gestions des commandes</h4>
                <ul class="text-overflow arrow">
                	<li>Détail et suivi d’une commande</li>
                    <li>Livraisons</li>
                    <li>Facturation et paiement en ligne</li>
                	<li>Suivi des litiges</li></ul></div> 
                    
                   
            <div class="four column mobile-two">
            	<div class="circle-border small float-left">
                	<i class="icon-server"></i>
                </div>
                <h4>Archives</h4>
                <ul class="text-overflow arrow">
                	<li>BDD clients</li>
                    <li>Suivi des paiements</li>
                    <li>Historiques des transactions</li></ul></div>                                                            
        </div>
        
        
        
        <p>Si toutefois, vous éprouvez des difficultés dans son utilisation, nos experts Soukeo.fr sont à votre disposition.</p>
        <p>Ils vous offriront un accompagnement personnalisé et vous proposeront une <a href="ressources_fomation_a_distance.php">formation à distance</a>, un <a href="ressources_formation_collective.php">atelier de groupe</a> ou éventuellement un entretien individuel.</p>
        <p class="lead">Vous avez des questions concernant les fonctionnalités techniques ? Consultez notre <a href="faq.php">FAQ</a> ou notre <a href="ressources_supports_technique.php">support technique</a>.</p>



        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text nine column">
                        <h3>Voir les catégories de produits disponibles</h3>
                        <p>Vérifiez que les produits que vous vendez sont supportés par notre plateforme.</p></div>
                    <div class="promo-button three column right">
                        <a href="services_categories_disponibles.php" class="button medium blue">voir les catégories disponibles</a></div>
                </div>                                   
            </div>
        </div>




	






	</section>











    <!-- content section end here -->
    
  <?php echo $vue->getFooter(); ?>   
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>