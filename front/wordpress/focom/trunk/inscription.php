<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->

    
    <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li class="current-page"><a href="inscription.php">Inscription</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Inscription commerçants</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->
      
    <!-- content section start here -->
    <section id="content-wrapper">

        <div class="row">
            <div class="column seven">
	            <h2>Pré-inscrivez vous sur Soukéo.fr</h2>                
                <p class="">Dès la réception de vos informations, un conseiller Soukéo traitera votre demande. </p>
                <p>Nous vous recontacterons dans les plus brefs délais pour finaliser votre inscription.</p></div>
            <div class="column five note-folded orange text-center">
            	<h4>A qui s'adresse Soukéo ?</h4>

                <ul class="arrow">
                	<li>Vous vendez des produits sur le territoire réunionnais.</li>
                    <li>Vos produits sont conformes à la réglementation.</li>
                    <li>Vous disposez d'un site e-commerce OU d'un fichier tableur, comprenant référence et prix de vente TTC de chaque produit.</li>
                    <li>Vous connaissez votre stock en temps réel, OU vous engagez à rembourser / surclasser en cas d'indisponibilté.</li></ul></div>
          
        <hr/>
                <div id="contact-form-area">
                    <!-- Contact Form Start //-->

                    <form action="#" id="contactform" class="contactformBig"> 
	                    <fieldset> 
		                        <label>&nbsp;</label>
                                <h3>Informations personnelles</h3>
                                <label for="civilite">Civilité</label>
                                <select name='civilite' id="civilite">
                                    <option value="mr">Mr</option>
                                    <option value="mme">Mme</option></select>
                                <label for='name'>Nom <em>*</em></label>                           
                                <input type="text" name="name" class="textfield" id="name" value="" /> 
                              
                                <label for='prenom'>Prénom<em>*</em></label>                           
                                <input type="text" name="prenom" class="textfield" id="prenom" value="" /> 
                              
                                <label>E-mail <em>*</em></label> 
                                <input type="email" name="email" class="textfield" id="email" value="" />                        
                              
               
                              
                              
                                <div class="clear"></div>
 		                        <label>&nbsp;</label>
                             
                             
                               
                             	<h3>Votre compte vendeur Soukéo</h3>
                             	
                             	<label>Nom de votre entreprise</label>
                                <input type="text" name="entreprise" class="textfield" id="entreprise" value=""/>
                             	
                             	<label>Adresse</label>
                                <input type="text" name="adresse" class="textfield" id="adresse" value=""/>
                             	
                             	<label>Code postal</label>
                                <input type="text" name="codepostal" class="textfield" id="codepostal" value=""/>                             	
                                
                                <label>Ville</label>
                                <input type="text" name="ville" class="textfield" id="ville" value=""/>                             	
                                
                                <label>Pays</label>
                                <select id="pays" name='pays'>
                                    <option value="reunion">Réunion</option>
                                    <option value="maurice">Maurice</option>
                                    <option value="madagascar">Madagascar</option>
                                    <option value="mayotte">Mayotte</option>
                                    <option value="metropole">France métropolitaine</option>
                                </select>
                                    
                             	<label>Téléphone principal</label>
                                <input type="text" name="telephone" class="textfield" id="telephone" value=""/>                             	
                               
                                <label>Numéro Siret</label>
                                <input type="text" name="siret" class="textfield" id="siret" value=""/>                             	
                              
                                <label>Site Internet</label>
                                <input type="text" name="siteweb" class="textfield" id="siteweb" value=""/>                                
                               
                                <label>Vous vendez déja en ligne ?</label>
                                
                                  <select id="ecommerce" name="ecommerce">
                                    <option value="oui">oui</option>
                                    <option value="non">non</option>
                                </select>
                               
                                <label>Si oui, avec quelle technologie ?</label>
                                <select id="ecommerce_techno"  name="ecommerce_techno">
                                    <option value="prestashop">Prestashop</option>
                                    <option value="magento">Magento</option>
                                    <option value="oscommerce">OScommerce</option>                                    
                                    <option value="thelia">Thelia</option>
									<option value="virtuemart">Joomla + Virtuemart</option>
                                    <option value="wordpress">Wordpress e-commerce</option>
                                    <option value="autre">Autre / je ne sais pas</option></select>                                 <label>Renseignements complémentaires</label>
                              
                                <textarea name="message" id="message" name='message' class="textarea" cols="2" rows="6">Indiquez ici ce que vous vendez, vos spécificités,...</textarea>
								
								<div class="clear"></div> 
                                <label>&nbsp;</label>
                                <button type="submit" name="submit" class="buttoncontact" id="buttonsend">Envoyer</button>
                                <span class="loading" style="display: none;">Patientez SVP...</span>
                                <div class="clear"></div>
	
                        </fieldset> 
                    </form>
                    <!-- Contact Form End //-->
                </div>
 
 

 
                
            </div>
            
            <hr/>
 
            
                 
        </div>
                         
    </section>
    <!-- content section end here -->
    

 <?php echo $vue->getFooter(); ?>   
<script src="js/inscription-form.js"></script>
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');
});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>