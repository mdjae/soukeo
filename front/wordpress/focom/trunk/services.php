<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
 
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li class="current-page"><a href="#">Services</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Services</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
            
    
       <div id="categories" class="row">        	
            <div class="six column mobile-two">
            	<h4>Comprendre le fonctionnement de soukeo.fr</h4>
                <i class="icon-settingsthree-gears"></i>
                <p>Vous avez des produits ? Soukéo se charge de les vendre sur la première place de marché de l'Océan Indien. Découvrez comment fonctionne Soukéo et vendez sur internet.</p>  
                <a href="services_fonctionnement.php" class="button small orange">en savoir plus</a>                                  
            </div>
            
            <div class="six column mobile-two">
            	<h4>Développez vos ventes</h4>
                <i class="icon-euroalt"></i>
                <p>Proposez vos produits à un marché de plus de 4 millions de clients potentiels. Tous les internautes de la Zone Océan Indien vous attendent !</p>
                <a href="services_developpez_vos_ventes.php" class="button small orange">en savoir plus</a>                
            </div>
            <hr />
            <div class="six column mobile-two">
            	<h4>Maîtrisez vos coûts d'acquistion client</h4>
                <i class="icon-affiliate"></i>
                <p>Une grille de commission claire et efficace, indexée sur la performance. Nous somme votre force de vente sur internet</p>
                <a href="services_couts_dacquisition.php" class="button small orange">en savoir plus</a>
            </div>
            
            <div class="six column mobile-two">
            	<h4>Interface personnalisé & intuitive</h4>
                <i class="icon-happy"></i>
                <p>Avec plus de 50 fontionnalités-clés, nous vous  accompagnons en douceur vers le e-commerce professionnel.</p>
                <a href="services_interface_personnalisee.php" class="button small orange">en savoir plus</a>                    
            </div> 
            <hr />
            <div class="six column mobile-two">
            	<h4>Catégories disponibles à la vente</h4>
                <i class="icon-stacks"></i>
                <p>Avec plus de 4 000 catégories disponibles nous sommes la plateforme de vente en ligne la plus généraliste de l'Océan Indien. Positionnez bien vos produits pour être sûr que vos clients vous trouverons.</p>
                <a href="services_categories_disponibles.php" class="button small orange">en savoir plus</a>                    
            </div> 
        </div>
                
    
    






	</section>
    <!-- content section end here -->
    

 <?php echo $vue->getFooter(); ?>   
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>