<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>

    <!-- header end here -->
    
    
    
          <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="nous_connaitre.php">Nous connaître</a></li>                    
                    <li class="current-page"><a href="#">Qui sommes-nous ?</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Qui sommes-nous ?</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
 	<div class="row">
    	<h3 class="column nine">Soukeo est une place de marché électronique pour l’Océan Indien, basée à l’Ile de la Réunion.</h3>
        <p class="nine column lead">Nous référençons les produits des commerçants et les proposons à la vente sur Internet.</p></div>            
 	<div class="row">
		<div class="seven column">
        	<p>Adresse de contact : 117, rue Monthyon - 97400 Saint-Denis de la Réunion</p>
            <p>Siège social : 91c, chemin de Montauban - 97490 Saint-Denis de la Réunion</p>
			<p>N° SIRET :  791 331 267 00017</p>
            <p>Code APE : 8299Z</p></div>

		<div class="note column five" style="float:right">
        	<h4>En savoir plus</h4>
			<p><a href="contact.php">Contactez-nous</a></p>
            <p><a href="documents/cgu_soukeo.pdf">Consultez nos conditions générales d’utilisation</a></p>
            <p><a href="documents/cgv_vendeur__soukeo.pdf">Consulter nos conditions générales de vente</a></p></div></div>

	<div class="row">
		<hr/></div>
 	<div class="row">
		<div class="seven column">
            <h4>Historique</h4>
			<ul class="arrow">
            	<li>avril 2012 - étude de marché, modèle économique, recherche de financement</li>
                <li>décembre 2012 - début de la conception technique et fonctionnelle du projet</li>
            	<li>avril 2013 - lancement de la production de la plateforme</li>
                <li>août 2013 - beta-test</li>
                <li>septembre 2013 - ouverture au public</li>
                <li>novembre 2013 : lancement de Soukeo.fr et ouverture officielle</li></ul>
	</div>   
    
    </section>























    <!-- content section end here -->
    
<?php echo $vue->getFooter(); ?>   

<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>