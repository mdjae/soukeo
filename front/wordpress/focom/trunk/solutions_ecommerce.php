<?php
require('inc/soukeo.inc.php');

$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
    
    
    
    
    
    
    
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li class="current-page"><a href="solutions_ecommerce.php">Solutions e-commerce</a></li>
               
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Services</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->

    <!-- content section starts here -->
    <section id="content-wrapper">     
            
    
       <div id="categories" class="row">        	
            <div class="six column mobile-two">
            	<h4>Création et gestion de catalogue produit</h4>
                <i class="icon-colocation"></i>
                <p>Saviez-vous qu'une bonne fiche produit c'est 25% de taux de conversion en plus ? En mutualisant nos données nous augmentons vos ventes, et nous pouvons vous aider à améliorer la rentabilité de votre site.
</p>  
                <a href="solutions_ecommerce_creation_et_gestion_catalogue.php" class="button small orange">en savoir plus</a>                                  
            </div>
            <div class="six column mobile-two">
            	<h4>Expédiez par Soukéo.fr</h4>
                <i class="icon-world"></i>
                <p>La logistique des colis est une problématique fondamentale pour chaque commerçant. Grace à Soukéo vous pourrez bénéficier des meilleurs négociations en terme de coûts de livraisons.</p>  
                <a href="solutions_ecommerce_expediez.php" class="button small orange">en savoir plus</a>                                  
            </div>
            <hr />     
            <div class="six column mobile-two">
            	<h4>Data & marketing</h4>
                <i class="icon-databaseadd"></i>
                <p>Les données sont un actif précieux de chaque entreprise. Soukéo vous offre une analyse fine sur vos clients et vos produits. Soukéo fourni également des données anonymisées concernant les catégorie de produits (le prix moyen, le nombre de vendeurs , quel public pour ces produits).</p>  
                <a href="solutions_ecommerce_data_et_marketing.php" class="button small orange">en savoir plus</a>                                  
            </div>

        </div>

	</section>
    <!-- content section end here -->


    <!-- content section end here -->
 
 <?php echo $vue->getFooter(); ?>   
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>