<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->

    
    <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li class="current-page"><a href="#">Contact</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Contactez-nous</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->
      
    <!-- content section start here -->
    <section id="content-wrapper">

        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text column seven">
                        <h3>Commerçants et e-commerçants</h3>
                        <p>Vous souhaitez nous contacter pour rejoindre Soukéo, ou pour avoir plus de précision ? Accédez au formulaire dédié.</p>
                    </div>
                    <div class="promo-button column five">
                        <a href="inscription.php" class="button orange medium">Inscription commerçants</a>
                    </div>
                </div>                                   
            </div>
        </div>
        
        <div class="row" id="ContactPartenaires">
        	<h1>Partenaires professionnels</h1>
        	<div class="five column">
            	<div id="map"></div>
                
                <address>
                  <strong>Soukéo SAS</strong><br>
                  <i class="icon-home"></i>117 rue Monthyon<br/>
                  <i class="icon-phonealt"></i>(+262) 262 51 59 17<br/>
                  <i class="icon-emailalt"></i>contact@soukeo.fr                 
                </address>                
            </div>
                   
            <div class="seven column">
            	<h4 class="smallmargin-bottom">Une question ?</h4>
                <p class="lead">N'hésitez pas à nous contactez !</p>
                <p>Remplissez le formulaire ci-dessous en indiquant au mieux vos besoins. Nous reviendrons vers vous sous les plus brefs délais.</p>                
                <hr class="margin-top2"/>
                
                <div id="contact-form-area">
                    <!-- Contact Form Start //-->
                    <form action="#" id="contactform"> 
                        <fieldset> 
                        <label>Nom <em>*</em></label>                           
                        <input type="text" name="name" class="textfield" id="name" value="" /> 
                        <label>E-mail <em>*</em></label> 
                        <input type="email" name="email" class="textfield" id="email" value="" />                        
                        <label>Sujet <em>*</em></label>
                        <input type="text" name="subject" class="textfield" id="subject" value="" />
                        <label>Message <em>*</em></label>
                        <textarea name="message" id="message" class="textarea" cols="2" rows="6"></textarea>
                        <div class="clear"></div> 
                        <label>&nbsp;</label>
                        <button type="submit" name="submit" class="buttoncontact" id="buttonsend">Envoyer</button>
                        <span class="loading" style="display: none;">Patientez SVP..</span>
                        <div class="clear"></div>
                        </fieldset> 
                    </form>
                    <!-- Contact Form End //-->
                </div>
                
            </div>
                 
        </div>
                         
    </section>
    <!-- content section end here -->

 <?php echo $vue->getFooter(); ?>   
<script src="js/contact-form.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/gmaps.js"></script>
<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Map Jquery
var map = new GMaps({
    el: '#map',
    lat: -20.886756,
    lng: 55.46187,
    zoom: 14,
    zoomControl : true,
    zoomControlOpt: {
        style : 'SMALL',
        position: 'TOP_LEFT'
    },
    panControl : false,
	streetViewControl : false,
	mapTypeControl: false,
	overviewMapControl: false 
  });

    map.addMarker({
      lat: -20.886756,
      lng: 55.46187,
      icon: "images/map-marker.png"
    });

  var styles = [
     {
        featureType: "road",
        elementType: "geometry",
        stylers: [
            { lightness: 100 },
            { visibility: "simplified" }
      ]
    }, {
        featureType: "road",
        elementType: "labels",
        stylers: [
            { visibility: "off" }
      ]
    }
  ];
        
  map.addStyle({
      styledMapName:"Styled Map",
      styles: styles,
      mapTypeId: "map_style"  
  });
  
  map.setStyle("map_style");
});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>