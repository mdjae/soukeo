<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>
    <!-- header end here -->
 
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="services.php">Services</a></li>                    
                    <li class="current-page"><a href="#">Comprendre le fonctionnement de soukeo.fr</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Comprendre le fonctionnement de soukeo.fr</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
                

       <div class="row margin-top2">
           	<h3 class="twelve column">Soukeo.fr est une place de marché électronique qui rassemble  acheteurs et commerçants.</h3>       
        	<div class="seven column">
                <p>Voici les étapes d’une collaboration type chez Soukeo.fr :</p>
                <ul class="arrow">
                    <li>Vous mettez en ligne votre stock de produit sur Soukeo.fr</li>
                    <li>Vos produits sont visibles sur Soukeo.fr, et de ce fait, accessible pour les milliers de clients potentiels transitant par notre plateforme.</li>
                    <li>Les clients achètent vos produits sur Soukeo.fr</li>
                    <li>Nous vous informons par email qu’une ou plusieurs ventes a été réalisé. Vous emballez et expédiez les produits commandés au client</li>
                    <li>Soukeo vous fait parvenir le paiement sur votre compte bancaire société tous les 15 et 31 du mois.</li></ul>          
            </div>
            <div class="five column">
            	<img src="images/soukeo.gif" style="margin:0 auto; width:400px text-align:center;">
            </div>
        </div>        
 
		<div class="row">
            <h4 class="twelve column">Qui peut adhérer ?</h4>
            <p class="nine column">Toute entreprise disposant d’un numéro du Registre du commerce et des sociétés (RCS), les auto-entrepreneurs et les structures pouvant facturer légalement en France.</p></div>


        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text">
                        <h3>Comment développer mes ventes grâce à Soukeo ?</h3>
                        <p>Mettre en vente mes produits maintenant !</p>
                    </div>
                    <div class="promo-button">
                        <a href="services_developpez_vos_ventes.php" class="button medium blue">en savoir plus</a>
                    </div>
                </div>                                   
            </div>
        </div>



	</section>














    <!-- content section end here -->
    
   <?php echo $vue->getFooter(); ?>   


<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>