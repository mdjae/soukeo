<?php

class svVue {

	public function getHeader() {
		$test = explode('/', $_SERVER['REQUEST_URI']);

		$item = (explode('.', end($test)));
		$item = $item[0];
//		var_dump($item);

		switch ($item) {

			case 'nous_connaitre' :
				$service = '';
				$connaitre = 'selected';
				$solution = '';
				$ressource = '';
				$contact = '';
				break;

			case 'services' :
				$service = 'selected';
				$connaitre = '';
				$solution = '';
				$ressource = '';
				$contact = '';
				break;

			case 'solutions_ecommerce' :
				$service = '';
				$connaitre = '';
				$solution = 'selected';
				$ressource = '';
				$contact = '';
				break;
			case 'ressources' :
				$service = '';
				$connaitre = '';
				$solution = '';
				$ressource = 'selected';
				$contact = '';
				break;
			case 'contact' :
				$service = '';
				$connaitre = '';
				$solution = '';
				$ressource = '';
				$contact = 'selected';
				break;

			default :
				break;
		}

		return '<!DOCTYPE html>
<!--[if IE 8 ]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="fr"> <!--<![endif]-->
<head>
<title>Soukeo, place de marché de l\'Océan Indien</title>
<meta charset="utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Soukéo, place de marché de l\'Océan Indien</title>
<link rel="apple-touch-icon-precomposed" href="apple-icon.png"/>
<link rel="apple-touch-icon-precomposed" href="apple-icon.png"/>
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/media.css"/>
<link rel="stylesheet" href="css/revolution.css" media="screen"/>
<link rel="stylesheet" href="css/media-slideshow.css" media="screen"/>
<link rel="stylesheet" href="css/noscript.css" media="screen,all" id="noscript"/>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400"/>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:400,300,200"/>
<link rel="shortcut icon" href="images/favicon.ico"/>
<!-- IE Fix for HTML5 Tags -->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->




<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/jquery.themepunch.plugins.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/superfish.js"></script>
<script src="js/mediaelement-and-player.min.js"></script>
<script src="js/jquery.flexisel.js"></script>
<script src="js/jquery.fancybox.js?v=2.0.6"></script>
<script src="js/jquery.fancybox-media.js?v=1.0.3"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/accordion-functions.js" ></script>
<script src="js/theme-functions.js"></script>
<script src="js/jquery.retina.js"></script>
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(["_setAccount", "UA-35610462-1"]);
_gaq.push(["_trackPageview"]);

(function() {
var ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true;
ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s);
})();

</script>

</head>
<body>
	<!-- header start here -->
	<header>
    	<!-- top info start here -->
    	<div id="top-info">
        	<div class="row">
            	<div class="twelve column">
                    <div class="phonemail-area">
                        <i class="icon-phonealt"></i><span>262 262 51 59 17</span>
                        <i class="icon-emailalt"></i><span><a href="mailto:contact@soukeo.fr">contact@soukeo.fr</a></span>
                    </div>
<!-- commenté car non utile pour le moment 
                    <div id="top-socials">
                        <ul class="socials-list">                                
                            <li><a href="#"><i class="social-twitter"></i></a></li>
                            <li><a href="#"><i class="social-facebook"></i></a></li>
                        </ul>
                    </div>
                    <div class="flag-area">
                    	<span>Changer de langue</span>
                        <a href="#"><img src="images/flags/fr.gif" alt=""/></a>
                        <a href="#"><img src="images/flags/mu.gif" alt=""/></a>
                    </div>-->
                </div>
            </div>
        </div>
<!-- top info end here -->      
        <div id="header-wrapper">
        	<div class="row">
            	<div class="column seven">
		            <a href="index.php"><img id="logo" src="images/logo_big.png" alt="main-logo" class="retina"/></a></div>

                	<div id="login">
                    	<a href="inscription.php" class="orange button medium">Inscrivez-vous</a>
<!-- commenté car non utile pour le moment 
                    	<a href="connexion.php" class="blue button medium">Connexion</a>
                        -->
                    </div>

            </div>
            
            <!-- navigation start here -->
            <div class="wrapper100" id="menu_container">
                <div class="row">            
                        <nav id="mainmenu">
                            <ul id="menu">
                                <li class="dropdown ' . $service . '"><a href="services.php">Services</a>
                                    <ul> 
                                        <li><a href="services_fonctionnement.php">Comprendre le fonctionnement de soukeo.fr</a></li>
                                        <li><a href="services_developpez_vos_ventes.php">Développez vos ventes</a></li>
                                        <li><a href="services_couts_dacquisition.php">Maîtrisez vos coûts d\'acquistion client</a></li>
                                        <li><a href="services_interface_personnalisee.php">Interface personnalisé & supports</a></li>
                                        <li><a href="services_categories_disponibles.php">Catégories disponibles à la vente sur soukeo.</a></li></ul></li>
                                <li class="dropdown ' . $connaitre . '"><a href="nous_connaitre.php">Mieux nous connaitre</a>
                                    <ul> 
                                        <li><a href="nous_connaitre_qui_sommes_nous.php">Qui sommes-nous ?</a></li>
                                        <li><a href="nous_connaitre_emplois_et_stages.php">Emplois et stages</a></li>
                                        <li><a href="nous_connaitre_communiques_de_presse.php">Communiqués de presse</a></li></ul>
            <li class="dropdown ' . $solution . '"><a href="solutions_ecommerce.php">Solutions e-commerce</a>
                      <ul>
                                        <li><a href="solutions_ecommerce_creation_et_gestion_catalogue.php">Création et gestion de catalogue produit</a></li>
                                        <li><a href="solutions_ecommerce_expediez.php">Expédiez avec Soukéo</a></li>
                                        <li><a href="solutions_ecommerce_data_et_marketing.php">Data & marketing</a></li>                                    <li><a href="solutions_ecommerce_combien_ca_coute.php">Combien ca coûte ?</a></li>         	
                      </ul>
                              </li>
                                <li class="dropdown ' . $ressource . '"><a href="ressources.php">Ressources</a>
                      <ul>
                                        <li><a href="ressources_faq.php">Foire aux questions</a></li>
                                        <li><a href="ressources_support_technique.php">Support technique</a></li>
                                        <li><a href="ressources_support_commercial.php">Support commercial</a></li>                                        	
                                        <li><a href="ressources_fomation_a_distance.php">Formation à distance</a></li>
                                        <li><a href="ressources_formation_collective.php">Formation collective</a></li>                                        	
                                  </ul>
                              </li>
                                <li class=" ' . $contact . '"><a href="contact.php">Contact</a></li>                                
                          </ul>
                        </nav>
                    </div>
                </div>                        
            
		</div>

<!-- navigation end here -->  
    </header>
            ';

	}

	public function getfooter() {

		return '    <footer>   
        <div class="row">
        	<div class="three column">
            	<div id="logo-footer"><a href="index.php"><img src="images/logo_footer.png" alt="main-logo" class="retina"/></a></div>
            	<p class="copyright">&copy; 2013 Soukeo - Ile de la Réunion - tous droits réservés</p>

<!-- commenté car non utile pour le moment 
                <ul class="social-list circle-social">
                	<li><a href="#"><i class="social-facebook"></i></a></li>
                    <li><a href="#"><i class="social-twitter"></i></a></li>
                    <li><a href="#"><i class="social-dribbble"></i></a></li>
                    <li><a href="#"><i class="social-linkedin"></i></a></li>
                </ul>-->
            </div>
            <div class="three column mobile-five">
            	<h4>Services</h4>
            	<ul class="no-bullet">
                    <li><a href="services_fonctionnement.php">Comprendre le fonctionnement de soukeo.fr</a></li>
                    <li><a href="services_developpez_vos_ventes.php">Développez vos ventes</a></li>
                    <li><a href="services_couts_dacquisition.php">Maîtrisez vos coûts d\'acquistion client</a></li>
                    <li><a href="services_interface_personnalisee.php">Interface personnalisé & supports</a></li>
                    <li><a href="services_categories_disponibles.php">Catégories disponibles à la vente sur soukeo.</a></li>
                </ul>
            </div>
            <div class="three column mobile-five">
            	<h4>Nous connaître</h4>
            	<ul class="no-bullet">
                    <li><a href="nous_connaitre_qui_sommes_nous.php">Qui sommes-nous ?</a></li>
                    <li><a href="nous_connaitre_emplois_et_stages.php">Emplois et stages</a></li>
                    <li><a href="nous_connaitre_communiques_de_presse.php">Communiqués de presse</a></li>
                </ul>
            </div>
            <div class="three column mobile-five">
            	<h4>Ressources</h4>
            	<ul class="no-bullet">
                    <li><a href="ressources_faq.php">Foire aux questions</a></li>
                    <li><a href="ressources_supports_technique.php">Support technique</a></li>
                    <li><a href="ressources_support_commercial.php">Support commercial</a></li>                                        	
                    <li><a href="ressources_fomation_a_distance.php">Formation à distance</a></li>
                    <li><a href="ressources_formation_collective.php">Formation collective</a></li>         
                </ul>
            </div>	           
        </div>
    </footer>     
	';

	}

}
