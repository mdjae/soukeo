<?php

/**
 * PDO SINGLETON CLASS
 *
 * @author tpo
 */
 
 
 

/**
 * Creates a PDO instance representing a connection to a database and makes the instance
 * available as a singleton
 *
 * @param string $dsn The full DSN, eg: mysql:host=localhost;dbname=testdb
 * @param string $username The user name for the DSN string. This parameter is optional for
 * some PDO drivers.
 * @param string $password The password for the DSN string. This parameter is optional for
 * some PDO drivers.
 * @param array $driver_options A key=>value array of driver-specific connection options
 *
 * @return PDO
 */

class DB extends PDO
{
    public static function &instance()
    {
        global $DATABASE_host, $DATABASE_user, $DATABASE_user_passwd, $DATABASE_name;
        static $instance = null;
		

        if ($instance === null)
        {
            try
            {
                $instance = new self("mysql:host=$DATABASE_host;dbname=$DATABASE_name", $DATABASE_user, $DATABASE_user_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        
			}
            catch(PDOException $e)
            {
             //   throw new DBException($e -> getMessage());
             echo $e -> getMessage();
            }
        }
        return $instance;
    }

}

class sdb
{

    static private $instance;
    //singleton instance
    public $db;

    public function __construct()
    {
        if (!self::$instance)
        {
            $this -> db = DB::instance();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    /**
     * Execute query and return all rows in assoc array
     *
     * @param string $statement
     * @return array
     */
    public function getAll($statement)
    {
        return ($res = $this -> db -> query($statement)) ? $res -> fetchAll(PDO::FETCH_ASSOC) : array();
    }

    /**
     * Execute query and return one row in assoc array
     *
     * @param string $statement
     * @return array
     */
    public function getRow($statement)
    {
        return ($res = $this -> db -> query($statement)) ? $res -> fetch(PDO::FETCH_ASSOC) : array();
    }

    /**
     * Execute query and return one value only
     *
     * @param string $statement
     * @return mixed
     */
    public function getOne($statement)
    {
        $row = ($res = $this -> db -> query($statement)) ? $res -> fetch() : null;
        return $row ? $row[0] : null;
    }

    /**
     * Initiates a transaction
     *
     * @return bool
     */
    public function beginTransaction()
    {
        return $this -> db -> beginTransaction();
    }

    /**
     * Commits a transaction
     *
     * @return bool
     */
    public function commit()
    {
        return $this -> db > commit();
    }

    /**
     * Rolls back a transaction
     *
     * @return bool
     */
    public function rollBack()
    {
        return $this -> db -> rollBack();
    }

    /**
     * Execute an SQL statement and return the number of affected rows
     *
     * @param string $statement
     */
    public function exec($statement)
    {
        return $this -> db -> exec($statement);
    }

    /**
     * Retrieve a database connection attribute
     *
     * @param int $attribute
     * @return mixed
     */
    public function getAttribute($attribute)
    {
        return $this -> db -> getAttribute($attribute);
    }

    /**
     * Return an array of available PDO drivers
     *
     * @return array
     */
    public function getAvailableDrivers()
    {
        return $this -> db > getAvailableDrivers();
    }

    /**
     * Returns the ID of the last inserted row or sequence value
     *
     * @param string $name Name of the sequence object from which the ID should be
     * returned.
     * @return string
     */
    public function lastInsertId($name='')
    {
    	if(!$name)
        	return $this -> db -> lastInsertId();
		else 
			return $this -> db -> lastInsertId($name);
    }

    /**
     * Prepares a statement for execution and returns a statement object
     *
     * @param string $statement A valid SQL statement for the target database server
     * @param array $driver_options Array of one or more key=>value pairs to set attribute
     * values for the PDOStatement obj
     returned
     * @return PDOStatement
     */
    public function prepare($statement, $driver_options = false)
    {
        if (!$driver_options)
            $driver_options = array();
        return $this -> db -> prepare($statement, $driver_options);
    }
    
    //public function execute ($statement, $data){
        
    //    return $statement -> execute ($data);
        
   // }
    
    public function getAllPrep($statement, $data){
        $sth = $this->prepare($statement); 
        $sth -> execute($data);
        return ($rs = $sth -> fetchall(PDO::FETCH_ASSOC)) ? $rs : array();
    }
    
    
    public function getRowPrep($statement, $data){
        $sth = $this->prepare($statement); 
        $sth -> execute($data);
        return ($rs = $sth -> fetch(PDO::FETCH_ASSOC)) ? $rs : array();
    }

    /**
     * Execute query and return one row in assoc array
     *
     * @param string $statement
     * @return array
     */
    public function queryFetchRowAssoc($statement)
    {
        return $this -> db -> query($statement) -> fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Execute query and select one column only
     *
     * @param string $statement
     * @return mixed
     */
    public function queryFetchColAssoc($statement)
    {
        return $this -> db -> query($statement) -> fetchColumn();
    }

    /**
     * Quotes a string for use in a query
     *
     * @param string $input
     * @param int $parameter_type
     * @return string
     */
    public function quote($input, $parameter_type = 0)
    {
        return $this -> db -> quote($input, $parameter_type);
    }

    /**
     * Set an attribute
     *
     * @param int $attribute
     * @param mixed $value
     * @return bool
     */
    public function setAttribute($attribute, $value)
    {
        return $this -> db -> setAttribute($attribute, $value);
    }

    /**
     * Fetch the SQLSTATE associated with the last operation on the database handle
     *
     * @return string
     */
    public function errorCode()
    {
        return $this -> db -> errorCode();
    }

    /**
     * Fetch extended error information associated with the last operation on the database
     * handle
     *
     * @return array
     */
    public function errorInfo()
    {
        return $this -> db -> errorInfo();
    }

}