<?php

class svModel extends sdb {

	public function setContact($nom, $email, $sujet, $message) {
		$sql = "INSERT INTO `soukeo_com`.`contact` (
				
					`NOM` ,
					`EMAIL` ,
					`SUJET` ,
					`MESSAGE` 
					)
					VALUES (
					 ?, ?, ?, ?
					)";

		$stmt = $this -> prepare($sql);

		$null = null;
		$stmt -> bindParam(1, $nom);
		$stmt -> bindParam(2, $email);
		$stmt -> bindParam(3, $sujet);
		$stmt -> bindParam(4, $message);
		$stmt ->execute();

	}
	
	public function setInscription ($nom, $email,$message,$civ,$prenom,$entreprise,$adresse,$codepostal,$ville, $pays, $tel,$siret,$siteweb,$ecom,$ecom_tec){

		$sql = "INSERT INTO `soukeo_com`.`inscription` (
				`CIV` ,
				`NOM` ,
				`PRENOM` ,
				`EMAIL` ,
				`ENTREPRISE` ,
				`ADRESSE` ,
				`CODEPOSTAL` ,
				`VILLE` ,
				`PAYS` ,
				`TEL` ,
				`SIRET` ,
				`SITEINTERNET` ,
				`VENTEENLIGNE` ,
				`TECHNOECOM` ,
				`RENSEIGNEMENT` 
				
				)
				VALUES (
				 ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
				)";

		$stmt = $this -> prepare($sql);

		$null = null;
		$stmt -> bindParam(1, $civ);
		$stmt -> bindParam(2, $nom);
		$stmt -> bindParam(3, $prenom);
		$stmt -> bindParam(4, $email);
		$stmt -> bindParam(5, $entreprise);
		$stmt -> bindParam(6, $adresse);
		$stmt -> bindParam(7, $codepostal);
		$stmt -> bindParam(8, $ville);
		$stmt -> bindParam(9, $pays);
		$stmt -> bindParam(10, $tel);
		$stmt -> bindParam(11, $siret);
		$stmt -> bindParam(12, $siteweb);
		$stmt -> bindParam(13, $ecom);
		$stmt -> bindParam(14, $ecom_tec);
		$stmt -> bindParam(15, $message);
		$stmt ->execute();
	}

}
