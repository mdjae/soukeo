<?php

require ("include/class.phpmailer.php");
function envoiMail($name, $subject, $email, $message) {

	$mail = new PHPMailer();

	$mail -> CharSet = "UTF-8";
	$mail -> From = $email;
	$mail -> FromName = $email;
	$mail -> AddAddress("jonathan@silicon-village.fr");
	$mail -> AddAddress("shanti@silicon-village.fr");
	$mail -> AddAddress("remi@silicon-village.fr");
	$mail -> AddAddress("thomas@silicon-village.fr");

	$corpsMessage .= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">";

	$corpsMessage .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
	$corpsMessage .= " <head></head><body>Bonjour, vous avez reçun nouvel email de contact provenant du formulaire du site Soukeo Commercant<br /><br />";

	$corpsMessage .= "<br /> Nom : " . $name . "<br />";
	$corpsMessage .= "<br /> Sujet : " . $subject . "<br />";
	$corpsMessage .= "<br /> Email : " . $email . "<br />";
	$corpsMessage .= "<br /> Message: " . $message . "<br />";
	$corpsMessage .= "</body> </html>";

	$mail -> Subject = "Soukeo : formulaire de contact";
	$mail -> Body = html_entity_decode($corpsMessage, ENT_QUOTES);

	$mail -> IsHTML(true);

	if (!$mail -> Send()) {
		$envoi = 3;
		//probleme lors de l envoi
		return trim(htmlentities($envoi));
	} else {
		$envoi = 1;
		//envoi validé
		return trim(htmlentities($envoi));
	}

}

function envoiMailInscription($name, $email, $message, $civ, $prenom, $entreprise, $adresse, $codepostal, $ville, $pays, $tel, $siret, $siteweb, $ecom, $ecom_tec) {
	$mail = new PHPMailer();

	$mail -> CharSet = "UTF-8";
	$mail -> From = $email;
	$mail -> FromName = $email;
	$mail -> AddAddress("jonathan@silicon-village.fr");
	$mail -> AddAddress("shanti@silicon-village.fr");
	$mail -> AddAddress("remi@silicon-village.fr");
	$mail -> AddAddress("thomas@silicon-village.fr");

	$corpsMessage .= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">";

	$corpsMessage .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
	$corpsMessage .= " <head></head><body>Bonjour, vous avez reçun nouvel email de contact provenant du formulaire d'inscription du site Soukeo Commercant<br /><br />";

	$corpsMessage .= "<br /> civilité : " . $civ . "<br />";
	$corpsMessage .= "<br /> Nom : " . $name . "<br />";
	$corpsMessage .= "<br /> prénom: " . $prenom . "<br />";
	$corpsMessage .= "<br /> email : " . $email . "<br />";
	$corpsMessage .= "<br /> message : " . $message . "<br />";

	$corpsMessage .= "<br /> entreprise: " . $entreprise . "<br />";
	$corpsMessage .= "<br /> adresse: " . $adresse . "<br />";
	$corpsMessage .= "<br /> code postal: " . $codepostal . "<br />";
	$corpsMessage .= "<br /> ville : " . $ville . "<br />";
	$corpsMessage .= "<br /> pays : " . $pays . "<br />";
	$corpsMessage .= "<br /> telephone : " . $tel . "<br />";
	$corpsMessage .= "<br /> siret: " . $siret . "<br />";
	$corpsMessage .= "<br /> site web : " . $siteweb . "<br />";
	$corpsMessage .= "<br /> déja vendu en ligne  : " . $ecom . "<br />";
	$corpsMessage .= "<br /> ecommercant technologie : " . $ecom_tec . "<br />";

	$corpsMessage .= "</body> </html>";

	$mail -> Subject = "Soukeo : formulaire d'inscription";
	$mail -> Body = html_entity_decode($corpsMessage, ENT_QUOTES);

	$mail -> IsHTML(true);

	if (!$mail -> Send()) {
		$envoi = 3;
		//probleme lors de l envoi
		return trim(htmlentities($envoi));
	} else {
		$envoi = 1;
		//envoi validé
		return trim(htmlentities($envoi));
	}

}
?>

