<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>

    <!-- header end here -->
    
    
    
    
          <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="nous_connaitre.php">Nous connaître</a></li>                    
                    <li class="current-page"><a href="#">Communiqués de presse</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Communiqués de presse</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
    	<div class="row">
        	<div class="twelve column">
            	<h2>Retrouvez ici nos supports de communication.</h2>
            	<p><strong>Aout 2013 : Aucun communiqué de presse pour le moment</strong></p>
                <p>Contactez notre équipe de presse à l’adresse <a href="mailto:presse@soukeo.fr">presse@soukeo.fr</a>.</p>
                <p>Pour les demandes non urgentes, vous pouvez nous laisser un message</p>
            </div>
            <div class="twelve column">
            	<p>Cette ligne n’est destinée qu’aux demandes de presse. Pour toute autre demande veuillez consultez nos options de <a href="contact.php">prise de contact</a>.</p></div>


 <!--        <div class="row">
        	<div class="twelve column">                    	
                <div class="promo-box">
                    <div class="promo-text">
                        <h3>Suivez-nous</h3>
                        <p>sur Twitter et Facebook</p>
                    </div>
                   <div class="promo-button">
                        <a href="#" class="button medium blue">Try it free today</a>
                    </div>-->
                </div>                                   
            </div>
        </div>
    
    </section>

    <!-- content section end here -->
 <?php echo $vue->getFooter(); ?>   

<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>