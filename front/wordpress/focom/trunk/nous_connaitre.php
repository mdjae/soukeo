<?php
require('inc/soukeo.inc.php');
$vue = new svVue();
echo $vue->getHeader();

?>

    <!-- header end here -->
    
    
    
      <!-- pagetitle start here -->
    <section id="pagetitle-container">
    	<div class="row">        	
            <div class="five column breadcrumb">
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li class="current-page"><a href="#">Nous connaître</a></li>
                </ul>
            </div>
            <div class="seven column pagetitle">
    			<h1>Nous connaître</h1>
        	</div>
        </div>	      
    </section>
    <!-- pagetitle end here -->


    <!-- content section starts here -->
    <section id="content-wrapper">     
            
    
       <div id="categories" class="row">        	
            <div class="six column mobile-two">
            	<h4>Qui sommes-nous ?</h4>
                <img src="images/logo_big.png" style="width:78px; float:left; margin-right:10px; " />
                <p>Soukéo est une jeune entreprise réunionnaise qui a l'ambition de faire grandir la filière du e-commerçe à la Réunion. <!--Si vous nous soutenez, suivez nous sur Facebook et Twitter.--></p>  
                <a href="nous_connaitre_qui_sommes_nous.php" class="button small orange">en savoir plus</a>                                  
            </div>
            
            <div class="six column mobile-two">
            	<h4>Emplois et stages</h4>
                <i class="icon-briefcase"></i>
                <p>Nous sommes à la recherche de talents, motivés et capables de s'adapter efficacement à nos contraintes. </p>
                <p>Si vous pensez correspondre à cette description envoyez nous un mail.</p>
                <a href="nous_connaitre_emplois_et_stages.php" class="button small orange">en savoir plus</a>                
            </div>
            <hr />                 
            <div class="six column mobile-two">
            	<h4>Communiqués de presse</h4>
                <i class="icon-inkpen"></i>
                <p>Retrouvez ici toutes les annonces et les dernieres informations concernant Soukéo.</p>
                <a href="nous_connaitre_communiques_de_presse.php" class="button small orange">en savoir plus</a>
            </div>
        </div>

	</section>
    <!-- content section end here -->    
    
    
    























    <!-- content section end here -->
    

 <?php echo $vue->getFooter(); ?>   

<script>
$(document).ready(function() {
//Retina Image
$('img.retina').retina('@2x');

//Slideshow
$('.banner').revolution({
delay:9000,
startwidth:1126,
startheight:450,
navigationType:"none",					// bullet, thumb, none
navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
navigationStyle:"navbar",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
navigationHAlign:"center",				// Vertical Align top,center,bottom
navigationVAlign:"bottom",				// Horizontal Align left,center,right
navigationHOffset:0,
navigationVOffset:0,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
touchenabled:"on",						// Enable Swipe Function : on/off
onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
fullWidth:"off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
})

});
</script>
<script>$('#noscript').remove();</script>
</body>
</html>