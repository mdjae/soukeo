<?php

// RECHERCHE FILTER CATEGORY
$catfilter = new FilterList("filtercat", ("Choix de catégorie :"), "all");
$sql = " SELECT DISTINCT CATEGORY FROM  sfk_produit_grossiste ORDER BY CATEGORY ASC";
$rs = dbQueryAll($sql);  
$catfilter->addElement('all','all');

foreach( $rs as $R){
    $catfilter->addElement($R['CATEGORY'],$R['CATEGORY'] );
}


// RECHERHCE PAR NOM DE PRODUIT
$nom  = new FilterText("nom", ("Nom du Produit")." :", "", false, 12);
 
// PRIX MIN ET MAX
$pricemin = new FilterText("pricemin", ("Prix Min : "), "", false, 6);
$pricemax = new FilterText("pricemax", ("Prix Max : "), "", false, 6);

// POIDS MIN ET MAX
$weightmin = new FilterText("weightmin", ("Poids Min :"), "", false, 6);
$weightmax = new FilterText("weightmax", ("Poids Max :"), "", false, 6);

// MINIMUM QUANTITY
$quantitymin = new FilterText("quantitymin", ("Quantité min dispo :"), "", false, 6);

// CHOIX DEALER LIST
$idGross = new FilterList("idgross", ("Grossiste :"),"1");
$sql = " SELECT GROSSISTE_ID, GROSSISTE_NOM FROM  sfk_grossiste ";
$rs = dbQueryAll($sql);  

foreach( $rs as $R){
    $idGross->addElement($R['GROSSISTE_ID'],$R['GROSSISTE_NOM'] );
}


// MINIMUM COEF
$coefmax = new FilterText("coefmax", ("Coef Max :"), "", false, 6);

// MINIMUM MARGE_NET
$margemin = new FilterText("margemin", ("Marge Min :"), "", false, 6);

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$added = new FilterList("added", ("Produit ajoutés :"),"1");
$added->addElement('1','ajouté');
$added->addElement('0','non-ajouté');
$added->addElement('all','all');

$selling = new FilterList("selling", ("Etat vente :"), "all");
$selling->addElement('1','déjà en vente');
$selling->addElement('0','non vendu');
$selling->addElement('all','all');

$filter = new Filter("venteAvahis");
$filter->addFilter($catfilter);
$filter->addFilter($nom);
$filter->addFilter($pricemin);
$filter->addFilter($pricemax);
$filter->addFilter($weightmin);
$filter->addFilter($weightmax);
$filter->addFilter($quantitymin);
$filter->addFilter($idGross);
$filter->addFilter($added);
$filter->addFilter($selling);
$filter->addFilter($coefmax);
$filter->addFilter($margemin);

$filter->setGridToReload("venteavahis");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

		  array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "save",
                "name" =>("Sauvegarder toutes les modifications (Pour la page)"),
                "url" => "javascript: saveAllModifsVenteAv ();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "cart",
                "name" =>("Vendre produits sélectionnés "),
                "url" => "javascript: sellSelectionVenteAv();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "denied",
                "name" =>("Ne plus vendre produits sélectionnés "),
                "url" => "javascript: cancelSellSelectionVenteAv();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("","iconCircleCheck","check",("Article en vente")),
				array("","iconCircleWarn","denied",("Article pas en vente")), 				
				array("qualification.venteavahis..","","search",("Afficher les information pour ce produit"))
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>