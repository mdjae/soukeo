<?php

// RECHERHCE PAR num commande
$numCommande  = new FilterText("numfilter", ("N° de commande")." :", "", false, 12);

// RECHERCHE FILTER CATEGORY
$statusfilter = new FilterList("statusfilter", ("Choix du statut :"), "all");
$sql = " SELECT DISTINCT order_status FROM  skf_orders_grossiste ORDER BY order_status ASC";
$rs = dbQueryAll($sql);  
$statusfilter->addElement('all','all');

foreach( $rs as $R){
    $statusfilter->addElement($R['order_status'],$R['order_status'] );
}


// RECHERHCE PAR NOM DE PRODUIT
$numCommande  = new FilterText("numfilter", ("N° de commande")." :", $_GET['id'], false, 12);
 
//Litiges
$litige = new FilterList("litigefilter", ("Commande en litige :"),"all");
$litige->addElement('1','litige');
$litige->addElement('0','pas litige');
$litige->addElement('all','all');


$filter = new Filter("commande_grossiste");
$filter->addFilter($numCommande);
$filter->addFilter($statusfilter);
$filter->addFilter($litige);

$filter->setGridToReload("commande_grossiste");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "grossistes.commande_avahis..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "grossistes.commande_avahis..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "grossistes.commande_avahis..",
                "picto" => "edit",
                "name" =>("Annonce de réception"),
                "url" => "javascript: popupNumAnnonceRecep();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("grossistes.commande_grossiste..","","denied",("Déclarer un litige")),
				array("grossistes.commande_grossiste..","","check",("Annuler un litige")), 	
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>