<?php


foreach( $rs as $R){
    $fSoft->addElement($R['SOFTWARE'],$R['SOFTWARE'] );
}
$fActive = new FilterList("fActive", ("Auto association :"),"all");
$fActive->addElement('all','all');
$fActive->addElement('1','actif');
$fActive->addElement('0','inactif');

$filter = new Filter("categoriesgrossiste");
$filter->addFilter($fActive);
SystemParams::getParam('system*select_ecom_delete') =='N' ? : $filter->addFilter($fDelete);

$filter->setGridToReload("categoriesgrossiste");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "qualification.categoriesgrossiste..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "qualification.categoriesgrossiste..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
           array
           (
          		"access" => "qualification.categoriesgrossiste..",
                "picto" => "add",
                "name" =>("Ajout auto-association sur catégorie"),
                "url" => "javascript: autoAssocActif(".$_GET['id'].")",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            ),
           array
           (
          		"access" => "qualification.ecommercant..",
                "picto" => "denied",
                "name" =>("Supression auto-association sur catégorie"),
                "url" => "javascript: autoAssocInactif(".$_GET['id'].")",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("","iconCircleCheck","check",("Auto-assoc actif")),
				array("","iconCircleWarn","denied",("Auto-assoc inactif")), 				
				array("qualification.ecommercant..","","save",("Sauvegarde modifications de poids")),
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
