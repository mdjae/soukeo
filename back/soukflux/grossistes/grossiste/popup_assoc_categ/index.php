<?php
$class = "BusinessSoukeoGrossisteModel";
$db = new $class();

$view = new BusinessView();
$skf = new BusinessSoukeoModel();
$data = $skf->getSsCatByParentId();

$categ = str_replace("-", " > ", $_POST['cat_ecom']);
$categ = str_replace("+", " ", $categ);

$listeProd = $db -> getAllUnassocProducts($categ, $_SESSION['gr']['vendor']);
//On prend le premier produit pour exemple au hasard
$prod=$listeProd[0];
$_SESSION['gr']['prodToAdd'] = $prod;
//Si la catégorie a déja été associée
if(isset($_POST['id_cat_assoc']) && !empty($_POST['id_cat_assoc']) && $_POST['id_cat_assoc'] !='false'){
	$categ_av_assoc = $skf -> getStrCateg($_POST['id_cat_assoc']);
	$categ_av_assoc = substr($categ_av_assoc, 0, strlen($categ_av_assoc)-2);
}


?>

<h1 id="">Association de catégorie</h1>
<span id="idV" style="visibility:hidden"><?php echo $_POST['vendor_id'] ?></span> 
<p class="alert">Associer : <span class="label label-info" id="catEcom"><?php echo $categ ?></span></p>
<p id="success" class="alert">Avec : </p>
<?php if(isset($categ_av_assoc) && !empty($categ_av_assoc)){ ?>
	<p id="oldAssoc" class="alert alert-info"><em>Déja associé à :</em> <span class="label label-info"><?php echo $categ_av_assoc ?></span></p>
<?php } ?>
<br />
<div class="treeviewcheck">
	<input type="submit" class="btn btn-primary" name="submit" value="VALIDER" id="submit" onclick="validCateg('cat');"/>
	
		<input type="hidden" name="optionvalue" id="selectbox-popup" class="input-xlarge" data-placeholder="Choisissez une catégorie" onchange="validSelect2Popup()"/>
		<div class="top-bar" style="margin-top: 15px;"><h3>Catégories</h3></div>
		<div class="well">
			<?php echo $view -> showTreeCheckbox($data, true, 0) ;?>
		</div>
	<input type="submit" class="btn btn-primary" name="submit" value="VALIDER" id="submit" onclick="validCateg('cat');"/>
</div>

<script type="text/javascript" charset="utf-8">
    
        $('#checkboxtree').checkboxTree({
            onCheck: {
                ancestors: 'check',
                descendants: 'uncheck',
                others: 'uncheck'
            },
            onUncheck: {
                descendants: 'uncheck'
            }
        });
        
	$("#selectbox-popup").select2({
		minimumInputLength: 3,
		ajax: {
			url: "/app.php/getlistcateg",
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
	});	
</script>
