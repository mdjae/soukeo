<?php 
$root_path = "../../../";
$top_body = ('Fiche Produit');


$class = "BusinessSoukeoGrossisteModel";

$db = new $class();

$skf = new BusinessSoukeoModel();

if(!empty($_POST['tech'])){
	$prod = $db -> getProduct($_POST['vendor_id'], $_POST['prod_id']);
	$prod = $prod[0];	
}else{
	$prod = $db -> getProductAlias($_POST['prod_id']);
	$prod['CATEGORY'] = $db -> getStrCateg($prod['CATEGORY']);
	$prod['CATEGORY'] = substr($prod['CATEGORY'], 0, strlen($prod['CATEGORY'])-2); 
}
?>
<h1><?php echo $prod['NAME_PRODUCT'] ?></h1>

<?php  
	if ($skf -> productIsAssocGr($_POST['vendor_id'], $_POST['prod_id'])){
		$prodAv = $skf -> getProdAvAssocGr($_POST['vendor_id'], $_POST['prod_id']);
		echo "<span class='badge'>Associé à :</span";
		echo "<h3>".$prodAv['NAME_PRODUCT']."</h3><b> de ".$skf->getStrCateg($prodAv['CATEGORY'])."</b>";
	}

?>
<div>
<a href = "<?php echo $prod['IMAGE_PRODUCT'] ?>" title="MYTITLE" ><img class="imgPopup thumbnail" src="<?php echo $prod['IMAGE_PRODUCT']?>" title="IMAGE TITLE" /></a>
<p>
            <label class="form_label"><b>Description : </b></label>
            <span><?php echo $prod['DESCRIPTION'] ?></span>
</p>
</div>

	<h5 class="sous_title">CHAMPS FIXES</h5>
<p>
	<label class="form_label"><b>ID : </b></label>
	<span><?php echo $prod['ID_PRODUCT'] ?></span>
</p>
        <p>
            <label class="form_label"><b>Référence : </b></label>
            <span ><?php echo $prod['REF_GROSSISTE'] ?></span>
        </p>

        <p>
            <label class="form_label"><b>Marque : </b></label>
            <span ><?php echo $prod['MANUFACTURER'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>Catégorie : </b></label>
            <span><?php echo $prod['CATEGORY'] ?></span>
        </p>
        
        <p>
            <label class="form_label"><b>Petite description : </b></label>
            <span ><?php echo $prod['DESCRIPTION_SHORT'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>Prix : </b></label>
            <span ><?php echo $prod['PRICE_PRODUCT'], " €"?></span>
        </p>
        <p>
            <label class="form_label"><b>Quantité : </b></label>
            <span ><?php echo $prod['QUANTITY'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>Poids : </b></label>
            <span ><?php echo $prod['WEIGHT'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>EAN : </b></label>
            <span ><?php echo $prod['EAN'] ?></span>
        </p>
      
     
        <p>
            <label class="form_label"><b>DATE_UPDATE : </b></label>
            <span ><?php echo $prod['DATE_UPDATE'] ?></span>
            
        </p>
        <p>
            <label class="form_label"><b>DATE_CREATE : </b></label>
            <span ><?php echo $prod['DATE_CREATE'] ?></span>
            
        </p>
        
        	<h5 class="sous_title">CHAMPS DYNAMIQUES</h5>
       
<?php
if(!empty($_POST['tech']) && $_POST['tech']!= "Carshop"){
	$attr = $db->getAllAttrFromProduct($prod['ID_PRODUCT'], $prod['GROSSISTE_ID']);

		foreach ($attr as $attribut) {?>
			<p>
				<label><b><?php echo $attribut['LABEL_ATTR'] ?> : </b></label>
				<span><?php echo $attribut['VALUE'] ?></span>
			</p>
				
<?php 	}
}elseif(empty($_POST['tech'])){
	$attr = $skf->getAllAttrFromProduct($prod['ID_PRODUCT'], $prod['VENDOR_ID']);
	
		foreach ($attr as $attribut) {?>
			<p>
				<label><b><?php echo $attribut['LABEL_ATTR'] ?> : </b></label>
				<span><?php echo $attribut['VALUE'] ?></span>
			</p>
				
<?php 	}
}
else{ ?>
	 <p>
            <label class="form_label"><b>CAR MODEL : </b></label>
            <span ><?php echo $prod['CAR_MODEL'] ?></span>
            
        </p>
        <p>
            <label class="form_label"><b>COULEUR : </b></label>
            <span ><?php echo $prod['COLOR'] ?></span>
            
        </p>
<?php }
//require $root_path . "inc/form.footer.inc.php";

?>
