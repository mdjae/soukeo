﻿<?php

$nom   = new FilterText("nom", ("Nom du Grossiste")." :", "", false, 12);

$fActive = new FilterList("fActive", ("ETAT \"Active\" :"),"1");
$fActive->addElement('all','all');
$fActive->addElement('1','actif');
$fActive->addElement('0','inactif');


$filter = new Filter("grossiste");
$filter->addFilter($nom);
$filter->addFilter($fActive);
$filter->setGridToReload("grossiste");

drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "grossistes.grossiste..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "grossistes.grossiste..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
           array
           (
          		"access" => "grossistes.grossiste..",
                "picto" => "add",
                "name" =>("Passe en le(s) grossiste(s) en actif"),
                "url" => "javascript: setActifGrossiste()",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            ),
           array
           (
          		"access" => "grossistes.grossiste..",
                "picto" => "denied",
                "name" =>("Passe le(s) grossiste(s) en inactif"),
                "url" => "javascript: setInactifGrossiste()",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("","iconCircleCheck","check",("Vendeur actif")),
				array("","iconCircleWarn","denied",("Vendeur inactif")), 				
				array("qualification.ecommercant..","","edit",("Qualification des fiches produits")),
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
