<?php

// RECHERHCE PAR NOM DE PRODUIT
$numAr  = new FilterText("numfilter", ("N° de réception")." :", $_GET['id'], false, 12);

//Date
$date = new FilterList("datefilter", ("Date :"),"all");
$date->addElement('2','Récent');
$date->addElement('3','3 derniers jours');
$date->addElement('7','7 derniers jours');
$date->addElement('all','Depuis le début');


$filter = new Filter("annonce_reception");
$filter->addFilter($numAr);
$filter->addFilter($date);

$filter->setGridToReload("annonce_reception");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "grossistes.annonce_reception..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "grossistes.annonce_reception..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(	
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
