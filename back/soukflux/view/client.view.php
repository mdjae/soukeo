<div id="content-client">
    <?php echo $this->_['clientInfo'] ?>
</div>
    
<script type="text/javascript" charset="utf-8">
               $('table.paginated').each(function() {
                var currentPage = 0;
                var numPerPage = 5;
                var $table = $(this);
                $table.bind('repaginate', function() {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager_wrapper = $('<div class="pagination pagination-centered"></div>');
                var $pager = $('<ul></ul>');
                for (var page = 0; page < numPages; page++) {
                    $('<li class="page-number" ></li>').html('<a href="#" onClick="return false;">'+(page + 1)+'</a>').bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                };
                $pager_wrapper.html($pager);
                $pager_wrapper.insertBefore($table).find('li.page-number:first').addClass('active');
            });
</script>