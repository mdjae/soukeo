﻿    <div id="menuRight" >
        <div class="menuRight">
            <h1><?php echo _("Mode d'emploi") ?></h1>  
            <p><?php echo _("Pour vous assister dans la gestion de votre camping et de vos produits, consultez notre mode d'emploi : ")?>
                
             <?php
             if (is_file($_SERVER['DOCUMENT_ROOT']."/document/Mode-emploi-my-campsite-".strtoupper(SystemLang::getUrlCampLang()).".pdf")){
                 $urlfile = "/document/Mode-emploi-my-campsite-".strtoupper(SystemLang::getUrlCampLang()).".pdf";
                
             }else 
                 $urlfile = "/document/Mode-emploi-my-campsite-FR.pdf";
             ?>
            <a target='_blank' href="<?php echo $urlfile ?>"><?php echo _("Mode d'emploi")?></a></p>
        </div>
        <div class="menuRight">
            <?php echo $this->_['bloc1']?>
        </div>          

       <?php if ( $this->_['PAYSGEO'] == 'FRANCE') {?>
        <div class="menuRight">
            <h1><?php echo _("Professionnels de l'HPA")?></h1>  
                <img src="/skins/mpf/ot.gif"  alt="L&aps;OT" />
                    <p>
                    <?php echo _("Gestionnaires de terrains et professionnels de l'Hôtellerie de Plein-Air, consultez :")?>
                    </p><p>
                    <a target='_blank'  href=" http://www.ot-campings.com/"><?php echo _("L'officiel des terrains de camping")?></a>
        </div>
        <div class="menuRight">
            <h1><?php echo _("Votre partenariat avec la FFCC")?> </h1>
            <img src="/skins/mpf/FCCC.gif"  alt="FCCC" />
            <p><a target='_blank' href=" http://logi242.xiti.com/go.click?xts=510066&s2=1&p=Liens_FFCC&clic=S&type=click&url=http://ffcc.fr/71/html/partenaires-professionnels/gestionnaires-de-campings.aspx"><?php echo _("DEVENEZ PARTENAIRE FFCC 2013")?></a></p>
        </div>
        <?php } ?>
        
        <div class="menuRight">
            <h1><?php echo _("Contacts")?></h1>  
            <p><?php echo _("Pour nous contacter à propos de :")?></p>
            <ul>
                <li><?php echo _("Vos informations camping : ")?><a href="mailto:annie.rheinbold@motorpresse.fr"><?php echo _("Service Guides")?></a></li>
                <li><?php echo _("Vos bons plans : ")?><a href="mailto:leslie.manzone@motorpresse.fr"><?php echo _("Gwenola Pazem")?></a></li>
            </ul>
        </div>
    </div>