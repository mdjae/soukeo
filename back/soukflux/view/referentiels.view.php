<div class="container" id="content">
    <div class="row-fluid">
        <h3>Référentiel de données :</h3>
    </div>
    <div class="well row-fluid">
        <div class="tabbable tabs"> 
              <ul class="nav nav-tabs">
                <li class="active"><a href="#credits_tab" data-toggle="tab">Packs de crédits</a></li>
                <li><a href="#Abonnement_tab" data-toggle="tab">Abonnements</a></li>
                <li><a href="#PaymentType_tab" data-toggle="tab">Types de paiement</a></li>
                <li><a href="#CustomerStatut_tab" data-toggle="tab">Status Acheteurs</a></li>
                <li><a href="#VendorStatut_tab" data-toggle="tab">Status Vendeurs</a></li>
                <li><a href="#RelanceType_tab" data-toggle="tab">Types de relance</a></li>
                <li><a href="#TicketType_tab" data-toggle="tab">Types de tickets</a></li>
                <li><a href="#TicketQuestionType_tab" data-toggle="tab">Types de question tickets</a></li>
              </ul>
              <div class="tab-content">

                <div class="tab-pane active" id="credits_tab">
                    
                    <div id="messagebox_creditspacks"></div>
                    <div id="ref_creditspack">
                        <?php 
                        if(!empty($this -> _['packList'])){
                            echo $this -> showCreditsPackTable($this -> _['packList']); 
                        } ?>                        
                    </div>
                    
                    <div class="accordion" id="collapse_add_creditpack">
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse_add_creditpack" href="#collapseOne">
                            <button class="btn btn-primary">+ Ajouter un pack</button>
                          </a>
                        </div>
                        
                        <div id="collapseOne" class="accordion-body collapse">
                          <div class="accordion-inner">
                            <form class="form-horizontal" id="add_credit_pack" name="add_credit_pack">
                                <strong id="form_errors_creditspacks" style="color:red;"></strong>
                                <div class="control-group">          
                                    <label class="control-label" for="input_label_pack">Libellé du pack :</label>
                                        <div class="controls">
                                            <input type="text" id="input_label_pack" placeholder="Nom du pack" name="input_label_pack" required>
                                        </div>
                                </div>
                                
                                <div class="control-group">          
                                    <label class="control-label" for="input_value_pack">Valeur en crédit :</label>
                                        <div class="controls">
                                            <input type="text" id="input_value_pack" placeholder="Valeur + ou - en crédits" name="input_value_pack" required>
                                        </div>
                                </div>
                                
                                <div class="control-group">          
                                    <label class="control-label" for="input_price_pack">Valeur d'achat :</label>
                                        <div class="controls">
                                            <div class="input-append">
                                                <input type="text" id="input_price_pack" placeholder="Valeur en euros" name="input_price_pack" required>
                                                <span class="add-on">€</span>
                                            </div>
                                        </div>
                                </div>
                                <button class="btn btn-primary" onClick="validAddCreditPack();return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Enregistrer</button>
                                <button type="reset" class="btn btn-inverse">Reset</button> 
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                  
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("Abonnement", "Abonnements", "Classique / Premium ...") ?>
                  
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("PaymentType", "Type de paiement", "CB / Chèque / Virement...") ?>
                
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("CustomerStatut", "Statut acheteur", "Inscrit / Prospec...") ?>
                
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("VendorStatut", "Statut Vendeur", "Inscrit / Prospec...") ?>
                
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("RelanceType", "Type de relance", "Tel / Mail...") ?>
                
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("TicketType", "Type de tickets", "Technique / Facture / Crédits...") ?>
                
                <?php echo BusinessReferentielUtils::getTabPaneReferentiel("TicketQuestionType", "Type de question client ", "Litige / Facture / Produit...") ?>
                
                               
              </div>
            </div>
    </div>
</div>