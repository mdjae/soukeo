<div id="content" class="content3">
    
    <div class="row-fluid well">
        <h3>Tableau de bord </h3>
        <h5>Bonjour <?php echo ucfirst($this -> _['user'] -> getFirstname()); ?>, bienvenue dans la section Gestion des commandes Avahis et grossistes ♥</h5>
    </div>
<?php 
        if($this -> _['ticketList']){ ?>
            <div class="row-fluid well">
                <div class="span10">
                    <h4>Mes demandes assignées : <button class='pull-right btn btn-primary' value='Créer' onClick='showDialogAddTicket();';>Créer ticket</button></h4>
                        <div id ='ticket_list'>
                    <?php 
                        echo $this -> getTicketsListTable($this -> _['ticketList'], null, $this -> _['user'] -> getUserId()); ?>
                        </div>
                </div>
            </div>
<?php   }
 ?>
    <div class='row-fluid'>
        <div class="span8 well">
            <h4>Commandes à traiter :</h4><hr/>
            <table class='table table-bordered'>
                <caption></caption>
                <thead>
                    <tr>
                        <th></th>
                        <th>N°Commande</th>
                        <th>Date</th>
                        <th>Client</th>
                        <th>Total</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
            <?php 
                foreach ($this ->_['poList'] as $po) {
                    
                    $order = new BusinessOrder($po -> getOrder_id());
                    $address = new BusinessAddress($order -> getShipping_address_id());
                    
                    
                    if($address -> getCountry() !=  "RE"){
                        $info = '<span id="addr_'.$po -> getOrder_id().'_etat" class="etat_order label label-important"> '.countryName($address -> getCountry()).' </span>';
                    }else{
                        $info = '<span id="addr_'.$po -> getOrder_id().'_etat" class="etat_order label label-important"></span>';
                    }
                    
                    if($order -> getOrder_customer_id() == "0"){
                        $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order -> getRecipient_name()) .'</em>';
                    }else{
                        $customer = new BusinessCustomer($po -> getCustomer_id());
                        $nomCli = '<a href="/app.php/clients/listing?id='.$customer -> getCustomer_id().'" target="_blank">'.
                                    $customer -> getName().'</a>';
                    } ?>
                    <tr>
                        <td><input type="button" class="btn closed" value="+" id="po_<?php echo $po->getPo_id(); ?>_<?php echo $po->getOrder_id(); ?>" onclick="showOrderAvahisInfo(this)"></td>
                        <td><?php echo $po -> getIncrement_id(); ?></td>
                        <td><?php echo smartDateNoTime($po -> getDate_created()); ?></td>
                        <td><?php echo $nomCli; ?></td>
                        <td><?php echo numberByLang($po -> getBase_total_value() + $po -> getBase_shipping_amount() + $po -> getBase_tax_amount()) ; ?> €</td>
                        <td><?php echo getLabelStatus($order->getOrder_status()) . " ". $info; ?></td>
                    </tr>
                <?php };
             ?>
                </tbody>
           </table >
        </div> <!-- span8 well -->
        
        <div class="span4 well">
            <h4>Produits grossistes à qualifier :</h4><hr/>
            <table class='table'>
                <thead>
                    <tr>
                        <th>Grossiste</th>
                        <th>Produits</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
            <?php foreach($this -> _['countProdGr'] as $data ){ ?>
                <tr>
                <td><?php echo $data['GROSSISTE_NOM'] ?> </td>
                <td><span style="color:red;"><?php echo $data['c'] ?></span> restant(s) </td>
                <td><a href="/app.php/grossistes/grossiste/assocdealer?id=<?php echo $data['GROSSISTE_ID'] ?>" target="_blank">
                        <button class = "btn btn-primary">Qualification</button>
                    </a>
                </td> 
                </tr>
<?php             } ?>
            </table>
        </div> <!-- span4 well -->
    </div> <!-- row-fluid -->
    
    <div class='row-fluid'>
        <div class="span8 well">
            <h4>Commandes grossistes non réceptionnées :</h4><hr/>
            <table class='table table-bordered'>
                <caption></caption>
                <thead>
                    <tr>
                        <th></th>
                        <th>N°Commande</th>
                        <th>Date</th>
                        <th>Total estimé</th>
                        <th>Poids estimé</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
            <?php foreach($this -> _['orderGrList'] as $orderGr){  ?>
                <tr>
                    <td><input type='button' class='closed btn' value='+' id='itemgr_<?php echo $orderGr->getOrder_id_grossiste() ?>_24'  onclick='showChild(this)' /></td>
                    <td><?php echo $orderGr -> getOrder_external_code(); ?></td>
                    <td><?php echo smartDateNoTime($orderGr -> getDate_created()); ?></td>
                    <td><?php echo numberByLang($orderGr -> getEstimated_price()); ?> €</td>
                    <td><?php echo numberByLang($orderGr -> getEstimated_weight()); ?></td>
                    <td><?php echo getDropDownButton($orderGr->getOrder_status(), $orderGr->getOrder_id_grossiste()); ?></td>
                </tr>
            <?php } ?>
               </tbody>
           </table>
        </div> <!-- span8 well -->
        
        <div class="span4 well">
            <h4>Produits à commander en délai/rupture :</h4><hr/>
            
            <?php foreach($this -> _['itemListToOrder'] as $item){  
                    $prod_gr = $item -> getProductGrossiste();
                    if($prod_gr->getQuantity() == "0" || $prod_gr->getActive() == "0"){
                        if($prod_gr->getQuantity() == "0"){
                            $stock= "<span class='etat_order label label-warning'><b style='color:black';> Délai ! <b></span>";
                        }elseif($prod_gr->getActive() == "0"){
                            $stock= "<span class='etat_order label label-important'> Rupture ! </span>";
                        }
            ?>
                    <p>
                    <table class="table">
                        <tr><td>Commande <a href="/app.php/avahis/commande_avahis?id=<?php echo $item->getIncrement_id() ?>" target="_blank"><?php echo $item->getIncrement_id() ?></a>  le <?php echo smartDateNoTime($item -> getDate_created()); ?></td></tr>
                        <tr>
                            <td><?php echo truncateStr($item->getName(), 40) ?></td>
                            <td><?php echo $prod_gr->getGrossiste_nom(); ?></td>
                            <td><a href='<?php echo $prod_gr->getUrl() ?>' target='_blank'><b><?php echo $prod_gr->getRef_grossiste() ?></b></a></td>
                            <td><?php echo $stock ?></td>
                        </tr>
                    </table>
                    </p>
            <?php   }
                  } ?>
        </div> <!-- span4 well -->
    </div> <!-- row-fluid -->
</div> <!-- .content3 -->