<?php 
    $userManager = new ManagerAppUser(); 
    $this -> _['ticket']->getTicket_visibility() ? $ticket_visible = "<span class='label label-important'>Visible client </span>" : $ticket_visible = "";
?>
<div class="container well" id="ticket_container">
    <div class="row-fluid">
        <a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/app.php/dashboard" >Retour Tableau de bord</a>
                    <button type="button big" id="close" class="close inverse" onclick="javascript:window.history.go(-1);">&times;</button>
    </div>
    <div class="row-fluid">
        <div class="top-bar">
            
            <h3 style="font-size: 2em;line-height: 27px;">Ticket N° <?php echo $this -> _['ticket'] ? $this -> _['ticket'] -> getTicket_code() : "" ?></h3>
            <div class="pull-right">
                <button class='btn maj'>Mettre à jour</button>
                <button class='btn' onClick='addObserver(<?php echo $this -> _['ticket'] ? $this -> _['ticket'] -> getTicket_id() : ""?>); return false;'>Surveiller</button>
            </div>
        </div>
        
<?php
    if($this->_['ticket']){ ?>
    <div id="ticket_info"  class="alert" style="color: black;">        
        <h4>Titre : <?php echo $this -> _['ticket'] -> getTicket_title() ?></h4>
        Crée par <a href="#"><?php echo $this -> _['creatorName']?></a> le <?php echo smartDate($this -> _['ticket'] -> getDate_created()) ?>

        <hr /><p class="lead"><b>Client : </b><?php echo $this->_['ticketClient'] ?></p>
        <p class="lead"><b>Type de ticket : </b><?php echo $this->_['ticket']->getTicket_type() ?></p>  
        <div class="row-fluid">
            <div class="span6">
                <dl class="dl-horizontal" >
                    <dt>Statut :</dt>
                    <dd><?php echo getLabelStateTicket($this->_['ticket']->getTicket_state_label()) ?></dd>
                    <br />
                    <dt>Priorité :</dt>
                    <dd><?php echo getLabelPriorityTicket($this->_['ticket']->getTicket_priority_label()) ?></dd>
                </dl>    
            </div>
            
            <div class="span6">
                <dl class="dl-horizontal">
                    <dt>Assigné à :</dt>
                    <dd><?php if($this->_['assigned_user']) echo $this->_['assigned_user']->getName(); 
                              else echo " - ";?></dd>
                    
                    <dt>Observateurs :</dt>
                    <dd><?php foreach ($this->_['listObservateur'] as $user) {
                                echo $user -> getName()."<br/>";  
                              } ?>   </dd>
                </dl>
            </div>
        </div> 
        <dl>
<?php      if($this->_['ticketRelances']){ ?>
            <dt>Relances : <a  href="#" onClick="showTicketViewRelances(); return false;">Voir (<?php echo count($this->_['ticketRelances']) ?>)</a></dt>
            <dd>
                <div id="relances" class="closed hide">
                    <?php echo $this -> getMiniRelanceListTable($this->_['ticketRelances'], $this -> _['ticket']);   ?>
                </div>                
            </dd>
<?php       }else{
                echo "<dt>Relances : Aucune </dt>";
            }
?>
        </dl>

        <br />
        <dl>
            <dt>Description : <?php echo $ticket_visible ?></dt>
            <dd class="ticket_description" style="font-size: 1em;line-height: 19px;"><?php echo $this -> _['ticket'] -> getTicket_text() ?></dd>
        </dl>
    </div>  
        <div class="historique">
            <h4>Historique</h4><hr />
            
<?php
            if ($this->_['ticketHistory']) {
                
                foreach ($this->_['ticketHistory'] as $history) { 
                    if($updater = $userManager -> getBy(array("user_id" => $history-> getCreator_user_id()))){
                        $updater_name = $updater -> getName();
                    }
                    else{
                        $updater_name = "Client";    
                    }
                    
                    $history->getTicket_history_visibility() ? $visible = "<span class='label label-important'>Visible client :</span>" : $visible = "";
                    ?>
                     
                     <p class="lead">Mis à jour par <b><?php echo $updater_name ?></b>, le <?php echo smartDate($history -> getDate_created()) ?> </p>
                     
                     
                         <?php
                         if($history -> getState_id_change() || $history -> getPriority_id_change()|| $history -> getTicket_assign_change()){ ?>
                       <ul class="unstyled" style="list-style:initial">       
                     <?php  if($history -> getState_id_change()){
                                  echo "<li>Statut changé de <strong>".getLabelStateTicket($history -> getState_id_old_label())."</strong> à <strong>".getLabelStateTicket($history -> getState_id_change_label())."</strong></li>";
                            }
                            if($history -> getPriority_id_change()){
                                  echo "<li>Priorité changée de <strong>".getLabelPriorityTicket($history -> getPriority_id_old_label())."</strong> à <strong>".getLabelPriorityTicket($history -> getPriority_id_change_label())."</strong></li>";   
                            }
                            if($history -> getTicket_assign_change()){
                                echo "<li> Assigné ";
                                    if($history -> getTicket_assign_old()) 
                                        echo " de <strong>".$history -> getTicket_assign_old_user() -> getName()."</strong>";
                                    echo " à <strong>".$history -> getTicket_assign_change_user() -> getName(). "</strong></li>";
                            } ?>
                       </ul>      
                 <?php  }
                         ?>
                     
                     <div class="ticket_history_text row-fluid">
                     <?php
                     if($history -> getCommentaire())
                        echo " $visible <br/>". $history -> getCommentaire(); 
                     ?>                         
                     </div>
                     <hr />                  
<?php           }
                
            }else {
                echo "<em>Aucun historique</em>";
            }

?>            
        </div>
        
        <div class="pull-right">
            <button class='btn maj'>Mettre à jour</button>
            <button class='btn'  onClick='addObserver(<?php echo $this -> _['ticket'] -> getTicket_id()?>); return false;'>Surveiller</button>
        </div>
        
        <div id="maj_section" class="hide row-fluid">
           <a name="maj_section"></a>
          <h4>Mettre à jour</h4>
          <?php //var_dump($this->_['stateList']) ?>
          <hr />
            <form class="form-horizontal" id="edit_ticket" name="edit_ticket" action="ticket_edit" method="POST" onSubmit="return confirmSubmitTicket();">
                <input id="id" name="id" type="hidden" class="hide" value="<?php echo $this -> _['ticket'] ->getTicket_id() ?>"/>
                <input id="update" name="update" type="hidden" class="hide" value="1"/>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">          
                            <label class="control-label" for="select_state">Statut :</label>
                                <div class="controls">
                                    <select name="select_state" id="select_state">
                                        <?php foreach ($this->_['stateList'] as $state) {
                                            echo "<option value='".$state->getState_id()."'";
                                                if($this->_['ticket']->getTicket_state_id() == $state->getState_id() )
                                                    echo " selected ";
                                            echo ">".ucfirst($state->getLabel())."</option>";
                                        } ?>
                                    </select>
                                    
                                </div>
                        </div>
                        
                        <div class="control-group">          
                            <label class="control-label" for="select_priority">Priorité :</label>
                            <div class="controls">
                                <select name="select_priority" id="select_priority">
                                    <?php foreach ($this->_['priorityList'] as $priority) {
                                        echo "<option value='".$priority->getPriority_id()."'"; 
                                            if($this->_['ticket']->getTicket_priority_id() == $priority->getPriority_id() )
                                                echo " selected ";
                                        echo ">".ucfirst($priority->getLabel())."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                                            
                        <div class="control-group">          
                            <label class="control-label" for="select_TicketType">Type de ticket :</label>
                            <div class="controls">
                                <?php echo BusinessReferentielUtils::getSelectList("TicketType", "Type de ticket...", $this->_['ticket']->getTicket_type_id()) ?>
                            </div>
                        </div>                        
                    </div>
                    
                    <div class="span6">
                        <div class="control-group">          
                            <label class="control-label" for="listuser">Assigné à :</label>
                            <div class="controls">
                               <input type="hidden" id="listuser" name="listuser">
                            </div>
                        </div>
                        
                        <div class="control-group">          
                            <label class="control-label" for="listobs">Observateurs :</label>
                            <div class="controls">
                               <input type="hidden" id="listobs" name="listobs" style="width:50%;">
                            </div>
                        </div>  
                        <div class="control-group">          
                            <label class="control-label" for="public">Visible pour le client :</label>
                            <div class="controls">
                               <input type="checkbox" id="public" name="public" value="1"> 
                            </div>
                        </div>                        
                    </div>
                </div>
                <br /><br />
                <div class="row-fluid">
                    <div class="control-group span12">
                        <label for="commentaire" class="control-label">Commentaire :</label>
                        <div class="controls">
                            <textarea class='tinyMce' name="commentaire" id"commentaire" rows="8" cols="200" style="width: 900px; height: 150px;"></textarea>
                        </div>
                    </div>                    
                </div>
 
                    
                <button class="btn btn-primary" type="submit"><i class='fa fa-check ' style='padding-right: 0px'></i> Enregistrer</button>
                <button type="reset" class="btn btn-inverse">Reset</button> 
            </form>
            
        </div>
    </div>
</div>

<div id="observerModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="observerModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="observerModalLabel" >Surveiller</h4>
  </div>
  <div class="modal-body">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<div id="modalAddProduct" class="modal hide fade"  role="dialog" aria-labelledby="addProductModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="addProductModalLabel">Ajouter lien produit</h4>
  </div>
  <div class="modal-body">
      <input type="hidden" id="listprods" name="listprods" style="width:95%;">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" onClick="addContentTinyMCE(); return false;">Ajouter</button> 
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<script type="text/javascript" charset="utf-8">

    $( ".maj" ).click(function() {
      $("#maj_section").removeClass("hide");
      scrollToAnchor('maj_section');
    });
    
    var obj = jQuery.parseJSON('<?php echo $this->_['json'] ?>');
    var list = obj.data;
    var selected = obj.selected;
    var obsselected = obj.obsselected;
    
    $('#listuser').select2({
        data:list,
        multiple: false,
        width: "250px"
    });
    $('#listuser').select2("val", selected);
    
    $('#listobs').select2({
        data:list,
        multiple: true,
        width: "250px"
    });
    $('#listobs').select2("val", obsselected);
    
    
    function productFormatResult(product) {
        var markup = "<table class='product-result'><tr>";
        if (product.image !== undefined ) {
            markup += "<td class='product-image'><img src='" + product.image + "'/></td>";
        }
        markup += "<td class='product-info'><div class='product-title'>" + product.text + "</div>";
        
        if (product.shortdesc !== undefined) {
            markup += "<div class='product-synopsis'>" + product.shortdesc + "</div>";
        }
        //else if (movie.synopsis !== undefined) {
          //  markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
        //}
        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.text;
    }
        
    $("#listprods").select2({
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 

    $("#fastsearch").select2({
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 
        
    function addContentTinyMCE (content) {
      var $id_produit = $("#listprods").select2('data').id;  
      
      theData = {
          id_produit : $id_produit
      }
      
      var $html = "";
      
      jQuery.ajax({
        type : 'POST',
        url : "/app.php/getproduct",
        data : theData,
        success : function(data) {
            
            tinyMCE.activeEditor.dom.add(tinyMCE.activeEditor.getBody(), 'p', {title : 'Produit :'}, data);
        }
      });
    }
</script>
<?php
}else{
?>
<div id="ticket_info"  class="alert" style="color: black;">
<h4>Ce ticket n'existe pas !</h4>
</div>
<?php } ?>