<script>
    $(function() {
        var $tabs = $("#tabs").tabs();
         $(".ui-tabs-panel").each(function(i){
          var totalSize = $(".ui-tabs-panel").size() - 1;
          //$(this).append
          outnext = outprev = "";
          if (i != totalSize) {
              next = i + 1;
              outnext = ("<a href='#' class='next-tab mover'style=' margin-left: 30%; font-weight:bold; font-size:14px' rel='" + next + "'> <?php echo _("Suivant") ?> >> </a>");
          }
         
          if (i != 0) {
              prev = i-1;
              outprev = ("<a href='#' class='prev-tab mover'  style=' font-weight:bold; font-size:14px' rel='" + prev + "'> << <?php echo _("Précédent") ?></a>");
          }
          
          var out = ""+outprev+" "+outnext+"";
          
          $("#button_form"+i).html(out);
        });
        
        $('.next-tab, .prev-tab').click(function() { 
               $tabs.tabs('select', parseInt($(this).attr("rel")) ) ;
               
               return false;
           });
    });
    </script>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<?php


?>

<form id='datamaj'>
    <input type='hidden' id='SITEID' name='SITEID' value='<?php echo $this->_["ID"]?>'/>
<div id="content" class="content ui-corner-all">
	<div id="tabs">
        <ul id="tabnav">
         <?php echo $head; ?>
        </ul>
	<p>Client : <b><?php echo $this -> _['VENDOR_NOM'] ?></b></p>
	<p>Type de système : <b><?php echo $this -> _['SOFTWARE'] ?></b></p>
	<p>Etat : <b><?php echo "tmp" ?></b></p>
	<p>Dernière synchro du plug-in : <b><?php echo smartdate($this -> _['DATE_INSERT']) ?></b></p>
	<table>
    <tr>
        <td>
        <table>
        	<thead>
        		<tr>
        			<td><b>Champ importé</b></td>
        			<td><b>Valeur</b></td>
        			<td><b>Correspondance</b></td>
        			<td></td>
        		</tr>
        	</thead>
            <tbody>
                <tr>
                    <td class="form_label">ID_PRODUCT</td>
                    <td><?php echo $this->_['ID_PRODUCT'] ?></td>
                    <td>ID_PRODUCT</td>
                </tr>
                
               <tr>
                    <td class="form_label">VENDOR_ID</td>
                    <td ><?php echo $this->_['VENDOR_ID'] ?></td>
                    <td>VENDOR_ID</td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">NAME_PRODUCT</td>
                    <td ><?php echo $this->_['NAME_PRODUCT'] ?></td>
                    <td>NAME_PRODUCT</td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">REFERENCE_PRODUCT</td>
                    <td ><?php echo $this->_['REFERENCE_PRODUCT'] ?></td>
                    <td>REFERENCE_PRODUCT</td>
                    <td>
                    	<select name="ref_prod" id="ref_prod">
                    		<option>Code EAN</option>
                    		<option>Référence produit</option>
                    	</select>
                    </td>
                </tr>
                <tr>
                    <td class="form_label">SUPPLIER_REFERENCE</td>
                    <td ><?php echo $this->_['SUPPLIER_REFERENCE'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">MANUFACTURER</td>
                    <td ><?php echo $this->_['MANUFACTURER'] ?></td>
                    <td></td>
                    <td>
                    	<select name="manufacturer" id="manufacturer">
                    		<option selected>MANUFACTURER</option>
                    		<option>SUPPLIER_REFERENCE</option>
                    	</select>
                    </td>
                </tr>
                <tr>
                    <td class="form_label">CATEGORY</td>
                </tr>
                <tr>
                    <td class="form_label">DESCRIPTION</td>
                    <td ><?php echo strlen($this->_['DESCRIPTION']) < 80 ?  $this->_['DESCRIPTION'] : substr($this->_['DESCRIPTION'], 0, 80)."..." ?></td>
                    <td></td>
                    <td>
                    	<select name="descrip" id="descrip">
                    		<option selected>DESCRIPTION</option>
                    		<option>CATEGORY</option>
                    		<option>EAN</option>
                    	</select>
                    </td>
                </tr>
                <tr>
                    <td class="form_label">DESCRIPTION_SHORT</td>
                    <td ><?php echo strlen($this->_['DESCRIPTION_SHORT']) < 80 ?  $this->_['DESCRIPTION_SHORT'] : substr($this->_['DESCRIPTION_SHORT'], 0, 80)."..." ?></td>
                    <td></td>
                    <td>
                    	<select name="descrip_short" id="descrip_short">
                    		<option selected>DESCRIPTION SHORT</option>
                    		<option>NAME PRODUCT</option>
                    	</select>
                    </td>
                </tr>
                <tr>
                    <td class="form_label">PRICE_PRODUCT</td>
                    <td ><?php echo $this->_['PRICE_PRODUCT'] ?></td>
                    <td>PRIX_TTC</td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">WHOLESALE_PRICE</td>
                    <td ><?php echo $this->_['WHOLESALE_PRICE'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">PRICE_HT</td>
                    <td ><?php echo $this->_['PRICE_HT'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">PRICE_REDUCTION</td>
                    <td ><?php echo $this->_['PRICE_REDUCTION'] ?></td>
                </tr>
                <tr>
                    <td class="form_label">POURCENTAGE_REDUCTION</td>
                    <td ><?php echo $this->_['POURCENTAGE_REDUCTION'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">QUANTITY</td>
                    <td ><?php echo $this->_['QUANTITY'] ?></td>
                    <td>STOCK</td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">WEIGHT</td>
                    <td ><?php echo $this->_['WEIGHT'] ?></td>
                    <td>POIDS</td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">EAN</td>
                    <td ><?php echo $this->_['EAN'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">ECOTAX</td>
                    <td ><?php echo $this->_['ECOTAX'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">AVAILABLE_PRODUCT</td>
                    <td ><?php echo $this->_['AVAILABLE_PRODUCT'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">URL_PRODUCT</td>
                    <td ><?php echo $this->_['URL_PRODUCT'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">IMAGE_PRODUCT</td>
                    <td ><?php echo $this->_['IMAGE_PRODUCT'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">FDP</td>
                    <td ><?php echo $this->_['FDP'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">ID_MERE</td>
                    <td ><?php echo $this->_['ID_MERE'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">DELAIS_LIVRAISON</td>
                    <td ><?php echo $this->_['DELAIS_LIVRAISON'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">IMAGE_PRODUCT_2</td>
                    <td ><?php echo $this->_['IMAGE_PRODUCT_2'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">IMAGE_PRODUCT_3</td>
                    <td ><?php echo $this->_['IMAGE_PRODUCT_3'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">REDUCTION_FROM</td>
                    <td ><?php echo $this->_['REDUCTION_FROM'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">REDUCTION_TO</td>
                    <td ><?php echo $this->_['REDUCTION_TO'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">META_DESCRIPTION</td>
                    <td ><?php echo $this->_['META_DESCRIPTION'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">META_KEYWORDS</td>
                    <td ><?php echo $this->_['META_KEYWORDS'] ?></td>
                    <td><em>inutilisé</em></td>
                    <td><a href="#">Modifier</a></td>
                </tr>
                <tr>
                    <td class="form_label">ACTIVE</td>
                    <td ><?php echo $this->_['ACTIVE'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">DATE_UPDATE</td>
                    <td ><?php echo $this->_['DATE_UPDATE'] ?></td>
                    
                </tr>
                <tr>
                    <td class="form_label">DATE_CREATE</td>
                    <td ><?php echo $this->_['DATE_CREATE'] ?></td>
                    
                </tr>
                
            </tbody>
        </table>
        </td>
    
       <?php echo $content .$msgadd; ?>
    </div>

</div>
</form>
<div id="dialog2" title=""></div>
<div id='err'>  </div>