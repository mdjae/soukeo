<?php $vendorManager = new ManagerVendor(); 
    $this -> _['embed'] ? $class = " col6 embed " : $class ="";
?>


<div class="container well <?php echo $class ?>" id="ficheproduit-<?php echo $this->_['product']->getId_produit() ?>">

    <div class="row-fluid">
        <a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/app.php/dashboard" >Retour Tableau de bord</a>
                    <button type="button big" id="close" class="close inverse" onclick="javascript:window.history.go(-1);">&times;</button>
    </div>
        
        <div class="top-bar">
            
            <h3 style="font-size: 1.70em;line-height: 27px;">
                <a href="/app.php/catalogue/produit/fiche?id=<?php echo $this->_['product'] ? $this->_['product']->getId_produit() : "" ?>" class='btn'><i class="fa fa-arrow-left"></i></a>
                <?php echo $this->_['product'] ? truncateStr($this-> _['product']->getName(), 55) : "-" ?>
            </h3>
            <form action="/app.php/catalogue/produit/fiche" method="get" name="fast-search">
              
            <button type="submit" class="btn pull-right" style="margin-top:15px; margin-left:15px"><i class="fa fa-search"></i></button>
            <input type="hidden" id="id" name="id" style="width:35%;margin-top:15px" class="pull-right" onchange="this.form.submit()">
            
            </form>
        </div>
        
<?php if($this->_['product']) { ?>
    <form action="/app.php/catalogue/produit/fiche/edit/valid" method="POST" accept-charset="utf-8">
    <div id="product_fields" class="row-fluid">
        <div class="tabbable tabs"> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#attrs_statiques" data-toggle="tab">Fiche produit</a></li>
            <li><a href="#desc_html" data-toggle="tab">Description HTML</a></li>
            <li><a href="#images" data-toggle="tab">Champs image</a></li>
            <li><a href="#attributs" data-toggle="tab">Attributs</a></li>
            <li><a href="#vue_avahis" data-toggle="tab">Vue Avahis</a></li>
          </ul>

            <div class="tab-content">
                
                <div class="tab-pane active" id="attrs_statiques" >

                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="inputName">Nom : </label>
                                    <div class="controls">
                                        <input type="hidden" id="id_produit" name="id_produit" value="<?php echo $this->_['product']->getId_produit(); ?>">
                                        <input type="text" id="name" name="name" placeholder="Nom du produit" value="<?php echo $this->_['product']->getName(); ?>">
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="inputManufacturer">Manufacturer : </label>
                                    <div class="controls">
                                        <input type="text" id="manufacturer" name="manufacturer" placeholder="Marque du produit..." value="<?php echo $this->_['product']->getManufacturer(); ?>">
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="weight">Poids : </label>
                                    <div class="controls">
                                        <input type="text" id="weight" name="weight" placeholder="Poids du produit..." value="<?php echo $this->_['product']->getWeight(); ?>">
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="sku">SKU : </label>
                                    <div class="controls">
                                        <input type="text" id="sku" name="sku" placeholder="SKU..." value="<?php echo $this->_['product']->getSku(); ?>">
                                    </div>
                                </div>
                            </div>
                        
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="categories">Id Catégorie : </label>
                                    <div class="controls">
                                        <input type="text" id="categories" name="categories" placeholder="Id catégorie" value="<?php echo $this->_['product']->getCategories(); ?>">
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label" for="country_of_manufacture">Pays fabrication : </label>
                                    <div class="controls">
                                        <input type="text" id="country_of_manufacture" name="country_of_manufacture" placeholder="Pays de fabrication..." value="<?php echo $this->_['product']->getCountry_of_manufacture(); ?>">
                                    </div>
                                </div> 
                                
                                <div class="control-group">
                                    <label class="control-label" for="ean">Ean : </label>
                                    <div class="controls">
                                        <input type="text" id="ean" name="ean" placeholder="Ean..." value="<?php echo $this->_['product']->getEan(); ?>">
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="dropship_vendor">Dropship_vendor : </label>
                                    <div class="controls">
                                        <input type="text" id="dropship_vendor" name="dropship_vendor" placeholder="Dropship_vendor..." value="<?php echo $this->_['product']->getDropship_vendor(); ?>">
                                    </div>
                                </div>     
                  
                            </div>
                        </div>
                        <br/><br/><br/><br/><br/><br/><br/>
                        <div class="row-fluid">                                                
                            <div class="control-group">
                                <label class="control-label" for="short_description">Petite description : </label>
                                <div class="controls">
                                    <textarea type="text" class="edit" id="short_description" name="short_description" placeholder="Petite description" ><?php echo $this->_['product']->getShort_description(); ?></textarea>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="description">Description longue : </label>
                                <div class="controls">
                                    <textarea type="text" class="edit" id="description" name="description" placeholder="Description" ><?php echo $this->_['product']->getDescription(); ?></textarea>
                                </div>
                            </div>
                        </div>
                </div>
                
                <div class="tab-pane" id="desc_html">
                        <div class="control-group">
                            <label class="control-label" for="description_html">Description HTML : </label>
                            <div class="controls">
                                <textarea type="text" id="description_html" name="description_html" placeholder="Description HTML ..." value=""><?php echo $this->_['product']->getDescription_html(); ?></textarea>
                            </div>
                        </div>
                </div>
                
                <div class="tab-pane" id="images">
                    
                    <div class="row-fluid">

                        <div class="span5">
    
                            <div class="control-group">
                                <label class="control-label" for="image">Image : </label>
                                <div class="controls">
                                    <input type="text" id="image" name="image" placeholder="Image..." value="<?php echo $this->_['product']->getImage(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="small_image">Small image : </label>
                                <div class="controls">
                                    <input type="text" id="small_image" name="small_image" placeholder="Small image..." value="<?php echo $this->_['product']->getSmall_image(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="thumbnail">Thumbnail : </label>
                                <div class="controls">
                                    <input type="text" id="thumbnail" name="thumbnail" placeholder="Thumbnail..." value="<?php echo $this->_['product']->getThumbnail(); ?>">
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="span7">
                                           
                            <div class="control-group">
                                <label class="control-label" for="local_image">Local image : </label>
                                <div class="controls">
                                    <input type="text" id="local_image" name="local_image" placeholder="Local image..." value="<?php echo $this->_['product']->getLocal_image(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="local_small_image">Local small image : </label>
                                <div class="controls">
                                    <input type="text" id="local_small_image" name="local_small_image" placeholder="Local small image..." value="<?php echo $this->_['product']->getLocal_small_image(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="local_thumbnail">Local thumbnail : </label>
                                <div class="controls">
                                    <input type="text" id="local_thumbnail" name="local_thumbnail" placeholder="Local thumbnail..." value="<?php echo $this->_['product']->getLocal_thumbnail(); ?>">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row-fluid">
                            <div class="span5">
                                <ul class="thumbnails">
                                    <li class="span4">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getImage() ? "href='".$this->_['product']->getImage()."' class='gallery'" : "#" ?> ><img  src="<?php echo $this->_['product']->getImage() ?>" alt="" /></a>
                                           <h4>Image</h4>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getSmall_image() ? "href='".$this->_['product']->getSmall_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getSmall_image() ?>" alt="" /></a>
                                           <h6>Small Image</h6>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getThumbnail() ? "href='".$this->_['product']->getThumbnail()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getThumbnail() ?>" alt="" /></a>
                                           <h6>Thumbnail</h6>    
                                        </div>
                                    </li>
                                </ul> 
                            </div>
                            <div class="span5">
                                <ul class="thumbnails">
                                    <li class="span4">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getLocal_image() ? "href='".$this->_['product']->getLocal_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getLocal_image() ?>" alt="" /></a>
                                           <h4>Local Image</h4>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getLocal_small_image() ? "href='".$this->_['product']->getLocal_small_image()."' class='gallery'" : "#" ?>>
                                               <img src="<?php echo $this->_['product']->getLocal_small_image() ?>" alt="" />
                                           </a>
                                           <h6>Local Small Image</h6>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getLocal_thumbnail() ? "href='".$this->_['product']->getLocal_thumbnail()."' class='gallery'" : "#" ?>>
                                               <img data-lightbox="image-1" src="<?php echo $this->_['product']->getLocal_thumbnail() ?>" alt="" />
                                           </a>
                                           <h6>Local thumbnail</h6>    
                                        </div>
                                    </li>
                                </ul>                                
                            </div>
                        </div>
                                
                       
                    </div>
                    

                </div>
                
                <div class="tab-pane" id="attributs">
                    
<?php               if($this->_['prod_attr_list']){ ?>
                        
                        <table class="table table-bordered">
                            
                            <thead>
                                <tr>
                                    <td>Id attribut</td>
                                    <td>Nom attribut</td>
                                    <td>Code attribut</td>
                                    <td>Valeur</td>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php 
                                foreach ($this->_['prod_attr_list'] as $product_attr) {
                                    
                                    echo "<tr>";
                                    if($product_attr->getAttribut()){
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getLabel_attr()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getCode_attr()."</td>";
                                        echo "  <td>".$product_attr->getValue()."</td>";    
                                    }
                                    else{
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td><strong style='color:red'>Attribut inexistant</strong></td>";
                                        echo "  <td> - </td>";
                                        echo "  <td> - </td>";
                                    }
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
<?php               }else{ 
                        echo "<em>Pas d'attributs</em>";    
                    } ?>
                    
                </div> 
                
                <div class="tab-pane" id="vue_avahis">
                    <iframe id="iframe_avahis" scrolling="auto" style="width:100%; height:1400px; padding:15px" sandbox="allow-scripts allow-same-origin"  src="/app.php/getAvahisView?id=<?php echo $this->_['product']->getId_produit(); ?>" frameborder="0"></iframe>
                </div>
                
            </div> 
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" id="inputSubmit" name="inputSubmit" class="btn btn-primary" placeholder="Valider..." value="Valider">
            </div>
        </div> 
    </div>
    </form>
</div>
<?php 
}
else{ ?>
    <div id="product_info"  style="color: black;" class="row-fluid">
        <br />
        <h3>Désolé, ce produit ne semble plus exister...</h3>
    </div> 
<?php
}
 ?>

<script type="text/javascript" charset="utf-8">
    
    jQuery('a.gallery').colorbox({opacity:0.5 , rel:'group1'});
    
    function productFormatResult(product) {
        var markup = "<table class='product-result'><tr>";
        if (product.image !== undefined ) {
            markup += "<td class='product-image'><img src='" + product.image + "'/></td>";
        }
        markup += "<td class='product-info'><div class='product-title'>" + product.text + "</div>";
        
        if (product.shortdesc !== undefined) {
            markup += "<div class='product-synopsis'>" + product.shortdesc + "</div>";
        }
        //else if (movie.synopsis !== undefined) {
          //  markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
        //}
        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.text;
    }

    $("#id").select2({
        placeholder: "Recherche rapide nom du produit / SKU ...",
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 
    
    function resetTest () {
      $("#message-sku").html("");
    }
    
    function testSku (elm) {
        
        var sku = $("#sku").val();   
        sku=sku.replace(/^\s+|\s+$/,'');

        if(sku != ""){
            $(elm).prop("disabled", true);
            $("#message-sku").html("<img src='/skins/soukeo/loading.gif' /> Vérfif...");  
            
            var theData = {
                sku : sku
            };
            
            jQuery.ajax({
                type : 'POST',
                url : "/app.php/checkSKU",
                data : theData,
                success : function(data) {
                    
                    if(data==0){
                        $("#message-sku").html("<img border='0' alt='Ok' align='absmiddle' class='iconCircleCheck' src='/skins/common/images/picto/check.gif'><strong style='color:green'> Sku Disponible<strong>");
                        $(elm).prop("disabled", false);
                    }else{
                        $("#message-sku").html("<img border='0' alt='KO' align='absmiddle' class='iconCircleWarn' src='/skins/common/images/picto/denied.gif'><strong style='color:red'> Sku déja pris<strong>");
                        $(elm).prop("disabled", false);
                    }
                }
            });  
        }
        
    }
    
</script>
