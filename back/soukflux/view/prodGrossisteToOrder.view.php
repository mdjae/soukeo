
<div class="container-fluid">
	
	<div class='row-fluid'>
		<div class="span5 well">
			<h4 id="title">Produits à commander <small>Assignez les produits séléctionnés à une commande grossiste</small></h4>
			<form class="form-inline">
		  		<input type="text" class="input-medium" placeholder="N°Commande Grossiste" id="inputNumCom" >
		  		<button type="submit" class="btn" onclick="verifAssignCommGrossiste(); return false;">Valider</button>
			</form>
		</div>
		<div class="span4" id="alert-recipient">

		</div>
	</div>
	
	<div class='row-fluid'>
		<div class='span6'>
			<?php echo $this-> _['gridMAGI'] ; ?>
		</div>
	    
	    <div class='span6'>
	    	<?php echo $this-> _['gridLDLC'] ; ?>
		</div>
	</div>
	<hr />
	<div class='row-fluid'>
		<div class='span6'>
			<?php echo $this-> _['gridFK'] ; ?>
		</div>
	</div>
</div>

<script type="text/javascript" charset="utf-8">
	$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'left'
						});
</script>
