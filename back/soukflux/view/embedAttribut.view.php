<?php $vendorManager = new ManagerVendor(); ?>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

     <h2 style="font-size: 2.5em; line-height: 31px; margin-bottom: 11px;">Attribut : <?php echo utf8_decode(truncateStr(htmlentities($this->_['attribut']->getLabel_attr()), 40)) ?></h2>   
<?php if($this->_['attribut']) { ?>
    <div id="attribut_info_<?php echo $this->_['attribut']->getId_attribut() ?>"  style="color: black;" class="row-fluid">
        <!--<button onClick="saveAttribut(<?php echo $this->_['attribut']->getId_attribut() ?>);return false;" class="btn btn-primary pull-right" style="margin-top:5px;">
            <i class="fa fa-save"></i>Sauvegarder
        </button> -->
        <button onClick="deleteAttribut(<?php echo $this-> _['attribut']->getId_attribut()?>, '<?php echo utf8_decode(str_replace('"', " pouces ", htmlentities($this->_['attribut']->getLabel_attr()))) ?>'); return false" class="btn btn-important pull-right" style="margin-top:5px;">
             <i class="fa fa-trash"></i>Supprimer
        </button>
        <br />
       <div class="tabbable tabs span11"> 
           <div id="messagebox-attribut_<?php echo $this->_['attribut']->getId_attribut() ?>"></div>
              <ul class="nav nav-tabs">
                <li class="active" id="goto_values_attr_<?php echo $this->_['attribut']->getId_attribut() ?>"><a href="#values_<?php echo $this->_['attribut']->getId_attribut() ?>" data-toggle="tab"><i class="fa fa-tags"></i><?php echo utf8_decode("Valeurs") ?></a></li>
                <li><a href="#prods_<?php echo $this->_['attribut']->getId_attribut() ?>" data-toggle="tab"><i class="fa fa-shopping-cart"></i><?php echo utf8_decode("Produits concernés") ?></a></li>
                <li><a href="#parameters_<?php echo $this->_['attribut']->getId_attribut() ?>" data-toggle="tab"><i class="fa fa-wrench"></i><?php echo utf8_decode("Paramètres") ?></a></li>
                
                
              </ul>
              <div class="tab-content">

                
                
                <div class="tab-pane active" id="values_<?php echo $this->_['attribut']->getId_attribut() ?>">
                     
                    <div id="pagivalueslist_<?php echo $this->_['attribut']->getId_attribut() ?>">
                        
                        <div class="pull-right">
                            <strong>Pour la <?php echo utf8_decode("sélection") ?> : </strong> <br />
                            <button class="btn btn-warning" onClick="showDialogFusionValues(<?php echo $this->_['attribut']->getId_attribut() ?>)"><i class="fa fa-gears"></i>Fusionner</button>
                        </div>                        
                        <br /><br />
                        <p><span ><?php echo $this->_['total_values']; ?></span><span><?php echo utf8_decode(" valeur(s) trouvé(s)"); ?></span></p>  
                        <div class='pager'>
                            <button onclick='firstPageValuesAttr(<?php echo $this->_['attribut']->getId_attribut() ?>);return false;' class='btn paging'><i class="fa fa-fast-backward"></i></button>
                          
                            <button onclick='prevPageValuesAttr(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;' class='btn paging'><i class="fa fa-backward"></i></button>
                            <span> [ <span id='curr_page_values_<?php echo $this->_['attribut']->getId_attribut() ?>'><?php echo $this->_['curr_page'] + 1; ?></span> / <span id="total_page_<?php echo $this->_['attribut']->getId_attribut() ?>"><?php echo $this->_['total_page_values']; ?></span> ] </span>
                            <button onclick='nextPageValuesAttr(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;' class='btn paging'><i class="fa fa-forward"></i></button>
                            <button onclick='lastPageValuesAttr(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;' class='btn paging'><i class="fa fa-fast-forward"></i></button>
                            
                            Afficher : <select onChange="setItemPerPage(<?php echo $this->_['attribut']->getId_attribut() ?>, this); return false;" name="itemsperpage_<?php echo $this->_['attribut']->getId_attribut() ?>>" class="input input-mini item_per_page_values" id="itemsperpage_<?php echo $this->_['attribut']->getId_attribut() ?>>" style="padding-left: 1px;">
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                    </div>
                    
                    <div id="values_list_<?php echo $this->_['attribut']->getId_attribut() ?>" class="values_attribut_table">
                        <?php echo  utf8_decode($this->_['list_values']) ?>    
                    </div>
                </div>
                
                <div class="tab-pane" id="prods_<?php echo $this->_['attribut']->getId_attribut() ?>">
                    <div class="row-fluid" id="catalogueattr_<?php echo $this->_['attribut']->getId_attribut() ?>">
                        <div id="pagicatalogattr_<?php echo $this->_['attribut']->getId_attribut() ?>">
                            <br /><br />
                            <p><span ><?php echo $this->_['total_prods']; ?></span><span><?php echo utf8_decode(" produit(s) trouvé(s)"); ?></span></p>  
                            <div class='pager'>
                                <button onclick='firstPageProdAttr(<?php echo $this->_['attribut']->getId_attribut() ?>);return false;' class='btn paging'><i class="fa fa-fast-backward"></i></button>
                                <button onclick='prevPageProdAttr(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;' class='btn paging'><i class="fa fa-backward"></i></button>
                                <span> [ <span id='curr_page_<?php echo $this->_['attribut']->getId_attribut() ?>'><?php echo $this->_['curr_page'] + 1; ?></span> / <span id="total_page_prods_<?php echo $this->_['attribut']->getId_attribut() ?>"><?php echo $this->_['total_page']; ?></span> ] </span>
                                <button onclick='nextPageProdAttr(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;' class='btn paging'><i class="fa fa-forward"></i></button>
                                <button onclick='lastPageProdAttr(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;' class='btn paging'><i class="fa fa-fast-forward"></i></button>
                                
                            Afficher : <select onChange="setItemPerPageProds(<?php echo $this->_['attribut']->getId_attribut() ?>, this); return false;" name="itemsperpageprods_<?php echo $this->_['attribut']->getId_attribut() ?>>" class="input input-mini item_per_page_values" id="itemsperpageprods_<?php echo $this->_['attribut']->getId_attribut() ?>>" style="padding-left: 1px;">
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            </div>
                        </div>
                        <div id='prods_avahisGP_<?php echo $this->_['attribut']->getId_attribut() ?>' class='gridview'>
                            <?php echo  utf8_decode($this->_['product_list']) ?>
                        </div>
                   
                   </div>
                </div>
                
                <div class="tab-pane" id="parameters_<?php echo $this->_['attribut']->getId_attribut() ?>">
                   <form action="" method="POST" accept-charset="utf-8">

                       <div class="control-group">
                            <label class="control-label" for="label_attr_<?php echo $this->_['attribut']->getId_attribut() ?>">Label : </label>
                            <div class="controls">
                                <input type="text" id="label_attr_<?php echo $this->_['attribut']->getId_attribut() ?>" name="label_attr_<?php echo $this->_['attribut']->getId_attribut() ?>" placeholder="Nom de l'attribut..." value="<?php echo utf8_decode($this->_['attribut']->getLabel_attr()); ?>">
                            </div>
                        </div>
    
                        <div class="control-group">
                            <label class="control-label" for="placeholder_<?php echo $this->_['attribut']->getId_attribut() ?>">Placeholder : </label>
                            <div class="controls">
                                <input type="text" id="placeholder_<?php echo $this->_['attribut']->getId_attribut() ?>" name="placeholder_<?php echo $this->_['attribut']->getId_attribut() ?>" placeholder="Placeholder..." value="<?php echo "" ?>">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <button type="button" onClick="saveEditLabelAttribut(<?php echo $this->_['attribut']->getId_attribut() ?>); return false;" id="input_labelattr_<?php echo $this->_['attribut']->getId_attribut() ?>" name="inputSubmit_<?php echo $this->_['attribut']->getId_attribut() ?>" class="btn btn-primary pull-right" >
                                    Sauvegarder modifications
                                </button>
                            </div>
                        </div> 
                        
                    </form>  
                </div>
                
            </div>   
         </div>

            
    </div>

<?php 
}
else{ ?>

        <br />
        <h3>Désolé, cet attribut ne semble plus exister...</h3>
 
<?php
}
 ?>

<script type="text/javascript" charset="utf-8">
	$("#values_list_<?php echo $this->_['attribut']->getId_attribut() ?> table").keyup(function(event){
        if(event.keyCode == 70){
            showDialogFusionValues(<?php echo $this->_['attribut']->getId_attribut() ?>);
        }
    });
</script>
