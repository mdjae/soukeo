<?php $vendorManager = new ManagerVendor(); 
    $this -> _['embed'] ? $class = " embed " : $class ="";
?>

    <h2 style="font-size: 2.5em; line-height: 31px; margin-bottom: 11px;"><?php echo truncateStr($this->_['product']->getName(), 40) ?></h2>
<?php if($this->_['product']) { ?>
    <form action="/app.php/catalogue/produit/fiche/edit/valid" method="POST" accept-charset="utf-8">
    <div id="product_fields<?php echo "d".$this->_['product']->getId_produit() ?>" class="row-fluid">
        <button type="button" onClick="saveDuplicateFiche(<?php echo $this->_['product']->getId_produit() ?>); return false;" id="inputSubmit_<?php echo "d".$this->_['product']->getId_produit() ?>" style="margin-top:5px;" name="inputSubmit_<?php echo $this->_['product']->getId_produit() ?>" class="btn btn-primary pull-right" >Créer ce produit</button>
        <button id="buttonclose-<?php echo "d".$this->_['product']->getId_produit() ?>" onClick="cancelDuplicate('<?php echo $this->_['product']->getId_produit() ?>'); return false;" class="btn btn-danger pull-right" style="margin-top:5px;"><i class="fa fa-trash"></i>Annuler</button>
        <div class="tabbable tabs"> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#vue_avahis_<?php echo "d".$this->_['product']->getId_produit() ?>" data-toggle="tab">Vue Avahis</a></li>
            <li><a href="#attrs_statiques_<?php echo "d".$this->_['product']->getId_produit() ?>" data-toggle="tab">Fiche produit</a></li>
            <li><a href="#images_<?php echo "d".$this->_['product']->getId_produit() ?>" data-toggle="tab">Champs image</a></li>
            <li><a href="#attributs_<?php echo "d".$this->_['product']->getId_produit() ?>" data-toggle="tab">Attributs</a></li>
            
          </ul>

            <div class="tab-content">
                
                <div class="tab-pane" id="attrs_statiques_<?php echo "d".$this->_['product']->getId_produit() ?>" >

                    <div class="row-fluid">
                        <div class="span6">
    
                            <div class="control-group">
                                <label class="control-label" for="manufacturer_<?php echo "d".$this->_['product']->getId_produit() ?>">Manufacturer : </label>
                                <div class="controls">
                                    <input type="text" id="manufacturer_<?php echo "d".$this->_['product']->getId_produit() ?>" name="manufacturer_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Marque du produit..." value="<?php echo $this->_['product']->getManufacturer(); ?>">
                                </div>
                            </div>
    
                            <div class="control-group">
                                <label class="control-label" for="weight_<?php echo "d".$this->_['product']->getId_produit() ?>">Poids : </label>
                                <div class="controls">
                                    <input type="text" id="weight_<?php echo "d".$this->_['product']->getId_produit() ?>" name="weight_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Poids du produit..." value="<?php echo $this->_['product']->getWeight(); ?>">
                                </div>
                            </div>
    
                            <div class="control-group">
                                <label class="control-label" for="sku_<?php echo "d".$this->_['product']->getId_produit() ?>">SKU : </label>
                                <div class="controls">
                                    <input type="text" id="sku_<?php echo "d".$this->_['product']->getId_produit() ?>" name="sku_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Sku unique" required onkeyup="resetTest('<?php echo "d".$this->_['product']->getId_produit() ?>'); return false;" value="<?php echo $_POST['sku'] ?>"/>
                                    <button style="margin-bottom: 9px;" class="btn" type="button" onClick="testSku(this, '<?php echo "d".$this->_['product']->getId_produit() ?>'); return false;">Vérifier SKU</button>
                                    <br /><span style="margin-bottom: 9px;" id="message-sku-<?php echo "d".$this->_['product']->getId_produit() ?>"></span>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="price_<?php echo "d".$this->_['product']->getId_produit() ?>">Prix (indicatif) : </label>
                                <div class="controls">
                                    <input type="text" id="price_<?php echo "d".$this->_['product']->getId_produit() ?>" name="price_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Prix..." value="<?php echo $this->_['product']->getPrice(); ?>">
                                </div>
                            </div>  
                        </div>
                    
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="categories_<?php echo "d".$this->_['product']->getId_produit() ?>">Id Catégorie : </label>
                                <div class="controls">
                                    <input type="text" id="categories_<?php echo "d".$this->_['product']->getId_produit() ?>" name="categories_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Id catégorie" value="<?php echo $this->_['product']->getCategories(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="country_of_manufacture_<?php echo "d".$this->_['product']->getId_produit() ?>">Pays fabrication : </label>
                                <div class="controls">
                                    <input type="text" id="country_of_manufacture_<?php echo "d".$this->_['product']->getId_produit() ?>" name="country_of_manufacture_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Pays de fabrication..." value="<?php echo $this->_['product']->getCountry_of_manufacture(); ?>">
                                </div>
                            </div> 
                            
                            <div class="control-group">
                                <label class="control-label" for="ean_<?php echo "d".$this->_['product']->getId_produit() ?>">Ean : </label>
                                <div class="controls">
                                    <input type="text" id="ean_<?php echo "d".$this->_['product']->getId_produit() ?>" name="ean_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Ean..." value="<?php echo $this->_['product']->getEan(); ?>">
                                </div>
                            </div>
    
                            <div class="control-group">
                                <label class="control-label" for="dropship_vendor_<?php echo "d".$this->_['product']->getId_produit() ?>">Dropship_vendor : </label>
                                <div class="controls">
                                    <input type="text" id="dropship_vendor_<?php echo "d".$this->_['product']->getId_produit() ?>" name="dropship_vendor_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Dropship_vendor..." value="<?php echo $this->_['product']->getDropship_vendor(); ?>">
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="description_<?php echo "d".$this->_['product']->getId_produit() ?>">Description (Propre à soukflux) : </label><br /><br />
                        <div class="controls">
                            <textarea id="description_<?php echo "d".$this->_['product']->getId_produit() ?>" name="price_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Description...">
                                <?php echo $this->_['product']->getDescription(); ?>"
                            </textarea>
                        </div>
                    </div>
                      
                </div>


                <div class="tab-pane" id="images_<?php echo "d".$this->_['product']->getId_produit() ?>">
                    
                    <div class="row-fluid">

                        <div class="span5">
    
                            <div class="control-group">
                                <label class="control-label" for="image_<?php echo "d".$this->_['product']->getId_produit() ?>">Image : </label>
                                <div class="controls">
                                    <input type="text" id="image_<?php echo "d".$this->_['product']->getId_produit() ?>" name="image_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Image..." value="<?php echo $this->_['product']->getImage(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="small_image_<?php echo "d".$this->_['product']->getId_produit() ?>">Small image : </label>
                                <div class="controls">
                                    <input type="text" id="small_image_<?php echo "d".$this->_['product']->getId_produit() ?>" name="small_image_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Small image..." value="<?php echo $this->_['product']->getSmall_image(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="thumbnail_<?php echo "d".$this->_['product']->getId_produit() ?>">Thumbnail : </label>
                                <div class="controls">
                                    <input type="text" id="thumbnail_<?php echo "d".$this->_['product']->getId_produit() ?>" name="thumbnail_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Thumbnail..." value="<?php echo $this->_['product']->getThumbnail(); ?>">
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="span7">
                                           
                            <div class="control-group">
                                <label class="control-label" for="local_image_<?php echo "d".$this->_['product']->getId_produit() ?>">Local image : </label>
                                <div class="controls">
                                    <input type="text" id="local_image_<?php echo "d".$this->_['product']->getId_produit() ?>" name="local_image_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Local image..." value="<?php echo $this->_['product']->getLocal_image(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="local_small_image_<?php echo "d".$this->_['product']->getId_produit() ?>">Local small image : </label>
                                <div class="controls">
                                    <input type="text" id="local_small_image_<?php echo "d".$this->_['product']->getId_produit() ?>" name="local_small_image_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Local small image..." value="<?php echo $this->_['product']->getLocal_small_image(); ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="local_thumbnail_<?php echo "d".$this->_['product']->getId_produit() ?>">Local thumbnail : </label>
                                <div class="controls">
                                    <input type="text" id="local_thumbnail_<?php echo "d".$this->_['product']->getId_produit() ?>" name="local_thumbnail_<?php echo "d".$this->_['product']->getId_produit() ?>" placeholder="Local thumbnail..." value="<?php echo $this->_['product']->getLocal_thumbnail(); ?>">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row-fluid">
                            <div class="span5">
                                <ul class="thumbnails">
                                    <li class="span4">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getImage() ? "href='".$this->_['product']->getImage()."' class='gallery'" : "#" ?> ><img  src="<?php echo $this->_['product']->getImage() ?>" alt="" /></a>
                                           <h4>Image</h4>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getSmall_image() ? "href='".$this->_['product']->getSmall_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getSmall_image() ?>" alt="" /></a>
                                           <h6>Small Image</h6>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getThumbnail() ? "href='".$this->_['product']->getThumbnail()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getThumbnail() ?>" alt="" /></a>
                                           <h6>Thumbnail</h6>    
                                        </div>
                                    </li>
                                </ul> 
                            </div>
                            <div class="span5">
                                <ul class="thumbnails">
                                    <li class="span4">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getLocal_image() ? "href='".$this->_['product']->getLocal_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getLocal_image() ?>" alt="" /></a>
                                           <h4>Local Image</h4>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getLocal_small_image() ? "href='".$this->_['product']->getLocal_small_image()."' class='gallery'" : "#" ?>>
                                               <img src="<?php echo $this->_['product']->getLocal_small_image() ?>" alt="" />
                                           </a>
                                           <h6>Local Small Image</h6>    
                                        </div>
                                    </li>
                                    <li class="span3">
                                        <div class="thumbnail">
                                           <a <?php echo $this->_['product']->getLocal_thumbnail() ? "href='".$this->_['product']->getLocal_thumbnail()."' class='gallery'" : "#" ?>>
                                               <img data-lightbox="image-1" src="<?php echo $this->_['product']->getLocal_thumbnail() ?>" alt="" />
                                           </a>
                                           <h6>Local thumbnail</h6>    
                                        </div>
                                    </li>
                                </ul>                                
                            </div>
                        </div>
                                
                       
                    </div>
                    

                </div>
                
                <div class="tab-pane" id="attributs_<?php echo "d".$this->_['product']->getId_produit() ?>">
                    
<?php               if($this->_['prod_attr_list']){ ?>
                        
                        <table class="table table-bordered">
                            
                            <thead>
                                <tr>
                                    <td>Id attribut</td>
                                    <td>Nom attribut</td>
                                    <td>Code attribut</td>
                                    <td>Valeur</td>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php 
                                foreach ($this->_['prod_attr_list'] as $product_attr) {
                                    
                                    echo "<tr>";
                                    if($product_attr->getAttribut()){
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getLabel_attr()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getCode_attr()."</td>";
                                        echo "  <td>".$product_attr->getValue()."</td>";    
                                    }
                                    else{
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td><strong style='color:red'>Attribut inexistant</strong></td>";
                                        echo "  <td> - </td>";
                                        echo "  <td> - </td>";
                                    }
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
<?php               }else{ 
                        echo "<em>Pas d'attributs</em>";    
                    } ?>
                    
                </div> 
                
                <div class="tab-pane  active" id="vue_avahis_<?php echo "d".$this->_['product']->getId_produit() ?>">
                    <iframe id="iframe_avahis_<?php echo "d".$this->_['product']->getId_produit() ?>" scrolling="auto" style="width:100%; height:1400px; padding:15px" sandbox="allow-scripts allow-same-origin"  src="/app.php/getAvahisView?id=<?php echo $this->_['product']->getId_produit(); ?>" frameborder="0"></iframe>
                </div>
                
            </div> 
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="button" onClick="saveDuplicateFiche(<?php echo $this->_['product']->getId_produit() ?>); return false;" id="inputSubmit_<?php echo "d".$this->_['product']->getId_produit() ?>" name="inputSubmit_<?php echo $this->_['product']->getId_produit() ?>" class="btn btn-primary" >Créer ce produit</button>
            </div>
        </div> 
    </div>
    </form>

<?php 
}
else{ ?> 

        <br />
        <h3>Désolé, ce produit ne semble plus exister...</h3>
 
<?php
}
 ?>

<script type="text/javascript" charset="utf-8">

    
    jQuery('a.gallery').colorbox({opacity:0.5 , rel:'group1'});
    
    function productFormatResult(product) {
        var markup = "<table class='product-result'><tr>"; 
        if (product.image !== undefined ) {
            markup += "<td class='product-image'><img src='" + product.image + "'/></td>";
        }
        markup += "<td class='product-info'><div class='product-title'>" + product.text + "</div>";
        
        if (product.shortdesc !== undefined) {
            markup += "<div class='product-synopsis'>" + product.shortdesc + "</div>";
        }
        //else if (movie.synopsis !== undefined) {
          //  markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
        //}
        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.text;
    }

    $("#id").select2({
        placeholder: "Recherche rapide nom du produit / SKU ...",
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 
</script>
</script>
