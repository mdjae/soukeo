<?php $vendorManager = new ManagerVendor(); ?>


     <h2 style="font-size: 2.5em; line-height: 31px; margin-bottom: 11px;">Consultation : <?php echo htmlentities(truncateStr($this->_['product']->getName()), 40) ?></h2>   
<?php if($this->_['product']) { ?>
    <div id="product_info"  style="color: black;" class="row-fluid">
        <button onClick="modeDuplicate(<?php echo $this->_['product']->getId_produit() ?>, this);return false;" class="btn pull-right" style="margin-top:5px;" id="btnduplicate_<?php echo $this->_['product']->getId_produit() ?>">
            <i class="fa fa-copy"></i>Dupliquer
        </button>
        <button onClick="modeEdition(<?php echo $this->_['product']->getId_produit() ?>);return false;" class="btn btn-primary pull-right" style="margin-top:5px;">
            <i class="fa fa-edit"></i>Passer à l'Edition
        </button>
        <button onClick="window.open('/app.php/getAvahisViewComplete?id=<?php echo $this-> _['product']->getId_produit()?>');" class="btn btn-warning pull-right" style="margin-top:5px;">
             <i class="fa fa-eye"></i>Vue avahis
        </button>
        <button onClick="modifCatOneProd(<?php echo $this->_['product']->getId_produit() ?>); return false;" class="btn pull-right" style="margin-top:5px;">
            <i class="fa fa-sitemap"></i>Changer de catégorie
        </button>
        <br />
       <div class="tabbable tabs span6"> 
              <ul class="nav nav-tabs">
                <li class="active"><a href="#images_<?php echo $this->_['product']->getId_produit() ?>" data-toggle="tab">Images</a></li>
                <li><a href="#local_images_<?php echo $this->_['product']->getId_produit() ?>" data-toggle="tab">Images locales</a></li>
              </ul>
              <div class="tab-content">

                <div class="tab-pane active" id="images_<?php echo $this->_['product']->getId_produit() ?>"> 
                    <ul class="thumbnails">
                    	<li class="span6">
                    	    <div class="thumbnail">
                    	       <a <?php echo $this->_['product']->getImage() ? "href='".$this->_['product']->getImage()."' class='gallery'" : "#" ?> ><img  src="<?php echo $this->_['product']->getImage() ?>" alt="" /></a>
                    	       <h4>Image</h4>    
                    	    </div>
                    	</li>
                    	<li class="span3">
                    	    <div class="thumbnail">
                               <a <?php echo $this->_['product']->getSmall_image() ? "href='".$this->_['product']->getSmall_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getSmall_image() ?>" alt="" /></a>
                               <h6>Small Image</h6>    
                            </div>
                    	</li>
                    	<li class="span3">
                    	    <div class="thumbnail">
                               <a <?php echo $this->_['product']->getThumbnail() ? "href='".$this->_['product']->getThumbnail()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getThumbnail() ?>" alt="" /></a>
                               <h6>Thumbnail</h6>    
                            </div>
                    	</li>
                    </ul> 
                </div>
                
                <div class="tab-pane" id="local_images_<?php echo $this->_['product']->getId_produit() ?>">
                    <ul class="thumbnails">
                        <li class="span6">
                            <div class="thumbnail">
                               <a <?php echo $this->_['product']->getLocal_image() ? "href='".$this->_['product']->getLocal_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getLocal_image() ?>" alt="" /></a>
                               <h4>Local Image</h4>    
                            </div>
                        </li>
                        <li class="span3">
                            <div class="thumbnail">
                               <a <?php echo $this->_['product']->getLocal_small_image() ? "href='".$this->_['product']->getLocal_small_image()."' class='gallery'" : "#" ?>>
                                   <img src="<?php echo $this->_['product']->getLocal_small_image() ?>" alt="" />
                               </a>
                               <h6>Local Small Image</h6>    
                            </div>
                        </li>
                        <li class="span3">
                            <div class="thumbnail">
                               <a <?php echo $this->_['product']->getLocal_thumbnail() ? "href='".$this->_['product']->getLocal_thumbnail()."' class='gallery'" : "#" ?>>
                                   <img data-lightbox="image-1" src="<?php echo $this->_['product']->getLocal_thumbnail() ?>" alt="" />
                               </a>
                               <h6>Local thumbnail</h6>    
                            </div>
                        </li>
                    </ul>
                </div>
            </div>   
         </div>
         
         <div id="#vendor_info" class="span5">
             <p style="line-height:20px">ID : <strong><?php echo $this->_['product']->getId_produit(); ?></strong></p>
                 <p style="line-height:20px">SKU : <strong><?php echo $this->_['product']->getSku(); ?></strong></p>
                 <p style="line-height:20px">Dropship_vendor :  
                     <strong><a style="font-size:1em" <?php echo $this->_['product']->getDropship_vendor_obj() ? 'href="/app.php/clients/client?type=vendor&id='.$this->_['product']->getDropship_vendor_obj()->getEntity_id().'"' : "" ; ?>>
                         N° <?php echo $this->_['product']->getDropship_vendor() ? $this->_['product']->getDropship_vendor(). " " : "" ?><?php echo $this->_['product']->getDropship_vendor_obj() ? $this->_['product']->getDropship_vendor_obj()->getVendor_nom() : "-"; ?>
                     </a></strong>
                 </p>
                 <p style="line-height:20px">Poids : <strong><?php echo $this->_['product']->getWeight() ? $this->_['product']->getWeight()." kg" : "<span class='label label-important'>Pas de poids !</span>" ?></strong></p>
                 <p style="line-height:20px">Catégorie : <strong><?php echo $this->_['product']->getCategory() ? $this->_['product']->getCategory()->getCat_html() : "<strong style='color:red'>Catégorie inexistante</strong>" ?></strong></p> 
             <br />
             
                <p>Liste des vendeurs du produit :<br/>
                   <div id="listvendor_product">
                     <table class="table table-bordered">
                         <thead>
                             <tr>
                                 <td>Nom du vendeur</td>
                                 <td>Quantité</td>
                                 <td>Prix</td>
                             </tr>
                         </thead>
                         
                         <tbody>
    <?php 
                         if($stock_prices = $this->_['product']->getStock_prix()){
                             
                             foreach ($stock_prices as $stock_price) {
                                 $vendor = $vendorManager->getBy(array("vendor_id" => $stock_price['VENDOR_ID']));
                                 echo "<tr>";
                                 echo "<td><a href='/app.php/clients/client?type=vendor&id=".$vendor->getEntity_id()."'>".$vendor->getVendor_nom()."</a></td>";
                                 echo "<td>".$stock_price['QUANTITY']."</td>";
                                 echo "<td>".numberByLang($stock_price['PRICE_PRODUCT'])." €</td>";
                                 echo "</tr>";
                             }
                         }
    ?>
                         </tbody>
                     </table>
                   </div>
                </p>   
    <?php          if($products_gr = $this->_['product']->getProduct_grossiste()){ ?>
                     <br />
                     <br />
                     <p>Liste des produits grossistes associés :
                         <div id="listgrossiste_products">
                             <table class="table table-bordered">
                                 <thead>
                                     <tr>
                                         <td>Grossiste</td>
                                         <td>Nom</td>
                                         <td>Quantité</td>
                                         <td>Prix</td>
                                         <td>Actif</td>
                                         <td></td>                                         
                                     </tr>
                                 </thead>
                                 
                                 <tbody>
            <?php                   
                                 foreach ($products_gr as $product_gr) {
                                     
                                     echo "<tr id='productgr_".$product_gr->getId_product()."'>";
                                     echo "<td>".$product_gr->getGrossiste_nom()."</td>";
                                     echo "<td><a href='".$product_gr->getUrl()."' target='blank'>".truncateStr($product_gr->getName_product(), 40)."</a></td>";
                                     echo "<td>".$product_gr->getQuantity()."</td>";
                                     echo "<td>".numberByLang($product_gr->getPrice_product())." €</td>";
                                     echo "<td>".getLabelActiveProdGr($product_gr->getActive())."</td>";
                                     echo "<td><button onClick='supprAssocGr(".$product_gr->getId_product().", ".$this->_['product']->getId_produit()."); return false;' class='btn btn-danger'>Suppr. Assoc</button></td>";
                                     echo "</tr>";
                                 }
            ?>
                                 </tbody>
                             </table>                             
                         </div>
                     </p>
                           
    <?php          } ?>
                
             </ul>
                 
         </div>
            
    </div>
    
    <div id="product_fields" class="row-fluid">
        <div class="tabbable tabs"> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#desc_html_<?php echo $this->_['product']->getId_produit() ?>" data-toggle="tab">Description HTML</a></li>
            <li><a href="#attrs_statiques_<?php echo $this->_['product']->getId_produit() ?>" data-toggle="tab">Informations statiques</a></li>
            <li><a href="#attributs_<?php echo $this->_['product']->getId_produit() ?>" data-toggle="tab">Attributs Dynamiques</a></li>
          </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="desc_html_<?php echo $this->_['product']->getId_produit() ?>" > 
                    <iframe sandbox="allow-scripts" scrolling="auto" style="width:60%; height:900px; padding:15px" src="/app.php/getDescProd?id=<?php echo $this->_['product']->getId_produit(); ?>" frameborder="0"></iframe>
                </div>
                
                <div class="tab-pane" id="attrs_statiques_<?php echo $this->_['product']->getId_produit() ?>" > 
                    <table border="0" cellspacing="50" cellpadding="15" class="table span11" style="padding: 15px;">
                        
                        <tbody>
                            <tr>
                                <td style="width:108px"><strong>Manufacturer :</strong></td>
                                <td><?php echo $this->_['product']->getManufacturer() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Description :</strong></td>
                                <td><?php echo $this->_['product']->getDescription() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Petite description :</strong></td>
                                <td><?php echo $this->_['product']->getShort_description() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Ean :</strong></td>
                                <td><?php echo $this->_['product']->getEan() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Meta description :</strong></td>
                                <td><?php echo $this->_['product']->getMeta_description() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Meta keyword :</strong></td>
                                <td><?php echo $this->_['product']->getMeta_keyword() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Meta title :</strong></td>
                                <td><?php echo $this->_['product']->getMeta_title() ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
                <div class="tab-pane" id="attributs_<?php echo $this->_['product']->getId_produit() ?>">
                    
<?php               if($this->_['prod_attr_list']){ ?>
                        
                        <table class="table table-bordered">
                            
                            <thead>
                                <tr>
                                    <td>Id attribut</td>
                                    <td>Nom attribut</td>
                                    <td>Code attribut</td>
                                    <td>Valeur</td>
                                    <td>Actif</td>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php 
                                foreach ($this->_['prod_attr_list'] as $product_attr) {
                                    
                                    echo "<tr>";
                                    if($product_attr->getAttribut()){
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getLabel_attr()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getCode_attr()."</td>";
                                        echo "  <td>".$product_attr->getValue()."</td>";
                                        echo "  <td>".$product_attr->getActive()."</td>";
                                    }
                                    else{
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td><strong style='color:red'>Attribut inexistant</strong></td>";
                                        echo "  <td> - </td>";
                                        echo "  <td> - </td>";
                                    }
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
<?php               }else{ 
                        echo "<em>Pas d'attributs</em>";    
                    } ?>
                    
                </div>

            </div> 
        </div>
    </div>

<?php 
}
else{ ?>

        <br />
        <h3>Désolé, ce produit ne semble plus exister...</h3>
 
<?php
}
 ?>

<script type="text/javascript" charset="utf-8">
	
    jQuery('a.gallery').colorbox({opacity:0.5 , rel:'group1'});
    
    function productFormatResult(product) {
        var markup = "<table class='product-result'><tr>";
        if (product.image !== undefined ) {
            markup += "<td class='product-image'><img src='" + product.image + "'/></td>";
        }
        markup += "<td class='product-info'><div class='product-title'>" + product.text + "</div>";
        
        if (product.shortdesc !== undefined) {
            markup += "<div class='product-synopsis'>" + product.shortdesc + "</div>";
        }

        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.text;
    }

    $("#id").select2({
        placeholder: "Recherche rapide nom du produit / SKU ...",
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 
</script>
</script>
