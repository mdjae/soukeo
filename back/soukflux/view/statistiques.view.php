<?php 


$november = new DateTime('November 2013');
$interval = new DateInterval('P1M');
$now = new DateTime();
//Période de novembre 2013($november) à maintenant (new DateTime()) +3mois  avec increment +1mois 
$period   = new DatePeriod($november, $interval, $now->modify('+3 month'));

$c = 1;
$first = true; 
?>

<div class="container" id="content">
    <h3>Statistiques : <a class="pull-right btn btn-primary" href="/app.php/o_admin/statistiques/download_stats">Télécharger CSV</a></h3>

<?php

//Calcul échéances payline
$arrPayment = array(); 

foreach($period as $dt) {
    
    //logic HTML pour avoir des blocs de 2 divs par ligne (odd // even)         
    if($c & 1){ 
        if(!$first){
            echo "</div>";
        }
        echo "<div class='row-fluid'>";
    } ?>
                <div class="span6">
                    <div class="top-bar">
                        <h3>
                     <?php echo $dt->format('M Y'); ?>  
                        </h3>
                    </div>
                    
                    <div class="well">
<?php 
    $end = clone $dt;
    //Chargement des stats pour la période voulue
    $stats = new BusinessStatistiqueModel($dt->format('Y-m-d '), $end->modify('+1 month')->modify('-1 days')->format('Y-m-d ')) ;
    
    //var_dump($payline_encours = $stats->getPayline_encours());
    $payline_encours = $stats->getPayline_encours();
    if($payline_encours){
        $echeance = $payline_encours/3;
       
        $dt_clone = clone $dt;
        
        $arrPayment[$dt_clone->format('Y_M')] += $echeance;
        $arrPayment[$dt_clone->modify('+1 month')->format('Y_M')] += $echeance;
        $arrPayment[$dt_clone->modify('+1 month')->format('Y_M')] += $echeance;
    }
    
    $arrPayment[$dt->format("Y_M")] ? $echeances_mois = $arrPayment[$dt->format("Y_M")] :  $echeances_mois = 0 ;
    
    $po_av = $stats->getCount_po_av();
    $po_v = $stats->getCount_po_v();
    $po_tot = $stats->getCount_po(); 
    $orders_tot = $stats->getCount_orders();
    $qty_item_tot = $stats->getTotal_qty_items();
    $price_tot_v = $stats->getTotal_value_po_v();
    $price_tot_av = $stats->getTotal_value_po_av();
    
    echo "<strong>Statistiques :</strong>";
    echo "<br/>Nb commandes total : ".$orders_tot ;
    echo "<br/>Nb Bons de commande : ".$po_tot ;
    echo "<br/>Bons commande Avahis : ".numberByLang(($po_av / $po_tot*100))."%  (".$po_av.")" ;
    echo "<br/>Bons commande Vendeurs : ".numberByLang(($po_v / $po_tot*100))."%  (".$po_v.")"  ;
    echo "<br/>CA plateforme : ".numberByLang($stats->getCa())." €" ;
    echo "<br/>CA vendeur : ".numberByLang($stats->getCa_v())." €" ;
    echo "<br/>Panier moyen : ".numberByLang($qty_item_tot / $orders_tot)." produits/panier" ;
    echo "<br/>Valeur du panier moyen vendeur : ".numberByLang($price_tot_v / $po_v)." €" ;
    echo "<br/>Valeur du panier moyen avahis : ".numberByLang($price_tot_av / $po_av)." €" ;    
    echo "<br/>Nouveaux vendeurs : ".$stats->getCount_new_vendor() ;
    echo "<br/>Nb Paniers abandonnés : ".$stats->getCount_panier_abdn();
    echo "<br/>Total commissions : ".numberByLang($stats->getTotal_comm())." €";
    echo "<br/>Echéances payline à recevoir ce mois ci : ".numberByLang($echeances_mois). " €";
?>
                    </div>
                </div>
<?php 
    $c++;
    $first = false;    
}
?>

</div>
   