<?php $vendorManager = new ManagerVendor(); ?>


<div class="container well">

    <div class="row-fluid">
        <a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/app.php/dashboard" >Retour Tableau de bord</a>
                    <button type="button big" id="close" class="close inverse" onclick="javascript:window.history.go(-1);">&times;</button>
    </div>
        
        <div class="top-bar">
            
            <h3 style="font-size: 1.70em;line-height: 27px;">
                Nouveau produit
            </h3>
        </div>
        

    <form action="/app.php/catalogue/produit/fiche/create/valid" method="POST" accept-charset="utf-8">
    <div id="product_fields" class="row-fluid">
        <?php echo $this->_['error_message'] ?>  
        <div class="tabbable tabs"> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#attrs_statiques" data-toggle="tab">Fiche produit</a></li>
            <li><a href="#desc_html" data-toggle="tab">Description HTML</a></li>
            <li><a href="#images" data-toggle="tab">Champs image</a></li>
            <li><a href="#attributs" data-toggle="tab">Attributs</a></li>
          </ul>
            
            <div class="tab-content">
                
                <div class="tab-pane active" id="attrs_statiques" >
                    
                    
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="sku">Sku <span class="input-required">* </span>: </label>
                                    <div class="controls">
                                        <input type="text" id="sku" name="sku" placeholder="Sku unique" required onkeyup="resetTest(); return false;" value="<?php echo $_POST['sku'] ?>"/>
                                        <button style="margin-bottom: 9px;" class="btn" type="button" onClick="testSku(this); return false;">Vérifier SKU</button>
                                        <br /><span style="margin-bottom: 9px;" id="message-sku"></span>
                                    </div> 
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label" for="name">Nom <span class="input-required">* </span>: </label>
                                    <div class="controls">
                                        <input type="text" id="name" name="name" placeholder="Nom du produit" required value="<?php echo $_POST['name'] ?>"/>
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="manufacturer">Manufacturer : </label>
                                    <div class="controls">
                                        <input type="text" id="manufacturer" name="manufacturer" placeholder="Marque du produit..." value="<?php echo $_POST['manufacturer'] ?>"/>
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="weight">Poids : </label>
                                    <div class="controls">
                                        <input type="text" id="weight" name="weight" placeholder="Poids du produit..." value="<?php echo $_POST['weight'] ?>"/>
                                    </div>
                                </div>

                            </div>
                        
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="categories">Id Catégorie <span class="input-required">* </span>: </label>
                                    <div class="controls">
                                        <input type="text" id="categories" name="categories" placeholder="Id catégorie" value="<?php echo $_POST['categories'] ?>"/>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label" for="country_of_manufacture">Pays fabrication : </label>
                                    <div class="controls">
                                        <input type="text" id="country_of_manufacture" name="country_of_manufacture" placeholder="Pays de fabrication..." value="<?php echo $_POST['country_of_manufacture'] ?>"/>
                                    </div>
                                </div> 
                                
                                <div class="control-group">
                                    <label class="control-label" for="ean">Ean : </label>
                                    <div class="controls">
                                        <input type="text" id="ean" name="ean" placeholder="Ean..." value="<?php echo $_POST['ean'] ?>"/>
                                    </div>
                                </div>
        
                                <div class="control-group">
                                    <label class="control-label" for="dropship_vendor">Dropship_vendor : </label>
                                    <div class="controls">
                                        <input type="text" id="dropship_vendor" name="dropship_vendor" placeholder="Dropship_vendor..." value="<?php echo $_POST['dropship_vendor'] ?>"/>
                                    </div>
                                </div>     
                  
                            </div>
                        </div>
                        <br/><br/><br/><br/><br/><br/><br/>
                        <div class="row-fluid">                                                
                            <div class="control-group">
                                <label class="control-label" for="short_description">Petite description : </label>
                                <div class="controls">
                                    <textarea type="text" class="edit" id="short_description" name="short_description" placeholder="Petite description" ><?php echo $_POST['short_description'] ?></textarea>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="description">Description longue : </label>
                                <div class="controls">
                                    <textarea type="text" class="edit" id="description" name="description" placeholder="Description" ><?php echo $_POST['description'] ?></textarea>
                                </div>
                            </div>
                        </div>
                        <em style="color:red">* Champs obligatoires</em>
                </div>
                
                <div class="tab-pane" id="desc_html">
                        <div class="control-group">
                            <label class="control-label" for="description_html">Description HTML : </label>
                            <div class="controls">
                                <textarea type="text" id="description_html" name="description_html" placeholder="Description HTML ..."><?php echo $_POST['description_html'] ?></textarea>
                            </div>
                        </div>
                </div>
                
                <div class="tab-pane" id="images">
                    
                    <div class="row-fluid">

                        <div class="span5">
    
                            <div class="control-group">
                                <label class="control-label" for="image">Image : </label>
                                <div class="controls">
                                    <input type="text" id="image" name="image" placeholder="Image..." value="<?php echo $_POST['image'] ?>">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="small_image">Small image : </label>
                                <div class="controls">
                                    <input type="text" id="small_image" name="small_image" placeholder="Small image..." value="<?php echo $_POST['small_image'] ?>"/>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="thumbnail">Thumbnail : </label>
                                <div class="controls">
                                    <input type="text" id="thumbnail" name="thumbnail" placeholder="Thumbnail..." value="<?php echo $_POST['thumbnail'] ?>"/>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="span7">
                                           
                            <div class="control-group">
                                <label class="control-label" for="local_image">Local image : </label>
                                <div class="controls">
                                    <input type="text" id="local_image" name="local_image" placeholder="Local image..." value="<?php echo $_POST['local_image'] ?>" />
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="local_small_image">Local small image : </label>
                                <div class="controls">
                                    <input type="text" id="local_small_image" name="local_small_image" placeholder="Local small image..." value="<?php echo $_POST['local_small_image'] ?>" />
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label" for="local_thumbnail">Local thumbnail : </label>
                                <div class="controls">
                                    <input type="text" id="local_thumbnail" name="local_thumbnail" placeholder="Local thumbnail..." value="<?php echo $_POST['local_thumbnail'] ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    s
                </div>
                
                <div class="tab-pane" id="attributs">
                    

                    
                </div>
                

            </div> 
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" id="inputSubmit" name="inputSubmit" class="btn btn-primary" placeholder="Valider..." value="Valider" />
            </div>
        </div> 
    </div>
    </form>
</div>

<script type="text/javascript" charset="utf-8">

    function resetTest () {
      $("#message-sku").html("");
    }
	
	function testSku (elm) {
	    
        var sku = $("#sku").val();   
        sku=sku.replace(/^\s+|\s+$/,'');

        if(sku != ""){
            $(elm).prop("disabled", true);
            $("#message-sku").html("<img src='/skins/soukeo/loading.gif' /> Vérfif...");  
            
            var theData = {
                sku : sku
            };
            
            jQuery.ajax({
                type : 'POST',
                url : "/app.php/checkSKU",
                data : theData,
                success : function(data) {
                    
                    if(data==0){
                        $("#message-sku").html("<img border='0' alt='Ok' align='absmiddle' class='iconCircleCheck' src='/skins/common/images/picto/check.gif'><strong style='color:green'> Sku Disponible<strong>");
                        $(elm).prop("disabled", false);
                    }else{
                        $("#message-sku").html("<img border='0' alt='KO' align='absmiddle' class='iconCircleWarn' src='/skins/common/images/picto/denied.gif'><strong style='color:red'> Sku déja pris<strong>");
                        $(elm).prop("disabled", false);
                    }
                }
            });  
        }
        
	}
	/*
     $(document).ready(function(){
         
        $("#sku").change(function(){
             $("#ajax-box").html("<img src='/skins/soukeo/product-ajax-loader.gif' /> checking...");
             
 
            var sku = $("#sku").val();
            alert(sku);
            
              $.ajax({
                    type:"post",
                url:"check.php",
                data:"username="+username,
                    success:function(data){
                    if(data==0){
                        $("#message").html("<img src='tick.png' /> Username available");
                    }
                    else{
                        $("#message").html("<img src='cross.png' /> Username already taken");
                        }
                    }
                 });
 
            });
            
 
         });
         */
</script>
