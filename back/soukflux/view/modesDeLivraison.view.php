<div class="container" id="content">
    
    <div class="top-bar row-fluid">
        <h3>Modes de livraison :</h3>
    </div>
    <div class="well row-fluid">

                    
        <div id="messagebox_livraisontypes"></div>
        <div id="ref_livraisontypes">
            <?php 
            if(!empty($this -> _['typesLivraison'])){
                echo $this -> showLivraisonTypesTable($this -> _['typesLivraison']); 
            } ?>                        
        </div>

    </div>
</div>