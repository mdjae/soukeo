<?php global $root_path, $skin_path ; 
/* 
 * Ecran d'affichage des produit Avahis permettant leur edition et suppression
 *
 *
 */
isset($_SESSION['avaGP']['listProdDeleted']) ? $_SESSION['avaGP']['listProdDeleted'] = $_SESSION['avaGP']['listProdDeleted'] : $_SESSION['avaGP']['listProdDeleted'] = $this -> _['listProdDeleted'];
!isset($_SESSION['view']) ? $_SESSION['view'] = "grid" : $_SESSION['view'] = $_SESSION['view'];
!isset($_SESSION['viewAv']) ? $_SESSION['viewAv'] = "grid" : $_SESSION['viewAv'] = $_SESSION['viewAv'];
?>
<link rel="stylesheet" href="<?php echo $root_path ?>/inc/js/jstree/themes/default/style.min.css" />

<script src="<?php echo $root_path ?>/inc/js/jstree/jstree.js"></script>
<script src="<?php echo $root_path ?>/inc/js/jquery.history.js"></script>
<div id="content" class="content3 ui-corner-all">
	<div id="header">    </div>
        
	<div id="file_mgmt">    
    	<div id="treeview_avahis" class="col1 treeview">
        	<div class="intro">
				<div class="top-bar"><h3>Catalogue AVAHIS</h3></div>
				<input name="optionvalue" id="selectbox-o" class="input-xlarge" data-placeholder="Choisissez une catégorie" onchange="testValidSelect2GP()"/> 

        	</div>
        	<div class="well">
        	   <div id="jstree_categories">
                   </div>
            </div>                              
        </div>

        <div id="catalogue_avahis" class="col7 catalogue">
        	
        	<div class="top-bar" style="padding-left: 0px; ">
        	<ul class="navigation_gp tab-container">
			<?php 

				echo $this -> showNavigationAvahisGP (count($_SESSION['avaGP']['listProdDeleted']), count($_SESSION['avaGP']['selectedProds']));
			 ?>
        	</ul>
        	</div>
        	
        	
        	
        	<div class="well" id="main-content-area">
        		
			<span id="context" style="display:none"><?php echo  isset($_SESSION['avaGP']['context']) ? $_SESSION['avaGP']['context'] : "all" ?></span>
			<span id="viewTypeEcom" style="display:none"><?php echo  isset($_SESSION['avaGP']['view']) ? $_SESSION['avaGP']['view'] : "grid" ?></span>
			

				
				<div class="breadcrumb">
			<div class="inform_action">
				
				
          		<!--Ajax -->                                                                                            
				<?php 
					if(isset($_SESSION['avaGP']['catAv'])) echo $this -> showEnteteAvahisGP($_SESSION['avaGP']['catAv']);
					
				 ?>
				 </div>
            </div>
            	
			
			 <!-- affiche les filtres -->
            <div class="filter">
				<div class="filters brand_filters">
                      <?php if(isset($_SESSION['avaGP']['brandsAv']))
					  	 	echo $this -> showBrandFilterGP($_SESSION['avaGP']['brandsAv'])?>
                </div>
				<!-- filtre dynamique sur les attributs-->
				<div class="filters dyna_filters">
					<?php if(isset($_SESSION['avaGP']['attrValAv']))
				 		echo $this -> showDynaFilter($_SESSION['avaGP']['attrValAv']) ?>
               	</div>
				
				<div class="filters">
					
					<!-- les boutons de l'entéte -->
					<div>
	                    	<span class="btn btn-primary" onclick="loadProdListFilteredGP('<?php echo $_SESSION['view']?>')">Valider</span>	                  	
		                	<span class="btn" id="btnSelectAll" onclick="selectAllProductsAvGP(this); return false">Select all</span>
		             </div>
		             
		             <!-- choix grille/liste -->
					<div class="views">
						<span class="btn" onclick="loadProductListAvGP ('grid');" ><img src="<?php echo $skin_path ?>/images/picto/grid_ecom.png" >grille</span>
						<span class="btn" onclick="loadProductListAvGP ('list');" ><img src="<?php echo $skin_path ?>images/picto/list_ecom.png" >liste</span>
					</div>
		             
		              <!-- Bonton eventuel pour désupprimé tous les produits qui sont dans l'onglet supprimé
		              	<li hidden id= "btnsupAll" ><span  class="big button"  onclick="supAllSelectedProds(this); return false;">Undelete All</span></li> -->
		               	
		               
		               
		               		                
		                <!--<li>
		                	<span hiden class="big button" id="btnSelectAll" onclick="deSelectAllProductsAv(this); return false">Deselect all</span>
		                </li>-->
                  	
                   	<span class="results"></span> <!-- AJAX -->

                   <!-- <span class="results">33 résultats trouvés</span> -->
            	</div> 
        	        <!-- Ne s'affiche que sur l'onglet SELECTION -->
                <div style="display: none" class="actions">
                 <span class="btn" onclick="deSelectAllProductsAvGP(this); return false;">Deselect All</span>
				 <span class="btn btn-danger" onclick="supAllSelectedProds(this); return false;">Delete All</span>
	             <span class="btn btn-warning" onclick="ModifCatAllSelectedProds(this); return false;">ModifCat All</span>
				 <span class="btn btn-danger" onclick="supAllSelectedProds(this); return false;">Undelete All</span>
			           
                  
	              
                </div> 
           </div>
           
           
           
           
			<!-- la liste des produits -->
			<div class="row-fluid">
			<div id="prods_avahisGP" class="<?php echo $_SESSION['viewAv'].'view' ?> ">
           		<div id="prod_av_loader">
					<!-- Loader AJAX pour rechargement -->
				</div>
				
				<?php if(isset($_SESSION['ecom']['listProdAv']))
					 echo $this -> showProdListGP(array_slice($_SESSION['avaGP']['listProdAv'],$_SESSION['avaGP']['offsetav'], 9), $_SESSION['avaGP']['viewAv'], 'av', "", count($_SESSION['avaGP']['listProdAv']), $_SESSION['avaGP']["currPageAv"]);
					 //showProdListGP(array_slice($listeProduits,$limit, $nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);	
				 ?>
				 
				 
            </div>
		</div>
      
        </div>
        <!-- Fin .well -->

    </div>

</div>


</div>

<?php //echo "TEST : ".var_dump($_SESSION['avaGP']['catAvIdTree']) ; ?>
 <script type="text/javascript" charset="utf-8">
 

    $('#jstree_categories')
    .on('select_node.jstree', function (e, data) {
        var i, j, r = [];
        for(i = 0, j = data.selected.length; i < j; i++) {
            r.push(data.instance.get_node(data.selected[i]).id);
        }
        $('#event_result').html('Selected: ' + r.join(', '));
        //loadAttributsCat(r.join(', '));
        
        current_cat_id = r.join(', ');
        current_page = 0;
        
    })
    .jstree({
        'core' : {
            'data' : {
                'url' : function (node) {
                  return '/app.php/getJsonUnivers';
                },
                'dataType ' : 'json',
                'data' : function (node) {
                  
                  return { 'id' : node.id.trim() };
                }
            }
        },
      "plugins" : [
        "contextmenu", "dnd", "search",
        "state", "types", "wholerow"
      ]
    });
	$("#selectbox-o").select2({
		minimumInputLength: 3,
		ajax: {
			url: "/app.php/getlistcateg",
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
		});
		
    $(document).ready(function(){
        var History = window.History,
            State = History.getState(); 
    });
    
    if($.urlParam("id")){
        openFicheProduit($.urlParam("id")); 
    }

</script> 
	