<div id="content" class="content3">
    <div class="row-fluid well">
        <h3>Tableau de bord </h3>
        <h5>Service client</h5>
    </div>
    
    <div class="row-fluid">
        
        <div class="span9 well">
            <h4>Recherche de clients :</h4><hr/>
            <div class="tabbable tabs-left"> <!-- Only required for left/right tabs -->
              <ul class="nav nav-tabs">
                <li class="active"><a href="#customer_tab" data-toggle="tab">Acheteurs</a></li>
                <li><a href="#vendor_tab" data-toggle="tab">Vendeurs</a></li>
              </ul>
              <div class="tab-content">
                  
                <div class="tab-pane active" id="customer_tab">
                  <?php echo $this -> getMiniCustomersListTable() ?>
                </div>
                  
                <div class="tab-pane" id="vendor_tab">
                   <?php echo $this -> getMiniVendorsListTable() ?>
                </div>
              </div>
            </div>
        </div> 
        
        <div class="span3 well">
            <h4>Recherche de litiges :</h4><hr/>
        </div>
           
    </div>
    
    <div class='row-fluid'>
        
        <div class="span9 well">
            <h4>Nouvelles demandes</h4><hr/>
            <div id ='recent_ticket_list'>
                <?php echo $this -> getNewTicketListTable( $this->_['recentTicketList']) ?>
            </div>
        </div> <!-- span8 well -->
        
        <div class="span3 well">
            <h4>Mes Litiges : </h4><hr/>
        </div> <!-- span4 well -->
        
    </div> <!-- row-fluid -->
    
    <div class="row-fluid well">
        <div class="span11">
            <h4>Mes demandes observées : <button class='pull-right btn btn-primary' value='Créer' onClick='showDialogAddTicket();';>Créer ticket</button></h4>
                <div id ='ticket_list'>
            <?php 
                echo $this -> getTicketsListTable($this -> _['ticketList'], null, $this -> _['user'] -> getUserId()); ?>
                </div>
        </div>
    </div>
        
    <div class='row-fluid'>
        <div class="span12 well">
            <div class="row-fluid">
                <div class="span11">
                    <h4>Relance client</h4>
                </div>
                <div class="span1 pull-right">
                    <label class='label label-success'>  </label> : Lointain <br/>
                    <label class='label label-warning'>   </label> : Proche <br/>
                    <label class='label label-important'>   </label> : Aujourd'hui ou Dépassé
                </div>
            </div>
                <div id ='relance_ticket_list'>
            <?php 
                echo $this -> getRelanceTicketsListTable($this -> _['relanceTicketList']); ?>
                </div>
        </div> <!-- span8 well -->
    </div> <!-- row-fluid -->
</div> <!-- .content3 -->

<script type="text/javascript" charset="utf-8">
	           $('table.paginated').each(function() {
                var currentPage = 0;
                var numPerPage = 5;
                var $table = $(this);
                $table.bind('repaginate', function() {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager_wrapper = $('<div class="pagination pagination-centered"></div>');
                var $pager = $('<ul></ul>');
                for (var page = 0; page < numPages; page++) {
                    $('<li class="page-number" ></li>').html('<a href="#" onClick="return false;">'+(page + 1)+'</a>').bind('click', {
                        newPage: page
                    }, function(event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                };
                $pager_wrapper.html($pager);
                $pager_wrapper.insertBefore($table).find('li.page-number:first').addClass('active');
            });
</script>