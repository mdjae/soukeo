<?php global $root_path, $skin_path ; 


isset($_SESSION['ecom']['listProdAssoc']) ? $_SESSION['ecom']['listProdAssoc'] = $_SESSION['ecom']['listProdAssoc'] : $_SESSION['ecom']['listProdAssoc'] = $this -> _['listProdAssoc'];
isset($_SESSION['ecom']['listProdRefused']) ? $_SESSION['ecom']['listProdRefused'] = $_SESSION['ecom']['listProdRefused'] : $_SESSION['ecom']['listProdRefused'] = $this -> _['listProdRefused'];
!isset($_SESSION['ecom']['view']) ? $_SESSION['ecom']['view'] = "grid" : $_SESSION['ecom']['view'] = $_SESSION['ecom']['view'];
!isset($_SESSION['ecom']['viewAv']) ? $_SESSION['ecom']['viewAv'] = "grid" : $_SESSION['ecom']['viewAv'] = $_SESSION['ecom']['viewAv'];
if(isset($_SESSION['ecom']['correspondance'])) $correspondance = $_SESSION['ecom']['correspondance'];
if(isset($_SESSION['ecom']['cat_id_assoc'])) $cat_id_correspondant = $_SESSION['ecom']['cat_id_assoc']; 
?>

<div id="content" class="content3 ui-corner-all">
	
	<div id="header">
    </div>
    <script type="text/javascript" charset="utf-8">
        
    </script>
        
	<div id="file_mgmt">
		
        <!-- ARBRE CATEGORIE ECOMMERCANT [LEFT] -->
		<div id="treeview_commercant" class="col1 treeview">
        	<div class="intro">
        		<div class="top-bar">
					<h3>Catalogue <?php echo $this ->_['vendName'] ?> </h3>
				</div>
				<p id = "tech" style="display:none"><?php echo $_GET['tech'] ?></p>
				<p id = "id"   style="display:none"><?php echo $_GET['id'] ?></p>
				
            </div>
            <!-- Rechargé en AJAX -->
            <div class="well">
			<?php //echo $this->showcatCli( $this->_['data'], $out, "", $_SESSION['ecom']['context'], true); 
					echo $this ->_['catcli'];
			?>
			</div>
		</div>
		
		<!-- CATALOGUE ECOMMERCANT [LEFT] -->
		<div id="catalogue_commercant" class="col3 catalogue" >
			
			
			
				
				<div class="top-bar" style="padding-left: 0px; ">
				<!-- ONGLETS NAV [TABS LEFT] -->
				<ul class="navigation">
					<?php if(isset($_SESSION['ecom']['listProdAssoc']) && isset($_SESSION['ecom']['listProdRefused']))
						echo $this -> showNavigation (count($_SESSION['ecom']['listProdAssoc']), count($_SESSION['ecom']['listProdRefused']), count($_SESSION['ecom']['selectedProds']));
					 ?>
	            </ul>
				</div>
				<div class="well">
					
					<div class="breadcrumb">
				<!-- Arianne et catégorie associée -->
				<div class="inform_action">
	   				<!-- Rechargé en Ajax -->
					<?php 
						if(isset($_SESSION['ecom']['cat']))
							echo $this -> showEnteteEcommercant($_SESSION['ecom']['cat'], $_SESSION['ecom']['tech'], $_SESSION['ecom']['vendor'], $correspondance, $cat_id_correspondant);
						else { ?>
	
					<ul class="ariane"><li>Choisissez une catégorie</li></ul>
					<?php   } ?>
	            </div>
			</div>
				
			<span id="context" style="display:none"><?php echo  isset($_SESSION['ecom']['context']) ? $_SESSION['ecom']['context'] : "todo" ?></span>
			<span id="viewTypeEcom" style="display:none"><?php echo  isset($_SESSION['ecom']['view']) ? $_SESSION['ecom']['view'] : "grid" ?></span>
				
				
	            
	            
	
	            <!-- FILTRES ECOMMERCANT [LEFT] -->
				<div class="filter">
					<!-- MARQUES ECOMMERCANT-->
					<div class="filters brand_filters" >
	                      <?php if(isset($_SESSION['ecom']['brands']))
						  	 	echo $this -> showBrandFilter($_SESSION['ecom']['brands'], "ecom")?>
	                </div>
	                
	                <!-- FILTRES DYNAMIQUES ECOMMERCANT ATTR PRODS -->
					<div class="filters dyna_filters">
							<?php if(isset($_SESSION['ecom']['attrVal']))
									echo $this -> showDynaFilter($_SESSION['ecom']['attrVal']) ?>
	                </div>
	                
	                <!-- CHOIX VUE ET BOUTTONS VALIDATION RECHERCHE -->                                                                            
					<div class="filters list2">
						
							<div class='list2' >
			                   <span class="btn btn-primary" onclick="loadProdListFilteredEcom('<?php echo $_SESSION['ecom']['view']?>'); return false;">Valider</span>
			                   <span class="btn" id="btnAddAllCat" onclick="addAllProductsToAv(); return false;" style="display:none">Ajouter tout</span>
			                   <span class="btn" id="btnSelectAll" onclick="selectAllProducts(this); return false">Select all</span>
			                    
		                    </div>
		                    
		                    <div class="views">
							
							<span class="btn" onclick="loadProductList ('grid');" ><img src="<?php echo $skin_path ?>images/picto/grid_ecom.png" > grille</span>
							<span class="btn" onclick="loadProductList ('list');" ><img src="<?php echo $skin_path ?>images/picto/list_ecom.png" > liste</span>
						</div>
	                    	<span class="results"></span> <!-- AJAX -->
	                	</div>
	                
	                
	                <!-- Ne s'affiche que sur l'onglet SELECTION -->
	                <div style="display: none" class="actions">
	                  
	                  	<span class="btn" id="btnAddAllSelected" onclick="addAllProductsToAv(); return false"; style="display:none">Ajouter tout</span>
	                  	<span class="btn" onclick="addAllSelectedProductsToAv (); return false;">Ajouter tout</span>
	                  	<span class="btn" onclick="deSelectAllProducts (this); return false;">Deselect All</span>
	                  
	                  <span class="btn btn-warning" onclick="refuseAllSelectedProds(this); return false;">Refuse All</span>
	                </div>                                                                           
	   			</div> <!-- Fermeture filter -->
	            
	            <!-- PRODUITS E COMMERCANT [CONTENT LEFT] -->
	            <div class="row-fluid">
	            <div id="prods_ecommercant" class="<?php echo $_SESSION['ecom']['view'].'view' ?>" >
	            	<div id="prod_ecom_loader">
	            		<!-- Loader AJAX pour rechargement -->
	            	</div>
	            	<?php if(isset($_SESSION['ecom']['listProdEcom'])){ ?>
	            		<script type="text/javascript" charset="utf-8">
							loadProductList ('<?php echo $_SESSION['ecom']['view'] ?>');
						</script>
	            	<?php }
					 ?>
	            </div> 
	            </div>
	           </div> 
		</div> <!-- Fermeture #catalogue_commercant -->
           
           
		 <!-- ARBRE CATEGORIE AVAHIS [RIGHT] -->    
    	<div id="treeview_avahis" class="col1 treeview">
    		
    		
        	<div class="intro">
        		<div class="top-bar">
					<h3>Catalogue AVAHIS</h3>
				</div>
				<input type="hidden" name="optionvalue" id="selectbox-o" class="input-xlarge" data-placeholder="Choisissez une catégorie" onchange="testValidSelect2()"/> 
        	</div>
        	<div class="well">
        	<!-- Rechargé en AJAX -->
        	<?php echo $this -> _['catav'] ?>  
        	</div>                                                          
        </div>
		
		<!-- CATALOGUE AVAHIS [RIGHT] -->
        <div id="catalogue_avahis" class="col3 catalogue">
        	<div class="top-bar"></div>
        	
        	<div class="well">
        		<div class="breadcrumb">
			<div class="inform_action" style="padding-top: 14px;">
          		<!--Ajax -->                                                                                            
				<?php if(isset($_SESSION['ecom']['catAv']))
					 echo $this -> showEnteteAvahis($_SESSION['ecom']['catAv']);
				 ?>
            </div>
            </div>
            
            <!-- FENETRE D'ASSOCIATION [UPPER RIGHT] -->                                                                                 
            <div id="associate_products" class="interaction">
                <div id ="assoc1" class="phases">
                	<p>Vous souhaitez associer : <span class="product_name label label-info">Nom du produit</span></div>
                    <p id = "prodAssocEcom" style="display:none;"></p>
                <div id ="assoc2" class="phases">
					<p>avec le produit : <span class="product_name label label-info">Nom du produit</span></div>
                    <p id = "prodAssocAv"   style="display:none;"></p>
                <div class="phases">
                	<a href="#" class="btn btn-mini btn-primary" onclick="validAssociation()">Valider</a>
                	<a href="#" class="btn btn-mini btn-primary" onclick="cancelAssociation()">Annuler</a>
            	</div>
            		
            </div>
            
            <!-- FILTRES AVAHIS [RIGHT] -->
            <div class="filter">
            	<!-- MARQUES AVAHIS-->
				<div class="filters brand_filters">
                  <?php if(isset($_SESSION['ecom']['brandsAv']))
				  	 	echo $this -> showBrandFilter($_SESSION['ecom']['brandsAv'], "av")?>
            	</div>
            	
            	<!-- FILTRES DYNAMIQUES ATTR PRODS AVAHIS -->
				<div class="filters dyna_filters">
					<?php if(isset($_SESSION['ecom']['attrValAv']))
							echo $this -> showDynaFilter($_SESSION['ecom']['attrValAv']) ?>
           		</div>     
           		
           		<!-- CHOIX VUE AVAHIS ET BOUTTONS VALIDATION RECHERCHE -->                                                                              
				<div class="filters">
					
                    <span class="btn btn-primary" onclick="loadProdListFilteredAv('<?php echo $_SESSION['ecom']['view']?>'); return false;">Valider</span>
                    <span class="results"></span>
                    
                    <div class="views">
						<span class="btn" onclick="loadProductListAv ('grid');" ><img src="<?php echo $skin_path ?>images/picto/grid_ecom.png" >grille</span>
						<span  class="btn" onclick="loadProductListAv ('list');" ><img src="<?php echo $skin_path ?>images/picto/list_ecom.png" >liste</span>
					</div>
                    
        		</div> 
           	</div>
			
           <!-- PRODUITS AVAHIS [CONTENT RIGHT] -->
           <div class="row-fluid">
           	<div id="prods_avahis" class="<?php echo $_SESSION['ecom']['viewAv'].'view' ?>">
				<div id="prod_av_loader">
					<!-- Loader AJAX pour rechargement -->
				</div>
            	<!-- AJAX -->
            	<?php if(isset($_SESSION['ecom']['listProdAv']))
					 echo $this -> showProdList(array_slice($_SESSION['ecom']['listProdAv'],$_SESSION['ecom']['offsetav'], 9), $_SESSION['ecom']['viewAv'], 'av', "", count($_SESSION['ecom']['listProdAv']), $_SESSION['ecom']["currPageAv"]);
				 ?>
           </div>
           </div>
           </div>
        </div><!-- Fermeture #catalogue_avahis -->

    </div> <!-- Fermeture #file_mgmt -->

</div> <!-- Fermeture #content -->
<?php echo $_SESSION['ecom']['cat'] ?>
<script type="text/javascript" charset="utf-8">
	$("#selectbox-o").select2({
		minimumInputLength: 3,
		ajax: {
			url: "/app.php/getlistcateg",
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
		});

		
		if($("#cat_ecom").text() != "" && $("#cat_ecom").length > 0){

			var catecom = $("#cat_ecom").text();
			var tabid = catecom.split(">");
			var old = "";
			$.each(tabid, function(key, value){
				if (old != ""){
					value = old + " > " + value;
				}
				//alert(value);
				$("#accordion [id='"+value+"']").trigger('click');
				old = value;
			});
			//$("#accordion [id='"+catecom+"']").trigger('click');
		}else {
			$("#accordion li span:first").next().trigger('click');

		}
		<?php if(!empty($_SESSION['ecom']['cat'])){?>
				recursiveCatEcom ('<?php echo $_SESSION['ecom']['cat'] ?>', 0);
		<?php } ?>
		<?php if(!empty($_SESSION['ecom']['catAvIdTree'])){?>
				loadcat ('<?php echo $_SESSION['ecom']['catAvIdTree']?>');
		<?php } ?>
		
</script> 
	