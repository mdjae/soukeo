<?php global $root_path, $skin_path ; 


isset($_SESSION['gr']['listProdAssoc']) ? $_SESSION['gr']['listProdAssoc'] = $_SESSION['gr']['listProdAssoc'] : $_SESSION['gr']['listProdAssoc'] = $this -> _['listProdAssoc'];
isset($_SESSION['gr']['listProdRefused']) ? $_SESSION['gr']['listProdRefused'] = $_SESSION['gr']['listProdRefused'] : $_SESSION['gr']['listProdRefused'] = $this -> _['listProdRefused'];
!isset($_SESSION['gr']['view']) ? $_SESSION['gr']['view'] = "grid" : $_SESSION['gr']['view'] = $_SESSION['gr']['view'];
!isset($_SESSION['gr']['viewAv']) ? $_SESSION['gr']['viewAv'] = "grid" : $_SESSION['gr']['viewAv'] = $_SESSION['gr']['viewAv'];
if(isset($_SESSION['gr']['correspondance'])) $correspondance = $_SESSION['gr']['correspondance'];
if(isset($_SESSION['gr']['cat_id_assoc'])) $cat_id_correspondant = $_SESSION['gr']['cat_id_assoc']; 
?>

<div id="content" class="content3 ui-corner-all">
	
	<div id="header">
    </div>
    <script type="text/javascript" charset="utf-8">
        
    </script>
        
	<div id="file_mgmt">
        
		<div id="treeview_commercant" class="col1 treeview">
        	<div class="intro">
        		<div class="top-bar">
					<h3>Catalogue <?php echo $this ->_['vendName'] ?> </h3>
				</div>
				<p id = "tech" style="display:none"><?php echo $_GET['tech'] ?></p>
				<p id = "id"   style="display:none"><?php echo $_GET['id'] ?></p>
				
				<p id = "typeVendor" style="display:none"><?php echo $_SESSION['typeVendor'] ?></p>
            </div>
			
			<div class="well">
			<?php
				//echo $this->_['catcli'];
				if(isset($this->_['catcli'])) echo $this->_['catcli'] ;
				//echo $this->showcatCli( $this->_['data'], $out, "", $_SESSION['gr']['context'], true);
			?>
			</div>
		</div>
	
	<div id="catalogue_commercant" class="col3 catalogue" >
		
			<div class="top-bar" style="padding-left: 0px; ">
			<ul class="navigation tab-container">
				<?php if(isset($_SESSION['gr']['listProdAssoc']) && isset($_SESSION['gr']['listProdRefused']))
					//echo $this -> showNavigation (count($_SESSION['listProdAssoc']), count($_SESSION['listProdRefused']), count($_SESSION['selectedProds']));
				 ?>
            </ul>
			</div>
			<div class="well">
				
		<div class="breadcrumb">
			<div class="inform_action">
   				<!-- remplie par Ajax -->
				<?php 
					if(isset($_SESSION['gr']['cat']))
						echo $this -> showEnteteEcommercant($_SESSION['gr']['cat'], $_SESSION['gr']['tech'], $_SESSION['gr']['vendor'], $correspondance, $cat_id_correspondant);
					else { ?>

				<ul class="ariane"><li>Choisissez une catégorie</li></ul>
			<?php   }
				 ?>

            </div>
           </div>
				
				
		<span id="context" style="display:none"><?php echo  isset($_SESSION['gr']['context']) ? $_SESSION['gr']['context'] : "todo" ?></span>
		<span id="viewTypeEcom" style="display:none"><?php echo  isset($_SESSION['gr']['view']) ? $_SESSION['gr']['view'] : "grid" ?></span>
			
		
            
			

            
			<div class="filter">
				<div class="filters brand_filters">
                      <?php if(isset($_SESSION['gr']['brands']))
					  	 	echo $this -> showBrandFilter($_SESSION['gr']['brands'], "ecom")?>
                    	
                </div>
				<div class="filters dyna_filters">
						<?php if(isset($_SESSION['gr']['attrVal']))
								echo $this -> showDynaFilter($_SESSION['gr']['attrVal']) ?>
                </div>                                                                            
				<div class="filters list2">
					
					<div class='list2' >
			        	
                    		<span class="btn btn-primary" onclick="loadProdListFilteredEcom('<?php echo $_SESSION['gr']['view']?>'); return false;">Valider</span>
                    	
                    		<span class="btn" id="btnAddAllCat" onclick="addAllGrossisteProds();"; return false; style="display:none">Ajouter tout</span>
                    	
                    		<span class="btn" id="btnSelectAll" onclick="selectAllProducts(this); return false">Select all</span>
                    </div>	

					<div class="views">
						<span  class="btn" onclick="loadProductList ('grid');" ><img src="<?php echo $skin_path ?>images/picto/grid_ecom.png" >grille</span>
						<span class="btn"  onclick="loadProductList ('list');" ><img src="<?php echo $skin_path ?>images/picto/list_ecom.png" >liste</span>
					</div>
                    <span class="results"></span>
                </div>
                
                <div style="display: none" class="actions">
                  
                  	<span class="btn" id="btnAddAllSelected" onclick="addAllProductsToAv(); return false"; style="display:none">Ajouter tout</span>
                  	<span class="btn" onclick="addAllSelectedProductsToAv (); return false;">Ajouter tout</span>
                  	<span class="btn" onclick="deSelectAllProducts (this); return false;">Deselect All</span>
                  
                  <span class="btn btn-warning" onclick="refuseAllSelectedProds (this); return false;">Refuse All</span>
                </div>                                                                           
            </div>
            <div class="row-fluid">
            <div id="prods_ecommercant" class="<?php echo $_SESSION['gr']['view'].'view' ?>" >
            	<div id="prod_ecom_loader"></div>
            	<?php if(isset($_SESSION['gr']['listProdEcom'])){ ?>
            		<script type="text/javascript" charset="utf-8">
						loadProductList ('<?php echo $_SESSION['gr']['view'] ?>');
					</script>
            	<?php }
				 ?>

            </div>
            </div>
            </div>
</div>
            
    	<div id="treeview_avahis" class="col1 treeview">
        	<div class="intro">
        		<div class="top-bar">
					<h3>Catalogue AVAHIS</h3>
				</div>
				<input type="hidden" name="optionvalue" id="selectbox-o" class="input-xlarge" data-placeholder="Choisissez une catégorie" onchange="testValidSelect2()"/> 
				
        	</div>
        	<div class="well">
        		<?php echo $this -> _['catav'] ?>
             </div>                                                           
        </div>


        <div id="catalogue_avahis" class="col3 catalogue">
        	<div class="top-bar"></div>
        	
        	<div class="well">
        		
        	<div class="breadcrumb">	
			<div class="inform_action">
          		<!--Ajax -->                                                                                            
				<?php if(isset($_SESSION['gr']['catAv']))
					 echo $this -> showEnteteAvahis($_SESSION['gr']['catAv']);
				 ?>

            </div>
            </div>
            
                                                                                             
                <div id="associate_products" class="interaction">
                    <div id ="assoc1" class="phases">
                    	<p>Vous souhaitez associer : <span class="product_name label label-info">Nom du produit</span></div>
                        <p id = "prodAssocEcom" style="display:none"></p>
                    <div id ="assoc2" class="phases">
						<p>avec le produit : <span class="product_name label label-info">Nom du produit</span</div>
                        <p id = "prodAssocAv"   style="display:none"></p>
                    </div>
                    <div class="phases" style="text-align: center;">
                    	<a href="#" class="btn btn-mini btn-primary" onclick="validAssociation()">Valider</a>
                    	<a href="#" class="btn btn-mini btn-primary" onclick="cancelAssociation()">Annuler</a>
                	</div>
                		
                </div>
            <div class="filter">
					<div class="filters brand_filters">
                      <?php if(isset($_SESSION['gr']['brandsAv']))
					  	 	echo $this -> showBrandFilter($_SESSION['gr']['brandsAv'], "av")?>
                    	
                	</div>
                	
					<div class="filters dyna_filters">
						<?php if(isset($_SESSION['gr']['attrValAv']))
								echo $this -> showDynaFilter($_SESSION['gr']['attrValAv']) ?>
               		</div>     
               		                                                                       
					<div class="filters">
						
                        <span class="btn btn-primary" onclick="loadProdListFilteredAv('<?php echo $_SESSION['gr']['view']?>'); return false;">Valider</span>
                       
                       <div class="views">
							<span class="btn" onclick="loadProductListAv ('grid');" ><img src="<?php echo $skin_path ?>images/picto/grid_ecom.png" >grille</span>
							<span class="btn" onclick="loadProductListAv ('list');" ><img src="<?php echo $skin_path ?>images/picto/list_ecom.png" >liste</span>
						</div>
                       
                        <span class="results"></span>
            		</div> 
           </div>
			
           <div class="row-fluid">
            <div id="prods_avahis" class="<?php echo $_SESSION['gr']['viewAv'].'view' ?>">
            	<div id="prod_av_loader"></div>
            	<!-- AJAX -->
            	<?php if(isset($_SESSION['gr']['listProdAv']))
					 echo $this -> showProdList(array_slice($_SESSION['gr']['listProdAv'],$_SESSION['gr']['offsetav'], 9), $_SESSION['gr']['viewAv'], 'av', "", count($_SESSION['gr']['listProdAv']), $_SESSION['gr']["currPageAv"]);
				 ?>

            </div>
            </div>
        </div>
</div>
    </div>

</div>
<script type="text/javascript" charset="utf-8">
	linkBreadcrumps('<?php echo $_SESSION['gr']['cat']?>');

	$("#selectbox-o").select2({
		minimumInputLength: 3,
		ajax: {
			url: "/app.php/getlistcateg",
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
	});
	
	if($("#cat_ecom").text() != "" && $("#cat_ecom").length > 0){

		var catecom = $("#cat_ecom").text();
		var tabid = catecom.split(">");
		var old = "";
		$.each(tabid, function(key, value){
			if (old != ""){
				value = old + " > " + value;
			}
			//alert(value);
			$("#accordion [id='"+value+"']").trigger('click');
			old = value;
		});
		//$("#accordion [id='"+catecom+"']").trigger('click');
	}else {
		$("#accordion li span:first").next().trigger('click');

	};
	<?php if(!empty($_SESSION['gr']['cat'])){?>
				recursiveCatEcom ('<?php echo $_SESSION['gr']['cat'] ?>', 0);
		<?php } ?>
	<?php if(!empty($_SESSION['gr']['catAvIdTree'])){?>
				loadcat ('<?php echo $_SESSION['gr']['catAvIdTree']?>');
		<?php } ?>		
</script> 
	