<?php global $root_path, $skin_path ; 

?>
<link rel="stylesheet" href="<?php echo $root_path ?>/inc/js/jstree/themes/default/style.min.css" />

<script src="<?php echo $root_path ?>/inc/js/jstree/jstree.js"></script>
<script src="<?php echo $root_path ?>/inc/js/jquery.history.js"></script>
<style type="text/css" media="screen">

    #attributs-table tbody>tr:hover>td  {
        background-color: rgb(104, 121, 128);
        cursor : pointer;
        color:white;
    }
    
    #attributs-table tbody>tr.selected>td, .values_attribut_table tbody>tr.selected>td{
        background-color: #c5deb8;
    }
    
    #attributs-table tbody>tr.selected:hover>td, .values_attribut_table tbody>tr.selected:hover>td {
        background-color: #c5deb9;
        color:black;
    }
    
    #goTop{
        background: rgba(0, 0, 0, 0.5);
        color:white;
        cursor:pointer;
        padding:5px;
        position:fixed;
        top:-500px;
        right:10px;
    }
	
</style>

<div id="content" class="content3 ui-corner-all">
	<div id="header">    </div>
    <a id="goTop"><i class="fa fa-chevron-up"></i>Top</a>    
	<div id="file_mgmt">    
	    <div class="row-fluid">
        	<div id="treeview_avahis" class="span3 treeview" >
            	<div class="intro">
    				<div class="top-bar"><h3><i class="fa fa-book"></i>Catalogue AVAHIS</h3></div>
            	</div>
            	<div class="well">
            	   <div id="jstree_categories">
            	   </div>
                </div>                              
            </div>
    
            <div id="catalogue_avahis" class="span9 catalogue" >
            	
            	<div class="top-bar" style="padding-left: 0px; ">
            	   <ul class="navigation_gp tab-container">
                    <li class="active tabattribut" id="ongAll" onClick="showOngAll(); return false;"><a href="#"><i class="fa fa-table"></i>Liste attributs</a></li>
            	   </ul>
            	</div>
            	
            	
            	<div class="well" id="main-content-area">
    			     <button class="btn btn-primary pull-right" onClick="createAttr()" disabled="true"><i class="fa fa-plus"></i>Créer attribut</button>
    			    <h4 id="current_cat_label"><? echo $this ->_['cat_name'] ? $this ->_['cat_name'] : "Cliquez sur une catégorie"?> </h4>
                     
                    <div id="messagebox-categories"></div>
                    <div id="grid_attributs">
                        <div id="pagiattrlist">
                            <button class="btn btn pull-left" onClick="refreshAttrList()"><i class="fa fa-refresh"></i> Refresh</button>
                            <div class="pull-right"> 
                                <strong>Pour la sélection : </strong> <br />
                                <button class="btn btn-danger" onClick="deleteSelectedAttr()"><i class="fa fa-trash"></i> Supprimer</button>
                                <button class="btn btn-warning" onClick="showDialogFusionAttributs()"><i class="fa fa-gears"></i> Fusionner</button>
                            </div>
                             
                            <div class='pager'>
                                <div class="pull-left">
                                    <span style="font-weight: bold" id="total_attributs"></span><span> résultat(s) trouvé(s)</span></p>
                                </div> 
                                <button onclick='firstPageAttr();return false;' class='btn paging'><i class="fa fa-fast-backward"></i></button>
                                <button onclick='prevPageAttr(); return false;' class='btn paging'><i class="fa fa-backward"></i></button>
                                <span> [ <span style="font-weight: bold" id='curr_page_attr'></span> / <span style="font-weight: bold" id='total_page_attr'></span> ] </span>
                                <button onclick='nextPageAttr(); return false;' class='btn paging'><i class="fa fa-forward"></i></button>
                                <button onclick='lastPageAttr(); return false;' class='btn paging'><i class="fa fa-fast-forward"></i></button>
                                
                                Afficher : <select name="items_per_page" class="input input-mini" id="items_per_page" style="padding-left: 1px;">
                                    <option value="10" <?php if(isset($_GET['nb_items_page']) && $_GET['nb_items_page'] == 10 ) echo "selected" ?>  >10</option>
                                    <option value="20" <?php if(isset($_GET['nb_items_page']) && $_GET['nb_items_page'] == 20 ) echo "selected" ?>>20</option>
                                    <option value="50" <?php if(isset($_GET['nb_items_page']) && $_GET['nb_items_page'] == 50 ) echo "selected" ?>>50</option>
                                    <option value="100" <?php if(isset($_GET['nb_items_page']) && $_GET['nb_items_page'] == 100 ) echo "selected" ?>>100</option>
                                    <option value="150" <?php if(isset($_GET['nb_items_page']) && $_GET['nb_items_page'] == 150 ) echo "selected" ?>>150</option>
                                </select>
                            </div>
                        </div>
                        <div id="table_attributs" style="width:100%; min-height:300px">
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin .well -->
        </div>
        
    </div>
</div>


</div>
<script src="<?php echo $root_path ?>/inc/js/attributs.js"></script>

	