<?php $vendorManager = new ManagerVendor(); ?>


<div class="container well" id="ficheproduit-<?php echo $this->_['product']->getId_produit() ?>">

    <div class="row-fluid">
        <a href="http://<?php echo $_SERVER['HTTP_HOST'] ?>/app.php/dashboard" >Retour Tableau de bord</a>
                    <button type="button big" id="close" class="close inverse" onclick="javascript:window.history.go(-1);">&times;</button>
    </div>
        <div class="top-bar">
            
            <h3 style="font-size: 1.70em;line-height: 27px;">
                <a href="/app.php/catalogue/produit/fiche/edit?id=<?php echo $this->_['product'] ? $this->_['product']->getId_produit() : "" ?>" class='btn'><i class="fa fa-pencil"></i></a>
                <?php echo $this->_['product'] ? truncateStr($this-> _['product']->getName(), 55) : "-" ?>
            </h3>
            <form action="/app.php/catalogue/produit/fiche" method="get" name="fast-search" >
              
            <button type="submit" class="btn pull-right" style="margin-top:15px; margin-left:15px"><i class="fa fa-search"></i></button>
            <input type="hidden" id="id" name="id" style="width:35%;margin-top:15px" class="pull-right" onchange="this.form.submit()">
            
            </form>
        </div>
        
<?php if($this->_['product']) { ?>
    <div id="product_info"  style="color: black;" class="row-fluid">
        <button onClick="window.open('/app.php/getAvahisView?id=<?php echo $this-> _['product']->getId_produit()?>');" class="btn btn-primary pull-right" style="margin-top:5px;">Vue avahis</button>
        <br />
       <div class="tabbable tabs span6"> 
              <ul class="nav nav-tabs">
                <li class="active"><a href="#images" data-toggle="tab">Images</a></li>
                <li><a href="#local_images" data-toggle="tab">Images locales</a></li>
              </ul>
              <div class="tab-content">

                <div class="tab-pane active" id="images"> 
                    <ul class="thumbnails">
                    	<li class="span6">
                    	    <div class="thumbnail">
                    	       <a <?php echo $this->_['product']->getImage() ? "href='".$this->_['product']->getImage()."' class='gallery'" : "#" ?> ><img  src="<?php echo $this->_['product']->getImage() ?>" alt="" /></a>
                    	       <h4>Image</h4>    
                    	    </div>
                    	</li>
                    	<li class="span3">
                    	    <div class="thumbnail">
                               <a <?php echo $this->_['product']->getSmall_image() ? "href='".$this->_['product']->getSmall_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getSmall_image() ?>" alt="" /></a>
                               <h6>Small Image</h6>    
                            </div>
                    	</li>
                    	<li class="span3">
                    	    <div class="thumbnail">
                               <a <?php echo $this->_['product']->getThumbnail() ? "href='".$this->_['product']->getThumbnail()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getThumbnail() ?>" alt="" /></a>
                               <h6>Thumbnail</h6>    
                            </div>
                    	</li>
                    </ul> 
                </div>
                
                <div class="tab-pane" id="local_images">
                    <ul class="thumbnails">
                        <li class="span6">
                            <div class="thumbnail">
                               <a <?php echo $this->_['product']->getLocal_image() ? "href='".$this->_['product']->getLocal_image()."' class='gallery'" : "#" ?>><img src="<?php echo $this->_['product']->getLocal_image() ?>" alt="" /></a>
                               <h4>Local Image</h4>    
                            </div>
                        </li>
                        <li class="span3">
                            <div class="thumbnail">
                               <a <?php echo $this->_['product']->getLocal_small_image() ? "href='".$this->_['product']->getLocal_small_image()."' class='gallery'" : "#" ?>>
                                   <img src="<?php echo $this->_['product']->getLocal_small_image() ?>" alt="" />
                               </a>
                               <h6>Local Small Image</h6>    
                            </div>
                        </li>
                        <li class="span3">
                            <div class="thumbnail">
                               <a <?php echo $this->_['product']->getLocal_thumbnail() ? "href='".$this->_['product']->getLocal_thumbnail()."' class='gallery'" : "#" ?>>
                                   <img data-lightbox="image-1" src="<?php echo $this->_['product']->getLocal_thumbnail() ?>" alt="" />
                               </a>
                               <h6>Local thumbnail</h6>    
                            </div>
                        </li>
                    </ul>
                </div>
            </div>   
         </div>
         
         <div id="#vendor_info" class="span5">
             <p style="line-height:20px">ID : <strong><?php echo $this->_['product']->getId_produit(); ?></strong></p>
                 <p style="line-height:20px">SKU : <strong><?php echo $this->_['product']->getSku(); ?></strong></p>
                 <p style="line-height:20px">Dropship_vendor :  
                     <strong><a style="font-size:1em" <?php echo $this->_['product']->getDropship_vendor_obj() ? 'href="/app.php/clients/client?type=vendor&id='.$this->_['product']->getDropship_vendor_obj()->getEntity_id().'"' : "" ; ?>>
                         N° <?php echo $this->_['product']->getDropship_vendor() ? $this->_['product']->getDropship_vendor(). " " : "" ?><?php echo $this->_['product']->getDropship_vendor_obj() ? $this->_['product']->getDropship_vendor_obj()->getVendor_nom() : "-"; ?>
                     </a></strong>
                 </p>
                 <p style="line-height:20px">Poids : <strong><?php echo $this->_['product']->getWeight() ? $this->_['product']->getWeight()." kg" : "<span class='label label-important'>Pas de poids !</span>" ?></strong></p>
                 <p style="line-height:20px">Catégorie : <strong><?php echo $this->_['product']->getCategory() ? $this->_['product']->getCategory()->getCat_html() : "<strong style='color:red'>Catégorie inexistante</strong>" ?></strong></p> 
             <br />
             
                <p>Liste des vendeurs du produit :<br/>
                   <div id="listvendor_product">
                     <table class="table table-bordered">
                         <thead>
                             <tr>
                                 <td>Nom du vendeur</td>
                                 <td>Quantité</td>
                                 <td>Prix</td>
                             </tr>
                         </thead>
                         
                         <tbody>
    <?php 
                         if($stock_prices = $this->_['product']->getStock_prix()){
                             
                             foreach ($stock_prices as $stock_price) {
                                 $vendor = $vendorManager->getBy(array("vendor_id" => $stock_price['VENDOR_ID']));
                                 echo "<tr>";
                                 echo "<td><a href='/app.php/clients/client?type=vendor&id=".$vendor->getEntity_id()."'>".$vendor->getVendor_nom()."</a></td>";
                                 echo "<td>".$stock_price['QUANTITY']."</td>";
                                 echo "<td>".numberByLang($stock_price['PRICE_PRODUCT'])." €</td>";
                                 echo "</tr>";
                             }
                         }
    ?>
                         </tbody>
                     </table>
                   </div>
                </p>   
    <?php          if($products_gr = $this->_['product']->getProduct_grossiste()){ ?>
                     <br />
                     <br />
                     <p>Liste des produits grossistes associés :
                         <div id="listgrossiste_products">
                             <table class="table table-bordered">
                                 <thead>
                                     <tr>
                                         <td>Grossiste</td>
                                         <td>Quantité</td>
                                         <td>Prix</td>
                                         <td>Actif</td>
                                     </tr>
                                 </thead>
                                 
                                 <tbody>
            <?php                   
                                 foreach ($products_gr as $product_gr) {
                                     
                                     echo "<tr>";
                                     echo "<td>".$product_gr->getGrossiste_nom()."</td>";
                                     echo "<td>".$product_gr->getQuantity()."</td>";
                                     echo "<td>".numberByLang($product_gr->getPrice_product())." €</td>";
                                     echo "<td>".getLabelActiveProdGr($product_gr->getActive())."</td>";
                                     echo "</tr>";
                                 }
            ?>
                                 </tbody>
                             </table>                             
                         </div>
                     </p>
                           
    <?php          } ?>
                
             </ul>
                 
         </div>
            
    </div>
    
    <div id="product_fields" class="row-fluid">
        <div class="tabbable tabs"> 
          <ul class="nav nav-tabs">
            <li class="active"><a href="#desc_html" data-toggle="tab">Description HTML</a></li>
            <li><a href="#attrs_statiques" data-toggle="tab">Informations statiques</a></li>
            <li><a href="#attributs" data-toggle="tab">Attributs Dynamiques</a></li>
            <li><a href="#desc_html_2" data-toggle="tab">Description HTML</a></li>
          </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="desc_html" style="padding: 15px;"> 
                    <?php echo $this->_['product']->getDescription_html() ? $this->_['product']->getDescription_html() : "<em>Pas de description HTML</em>"?>
                </div>
                
                <div class="tab-pane" id="attrs_statiques" > 
                    <table border="0" cellspacing="50" cellpadding="15" class="table span11" style="padding: 15px;">
                        
                        <tbody>
                            <tr>
                                <td style="width:108px"><strong>Manufacturer :</strong></td>
                                <td><?php echo $this->_['product']->getManufacturer() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Description :</strong></td>
                                <td><?php echo $this->_['product']->getDescription() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Petite description :</strong></td>
                                <td><?php echo $this->_['product']->getShort_description() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Ean :</strong></td>
                                <td><?php echo $this->_['product']->getEan() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Meta description :</strong></td>
                                <td><?php echo $this->_['product']->getMeta_description() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Meta keyword :</strong></td>
                                <td><?php echo $this->_['product']->getMeta_keyword() ?></td>
                            </tr>
                            <tr>
                                <td><strong>Meta title :</strong></td>
                                <td><?php echo $this->_['product']->getMeta_title() ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
    
                <div class="tab-pane" id="attributs">
                    
<?php               if($this->_['prod_attr_list']){ ?>
                        
                        <table class="table table-bordered">
                            
                            <thead>
                                <tr>
                                    <td>Id attribut</td>
                                    <td>Nom attribut</td>
                                    <td>Code attribut</td>
                                    <td>Valeur</td>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php 
                                foreach ($this->_['prod_attr_list'] as $product_attr) {
                                    
                                    echo "<tr>";
                                    if($product_attr->getAttribut()){
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getLabel_attr()."</td>";
                                        echo "  <td>".$product_attr->getAttribut()->getCode_attr()."</td>";
                                        echo "  <td>".$product_attr->getValue()."</td>";    
                                    }
                                    else{
                                        echo "  <td>".$product_attr->getId_attribut()."</td>";
                                        echo "  <td><strong style='color:red'>Attribut inexistant</strong></td>";
                                        echo "  <td> - </td>";
                                        echo "  <td> - </td>";
                                    }
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
<?php               }else{ 
                        echo "<em>Pas d'attributs</em>";    
                    } ?>
                    
                </div>
                
               <div class="tab-pane" id="desc_html_2" style="height:auto">
                    <iframe sandbox="allow-scripts" scrolling="auto" style="background:white; width:100%; height:1900px; padding:15px" src="/app.php/getAvahisView?id=<?php echo $this->_['product']->getId_produit(); ?>" frameborder="0"></iframe>
                </div> 
            </div> 
        </div>
    </div>
</div>
<?php 
}
else{ ?>
    <div id="product_info"  style="color: black;" class="row-fluid">
        <br />
        <h3>Désolé, ce produit ne semble plus exister...</h3>
    </div> 
<?php
}
 ?>

<script type="text/javascript" charset="utf-8">
	
    jQuery('a.gallery').colorbox({opacity:0.5 , rel:'group1'});
    
    function productFormatResult(product) {
        var markup = "<table class='product-result'><tr>";
        if (product.image !== undefined ) {
            markup += "<td class='product-image'><img src='" + product.image + "'/></td>";
        }
        markup += "<td class='product-info'><div class='product-title'>" + product.text + "</div>";
        
        if (product.shortdesc !== undefined) {
            markup += "<div class='product-synopsis'>" + product.shortdesc + "</div>";
        }
        //else if (movie.synopsis !== undefined) {
          //  markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
        //}
        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.text;
    }

    $("#id").select2({
        placeholder: "Recherche rapide nom du produit / SKU ...",
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 
</script>
</script>
