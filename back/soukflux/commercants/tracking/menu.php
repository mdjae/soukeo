﻿<?php

$vendor_id   = new FilterText("vendor_id", ("vendor_id de l'e-commerçant")." :", "", false, 12);
$fdate1 = new FilterText("date1", ("<b>Date de début :</b><br/>(jj/mm/aaaa) "), "", false, 10);
$fdate2 = new FilterText("date2", ("<b>Date de fin :</b><br/>(jj/mm/aaaa) "), "", false, 10);

$fSoft = new FilterList("fSoft", ("SoftWare :"),"all");
$sql = " SELECT DISTINCT SOFTWARE FROM  sfk_tracking_vendor ";
$rs = dbQueryAll($sql);  
$fSoft->addElement('all','all');
//$fSoft->addElement('aucun','aucun');

foreach( $rs as $R){
    $fSoft->addElement($R['SOFTWARE'],$R['SOFTWARE'] );
}
$fActive = new FilterList("fActive", ("Active :"),"1");
$fActive->addElement('all','all');
$fActive->addElement('1','actif');
$fActive->addElement('0','inactif');

$filter = new Filter("tracking");
$filter->addFilter($vendor_id);
$filter->addFilter($fdate1);
$filter->addFilter($fdate2);
$filter->addFilter($fSoft);

$filter->setGridToReload("tracking");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(
         /*
		  * array
          (
          		"access" => "qualification.tracking..",
                "picto" => "add",
                "name" =>("Ajouter un vendeur"),
                "url" => "/qualification/tracking/popup_add/index.php",
                "width" => 650,
                "height" => 600,
                "popup" => "jdiag"
           ),
          */ 
          array
          (
                "access" => "commercants.tracking..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.tracking..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),

           array
           (
          		"access" => "commercants.tracking..",
                "picto" => "denied",
                "name" =>("Suprimme le/les tracking(s)"),
                "url" => "javascript: deleteTracking()",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            ), 
			array
           (
          		"access" => "commercants.tracking..",
                "picto" => "add",
                "name" =>("Ajout URL tracking au(x) Vendeur(s)"),
                "url" => "javascript: addTrackingVendor()",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            )  
  );
  echo drawRMenuAction2(("Actions"),$act_array);

//legende
 $expl_array = array(
//				array("","iconCircleCheck","check",("Vendeur actif")),
//				array("","iconCircleWarn","denied",("Vendeur inactif")), 				
				array("commercants.tracking..","","search",("Afficher les information pour cette e-commerçant")),
				//array("qualification.tracking..","","grids_catalog",("Afficher liste des produits pour cet e-commerçant")),
				//array("qualification.tracking..","","edit",("Ecran d'association des produits e-commerçant et Avahis)"))
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);

 
