<?php 
$ACCESS_key ="qualification.tracking..";
$security_domain = "qualification";
$domain_categ = "tracking";
$right_menu_visibility = true;

// init step of SQL limit
$step_limit = SystemParams::getParam('system*nlist_element');
//--------------------------------- a transféré dans js.js---------------
?>
<script>

	function addTrackingVendor() {
		var premier = true;
		//var url = "popup_actif/actif?vids=";
		var url = "";
		var aInput = document.getElementsByTagName('input');
		//var alert(aInput[0]);
		for (var i = 0; i < aInput.length ; i++)
		{
			if (aInput[i].type == 'checkbox' && aInput[i].id != '')
			{
				if (aInput[i].checked)
				{
					if (premier)
					{
						premier = false;
						//alert(aInput[i].id);
						url += aInput[i].id.substr(4);
					}
					else
					{
						//alert(aInput[i].id);
						url += "|" + aInput[i].id.substr(4);
					}
				}
			}
		}
	
		if (!premier)
		{

			var theData = {
	    		vids : url
	    	} ;
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/addTrackingVendor",
				data : theData,
				success : function(data) {
					
				$('#dialog').html(data);
				$("#dialog").dialog("open");
				
				}
			}); 
		}
		
	}

	function deleteTracking() {
		var premier = true;
		var url = "";
		var aInput = document.getElementsByTagName('input');
		for (var i = 0; i < aInput.length ; i++)
		{
			if (aInput[i].type == 'checkbox' && aInput[i].id != '')
			{
				if (aInput[i].checked)
				{
					if (premier)
					{
						premier = false;
						url += aInput[i].id.substr(4);
					}
					else
					{
						url += "|" + aInput[i].id.substr(4);
					}
				}
			}
		}
	
		if (!premier)
		{

			var theData = {
	    		vids : url
	    	} ;
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/deleteTracking",
				data : theData,
				success : function(data) {
					
				$('#dialog').html(data);
				$("#dialog").dialog("open");
				
				}
			}); 
		}
		
	}
	function openInfoVendor(vendor_id){
		var theData = {
			idVendor : vendor_id
			} ;
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/popup_infovend",
				data : theData,
				success : function(data) {
				$('#dialog').html(data);
				$("#dialog").dialog("open");
				}
			}); 
		
	}
	
</script>
<!------------------------------------- fin script ------------------------------------- -->
<?php
/*$query = "	SELECT v.VENDOR_ID, v.VENDOR_NOM, tv.SOFTWARE, tv.DATE_INSERT, v.VENDOR_URL 
			FROM `sfk_vendor` as v 
			INNER JOIN `sfk_tracking_vendor` as tv 
				ON v.VENDOR_ID = tv.VENDOR_ID 
			ORDER BY v.VENDOR_ID
		";*/
		
/*		
$query = "SELECT v.VENDOR_ID, v.VENDOR_NOM, tv.SOFTWARE, tv.DATE_INSERT, v.VENDOR_URL ,
(SELECT count(*) FROM sfk_produit_ps as ps 
WHERE ps.VENDOR_ID = v.VENDOR_ID 
 ) as NBPRODS_Prestashop, 
(SELECT count(*) FROM sfk_produit_th as th 
WHERE th.VENDOR_ID = v.VENDOR_ID 
 ) as NBPRODS_Thelia, 
(SELECT count(*) FROM sfk_produit_vm as vm 
WHERE vm.VENDOR_ID = v.VENDOR_ID 
 ) as NBPRODS_Virtuemart
			FROM `sfk_vendor` as v 
			INNER JOIN `sfk_tracking_vendor` as tv 
				ON v.VENDOR_ID = tv.VENDOR_ID 
			ORDER BY v.VENDOR_ID";
*/
$grid = new TrackingGrid();

?>