<?php

$numvendor  = new FilterText("numvendor", ("N° Vendeur")." :", $_GET['id'], false, 12);

// RECHERCHE FILTER CATEGORY
$commfilter = new FilterList("commfilter", ("Choix du commerçant :"), "all");
$sql = " SELECT DISTINCT po.vendor_id, v.VENDOR_NOM FROM  skf_po as po INNER JOIN
         sfk_vendor v ON v.VENDOR_ID = po.vendor_id 
         WHERE po.vendor_id <> 24  
         ORDER BY v.VENDOR_NOM ASC";
$rs = dbQueryAll($sql);  
$commfilter->addElement('all','all');

foreach( $rs as $R){
    $commfilter->addElement($R['vendor_id'],$R['VENDOR_NOM'] );
}

//Date
$date = new FilterList("datefilter", ("Date :"),"next");
$date->addElement('all','Depuis le début');
$date->addElement('next','Prochain paiement');


$filter = new Filter("facturationcommercants");
$filter->addFilter($commfilter);
$filter->addFilter($numvendor);
$filter->addFilter($date);

$filter->setGridToReload("facturationcommercants");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "commercants.facturation..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.facturation..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.facturation..",
                "picto" => "stop",
                "name" =>("Generer les factures pour la selection"),
                "url" => "javascript: saveFactuAllCommercant();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(			
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>