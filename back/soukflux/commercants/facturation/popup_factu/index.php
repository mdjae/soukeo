<?php
$skf = new BusinessSoukeoModel();

$vendorManager = new ManagerVendor();

$vendor = $vendorManager -> getBy (array("vendor_id" => $_POST['vendor_id']));

$date = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
$date = $date['MIN(date_value)'];
 
$poList = $vendor -> getAllPoToPay();

?>
<style type="text/css" media="screen">
	.total {
	    font-weight:bold;
	}
</style>
<div class="well">
	<div class="row-fluid">
		<div class="span6">
			<h4><?php echo $vendor -> getVendor_nom() ?></h4>	
			<div>	
				<?php echo $vendor -> getStreet() ?><br />
				<?php echo $vendor -> getCity() ?><br />
				<?php echo $vendor -> getZip() ?><br />
				<?php echo $vendor -> getCountry_id() ?><br />
			</div>
			<div>Tel : <?php echo $vendor -> getTelephone() ?></div>		
		</div>
		<div class="span6">
			<h4>SOUKEO</h4>	
			<div>	
				Rue Pasteur<br />
				St Denis<br />
				97490<br />
				RE<br />
			</div>
			<div>Tel : <?php echo $vendor -> getTelephone() ?></div>
		</div>
	</div>
	
	<div class="row-fluid">
		<table class="table table-bordered">
			<thead>
				<tr>
					<td>Commande</td>
					<td>Produit</td>
					<td>% Comm</td>
					<td>Prix</td>
					<td>MT Commission</td>
					<td>MT Frais banquaires</td>
					<td></td>
				</tr>
			</thead>
			
			<tbody id="lines">
		
	<?php 
		$total_prix = 0;
		$total_comm = 0;
		$total_frais_b = 0;
		$grand_total = 0;
		foreach ($poList as $po) {
			
			$itemList = $po -> getAllItems();
			foreach ($itemList as $item) {
				$comm = dbQueryOne('SELECT COMM FROM sfk_categorie WHERE cat_id = '.$item -> getCategory_id());
				$comm = $comm['COMM'];
				if(!$item -> getLitiges() && !$item -> getPayment()){
					$total_prix += $item -> getBase_row_total_incl_tax();
					$total_comm += $item -> getBase_row_total_incl_tax() * $comm;
					$total_frais_b += $item -> getBase_row_total_incl_tax() * 0.005;
					$grand_total += $total_comm + $total_frais_b;
	?>
				<tr>
					<td><?php echo $po -> getIncrement_id() ?></td>
					<td><?php echo $item -> getName() ?></td>
					<td><?php echo $comm ?></td>
					<td class="line_prix"><?php echo numberByLang($item -> getBase_row_total_incl_tax()) ?> €</td>
					<td class="line_comm"><?php echo numberByLang($item -> getBase_row_total_incl_tax() * $comm) ?>  €</td>
					<td class="line_fraisb"><?php echo numberByLang($item -> getBase_row_total_incl_tax() * 0.005) ?>  €</td>
					<td><input type="checkbox" checked onChange="calculPopupFactu()" id="item_<?php echo $item -> getItem_id()?>"/></td>
				</tr>
	<?php		}elseif(!$item -> getPayment()){
	?>
				<tr>
					<td style="color: red"><?php echo $po -> getIncrement_id() ?></td>
					<td style="color: red"><?php echo $item -> getName() ?></td>
					<td style="color: red"><?php echo $comm ?></td>
					<td style="color: red">0 €</td>
					<td style="color: red">0 €</td>
					<td style="color: red">0 €</td>
					<td><input type="checkbox" checked onChange="calculPopupFactu()" id="item_<?php echo $item -> getItem_id()?>"/></td>
				</tr>	
	<?php
	}
			}
            if($po->isShippingRefund() &&  (float)$po->getBase_shipping_amount() > 0){
                                                 
                $total_prix += (float)$po->getBase_shipping_amount();
                ?>
                
                <tr>
                    <td><?php echo $po -> getIncrement_id() ?></td>
                    <td><?php echo "Remboursement frais d'envoi ".numberByLang($po->getBase_shipping_amount())." €"; ?></td>
                    <td></td>
                    <td class="line_prix"><?php echo numberByLang($po->getBase_shipping_amount()) ?> €</td>
                    <td class="line_comm"></td>
                    <td class="line_fraisb"></td>
                    <td><input type="checkbox" checked onChange="calculPopupFactu()" id="item_<?php echo $po->getIncrement_id() ?>"/></td>
                </tr>


          <?php                   

            }
		} 
	?>
			
			<tr>
				<td></td>
				<td><h5>Total HT:</h5></td>
				<td></td>
				<td id="total_HT" class="total"><?php echo numberByLang($total_prix) ?> €</td>
				<td id="total_comm_HT" class="total"><?php echo numberByLang($total_comm) ?> €</td>
				<td id="total_fraisb_HT" class="total"><?php echo numberByLang($total_frais_b) ?> €</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><h5>Total TTC:</h5></td>
				<td></td>
				<td id="total_TTC" class="total"><strong><?php echo numberByLang($total_prix *1.085) ?> €</strong></td>
				<td id="total_comm_TTC" class="total"><?php echo numberByLang($total_comm *1.085) ?> €</td>
				<td id="total_fraisb_TTC" class="total"><?php echo numberByLang($total_frais_b* 1.085) ?> €</td>
				<td></td>
			</tr>
			</tbody>
		</table>

	</div>
	<em style="color: red">* Produits en litiges</em>
	<div class="row-fluid">
		<h4>Ajout de ligne :</h4>
		<span class="input-prepend ">
  			<input id="addligne_designation" class="span3 input-large" id="appendedPrependedInput" type="text" placeholder="Désignation...">
		</span>
		<span class="input-append">
  			<input id="addligne_prix" class="span2" id="appendedPrependedInput" type="text" placeholder="Prix..">
  			<span class="add-on"> € </span>
		</span>
		<button class="btn btn-primary" onClick= "addLineFactu()">Ajouter Ligne</button>
	</div>
</div>
<button class="btn btn-primary" onClick= "saveFactuCommercant(<?php echo $_POST['vendor_id'] ?>)">Enregistrer Facture</button>