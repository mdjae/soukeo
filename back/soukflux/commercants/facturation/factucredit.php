<style type="text/Css">
<!--
.test1
{
    border: solid 1px #FF0000;
    background: #FFFFFF;
    border-collapse: collapse;
}
-->
</style>
<page style="font-size: 11px ">
	

	<table border="0" cellspacing="5" cellpadding="5">
		<tr>
			<td width=650>Soukéo SAS -Market place Avahis.com Facture N° <?php echo $facture -> getFacture_code() ?></td>
			<td>Page 1/1</td>
		</tr>
		
	</table>	
	<hr /><br />
	<table >
		<tr>
			<td width=400>
			<div >
				<img src="skins/soukeo/logo_300x150.jpg" alt="Logo" width=180 /></div></td>
			<td>Facture N° <?php echo $facture -> getFacture_code() ?><br/>
				En date du <?php echo smartDateNoTime($facture -> getDate_created()) ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo $emeteur -> getStreet() ?> <br />
				<?php echo $emeteur -> getZip_code() ?> - <?php echo $emeteur -> getCity() ?> <br />
				Réunion <br />
				SIRET : <?php echo $emeteur -> getSiret() ?>
			</td>
			<td style="text-align: center; border: solid 2px black">
				<?php echo $recipient->getName() ?  $recipient->getName() : "" ?> <br />
				SIRET : <?php echo $recipient->getSiret() ?><br />
				<?php echo $recipient -> getStreet() ?> <br />
				<?php echo $recipient -> getZip_code() ?> <?php echo $recipient -> getCity() ?>  
			</td>
		</tr>
		<tr>	
			<td>
				Email : <?php echo $factureCredit -> getEmail() ?><br />
				Tel : <?php echo $factureCredit -> getTel()  ?><br />
			</td>
			<td></td>
		</tr>
	</table>
	<br />
	<div style="text-align: center;"><h5>Récapitulatif des achats :</h5></div>
	<table border=1 >
		<tr >
			<td width=60>Numéro de <br />pack</td>
			<td width=360>Désignation</td>
			<td >Quantité</td>
			<td >Valeur totale <br />des produits <br />HT</td>
			<td >Valeur totale <br />des produits <br />TTC</td>
		</tr>
<?php 
		$atLeastOneLitige = false;
		foreach ($lines as $line) { 
?>
		<tr>
			<td><?php echo $line -> getPack_id() ?></td>
			<td><?php echo truncateStr($line -> getDesignation(), 65) ?></td>
			<td><?php echo $line -> getQty() ?></td>
			<td style="text-align: right;"><?php echo numberByLang($line -> getPrice_ht()) ?> €</td>
			<td style="text-align: right;"><?php echo numberByLang($line -> getPrice_ttc()) ?> €</td>
		</tr>
<?php 	 
	  } ?>
		<tr>
			<td></td>
			<td><strong>Total</strong></td>
			<td></td>
			<td style="text-align: right;"><strong><?php echo numberByLang($factureCredit -> getTotal_prix()) ?> €</strong></td>
			<td style="text-align: right;"><strong><?php echo numberByLang($factureCredit -> getGrand_total()) ?> €</strong></td>
			
		</tr>
	</table>
	
	<table border="0" cellspacing="5" cellpadding="5">
		<tr>
			<td width=300><h4>Somme à payer par <?php echo $paymentType ? $paymentType -> getLabel() : "" ?></h4></td>
			<td width=150> </td>
			<td></td>	
		</tr>
		<tr>
			<td style="text-align: center; border: solid 2px black"><strong><h5><?php echo numberByLang($factureCredit -> getGrand_total()) ?> €</h5></strong></td>
			<td></td>
			<td><strong>
				<table border="0" cellspacing="5" cellpadding="5">
					<tr >
						<td style="border-top: solid 3px; border-bottom: solid 3px;" width=100><h5>Total <br />
							Avahis.com</h5>
						</td > 
						<td style="border-top: solid 3px; border-bottom: solid 3px;"><h5><?php echo numberByLang($factureCredit -> getGrand_total()) ?> €</h5></td>
					</tr>
				</table>		
				</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center; border: solid 2px black; background:black; color:red">
				En cas de litige déclaré et non résolu, <br />
				le paiment du produit est suspendu jusqu'à <br />
				sa résolution.
			</td>
			<td></td>
			<td>
				<table border="0" cellspacing="5" cellpadding="5">
					<tr>
						<td width=100>TVA 8.5 %
						</td>
						<td style="text-align: right;"><?php echo numberByLang($factureCredit -> getTotal_prix() * 0.085) ?> €</td>
					</tr>
					<tr>
						<td>Total HT</td>
						<td style="text-align: right;"><?php echo numberByLang($factureCredit -> getTotal_prix()) ?> €</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			
		</tr>
	</table>
    
    <p>Moyen du règlement : <?php echo $paymentType ? $paymentType -> getLabel() : "" ?></p>
    
    <p>Délais de règlement : immédiat <br />
       Date limite du règlement : date d'édition du récapitulatif de commande <br />
       En cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement <br />
       de 40€ selon l'article D. 441-5 du code du commerce
    </p>

</page>