<?php 
$ACCESS_key ="commercants.ecommercant..";
$security_domain = "commercants";
$domain_categ = "ecommercant";
$right_menu_visibility = true;

// init step of SQL limit
$step_limit = SystemParams::getParam('system*nlist_element');
//--------------------------------- a transféré dans js.js---------------
?>
<script>
	function actifVendor() {
		var premier = true;
		var url = "";
		var aInput = document.getElementsByTagName('input');
		for (var i = 0; i < aInput.length ; i++)
		{
			if (aInput[i].type == 'checkbox' && aInput[i].id != '')
			{
				if (aInput[i].checked)
				{
					if (premier)
					{
						premier = false;
						url += aInput[i].id.substr(4,11);
					}
					else
					{
						url += "|" + aInput[i].id.substr(4,11);
					}
				}
			}
		}
	
		if (!premier)
		{
			
			var theData = {
	    		vids : url
	    	} ;
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/actifVendor",
				data : theData,
				success : function(data) {
					
				$('#dialog').html(data);
				//$("#dialog").dialog("open");
				
				}
			}); 
			 //location.reload();
		}
		
	}
	function openInfoVendor(vendor_id){
		var theData = {
			idVendor : vendor_id
			} ;
		//alert (theData);
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/popup_infovend",
				data : theData,
				success : function(data) {
					
				$('#dialog').html(data);
				$("#dialog").dialog("open");
				
				}
			}); 
		
	}

	function inactifVendor() {
		var premier = true;
		//var url = "popup_inactif/index.php?vids=";
		var url = "";
		var aInput = document.getElementsByTagName('input');
		for (var i = 0; i < aInput.length ; i++)
		{
			if (aInput[i].type == 'checkbox' && aInput[i].id != '')
			{
				if (aInput[i].checked)
				{
					if (premier)
					{
						premier = false;
						url += aInput[i].id.substr(4,11);
					}
					else
					{
						url += "|" + aInput[i].id.substr(4,11);
					}
				}
			}
		}
	
		if (!premier)
		{
			//alert(url);
			//this.location = url;
			//openWindoo(url,'Déverrouillage',300,200);
			var theData = {
	    		vids : url
	    	} ;
	    	//alert (theData);
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/passifVendor",
				data : theData,
				success : function(data) {
					
				$('#dialog').html(data);
				
				//$("#dialog").dialog("open");
				}
		}); 
			
			
			
		}
	}
	function deleteVendor() {
		var premier = true;
		//var url = "popup_actif/actif?vids=";
		var url = "";
		var aInput = document.getElementsByTagName('input');
		for (var i = 0; i < aInput.length ; i++)
		{
			if (aInput[i].type == 'checkbox' && aInput[i].id != '')
			{
				if (aInput[i].checked)
				{
					if (premier)
					{
						premier = false;
						//alert(aInput[i].id);
						url += aInput[i].id.substr(4,11);
					}
					else
					{
						//alert(aInput[i].id);
						url += "|" + aInput[i].id.substr(4,11);
					}
				}
			}
		}
	
		if (!premier)
		{
			//url = "popup_actif/index.php?vids=1|3|4"
			//this.location = url;
			//alert(url);
			//openWindoo(url,'Activation',300,200);
			//window.open(url);
			var theData = {
	    		vids : url
	    	} ;
	    	//alert (theData);
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/deleteVendor",
				data : theData,
				success : function(data) {
					
				$('#dialog').html(data);
				$("#dialog").dialog("open");
				
				}
			}); 
			 //location.reload();
		}
		
	}
</script>
<!------------------------------------- fin script ------------------------------------- -->
<?php
/*$query = "	SELECT v.VENDOR_ID, v.VENDOR_NOM, tv.SOFTWARE, tv.DATE_INSERT, v.VENDOR_URL 
			FROM `sfk_vendor` as v 
			INNER JOIN `sfk_tracking_vendor` as tv 
				ON v.VENDOR_ID = tv.VENDOR_ID 
			ORDER BY v.VENDOR_ID
		";*/
		
/*		
$query = "SELECT v.VENDOR_ID, v.VENDOR_NOM, tv.SOFTWARE, tv.DATE_INSERT, v.VENDOR_URL ,
(SELECT count(*) FROM sfk_produit_ps as ps 
WHERE ps.VENDOR_ID = v.VENDOR_ID 
 ) as NBPRODS_Prestashop, 
(SELECT count(*) FROM sfk_produit_th as th 
WHERE th.VENDOR_ID = v.VENDOR_ID 
 ) as NBPRODS_Thelia, 
(SELECT count(*) FROM sfk_produit_vm as vm 
WHERE vm.VENDOR_ID = v.VENDOR_ID 
 ) as NBPRODS_Virtuemart
			FROM `sfk_vendor` as v 
			INNER JOIN `sfk_tracking_vendor` as tv 
				ON v.VENDOR_ID = tv.VENDOR_ID 
			ORDER BY v.VENDOR_ID";
*/
$grid = new EcommercantGrid($query);

?>