<?php 
$root_path = "../../../";
$top_body = ('Fiche Produit');
//var_dump("tptptppt");
//require $root_path . 'inc/form.header.inc.php';
//require_once $root_path .	'inc/offipse.inc.php';
$class = "BusinessSoukeo".$_GET['tech']."Model";
$db = new $class();

$prod = $db -> getProduct($_GET['vendor_id'], $_GET['prod_id']);
$prod = $prod[0];
?>

<img src="<?php echo $prod['IMAGE_PRODUCT']?>" />
<table>
	<thead>
		<tr>
			<td><b>Champ importé</b></td>
			<td><b>Valeur</b></td>
		</tr>
	</thead>
    <tbody>
        <tr>
            <td class="form_label">ID_PRODUCT</td>
            <td><?php echo $prod[0]['ID_PRODUCT'] ?></td>
            <td>ID_PRODUCT</td>
        </tr>
        
       <tr>
            <td class="form_label">VENDOR_ID</td>
            <td ><?php echo $prod['VENDOR_ID'] ?></td>
            <td>VENDOR_ID</td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">NAME_PRODUCT</td>
            <td ><?php echo $prod['NAME_PRODUCT'] ?></td>
            <td>NAME_PRODUCT</td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">REFERENCE_PRODUCT</td>
            <td ><?php echo $prod['REFERENCE_PRODUCT'] ?></td>
            <td>REFERENCE_PRODUCT</td>
            <td>
            	<select name="ref_prod" id="ref_prod">
            		<option>Code EAN</option>
            		<option>Référence produit</option>
            	</select>
            </td>
        </tr>
        <tr>
            <td class="form_label">SUPPLIER_REFERENCE</td>
            <td ><?php echo $prod['SUPPLIER_REFERENCE'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">MANUFACTURER</td>
            <td ><?php echo $prod['MANUFACTURER'] ?></td>
            <td></td>
            <td>
            	<select name="manufacturer" id="manufacturer">
            		<option selected>MANUFACTURER</option>
            		<option>SUPPLIER_REFERENCE</option>
            	</select>
            </td>
        </tr>
        <tr>
            <td class="form_label">CATEGORY</td>
        </tr>
        <tr>
            <td class="form_label">DESCRIPTION</td>
            <td ><?php echo strlen($prod['DESCRIPTION']) < 80 ?  $prod['DESCRIPTION'] : substr($prod['DESCRIPTION'], 0, 80)."..." ?></td>
            <td></td>
            <td>
            	<select name="descrip" id="descrip">
            		<option selected>DESCRIPTION</option>
            		<option>CATEGORY</option>
            		<option>EAN</option>
            	</select>
            </td>
        </tr>
        <tr>
            <td class="form_label">DESCRIPTION_SHORT</td>
            <td ><?php echo strlen($prod['DESCRIPTION_SHORT']) < 80 ?  $prod['DESCRIPTION_SHORT'] : substr($prod['DESCRIPTION_SHORT'], 0, 80)."..." ?></td>
            <td></td>
            <td>
            	<select name="descrip_short" id="descrip_short">
            		<option selected>DESCRIPTION SHORT</option>
            		<option>NAME PRODUCT</option>
            	</select>
            </td>
        </tr>
        <tr>
            <td class="form_label">PRICE_PRODUCT</td>
            <td ><?php echo $prod['PRICE_PRODUCT'] ?></td>
            <td>PRIX_TTC</td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">WHOLESALE_PRICE</td>
            <td ><?php echo $prod['WHOLESALE_PRICE'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">PRICE_HT</td>
            <td ><?php echo $prod['PRICE_HT'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">PRICE_REDUCTION</td>
            <td ><?php echo $prod['PRICE_REDUCTION'] ?></td>
        </tr>
        <tr>
            <td class="form_label">POURCENTAGE_REDUCTION</td>
            <td ><?php echo $prod['POURCENTAGE_REDUCTION'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">QUANTITY</td>
            <td ><?php echo $prod['QUANTITY'] ?></td>
            <td>STOCK</td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">WEIGHT</td>
            <td ><?php echo $prod['WEIGHT'] ?></td>
            <td>POIDS</td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">EAN</td>
            <td ><?php echo $prod['EAN'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">ECOTAX</td>
            <td ><?php echo $prod['ECOTAX'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">AVAILABLE_PRODUCT</td>
            <td ><?php echo $prod['AVAILABLE_PRODUCT'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">URL_PRODUCT</td>
            <td ><?php echo $prod['URL_PRODUCT'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">IMAGE_PRODUCT</td>
            <td ><?php echo $prod['IMAGE_PRODUCT'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">FDP</td>
            <td ><?php echo $prod['FDP'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">ID_MERE</td>
            <td ><?php echo $prod['ID_MERE'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">DELAIS_LIVRAISON</td>
            <td ><?php echo $prod['DELAIS_LIVRAISON'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">IMAGE_PRODUCT_2</td>
            <td ><?php echo $prod['IMAGE_PRODUCT_2'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">IMAGE_PRODUCT_3</td>
            <td ><?php echo $prod['IMAGE_PRODUCT_3'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">REDUCTION_FROM</td>
            <td ><?php echo $prod['REDUCTION_FROM'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">REDUCTION_TO</td>
            <td ><?php echo $prod['REDUCTION_TO'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">META_DESCRIPTION</td>
            <td ><?php echo $prod['META_DESCRIPTION'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">META_KEYWORDS</td>
            <td ><?php echo $prod['META_KEYWORDS'] ?></td>
            <td><em>inutilisé</em></td>
            <td><a href="#">Modifier</a></td>
        </tr>
        <tr>
            <td class="form_label">ACTIVE</td>
            <td ><?php echo $prod['ACTIVE'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">DATE_UPDATE</td>
            <td ><?php echo $prod['DATE_UPDATE'] ?></td>
            
        </tr>
        <tr>
            <td class="form_label">DATE_CREATE</td>
            <td ><?php echo $prod['DATE_CREATE'] ?></td>
            
        </tr>
        
    </tbody>
</table>

<?php
require $root_path . "inc/form.footer.inc.php";

 ?>

