﻿<?php

$nom   = new FilterText("nom", ("Nom de l'e-commerçant")." :", "", false, 12);
 
$fNbProd = new FilterText("fNbProd", ("Nb prod Sup ou egal à : "), "", false, 10);
$fNbProdAss = new FilterText("fNbProdAss", ("Nb prod asso Sup ou egal à : "), "", false, 10);

$fSoft = new FilterList("fSoft", ("SoftWare :"),"all");
$sql = " SELECT DISTINCT SOFTWARE FROM  sfk_tracking_vendor ";
$rs = dbQueryAll($sql);  
$fSoft->addElement('all','all');
$fSoft->addElement('aucun','aucun');

foreach( $rs as $R){
    $fSoft->addElement($R['SOFTWARE'],$R['SOFTWARE'] );
}
$fActive = new FilterList("fActive", ("ETAT \"Active\" :"),"1");
$fActive->addElement('all','all');
$fActive->addElement('1','actif');
$fActive->addElement('0','inactif');

$fDelete = new FilterList("fDelete", ("ETAT \"Delete\" :"),"0");
$fDelete->addElement('all','all');
$fDelete->addElement('1','Delete');
$fDelete->addElement('0','noDelete');

$filter = new Filter("ecommercant");
$filter->addFilter($nom);
$filter->addFilter($fNbProd);
$filter->addFilter($fNbProdAss);
$filter->addFilter($fSoft);
$filter->addFilter($fActive);
SystemParams::getParam('system*select_ecom_delete') =='N' ? : $filter->addFilter($fDelete);

$filter->setGridToReload("ecommercant");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(
         /*
		  * array
          (
          		"access" => "qualification.ecommercant..",
                "picto" => "add",
                "name" =>("Ajouter un vendeur"),
                "url" => "/qualification/ecommercant/popup_add/index.php",
                "width" => 650,
                "height" => 600,
                "popup" => "jdiag"
           ),
          */ 
          array
          (
                "access" => "commercants.ecommercant..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.ecommercant..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
           array
           (
          		"access" => "commercants.ecommercant..",
                "picto" => "add",
                "name" =>("Passe en le/les vendeur(s) en actif"),
                "url" => "javascript: actifVendor()",
                //"url" => "/qualification/ecommercant/popup_add/index.php?vids=1",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            ),
           array
           (
          		"access" => "commercants.ecommercant..",
                "picto" => "denied",
                "name" =>("Passe le/les vendeur(s) en inactif"),
                "url" => "javascript: inactifVendor()",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            ),  
			 array
           (
          		"access" => "commercants.ecommercant..",
                "picto" => "trash",
                "name" =>(SystemParams::getParam('system*select_ecom_delete') == 'N'?"Passe en le/les vendeur(s) en delete":"Passe en le/les vendeur(s) en delete/no delete"),
                "url" => "javascript: deleteVendor()",
                //"url" => "/qualification/ecommercant/popup_add/index.php?vids=1",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("","iconCircleCheck","check",("Vendeur actif")),
				array("","iconCircleWarn","denied",("Vendeur inactif")), 				
				array("commercants.ecommercant..","","search",("Afficher les information pour cette e-commerçant")),
				array("commercants.ecommercant..","","edit",("Qualification des fiches produits")),
				array("commercants.ecommercant..","","reload",("Réimporte les produits de l'e-commerçant")),
				array("commercants.ecommercant..","","grids_catalog",("Exporte fiches avahis du commerçant")),
				array("commercants.ecommercant..","","dollar",("Exporte stock / prix du commerçant")),
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
