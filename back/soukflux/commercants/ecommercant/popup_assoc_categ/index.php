<?php
//GROSSISTE
if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
//ECOMMERCANT	
else{ $typev = "ecom";}

$class = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
$db = new $class();

$view = new BusinessView();
$skf = new BusinessSoukeoModel();
$data = $skf->getSsCatByParentId();

$categ = str_replace("|", " > ", $_POST['cat_ecom']);
$categ = str_replace("+", " ", $categ);

$listeProd = $db -> getAllUnassocProducts($categ, $_SESSION[$typev]['vendor']);
//On prend le premier produit pour exemple au hasard
$prod=$listeProd[0];
$_SESSION[$typev]['prodToAdd'] = $prod;
//Si la catégorie a déja été associée
if(isset($_POST['id_cat_assoc']) && !empty($_POST['id_cat_assoc']) && $_POST['id_cat_assoc'] !='false'){
	$categ_av_assoc = $skf -> getStrCateg($_POST['id_cat_assoc']);
	$categ_av_assoc = substr($categ_av_assoc, 0, strlen($categ_av_assoc)-2);
}


?>

<h1>Association de catégorie</h1>
<span id="idV" style="visibility:hidden"><?php echo $_POST['vendor_id'] ?></span> 
<p class="alert">Associer : <span class="label label-info" id="catEcom"><?php echo $categ ?></span> </p>
<p id="success" class="alert">Avec : </p>
<?php if(isset($categ_av_assoc) && !empty($categ_av_assoc)){ ?>
	<p id="oldAssoc" class="alert alert-info"><em>Déja associé à :</em> <span class="label label-info"><?php echo $categ_av_assoc ?></span></p>
<?php } ?>
<br />
<div class="treeviewcheck">
	<input type="submit" class="btn btn-primary" name="submit" value="VALIDER" id="submit" onclick="validCateg('cat');"/>
		<input type="hidden" name="optionvalue" id="selectbox-popup" class="input-xlarge" data-placeholder="Choisissez une catégorie" onchange="validSelect2Popup()"/>
		<div class="top-bar" style="margin-top: 15px;"><h3>Catégories</h3></div>
		<div class="well">
			<?php echo $view -> showTreeCheckbox($data, true, 0) ;?>
		</div>
	<input type="submit" class="btn btn-primary" name="submit" value="VALIDER" id="submit" onclick="validCateg('cat');"/>
</div>

<div id="fixed_fields" style="display:none">
	<h1 id="">Veuillez maintenant choisir des préférences d'association pour la catégorie</h1>
	<a href="#" onclick="validPrefCateg();">Sauvegarder ces préférences pour la catégorie</a>
	<table >
		<thead>
			<tr>
				<td style="width:200px;"><b>Champs attendus</b></td>
				<td style="width:200px;"><b>Champs ecommercant</b></td>
				<td style="width:200px;"><b>Valeur</b></td>
			</tr>
		</thead>
	    <tbody>
	
	        <tr>
	        	<td class="form_label">name</td>
	        	<td>
	            	<select name="name" id="name" onchange="loadField(this)">
	            		<option selected="selected">NAME_PRODUCT</option>
	            		<option>REFERENCE_PRODUCT</option>
	            	</select>
	            </td>   
	            <td ><input id="val_name" type="text" value="<?php echo $prod['NAME_PRODUCT'] ?>"/></td>
	        </tr>
	        
	        <tr>
	        	<td class="form_label">sku</td>
	        	<td>
	            	<select name="sku" id="sku" onchange="loadField(this)">
	            		<option >EAN</option>
	            		<option>REFERENCE_PRODUCT</option>
	            	</select>
	            </td>
	            <td ><input id="val_sku" type="text" value="<?php echo $prod['EAN'] ?>"/></td>            
	        </tr>
	        
	        <tr class="form_label">
	        	<td>categories</td>
	            <td id="popup_categ" class="form_label">

	            </td>
	
	            <td style="text-align: none"><?php //echo $out  ?>
	            	<span id="catEcom"><?php echo $categ ?></span>
	            	<span id="cat_id_assoc" style="visibility:hidden"><?php echo $_POST['cat_id_assoc']  ?></span>
	            </td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">description</td>
	            <td>
	            	<select name="description" id="description" onchange="loadField(this)">
	            		<option selected>DESCRIPTION</option>
	            		<option>DESCRIPTION_SHORT</option>
	            		<option>--</option>
	            	</select>
	            </td>
	             <td ><textarea id="val_description" name="Name" rows="8" cols="40"><?php echo $prod['DESCRIPTION']?></textarea></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">short_description</td>
	
	            <td>
	            	<select name="short_description" id="short_description" onchange="loadField(this)">
	            		<option selected>DESCRIPTION_SHORT</option>
	            		<option>DESCRIPTION</option>
	            		<option>NAME_PRODUCT</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><textarea name="descriptionshort" id="val_short_description" cols="30" rows="10"><?php echo $prod['DESCRIPTION_SHORT'] ?></textarea></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">price</td>
	            <td>
	            	<select name="price" id="price" onchange="loadField(this)">
	            		<option selected>PRICE_PRODUCT</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_price" type="text" value="<?php echo $prod['PRICE_PRODUCT'] ?>"/></td>
	        </tr>
	        <tr>
	            <td class="form_label">weight</td>
	            <td>WEIGHT</td>
	            <td ><input type="text" value="<?php echo $prod['WEIGHT'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">country_of_manufacture</td>
	            <td>
	            	<select name="country_of_manufacture" id="country_of_manufacture" onchange="loadField(this)">
	            		<option selected>--</option>
	            		<option>MANUFACTURER</option>
	            		<option>SUPPLIER_REFERENCE</option>
	            	</select>
	            </td>
	            <td ><input id="val_country_of_manufacture" type="text" value="<?php echo $prod['SUPPLIER_REFERENCE'] ?>"/></td>     
	        </tr>
	        
	        <tr>
	            <td class="form_label">meta_description</td>
	            <td>
	            	<select name="meta_description" id="meta_description" onchange="loadField(this)">
	            		<option selected>META_DESCRIPTION</option>
	            		<option>META_KEYWORDS</option>
	            		<option>NAME_PRODUCT</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_meta_description" type="text" value="<?php echo $prod['META_DESCRIPTION'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">meta_keyword</td>
	            <td>
	            	<select name="meta_keyword" id="meta_keyword" onchange="loadField(this)">
	            		<option selected>META_KEYWORDS</option>
	            		<option>META_DESCRIPTION</option>
	            		<option>NAME_PRODUCT</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_meta_keyword" type="text" value="<?php echo $prod['META_KEYWORDS'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">meta_title</td>
	            <td>
	            	<select name="meta_title" id="meta_title" onchange="loadField(this)">
	            		<option selected>NAME_PRODUCT</option>
	            		<option>META_DESCRIPTION</option>
	            		<option>NAME_PRODUCT</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_meta_title" type="text" value="<?php echo $prod['META_KEYWORDS'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">image</td>
	            <td>
	            	<select name="image" id="image" onchange="loadField(this)">
	            		<option selected>IMAGE_PRODUCT</option>
	            		<option>IMAGE_PRODUCT_2</option>
	            		<option>IMAGE_PRODUCT_3</option>
	            		<option>THUMBNAIL</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_image" type="text" value="<?php echo $prod['IMAGE_PRODUCT'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">small_image</td>
	            <td>
	            	<select name="small_image" id="small_image" onchange="loadField(this)">
	            		<option selected>IMAGE_PRODUCT_2</option>
	            		<option>IMAGE_PRODUCT</option>
	            		<option>IMAGE_PRODUCT_3</option>
	            		<option>THUMBNAIL</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_small_image" type="text" value="<?php echo $prod['IMAGE_PRODUCT_2'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">thumbnail</td>
	            <td>
	            	<select name="thumbnail" id="thumbnail" onchange="loadField(this)">
	            		<option selected>IMAGE_PRODUCT_3</option>
	            		<option>IMAGE_PRODUCT</option>
	            		<option>IMAGE_PRODUCT_2</option>
	            		<option>THUMBNAIL</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_thumbnail" type="text" value="<?php echo $prod['IMAGE_PRODUCT_3'] ?>"/></td>
	        </tr>
	        
	        <tr>
	            <td class="form_label">manufacturer</td>
	            <td>
	            	<select name="manufacturer" id="manufacturer" onchange="loadField(this)">
	            		<option selected>MANUFACTURER</option>
	            		<option>SUPPLIER_REFERENCE</option>
	            		<option>NAME_PRODUCT</option>
	            		<option>DESCRIPTION</option>
	            		<option>SHORT_DESCRIPTION</option>
	            		<option>--</option>
	            	</select>
	            </td>
	            <td ><input id="val_manufacturer" type="text" value="<?php echo $prod['MANUFACTURER'] ?>"/></td>
	        </tr>
	        
	        
	    </tbody>
	</table>
</div>
<?php
$attr = $db->getAllAttrFromProduct($prod['ID_PRODUCT'], $prod['VENDOR_ID']);
$attrAvahis = $skf -> getAllAttrByProdFromCat($_POST['id_cat_assoc']);
?>
			
   <table style = "display: none" border = "1" cellspacing="5" cellpadding="5" id="attr_chosen">
    	<thead>
    		<tr>
    			<td>Attributs de l'e-commerçant</td>
				<!-- <td>Valeur</td> -->
				<td>Attributs avahis</td>
				<td>Action</td>
    		</tr>
    		
    	</thead>
        <tbody>
			<!-- A REMPLIR EN AJAX -->
		</tbody>
    </table>	
	<a href="#" onclick="validPrefCateg();">Sauvegarder ces préférences pour la catégorie</a>
<script type="text/javascript" charset="utf-8">
    
        $('#checkboxtree').checkboxTree({
            onCheck: {
                ancestors: 'check',
                descendants: 'uncheck',
                others: 'uncheck'
            },
            onUncheck: {
                descendants: 'uncheck'
            }
        });
        
	$("#selectbox-popup").select2({
		minimumInputLength: 3,
		ajax: {
			url: "/app.php/getlistcateg",
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
	});	

</script>
