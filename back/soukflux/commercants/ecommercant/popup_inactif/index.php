﻿<?PHP
$msg_info = "";
$onload = "javascript:window.focus();";
$root_path = "../../../";
$ACCESS_key = "commercants.ecommercant..";
//require $root_path.'inc/popup.header.inc.php';


if ( !array_key_exists('vids',$_POST) )
{
	echo "<p><b>Aucun code vendeur n'a été précisé.</b></p>";
}
else
{
?>
	<p><b>Passe le/les vendeur(s) en inactif</b></p>
<?PHP
  
  $vids_array = explode("|",$_POST['vids']);
  $vids_where = "";
  $vids_count = 0;
  
  foreach($vids_array as $vid)
  {
    if ($vids_count!=0) $vids_where .= ",";
    $vids_where .= "'".$vid."'";
    $vids_count += 1; 
  }
  
  $res_count = dbQueryOne("select count(VENDOR_ID) from sfk_vendor where VENDOR_ID in (".$vids_where.") and ACTIVE ='1'");
  //dbQuery("update sfk_vendor set ACTIVE='0' where VENDOR_ID in (".$vids_where.") and ACTIVE >=".SystemParams::getParam('security*pwd_error'));
  dbQuery("update sfk_vendor set ACTIVE='0' where VENDOR_ID in (".$vids_where.") and ACTIVE ='1'");
?>
  <script>parent.gridReload('ecommercant','../../');</script>
  
  <p>
  <b><?php  echo $vids_count ?></b> <?php  echo ("vendeur(s) sélectionné(s).") ?>
  <br/><b><?php  echo $res_count['count(VENDOR_ID)'] ?></b> <?php  echo ($res_count['count(VENDOR_ID)']<=1?("vendeur a été inactivé."):("vendeurs ont été inactivés.")) ?>
  </p>

<?PHP
}

//require $root_path.'inc/popup.footer.inc.php';
?>
