<?php 
//GROSSISTE
if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
//ECOMMERCANT	
else{ $typev = "ecom";}

$class = "BusinessSoukeo".$_POST['tech']."Model";
$db = new $class();
if(!empty($_POST['tech'])){
	$prod = $db -> getProduct($_POST['vendor_id'], $_POST['prod_id']);
	$prod = $prod[0];	
	$_SESSION[$typev]['prodToAdd'] = $prod;
}
$view = new BusinessView();
$skf = new BusinessSoukeoModel();
$data = $skf->getSsCatByParentId();

$categ = str_replace("|", " > ", $prod['CATEGORY']);
$categ = str_replace("+", " ", $categ);


$cat_id_assoc = $skf ->isCatAssoc($_POST['vendor_id'], $categ);
if($cat_id_assoc != false){
	$categAssocAv = $skf -> getStrCateg($cat_id_assoc);
}
$attr = $db->getAllAttrFromProduct($prod['ID_PRODUCT'], $prod['VENDOR_ID']);
$attrAvahis = $skf -> getAllAttrByProdFromCat($cat_id_assoc);
if (empty($attrAvahis)){
	$attrAvahis = $skf -> getAllAttrDistFromCat($cat_id_assoc);
}

?>
<!-- <img src="<?php echo $prod['IMAGE_PRODUCT']?>" /> -->
<span id="idV" style="visibility:hidden"><?php echo $_POST['vendor_id'] ?></span> 



<?php
///////////////////////////////////// GESTION BOUTTON ADD PRODUCT CAS DIFFERENTS /////////////////////////////////////////
//CAS 1
//Produit e-commercant possède attribut ET a une catégorie Avahis associée...
if(!empty($attr) && !empty($categAssocAv)) {?>
	<p><button class="big button" onclick="validAddProduct();">Ajouter Produit</button></p>	
<?php 
//CAS 2
//Produit e-commercant n'a aucun attribut mais est DEJA associé à une catégorie Avahis
}elseif(empty($attr) && !empty($categAssocAv)) {
		 
	if(empty($attrAvahis)){  ?>
	
	<p><button onclick="validAddProductNoAttrAv(); return false;">Ajouter Produit</button></p>
<?php }else{  ?>
	<!-- <a href="#" onclick="addRowAttr(); return false;">Ajouter un attribut</a> -->
	<p><button onclick="validAddProductNoAttr(); return false;">Ajouter Produit</button></p>
<?php } ?>
<?php 

//CAS 3
//Pas de catégories associée on va lui proposer les attributs après avoir choisi la categ donc visibility hidden
}elseif(empty($categAssocAv)){
	?>
    <p><button class="btn btn-primary" onclick="validAddProduct();">Ajouter Produit</button></p>
 <?php 
} 
/////////////////////////////////// FIN GESTION BOUTTON ADD PRODUCT CAS DIFFERENTS /////////////////////////////////////////
?>





	<span style="display: none" id="current_product"><?php echo $prod['ID_PRODUCT'] ?></span>
    <h5 style="border-bottom: 1px solid #c2c2c2; background-color: #fff; padding: 10px; text-align: center;" >CHAMPS STATIQUES</h5>
<a href="#" class="button" onclick="validPrefCateg();">Sauvegarder ces préférences pour tous les produits de cette catégorie du vendeur</a>
<table border="0" cellspacing="5" cellpadding="5" id="fixed_fields" style="width:100%">
	<thead>
		<tr>
			<td style="width:25%;"><b>Champs attendus</b></td>
			<td style="width:25%;"><b>Champs ecommercant</b></td>
			<td style="width:50%;"><b>Valeur</b></td>
		</tr>
	</thead>
    <tbody>

        <tr>
        	<td class="form_label">name</td>
        	<td>
            	<select name="name" id="name" onchange="loadField(this)">
            		<option selected="selected">NAME_PRODUCT</option>
            		<option>REFERENCE_PRODUCT</option>
            	</select>
            </td>   
            <td ><input id="val_name" type="text" value="<?php echo $prod['NAME_PRODUCT'] ?>"/></td>
        </tr>
        
        <tr>
        	<td class="form_label">sku</td>
        	<td>
            	<select name="sku" id="sku" onchange="loadField(this)">
            		<option >EAN</option>
            		<option>REFERENCE_PRODUCT</option>
            	</select>
            </td>
            <td >
                <input id="val_sku" type="text" value="<?php echo $prod['EAN'] ?>" onChange="resetSku2(); return false;"/>
                <button style="margin-bottom: 9px;" class="btn" type="button" onClick="testSku2(this); return false;">Vérifier SKU</button>
                <br /><span style="margin-bottom: 9px;" id="message-sku"></span>
            </td>            
        </tr>
        
        <tr class="form_label">
        	<td>categories</td>
            <td class="form_label">
            	<span id="popup_categ">
            	<?php 
            	 if(isset($categAssocAv) && !empty($categAssocAv)){
            	 	echo  substr($categAssocAv,0,strlen($categAssocAv)-2) ; ?>
            	 </span>
            	 	<br/><a id="openCatTree" href="#ancreTreeView" onclick="openCatTree();">Changer la catégorie pour ce produit</a>
            </td>
			<td style="text-align: left">
				<span id="catEcom"><?php echo $categ ?></span>
			</td>
					<span id="cat_id_assoc" style="visibility:hidden"><?php echo $cat_id_assoc ? $cat_id_assoc : $_POST['cat_id_assoc'] ?></span> 
           <?php }else{ ?>

            	 	<a id="goToCatTree" href="#ancreTreeView">Non associé ! Choisir catégorie ?</a>
            <td style="text-align: left">
        	 	<span id="catEcom"><?php echo $categ ?></span>
        		<span id="cat_id_assoc" style="visibility:hidden"></span>
            </td>	
           <?php } 
            	 ?>
            </td>
        </tr>
        
        <tr>
            <td class="form_label">description</td>
            <td>
            	<select name="description" id="description" onchange="loadField(this)">
            		<option selected>DESCRIPTION</option>
            		<option>DESCRIPTION_SHORT</option>
            		<option>--</option>
            	</select>
            </td>
             <td ><textarea id="val_description" name="Name" rows="8" cols="40"><?php echo $prod['DESCRIPTION']?></textarea></td>
        </tr>
        
        <tr>
            <td class="form_label">short_description</td>

            <td>
            	<select name="short_description" id="short_description" onchange="loadField(this)">
            		<option selected>DESCRIPTION_SHORT</option>
            		<option>DESCRIPTION</option>
            		<option>NAME_PRODUCT</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><textarea name="descriptionshort" id="val_short_description" cols="30" rows="10"><?php echo $prod['DESCRIPTION_SHORT'] ?></textarea></td>
        </tr>
        
        <tr>
            <td class="form_label">price</td>
            <td>
            	<select name="price" id="price" onchange="loadField(this)">
            		<option selected>PRICE_PRODUCT</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_price" type="text" value="<?php echo $prod['PRICE_PRODUCT'] ?>"/></td>
        </tr>
        <tr>
            <td class="form_label">weight</td>
            <td>WEIGHT</td>
            <td ><input type="text" id="val_weight" value="<?php echo $prod['WEIGHT'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">country_of_manufacture</td>
            <td>
            	<select name="country_of_manufacture" id="country_of_manufacture" onchange="loadField(this)">
            		<option selected>--</option>
            		<option>MANUFACTURER</option>
            		<option>SUPPLIER_REFERENCE</option>
            	</select>
            </td>
            <td ><input id="val_country_of_manufacture" type="text" value="<?php echo $prod['SUPPLIER_REFERENCE'] ?>"/></td>     
        </tr>
        
        <tr>
            <td class="form_label">meta_description</td>
            <td>
            	<select name="meta_description" id="meta_description" onchange="loadField(this)">
            		<option selected>META_DESCRIPTION</option>
            		<option>META_KEYWORDS</option>
            		<option>NAME_PRODUCT</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_meta_description" type="text" value="<?php echo $prod['META_DESCRIPTION'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">meta_keyword</td>
            <td>
            	<select name="meta_keyword" id="meta_keyword" onchange="loadField(this)">
            		<option selected>META_KEYWORDS</option>
            		<option>META_DESCRIPTION</option>
            		<option>NAME_PRODUCT</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_meta_keyword" type="text" value="<?php echo $prod['META_KEYWORDS'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">meta_title</td>
            <td>
            	<select name="meta_title" id="meta_title" onchange="loadField(this)">
            		<option selected>META_KEYWORDS</option>
            		<option>META_DESCRIPTION</option>
            		<option>NAME_PRODUCT</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_meta_title" type="text" value="<?php echo $prod['META_KEYWORDS'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">image</td>
            <td>
            	<select name="image" id="image" onchange="loadField(this)">
            		<option selected>IMAGE_PRODUCT</option>
            		<option>IMAGE_PRODUCT_2</option>
            		<option>IMAGE_PRODUCT_3</option>
            		<option>THUMBNAIL</option>

            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_image" type="text" value="<?php echo $prod['IMAGE_PRODUCT'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">small_image</td>
            <td>
            	<select name="small_image" id="small_image" onchange="loadField(this)">
            		<option selected>IMAGE_PRODUCT_2</option>
            		<option>IMAGE_PRODUCT</option>
            		<option>IMAGE_PRODUCT_3</option>
            		<option>THUMBNAIL</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_small_image" type="text" value="<?php echo $prod['IMAGE_PRODUCT_2'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">thumbnail</td>
            <td>
            	<select name="thumbnail" id="thumbnail" onchange="loadField(this)">
            		<option selected>IMAGE_PRODUCT_3</option>
            		<option>IMAGE_PRODUCT</option>
            		<option>IMAGE_PRODUCT_2</option>
            		<option>THUMBNAIL</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_thumbnail" type="text" value="<?php echo $prod['IMAGE_PRODUCT_3'] ?>"/></td>
        </tr>
        
        <tr>
            <td class="form_label">manufacturer</td>
            <td>
            	<select name="manufacturer" id="manufacturer" onchange="loadField(this)">
            		<option selected>MANUFACTURER</option>
            		<option>SUPPLIER_REFERENCE</option>
            		<option>NAME_PRODUCT</option>
            		<option>DESCRIPTION</option>
            		<option>SHORT_DESCRIPTION</option>
            		<option>--</option>
            	</select>
            </td>
            <td ><input id="val_manufacturer" type="text" value="<?php echo $prod['MANUFACTURER'] ?>"/></td>
        </tr>
        
        
    </tbody>
</table>
<a href="#" class="button"onclick="validPrefCateg();">Sauvegarder ces préférences pour tous les produits de cette catégorie du vendeur</a>
    <hr id="ancreTreeView"/>
    <h5 style="border-bottom: 1px solid #c2c2c2; background-color: #fff; padding: 10px; text-align: center;">ATTRIBUTS DYNAMIQUES</h5>
    <?php
    if(!isset($categAssocAv) || empty($categAssocAv)) { $visibility = ""; } else{$visibility ="display:none" ;}?>
    
       <div class="treeviewcheck" style="<?php echo $visibility ?>">
       	<strong>Il faut tout d'abord que ce produit soit associé à une catégorie</strong><br/><br>
       	<input class="btn btn-primary" type="submit" name="submit" value="Valider" onclick="validCateg('prod');"/>
		<input type="hidden" name="optionvalue" id="selectbox-popup" class="input-xlarge" data-placeholder="Choisissez une catégorie" onchange="validSelect2Popup()"/>
	<br><br>
			<div class="top-bar"><h3>Catégories</h3></div>
    		<div class="well">
    			<?php echo $view -> showTreeCheckbox($data, true, 0) ; ?>	
    		</div>
		<input class="btn btn-primary" type="submit" name="submit" value="Valider" onclick="validCateg('prod');"/>
	 </div>



<?php


//CAS 1
//Produit e-commercant possède attribut ET a une catégorie Avahis associée...
if(!empty($attr) && !empty($categAssocAv)) {?>
			
   <table border="1" cellspacing="5" cellpadding="5" id="attr_chosen">
    	<thead>
    		<tr>
    			<td>Attributs de l'e-commerçant</td>
				<td>Attributs Avahis</td>
				<td>Valeur</td>
				<td>Action</td>
    		</tr>
    		
    	</thead>
       <tbody>

	</tbody> 
    </table>	
	<p><button class="big button" onclick="validAddProduct();">Ajouter Produit</button></p>	
<?php 
//CAS 2
//Produit e-commercant n'a aucun attribut mais est DEJA associé à une catégorie Avahis
}elseif(empty($attr) && !empty($categAssocAv)) { ?>
	<b>Il n'y a aucun attribut pour ce produit e-commercant, voici la liste de nos attributs :</b>
	<table border="1" cellspacing="5" cellpadding="5" id="no_attr" style="width:100%">
		<thead>
			<td>Attribut Avahis</td>
			<td>Valeur</td>
			<td>Action</td>
		</thead>
		<tbody>
			<?php foreach ($attrAvahis as $a) { ?>
				<tr>
					<td><input disabled type="text" value="<?php echo $a['label_attr'] ?>"/></td>
					<td>
						<!-- On propose les valeurs pour ne pas avori trop n'importe quoi en base avec un Chosen ou autre
							outil d'autocomplétion sur liste ce sera encore plus simple -->
						<select id="attr_<?php echo $a['id_attribut'] ?>">
							<option value='--' selected="selected">Aucune Valeur</option>
					<?php 
						$values = $skf -> getAllDisctValueForAttrFromCateg($cat_id_assoc, $a['id_attribut']); 
						foreach ($values as  $value) {
							echo '<option value="'.$value['VALUE'].'">'.$value['VALUE'].'</option>';
						}	
					?>		
						</select>
						<a href="#" id="addv_<?php echo $a['id_attribut'] ?>" onclick="addValPopUp(this);">+ Ajouter</a>
					</td>

				<!--<td><input type="text" name="some_name" value="" id="<?php echo $a['id_attribut'] ?>"/></td>-->
					<td><a class="supprAttr" href="#" onclick="supprLigne(this)">Supprimer</a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
<?php if(empty($attrAvahis)){  ?>
	<a href="#" class="btn btn-primary" onclick="addRowAttr(); return false;">Ajouter un attribut</a>
	<p><button class="btn btn-primary" onclick="validAddProductNoAttrAv(); return false;">Ajouter Produit</button></p>
<?php }else{  ?>
	<!-- <a href="#" onclick="addRowAttr(); return false;">Ajouter un attribut</a> -->
	<p><button class="btn btn-primary" onclick="validAddProductNoAttr(); return false;">Ajouter Produit</button></p>
<?php } ?>
<?php 

//CAS 3
//Pas de catégories associée on va lui proposer les attributs après avoir choisi la categ donc visibility hidden
}elseif(empty($categAssocAv)){
	?>
	<table  style="display: none" id="attr_chosen" border="1" cellspacing="5" cellpadding="5" style="width:100%">
    	<thead>
    		<tr>
    			<td>Attributs de l'e-commerçant</td>
				<td>Attributs avahis</td>
				<td>Valeur</td>
				<td>Action</td>
    		</tr>
    		
    	</thead>
        <tbody>
			<!-- Rempli en AJAX -->
		</tbody>
    </table>
    <p><button class="btn btn-primary" onclick="validAddProduct();">Ajouter Produit</button></p>
 <?php } ?>

  
  
  
  
  
  
  
  
  
  
  
  
  
    
    <script type="text/javascript" charset="utf-8">
    	
 

		
<?php if(!empty($categAssocAv)){ ?>
		$(document).ready(function() {
 			fillFixedFields ('<?php echo $_POST['vendor_id']?>', '<?php echo $cat_id_assoc ?>', "<?php echo $categ ?>");
 			showAttrAvPopup ('<?php echo $cat_id_assoc ?>', '<?php echo $_POST['prod_id'] ?>');
		});
 <?php } ?>   
		
		function addRowAttr () {
			var $myDialog = $('<div></div>')
	    		.html('Entrez le nom de l\'attribut que vous voulez ajouter : <br/><input id="newAttr" type="text" \>')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Ajout d\'un attribut',
	    			buttons: {
	    				"OK": function () {
						      		var newAttr = $('#newAttr').val();
						      		$('#no_attr tbody').append("<tr><td><input value ='"+newAttr+"' type='text' disabled/></td><td><input class='newAttr' id='new_"+newAttr+"' type='text' \></td><td><a class='supprAttr' href='#' onclick='supprLigne(this)'>Supprimer</a></td></tr>");
						      		$('#newAttr').remove();
									$(this).dialog("destroy");
							    	return true;
						 },
					     "Annuler": function () {
					     			$('#newAttr').remove();
						        	$(this).dialog("destroy");
						        	return false;
					     }
	    			}
	    		});
		 	$myDialog.dialog('open');
			
		}
		
		function supprLigne (elm) {
		  $(elm).parent().parent().remove();
		};
		
		function openCatTree(){
			$('.treeviewcheck').show();
		};
        
        function resetSku2(){
            $("#message-sku").html("");
        }
       	
    </script>