<?php 
$root_path = "../../../";
$top_body = ('Fiche Produit');
$class = "BusinessSoukeo".$_POST['tech']."Model";
if($_POST['tech'] == "Carshop") $class = "BusinessSoukeoGrossisteModel";
$db = new $class();
$skf = new BusinessSoukeoModel();
if(!empty($_POST['tech'])){
	$prod = $db -> getProduct($_POST['vendor_id'], $_POST['prod_id']);
	$prod = $prod[0];	
}else{
	$prod = $db -> getProductAlias($_POST['prod_id']);
	$prod['CATEGORY'] = $db -> getStrCateg($prod['CATEGORY']);
	$prod['CATEGORY'] = substr($prod['CATEGORY'], 0, strlen($prod['CATEGORY'])-2); 
}
?>

<h1><?php echo $prod['NAME_PRODUCT'] ?></h1>
<?php  
	if ($skf -> productIsAssoc($_POST['vendor_id'], $_POST['prod_id'])){
		$prodAv = $skf -> getProdAvAssoc($_POST['vendor_id'], $_POST['prod_id']);
		echo "<p><em>Associé à :</em>";
		echo "<h3>".$prodAv['NAME_PRODUCT']."</h3><b> de ".$skf->getStrCateg($prodAv['CATEGORY'])."</b></p>";
	}

?>
<img src="<?php echo $prod['IMAGE_PRODUCT']?>" />
<p>
            <label class="form_label"><b>Description : </b></label>
            <span><?php echo htmlentities($prod['DESCRIPTION'], ENT_QUOTES, 'UTF-8') ?></span>
        </p>
<hr/>
	<h4>CHAMPS FIXES</h4>
<hr/>
<p>
	<label class="form_label"><b>ID : </b></label>
	<span><?php echo $prod['ID_PRODUCT'] ?></span>
</p>
<p>

	
</p>
        <p>
            <label class="form_label"><b>Référence : </b></label>
            <span ><?php echo $prod['REFERENCE_PRODUCT'] ?></span>
        </p>

        <p>
            <label class="form_label"><b>Marque : </b></label>
            <span ><?php echo $prod['MANUFACTURER'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>Catégorie : </b></label>
            <span><?php echo $prod['CATEGORY'] ?></span>
        </p>
        
        <p>
            <label class="form_label"><b>Petite description : </b></label>
            <span ><?php echo htmlentities($prod['DESCRIPTION_SHORT'], ENT_QUOTES, 'UTF-8')  ?></span>
        </p>
        <p>
            <label class="form_label"><b>Prix : </b></label>
            <span ><?php echo $prod['PRICE_PRODUCT'], " €"?></span>
        </p>
        <p>
            <label class="form_label"><b>Quantité : </b></label>
            <span ><?php echo $prod['QUANTITY'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>Poids : </b></label>
            <span ><?php echo $prod['WEIGHT'] ?></span>
        </p>
        <p>
            <label class="form_label"><b>EAN : </b></label>
            <span ><?php echo $prod['EAN'] ?></span>
        </p>
      
     
        <p>
            <label class="form_label"><b>DATE_UPDATE : </b></label>
            <span ><?php echo $prod['DATE_UPDATE'] ?></span>
            
        </p>
        <p>
            <label class="form_label"><b>DATE_CREATE : </b></label>
            <span ><?php echo $prod['DATE_CREATE'] ?></span>
            
        </p>
        <hr/>
        	<h4>CHAMPS DYNAMIQUES</h4>
        <hr/>
<?php
$attr = $db->getAllAttrFromProduct($prod['ID_PRODUCT'], $prod['VENDOR_ID']);

		foreach ($attr as $attribut) {?>
			<p>
				<label><b><?php echo $attribut['LABEL_ATTR'] ?> : </b></label>
				<span><?php echo $attribut['VALUE'] ?></span>
			</p>
				
<?php 	}
//require $root_path . "inc/form.footer.inc.php";

?>

