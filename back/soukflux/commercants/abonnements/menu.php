<?php

$numvendor  = new FilterText("numvendor", ("N° Vendeur")." :", $_GET['id'], false, 12);

// RECHERCHE FILTER CATEGORY
$vendeurfilter = new FilterList("vendeurfilter", ("Nom vendeur :"), "all");
$sql = " SELECT DISTINCT vendor_nom, vendor_id FROM  sfk_vendor 
         WHERE ACTIVE = 1 AND vendor_id IS NOT NULL ORDER BY vendor_nom ASC";
$rs = dbQueryAll($sql);  
$vendeurfilter->addElement('all','all');

foreach( $rs as $R){
    $vendeurfilter->addElement($R['vendor_id'],$R['vendor_nom'] );
}


// RECHERCHE FILTER CATEGORY
$typeabofilter = new FilterList("typeabofilter", ("Type abonnement :"), "all");
$sql = " SELECT DISTINCT abonnement_id, label FROM  skf_abonnement 
         ORDER BY label ASC";
$rs = dbQueryAll($sql);  
$typeabofilter->addElement('all','all');

foreach( $rs as $R){
    $typeabofilter->addElement($R['abonnement_id'],$R['label'] );
}

//Date
$typevendorfilter = new FilterList("typevendorfilter", ("Type vendeur : "),"all");
$sql = " SELECT DISTINCT type_id, label FROM  skf_vendor_types 
         ORDER BY label ASC";
$rs = dbQueryAll($sql); 
$typevendorfilter->addElement('all','all');
foreach( $rs as $R){
    $typevendorfilter->addElement($R['type_id'],$R['label'] );
}


$creditspendfilter = new FilterList("creditspendfilter", "A utilisé des crédits :", "all");
$creditspendfilter -> addElement("all", "all");
$creditspendfilter -> addElement("yes", "Oui");
$creditspendfilter -> addElement("no", "Non");

$filter = new Filter("abonnements");
$filter->addFilter($numvendor);
$filter->addFilter($vendeurfilter);
$filter->addFilter($typeabofilter);
$filter->addFilter($typevendorfilter);
$filter->addFilter($creditspendfilter);

$filter->setGridToReload("abonnements");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "commercants.abonnements..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.abonnements..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(           
               ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
    #id_filtercat{width:150px}
    #id_filtercat option{width:150px}
</style>