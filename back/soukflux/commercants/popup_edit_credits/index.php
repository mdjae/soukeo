<div id="myModalCredits" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Modifier les crédits de <?php echo $vendor->getVendor_nom() . " : " . $vendor->getSolde_credit() . " crédit(s)"?></h3>
  </div>
  <div class="modal-body">
      <div class="tabbable tabs"> <!-- Only required for left/right tabs -->
          <ul class="nav nav-tabs">
            <li class="active"><a href="#select_tab" data-toggle="tab">Sélection</a></li>
            <li><a href="#custom_tab" data-toggle="tab">Montant spécifique</a></li>
          </ul>
          <div class="tab-content">
              
            <div class="tab-pane active" id="select_tab">
                <form class="form-horizontal" id="form_add_customer">

                    <div class="control-group">          
                        <label class="control-label" for="inputTransacType">Choisir un type de transaction :</label>
                            <div class="controls">
                                <select id="inputTransacType" name="inputTransacType">
                                    <?php foreach($transactionTypes as $transactionType){ ?>
                                        <option value="<?php echo $transactionType -> getPack_id() ?>"><?php echo ucfirst($transactionType -> getLabel()) ." ( " . $transactionType -> getValue(). " crédits )"?></option>
                                    <?php } ?>
                                </select>
                            </div>
                    </div>
                    
                    <div class="control-group">          
                        <label class="control-label" for="inputPaymentType">Choisir un type de paiement :</label>
                            <div class="controls">
                                <select id="inputPaymentType" name="inputPaymentType">
                                    <?php foreach($paymentTypes as $paymentType){ ?>
                                        <option value="<?php echo $paymentType -> getPayment_type_id() ?>"><?php echo ucfirst($paymentType -> getLabel()) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                    </div>
                                        
                    
                    <button class="btn btn-primary" onClick="validModifCredits('select', '<?php echo $vendor -> getVendor_id() ?>');return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Enregistrer</button>
                    <button type="reset" class="btn btn-inverse">Reset</button> 
                </form>
            </div>
              
            <div class="tab-pane" id="custom_tab">
                <form class="form-horizontal" id="form_add_customer">
                    <em style='color:red'>Attention ce mode d'ajout de crédit n'est pas encore facturé automatiquement.</em>
                    <div class="control-group">
                        <label class="control-label" for="inputCustomMt">Montant spécifique :</label>
                        <div class="controls">
                            <input type="text" id="inputCustomMt" placeholder="Entier positif ou négatif pour débit" name="inputCustomMt" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputCustomLabel">Libellé :</label>
                        <div class="controls">
                            <input type="text" id="inputCustomLabel" placeholder="Libellé (obligatoire)" name="inputCustomLabel" >
                        </div>
                    </div>
                    <button class="btn btn-primary" onClick="validModifCredits('custom', '<?php echo $vendor -> getVendor_id() ?>');return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Enregistrer</button>
                    <button type="reset" class="btn btn-inverse">Reset</button> 
            </div>
          </div>
        </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
