<?php


$facturecode  = new FilterText("facturecode", ("N° Facture")." :", $_GET['id'], false, 12);

$numvendor  = new FilterText("numvendor", ("N° Vendeur")." :", '', false, 12);

// RECHERCHE FILTER CATEGORY
$commfilter = new FilterList("commfilter", ("Choix du commerçant :"), "all");
$sql = " SELECT DISTINCT po.vendor_id, v.VENDOR_NOM FROM  skf_po as po INNER JOIN
         sfk_vendor v ON v.VENDOR_ID = po.vendor_id 
         WHERE po.vendor_id <> 24  
         ORDER BY v.VENDOR_NOM ASC";
$rs = dbQueryAll($sql);  
$commfilter->addElement('all','all');

foreach( $rs as $R){
    $commfilter->addElement($R['vendor_id'],$R['VENDOR_NOM'] );
}



$datelimitfilter = new FilterList("datelimitfilter", ("Date limite jusqu'au :"), "all");
$sql = " SELECT DISTINCT date_limit from skf_factures_com ORDER BY date_limit DESC";
$rs = dbQueryAll($sql);  
$datelimitfilter -> addElement('all', 'all');

foreach ($rs as $R) {
	$datelimitfilter->addElement($R['date_limit'], smartDateNoTime($R['date_limit']));
}

$filter = new Filter("historique_factures_com");
$filter->addFilter($facturecode);
$filter->addFilter($numvendor);
$filter->addFilter($commfilter);
$filter->addFilter($datelimitfilter);

$filter->setGridToReload("historique_factures_com");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "commercants.historique_factures_com..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.historique_factures_com..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.historique_factures_com..",
                "picto" => "download",
                "name" =>("Telecharger sélection"),
                "url" => "javascript: downloadAllFact();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.historique_factures_com..",
                "picto" => "mail",
                "name" =>("Envoyer les mails pour la sélection"),
                "url" => "javascript: sendAllFactMail();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "commercants.historique_factures_com..",
                "picto" => "trash",
                "name" =>("Supprimer la sélection"),
                "url" => "javascript: supprAllFactCom();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
          
          
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(			
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>