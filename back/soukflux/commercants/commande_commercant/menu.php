<?php

$commfilter = new FilterList("commfilter", ("Choix du commerçant :"), "all");
$sql = " SELECT DISTINCT po.vendor_id, v.VENDOR_NOM FROM  skf_po as po INNER JOIN
         sfk_vendor v ON v.VENDOR_ID = po.vendor_id 
         WHERE po.vendor_id <> 24  
         ORDER BY v.VENDOR_NOM ASC";
$rs = dbQueryAll($sql);  
$commfilter->addElement('all','all');

foreach( $rs as $R){
    $commfilter->addElement($R['vendor_id'],$R['VENDOR_NOM'] );
}


// RECHERCHE FILTER CATEGORY
$statusfilter = new FilterList("statusfilter", ("Choix du statut :"), "all");
$sql = " SELECT DISTINCT order_status FROM  skf_orders ORDER BY order_status ASC";
$rs = dbQueryAll($sql);  
$statusfilter->addElement('all','all');

foreach( $rs as $R){
    $statusfilter->addElement($R['order_status'],$R['order_status'] );
}


// RECHERHCE PAR NOM DE PRODUIT
$numCommande  = new FilterText("numfilter", ("N° de commande")." :", $_GET['id'], false, 12);
 
//Litiges
$litige = new FilterList("litigefilter", ("Commande en litige :"),"all");
$litige->addElement('1','litige');
$litige->addElement('0','pas litige');
$litige->addElement('all','all');

//Date
$date = new FilterList("datefilter", ("Date :"),"all");
$date->addElement('2','Récent');
$date->addElement('3','3 derniers jours');
$date->addElement('7','7 derniers jours');
$date->addElement('all','Depuis le début');


$filter = new Filter("commande_avahis");
$filter->addFilter($commfilter);
$filter->addFilter($statusfilter);
$filter->addFilter($numCommande);
$filter->addFilter($litige);
$filter->addFilter($date);

$filter->setGridToReload("commande_commercant");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "commercants.commande_commercant..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "commercants.commande_commercant..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("commercants.commande..","","denied",("Déclarer un litige")),
				array("commercants.commande..","","check",("Annuler un litige")), 	
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
