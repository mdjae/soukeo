Windoo 1.2 beta
copyright (c) 2007 Yevgen Gorshkov

Maintained by Scott Murphy <scottland6688 at gmail.com>

Yevgen wrote the code originally for Mootools v1.1.  I have *passively*
upgraded it to support the Mootools 1.2 release.

Please note: I have *not* spent a lot (aka any) time testing it.

Please report any bugs at the project home page:

http://code.google.com/p/windoo/

Please include:

a working example of the bug.

and if possible, a fix--if you want it fixed quickly.


Enjoy!

Scott


Open Source MIT License

Mootools is (c) 2006-2008 Valerio Proietti and can be downloaded @
http://www.mootools.net/