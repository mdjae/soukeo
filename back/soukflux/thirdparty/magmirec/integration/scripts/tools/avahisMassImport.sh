#!/bin/bash
PATHCLI=/var/www/soukflux/thirdparty/magmi/cli
DIR=/opt/soukeo/export/*
DIRARCHIVE=/opt/soukeo/export/archives

for f in $DIR
do
  echo "Processing import $f file..."
  # take action on each file. $f store current file name
  php $PATHCLI/magmi.cli.php -mode=create -CSV:filename="$f";
done

if [ -d "$ARCHIVE" ]; 
    then
        echo "$DIRARCHIVE existe !"
        mv *.csv $DIRARCHIVE
    else
        cd $DIR
        mkdir archives
        mv *.csv $DIRARCHIVE
    fi