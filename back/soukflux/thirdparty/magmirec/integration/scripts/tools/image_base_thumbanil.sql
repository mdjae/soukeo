UPDATE catalog_product_entity_media_gallery AS mg,
catalog_product_entity_media_gallery_value AS mgv,
catalog_product_entity_varchar AS ev
SET ev.value = mg.value
WHERE  mg.value_id = mgv.value_id
AND mg.entity_id = ev.entity_id
AND ev.attribute_id IN (85, 86, 87)
AND mgv.position = 1


require 'app/Mage.php';
Mage::app();

$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
foreach ($products as $product) {
    if (!$product->hasImage()) continue;
    if (!$product->hasSmallImage()) $product->setSmallImage($product->getImage());
    if (!$product->hasThumbnail()) $product->setThumbnail($product->getImage());
    $product->save();
}


INSERT INTO catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) 
SELECT 4, 85, 0, cpev.entity_id, cpemg.value 
FROM catalog_product_entity_varchar AS cpev 
INNER JOIN catalog_product_entity_media_gallery AS cpemg ON cpev.entity_id = cpemg.entity_id 
WHERE cpev.entity_id NOT IN (SELECT entity_id FROM catalog_product_entity_varchar WHERE entity_type_id = 4 AND attribute_id = 85 AND value IS NOT NULL AND value !=  'no_selection') 
ON DUPLICATE KEY UPDATE entity_type_id = VALUES (entity_type_id)

INSERT INTO catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) 
SELECT 4, 86, 0, cpev.entity_id, cpemg.value 
FROM catalog_product_entity_varchar AS cpev 
INNER JOIN catalog_product_entity_media_gallery AS cpemg ON cpev.entity_id = cpemg.entity_id 
WHERE cpev.entity_id NOT IN (SELECT entity_id FROM catalog_product_entity_varchar WHERE entity_type_id = 4 AND attribute_id = 86 AND value IS NOT NULL AND value !=  'no_selection') 
ON DUPLICATE KEY UPDATE entity_type_id = VALUES (entity_type_id)

INSERT INTO catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, entity_id, value) 
SELECT 4, 87, 0, cpev.entity_id, cpemg.value FROM catalog_product_entity_varchar AS cpev 
INNER JOIN catalog_product_entity_media_gallery AS cpemg ON cpev.entity_id = cpemg.entity_id 
WHERE cpev.entity_id NOT IN (SELECT entity_id FROM catalog_product_entity_varchar WHERE entity_type_id = 4 AND attribute_id = 87 AND value IS NOT NULL AND value !=  'no_selection') 
ON DUPLICATE KEY UPDATE entity_type_id = VALUES (entity_type_id)