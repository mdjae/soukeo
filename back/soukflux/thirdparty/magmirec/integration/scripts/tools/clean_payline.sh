#!/bin/bash

echo "Start"

rm -R /var/www/magento/app/code/community/Monext/Payline
rm -R /var/www/magento/app/design/adminhtml/default/default/layout/payline.xml
rm -R /var/www/magento/app/design/adminhtml/default/default/template/payline

rm -R /var/www/magento/app/design/frontend/default/default/layout/payline.xml
rm -R /var/www/magento/app/design/frontend/default/default/template/payline
rm -R /var/www/magento/app/etc/modules/Monext_Payline.xml
rm -R /var/www/magento/skin/frontend/default/default/css/payline.css
rm -R /var/www/magento/skin/frontend/default/default/images/payline_moyens_paiement
rm -R /var/www/magento/skin/frontend/default/default/images/ae.gif
rm -R /var/www/magento/skin/frontend/default/default/images/amex.gif
rm -R /var/www/magento/skin/frontend/default/default/images/cb.gif
rm -R /var/www/magento/skin/frontend/default/default/images/mastercard.gif
rm -R /var/www/magento/skin/frontend/default/default/images/mc.gif
rm -R /var/www/magento/skin/frontend/default/default/images/vi.gif
rm -R /var/www/magento/skin/frontend/default/default/images/visa.gif
rm -R /var/www/magento/skin/frontend/default/default/images/payline-logo.png


rm -R /var/www/magento/app/locale/en_US/Monext_Payline.csv
rm -R /var/www/magento/app/locale/fr_FR/Monext_Payline.csv
rm -R /var/www/magento/maintenance.flag


echo "Stop"


