-- Reset products
SET FOREIGN_KEY_CHECKS=0;
TRUNCATE catalog_product_bundle_option;
TRUNCATE catalog_product_bundle_option_value;
TRUNCATE catalog_product_bundle_price_index;
TRUNCATE catalog_product_bundle_selection;
TRUNCATE catalog_product_bundle_selection_price;
TRUNCATE catalog_product_bundle_stock_index;
TRUNCATE catalog_product_enabled_index;
TRUNCATE catalog_product_entity;
TRUNCATE catalog_product_entity_datetime;
TRUNCATE catalog_product_entity_decimal;
TRUNCATE catalog_product_entity_gallery;
TRUNCATE catalog_product_entity_group_price;
TRUNCATE catalog_product_entity_int;
TRUNCATE catalog_product_entity_media_gallery;
TRUNCATE catalog_product_entity_media_gallery_value;
TRUNCATE catalog_product_entity_text;
TRUNCATE catalog_product_entity_tier_price;
TRUNCATE catalog_product_entity_varchar;
TRUNCATE catalog_product_flat_1;
TRUNCATE catalog_product_flat_4;
TRUNCATE catalog_product_index_eav;
TRUNCATE catalog_product_index_eav_decimal;
TRUNCATE catalog_product_index_eav_decimal_idx;
TRUNCATE catalog_product_index_eav_decimal_tmp;
TRUNCATE catalog_product_index_eav_idx;
TRUNCATE catalog_product_index_eav_tmp;
TRUNCATE catalog_product_index_group_price;
TRUNCATE catalog_product_index_price;
TRUNCATE catalog_product_index_price_bundle_idx;
TRUNCATE catalog_product_index_price_bundle_opt_idx;
TRUNCATE catalog_product_index_price_bundle_opt_tmp;
TRUNCATE catalog_product_index_price_bundle_sel_idx;
TRUNCATE catalog_product_index_price_bundle_sel_tmp;
TRUNCATE catalog_product_index_price_bundle_tmp;
TRUNCATE catalog_product_index_price_cfg_opt_agr_idx;
TRUNCATE catalog_product_index_price_cfg_opt_agr_tmp;
TRUNCATE catalog_product_index_price_cfg_opt_idx;
TRUNCATE catalog_product_index_price_cfg_opt_tmp;
TRUNCATE catalog_product_index_price_downlod_idx;
TRUNCATE catalog_product_index_price_downlod_tmp;
TRUNCATE catalog_product_index_price_final_idx;
TRUNCATE catalog_product_index_price_final_tmp;
TRUNCATE catalog_product_index_price_idx;
TRUNCATE catalog_product_index_price_opt_agr_idx;
TRUNCATE catalog_product_index_price_opt_agr_tmp;
TRUNCATE catalog_product_index_price_opt_idx;
TRUNCATE catalog_product_index_price_opt_tmp;
TRUNCATE catalog_product_index_price_tmp;
TRUNCATE catalog_product_index_tier_price;
TRUNCATE catalog_product_index_website;
TRUNCATE catalog_product_link;
TRUNCATE catalog_product_link_attribute;
TRUNCATE catalog_product_link_attribute_decimal;
TRUNCATE catalog_product_link_attribute_int;
TRUNCATE catalog_product_link_attribute_varchar;
TRUNCATE catalog_product_option;
TRUNCATE catalog_product_option_price;
TRUNCATE catalog_product_option_title;
TRUNCATE catalog_product_option_type_price;
TRUNCATE catalog_product_option_type_title;
TRUNCATE catalog_product_option_type_value;
TRUNCATE catalog_product_relation;
TRUNCATE catalog_product_super_attribute;
TRUNCATE catalog_product_super_attribute_label;
TRUNCATE catalog_product_super_attribute_pricing;
TRUNCATE catalog_product_super_link;
TRUNCATE catalog_product_website;
TRUNCATE cataloginventory_stock_item; 
TRUNCATE catalog_category_flat_store_1;
TRUNCATE catalog_category_flat_store_4;
TRUNCATE TABLE `catalog_category_product`;
TRUNCATE TABLE `catalog_category_product_index`;
ALTER TABLE catalog_product_flat_1 AUTO_INCREMENT = 1;
ALTER TABLE catalog_product_flat_4 AUTO_INCREMENT = 1;
ALTER TABLE catalog_product_entity AUTO_INCREMENT = 1;
SET FOREIGN_KEY_CHECKS=1;


--Reset categories
SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE `catalog_category_entity`;
TRUNCATE TABLE `catalog_category_entity_datetime`;
TRUNCATE TABLE `catalog_category_entity_decimal`;
TRUNCATE TABLE `catalog_category_entity_int`;
TRUNCATE TABLE `catalog_category_entity_text`;
TRUNCATE TABLE `catalog_category_entity_varchar`;

 
INSERT INTO `catalog_category_entity` (`entity_id`, `entity_type_id`, `attribute_set_id`, `parent_id`, `created_at`, `updated_at`, `path`, `position`, `level`, `children_count`) VALUES
(1, 3, 0, 0, '2013-05-07 11:49:29', '2013-05-07 11:49:29', '1', 0, 0, 1),
(2, 3, 3, 1, '2013-05-07 11:49:29', '2013-05-29 04:50:55', '1/2', 1, 1, 0);


INSERT INTO `catalog_category_entity_varchar` (`value_id`, `entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES
(1, 3, 41, 0, 1, 'Root Catalog'),
(2, 3, 41, 1, 1, 'Root Catalog'),
(3, 3, 43, 1, 1, 'root-catalog'),
(4, 3, 41, 0, 2, 'Default categorie'),
(5, 3, 41, 1, 2, 'Default Category'),
(7, 3, 43, 1, 2, 'default-category');


SET FOREIGN_KEY_CHECKS=1;
