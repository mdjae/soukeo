DELETE FROM `catalog_category_entity`  
WHERE entity_id NOT IN (SELECT category_id FROM catalog_category_product); 
AND NOT IN (0,1,2)


-- Suppression product associer à des categories vides
DELETE FROM `catalog_category_product` WHERE 
category_id not in (select entity_id from catalog_category_entity)