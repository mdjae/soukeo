<?php

/* SOUKEO SAS
 * @author : Silicon Village Team (mamode@silicon-village.fr)
 * @package : Avahis
 */

class QtyPrice extends Magmi_ItemProcessor {
    
    private $_debug = 1;
    private $_debugLevel = "info";
    private $_debugMode = PHP_SAPI;
    private $_countLines = 1;
    private $_img = 0;

    public function getPluginInfo() {
        return array(
            "name" => "SOUKEO Import Qty Price",
            "author" => "Soukeo",
            "version" => "0.0.1",
            "url" => "http://sourceforge.net/apps/mediawiki/magmi/index.php?title=#"
        );
    }

    public function debugMsg($debug, $debugLevel, $debugMode, $msg) {
        if ($debug != 0) {
            if ($debugMode === "CLI") {
                echo '[' . $debugLevel . '] - ' . $msg . PHP_EOL;
            } else {
                $this->log($msg . PHP_EOL, $debugLevel);
            }
        }
    }

    public function infoLoading() {
        // Information sur les lignes imports  
        //$csv = explode("=",$_SERVER['argv'][2]);
        //$res = $csv[1];
        //$lines = count(file($res));
        //TODO
        $lines = "-NC-";
        $res = "-NC-";
        $cnt = $this->_countLines++;
        return $this->debugMsg($this->_debug, $this->_debugLevel, $this->_debugMode, "$cnt sur :: $lines lignes dans :: $res");
    }

    public function AdjustImg() {

        $qBaseImg = "INSERT INTO catalog_product_entity_varchar (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) 
        SELECT 4, 85, 0, cpev.entity_id, cpemg.value 
        FROM catalog_product_entity_varchar AS cpev 
        INNER JOIN catalog_product_entity_media_gallery AS cpemg ON cpev.entity_id = cpemg.entity_id 
        WHERE cpev.entity_id NOT IN (SELECT `entity_id` FROM catalog_product_entity_varchar WHERE `entity_type_id` = 4 AND `attribute_id`= 85 AND value IS NOT NULL AND `value` !=  'no_selection') 
        ON DUPLICATE KEY UPDATE `entity_type_id` = VALUES (entity_type_id)";

        $qThumbnailImg = "INSERT INTO catalog_product_entity_varchar (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) 
        SELECT 4, 86, 0, cpev.entity_id, cpemg.value 
        FROM catalog_product_entity_varchar AS cpev 
        INNER JOIN catalog_product_entity_media_gallery AS cpemg ON cpev.entity_id = cpemg.entity_id 
        WHERE cpev.entity_id NOT IN (SELECT `entity_id` FROM catalog_product_entity_varchar WHERE `entity_type_id` = 4 AND `attribute_id` = 86 AND value IS NOT NULL AND `value` !=  'no_selection') 
        ON DUPLICATE KEY UPDATE `entity_type_id` = VALUES (entity_type_id)";

        $qSmallImg = "INSERT INTO catalog_product_entity_varchar (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) 
        SELECT 4, 87, 0, cpev.entity_id, cpemg.value FROM catalog_product_entity_varchar AS cpev 
        INNER JOIN catalog_product_entity_media_gallery AS cpemg ON cpev.entity_id = cpemg.entity_id 
        WHERE cpev.entity_id NOT IN (SELECT `entity_id` FROM catalog_product_entity_varchar WHERE `entity_type_id` = 4 AND `attribute_id` = 87 AND value IS NOT NULL AND `value` !=  'no_selection') 
        ON DUPLICATE KEY UPDATE `entity_type_id` = VALUES (entity_type_id)";


        $this->insert($qBaseImg);
        $this->insert($qThumbnailImg);
        $this->insert($qSmallImg);

        return true;
    }

    public function createAssocQtyPriceVendor($vendor_id, $product_id, $vendor_sku, $stock_qty, $vendor_price) {
        $data = array($vendor_id, $product_id, $vendor_sku, $stock_qty, $vendor_price, $stock_qty, $vendor_price);
        $q = "INSERT INTO udropship_vendor_product (`vendor_id`, `product_id`, `priority`, `vendor_sku`, `vendor_cost`, `stock_qty`, `backorders`, `status`, `vendor_title`, `vendor_price`, `state`, `special_price`, `special_from_date`, `special_to_date`, `state_descr`, `freeshipping`) VALUES 
            (?,?, '9999',?, NULL, ?, '0', '1', NULL,?, 'new', NULL, NULL, NULL, NULL, '0') ON DUPLICATE KEY UPDATE stock_qty =?, vendor_price =?";
        $liid = $this->insert($q, $data);

        unset($q);
        unset($data);
        return $liid;
    }

    public function processItemBeforeId(&$item, $params = null) {
        // Recuperer l'ID du produit
        $msg = "--Udropship_Vendor" . $item['udropship_vendor'];
        $this->debugMsg($this->_debug, $this->_debugLevel, $this->_debugMode, $msg);
        $sku = $item['sku'];
        $q = "SELECT * FROM `catalog_product_entity` WHERE (`catalog_product_entity`.`sku` = ?)";
        $product_id = $this->selectone($q, $sku, 'entity_id');

        $msg = "Product_id" . $product_id . '=== Qty ' . $item['qty'];
        $this->debugMsg($this->_debug, $this->_debugLevel, $this->_debugMode, $msg);

        $this->createAssocQtyPriceVendor($item['udropship_vendor'], $product_id, $item['sku'], $item['qty'], $item['price']);
        
        if($this->_img == 0){
            $this->AdjustImg();
            $this->_img = 1;
        }


        return true;
    }

}

?>
