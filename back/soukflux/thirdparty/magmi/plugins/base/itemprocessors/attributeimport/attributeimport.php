<?php

class AttributImporter extends Magmi_ItemProcessor 
{	
    private $_debug = 1;
    private $_debugLevel = "info";
    private $_debugMode = PHP_SAPI;
    private $_countLines = 1;

    public function getPluginInfo() {
        return array(
            "name" => "SOUKEO Import Atribute",
            "author" => "Soukeo",
            "version" => "0.1.5",
            "url" => "http://sourceforge.net/apps/mediawiki/magmi/index.php?title=#"
        );
    }
    
    public function debugMsg($debug,$debugLevel,$debugMode,$msg){
        if ($debug != 0){
            if ($debugMode === "CLI"){
                echo '['.$debugLevel.'] - '.$msg.PHP_EOL;
                
            } else {
                $this->log($msg.PHP_EOL, $debugLevel);
            }
        }
    }

    public function getEntityTypeData() {
        // Recuperer id_type pour product_catalog
        $eet = $this->tablename("eav_entity_type");
        $sqlEet = "SELECT * FROM $eet WHERE entity_type_code = ?";
        $rowset = $this->selectAll($sqlEet, array('catalog_product'));
        $r = $rowset[0];
        return $r;
    }

    public function countAttributeSet($name) {
        $eas = $this->tablename("eav_attribute_set");
        $sql = "SELECT * FROM $eas WHERE attribute_set_name = ?";
        $set = $this->selectAll($sql, $name);
        $r = count($set);
        return $r;
    }

    public function getAttributeSetId($name) {
        $eas = $this->tablename("eav_attribute_set");
        $sql = "SELECT * FROM $eas WHERE attribute_set_name = ?";
        $rowset = $this->selectAll($sql, $name);
        $r = $rowset[0]['attribute_set_id'];
        return $r;
    }

    public function getAttributeSetData($name) {
        $id = $this->getAttributeSetId($name);
        $eag = $this->tablename("eav_attribute_group");

        $q = "SELECT * FROM $eag WHERE `attribute_set_id` =? AND `attribute_group_name` = 'Avahis'";
        $rowset = $this->selectAll($q, $id);
        $r = $rowset[0];
        
        unset($q);
        return $r;
    }

    public function createAttributeSet($name) {
        $msg = "function createAttributeSet : ".$name;
        $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
        
        // Recuperer entiy_type_id et autres infos pour product_catlaog
        $rowset = $this->getEntityTypeData();
        
        $data[] = $rowset['entity_type_id'];
        $data[] = $name;
        
        $sqlAttrSet = "INSERT INTO eav_attribute_set (`entity_type_id` ,`attribute_set_name` ,`sort_order`)
                      VALUES (?,?, '0')";
        $attrSetLastId = $this->insert($sqlAttrSet,$data);

        $qEagAvahis = "INSERT INTO `eav_attribute_group` (`attribute_set_id`, `attribute_group_name`, `sort_order`, `default_id`) VALUES
			(?, 'Avahis', 9, 0) ON DUPLICATE KEY UPDATE attribute_group_name = VALUES (attribute_group_name)";
        $arrLiid = $this->insert($qEagAvahis, $attrSetLastId);
        
        //Copie des attributs par defaut
        $this->copyAttributeSetDefault($attrSetLastId);
       
        unset($data);
        return $arrLiid;
    }

    
    public function beforeImport() {}
    
    public function initialize($params){}
    
        
    public function copyAttributeSetDefault($attrSetLastId) {
        $arrEntityType = $this->getEntityTypeData();
        $eea = $this->tablename("eav_entity_attribute");

        //Recupere ID de attribute_set Dfault
        $q = "SELECT `attribute_set_id` FROM `eav_attribute_set` WHERE attribute_set_name = 'Default' AND entity_type_id = ?";
        $rowset = $this->selectAll($q, $arrEntityType['entity_type_id']);
        $defaultId = $rowset[0]['attribute_set_id'];

        $qSelectGroup = "SELECT * FROM `eav_attribute_group` WHERE attribute_set_id = ? ORDER BY  `eav_attribute_group`.`sort_order` ASC ";
        $res = $this->selectAll($qSelectGroup, $defaultId);

        $allData = array();
        $sort_order = '1';

        foreach ($res as $value) {
            $tmp = array();
            $qSelectEea = "SELECT * FROM $eea WHERE `attribute_group_id`= ?";
            $r = $this->selectAll($qSelectEea, $value['attribute_group_id']);
            $n = $value['attribute_group_id'];
            $grpName = $value['attribute_group_name'];

            foreach ($r as $v) {
                $tmp[] = $v['attribute_id'];
            }
            
            
            $allData[$n] = $tmp;
            if ($grpName == 'General'){
                    $qInsertEag = "INSERT INTO `eav_attribute_group` (`attribute_set_id`, `attribute_group_name`, `sort_order`, `default_id`) VALUES
                              (?, ?, 1, 1) ON DUPLICATE KEY UPDATE attribute_group_name = VALUES (attribute_group_name)";
                    $arrLiid[$n] = $this->insert($qInsertEag,array($attrSetLastId,$grpName));
            } else {
                    $qInsertEag = "INSERT INTO `eav_attribute_group` (`attribute_set_id`, `attribute_group_name`, `sort_order`, `default_id`) VALUES
                              (?, ?, ?, 0) ON DUPLICATE KEY UPDATE attribute_group_name = VALUES (attribute_group_name)";
                    $arrLiid[$n] = $this->insert($qInsertEag,array($attrSetLastId,$grpName,$sort_order));
                
            }
            $sort_order++;
        }
        
        //Inserer dans Eav_entity_attribute

        foreach ($allData as $key => $value) {
            foreach ($value as $k => $v) {
                $dataInsertEea = array($arrEntityType['entity_type_id'], $attrSetLastId, $arrLiid[$key], $v, $k);
                $sqlInsertEea = "INSERT INTO $eea (`entity_attribute_id`, `entity_type_id`, `attribute_set_id`, `attribute_group_id`, `attribute_id`, `sort_order`) 
				VALUES ('',?,?,?,?,?)  ON DUPLICATE KEY UPDATE attribute_id = VALUES (attribute_id)";
                $this->insert($sqlInsertEea, $dataInsertEea);
                
                //$msg = "INSERT EEA sql " . $sqlInsertEea." and data= ".$arrEntityType['entity_type_id']." -- ".$attrSetLastId." -- ".$arrLiid[$key]." -- ".$v." -- ".$k;
                //$this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
            }
        }
    }

    
    

    public function prepareDataItem($arr) {
        $part1 = array_slice($arr, 0, 1);
        $r1 = array("store" => "admin", "websites" => "base", "type" => "simple");
        $part2 = array_slice($arr, 1, 5);
        if(isset($arr['qty']) && $arr['qty'] != 0 && !empty($arr['qty'])){
                $r2 = array("tax_class_id" => "8,50% Tva Réunion");
            } else {
                $r2 = array("tax_class_id" => "8,50% Tva Réunion");
        }
        
        $part3 = array_slice($arr, 6, 1);
        $part4 = array_slice($arr, 6, count($arr) - 6);

        $r = array_merge($part1, $r1, $part2, $r2, $part3, $part4);

       // $msg = "qty / price" . $r['qty'] . "----" . $r['price'];
       //$this->debugMsg($this->_debug, $this->_debugLevel, $this->_debugMode, $msg);
        
        return $r;
    }
    
    public function infoLoading(){
                // Information sur les lignes imports  
        //$csv = explode("=",$_SERVER['argv'][2]);
        //$res = $csv[1];
        //$lines = count(file($res));
        
        //TODO
        $lines = "-NC-";
        $res = "-NC-";
        $cnt = $this->_countLines++;
        return $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,"$cnt sur :: $lines lignes dans :: $res");
        
    }
    
    public function maxIdEa(){
           $q = "SELECT MAX(`attribute_id`) AS ID FROM `eav_attribute` ";
           $res = $this->selectAll($q);
           $attrLastId = $res[0]['ID'];
           
           return $attrLastId;
    }
    
    public function attrExist($attribute_code){
        $ea = $this->tablename("eav_attribute");
        $q= "SELECT `attribute_code` FROM $ea WHERE `attribute_code` =?";
        $attrExist = count($this->selectAll($q,$attribute_code));
        
        return $attrExist;
    }
    
    public function processItemBeforeId(&$item, $params = null) {

        $this->infoLoading();
        $item = $this->prepareDataItem($item);

        // Si attribute_set existe dans CSV
        if (isset($item['attribute_set']) && $item['attribute_set'] != "") {
            $rowCount = $this->countAttributeSet($item['attribute_set']);
            // Si rowCount > O, le set existe déja dans Magento
            if ($rowCount < 1) {
                // Le set n'existe pas donc création
                $msg = "start create attribute_set " . $rowCount;
                $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
                $arrLiidSet = $this->createAttributeSet($item['attribute_set']);
                $msg = "le set existe num ".$arrLiidSet." - ". $rowCount . PHP_EOL;
                $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
            } 

                // Le set existe, Boucle sur les attribut
                $itemKeys = array_keys($item);
                $pos = array_search('attribute_set', $itemKeys);

                $arrAttrDefault = array_slice($item, 0, $pos);
                $arrAttrCustom = array_diff_key($item, $arrAttrDefault);

                $ea = $this->tablename("eav_attribute");
                $eea = $this->tablename("eav_entity_attribute");
                $cea = $this->tablename("catalog_eav_attribute");
                $eag = $this->tablename("eav_attribute_group");
                
                foreach ($arrAttrCustom as $key => $value) {
                        //$key fournie le nom de l'attribut
                        if(!empty($key) && ($key != 'attribute_set') && ($key != 'udropship_vendor') && ($key != 'tax_class_id')){
                            $attrExist = $this->attrExist($key);
                        } 
                        else {
                            $attrExist =1;
                        }
                        
                        
                        if ($attrExist == 0 && !empty($key) && ($key != 'attribute_set')) {
                            // Attribut n'existe pas
                            $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,"AttrCustom : ".$attrExist. PHP_EOL);

                            // Creation attribut et ajout attribvt dans le set 
                            $rowset = $this->getEntityTypeData();
                            $attrSet = $this->getAttributeSetData($item['attribute_set']);

                            $sqlInsertAttr = "INSERT INTO $ea (`entity_type_id`,`attribute_code` ,`attribute_model`,`backend_model`,`backend_type`,`backend_table`,`frontend_model`,`frontend_input`,
                                                                            `frontend_label`,`frontend_class` ,`source_model` ,`is_required` ,`is_user_defined` ,`default_value` ,`is_unique` ,`note`)
                                                                            VALUES (?,?,NULL,'','varchar','','','multiselect',?,'','','0','1','','0','') ON DUPLICATE KEY UPDATE attribute_code = VALUES (attribute_code)";


                                if($key != "udropship_vendor" && $key != "volume" && $key != "tax_class_id" ){
                                    $dataInsertAttr = array($rowset['entity_type_id'], $key, ucfirst(substr($key,3)));
                                    $attrLastId = $this->insert($sqlInsertAttr, $dataInsertAttr);

                                    $msg = "INSERT EA ".$rowset['entity_type_id']."//".$key;
                                    $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
                                }

                                if ($attrLastId == 0) { 
                                    $attrLastId = $this->maxIdEa();
                                }

                                
                                
                            $sqlInsertCea = "INSERT INTO $cea (`attribute_id`,`frontend_input_renderer`,`is_global`,`is_visible`,`is_searchable`,`is_filterable`,`is_comparable`, 
                                                               `is_visible_on_front`,`is_html_allowed_on_front`,`is_used_for_price_rules`,`is_filterable_in_search`,`used_in_product_listing`,`used_for_sort_by`, 
                                                               `is_configurable`,`apply_to`,`is_visible_in_advanced_search`,`position`,`is_wysiwyg_enabled`,`is_used_for_promo_rules`) 
                                                                VALUES (?, '', 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, '', 0, 0, 0, 0) ON DUPLICATE KEY UPDATE attribute_id = VALUES (attribute_id)";
                            //$msg = "INSERT CEA ".$sqlInsertCea."//".$attrLastId;
                            //$this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
                            $this->insert($sqlInsertCea,array($attrLastId));
                            
                           
                            $attrGrpLastId = $attrSet['attribute_group_id'];
                            $sqlInsertEea = "INSERT INTO $eea (`entity_attribute_id`, `entity_type_id`, `attribute_set_id`, `attribute_group_id`, `attribute_id`, `sort_order`) 
                                            VALUES ('',?,?,?,?,0)  ON DUPLICATE KEY UPDATE attribute_id = VALUES (attribute_id)";
                            $dataInsertEea = array($rowset['entity_type_id'], $attrSet['attribute_set_id'], $attrGrpLastId,$attrLastId);
                            $eeaLastInserId = $this->insert($sqlInsertEea, $dataInsertEea);
                        
                        
                         
                        } else {

                        // Attribut existe 
                            

                    }
                }

            return true;
        } else {

            $msg = "attribut_set inexistant dans le csv";
            $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
            return true;
        } 
    }


    

}

?>