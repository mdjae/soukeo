<?php

class BusinessSoukeoModel extends sdb {

	protected $stmtAttrByCatId;
	protected $stmtAttrDynaByProd;

	public function getAllCat() {

		$sql = 'SELECT cat_id, cat_label, cat_data FROM sfk_categorie';
		return $this -> getall($sql);

	}

	public function PrepAttrByCatId() {
		$sql = "SELECT id_attribut,cat_id, code_attr, label_attr FROM sfk_attribut WHERE cat_id = ? ORDER BY id_attribut";
		$this -> stmtAttrByCatId = $this -> prepare($sql);
	}

	public function GetAttrByCatId($catid) {
		$null = null;

		$this -> stmtAttrByCatId -> bindParam(1, $catid);

		$this -> stmtAttrByCatId -> execute();
		return ($rs = $this -> stmtAttrByCatId -> fetchall(PDO::FETCH_ASSOC)) ? $rs : array();
	}

	public function getAllProdByCatId($catid) {
		$sql = "select * from sfk_catalog_product where categories ='$catid' ";
		return $this -> getall($sql);

	}

	public function prepAttrDynaByProd(){
		$sql = "SELECT * FROM sfk_product_attribut WHERE id_produit = ? ";
		$this -> stmtAttrDynaByProd =  $this->prepare($sql) ;
	}

	public function getAttrDynaByProd($idproduit) {
		$this -> prepAttrDynaByProd();
		$this -> stmtAttrDynaByProd  -> bindParam(1, $idproduit ? $idproduit : $null);
		return $this -> getAllPrep($this -> stmtAttrDynaByProd );

	}

	public function getAttrDynaByProdAndAttr($idproduit, $idattribut) {
		$sql = "SELECT value as val  FROM sfk_product_attribut WHERE id_produit = ? AND id_attribut = ? ";
		
				$this -> AttrDynaByProdAndAttr =  $this->prepare($sql) ;
		$this -> AttrDynaByProdAndAttr -> bindParam(1, $idproduit ? $idproduit : $null);
		$this -> AttrDynaByProdAndAttr -> bindParam(2, $idattribut ? $idattribut : $null);
		return $this -> getOnePrep($this -> AttrDynaByProdAndAttr);
		
		
		//return $this -> getOne($sql);

	}
	
	public function prepInsertCat(){
			$sql = "INSERT INTO sfk_categorie (
					cat_id, cat_label  , parent_id , cat_position , cat_level, cat_active )
					VALUES ( ?, ?, ?, ?, ?, 1 )  ON DUPLICATE KEY UPDATE
					cat_label = VALUES(cat_label),
					parent_id = VALUES(parent_id),
					cat_position = VALUES(cat_position),
					cat_level = VALUES(cat_level), 
					cat_active = VALUES(cat_active) 
					
										 ";
			$this->stmtInsertCat = $this->prepare($sql) ;
	}
	
	public function AddCategory($V){
		$null = null;
				
		$this -> stmtInsertCat  -> bindParam(1, $V['cateory_id'] ? $V['cateory_id'] : $null);
		$this -> stmtInsertCat  -> bindParam(2, $V['name']  ? $V['name']  : $null);
		$this -> stmtInsertCat  -> bindParam(3, $V['parent_id'] ? $V['parent_id'] : $null);
		$this -> stmtInsertCat  -> bindParam(4, $V['position'] ? $V['position'] : $null);
		$this -> stmtInsertCat  -> bindParam(5, $V['level'] ? $V['level'] : $null);
		return $this -> stmtInsertCat  -> execute();
	
	}
	
	
	protected function addProductCategorie($id_product, $cat_id) {
		$null = null;
		$this -> stmtProductCat -> bindParam(1, $cat_id ? $cat_id : $null);
		$this -> stmtProductCat -> bindParam(2, $id_product ? $id_product : $null);
		$this -> stmtProductCat -> execute();
	}

	protected function prepProductCategorie() {
		$sql = "INSERT INTO sfk_cat_product (cat_id, id_produit) VALUES ( ?, ?) ";
		$this -> stmtProductCat = self::$db -> prepare($sql);

	}

	protected function prepInsertValAttr() {
		$insValattr = "INSERT INTO sfk_product_attribut  (id_produit, id_attribut, value )
					 				VALUES (?,?,?) ON DUPLICATE KEY UPDATE
					 				value = VALUES(value)
					 
					 ";
		$this -> stmtValAttr = self::$db -> prepare($insValattr);
	}

	public function getIdProduct($sku) {
		$sql = "SELECT id_produit from sfk_catalog_product where sku = '$sku' ";
		return $this -> getone($sql);
	}

	public function addRowValAttr($V) {
		$null = null;
		$this -> stmtValAttr -> bindParam(1, $V['id_produit'] ? $V['id_produit'] : $null);
		$this -> stmtValAttr -> bindParam(2, $V['id_attribut'] ? $V['id_attribut'] : $null);
		$this -> stmtValAttr -> bindParam(3, $V['value'] ? $V['value'] : $null);

		$this -> stmtValAttr -> execute();

	}

	public function prepInsertSfkCp() {

		$sql = "INSERT INTO `sfk_catalog_product` (
					`sku`,`categories`,`name`,`description`,`short_description`,`price`,
					`weight`,`country_of_manufacture`,`meta_description`,`meta_keyword`,
					`meta_title`,`image`,`small_image`,`thumbnail`,`date_create`)
				VALUES ( ?,?,?,?,?, ?,?,?,?,?,?,?,?,?,NOW() )
				ON DUPLICATE KEY UPDATE 
					categories	=VALUES(categories),
					name		=VALUES(name),
					description	=VALUES(description),
					short_description=VALUES(short_description),
					price		=VALUES(price),
					weight		=VALUES(weight),
					country_of_manufacture	=VALUES(country_of_manufacture),
					meta_description=VALUES(meta_description),
					meta_keyword	=VALUES(meta_keyword),
					meta_title		=VALUES(meta_title),
					image			=VALUES(image),
					small_image		=VALUES(small_image),
					thumbnail		=VALUES(thumbnail)
					
				";
		$this -> stmtCatalogProduct = self::$db -> prepare($sql);

	}

	public function addRowCatProduct($V) {
		$null = null;
		$this -> stmtCatalogProduct -> bindParam(1, $V['sku'] ? $V['sku'] : $null);
		$this -> stmtCatalogProduct -> bindParam(2, $V['categories'] ? $V['categories'] : $null);
		$this -> stmtCatalogProduct -> bindParam(3, $V['name'] ? $V['name'] : $null);
		$this -> stmtCatalogProduct -> bindParam(4, $V['description'] ? $V['description'] : $null);
		$this -> stmtCatalogProduct -> bindParam(5, $V['short_description'] ? $V['short_description'] : $null);
		$this -> stmtCatalogProduct -> bindParam(6, $V['price'] ? $V['price'] : $null);
		$this -> stmtCatalogProduct -> bindParam(7, $V['weight'] ? $V['weight'] : $null);
		$this -> stmtCatalogProduct -> bindParam(8, $V['country_of_manufacture'] ? $V['country_of_manufacture'] : $null);
		$this -> stmtCatalogProduct -> bindParam(9, $V['meta_description'] ? $V['meta_description'] : $null);
		$this -> stmtCatalogProduct -> bindParam(10, $V['meta_keyword'] ? $V['meta_keyword'] : $null);
		$this -> stmtCatalogProduct -> bindParam(11, $V['meta_title'] ? $V['meta_title'] : $null);
		$this -> stmtCatalogProduct -> bindParam(12, $V['image'] ? $V['image'] : $null);
		$this -> stmtCatalogProduct -> bindParam(13, $V['small_image'] ? $V['small_image'] : $null);
		$this -> stmtCatalogProduct -> bindParam(14, $V['thumbnail'] ? $V['thumbnail'] : $null);
		//$this -> stmtCatalogProduct -> bindParam( 15, $V['date_create'] ? $V['date_create'] : $null);
		//$this -> stmtCatalogProduct -> bindParam( 16, $V['date_update'] ? $V['date_update'] : $null);

		return $this -> stmtCatalogProduct -> execute();
	}

	protected function formatarr($rs) {
		$arr = array();
		foreach ($rs as $R) {
			$arr[] = $R['ID'];
		}
		return $arr;
	}

	protected function prepAttribut() {
		$sql = "INSERT INTO sfk_attribut ( cat_id , code_attr, label_attr ) 
				values ( ?, ? , ? )";
		$this -> stmtAttr = self::$db -> prepare($sql);

	}

	protected function addAttribut($V) {
		$null = null;
		$this -> stmtAttr -> bindParam(1, $V['cat_id'] ? $V['cat_id'] : $null);
		$this -> stmtAttr -> bindParam(2, $V['code_attr'] ? $V['code_attr'] : $null);
		$this -> stmtAttr -> bindParam(3, $V['label_attr'] ? $V['label_attr'] : $null);
		return $this -> stmtAttr -> execute();
	}
	/**
	 * fonction permettant d'avoir tous les id vendeur ainsi que leur état actif ou pas
	 */
	public function getAllVendor()
	{
		$sql = "SELECT VENDOR_ID,ACTIVE FROM sfk_vendor";
		$result = $this->getAll($sql);
		
		return $result;
	}
	
		
	/**
	 * Cette fonction permet de vérifier si les informations entre les tables
	 * sfk_vendor et sfk_tracking_vendor concordent et si c'est le cas les informations 
	 * de la table sfk_tracking_vendor à propos de ce vendeur sont renvoyés.
	 * @param vendorID l'id du vendeur
	 * @return $result array le résultat de la requete, les informations de tracking
	 */
	public function getInfoTracking($vendorID)
	{
		$sql = "SELECT * FROM sfk_tracking_vendor WHERE VENDOR_ID =".$vendorID." ORDER BY DATE_INSERT LIMIT 1";
		
		if($infoTracking = $this -> getRow($sql)) {
			return $infoTracking;
		}
		else {
			return false;
		}
	}
	
	
	/**
	 * Fonction récursive permettant de retourner sous forme de chaine de caractère 
	 * l'arborescence complète de la catégorie. On va chercher si la catégorie a un parent
	 * pour en chercher le parent, tant que le parent n'a pas l'id "2" qui correspond à 
	 * l'id de la catégorie Racine.
	 * @author 	Philippe_LA
	 * @param 	cat_id Int 		l'id de la catégorie 
	 * @param 	result String 	Le résultat sous forme de chaine de caractère
	 * @return 	result String 	Le résultat sous forme de chaine de caractère
	 */
	public function getStrCateg($cat_id, $result)
	{
		$sql = "SELECT parent_id, cat_label FROM sfk_categorie WHERE cat_id = $cat_id";
		
		$info = $this -> getRow($sql);

		//2 est la catégorie mère point d'arret de la récursive
		if($info['parent_id'] != 2 && $info != null) {
			$result = $this -> getStrCateg($info['parent_id'], $info['cat_label'] . "/" . $result) ;
		}
		else {
			$result = $info['cat_label'] . "/" . $result;
		}
		
		return $result;
	}
	
	/**
	 * Permet d'obtenir tous les attributs présents pour une catégorie pour tous les produits
	 * de cette catégorie.
	 */
	public function getAllAttrByCat($cat_id)
	{
		$sql = "SELECT label_attr FROM sfk_attribut WHERE cat_id = $cat_id";
		$result = $this->getAll($sql);
		
		return $result;
	}
	
	public function getAllAttrByProd($idProd)
	{
		$sql = "SELECT a.label_attr ,pa.value FROM `sfk_product_attribut` as pa 
				INNER JOIN sfk_attribut as a ON a.id_attribut = pa.id_attribut
				WHERE pa.id_produit = $idProd";
				
		$result = $this->getAll($sql);
		
		$aAttrVal = array();
		
		foreach ($result as $row) {
			$aAttrVal[$row['label_attr']] = $row['value'];
		}
		
		return $aAttrVal;
	}
	
	/**
	 * Retourne toutes les catégories possédant des produits
	 */
	public function getAllCatWithProducts()
	{
		$sql = "SELECT DISTINCT categories FROM sfk_catalog_product ";
		
		return $result = $this->getAll($sql);
	}
	
	public function getAllAttrByProdFromCat($cat_id)
	{
		$sql = "SELECT DISTINCT a.label_attr FROM sfk_attribut as a 
		INNER JOIN sfk_product_attribut as pa ON a.id_attribut = pa.id_attribut
		INNER JOIN sfk_catalog_product as cp  ON pa.id_produit = cp.id_produit
		WHERE cp.categories = $cat_id";
		
		$result = $this->getAll($sql);
		return $result ;
	}
	/**
	* F Retourne tous les produits associé avahis/soukflux
	* 
	*/
	public function getAllProdAssoc()
	{
		$sql = "SELECT VENDOR_ID, id_produit_ecommercant FROM sfk_assoc_product";
		
		$result = $this->getAll($sql);
		return $result;
	}
            
    /**
	* F Cette fonction permet de vérifier si les informations entre les tables
	* sfk_assoc_product et sfk_product concordent et si c'est le cas les informations 
	* de la table sfk_product à propos de ce produit sont renvoyés.
	* @param vendorID l'id du vendeur
	* @return $result array le résultat de la requete, les informations de tracking
	*/
	public function getInfoTrackingAssoc($produit,$infoTrackingVendor)
	{
            $software = $infoTrackingVendor['SOFTWARE'];
            $idVendeur = $produit['VENDOR_ID'];
            $idProduitEcom = $produit['id_produit_ecommercant'];
            //suivant la techno utilisé on choisirat la table adéquate 
            if($software=='Prestashop'){//si prestashop
                $sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID ='".$idVendeur."' AND ID_PRODUCT ='".$idProduitEcom."' AND ACTIVE ='1' AND ORDER BY DATE_INSERT LIMIT 1";
                return false; // a complété
            }
            elseif ($software=='Thelia') {//si Thelia
                $sql = "SELECT * FROM sfk_produit_th WHERE VENDOR_ID ='".$idVendeur."' AND ID_PRODUCT ='".$idProduitEcom."' AND ACTIVE ='1' ORDER BY DATE_UPDATE LIMIT 1";
                // Pour la prochaine rectification $sql = "SELECT * FROM sfk_produit_th WHERE VENDOR_ID ='".$idVendeur."' AND ID_PRODUCT_SF ='".$idProduitEcom."' AND ACTIVE ='1' ORDER BY DATE_INSERT LIMIT 1";
            }
            else{}
            if($infoTrackingAssocProduit = $this->getRow($sql)){
                return $infoTrackingAssocProduit;
            }else
            return false;
	}
    /**
	* F Cette fonction permet de vérifier si les informations entre les tables
	* sfk_product_ecommercant_stock_prix et sfk_product concordent(date_update) et si c'est le cas les informations 
	* de la table sfk_product_ecommercant_stock_prix à propos de ce produit sont renvoyés.
	* @param $produit les infos sur ce produit de la table sfk_produit
	* @return $result array le résultat de la requete, les informations de tracking
	*/
	public function verifDate_upBddLocalImport($produit)
	{
            $idVendeur 			= $produit['VENDOR_ID'];
            $date_update_ecom 	= $produit['DATE_UPDATE'];
            $idProduitEcom 		= $produit['ID_PRODUCT'];
            
            $sql 				= "SELECT * FROM sfk_product_ecommercant_stock_prix 
            					   WHERE VENDOR_ID ='".$idVendeur."' 
            					   AND id_produit='".$idProduitEcom."' 
            					   ORDER BY DATE_UPDATE LIMIT 1";
								   
            $infotrackingMajSP = $this -> getRow($sql);
            //var_dump($produit);
            //var_dump($date_update_ecom);
            //var_dump($infotrackingMajSP['date_update']);
            
            if(($date_update_ecom>$infotrackingMajSP['date_update'])||($infotrackingMajSP['date_update']==null)) {//si la date ecom est supérieur a la date table commercan_stock_prix ou si elle n'existe pas 
                return true;
            }else
                return false;
	}
	
	/**
	 * 
	 */
	public function productIsAssoc($vendorID, $idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product WHERE VENDOR_ID = :VENDOR_ID AND id_produit_ecommercant = :id_produit_ecommercant";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> bindValue(':id_produit_ecommercant' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;
	}
	

	public function getValidVendor($vendorID)
	{
		$sql = "SELECT * FROM sfk_tracking_vendor as tv
				INNER JOIN sfk_vendor as v 
				WHERE v.VENDOR_ID = :VENDOR_ID ORDER BY tv.DATE_INSERT LIMIT 1";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID', $vendorID, PDO::PARAM_INT);
		$stmt -> execute();
		
		if($infoTracking = $stmt -> fetch()) {
			return $infoTracking;
		}
		else {
			return false;
		}
	}
	
		/**
	 * F Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif de tous les ecommerçants
	 */
	public function purgeAllInactiveProduct() {
		//Si le produit a été passé en inactif chez le commercant, on l'a recu en inactif coté soukflux
		//si la mise a jour a bien eu lieu alors on va simplement supprimer les produits inactifs de cet ecommercant
		$this -> purgeTheliaInactiveProduct() ;
		$this -> purgePrestashopInactiveProduct() ;
	}
	/**
	 * F Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif dans son Thelia
	 */
	public function purgeTheliaInactiveProduct() {
		$sql = "DELETE  spe  
				FROM sfk_product_ecommercant_stock_prix spe
				INNER JOIN  sfk_produit_th sp 
				ON spe.VENDOR_ID = sp.VENDOR_ID AND spe.id_produit = sp.ID_PRODUCT
				WHERE sp.ACTIVE = 0;";
			
		$this -> exec($sql);
	}
	/**
	 * F Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif dans son Prestatshop
	 */
	public function purgePrestashopInactiveProduct() {
		$sql = "DELETE  spe  
				FROM sfk_product_ecommercant_stock_prix spe
				INNER JOIN  sfk_produit_ps sp 
				ON spe.VENDOR_ID = sp.VENDOR_ID AND spe.id_produit = sp.ID_PRODUCT
				WHERE sp.ACTIVE = 0;";
			
		$this -> exec($sql);
	
	}
	
	
	/**
	 * Idem que fonction getAllProdByCatId sauf que l'on utilie des alias pour le rendre générique
	 * et utiliable dan la vue de la liste de produits
	 */
	public function getAllProdByCatIdAlias($catid, $filter, $brand) {
		$sql = " SELECT id_produit as ID_PRODUCT, name as NAME_PRODUCT, image as IMAGE_PRODUCT, price as PRICE_PRODUCT, description as DESCRIPTION_SHORT, 
						sku as REFERENCE_PRODUCT, country_of_manufacture as MANUFACTURER
				 FROM sfk_catalog_product 
				 WHERE categories = :categories ".$filter;
				 
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":categories", $catid);
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		return $this->getAllPrepa($stmt);

	}
	
	public function getAllDistinctBrandFromCat($catId)
	{
		$sql =" SELECT DISTINCT manufacturer as MANUFACTURER FROM sfk_catalog_product
				WHERE categories = :categories
				AND MANUFACTURER <> ''";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":categories", $catId);

		return $this->getAllPrepa($stmt);
	}
	
	
	
		/**
	 * Ne remonte que les 3 attributs les plus utilisés pour une catégorie et un vendeur donné
	 */
	public function getBestAttrFromCateg($cat)
	{
		$sql =" SELECT COUNT(*) as count, a.id_attribut as ID_ATTR, a.label_attr as LABEL_ATTR FROM sfk_attribut as a
			    INNER JOIN sfk_product_attribut as pa ON a.id_attribut= pa.id_attribut
			    INNER JOIN sfk_catalog_product as p ON pa.id_produit= p.id_produit
				WHERE  p.categories = :cat_id
				GROUP BY a.id_attribut
				ORDER BY count DESC
				LIMIT 3";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":cat_id", $cat);
		
		
		return $this->getAllPrepa($stmt);
	}

	
	public function getAllDisctValueForAttrFromCateg($cat, $idAttr)
	{
		$sql =" SELECT DISTINCT pa.value as VALUE FROM sfk_product_attribut as pa
			    INNER JOIN sfk_attribut as a ON a.id_attribut= pa.id_attribut
				INNER JOIN sfk_catalog_product as p ON pa.id_produit= p.id_produit
				WHERE p.categories =  :categories
				AND a.id_attribut = :id_attribut";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":categories", $cat);
		$stmt -> bindValue(":id_attribut", $idAttr);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	public function verifAttrForProductSkf($idProduct, $idAttr, $valAttr)
	{
		$sql = "SELECT * FROM sfk_product_attribut 
				WHERE 	id_produit = :ID_PRODUCT 
				AND 	id_attribut    = :ID_ATTR
				AND 	value      = :VALUE ";
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct); 
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		$stmt -> bindValue(":VALUE", $valAttr);
		
		$stmt -> execute();
				
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;
	}
	
	public function getSsCatByParentId($id=2) {

		$sql = 'SELECT * FROM sfk_categorie';
		if ($id == 'null'){
		$sql .= ' where parent_id is '.$id;
		}else{
		$sql .= ' where parent_id = '.$id;
		}
		//echo $sql;
		return $this -> getall($sql);
	
	}
	
	
	public function countAllProductInCatRecursive($catId, $count)
	{
		//On va chercher les cat filles
		$sql = "SELECT DISTINCT cat_id FROM sfk_categorie WHERE parent_id = :parent_id";
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":parent_id", $catId);

		$childs = $this->getAllPrepa($stmt);
		
		$tmp=0;
		if(!empty($childs) ){
			foreach ($childs as $child) {
				$tmp += $this->countAllProductInCatRecursive($child['cat_id'], $count);
			}	
		}
		
		//On compte tous les produits dans cette catégorie
		$sql = "SELECT count(*) as count FROM sfk_catalog_product WHERE categories = :categories";
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":categories", $catId);
		$stmt -> execute();
		
		$res = $stmt -> fetch();
		$count = $count + $res['count'];
		
		
		
		return $count + $tmp;
	}
	
	public function insertCountProdForCat($catId, $count)
	{
		$sql = "UPDATE sfk_categorie SET count_prod = $count WHERE cat_id = :cat_id";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":cat_id", $catId);	
		$stmt -> execute();
	}
}
