<?php 
/**
 * Model pour les tables soukflux dédiées à PRESTASHOP
 * 
 * @version 0.1.0
 * @author 	Philippe_LA
 */
class BusinessSoukeoCrawlerModel extends BusinessSoukeoModel {
	
	protected $stmtCatalogProduct;
	protected $stmtProductAttribute;
	protected $stmtAttribute;
	protected $stmtProductCat;
	

	/**
	 * Prépare la requete d'insertion d'un produit dans la table catalog_product 
	 */
	public function prepareInsertSfkCatProduct()
	{
		$sql = "INSERT INTO `sfk_catalog_product` (
							`sku`,`categories`,`name`,`description`,`short_description`,`price`,
							`weight`,`country_of_manufacture`,`meta_description`,`meta_keyword`,
							`meta_title`,`image`,`small_image`,`thumbnail`,`date_create`)
							
				VALUES ( 	:sku, :categories, :name, :description, :short_description, 
							:price, :weight, :country_of_manufacture, :meta_description , 
							:meta_keyword, :meta_title, :image, :small_image, :thumbnail,NOW() )
							
				ON DUPLICATE KEY UPDATE 
							categories				=VALUES(categories),
							name					=VALUES(name),
							description				=VALUES(description),
							short_description 		=VALUES(short_description),
							price					=VALUES(price),
							weight					=VALUES(weight),
							country_of_manufacture	=VALUES(country_of_manufacture),
							meta_description 		=VALUES(meta_description),
							meta_keyword			=VALUES(meta_keyword),
							meta_title				=VALUES(meta_title),
							image					=VALUES(image),
							small_image				=VALUES(small_image),
							thumbnail				=VALUES(thumbnail)";
					
		$this->stmtCatalogProduct = $this->prepare($sql);
	}
	
	/**
	 * Fonction permettant l'insertion ou l'update de produits dans la base de donnée soukflux 
	 * Si le couple VENDOR_ID/ID_PRODUCT existe déjà il y a juste update du produit.
	 * Changements à effectuer dans les champs récupérés, besoin d'affiner
	 */
	public function addRowSfkCatProduct($product, $cat_id)
	{
		$ean = explode(",",$product['EAN']);
		$product['EAN'] = $ean[0];	
		
		$this->stmtCatalogProduct->bindValue(':sku', 					$product['EAN']);
		$this->stmtCatalogProduct->bindValue(':categories', 			isset($product['categories']) ? $product['categories'] : $cat_id);
		$this->stmtCatalogProduct->bindValue(':name', 					$product['nom']);
		$this->stmtCatalogProduct->bindValue(':description', 			$product['desc']);
		$this->stmtCatalogProduct->bindValue(':short_description', 		'NULL');
		$this->stmtCatalogProduct->bindValue(':price', 					$product['price']);
		$this->stmtCatalogProduct->bindValue(':weight',					isset($product['poids']) ? $product['poids'] : 10 );
		$this->stmtCatalogProduct->bindValue(':country_of_manufacture',	'NULL');
		$this->stmtCatalogProduct->bindValue(':meta_description' , 		'NULL');
		$this->stmtCatalogProduct->bindValue(':meta_keyword', 			'NULL');
		$this->stmtCatalogProduct->bindValue(':meta_title', 			isset($product['meta_title']) ? $product['meta_title'] : $product['nom']);
		$this->stmtCatalogProduct->bindValue(':image', 					$product['image']);
		$this->stmtCatalogProduct->bindValue(':small_image', 			'NULL');
		$this->stmtCatalogProduct->bindValue(':thumbnail', 				'NULL');
		
		$this->stmtCatalogProduct->execute();
	}
	
	
	/**
	 * Prépare la requete d'insertion d'un attribut dans sfk_attribut
	 */
	public function prepareInsertSfkAttribute()
	{
		$sql = "INSERT INTO sfk_attribut ( cat_id , code_attr, label_attr ) 
				values ( :cat_id, :code_attr , :label_attr )
				ON DUPLICATE KEY UPDATE
				cat_id = VALUES(cat_id),
				label_attr = VALUES(label_attr)";
				
		$this->stmtAttribute = $this->prepare($sql);
	}
	
	/**
	 * Insertion des relations produits attributs dans les tables dédiées à 
	 * Prestashop de Soukflux 
	 */
	public function addRowSfkAttr($attr, $codCat)
	{
		$codeAttr = trim($attr);
		$codeAttr = str_replace(' ', '_', $codeAttr);
		$codeAttr = strtoupper($this->stripAccents($codeAttr));
			
		$this->stmtAttribute->bindValue(':cat_id', 		$codCat);
		$this->stmtAttribute->bindValue(':code_attr', 	$codeAttr);
		$this->stmtAttribute->bindValue(':label_attr', 	$attr);
		$this->stmtAttribute->execute();
	}
	
	/**
	 * Prépare la requete d'insertion d'un couple produit / attribut dans 
	 * sfk_product_attribut
	 */
	public function prepareInsertSfkProductAttribute() {
		$sql = "INSERT INTO sfk_product_attribut  (id_produit,  id_attribut, value )
					 				VALUES (:id_produit, :id_attribut, :value) 
									ON DUPLICATE KEY UPDATE
					 				value = VALUES(value)";
					 				
		$this->stmtProductAttribute = $this->prepare($sql);
	}
	
	
	public function addRowSfkProdAttr($idProd, $idAttr, $value)
	{
		//INSERT de la relation entre CE PRODUIT et CET ATTRIBUT dans soukflux 
		$this->stmtProductAttribute->bindValue(':id_produit', 	$idProd);
		$this->stmtProductAttribute->bindValue(':id_attribut', 	$idAttr);
		$this->stmtProductAttribute->bindValue(':value', 		$value);
	 	$this->stmtProductAttribute->execute();
	}
	
	public function getIdAttr($codeAttr) {
		$sql = "SELECT id_attribut from sfk_catalog_product where code_attr = '$codeAttr' ";
		return $this->getone($sql);
	}
	
		/**
	 * Va chercher si l'attribut existe déja dans la table s'il existe retourne l'ID_ATTRIBUT de cet
	 * attribut sinon la fonction Crée ce nouvel Attribut et renvoie le dernier ID_ATTRIBUT auto incrémenté
	 * correspondant
	 */
	public function checkIfAttributeExistsPS($attribute, $codCat)
	{
		$codeAttr = trim($attribute);
		$codeAttr = str_replace(' ', '_', $codeAttr);
		$codeAttr = strtoupper($this->stripAccents($codeAttr));
		
		$sql = "SELECT id_attribut FROM sfk_attribut WHERE code_attr = :CODE_ATTR";
		
		$stmt = $this->prepare($sql);
		$stmt->bindValue(":CODE_ATTR", $codeAttr);
		$stmt->execute();
		
		if ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			return $result['id_attribut'];
		}
		else{
			return false;
		}
	}
	
	public function getCatId($labelCat)
	{
		$sql = "SELECT cat_id FROM sfk_categorie WHERE cat_label = '$labelCat'";
		return $this->getone($sql);;
	}
	
	public function addRowSfkProductCategorie($id_product, $cat_id) {
		$null = null;
		$this -> stmtProductCat -> bindParam(1, $cat_id ? $cat_id : $null);
		$this -> stmtProductCat -> bindParam(2, $id_product ? $id_product : $null);
		$this -> stmtProductCat -> execute();
	}

	public function prepareInsertSfkProductCategorie() {
		$sql = "INSERT INTO sfk_cat_product (cat_id, id_produit) VALUES ( ?, ?) ";
		$this -> stmtProductCat = $this -> prepare($sql);

	}
	
		
	public function getAllProdNoBook()
	{
		$sql = "SELECT id_produit, categories, price FROM sfk_catalog_product WHERE categories <> 1";
		return $result = $this->getAll($sql);
	}
	
	public function updatePrix($prix, $idprod)
	{
		$price = explode(",", $prix);
		$price = $price[0].",". substr($price[1], 0,2);
		
		echo $price."\n";
		$sql = "UPDATE sfk_catalog_product SET price = '$price' WHERE id_produit = $idprod";
		
		$this->getAll($sql);
	}
	
	
	function stripAccents($texte) {
		$texte = str_replace(
			array(
				'à', 'â', 'ä', 'á', 'ã', 'å',
				'î', 'ï', 'ì', 'í', 
				'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
				'ù', 'û', 'ü', 'ú', 
				'é', 'è', 'ê', 'ë', 
				'ç', 'ÿ', 'ñ', 'œ',
				'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
				'Î', 'Ï', 'Ì', 'Í', 
				'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
				'Ù', 'Û', 'Ü', 'Ú', 
				'É', 'È', 'Ê', 'Ë', 
				'Ç', 'Ÿ', 'Ñ', 'Œ'
			),
			array(
				'a', 'a', 'a', 'a', 'a', 'a', 
				'i', 'i', 'i', 'i', 
				'o', 'o', 'o', 'o', 'o', 'o', 
				'u', 'u', 'u', 'u', 
				'e', 'e', 'e', 'e', 
				'c', 'y', 'n', 'oe',
				'A', 'A', 'A', 'A', 'A', 'A', 
				'I', 'I', 'I', 'I', 
				'O', 'O', 'O', 'O', 'O', 'O', 
				'U', 'U', 'U', 'U', 
				'E', 'E', 'E', 'E', 
				'C', 'Y', 'N', 'OE' 
			),$texte);
		return $texte;
	}
}

?>