<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA  
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */


class BatchPitcuresPrestashop {
	
	//Variables pour la clase batch
	private $_name = "BatchPitcuresPrestashop";
	static protected $_description = "Modifie les urls pour ecom Prestashop avec mauvais url images";
	
	private static $db;

	// A COMPLEXIFIER
	protected $cat_id; //L'ID de la catégorie dans la BDD correspondante aux produits qu'on va crawler

	protected $Aattr = array(); 			//Tableau de fiches produit

	protected $curProd;						//Produit actuellement lu
	protected $curCat;						//Catégorie dans laquelle on se trouve actuellement
	protected $curSCat;  					//Sous Catégorie dans laquelle on se trouve actuellement
	protected static $countCatChecked=0;	//Compteur du nombre de catégories déja vues
	
	//Entrez des nombres très grand (99999) pour faire tous les produits de toutes les sous catégories etc...
	private $nbCat = 1;
	private $nbSsCat = 4;
	private $nbProdBySsCat = 20;
	

	public function work() {
		$temps_debut = microtime(true);

		$this -> cat_id = '';
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoPrestashopModel();
		
		
		////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////// TONER.RE /////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////
		$listeProd = self::$db -> getAllProductsPSByVendor(3);
		
		foreach ($listeProd as $prod) {
			$newUrl = "http://toner.re/img/p/".$prod['ID_PRODUCT'];
			$tmp = $prod['IMAGE_PRODUCT'];
			$tmp = explode('_', $tmp);
			$tmp = explode('/', $tmp[0]);
			$tmp = end($tmp);
			$newUrl = $newUrl . "-" . $tmp . ".jpg";
			echo $newUrl . "\n";
			
			self::$db -> updatePicProd($prod['ID_PRODUCT'], $prod['VENDOR_ID'], $newUrl);

		}
		
		
		////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////// KDOPAYS /////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////
		
		$listeProd = self::$db -> getAllProductsPSByVendor(77);
		
		foreach ($listeProd as $prod) {
			$newUrl = "http://www.kdopays.re/img/p/".$prod['ID_PRODUCT'];
			$tmp = $prod['IMAGE_PRODUCT'];
			$tmp = explode('_', $tmp);
			$tmp = explode('/', $tmp[0]);
			$tmp = end($tmp);
			$newUrl = $newUrl . "-" . $tmp . ".jpg";
			echo $newUrl . "\n";

			self::$db -> updatePicProd($prod['ID_PRODUCT'], $prod['VENDOR_ID'], $newUrl);
			
		}
		
		
		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

	}
	
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
