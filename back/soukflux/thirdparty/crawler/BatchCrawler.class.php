<?php

class BatchCrawler {
	
	private $_name = "BatchCrawler";
	static protected $_description = "BatchCrawler";

	private static $db;

	protected $cat_id;

	protected $rooturl = "http://www.achetezle.fr";
	protected $Aattr = array(); //Tableau de fiches produit

	protected $curProd;
	protected $curCat;

	//ID DE LA CATEGORIE EN COURS DE TRAITEMENT

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);
		
		$this->cat_id = 23;
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoCrawlerModel();
		//Prepare stmt
		self::$db->prepareInsertSfkCatProduct();
		self::$db->prepareInsertSfkAttribute();
		self::$db->prepareInsertSfkProductAttribute();
		
		$i = 0;

		/*

		 * OUT DB SKFLUX
		 *
		 * Table : sfk_catalog_product
		 * Chps sfkcp		-->		champs CRAWLER
		 id_produit			--> 	AUTO GEN
		 sku	 			--> 	EAN -->
		 categories			-->		FROM SCRATCH
		 name				--> 	NOM
		 description 		--> 	CONCAT NOM ?
		 short_description 	--> 	CONCAT NOM ?
		 price	 			--> 	price
		 weight				--> 	Poids
		 country_of_manufacture --> 	null
		 meta_description	--> 	null
		 meta_keyword	 	--> 	null
		 meta_title			--> 	meta_title
		 image				--> 	image
		 small_image			--> 	null
		 thumbnail			--> 	null
		 attribute_set		--> 	null
		 date_create			--> 	now()
		 date_update			--> 	now()
		 *
		 */

		$nbtocheck = 17;
		$nbSousCatToCheck = 4; 

		$aurl = array("http://www.achetezle.fr/electromenager/");

		foreach ($aurl as $u) {
			$html2 = file_get_html($u);
			$i = 1;
			foreach ($html2->find("ul.subtree" ) as $ul) {
				foreach ($ul->children() as $e) {
					$cat = $e -> find('a', 0) -> innertext;
					echo "\n==================================Begin cat  $cat {$i}==========================\n";
					$mycat = $cat . "/";

					echo "\n==================================Begin Sous - cat ==========================\n\n\n";
					foreach ($e->find("ul li ") as $a) {
						$url = $a -> find('a', 0) -> href;
						$this->curCat = $a -> find('a', 0) -> innertext;

						echo "********************* Sous cat " . ($this->curCat) . "  ////// href:" . ($url) . "\n";

						$url = explode(',', $url);

						//TRAITEMENT DES PAGES PRODUITS :
						for ($i = 1; $i <= $nbtocheck; $i++) {
							$uri = $url[0] . ',' . $url[1] . ',' . $i . ".htm";
							echo $uri . "\n";
							$this -> checksouscat($uri);
							
						}
						echo "\n==================================End Sous - cat==========================\n\n";
						//break;
					}

					echo "\n==================================END cat==========================\n\n";
					$i++;
					if ($i>$nbSousCatToCheck) {
						break;
					}
					
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////// INSERTION EN BDD /////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//Pour chaque produits qu'on a récupéré 
		foreach ($this->Aattr as $produit ) {
					
			//Si pas d'EAN inutile d'insérer le produit
			if( isset($produit['EAN']) && $produit['EAN'] != ""){	
			
				//Insertion du produit avec tous ses champs "basique" 
				self::$db->addRowSfkCatProduct($produit, $this->cat_id);
				
				//Besoin de l'id produit inseré ou mis à jour pour les relation attr/prod
				$idProd = self::$db->getIdProduct($produit['EAN']);
				
				//Récupération des attributs du produit			
				$listeAttributsProduit = $this->getAttributsProduct($produit);
				
				//boucle Attributs
				foreach ($listeAttributsProduit as $attr => $value) {
					
					//Si c'est un attribut déja connu on récupère son ID 
					$idAttr = self::$db->checkIfAttributeExistsPS($attr, $this->cat_id);
					
					//Sinon on insère cet attribut et on récupère le lastInsertId
					if ($idAttr == false ) {
						self::$db->addRowSfkAttr($attr, $this->cat_id); 
						$idAttr = self::$db->lastInsertId();	
					}
					
					//Ajout de la ligne de relation entre ce produit et cet attribut
					self::$db->addRowSfkProdAttr($idProd, $idAttr, $value, $this->cat_id);
				}
			}
		}
		
		//var_dump ($listeAttributsProduit);
		
		$temps_fin = microtime(true);
		echo "Duree du batch : ".number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		

	}

	function checksouscat($u) {

		$url = $this -> rooturl . $u;
		$html = file_get_html($url);
		foreach ($html->find('td.description') as $desc) {
			$minidesc = $desc -> find('span', 0) -> innertext;
			if ($minidesc != '') {
				$this -> curProd = $desc -> find('a', 0) -> innertext;
				echo "\nNOM PRODUIT : " . $desc -> find('a', 0) -> innertext . "\n";
				
				//Nom Prod
				$this -> Aattr[$this -> curProd]['nom'] = $desc -> find('a', 0) -> innertext;
				
				//Desc Prod
				$this -> Aattr[$this -> curProd]['desc'] = $this->curCat . ", " . $this -> Aattr[$this -> curProd]['nom'] ;
				
				$this -> getpartDescrition($this -> rooturl . $desc -> find('a', 0) -> href);
				
				$tmp = explode(',', $desc -> find('a', 0) -> href);
				$idprod = explode('.', end($tmp));

				$this -> getlargedescription($this -> constructurldescproduit($idprod[0]));

				echo "\n";
				sleep(rand(1,3)); //echo Anti spam sleep 5 
			}
		}
		
	}

	function constructurldescproduit($idprod) {
		$uridesclarge = "http://www.achetezle.fr/electromenager/specs,";
		return $uridesclarge . $idprod . ".htm";

	}

	function getpartDescrition($url) {

		$html = file_get_html($url);
		if ($html) {
			foreach ($html->find('.pdtInfos') as $elm)
				foreach ($elm -> find ("li") as $e) {
					$temp = strip_tags($e -> innertext);
					$atemp = explode(':', $temp);
					if (trim($atemp[0]) != 'reference')
						$this -> Aattr[$this -> curProd][trim($atemp[0])] = trim($atemp[1]);

				}
			//var_dump($html->find(".pdtPhoto img", 0)->src );
			$this -> Aattr[$this -> curProd]['image'] = $html -> find(".pdtPhoto img", 0) -> src;
			$this -> Aattr[$this -> curProd]['meta_title'] = $html -> find("title", 0) -> innertext;
			
			//Price product
			$price = $html -> find(".price b", 0) -> innertext ;
			$price = filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);
			$this -> Aattr[$this -> curProd]['price'] = $price ;
		}

	}

	function getlargedescription($url) {
		$html = file_get_html($url);
		//var_dump($html);
		if ($html) {
			foreach ($html->find("td.name" ) as $elm) {
				//echo "Desc Attribut " . $elm -> innertext . " :  ";
				//echo " Val attr : " . $elm -> nextsibling() . "<br/>";
				//var_dump(trim($elm -> innertext));
				$this -> Aattr[$this -> curProd][trim($elm -> innertext)] = trim($elm -> nextsibling() -> innertext);
			}
		}
	}

	protected function ChecKAndUpdateAtt($arr) {
		$sqlT = "select code_attr as ID from sfk_attribut where cat_id= '$this->cat_id' ";
		$attrUsed = $this -> formatarr(self::$db -> getAll($sqlT));
		//Don t want titre in ATTR
		$attrUsed[] = 'titre';

		foreach ($arr as $k => $v) {
			if (!in_array($k, $attrUsed)) {
				if ($k == 'type')
					$k = "format";
				$A['cat_id'] = $this -> cat_id;
				$A['code_attr'] = $k;
				$A['label_attr'] = $k;
				$this -> addAttribut($A);
			}
		}
	}
	
	/**
	 * Fonction permettant de ne garder que les champs attributs d'un produit crawlé
	 */
	protected function getAttributsProduct($produit)
	{
		//On enleve les champs basiques du tableau pour n'avoir que les attributs dynamiques
		unset($produit['nom']);
		unset($produit['Marque']);
		unset($produit['Référence']);
		unset($produit['EAN']);
		unset($produit['image']);
		unset($produit['meta_title']);
		unset($produit['price']);
		if(isset($produit['poids'])) unset($produit['poids']);
		
		return $produit;
	}

	
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
