<?php 

/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * 
 */
class BatchExportCatalogAvahis {

	private $_name = "BatchExportCatalogAvahis";
	static protected $_description = "Batch d'export des fiches valides Soukflux vers Avahis";

	private static $db;
	
	protected $cat_id;
	
	protected $ArrCSV = array();
	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		$temps_debut = microtime(true);
		
		$this->cat_id = 150; // EXEMPLE cartes graphiques
		
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		
		//Obtenir categories sous forme de chaine grace à récursive
		$categories = self::$db->getStrCateg($this->cat_id, "");
		
		$attributeList = self::$db->getAllAttrByCat($this->cat_id);
		echo $categories. "\n";
		
		/*sku;
		 * categories;
		 * name;
		 * description;
		 * short_description;
		 * price;
		 * weight;
		 * country_of_manufacture;
		 * meta_description;
		 * meta_keyword;
		 * meta_title;
		 * image;
		 * small_image;
		 * thumbnail;
		 * visibility;
		 * attribute_set;
		 * volume rand entre 1 et 150
		 * tax_class_id;
		 * udropship_vendor
		 */
		$this->ArrCSV['entete'] = array(	"sku",
											"categories",
											"name",
											"description",
											"short_description",
											"price",
											"weight",
											"country_of_manufacture",
											"meta_description",
											"meta_keyword",
											"meta_title",
											"image",
											"small_image",
											"thumbnail",
											"visibility",
											"attribute_set",
											"volume",
											"tax_class_id",
											"udropship_vendor");
		
		foreach($attributeList as $attr){
			array_push($this->ArrCSV['entete'], $attr['label_attr']);
		}
		
		var_dump($this->ArrCSV);
		
		/*$entête = array (
		   array('aaa', 'bbb', 'ccc', 'dddd'),
		   array('123', '456', '789'),
		   array('"aaa"', '"bbb"')
		);
		
		$fp = fopen('file.csv', 'w');
		
		foreach ($list as $fields) {
		    fputcsv($fp, $fields);
		}
		
		fclose($fp); */
		
	}	


	
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>