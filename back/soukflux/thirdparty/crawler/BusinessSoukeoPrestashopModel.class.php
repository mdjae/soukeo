<?php 
/**
 * Model pour les tables soukflux dédiées à PRESTASHOP
 * 
 * @version 0.1.0
 * @author 	Philippe_LA
 */
class BusinessSoukeoPrestashopModel extends BusinessSoukeoModel {
	
	protected $stmtProduitPS;
	protected $stmtProdAttrPS;
	
	/**
	 * Préparation de l'insertion / update des produits du vendeur dans sfk_produits_ps
	 * Il y a également une remise à 0 du champ Active avant la nouvelle insertion ou update
	 * ainsi, même si l'eCommercant a changé d'avis dans ce qu'il veux exporter les produits
	 * restent en base de donnée et seuls ceux actifs iront vers Avahis
	 */
	public function prepareInsertSfkProdPS($vendorID)
	{
		//Remise à 0 actif de tous les produits
		$sql = "UPDATE sfk_produit_ps SET ACTIVE = 0 WHERE VENDOR_ID = :VENDOR_ID"; //ID DU VENDEUR DYNAMIQUE BESOIN TRACKING
		$stmt = $this->prepare($sql);
		$stmt->bindValue(':VENDOR_ID', $vendorID);
		$stmt->execute();
		
		$sql = "INSERT INTO sfk_produit_ps  (ID_PRODUCT,VENDOR_ID, NAME_PRODUCT,REFERENCE_PRODUCT,
												SUPPLIER_REFERENCE,MANUFACTURER,CATEGORY,DESCRIPTION,
												DESCRIPTION_SHORT,PRICE_PRODUCT,WHOLESALE_PRICE,PRICE_HT,
												PRICE_REDUCTION,POURCENTAGE_REDUCTION,QUANTITY,WEIGHT,EAN,
												ECOTAX,AVAILABLE_PRODUCT,URL_PRODUCT,IMAGE_PRODUCT,
												PRODUCT_FEATURE,FDP,ID_MERE,DELAIS_LIVRAISON,IMAGE_PRODUCT_2,
												IMAGE_PRODUCT_3,REDUCTION_FROM,REDUCTION_TO,META_DESCRIPTION,
												META_KEYWORDS, ACTIVE, DATE_CREATE)
					 				VALUES (:ID_PRODUCT, :VENDOR_ID, :NAME_PRODUCT, :REFERENCE_PRODUCT,
												:SUPPLIER_REFERENCE, :MANUFACTURER, :CATEGORY, :DESCRIPTION,
												:DESCRIPTION_SHORT, :PRICE_PRODUCT, :WHOLESALE_PRICE, :PRICE_HT,
												:PRICE_REDUCTION, :POURCENTAGE_REDUCTION, :QUANTITY, :WEIGHT, :EAN,
												:ECOTAX, :AVAILABLE_PRODUCT, :URL_PRODUCT, :IMAGE_PRODUCT,
												:PRODUCT_FEATURE, :FDP, :ID_MERE, :DELAIS_LIVRAISON, :IMAGE_PRODUCT_2,
												:IMAGE_PRODUCT_3, :REDUCTION_FROM, :REDUCTION_TO, :META_DESCRIPTION,
												:META_KEYWORDS, 1, NOW())
									ON DUPLICATE KEY UPDATE 
												
												NAME_PRODUCT			=VALUES(NAME_PRODUCT),
												REFERENCE_PRODUCT 		=VALUES(REFERENCE_PRODUCT),
												SUPPLIER_REFERENCE		=VALUES(SUPPLIER_REFERENCE),
												MANUFACTURER			=VALUES(MANUFACTURER),
												CATEGORY				=VALUES(CATEGORY),
												DESCRIPTION 			=VALUES(DESCRIPTION),
												DESCRIPTION_SHORT		=VALUES(DESCRIPTION_SHORT),
												PRICE_PRODUCT			=VALUES(PRICE_PRODUCT),
												WHOLESALE_PRICE			=VALUES(WHOLESALE_PRICE),
												PRICE_HT				=VALUES(PRICE_HT),
												PRICE_REDUCTION			=VALUES(PRICE_REDUCTION),					
												POURCENTAGE_REDUCTION 	=VALUES(POURCENTAGE_REDUCTION),
												QUANTITY				=VALUES(QUANTITY),
												WEIGHT					=VALUES(WEIGHT),
												EAN 					=VALUES(EAN),
												ECOTAX					=VALUES(ECOTAX),
												AVAILABLE_PRODUCT		=VALUES(AVAILABLE_PRODUCT),
												URL_PRODUCT				=VALUES(URL_PRODUCT),
												IMAGE_PRODUCT			=VALUES(IMAGE_PRODUCT),
												PRODUCT_FEATURE			=VALUES(PRODUCT_FEATURE),
												FDP						=VALUES(FDP),
												ID_MERE					=VALUES(ID_MERE),
												DELAIS_LIVRAISON		=VALUES(DELAIS_LIVRAISON),
												IMAGE_PRODUCT_2 		=VALUES(IMAGE_PRODUCT_2),
												IMAGE_PRODUCT_3			=VALUES(IMAGE_PRODUCT_3),
												REDUCTION_FROM			=VALUES(REDUCTION_FROM),
												REDUCTION_TO			=VALUES(REDUCTION_TO),
												META_DESCRIPTION		=VALUES(META_DESCRIPTION),
												META_KEYWORDS			=VALUES(META_KEYWORDS),
												ACTIVE 					=VALUES(ACTIVE)";
												
		$this->stmtProduitPS = $this->prepare($sql);
	}
	
	/**
	 * Fonction permettant l'insertion ou l'update de produits dans la base de donnée soukflux 
	 * Si le couple VENDOR_ID/ID_PRODUCT existe déjà il y a juste update du produit.
	 * Changements à effectuer dans les champs récupérés, besoin d'affiner
	 */
	public function addRowSfkProdPS($product, $vendorID)
	{
		$this->stmtProduitPS->bindValue(':ID_PRODUCT', 	   			$product['ID_PRODUCT'], PDO::PARAM_STR);
		$this->stmtProduitPS->bindValue(':VENDOR_ID', 	   			$vendorID);
		$this->stmtProduitPS->bindValue(':NAME_PRODUCT', 			$product['NAME_PRODUCT']);
		$this->stmtProduitPS->bindValue(':REFERENCE_PRODUCT', 		$product['REFERENCE_PRODUCT']);
		$this->stmtProduitPS->bindValue(':SUPPLIER_REFERENCE', 		$product['SUPPLIER_REFERENCE']);
		$this->stmtProduitPS->bindValue(':MANUFACTURER', 			$product['MANUFACTURER']);
		$this->stmtProduitPS->bindValue(':CATEGORY', 				$product['CATEGORY']);
		$this->stmtProduitPS->bindValue(':DESCRIPTION', 			$product['DESCRIPTION']);
		$this->stmtProduitPS->bindValue(':DESCRIPTION_SHORT',		$product['DESCRIPTION_SHORT']);
		$this->stmtProduitPS->bindValue(':PRICE_PRODUCT', 			$product['PRICE_PRODUCT']);
		$this->stmtProduitPS->bindValue(':WHOLESALE_PRICE',			$product['WHOLESALE_PRICE']);
		$this->stmtProduitPS->bindValue(':PRICE_HT', 				$product['PRICE_HT']);
		$this->stmtProduitPS->bindValue(':PRICE_REDUCTION', 		$product['PRICE_REDUCTION']);
		$this->stmtProduitPS->bindValue(':POURCENTAGE_REDUCTION', 	$product['POURCENTAGE_REDUCTION']);
		$this->stmtProduitPS->bindValue(':QUANTITY',				$product['QUANTITY']);
		$this->stmtProduitPS->bindValue(':WEIGHT', 					$product['WEIGHT']);
		$this->stmtProduitPS->bindValue(':EAN', 					$product['EAN']);
		$this->stmtProduitPS->bindValue(':ECOTAX', 					$product['ECOTAX']);
		$this->stmtProduitPS->bindValue(':AVAILABLE_PRODUCT', 		$product['AVAILABLE_PRODUCT']);
		$this->stmtProduitPS->bindValue(':URL_PRODUCT', 			$product['URL_PRODUCT']);
		$this->stmtProduitPS->bindValue(':IMAGE_PRODUCT', 			$product['IMAGE_PRODUCT']);
		$this->stmtProduitPS->bindValue(':PRODUCT_FEATURE', 		$product['PRODUCT_FEATURE']);
		$this->stmtProduitPS->bindValue(':FDP', 					$product['FDP']);
		$this->stmtProduitPS->bindValue(':ID_MERE', 				$product['ID_MERE']);
		$this->stmtProduitPS->bindValue(':DELAIS_LIVRAISON', 		$product['DELAIS_LIVRAISON']);
		$this->stmtProduitPS->bindValue(':IMAGE_PRODUCT_2', 		$product['IMAGE_PRODUCT_2']);
		$this->stmtProduitPS->bindValue(':IMAGE_PRODUCT_3', 		$product['IMAGE_PRODUCT_3']);
		$this->stmtProduitPS->bindValue(':REDUCTION_FROM', 			$product['REDUCTION_FROM']);
		$this->stmtProduitPS->bindValue(':REDUCTION_TO', 			$product['REDUCTION_TO']);
		$this->stmtProduitPS->bindValue(':META_DESCRIPTION', 		$product['META_DESCRIPTION']);
		$this->stmtProduitPS->bindValue(':META_KEYWORDS', 			$product['META_KEYWORDS']);
		
		return $this->stmtProduitPS->execute();
	}
	
	
	/**
	 * Préparation des insertions des relations produits_attributs dans les tables dédiées à 
	 * Prestashop de Soukflux
	 */
	public function prepareInsertSfkProdAttrPS()
	{									
		$sql = "INSERT INTO sfk_produit_attribut_ps (ID_PRODUCT, VENDOR_ID, ID_ATTR, VALUE)
													VALUES (:ID_PRODUCT, :VENDOR_ID, :ID_ATTR, :VALUE)
													ON DUPLICATE KEY UPDATE
													ID_ATTR 	=VALUES(ID_ATTR),
													VALUE 		=VALUES(VALUE)";
														
		$this->stmtProdAttrPS = $this->prepare($sql);											
	}
	
	/**
	 * Insertion des relations produits attributs dans les tables dédiées à 
	 * Prestashop de Soukflux 
	 */
	public function addRowSfkProdAttrPS($idProd, $vendorID, $idAttr, $value)
	{
		$this->stmtProdAttrPS->bindValue(':ID_PRODUCT',	$idProd);
		$this->stmtProdAttrPS->bindValue(':VENDOR_ID', 	$vendorID);
		$this->stmtProdAttrPS->bindValue(':ID_ATTR', 		$idAttr);
		$this->stmtProdAttrPS->bindValue(':VALUE', 		$value);
		return $this->stmtProdAttrPS->execute();
	}
	
	/**
	 * Va chercher si l'attribut existe déja dans la table s'il existe retourne l'ID_ATTRIBUT de cet
	 * attribut sinon la fonction Crée ce nouvel Attribut et renvoie le dernier ID_ATTRIBUT auto incrémenté
	 * correspondant
	 */
	public function checkIfAttributeExistsPS($attribute)
	{
		$sql = "SELECT ID_ATTR FROM sfk_attribut_ps WHERE CODE_ATTR = :CODE_ATTR";
		
		$stmt = $this->prepare($sql);
		$stmt->bindValue(":CODE_ATTR", $attribute);
		$stmt->execute();
		
		if ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			return $result['ID_ATTR'];
		}
		else{
			$stmt = $this->prepare("INSERT INTO sfk_attribut_ps (LABEL_ATTR, CODE_ATTR) 
												VALUES (:LABEL_ATTR, :CODE_ATTR)");
			$stmt->bindValue(":LABEL_ATTR", str_replace("_", " ", ucfirst(strtolower($attribute))));
			$stmt->bindValue(":CODE_ATTR", $attribute);
			$stmt->execute();
			
			return $this->lastInsertId();
		}
	}
	
	
	/**
	 * Permet de renvoyer un attribut prestashop particulier à partir de son ID_ATTR 
	 * @param idAttr int l'id de l'attribut recherché
	 */
	public function getAttributePS($idAttr)
	{
		$sql = "SELECT ID_ATTR, LABEL_ATTR, CODE_ATTR FROM sfk_attribut_ps WHERE ID_ATTR = '$idAttr' ";

		return $this->getOne($sql);
	}
	
	
	/**
	 * Prépare requete de selection de tous les attributs prestashop
	 */
	public function getAllAttributesPS()
	{
		$sql = 'SELECT * FROM sfk_attribut_ps';
		return $this->getAll($sql);
	}
	
	
	/**
	 * Renvoie toutes les relations produit-attribut pour un idProduit donné
	 * @param $idProductPs 	String 	idProduit Pretashop dont on veux les attributs
	 */
	public function getAttributeProductPS($idProductPs)
	{
		$sql = "SELECT * FROM sfk_produit_attribut_ps WHERE ID_PRODUCT = '".$idProductPs."'";
		
		return $this->getAll($sql);
	}
	
	
	
	
	
	/**
	 * Renvoie tous les produits Prestashop
	 */
	public function getAllProductsPS()
	{
		$sql = "SELECT * FROM sfk_produit_ps";
		
		return $this->getAll($sql);
	}
	
	/**
	 * Renvoie tous les produits Prestashop pour un vendeur donné
	 */
	public function getAllProductsPSByVendor($vendorID)
	{
		$sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID = :VENDOR_ID AND ACTIVE = 1";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> execute() ;
		$res =  $stmt-> fetchAll();
		return $res;
	}
	
	/**
	 * Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif dans SON prestashop 
	 */
	public function purgeInactiveProduct()
	{
		//Si le produit a été passé en inactif chez le commercant, on l'a recu en inactif coté soukflux
		//si la mise a jour a bien eu lieu alors on va simplement supprimer les produits inactifs de cet ecommercant
		$sql ="	DELETE  spe  
				FROM sfk_product_ecommercant_stock_prix spe
				INNER JOIN  sfk_produit_ps sp 
				ON spe.VENDOR_ID = sp.VENDOR_ID AND spe.ref_ecommercant = sp.REFERENCE_PRODUCT
				WHERE sp.ACTIVE = 0;";	
		$this->query($sql);
	}
	
	public function getAllDataProductByVendor($idVendor, $idProduct)
	{
		$data = array();
		
		$sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID = :VENDOR_ID AND ID_PRODUCT = :ID_PRODUCT";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' ,  $idVendor);
		$stmt -> bindValue(':ID_PRODUCT' , $idProduct);
		$stmt -> execute() ;
		$prod =  $stmt-> fetch();
		
		$sql = "SELECT DISTINCT a.ID_ATTR, a.LABEL_ATTR, pa.VALUE FROM sfk_produit_attribut_ps as pa 
				INNER JOIN sfk_produit_ps as p
					ON p.ID_PRODUCT = pa.ID_PRODUCT
				INNER JOIN sfk_attribut_ps as a 
					ON a.ID_ATTR = pa.ID_ATTR
				WHERE p.ID_PRODUCT = :ID_PRODUCT 
				AND   p.VENDOR_ID = :VENDOR_ID";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' ,  $idVendor);
		$stmt -> bindValue(':ID_PRODUCT' , $idProduct);
		$stmt -> execute() ;
		$attr =  $stmt-> fetchAll();
		
		$res['prod'] = $prod;
		$res['attr'] = $attr;
		return $res;
	}
	
	public function getDistinctCat($idVendor)
	{
		$sql = "SELECT DISTINCT CATEGORY FROM sfk_produit_ps WHERE VENDOR_ID= :VENDOR_ID AND ACTIVE = 1 ORDER BY CATEGORY ";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		
		return $this->getAllPrepa($stmt);
		
		//$stmt -> execute();
		
		//return $stmt -> fetchAll();
	}
	
	/**
	 * Récupère tous les produits d'une catégorie pour un vendeur donné
	 */
	public function getAllProdByCategVendor($cat, $idVendor)
	{	
		$sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID = :VENDOR_ID AND CATEGORY LIKE :CATEGORY AND ACTIVE = 1";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);	
	}
	
	/**
	 * récupère les produits déjà associées pour un vendeur et sa catégorie
	 */
	public function getCountAssocProdByCatVendor($catCli, $idVendor)
	{
		$sql = "SELECT COUNT(*) as count FROM sfk_produit_ps as ps
				INNER JOIN sfk_assoc_product as ap 
				ON ps.VENDOR_ID = ap.VENDOR_ID AND ps.ID_PRODUCT = ap.id_produit_ecommercant
				WHERE ps.VENDOR_ID = :VENDOR_ID AND ps.CATEGORY LIKE :CATEGORY AND ACTIVE = 1";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $catCli);
		$stmt -> execute();
		return $stmt -> fetch();
			
	}
	
		/**
	 * Renvoie un produit Prestashop pour un vendeur donné
	 */
	public function getProduct($vendorID, $idProduct)
	{
		$sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID = :VENDOR_ID AND ID_PRODUCT = :ID_PRODUCT";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> bindValue(':ID_PRODUCT' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		return $this -> getAllPrepa($stmt);
	}
	
	public function getAllUnassocProducts($cat, $idVendor, $brand=null, $priceMin=null, $priceMax=null, $search="", $context="", $order="")
	{
		$sql = "SELECT * FROM sfk_produit_ps as p
				WHERE p.VENDOR_ID = :VENDOR_ID AND ACTIVE = 1 
				";
				
		if($cat != ""){
			$sql.=" AND p.CATEGORY LIKE :CATEGORY ";
		}		
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= "AND ID_PRODUCT NOT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) 
					AND p.refus = 0";
		}
				
		if($context == "refused"){
			$sql.= " AND p.refus = 1 ";
		} 
		
		if($context == "assoc"){
			$sql.= "AND ID_PRODUCT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) ";
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, REFERENCE_PRODUCT, EAN) AGAINST (:NAME_PRODUCT) ";	
		}
		
		if($order == ""){
			$sql .= " ORDER BY NAME_PRODUCT ";	
		}elseif($order == "asc"){
			$sql .= " ORDER BY CAST(PRICE_PRODUCT AS DECIMAL(10,5)) ASC ";
		}elseif($order == "desc"){
			$sql .= " ORDER BY CAST(PRICE_PRODUCT AS DECIMAL(10,5)) DESC ";
		}
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		if($cat != ""){
			$stmt -> bindValue(":CATEGORY", $cat."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		
		return $this->getAllPrepa($stmt);
	}
	
	
	public function getAllAssocProds($idVendor, $categ = "", $brand=null, $priceMin=null, $priceMax=null, $search=null)
	{
		$sql = "SELECT * FROM sfk_produit_ps as p
				WHERE p.VENDOR_ID = :VENDOR_ID
				AND ACTIVE = 1 AND 
				ID_PRODUCT IN (SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID )
				";
				
		if($categ!= ""){
			$sql .= " AND CATEGORY LIKE :CATEGORY ";
		} 	
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, REFERENCE_PRODUCT, EAN) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$stmt = $this -> prepare($sql); 
		
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		
		if($categ!= ""){
			$stmt -> bindValue(":CATEGORY", $categ."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		return $this->getAllPrepa($stmt);
	}
	
	
	public function getAllRefusedProds($idVendor, $categ = "", $brand=null, $priceMin=null, $priceMax=null, $search=null)
	{
		$sql = "SELECT * FROM sfk_produit_ps as p
				WHERE p.VENDOR_ID = :VENDOR_ID
				AND refus = 1 AND ACTIVE = 1 
				";
				
		if($categ!= ""){
			$sql .= " AND CATEGORY LIKE :CATEGORY ";
		} 	
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, REFERENCE_PRODUCT, EAN) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$stmt = $this -> prepare($sql); 
		
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		
		if($categ!= ""){
			$stmt -> bindValue(":CATEGORY", $categ."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		return $this->getAllPrepa($stmt);
	}
	
	
	/**
	 * Remonte tous les attributs pour une catégorie donnée de produits appartenant
	 * à un vendeur
	 */
	public function getAllAttrFromVendorCateg($cat, $idVendor)
	{
		$sql =" SELECT DISTINCT a.ID_ATTR, a.LABEL_ATTR FROM sfk_attribut_ps as a
			    INNER JOIN sfk_produit_attribut_ps as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_ps as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY AND p.ACTIVE = 1";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	
	/**
	 * Ne remonte que les 3 attributs les plus utilisés pour une catégorie et un vendeur donné
	 */
	public function getBestAttrFromVendorCateg($cat, $idVendor)
	{
		$sql =" SELECT COUNT(*) as count, a.ID_ATTR, a.LABEL_ATTR FROM sfk_attribut_ps as a
			    INNER JOIN sfk_produit_attribut_ps as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_ps as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY AND p.ACTIVE = 1 
				GROUP BY ID_ATTR
				ORDER BY count DESC
				LIMIT 3";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	public function getAllDisctValueForAttrFromVendorCateg($cat, $idVendor, $idAttr)
	{
		$sql =" SELECT DISTINCT TRIM(pa.VALUE) as VALUE FROM sfk_attribut_ps as a
			    INNER JOIN sfk_produit_attribut_ps as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_ps as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY AND p.ACTIVE = 1 
				AND a.ID_ATTR = :ID_ATTR 
				ORDER BY VALUE ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	public function getAllDistinctManufFromCatVendor($cat, $idVendor)
	{

		$sql =" SELECT DISTINCT p.MANUFACTURER FROM sfk_produit_ps as p
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY AND p.ACTIVE = 1 
				AND MANUFACTURER <> ''
				ORDER BY MANUFACTURER ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);

		return $this->getAllPrepa($stmt);
	}
	
	public function verifAttrForProduct($idVendor, $idProduct, $idAttr, $valAttr)
	{
		$sql = "SELECT * FROM sfk_produit_attribut_ps 
				WHERE 	ID_PRODUCT = :ID_PRODUCT 
				AND 	VENDOR_ID  = :VENDOR_ID 
				AND 	ID_ATTR    = :ID_ATTR
				AND 	VALUE      = :VALUE ";
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		$stmt -> bindValue(":VALUE", $valAttr);
		
		$stmt -> execute();
				
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;		
	}
	
	public function getAllAttrFromProduct($idProduct, $vendorId)
	{
		$sql = "SELECT pa.VALUE, a.LABEL_ATTR, a.ID_ATTR FROM sfk_produit_attribut_ps as pa 
				INNER JOIN sfk_attribut_ps as a ON a.ID_ATTR = pa.ID_ATTR 
				WHERE pa.ID_PRODUCT = :ID_PRODUCT 
				AND pa.VENDOR_ID = :VENDOR_ID"; 
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);  
		
		return $this->getAllPrepa($stmt);
	}
	
	public function countAllProductInCat($cat, $vendorId, $context="all")
	{
		$sql = "SELECT count(*) as count FROM sfk_produit_ps WHERE CATEGORY LIKE CONCAT(:categories, '%') 
				AND VENDOR_ID = :VENDOR_ID AND ACTIVE = 1 ";
		
		if($context == "refused"){
			$sql.= " AND refus = 1 ";
		}
		
		if($context == "assoc"){
			$sql.= " AND ID_PRODUCT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) ";
		}
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= " AND ID_PRODUCT NOT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) 
					AND refus = 0";
		}
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":categories", $cat);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);
		$stmt -> execute();
		
		$res = $stmt -> fetch();
		$count = $count + $res['count'];
		
		return $count;
	}
	
	public function setRefusProduct($vendorId, $idProduct, $refus)
	{
		$sql = "UPDATE sfk_produit_ps SET refus = :refus
				WHERE ID_PRODUCT = :ID_PRODUCT 
				AND VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);
		$stmt -> bindValue(":refus", $refus);
		
		$stmt -> execute();
	}
	
/*	public function authorizeProduct($vendorId, $idProduct)
	{
		$sql = "UPDATE sfk_produit_ps SET refus = 0
				WHERE ID_PRODUCT = :ID_PRODUCT 
				AND VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);
		
		//$stmt -> bindValue(":refus", 0);
		$stmt -> execute();
	}*/
	
	/*----------------------------Synchronisation SP--------------------------------------------*/

	/**
	 * Fonction permettant l'insertion ou la mise a jour de la table stock/prix
	 */
	public function addRowSfkProdEcomSP($product, $vendorID) {
	
		//var_dump($product);
		$idProduit = $product['ID_PRODUCT']."_".$product['ID_DECLINAISON'];
		// ETAT spécial ou pas ?
		if ($product['SPECIAL_PRODUCT']==1){
			$ETAT = "SPECIAL";
		}
		else {
			$ETAT = "";
		}
		//var_dump($idProduit);
		//var_dump($vendorID);
		$this -> stmtProdEcommercant -> bindValue(':id_produit',			 	$idProduit);
		$this -> stmtProdEcommercant -> bindValue(':VENDOR_ID', 				$vendorID);
		$this -> stmtProdEcommercant -> bindValue(':ref_ecommercant', 			$product['REFERENCE_PRODUCT']);
		$this -> stmtProdEcommercant -> bindValue(':PRICE_PRODUCT', 			$this -> verifNumNC($product['PRICE_PRODUCT']));
		$this -> stmtProdEcommercant -> bindValue(':PRICE_TTC', 				$this -> verifNumNC($product['PRICE_TTC']));
		$this -> stmtProdEcommercant -> bindValue(':QUANTITY', 					$product['QUANTITY']);
		$this -> stmtProdEcommercant -> bindValue(':PRICE_HT', 					'');
		$this -> stmtProdEcommercant -> bindValue(':REDUCTION_FROM', 			$this -> verifNumNC($product['REDUCTION_FROM']));
		$this -> stmtProdEcommercant -> bindValue(':REDUCTION_TO', 				$this -> verifNumNC($product['REDUCTION_TO']));
		//$this -> stmtProdEcommercant -> bindValue(':PRICE_QUANTITY_START',$product['PRICE_QUANTITY_START']);
		//$this -> stmtProdEcommercant -> bindValue(':PRICE_QUANTITY_END',$product['PRICE_QUANTITY_END']);
		$this -> stmtProdEcommercant -> bindValue(':PRICE_REDUCTION', 			$this -> verifNumNC($product['PRICE_REDUCTION']));
		$this -> stmtProdEcommercant -> bindValue(':POURCENTAGE_REDUCTION', 	'');
		$this -> stmtProdEcommercant -> bindValue(':ETAT_PROMO', 				$product['ETAT_PROMO']);
		$this -> stmtProdEcommercant -> bindValue(':ETAT', 						$ETAT );

		return $this -> stmtProdEcommercant -> execute();
	}
	
	/**
	 * Vérifie si ce produit est bien associé dans la table assoc_product
	 * @param string l'id produit
	 * @param string l'id vendeur
	 * @return boolean oui si elle est bien associé
	 */
	public function verifAssocProduct(){
	
		$sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID = :VENDOR_ID AND ID_PRODUCT = :ID_PRODUCT";
			if($infoTracking = $this -> getRow($sql)) {
			return true ;
		}
		else {
			return false;
		}
		
	}
	
	
	public function updatePicProd($prodId, $vendorId, $newurl)
	{
		$sql = "UPDATE sfk_produit_ps SET IMAGE_PRODUCT = :IMAGE_PRODUCT WHERE ID_PRODUCT = :ID_PRODUCT AND VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID', $vendorId);
		$stmt -> bindValue(':ID_PRODUCT', $prodId);
		$stmt -> bindValue(':IMAGE_PRODUCT', $newurl);
		$stmt -> execute();
	}
	/*---------------------------------------------------------------------------------------------------------------------*/	
	/*------------------------------------En attente d'exportation ou de mise a jour------------------------------------------------*/
    /**
	* Préparation de la requete qui va insérer les données dans la table sfk_product_ecommercant_stock_prix
	* cette Table correspond Ã  l'association entre vendeur, un produit Avahis type , la réference de ce produit
	* dans le Thelia du vendeur ainsi que les différents prix (promotions) et le stock
	* C'est donc l'insertion représentant les donnÃ©es juste avant leur transfert vers Avahis marketplace
	*/
	public function prepareStmtProdEcommercant($vendorID) {
			
		$sql = "UPDATE sfk_product_ecommercant_stock_prix SET QUANTITY = 0 WHERE VENDOR_ID = :VENDOR_ID";
		//ID DU VENDEUR DYNAMIQUE BESOIN TRACKING
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':VENDOR_ID', $vendorID);
		$stmt -> execute();	
		$sql = "INSERT INTO `sfk_product_ecommercant_stock_prix` 
							(`id_produit`, `VENDOR_ID`, `ref_ecommercant`, `PRICE_PRODUCT`, `PRICE_TTC` , `QUANTITY`, `PRICE_HT`,
							 `REDUCTION_FROM`, `REDUCTION_TO`, `PRICE_REDUCTION`, `POURCENTAGE_REDUCTION`,`ETAT_PROMO`, `ETAT`,`date_create`)
							 
							VALUES (:id_produit, :VENDOR_ID, :ref_ecommercant, :PRICE_PRODUCT, :PRICE_TTC , :QUANTITY, :PRICE_HT,
							 :REDUCTION_FROM, :REDUCTION_TO, :PRICE_REDUCTION, :POURCENTAGE_REDUCTION, :ETAT_PROMO , :ETAT, NOW() )
							 
							ON DUPLICATE KEY UPDATE
								ref_ecommercant 		=VALUES(ref_ecommercant),
								PRICE_PRODUCT 			=VALUES(PRICE_PRODUCT),
								PRICE_TTC				=VALUES(PRICE_TTC),
								QUANTITY 				=VALUES(QUANTITY),
								PRICE_HT 				=VALUES(PRICE_HT),
								REDUCTION_FROM 			=VALUES(REDUCTION_FROM),
								REDUCTION_TO 			=VALUES(REDUCTION_TO),
								PRICE_REDUCTION 		=VALUES(PRICE_REDUCTION),
								POURCENTAGE_REDUCTION 	=VALUES(POURCENTAGE_REDUCTION),
								ETAT_PROMO				=VALUES(ETAT_PROMO),
								ETAT 					=VALUES(ETAT)";

		$this -> stmtProdEcommercant = $this -> db -> prepare($sql);
	}
	/**
	 * Vérifie si la valeur test est null
	 * @param String $valTest 
	 * @return String retourne sa valeur ou nc si elle est null
	 */
	public function verifStringNC($valTest){
			
		if($valTest!=null){
			return $valTest;
		}
		else{
			return "nc";
		}
		
	}
	/**
	 * Vérifie si la valeur test est 0
	 * @param String $valTest 
	 * @return String retourne sa valeur ou nc si elle est null
	 */
	public function verifNumNC($valTest){
			
		if($valTest>0){
			return $valTest;
		}
		else{
			return '';
		}
		
	}
	/*--------------------------------------------------------------------------------------*/
}

?>