<style type="text/Css">
<!--
.test1
{
    border: solid 1px #FF0000;
    background: #FFFFFF;
    border-collapse: collapse;
}
-->
</style>
<page style="font-size: 11px ">
	

	<table border="0" cellspacing="5" cellpadding="5">
		<tr>
			<td>Soukéo SAS -Market place Avahis.com Facture N° <?php echo $facture -> getFacture_code() ?></td>
			<td>Page 1/1</td>
		</tr>
		
	</table>	
	<hr />
	<table >
		<tr>
			<td width=400>
			<div >
				<img src="skins/soukeo/logo.jpg" alt="Logo" width=150 /></div></td>
			<td>Facture N° <?php echo $facture -> getFacture_code() ?><br/>
				EN date du <?php echo smartdate($facture -> getDate_created()) ?><br/>
				Commandes du
			</td>
		</tr>
		<tr>
			<td>
				<?php echo $emeteur -> getStreet() ?> <br />
				<?php echo $emeteur -> getZip_code() ?> - <?php echo $emeteur -> getCity() ?> <br />
				Réunion <br />
				SIRET : 791331267
			</td>
			<td style="text-align: center; border: solid 2px black">
				<?php echo $vendor->getVendor_nom() ?> <br />
				SIRET : <?php echo $vendor->getSiret() ?><br />
				<?php echo $recipient -> getStreet() ?> <br />
				<?php echo $recipient -> getZip_code() ?> <?php echo $recipient -> getCity() ?>  
			</td>
		</tr>
		<tr>	
			<td>
				Email : vendeur@avahis.zendesk.com<br />
				Tel : 0693-911-859<br />
			</td>
			<td></td>
		</tr>
	</table>
	<br />
	<div style="text-align: center;">Récapitulatif des ventes :</div>
	<table border="1">
		<tr>
			<td >Numéro de la <br />commande</td>
			<td >Désignation</td>
			<td >Quantité</td>
			<td >Valeur totale <br />des produits <br />TTC</td>
			<td >Commissions <br />dues HT</td>
			<td >Frais transaction <br />bancaire HT <br />(0.50%)</td>
		</tr>
	<?php foreach ($lines as $line) { 
	
		?>
		<tr>
			<td><?php echo $line -> getOrder_increment_id() ?></td>
			<td><?php echo $line -> getDesignation() ?></td>
			<td><?php echo $line -> getQty() ?></td>
			<td style="text-align: right;"><?php echo numberByLang($line -> getPrice_ttc()) ?> €</td>
			<td style="text-align: right;"><?php echo numberByLang($line -> getMt_comm()) ?> €</td>
			<td style="text-align: right;"><?php echo numberByLang($line -> getMt_frais_banq()) ?> €</td>
		</tr>
<?php 	  } ?>
		<tr>
			<td></td>
			<td></td>
			<td><strong>Total</strong></td>
			<td style="text-align: right;"><strong><?php echo numberByLang($factureCom -> getTotal_prix()) ?> €</strong></td>
			<td style="text-align: right;"><strong><?php echo numberByLang($factureCom -> getTotal_comm()) ?> €</strong></td>
			<td style="text-align: right;"><strong><?php echo numberByLang($factureCom -> getTotal_frais_banq()) ?> €</strong></td>
			
		</tr>
	</table>
	
	<table border="0" cellspacing="5" cellpadding="5">
		<tr>
			<td width=300><h4>Somme à transférer par virement</h4></td>
			<td width=150> </td>
			<td></td>	
		</tr>
		<tr>
			<td style="text-align: center; border: solid 2px black"><strong><h5><?php echo numberByLang($factureCom -> getTotal_prix() -  $factureCom -> getGrand_total()) ?> €</h5></strong></td>
			<td></td>
			<td><strong>
				<table border="0" cellspacing="5" cellpadding="5">
					<tr >
						<td style="border-top: solid 3px; border-bottom: solid 3px;" width=100><h5>Total <br />
							Avahis.com</h5>
						</td > 
						<td style="border-top: solid 3px; border-bottom: solid 3px;"><h5><?php echo numberByLang($factureCom -> getGrand_total() * 1.085) ?> €</h5></td>
					</tr>
				</table>		
				</strong>
			</td>
		</tr>
		<tr>
			<td style="text-align: center; border: solid 2px black; background:black; color:red">
				En cas de litige déclaré et non résolu, <br />
				le paiment du produit est suspendu jusqu'à <br />
				sa résolution.
			</td>
			<td></td>
			<td>
				<table border="0" cellspacing="5" cellpadding="5">
					<tr>
						<td width=100>TVA 8.5 %
						</td>
						<td style="text-align: right;"><?php echo numberByLang($factureCom -> getGrand_total() * 0.085) ?> €</td>
					</tr>
					<tr>
						<td>Total HT</td>
						<td style="text-align: right;"><?php echo numberByLang($factureCom -> getGrand_total()) ?> €</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			
		</tr>
	</table>
    
    <p>Moyen du règlement : Déduction du compte de réserve</p>
    
    <p>Délais de règlement : immédiat <br />
       Date limite du règlement : date d'édition du récapitulatif de commande <br />
       En cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement <br />
       de 40€ selon l'article D. 441-5 du code du commerce
    </p>
</page>