﻿<?php  echo drawRMenuHeader(("Options de filtrage"),"FILTER","80") ?>
<form name="form_main_selection" action="/app.php/o_admin/users/" method="POST">
    <p>
		<?php  echo ("Fermé ?") ?> :&nbsp;
			<select class="selectTimesheetLine" name="select_userlocking" onchange="javascript:document.forms.form_main_selection.submit();">
				<option value="all" <?php  echo ($_POST['select_userlocking']=='all'?" selected ":"") ?>><?php  echo ("Tous...") ?></option>
				<option value="1" <?php  echo ($_POST['select_userlocking']=='1'?" selected ":"") ?>><?php  echo ("Fermé") ?></option>
				<option value="0" <?php  echo ($_POST['select_userlocking']=='0'?" selected ":"") ?>><?php  echo ("Actif") ?></option>
			</select>
		</p>
		
		<p>
		<?php  echo ("Autorisé ou bloqué") ?> :&nbsp;
			<select class="selectTimesheetLine" name="select_authlocking" onchange="javascript:document.forms.form_main_selection.submit();">
				<option value="all" <?php  echo ($_POST['select_authlocking']=='all'?" selected ":"") ?>><?php  echo ("Tous...") ?></option>
				<option value="1" <?php  echo ($_POST['select_authlocking']=='1'?" selected ":"") ?>><?php  echo ("Bloqué") ?></option>
				<option value="0" <?php  echo ($_POST['select_authlocking']=='0'?" selected ":"") ?>><?php  echo ("Autorisé") ?></option>
			</select>
		</p>
		
</form>
<?php  echo drawRMenuFooter() ?>



<?php 
  $act_array = array(
          array
          (
          		"access" => "admin.users..addmod",
                "picto" => "add",
                "name" =>("Ajouter un utilisateur"),
                "url" => "/o_admin/users/popup_add/index.php",
                "width" => 650,
                "height" => 600,
                "popup" => "jdiag"
           ),
           array
          (
                "access" => "admin.users..addmod",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "admin.users..addmod",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
           array
           (
          		"access" => "admin.users..addmod",
                "picto" => "unlock",
                "name" =>("Déverrouiller les utilisateurs bloqués dans la sélection faite"),
               	"url" => "javascript: unlockUsers();",
                //"url" =>"/o_admin/users/popup_unlock/index.php",
                "width" => 1,
                "height" => 1,
                "popup" => ""
            ) 
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  


  $expl_array = array(
                  array("","iconCircleCheck","unlock",("Profil autorisé")),
                  array("","iconCircleWarn","lock",("Profil bloqué")),
                  array("","iconCircleCheck","check",("Profil actif")),
                  array("","iconCircleWarn","denied",("Profil fermé")),
                  array("admin.users..addmod","","edit",("Mettre à jour l'utilisateur (actif?, infos,..)")),
                  array("admin.users..addmod","","trash",("Supprimer l'utilisateur")),
        	  );
  echo drawRMenuExplanation(("Légende"),$expl_array);

?>

