﻿<?php 
$l_path = "/";
$ACCESS_key = "admin.users..";
$security_domain = "admin";
$domain_categ = "users";
$right_menu_visibility = true;
//require $root_path.'inc/header.inc.php';

$main_url = $root_path."o_admin/users/index.php?list=1";

if (!array_key_exists('orderby',$_POST))
{
	$_POST['orderby'] = '';
}

if (!array_key_exists('select_userlocking',$_POST))
{
	$_POST['select_userlocking'] = '0';
}

if (!array_key_exists('select_authlocking',$_POST))
{
	$_POST['select_authlocking'] = 'all';
}

if (!array_key_exists('select_userattachement',$_POST))
{
	$_POST['select_userattachement'] = 'all';
}

// loop on the users
$kuery = "SELECT c_user.user_id,
				REPLACE(GROUP_CONCAT(DISTINCT c_group.name),',',', ') as name,
				c_user.lastname,
				c_user.firstname,
				c_user.locked,
				c_user.lockauth,
				c_user.login,
				c_user.email,
				c_user.erm_code
			FROM c_user
				LEFT JOIN c_group_users 
					ON c_group_users.user_id = c_user.user_id 
				LEFT join c_group 
					ON c_group.group_id = c_group_users.group_id";

// filter
$where = " WHERE ";
if ($_POST['select_userlocking'] != 'all')
{
	$kuery = $kuery. $where. " c_user.locked='".$_POST['select_userlocking']."' ";
	$where = " AND ";
}
if ($_POST['select_authlocking'] != 'all')
{
  if ($_POST['select_authlocking'] == '1') {
	 $kuery = $kuery. $where. " c_user.lockauth >= ". SystemParams::getParam('security*pwd_error') ." ";
	} else {
	 $kuery = $kuery. $where. " c_user.lockauth < ". SystemParams::getParam('security*pwd_error') ." ";
  }
  $where = " AND ";
}
if ($_POST['select_userattachement'] != 'all')
{
	if ($_POST['select_userattachement'] == 1) $kuery = $kuery. $where. " c_user.company!='INTERNAL' ";
	else $kuery = $kuery. $where. " c_user.company='INTERNAL' ";
}

$kuery = $kuery . $where . "c_user.login != 'tpo' ";

// Group by
$kuery .= " GROUP BY c_user.user_id,
				c_user.lastname,
				c_user.firstname,
				c_user.locked,
				c_user.login,
				c_user.email";

$kuery = $kuery ." ORDER BY c_user.lastname";


$grid = new UserGrid($kuery);
$out =  $grid->display();


?>
<script>
	function unlockUsers() {
		var premier = true;
		//var url = "popup_actif/actif?vids=";
		var url = "";
		var aInput = document.getElementsByTagName('input');
		for (var i = 0; i < aInput.length ; i++)
		{
			if (aInput[i].type == 'checkbox' && aInput[i].id != '')
			{
				if (aInput[i].checked)
				{
					if (premier)
					{
						premier = false;
						//alert(aInput[i].id);
						url += aInput[i].id.substr(4,11);
					}
					else
					{
						//alert(aInput[i].id);
						url += "|" + aInput[i].id.substr(4,11);
					}
				}
			}
		}
	
		if (!premier)
		{
			//url = "popup_actif/index.php?vids=1|3|4"
			//this.location = url;
			//alert(url);
			//openWindoo(url,'Activation',300,200);
			//window.open(url);
			var theData = {
	    		uids : url
	    	} ;
	    	//alert (theData);
			jQuery.ajax({
				type : 'GET',
				url : "/app.php/unlockUsers",
				data : theData,
				success : function(data) {
					alert(data);
				//$('#dialog').html(data);
				//$("#dialog").dialog("open");
				 location.reload();
				//
				}
			}); 
			
		}
		
	}
</script>