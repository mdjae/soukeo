<div id="myModalSuppr<?php echo trim($_POST['entity'])?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Supprimer le <?php echo trim($_POST['entity'])?></h3>
    </div> <!-- modal-header -->
  
    <div class="modal-body">
        Êtes-vous sûr de vouloir supprimer la ligne N° <?php echo trim($_POST['entity_id']) ?> :
        <br/>
        -<?php echo $entity_to_delete -> getLabel()?> 

  </div> <!-- modal-body -->
  
  <div class="modal-footer">
    <button class="btn btn-danger"  data-dismiss="modal" aria-hidden="true" onClick="deleteReferentiel('<?php echo trim($_POST['entity']); ?>', <?php echo trim($_POST['entity_id']) ?>)">Supprimer</button>
    <button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">Annuler</button>
  </div>
</div> <!-- #myModalCreditPack -->
