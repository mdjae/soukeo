<div id="myModalCreditPack" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modifier le pack</h3>
    </div> <!-- modal-header -->
  
    <div class="modal-body">
        <form class="form-horizontal" id="form_edit_pack" name="form_edit_pack">
    
        <div class="control-group">
            <label class="control-label" for="edit_label_pack">Libellé :</label>
            <div class="controls">
                <input type="text" id="edit_label_pack" placeholder="Titre..." name="edit_label_pack" value="<?php echo $pack_credit -> getLabel()?>"> 
            </div>
        </div><!-- control-group -->
      
        <div class="control-group">          
            <label class="control-label" for="edit_value_pack">Valeur en crédit :</label>
                <div class="controls">
                    <input type="text" id="edit_value_pack" placeholder="Valeur + ou - en crédits" name="edit_value_pack" value="<?php echo $pack_credit -> getValue() ?>">
                </div>
        </div>
                            
        <div class="control-group">          
            <label class="control-label" for="edit_price_pack">Valeur d'achat :</label>
                <div class="controls">
                    <div class="input-append">
                        <input type="text" id="edit_price_pack" placeholder="Valeur en euros" name="edit_price_pack" value="<?php echo $pack_credit -> getPrice() ?>" >
                        <span class="add-on">€</span>
                    </div>
                </div>
        </div>
              
        <button class="btn btn-primary" onClick="validEditPack(<?php echo $pack_credit -> getPack_id() ?>);return false;" data-dismiss="modal" aria-hidden="true">Enregistrer</button>
        <button type="reset" class="btn btn-inverse">Reset</button> 
    </form>
  </div> <!-- modal-body -->
  
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div> <!-- #myModalCreditPack -->
