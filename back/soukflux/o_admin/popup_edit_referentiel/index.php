<div id="myModal<?php echo trim($_POST['entity'])?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modifier le <?php echo $_POST['entity']?></h3>
    </div> <!-- modal-header -->
  
    <div class="modal-body">
        <form class="form-horizontal" id="form_edit_<?php echo trim($_POST['entity'])?>" name="form_edit_<?php echo trim($_POST['entity'])?>">
    
        <div class="control-group">
            <label class="control-label" for="edit_label_<?php echo trim($_POST['entity'])?>">Libellé :</label>
            <div class="controls">
                <input type="text" id="edit_label_<?php echo trim($_POST['entity'])?>" placeholder="Titre..." name="edit_label_<?php echo trim($_POST['entity'])?>" value="<?php echo $entity_to_edit -> getLabel()?>"> 
            </div>
        </div><!-- control-group -->

              
        <button class="btn btn-primary" onClick="validEditReferentiel('<?php echo trim($_POST['entity'])?>' , <?php echo $_POST['entity_id']?>);return false;" data-dismiss="modal" aria-hidden="true">Enregistrer</button>
        <button type="reset" class="btn btn-inverse">Reset</button> 
    </form>
  </div> <!-- modal-body -->
  
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div> <!-- #myModalCreditPack -->
