<div id="myConfirmModalDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $_POST['title'] ?></h3>
    </div> <!-- modal-header -->
  
    <div class="modal-body">
        <?php echo $_POST['message'] ?> 

  </div> <!-- modal-body -->
  
  <div class="modal-footer">
    <button id="btnYes" class="btn btn-success"  data-dismiss="modal" aria-hidden="true">Valider</button>
    <button id="btnNo"  class="btn btn-inverse"  data-dismiss="modal" aria-hidden="true">Annuler</button>
  </div>
</div> <!-- #myModalCreditPack -->
