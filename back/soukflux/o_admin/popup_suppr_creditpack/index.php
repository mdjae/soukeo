<div id="myModalSupprCreditPack" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Supprimer le pack</h3>
    </div> <!-- modal-header -->
  
    <div class="modal-body">
        Êtes-vous sûr de vouloir supprimer le pack N° <?php echo $pack_credit -> getPack_id() ?> :
        
        -<?php echo $pack_credit -> getLabel()?> - <?php echo $pack_credit -> getPrice() ?> €

  </div> <!-- modal-body -->
  
  <div class="modal-footer">
    <button class="btn btn-danger"  data-dismiss="modal" aria-hidden="true" onClick="deleteCreditPack(<?php echo $pack_credit -> getPack_id() ?>)">Supprimer</button>
    <button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">Annuler</button>
  </div>
</div> <!-- #myModalCreditPack -->
