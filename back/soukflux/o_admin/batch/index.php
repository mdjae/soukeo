﻿<?php 
//$root_path = "../../";
$ACCESS_key = "admin.batch..";
$security_domain = "admin";
$domain_categ = "batch";
$right_menu_visibility = true;


// init step of SQL limit
$step_limit = SystemParams::getParam('system*nlist_element');

// loop sur les programmations
$query = "select b.prog_id,
b.group,
				b.class,
				b.comment,
				b.hour,
				b.day,
				b.month,
				b.year,
				b.email,
				b.active,
				b.params,
				max(start_date) as last_exec
			FROM c_batch b
				LEFT JOIN c_batch_log l
					ON b.prog_id = l.prog_id
			GROUP BY b.prog_id,
				b.class,
				b.comment,
				b.hour,
				b.day,
				b.month,
				b.year,
				b.email,
				b.active,
				b.params
			ORDER BY b.group,b.order,  b.prog_id";
$grid = new BatchGrid($query);


?>