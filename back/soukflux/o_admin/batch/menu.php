﻿<?php 
  $act_array = array(
          array("access" => "",
                "picto" => "add",
                "name" =>("Ajout d'une programmation"),
                "url" => "/o_admin/batch/popup_addmod/index.php",
                "width" => 600,
                "height" => 600,
                "popup" => "jdiag"
                )    
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
  $expl_array = array(
                  array("admin.batch..","iconCircleCheck","check",("Programmation active")),
                  array("admin.batch..","iconCircleWarn","close",("Programmation inactive")),
                  array("admin.batch..","","zoomin",("Suivi des logs")),
                  array("admin.batch..","","edit",("Modification d'une programmation")),
                  array("admin.batch..","","trash",("Supprimer la programmation")),
                  array("admin.batch..","","reload",("Exécuter le batch")),
				  array("admin.batch..","","download",("Récupérer dernier fichier généré par le batch"))
          );
  echo drawRMenuExplanation(("Légende"),$expl_array);
?>
