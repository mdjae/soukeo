﻿<?php 
$root_path = "../../../";
$top_body = ('Programmation');
$ACCESS_key = "admin.batch..";
require $root_path . 'inc/popup.header.inc.php';
//var_dump($root_path . 'inc/popup.header.inc.php');
require_once $root_path .	'inc/offipse.inc.php';

$query = "SELECT start_date,
				end_date,
				return_code,
				status,
				log
			FROM c_batch_log
			WHERE prog_id = " . $_GET['prog_id'] . "
			ORDER BY start_date DESC";
$f = new BatchLogGrid($query);
echo $f->display();
require $root_path . "inc/popup.footer.inc.php";
?>