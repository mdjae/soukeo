﻿<?php 
// Init
$ACCESS_key = "admin.batch..";
$root_path = "../../";
require $root_path.'inc/webservice.header.inc.php';

if (SystemAccess::canAccess("admin.batch.."))
{ 
	// Quel fonction est appelée ?
	if (array_key_exists('function',$_GET))
	{
		// La clé de fonction existe
		// Quelle fonction ?
		switch ($_GET['function'])
		{
			case 'deleteBatch':
				if (array_key_exists('prog_id',$_GET))
				{
					// Suppression des logs
					$query = "DELETE FROM c_batch_log
								WHERE prog_id = " . $_GET['prog_id'];
					dbQuery($query);
					
					$query = "DELETE FROM c_batch
								WHERE prog_id = " . $_GET['prog_id'];
					dbQuery($query);
					echo "true";
				}
				else
				{
					echo "false";
				}
			break;
		}
	}
}
else
{
	// Dans le cas contraire on ne faire rien
	echo "false";
}
			
require $root_path . "inc/webservice.footer.inc.php";
?>
