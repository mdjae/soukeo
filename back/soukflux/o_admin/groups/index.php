﻿<?
//$root_path = "../../";
$ACCESS_key = "admin.groups..";
$security_domain = "admin";
$domain_categ = "groups";
$right_menu_visibility = true;
//require $root_path.'inc/header.inc.php';
$main_url = $root_path."o_admin/groups/index.php?list=1";

if (!array_key_exists('orderby',$_GET))
{
	$_GET['orderby'] = '';
}

// loop on the groups
$kuery = "SELECT g.group_id,
              g.name,
						  g.descr,
						  g.add_date,
						  g.mod_date ,
						  g.email,
						  concat(d.firstname,' ',d.lastname) as who_mod,
						  count(u.group_id) as nb 
					FROM c_group g  
					 left join c_user d on d.user_id = g.user_id
					 LEFT JOIN c_group_users u
					 	ON g.group_id = u.group_id
					 	WHERE 
					 	g.group_id != '3'
					 GROUP BY g.group_id, g.name, g.descr, g.add_date, g.mod_date,
					 	concat(d.firstname,' ',d.lastname)
					ORDER BY g.name ";

$grid = new GroupGrid($kuery);
//echo $grid->display();

// -- FOOTER
//require $inc_path.'footer.inc.php';
?>
