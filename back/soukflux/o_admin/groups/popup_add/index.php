﻿<?php 
$msg_info = "";
$root_path = "../../../";
$ACCESS_key = "admin.groups..addmod";
require $root_path . 'inc/form.header.inc.php';
require $inc_path . "libs/image.inc.php";
$copy_url = "index.php?list=1";
$need_js_checkboxes = true;
$debug = false; 

if (array_key_exists('g_id',$_GET))
{
	$_POST["open_code"] = $_GET["g_id"];
}

// flag pour la mise � jour de la s�curit� si l'update ou la cr�ation d'un groupe est r�ussie
$security_update = false;
$current_group_security_array = array();
$current_group_domains_array = array();

/////////////////////
// TEXT
$msg_exists =			("Ce groupe existe déjà !");
$msg_idko=				("Récupération du nouvel identifiant impossible !");
$msg_update01=			("Le groupe");
$msg_update02=			("a été mis à jour.");
$msg_update03=			("Le nouveau groupe a été enregistré.");
$msg_nomembers=			("Aucun utilisateur n'est associé à ce groupe.");

$title_grp=				("Groupes existants");
$title_update=			("Modification d'un groupe existant");
$title_rights =			("Droits d'accès et fonctionnalités");
$title_keys=			("Informations clés");
$title_members=			("Utilisateurs, membres de ce groupe");
$title_add=				("Ajouter un nouveau groupe");

$txt_but_save =			("Enregistrer");

$txt_date=				("Date d'ajout");
$txt_moddate=			("Date de modification");
$txt_lastuser=			("Dernier utilisateur");
$txt_user=				("Utilisateur");
$txt_id=				("Identifiant");
$txt_name=				("Nom");
$txt_descr=				("Description");

$txt_members_code=		("Code");
$txt_members_flname=	("Nom Prénom");
// Doit-on modifier un groupe ?
if (array_key_exists('modified',$_POST) &&
		isset($_POST['group_id']) &&
		isset($_POST['new_name']) && (trim($_POST['new_name'])!='') )
{
	$res_test = dbQueryOne("SELECT name 
							FROM c_group 
							WHERE name='". strtoupper($_POST['new_name']) ."' and group_id <> ".$_POST['group_id']);	
							
	if ($res_test["name"] == strtoupper($_POST['new_name'])) 
	{
		// erreur
		$msg_info = $msg_exists;
	}
	else
	{
		// Update du groupe
		$query = "UPDATE c_group SET 
					name =					'" . strtoupper(addslashes($_POST['new_name'])) . "',
					descr =					'" . addslashes($_POST['new_desc']) . "',
					email =					'" . addslashes($_POST['new_email']) . "',
					mod_date =				'" . date("Y-m-d H:i:s") . "',
					user_id =				'" . addslashes($_SESSION['smartuserid']) . "' 
				WHERE group_id = '" . $_POST['group_id'] . "'";
		
		dbQuery($query);
		
		$_POST["open_code"] = $_POST["group_id"];
		
		$security_update = true;
	}  
}

// Doit-on ajouter un nouveau groupe ?
if (array_key_exists('submitted',$_POST) &&
		isset($_POST['new_name']) && (trim($_POST['new_name'])!='') &&
		isset($_POST['new_desc']) && (trim($_POST['new_desc'])!='') )
{

	$res_test = dbQueryOne("SELECT name 
							FROM c_group 
							WHERE name='". strtoupper($_POST['new_name']) ."'");	
	if ($res_test["name"] == strtoupper($_POST['new_name']))
	{
		// erreur
		$msg_info = $msg_exists;
	}
	else
	{
		dbQuery("INSERT INTO c_group 
					(
						name,
						descr,
						email,
						add_date,
						user_id 
					) VALUES
					(
						'" . strtoupper(addslashes($_POST['new_name'])) . "',
						'" . addslashes($_POST['new_desc']) . "',
						'" . addslashes($_POST['new_email']) . "',
						'" . date("Y-m-d H:i:s") . "',
						'" . addslashes($_SESSION['smartuserid']) . "' 
					)");
		

		$res_new = dbQueryAll("SELECT group_id FROM c_group WHERE name='". strtoupper($_POST['new_name']) ."'");
		if (count($res_new)==1)
		{
			$_POST['group_id'] = $res_new[0]['group_id'];
			$security_update = true;
			
		} else 
		{
			// erreur
			$msg_info = $msg_idko;
		}
	}
}

// Si un ajout ou un update de groupe a �t� r�alis�, alors on met � jour la s�curit�
// et les membres
//$security_update=true;
if (isset($security_update) && $security_update)
{
		// security
	
		
		if ( isset($_POST['group_id']) && ($_POST['group_id']!=0) && ($_POST['group_id']!='') ) {
			
			$query = "DELETE FROM c_group_security 
					WHERE group_id = '" . $_POST['group_id'] . "'";
			dbQuery($query);
			
			$kuery = "SELECT distinct seq, domain, catagory, function, special 
						FROM c_group_security 
						WHERE group_id=0 
						ORDER BY seq";
			//$res_g = dbQueryAll($kuery);
			$res_g = SystemAccess::getAccessList();
			
			$security_keys = "";

			//for ($g=0 ; $g<count($res_g) ; $g++)
			foreach($res_g as $r)
			{
				if (isset($_POST[$r["domain"].'_cb_'.$r["seq"]]) )
				{
					if ($security_keys!='') $security_keys .= ",";
					$security_keys .= "'".$_POST[$r["domain"].'_cb_'.$r["seq"]]."'";
					
					$query = "INSERT INTO c_group_security
							(
								group_id,
								seq,
								page_path,
								domain,
								catagory,
								function,
								special
							)
							VALUES
							(
								" . $_POST['group_id'] . ",
								'" . $r['seq'] . "',
								'" . $r['page_path'] . "',
								'" . $r['domain'] . "',
								'" . $r['catagory'] . "',
								'" . $r['function'] . "',
								'" . $r['special'] . "'
							)";
					//echo "<p>" . $query . "</p>";
					dbQuery($query);
				}	
			}
			
			// Mise � jour des membres de l'�quipe
			$query = "DELETE FROM c_group_users
						WHERE group_id = '" . $_POST['group_id'] . "'";
			dbQuery($query);
		
			// Remise en places des membres
			if (array_key_exists('members',$_POST))
			{
				foreach ($_POST['members'] as $user_id)
				{
					$query = "INSERT INTO c_group_users (group_id, user_id)
								VALUES (" . $_POST['group_id'] . "," . $user_id . ")";
					dbQuery($query);
				}
			}
			
			
			// r�ussite
			if (array_key_exists('modified',$_POST))
			{
				$msg_info = "<font color='green'>".$msg_update01." " . strtoupper($_POST['new_name']) . " ".$msg_update02."</font>";
			}

			if (array_key_exists('submitted',$_POST))
			{
				$msg_info = "<font color='green'>".$msg_update03."</font>";
			}
			
			echo "<script>javascript: parent.gridReload('group','../../');</script>";
			
		} else 
		{
			//erreur
			$msg_info = $msg_idko;
		}
}


			// Doit-on ouvrir un groupe ?
			if (array_key_exists('open_code', $_POST) && $_POST['open_code'] != "none")
			{
				// Oui
				$query = "SELECT c_group.group_id, 
								c_group.name,
								c_group.descr,
								c_group.email,
								c_group.add_date,
								c_group.mod_date,
								c_user.firstname,
								c_user.lastname 
							FROM c_group 
								left join c_user on c_user.user_id = c_group.user_id 
							WHERE group_id = ". $_POST['open_code'] ; 
				
				//echo $query . "<br/>";
				$res = dbQueryOne($query); 
				//print_r($res);
				//echo "<br/>";
		
				$group_id 	=	$res['group_id'];
				$name 	  	=	$res['name'];
				$tdesc		=	$res['descr'];
				$email 		=	$res['email'];
				$add_date 	=	$res['add_date'];
				$mod_date 	=	$res['mod_date'];
				$user 		=	$res['firstname']. ' '.$res['lastname'];
				
				// security data
				$kuery = "SELECT distinct concat(domain,'.',catagory,'.',function,'.',special) as security_key
						FROM c_group_security 
						WHERE group_id=".$group_id;
				//DEBUG
				//echo $kuery;
				$res_s = dbQueryAll($kuery);
				for ($s=0 ; $s<count($res_s) ; $s++)
				{
					$current_group_security_array[$s] = $res_s[$s]['security_key'];
				}
				
				$kuery = "SELECT distinct domain FROM c_group_security WHERE group_id=".$group_id;
				$res_s = dbQueryAll($kuery);
				for ($s=0 ; $s<count($res_s) ; $s++)
				{
					$current_group_domains_array[$s] = $res_s[$s]['domain'];
				}
			
			}
			else
			{
				$group_id 	=	"";
				$name 		=	"";
				$tdesc 		=	"";
				$add_date 	=	"";
				$mod_date 	=	"";
				$user 		=	$_SESSION['smartfirst']. ' '.$_SESSION['smartlast'];
			}
		?>

  
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
	<td colspan="2">
		<form class="contentform" action="<?php  echo $copy_url ?>" method="POST" onSubmit="selectAllUsers();">
		<?php if ($msg_info!='') {
    ?>
      <p>
  		  <b><font color=red><?php  echo $msg_info ?>&nbsp;</font></b>
  		</p>
  	<?php 
    } ?>
				<table cellpadding="2" cellspacing="2" border="0" width="100%">
			<?
				// Une phrase d'indication de l'action
				if (array_key_exists('open_code',$_POST) && $_POST['open_code'] != "none")
				{
					// Modification d'un groupe
			?>
				<tr><td colspan="2"><h3><?php  echo ("Modification d'un groupe existant") ?></h3></td></tr>
				<tr><td><p><?php  echo ("Date d'ajout") ?></p></td><td><p><b><?php  echo smartdate($add_date) ?></b></p></td></tr>
				<tr><td><p><?php  echo ("Date de modification") ?></p></td><td><p><b><?php  echo smartdate($mod_date) ?></b></p></td></tr>
				<tr><td><p><?php  echo ("Utilisateur") ?></p></td><td><p><b><?php  echo $user ?></b></p></td></tr>
				<tr>
					<td colspan="2">
						<h2><?php  echo htmlentities($title_keys) ?></h2>
						<hr>
					</td>
				</tr>
				<tr>
					<td>
						<p><?php  echo $txt_id ?></p>
					</td>
					<td>
						<input type="text" name="group_id2" value="<?php  echo $group_id ?>" DISABLED>
						<input type="hidden" name="group_id" value="<?php  echo $group_id ?>">
					</td>
				</tr>
			<?
				}
				else
				{
			?>
				<tr><td colspan="2"><h1><?php  echo ("Ajouter un nouveau groupe") ?></h1></td></tr>
				<tr><td><p><?php  echo ("Date d'ajout") ?></p></td><td><p><b><?php  echo $add_date ?></b></p></td></tr>
				<tr><td><p><?php  echo ("Date de modification") ?></p></td><td><p><b><?php  echo $mod_date ?></b></p></td></tr>
				<tr><td><p><?php  echo ("Dernier utilisateur") ?></p></td><td><p><b><?php  echo $user ?></b></p></td></tr>
				<tr>
					<td colspan="2">
						<h2><?php  echo ("Informations clés") ?></h2>
						<hr>
					</td>
				</tr>
			<?
				}
			?>
				<tr><td><p><?php  echo ("Nom") ?></p></td><td> <input type="text" name="new_name" value="<?php  echo $name ?>"></td></tr>
				<tr><td><p><?php  echo ("Description") ?></p></td><td> <input type="text" name="new_desc" value="<?php  echo $tdesc ?>" size="50"></td></tr>
				<tr><td><p><?php  echo ("Email") ?></p></td><td> <input type="text" name="new_email" value="<?php  echo $email?>" size="50"></td></tr>
<?
			// Membres du groupe

			// Si pas de groupe ouvert, alors la liste des membres est vide
			if ($group_id == '')
			{
				// Liste des utlisateurs
				$query = "SELECT u.user_id,
									u.firstname,
									u.lastname
							FROM c_user u
							WHERE u.locked = 0
							ORDER BY u.lastname";
				$res_user = dbQueryAll($query);

				// La liste des membres est vide
				$res_group_user = Array();
			}
			if ($group_id!='')
			{

				// Gestion des membres du groupe
				// On r�cup�re les utilisateurs qui ne sont pas dans l'�quipe
				$query = "SELECT g.group_id,
									u.user_id,
									u.firstname,
									u.lastname
							FROM c_user u
								LEFT JOIN c_group_users g
								ON u.user_id = g.user_id
									AND g.group_id = " . $_POST['open_code'] . "
							WHERE g.group_id IS NULL
							AND u.locked = 0
							ORDER BY u.lastname";
				$res_user = dbQueryAll($query);

				// On r�cup�re les utilisateurs du groupe
				$query = "SELECT g.group_id,
									g.user_id,
									u.firstname,
									u.lastname
							FROM c_group_users g, c_user u
							WHERE g.user_id = u.user_id
							AND u.locked = 0
							AND g.group_id = " . $_POST['open_code'] . "
							ORDER BY u.lastname";
				$res_group_user = dbQueryAll($query);
			}
?>
				<tr>
					<td colspan="2">
						<h2><?php  echo ("Utilisateurs, membres de ce groupe") ?></h2>
						<hr>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<script>
							function addGroup()
							{
								var usersBox = document.getElementById('users');
								var membersBox = document.getElementById('members');
								for (var i = usersBox.options.length - 1;  i >= 0; i--)
								{
									if (usersBox.options[i].selected)
									{
										// Ajout dans l'autre liste
										//alert(i);
										//alert(usersBox.options[i].text);
										//usersBox.options[i].selected = false;
										//membersBox.options[membersBox.length] = usersBox.options[i];
										membersBox.options[membersBox.length] = new Option(usersBox.options[i].text,usersBox.options[i].value);
										usersBox.options[i] = null;
									}
								}
			
							}
			
							function removeGroup()
							{
								var usersBox = document.getElementById('users');
								var membersBox = document.getElementById('members');
								for (var i = membersBox.options.length - 1;  i >= 0; i--)
								{
									if (membersBox.options[i].selected)
									{
										// Ajout dans l'autre liste
										//membersBox.options[i].selected = false;
										//usersBox.options[usersBox.length] = membersBox.options[i];
										usersBox.options[usersBox.length] = new Option(membersBox.options[i].text,membersBox.options[i].value);
										membersBox.options[i] = null;
									}
								}
			
							}
			
							function selectAllUsers()
							{
								var membersBox = document.getElementById('members');
								for (var i = 0;  i < membersBox.options.length; i++)
								{
									// Selection
									membersBox.options[i].selected = true;
								}
							}
						</script>
			
						<table border="0" width="100%">
							<tr>
								<td align="center" width="40%">
			
							<h2><?php  echo ("Utilisateurs") ?></h2>
							<select id="users" name="users" size="10" multiple>
			<?
					for ($i = 0; $i < count($res_user); $i++)
					{
			?>
								<option value="<?php  echo $res_user[$i]['user_id'] ?>"><?php  echo $res_user[$i]['firstname'] . " " . $res_user[$i]['lastname'] ?></option>
			<?
					}
			?>
							</select>
						</td>
						<td align="center">
							<a href="javascript: addGroup();"><img src="<?php  echo $skin_path ?>images/arrow_next.gif" border="0"/></a>
							<h2><a href="javascript: removeGroup();"><img src="<?php  echo $skin_path ?>images/arrow_previous.gif" border="0"/></a></h2>
						</td>
						<td align="center" width="40%">
							<h2><?php  echo ("Membres du groupe") ?></h2>
							<select id="members" name="members[]" size="10" multiple>
			<?
					for ($i = 0; $i < count($res_group_user); $i++)
					{
			?>
								<option value="<?php  echo $res_group_user[$i]['user_id'] ?>"><?php  echo $res_group_user[$i]['firstname'] . " " . $res_group_user[$i]['lastname'] ?></option>
			<?
				}
			?>
							</select>
						</td>
					</tr>
				</table>
				</td>
				</tr>
				</table>

				<table cellpadding="2" cellspacing="2" border="0" width="100%">
				<tr>
					<td colspan="3">
						<h2><?php  echo ("Droits d'accés et fonctionnalités") ?></h2>
						<hr>
					</td>
				</tr>
				<tr>
					<td valign="top" width="50%">
<?php  					
								// select distinct des domaines de s�curit�
								$kuery = "SELECT distinct seq,
												 domain, catagory, function, special, 
												 concat(domain,'.',catagory,'.',function,'.',special) as security_key
											FROM c_group_security 
											WHERE group_id=0   
											ORDER BY seq";
								//$res = dbQueryAll($kuery);
								$res = SystemAccess::getAccessList();
								
								$secu_last_domain = "";
								$secondCol = false;
								$thirdCol = false;
								
								$u = 0;
								for ($u=0 ; $u<count($res) ; $u++)
								foreach($res as $r)
								{
									$u++;
									
									if ($secu_last_domain != $r["domain"])
									{
									  	if (($u >= (count($res)/2) - 2)&&(!$secondCol))
									  	{
								        	$secondCol = true;
?>
                        </td><td valign="top" width="50%">
<?
                    					}
?>
										<script type="text/javascript">
											var cb_grp_<?php  echo $r["domain"] ?> = new CheckBoxGroup();
											cb_grp_<?php  echo $r["domain"] ?>.addToGroup("<?php  echo $r["domain"] ?>*");
											cb_grp_<?php  echo $r["domain"] ?>.setControlBox("cb_master_<?php  echo $r["domain"] ?>");
											cb_grp_<?php  echo $r["domain"] ?>.setMasterBehavior("all");
											cb_grp_<?php  echo $r["domain"] ?>.setMasterBehavior("some");
										</script>
				
                    <p>&nbsp;</p>
										<table cellpadding="0" cellspacing="3" bgcolor="#EFEFEF" width="100%">
										<tr><td width="20">
											<?php
										//	var_dump($r["domain"]);
									//		var_dump($current_group_domains_array);
											
											?>
										  <INPUT TYPE="checkbox" NAME="cb_master_<?php  echo $r["domain"] ?>" VALUE="<?php  echo $r["domain"] ?>" 
										      onClick="cb_grp_<?php  echo $r["domain"] ?>.check(this)"	<?php  echo (in_array($r["domain"],$current_group_domains_array)?" checked":"") ?>>
										</td><td>
										  <b><?php  echo SystemAccess::getLblSecu($r["domain"]) ?></b>
										</td>
										</tr>
										</table>
									<?
									}
									
									?>
									
									
						      <table cellpadding="0" cellspacing="0" width="100%">
										<tr>
                    <td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <?
                        if ($r["function"].'.'.$r["special"] != ".") {
      								//  var_dump	($r["security_key"],$current_group_security_array);
                  ?>
      									<td width="20">&nbsp;&nbsp;&nbsp;&nbsp;</td>
      						<?php } ?>
                    <td valign="top" width="20">
      									<INPUT TYPE="checkbox" NAME="<?php  echo $r["domain"].'_cb_'.$r["seq"] ?>" VALUE="<?php  echo $r["security_key"] ?>" 
      									    onClick="cb_grp_<?php  echo $r["domain"] ?>.check(this)" <?php  echo (in_array($r["security_key"],$current_group_security_array)?" checked":"") ?>>
    								</td>
                    
                    <td>
    									 <font size="-3">
                        <?
                        if ($r["function"].'.'.$r["special"] == ".")
      									{
      						?>
      									<b>
      						<?php } ?>
                        <?php if ($debug) echo $r["seq"]; ?>
    										<?php  echo _($r["description"]) ?>
    							<?
                  if ($r["function"].'.'.$r["special"] == ".") 
                  echo "</b>";
                  ?>			
    									 </font></td></tr>
</table>
						<?
									
									$secu_last_domain = $r["domain"];
									
								}
						?>
						
					</td>
				</tr>
				
				</table>	
			

    <hr>
    

			<?
				// Est-on en train de modifier un groupe ou d'en ajouter 1 ?
				if (array_key_exists('open_code', $_POST) && $_POST['open_code'] != "none")
				{
					// Modification
			?>
					<input type="hidden" name="modified" value="1">
			<?
				}
				else
				{
					// Ajout
			?>
					<input type="hidden" name="submitted" value="1">
			<?
				}
			?>
					<input type="submit" value="<?php  echo $txt_but_save ?>"> 
					
		</form>
		
	</td></tr>
</table>

<?
require $root_path . "inc/form.footer.inc.php";
?>
