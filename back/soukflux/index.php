﻿<?php 
$root_path = '';
// Est-ce que le fichier de configuration existe ?
if (file_exists("config/config.inc.php"))
{
	// Ok le fichier existe
	require_once $root_path .	"inc/offipse.inc.php";
 
	if (array_key_exists('last_seen',$_SESSION)) $url = $_SESSION['last_seen'];
	//else 
	$url = "common/login/index.php";

	// Redirection vers la page de login
	header ("location: " . $url);
}
else
{
	// Pas de fichier de configuration
	// On va vers la page de controle du serveur
	header("location: install/01checksrv/index.php");
}
?>