<?php
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessExportXML.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessExportXML.class.php";	
	require_once $root_path . "inc/offipse.inc.php";
	
}

class BatchImportIsbn extends Batch {
	private $_name = "BatchImportIsbn";
	static protected $_description = "Import de la base isbn";

	private static $db;
	private static $db2;

	protected $stmtAttr;
	protected $stmtCatalogProduct;
	protected $stmtValAttr;
	protected $stmtIsbn;
	
	protected $cat_id ; //ID DE LA CATEGORIE EN COURS DE TRAITEMENT 



	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);
		//CONNECTION BASE ISBN
		self::$db2 = new sdb2();

		//Connection base SoukFlux
		self::$db = new sdb();

		//chargement des livres catid = 1
		$this->cat_id = 1;


		$i = 0;

		/*
		 *
		 * IN DB ISBN
		 'isbn10' => string '2070720071' (length=10)    --> ATTR
		 'isbn13' => string '978-2070720071' (length=14) --> ATTR
		 'titre' => string 'Les Quartiers d'hiver - Prix Médicis 1988' (length=42) --> name
		 'auteur' => string 'Jean-Noël Pancrazi (Auteur)' (length=28) --> ATTR
		 'editeur' => string 'Gallimard' (length=9) --> ATTR
		 'edition' => string '' (length=0) --> ATTR
		 'date' => string '12 septembre 1990' (length=17) --> ATTR
		 'collection' => string 'Blanche' (length=7) --> ATTR
		 'type' => string 'Broché' (length=7) --> ATTR
		 'pages' => string '197' (length=3) --> ATTR
		 'langue' => string 'Français' (length=9) --> ATTR

		 **********************************************************************

		 * OUT DB SKFLUX
		 *
		 * Table : sfk_catalog_product
		 * Chps sfkcp		-->		champs isbn
		 id_produit			--> 	AUTO GEN
		 sku	 				--> 	isbn 10  -->
		 categories			-->		DUNNO
		 name				--> 	titre
		 description 		--> 	null	 ( for test concat all data from table isbn.isbn)
		 short_description 	--> 	null
		 price	 			--> 	null
		 weight				--> 	null
		 country_of_manufacture --> 	null
		 meta_description	--> 	null
		 meta_keyword	 	--> 	null
		 meta_title			--> 	null
		 image				--> 	null
		 small_image			--> 	null
		 thumbnail			--> 	null
		 attribute_set		--> 	null
		 date_create			--> 	now()
		 date_update			--> 	now()
		 *
		 */


		$this -> getIsbnToProcess();
		$this -> prepInsertSfkCp();
		$this -> prepAttribut();
		$this -> prepProductCategorie();
		$this -> prepInsertValAttr();

		$ins = 0;
		$checkAttr = false;


		while ($arr = $this -> stmtIsbn -> fetch(PDO::FETCH_ASSOC)) {
			$lastinsert = 0;
			//CHECK DES ATTRIBUT Pour la catégorie en cours POUR INSERT EVENTUELLE
			if (!$checkAttr) {
				$this -> ChecKAndUpdateAtt($arr);
				
				//Génération du tableau associatif id_attribut_code_attr
				$sqlT = "select id_attribut, code_attr from sfk_attribut where cat_id= '$this->cat_id' ";
				$res = self::$db -> getAll($sqlT);
				foreach ($res as $val) {
					$artest[$val['code_attr']] = $val['id_attribut'];
				}
				$checkAttr = true;
			}



			$V['sku'] = $arr['isbn10'];
			$V['categories'] = $this->cat_id;
			$V['name'] = $arr['titre'];
			$V['description'] = $arr['titre'] . '<br/>' . $arr['auteur'] . '<br/>' . $arr['editeur'] . '<br/>' . $arr['edition'] . '<br/>' . $arr['date'];
			$V['short_description'] = $arr['titre'] . '<br/>' . $arr['auteur'];
			$V['price'] = null;
			$V['weight'] = null;
			$V['country_of_manufacture'] = null;
			$V['meta_description'] = null;
			$V['meta_keyword '] = null;
			$V['meta_title'] = null;
			$V['image'] = null;
			$V['small_image'] = null;
			$V['thumbnail'] = null;

			$ins += $this -> addRowCatProduct($V);

			$lastinsert = self::$db -> lastInsertId() ? self::$db -> lastInsertId() : $this -> getIdProduct($V['sku']);

			$this -> addProductCategorie($lastinsert, $this->cat_id);

			foreach ($arr as $key => $val) {
				if ($key == 'type') $key = 'format';
				if (isset($artest[$key])) {
					//c'est une colonne attribut spécifique de la catégorie ( l'attribut existe déja en base);
					$PA['id_produit'] = $lastinsert;
					$PA['id_attribut'] = $artest[$key];
					$PA['value'] = $val;
					if($val != '' || $val != null)
					$this -> addRowValAttr($PA);
				}
			}

		}

		$this -> _appendLog('nb insert ' . $ins);

		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

	}



	protected function getIsbnToProcess() {
		$this -> stmtIsbn = self::$db2 -> prepare("select * from isbn.isbn where langue ='Français'  and titre != '' and auteur != '' and auteur != '- (Auteur)' and collection != '.' and pages > 1
									 and editeur != '' and date != '' and date != '\'jQuery\', function('  and edition != '1' and pages != '' and type != '' limit 200000 ");
		$this -> stmtIsbn -> execute();
	}

	protected function ChecKAndUpdateAtt($arr) {
		$sqlT = "select code_attr as ID from sfk_attribut where cat_id= '$this->cat_id' ";
		$attrUsed = $this -> formatarr(self::$db -> getAll($sqlT));
		//Don t want titre in ATTR
		$attrUsed[] = 'titre';
		
		foreach ($arr as $k => $v) {
			if (!in_array($k, $attrUsed)) {
				if ($k == 'type' ) $k = "format";
				$A['cat_id'] = $this->cat_id;
				$A['code_attr'] = $k;
				$A['label_attr'] = $k;
				$this -> addAttribut($A);
			}
		}
	}

	protected function addProductCategorie($id_product, $cat_id) {
		$null = null;
		$this -> stmtProductCat -> bindParam(1, $cat_id ? $cat_id : $null);
		$this -> stmtProductCat -> bindParam(2, $id_product ? $id_product : $null);
		$this -> stmtProductCat -> execute();
	}

	protected function prepProductCategorie() {
		$sql = "INSERT INTO sfk_cat_product (cat_id, id_produit) VALUES ( ?, ?) ";
		$this -> stmtProductCat = self::$db -> prepare($sql);

	}

	protected function prepInsertValAttr() {
		$insValattr = "INSERT INTO sfk_product_attribut  (id_produit, id_attribut, value )
					 				VALUES (?,?,?) ON DUPLICATE KEY UPDATE
					 				value = VALUES(value)
					 
					 ";
		$this -> stmtValAttr = self::$db -> prepare($insValattr);
	}

	protected function getIdProduct($sku) {
		$sql = "SELECT id_produit from sfk_catalog_product where sku = '$sku' ";
		return self::$db -> getone($sql);
	}

	public function addRowValAttr($V) {
		$null = null;
		$this -> stmtValAttr -> bindParam(1, $V['id_produit'] 	? $V['id_produit'] : $null);
		$this -> stmtValAttr -> bindParam(2, $V['id_attribut']	? $V['id_attribut'] : $null);
		$this -> stmtValAttr -> bindParam(3, $V['value'] 		? $V['value'] : $null);

		$this -> stmtValAttr -> execute();

	}

	public function prepInsertSfkCp() {

		$sql = "INSERT INTO `sfk_catalog_product` (
					`sku`,`categories`,`name`,`description`,`short_description`,`price`,
					`weight`,`country_of_manufacture`,`meta_description`,`meta_keyword`,
					`meta_title`,`image`,`small_image`,`thumbnail`,`date_create`)
				VALUES ( ?,?,?,?,?, ?,?,?,?,?,?,?,?,?,NOW() )
				ON DUPLICATE KEY UPDATE 
					categories	=VALUES(categories),
					name		=VALUES(name),
					description	=VALUES(description),
					short_description=VALUES(short_description),
					price		=VALUES(price),
					weight		=VALUES(weight),
					country_of_manufacture	=VALUES(country_of_manufacture),
					meta_description=VALUES(meta_description),
					meta_keyword	=VALUES(meta_keyword),
					meta_title		=VALUES(meta_title),
					image			=VALUES(image),
					small_image		=VALUES(small_image),
					thumbnail		=VALUES(thumbnail)
				";
		$this -> stmtCatalogProduct = self::$db -> prepare($sql);

	}

	public function addRowCatProduct($V) {
		$null = null;
		$this -> stmtCatalogProduct -> bindParam(1 , $V['sku'] ? $V['sku'] : $null);
		$this -> stmtCatalogProduct -> bindParam(2 , $V['categories'] ? $V['categories'] : $null);
		$this -> stmtCatalogProduct -> bindParam(3 , $V['name'] ? $V['name'] : $null);
		$this -> stmtCatalogProduct -> bindParam(4 , $V['description'] ? $V['description'] : $null);
		$this -> stmtCatalogProduct -> bindParam(5 , $V['short_description'] ? $V['short_description'] : $null);
		$this -> stmtCatalogProduct -> bindParam(6 , $V['price'] ? $V['price'] : $null);
		$this -> stmtCatalogProduct -> bindParam(7 , $V['weight'] ? $V['weight'] : $null);
		$this -> stmtCatalogProduct -> bindParam(8 , $V['country_of_manufacture'] ? $V['country_of_manufacture'] : $null);
		$this -> stmtCatalogProduct -> bindParam(9 , $V['meta_description'] ? $V['meta_description'] : $null);
		$this -> stmtCatalogProduct -> bindParam(10, $V['meta_keyword'] ? $V['meta_keyword'] : $null);
		$this -> stmtCatalogProduct -> bindParam(11, $V['meta_title'] ? $V['meta_title'] : $null);
		$this -> stmtCatalogProduct -> bindParam(12, $V['image'] ? $V['image'] : $null);
		$this -> stmtCatalogProduct -> bindParam(13, $V['small_image'] ? $V['small_image'] : $null);
		$this -> stmtCatalogProduct -> bindParam(14, $V['thumbnail'] ? $V['thumbnail'] : $null);
		//$this -> stmtCatalogProduct -> bindParam( 15, $V['date_create'] ? $V['date_create'] : $null);
		//$this -> stmtCatalogProduct -> bindParam( 16, $V['date_update'] ? $V['date_update'] : $null);

		return $this -> stmtCatalogProduct -> execute();
	}

	protected function formatarr($rs) {
		$arr = array();
		foreach ($rs as $R) {
			$arr[] = $R['ID'];
		}
		return $arr;
	}

	protected function prepAttribut() {
		$sql = "INSERT INTO sfk_attribut ( cat_id , code_attr, label_attr ) 
				values ( ?, ? , ? )";
		$this -> stmtAttr = self::$db -> prepare($sql);

	}

	protected function addAttribut($V) {
		$null = null;
		$this -> stmtAttr -> bindParam(1, $V['cat_id'] 		? $V['cat_id'] : $null);
		$this -> stmtAttr -> bindParam(2, $V['code_attr'] 	? $V['code_attr'] : $null);
		$this -> stmtAttr -> bindParam(3, $V['label_attr'] 	? $V['label_attr'] : $null);
		return $this -> stmtAttr -> execute();
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
