<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";

} else {
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchModifUrlImg extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchModifUrlImg";
	static protected $_description = "[TMP] [RENAME IMAGE] Rename des images pour URL correcte ";

	private static $db;
	
	protected $path = "/var/www/media.avahis/media/catalog/product/";
	protected $local_url = "http://media.avahis.com/media/catalog/product/";
	
	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);


		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		//Prepare stmt
		self::$db -> prepUpdateLovalImage();

		$listeProd = self::$db -> getAllProductsImage();


		$nbProds = 0;
		
		// pour chaque produit dans la table grossiste et pour le grossiste LDLC
		foreach ($listeProd as $prod) {

			
			$url = $prod['local_image'];

			
			$cat = $prod['categories'];
			
			$tmp = explode("/", $url);
			$tmp = end($tmp);
			
			
			$url = $this->local_url . $prod['categories'] . "/" . $tmp ;
			
			$this -> _appendLog("Prod ".$prod['id_produit']." - Image : ".$url);	
			
			self::$db -> updateLocalImage($prod['id_produit'], $url);
			$nbProds++;
			

		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
	}



	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
