<?php
setlocale(LC_ALL, 'fr_FR.UTF-8');
/**
 * Ce fichier sert de routeur du framework
 * en fonction de l'url envoyee, on va rediriger vers la bonne action
 */
$root_path = '';

require_once $root_path . "inc/offipse.inc.php";

$test = true;

$mt1 = microTime();

$_SESSION['last_seen'] = $_SERVER['REQUEST_URI'];
$title = _("Soukeo Flux");
if (PHP_SAPI === 'cli') {
	$_SESSION['isauth'] = true;
	$_SERVER['PATH_INFO'] = "/save";
}

if ($_SESSION['isauth']) {

	if (PHP_SAPI === 'cli') {

		$user = BusinessUser::getClassUser('tpo');
	} else {
		$user = BusinessUser::getClassUser($_SESSION['smartlogin']);

	}

	// ------- Dispatch switch Admin -----
	$view  = new BusinessView();
	$skfView = new BusinessSoukeoView();
	$skf   = new BusinessSoukeoModel();
	
	//$skfPS =
	
	
	//	$data = new BusinessMpfModel();
	
	// break;
	if ($user -> getAdmin() === true) {
		//var_dump($_SERVER['PATH_INFO']);
		if ($_SERVER['ORIG_PATH_INFO']) 
			$_SERVER['PATH_INFO'] = $_SERVER['ORIG_PATH_INFO'];
		
		if ($_SERVER['PATH_INFO'] == "")
			$_SERVER['PATH_INFO'] = "/o_admin";
		
		switch ($_SERVER['PATH_INFO']) {
			
	
			case '/loadshowdialog' :
							$dispatch = true ;	
				ob_start();
				include ("./".$_POST['url']);
				$out = ob_get_contents();
				ob_end_clean();
				$view -> assign("out", $out);
				$template = "rendered";
			break;
				
			case '/common/pwd/' :
				include ("./common/pwd/index.php");
				$view -> assign("out", $out);
				$template = "rendered";
				
			break;
				
			case '/o_admin/groups/' :
				include ("./o_admin/groups/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/groups/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;

			case '/o_admin/system/' :
				include ("./o_admin/system/index.php");
				$view -> assign("out", $out);
				$template = "rendered";
			break;

			case '/o_admin' :
				include ("./o_admin/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

				break;

			case '/o_admin/batch/' :
				include ("./o_admin/batch/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/batch/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

				break;

			case '/o_admin/users/' :
				include ("./o_admin/users/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/users/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case "/unlockUsers" :
				$dispatch = true;
				ob_start();
				include ("./o_admin/users/popup_unlock/index.php");
				$popup_unlock = ob_get_contents();
				ob_end_clean();
				$out = $popup_unlock ;
			break;

			case    "/gestion/categorie" :
				$ACCESS_key = "gestion.categorie..";

					$template = "listingAvahis";
								//CATEGORIES AVAHIS
				$data = $skf->getSsCatByParentId();
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);

			break;
			
			//Permet d'afficher l'arbre de catégorie déroulant
			//Chargeant les sous catégories en AJAX
			case '/showcat' : 
				$dispatch = true ;
				$data = ($skf->getSsCatByParentId($_POST['catid']));
				$ct = $_POST['level']+1;
				$out = $view->showcat($data, $ct);
				
			break;
			
			//Permet d'afficher l'arbre des catégories avec les checkboxess dans les popup
			//Les sous catégories sont rechargés ici en Ajax
			case '/showtreecheckbox' : 
				$dispatch = true ;
				$data = ($skf->getSsCatByParentId($_POST['catid']));
				$ct = $_POST['level']+1;
				$out = $view->showTreeCheckbox($data, false , $ct );
				
			break;
			
			
			//Route permettant juste de faire appel à la fonction du model 
			//getCatIdTree permettant d'obtenir la parenté entiere d'une catégorie sous forme de
			//chaine idCatGrandPere - idCatPere - idCatFils
			case '/getidcattree':
				$dispatch = true;
				if(isset($_POST['cat_id_assoc'])){
					$out = $skf -> getCatIdTree($_POST['cat_id_assoc']);
				}
			break;
				
				
			//association à une catégorie pour UN produit lorsqu'on est sur le pop up d'ajout
			case '/assoccategprod':
				$dispatch = true;
				
				//Cette page est appellée deux fois mais on n'insert que si on a reçu en POST
				//insert
				if($_POST['insert'] == true){
					//Si aucune catégorie n'est associée à ce moment la ...
					if(!$skf -> isCatAssoc($_POST['id_vendor'], $_POST['cat_ecom'])){
						//On profite de cette ajout produit pour aussi associer cet categ ecom à une categ Avahis
						$skf -> addAssocCatVendor($_POST['id_vendor'], $_POST['cat_ecom'], end($_POST['cat_id']));
						$data['catIdTree'] = $skf -> getCatIdTree(end($_POST['cat_id']));
						$data['newcat'] ="newcat";
						$data['newcat_id'] = end($_POST['cat_id']);
						$out = json_encode($data);
					}
					else{
						$data['catIdTree'] = $skf -> getCatIdTree(end($_POST['cat_id']));
						$data['newcat_id'] = end($_POST['cat_id']);
						$out = json_encode($data);
					}
				//Cas normal permettant d'envoye l'ID de la catégorie associée
				//Ainsi qu'une chaine de type Root > Cat > sCat etc à AJAX sous forme JSON
				}else{
					$tmp = "";
					foreach ($_POST['cat_id'] as $id) {
						$cat = $skf -> getCatbyId($id);
						
						$tmp .= $cat['cat_label']. " > ";
					}
					//Envoi de données en JSON
					$arr = array('cat_id' => end($_POST['cat_id']), 
								 'cat_label' => substr($tmp, 0, strlen($tmp)-2));
					$out = json_encode($arr);
				}
				
			break;
			
			
			//appelé en AJAX pour l'association entre catégorie ecommercant
			case '/assoccateg':
				$dispatch = true;
				

				//Cette page est appellée deux fois mais on n'insert que si on a reçu en POST
				//insert
				if($_POST['insert'] == true){
					// CAS DES GROSSISTES //////////////////////////////////////////////////////////////
					if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
						$db = new BusinessSoukeoGrossisteModel();
						
						$db -> addAssocCatVendor($_POST['id_vendor'], $_POST['cat_ecom'], end($_POST['cat_id']));
						//Pour la navigation dans cette catégorie
						$_SESSION['gr']['cat_id_assoc'] = end($_POST['cat_id']);
						$_SESSION['gr']['correspondance'] = $skf ->getStrCateg(end($_POST['cat_id']));
						
						//On renvoie la chaine d'id catégorie associée pour le lien (Voir)
						$out = $skf -> getCatIdTree(end($_POST['cat_id']));
					}
					// CAS DES ECOMMERCANTS ///////////////////////////////////////////////////////////
					else{
						$skf -> addAssocCatVendor($_POST['id_vendor'], $_POST['cat_ecom'], end($_POST['cat_id']));
						//On en profite pour mettre la chaine correspondante a la catégorie associée directement en Session
						//Pour la navigation dans cette catégorie
						$_SESSION['ecom']['cat_id_assoc'] = end($_POST['cat_id']);
						$_SESSION['ecom']['correspondance'] = $skf ->getStrCateg(end($_POST['cat_id']));	
						
						//On renvoie la chaine d'id catégorie associée pour le lien (Voir)
						$out = $skf -> getCatIdTree(end($_POST['cat_id']));
					}
					
					
				//Cas normal permettant d'envoyer l'ID de la catégorie associée
				//Ainsi qu'une chaine de type Root > Cat > sCat etc à AJAX sous forme JSON		
				}else{
					$tmp = "";
					foreach ($_POST['cat_id'] as $id) {
						$cat = $skf -> getCatbyId($id);
						
						$tmp .= $cat['cat_label']. " > ";
					}
					$arr = array('cat_id' => end($_POST['cat_id']), 
								 'cat_label' => substr($tmp, 0, strlen($tmp)-2));
					$out = json_encode($arr);
				}
			break;
			
			
			//Appellé en AJAX permet de récupérer les informations permettant d'afficher l'entete pour l'ecommercant
			//Vérification si la catégorie est associée et mise en session de la categ associée 
			case '/showentetecommercant' :
				  
				$dispatch = true ;

				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					//Vide la session
					unset($_SESSION['gr']['cat_id_assoc']);
					unset($_SESSION['gr']['correspondance']);
					unset($_SESSION['gr']['offset']);
				
					$db = new BusinessSoukeoGrossisteModel();
					
					$cat_id = $db -> isCatAssoc($_POST['id'], $_POST['category']);
				}
				else{
					//Vide la session
					unset($_SESSION['ecom']['cat_id_assoc']);
					unset($_SESSION['ecom']['correspondance']);
					unset($_SESSION['ecom']['offset']);
					//Si il y a association on a l'id sinon on obtient false
					$cat_id = $skf -> isCatAssoc ($_POST['id'], $_POST['category']);
				
				}
				
				
				if($cat_id != false && $skf -> catExists($cat_id)){
					
					if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
						//Récupération de la correspondance Avahis ou forme de chaine 
						$correspondance = $skf -> getStrCateg($cat_id);
						//Mise en session variables utiles
						$_SESSION['gr']['cat_id_assoc']= $cat_id;
						$_SESSION['gr']['correspondance']= $correspondance;
					}
					else{
						//Récupération de la correspondance Avahis ou forme de chaine 
						$correspondance = $skf -> getStrCateg($cat_id);
						//Mise en session variables utiles
						$_SESSION['ecom']['cat_id_assoc']= $cat_id;
						$_SESSION['ecom']['correspondance']= $correspondance;
					}
				} 
				//Appel de la vue
				$out = $view->showEnteteEcommercant($_POST['category'], $_POST['t'], $_POST['id'], $correspondance, $cat_id);
			break;
				
			
			//route permettant de mettre en session des listes de produits utiles et des comptes de produits	
			case '/shownavigation':
				$dispatch = true;
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					
					$dbClass = "BusinessSoukeoGrossisteModel";
					if(is_string($dbClass)) $db = new $dbClass();
					
					if(isset($db)){
						$listProdAssoc = $db -> getAllAssocProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);				
						$listProdRefused = $db -> getAllRefusedProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);	
					}
					
					$_SESSION['gr']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['gr']['listProdRefused'] = $listProdRefused ;
					
					$nbAssoc = count($_SESSION['gr']['listProdAssoc']) ;
					$nbRefused = count($_SESSION['gr']['listProdRefused']); 
					$nbSelected = count($_SESSION['gr']['selectedProds']); 
					
					$_SESSION['gr']['nbAssoc']=$nbAssoc;
					$_SESSION['gr']['nbRefused']=$nbAssoc;
					$_SESSION['gr']['nbSelected']=$nbSelected;
					
					$out = $view -> showNavigationGrossiste($nbAssoc, $nbRefused, $nbSelected);
				}
				/*
				 * cas pour l'écran gestion/produit a vérifier. doit chargé le nombre de produit supprimé le nombre de produit
				 * sélectionner
				 */ 
				elseif (isset($_SESSION['gestion']) && $_SESSION['gestion'] == "produit") {
						
					if($_SESSION['gr']['tech']=="Carshop") $dbClass = "BusinessSoukeoGrossisteModel";
					if(is_string($dbClass)) $db = new $dbClass();
					
					if(isset($db)){
						$listProdAssoc = $db -> getAllAssocProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);				
						$listProdRefused = $db -> getAllRefusedProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);	
					}
					
					$_SESSION['gr']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['gr']['listProdRefused'] = $listProdRefused ;
					
					$nbAssoc = count($_SESSION['gr']['listProdAssoc']) ;
					$nbRefused = count($_SESSION['gr']['listProdRefused']); 
					$nbSelected = count($_SESSION['gr']['selectedProds']); 
					
					$_SESSION['gr']['nbAssoc']=$nbAssoc;
					$_SESSION['gr']['nbRefused']=$nbAssoc;
					$_SESSION['gr']['nbSelected']=$nbSelected;
					
					$out = $view -> showNavigationAvahis(78, 78, $nbSelected);	
					//$out = $view -> showNavigationAvahis($nbAssoc, $nbRefused, $nbSelected);	
					
					
				}
				else{
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					
					if(is_string($dbClass)) $db = new $dbClass();
					
					if(isset($db)){
						$listProdAssoc = $db -> getAllAssocProds($_SESSION['ecom']['vendor'], $_SESSION['ecom']['cat']);				
						$listProdRefused = $db -> getAllRefusedProds($_SESSION['ecom']['vendor'], $_SESSION['ecom']['cat']);	
					}
					
					$_SESSION['ecom']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['ecom']['listProdRefused'] = $listProdRefused ;
					
					$nbAssoc = count($_SESSION['ecom']['listProdAssoc']) ;
					$nbRefused = count($_SESSION['ecom']['listProdRefused']); 
					$nbSelected = count($_SESSION['ecom']['selectedProds']); 
					
					$_SESSION['ecom']['nbAssoc']=$nbAssoc;
					$_SESSION['ecom']['nbRefused']=$nbAssoc;
					$_SESSION['ecom']['nbSelected']=$nbSelected;
					
					//var_dump($_SESSION['gestion']);
					$out = $view -> showNavigation($nbAssoc, $nbRefused, $nbSelected);					
				}

			break;	
			
			//PErmet de mettre à jour le compteur de produit dans l'arbre de catégories en fonction du 
			//Context de navigation dans lequel on se trouve
			case '/countprodcontext':
				$dispatch = true;
				
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					
					 
					$dbClass = "BusinessSoukeoGrossisteModel";
					$vendor = 	$_SESSION['gr']['vendor'] ;
					
				}else{
					
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					$vendor = 	$_SESSION['ecom']['vendor'] ;	
				}
				
				$db = new $dbClass();
				
				$count = $db -> countAllProductInCat($_POST['cat_id'], $vendor, $_POST['context']);
				$out = $count;
			break;
			
			
			//Récupère les données pour afficher la liste des produits de l'ECOMMERCANT
			//Prie en compte de la vue utiliée Grid ou List
			case '/showprodlist' :
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true ;
				
				//GROSSISTES
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){

					 
					$dbClass = "BusinessSoukeoGrossisteModel";
					$vendor  = $_SESSION['gr']['vendor'] ;
					$currCat = $_SESSION['gr']['cat'];
					//Si on a reçu en POST en vue (les autres fois, changement) on la met en session
					isset($_POST['view'])? $_SESSION['gr']['view'] = $_POST['view'] : $_SESSION['gr']['view'] = $_SESSION['gr']['view'];
					$viewType = $_SESSION['gr']['view'];
				
				//ECOMMERCANTS	
				}else{
					
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					$vendor  = $_SESSION['ecom']['vendor'] ;
					$currCat = $_SESSION['ecom']['cat'];
					//Si on a reçu en POST en vue (les autres fois, changement) on la met en session
					isset($_POST['view'])? $_SESSION['ecom']['view'] = $_POST['view'] : $_SESSION['ecom']['view'] = $_SESSION['ecom']['view'];
					$viewType = $_SESSION['ecom']['view'];	
				}
				
				

				if(is_string($dbClass))$db = new $dbClass();
				
				//récupère le contexte pour savoir dans quel onglet on se trouve
				$context = $_POST['context'] ;
				
				
				//Si la catégorie est asssociée on obtient l'id 
				$id_cat_assoc = $skf -> isCatAssoc($vendor, $currCat);
				
				//Liste de tous les produits non associés pour la catégorie
				$listeProduits = $db -> getAllUnassocProducts($currCat, $vendor, "", "", "", "", $context);
				$totalProd = count($listeProduits);
				foreach ($listeProduits as &$prod) {
					if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
						if($skf -> productIsAssocGr($_SESSION['gr']['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}else{
						if($skf -> productIsAssoc($_SESSION['ecom']['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}
				}
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$listProdAssoc = $db -> getAllAssocProds($vendor, $currCat);				
					$listProdRefused = $db -> getAllRefusedProds($vendor, $currCat);
					
					
					$_SESSION['gr']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['gr']['listProdRefused'] = $listProdRefused ;
					
					//PAGINATION
					$_SESSION['gr']["currPage"] = 1;
					$_SESSION['gr']['offset'] = 0;
					$limit = $_SESSION['gr']['offset'];
					//Mise en session de la liste produits ecommercant
					$_SESSION['gr']['listProdEcom'] = $listeProduits;
				}
				else{
					$listProdAssoc = $db -> getAllAssocProds($vendor, $currCat);				
					$listProdRefused = $db -> getAllRefusedProds($vendor, $currCat);
					
					
					$_SESSION['ecom']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['ecom']['listProdRefused'] = $listProdRefused ;
					
					//PAGINATION
					$_SESSION['ecom']["currPage"] = 1;
					$_SESSION['ecom']['offset'] = 0;
					$limit = $_SESSION['ecom']['offset'];
					//Mise en session de la liste produits ecommercant
					$_SESSION['ecom']['listProdEcom'] = $listeProduits;
				}
				
				
				if(count($listeProduits)<1 ){
					$out ="";
				}else{
					$out = $view->showProdList(array_slice($listeProduits,$limit, $nbProdsByPage), $viewType, 'ecom', $id_cat_assoc, $totalProd);	
				}
				
			break;
			
			//Gère le clique sur next page pour pagination et fait la différence coté avahis ou ecom
			case '/nextpageprod':
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}   
				
				$nbProd = $_POST['nbprod'];
				if( ($_SESSION[$typev][$offset] + $nbProdsByPage) > $nbProd){
					 $_SESSION[$typev][$offset] = $nbProd - $nbProd % $nbProdsByPage ;	
				}
				elseif(($_SESSION[$typev][$offset] + $nbProdsByPage) == $nbProd){
					$_SESSION[$typev][$offset] = $nbProd - $nbProdsByPage ;
				}
				else{
					$_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] + $nbProdsByPage ;

				}
				
				$nbPages = (int)($nbProd / $nbProdsByPage) ;
				if(($nbProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ;
				if(($nbProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;  
				
				if($_SESSION[$typev][$currPage]<$nbPages){
					$_SESSION[$typev][$currPage] = $_SESSION[$typev][$currPage] +1;
				}
			break;
			
			//Gère le clique sur prev page pour pagination et fait la différence coté avahis ou ecom
			case '/prevpageprod':
			
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}   
				
				$_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] - $nbProdsByPage;
				$_SESSION[$typev][$currPage] = $_SESSION[$typev][$currPage] -1 ;
				
				if($_SESSION[$typev][$offset] < 0){
					$_SESSION[$typev][$offset]   = 0;
				} 
				if($_SESSION[$typev][$currPage] < 1){
					$_SESSION[$typev][$currPage] = 1;
				}
					
			break;
			
			case '/firstpageprod':
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}
				$_SESSION[$typev][$offset] = 0;
				$_SESSION[$typev][$currPage] = 1; 
			break;
			
			case '/lastpageprod':
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}
				$nbProd = $_POST['nbprod'];
				
				$_SESSION[$typev][$offset] = $nbProd - $nbProd % $nbProdsByPage;	
				//Si modulo de 9 = 0 il n'y aura aucun produits sur la dernière page;
				if(($nbProd % $nbProdsByPage) == 0) $_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] - $nbProdsByPage;
				
				$nbPages = (int)($nbProd / $nbProdsByPage) ;
				if(($nbProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ;
				if(($nbProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;  
				
				$_SESSION[$typev][$currPage] = $nbPages;
				
			break;
			
			//Gère l'affichage de la liste produit après clics sur pagination fait la différence entre coté avahis et ecom
			case '/loadessionprodlist':
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				//CHOIX DU TYPE coté ecommercant ou coté Avahis
				if(isset($_GET['type']) && $_GET['type'] == "ecom"){
					$offset="offset";
					$listeProd = $_SESSION[$typev]['listProdEcom'];
					$vue = $_SESSION[$typev]['view'];
					$type = "ecom";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av"){
					$offset="offsetav";
					$listeProd =$_SESSION[$typev]['listProdAv'];
					$vue = $_SESSION[$typev]['viewAv'];
					$type = "av";
					$currPage = "currPageAv";
				}   
				
				$dispatch=true;
				$nbProd = count($listeProd);
				
				//GESTION PAGINATION
				isset($_SESSION[$typev][$offset]) ? $_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] : $_SESSION[$typev][$offset] = 0;
				isset($_SESSION[$typev][$currPage]) ? $_SESSION[$typev][$currPage] =$_SESSION[$typev][$currPage] : $_SESSION[$typev][$currPage] = 1;
				
				if($_SESSION[$typev][$offset] > $nbProd) {
					$limit = $nbProd - $nbProdsByPage;	
					$_SESSION[$typev][$offset] = $nbProd - $nbProdsByPage;
				}
				else {
					$limit = $_SESSION[$typev][$offset];
				}

				$out = $view->showProdList(array_slice($listeProd,$limit, $nbProdsByPage), $vue, $type, false, $nbProd, $_SESSION[$typev][$currPage]);
			break;
					
			//Récupération des informations pour l'affichage des filtres dynamiquess d'attributs
			//Des produits de l'ecommercant
			case '/showdynafilter' :
				
				$dispatch = true;
				//GROSSISTES
				if(isset($_GET['type']) && $_GET['type'] == "grossiste"){
					
					if($_POST['t'] == "Carshop"){
						$dbClass = "BusinessSoukeoCarshopModel";	
					}else{
						$dbClass = "BusinessSoukeoGrossisteModel";
					}
					
					$db = new $dbClass();
					
					$coupleAttrVal = $db -> getDistinctAttr($_POST['category']);
					//Mise en session du tableau
					$_SESSION['gr']['attrVal'] = $coupleAttrVal;
				
					$out = $view -> showDynaFilter($coupleAttrVal, "Grossiste");
				}
				//ECOMMERCANTS
				else{
					
					$dbClass = "BusinessSoukeo".$_POST['t']."Model";
					$db = new $dbClass();
					
					//Montre les 3 attributs les plus utilisés
					$listeAttr = $db -> getBestAttrFromVendorCateg($_POST['category'],$_POST['id']);
					
					$coupleAttrVal  = array();
					//Création d'un tableau de couple attribut => liste de valeurs
					foreach($listeAttr as $attr){
						
						$listVal = $db -> getAllDisctValueForAttrFromVendorCateg($_POST['category'],$_POST['id'], $attr['ID_ATTR']);
						$coupleAttrVal[] = array('attr' => $attr, 'val' => $listVal); 
					}
					//Mise en session du tableau
					$_SESSION['ecom']['attrVal'] = $coupleAttrVal;
				
					$out = $view -> showDynaFilter($coupleAttrVal);	
				}

			break;
			
			//Récupération des informations pour l'affichage des filtres dynamiquess d'attributs
			//Des produits Avahis
			case '/showdynafilterav':
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				$dispatch = true;
				
				//VERSION EN COMMENTAIRE EST LA VERSION Où on voit même les attributs pour les sur-catégorie ne possédant pas directement
				//de produits
				//$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				//$cat =  substr($cat, 0,-2);
				$listeAttr = $skf -> getBestAttrFromCateg($_POST['category']);
				//$listeAttr = $skf -> getBestAttrFromCateg($cat);
				
				$coupleAttrVal  = array();
				//Création d'un tableau de couple attribut => liste de valeurs
				foreach($listeAttr as $attr){
					$listVal = $skf -> getAllDisctValueForAttrFromCateg($_POST['category'], $attr['ID_ATTR']);
					//$listVal = $skf -> getAllDisctValueForAttrFromCateg($cat, $attr['ID_ATTR']);
					
					$coupleAttrVal[] = array('attr' => $attr, 'val' => $listVal); 
				}
				//Mise en session du tableau
				$_SESSION[$typev]['attrValAv'] = $coupleAttrVal;
				//var_dump($coupleAttrVal);
				$out = $view -> showDynaFilter($coupleAttrVal);
				
			break;
			
			
			//Récupération des informations nécessaires à l'affichage des filtres de recherche par 
			//marque des produits ECOMMERCANT
			case '/showbrandfilter' :
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				
				$dbClass = "BusinessSoukeo".$_POST['t']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//Récupération de toute les marques uniques pour la catégorie
				$brands = $db -> getAllDistinctManufFromCatVendor($_POST['category'],$_POST['id']);
				$_SESSION[$typev]['brands']= $brands;
				
				$out = $view -> showBrandFilter($brands, "ecom");
				
			break;
			
			//Récupération des informations nécessaires à l'affichage des filtres de recherche par 
			//marque des produits AVAHIS
			case '/showbrandfilterav' :
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				
				//Récupération de toute les marques uniques pour la catégorie
				$brands = $skf -> getAllDistinctBrandFromCat($_POST['category']);
				$_SESSION[$typev]['brandsAv']= $brands;
				
				$out = "";
				echo $view -> showBrandFilter($brands, "av");
			break;
			
			
			//Appel de la vue pour afficher l'entete coté droit Avahis
			case '/showenteteavahis' :
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				} 
				
				unset($_SESSION[$typev]['offsetAv']);
				$dispatch = true ;
				$_SESSION[$typev]['catAv']= $_POST['category'] ;
				$catIdTree = $skf -> getCatIdTree($_POST['category']);
				$view -> assign('catIdTree', $catIdTree);
				$out = $view->showEnteteAvahis($_POST['category']);
				
			break;
			
			//Appel de la vue pour afficher l'entete Avahis GP
			case '/showenteteavahisGP' :
				
				$typev = "ecom";
				unset($_SESSION[$typev]['offsetAv']);
				$dispatch = true ;
				$_SESSION[$typev]['catAv']= $_POST['category'] ;
				$catIdTree = $skf -> getCatIdTree($_POST['category']);
				$skfView -> assign('catIdTree', $catIdTree);
				$out = $skfView->showEnteteAvahisGP($_POST['category']);
				
			break;
			
			
			//Liste des produits pour avahis
			case '/showprodlistav' :
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true ;
				//Le $_POST['view'] est récupéré depui l'appel AJAX et mis en session
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//Récupération d'une liste de produit avahis avec champs renommés en Alias pour utilisation dans la vue
				//$listeProduits = $skf -> getAllProdByCatIdAlias($_SESSION[$typev]['catAv'], "", "", "", "");
				$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				$cat =  substr($cat, 0,-2);
				if($_SESSION['ecom']['context'] =='todo'&&$_SESSION['ecom']['context']==''){
					$listeProduits = $skf -> getAllProdByCatIdAlias($cat);
				}
				elseif ($_SESSION['ecom']['context'] =='deleted'){
					$_SESSION['ecom']['catAv']== null?	
						$listeProduits = $skf -> getAllProdByCatIdAlias(null,null,null,null,null,null,null,'1'):
						$listeProduits = $skf -> getAllProdByCatIdAlias($cat,null,null,null,null,null,null,'1')	;
					
				}
				else{
					$listeProduits = $skf -> getAllProdByCatIdAlias($cat);
				} 
				$nbProd = count($listeProduits);
				
				//Pagination RAZ
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				
				//Mise en session de la lite produit pour actualiation 
				$_SESSION[$typev]['listProdAv']=$listeProduits;
				
				//Si écran Gestion/produit
				if(isset($_SESSION['gestion'])&&$_SESSION['gestion']== 'produit'){
					$listProdDeleted= $skf -> getAllDeletedProds();
					//var_dump($listProdDeleted);
					
					$_SESSION['ecom']['listProdDeleted'] = $listProdDeleted;
				}	
		
				
				$out = $view->showProdList(array_slice($listeProduits,$limit, $nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);	
				
			break;
			
			//Liste des produits pour avahis GP
			case '/showprodlistavGP' :
				
				$typev = "ecom";
				
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true ;
				//Le $_POST['view'] est récupéré depui l'appel AJAX et mis en session
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//Récupération d'une liste de produit avahis avec champs renommés en Alias pour utilisation dans la vue
				//$listeProduits = $skf -> getAllProdByCatIdAlias($_SESSION[$typev]['catAv'], "", "", "", "");
				$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				$cat =  substr($cat, 0,-2);
				$listeProduits = $skf -> getAllProdByCatIdAlias($cat);
				
				$nbProd = count($listeProduits);
				
				//Pagination RAZ
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				
				//Mise en session de la lite produit pour actualiation 
				$_SESSION[$typev]['listProdAv']=$listeProduits;
				
				$listProdDeleted= $skf -> getAllDeletedProds();
				//var_dump($listProdDeleted);
				
				$_SESSION['ecom']['listProdDeleted'] = $listProdDeleted;
				
		
				
				$out = $skfView->showProdList(array_slice($listeProduits,$limit, $nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);	
				
			break;
			
			
			//Récupération des informations et d'une liste produits Avahis filtrée en fonction de ce que l'utilisateur a entré
			case '/showprodlistfilteredav' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Récupération de la vue grid ou lit
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//GESTION DES FILTRES
				$brand = $_POST['brand'];
				$priceMin = (float)$_POST['pmin'];
				$priceMax = (float)$_POST['pmax'];
				$search = $_POST['search'];
				$order = $_POST['order'];
				$allProd = $_POST['allProd'];
				$idVendor = $_POST['searchVendor'];
				//var_dump($allProd);
				//Recuperation de la liste produits avec filtres de recherche, la catégorie est en session
				$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				$cat =  substr($cat, 0,-2);
				
				if($_SESSION['gestion'] =='produit'){
					$allProd ?	$cat = null : $cat =$cat ;
					if($_SESSION['ecom']['context'] =='deleted'){
						$deleted = 1 ;
					}
				}
				
				$listeProduits = $skf -> getAllProdByCatIdAlias($cat, $brand, $priceMin, $priceMax, $search, $order, $idVendor, $deleted);
				//Attributs reçus en ajax, tableau 2 dimensions avec les 3 attributs 
				//Fonction AJAX peut etre rendu dynamique pour X attributs
				$attr = $_POST['attr'];
				
				//GESTION DES PRODUITS FILTRES PAR ATTRIBUTS DYNAMIQUES
				$listeProdFiltre = array();
				//Pour chaque produit on vérifie s'il correspond aux critères de recherche
				
				if(!empty($attr)){
					foreach ($listeProduits as $prod) {
					
						//Création d'un tableau de vérification il faut que toutes les valeurs soient vraies
						$verif = array();
						
						foreach ($attr as $a) {
							
							//Si on a bien entré ce critère et qu'il correspond
							if ($a['val'] != "--"){
								
								if ($skf -> verifAttrForProductSkf($prod['ID_PRODUCT'], $a['id'], $a['val'])){
									$verif[] = true;
								}else{
									$verif[] = false;
								}
							}		
						}
						
						if(!in_array(false, $verif)){
							//ON remplit le tableau des produits corrects si tout correspond
							$listeProdFiltre[] = $prod;
						}
					}
				}else{
					$listeProdFiltre = $listeProduits;
				}
				
				$_SESSION[$typev]['listProdAv']=$listeProdFiltre;
				
				//PAGINATION
				$nbProd = count($listeProdFiltre);
				//Variables session pour pagination limite de 9 produits par page
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				
				//Affichage 
				$out = $view->showProdList(array_slice($listeProdFiltre,$limit,$nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);
				
			break;	
			
			
			//Récupération d'une liste de produit Ecommercant filtré en fonction des filtres de recherche entrés
			case '/showprodlistfiltered' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				isset($_POST['view'])? $_SESSION[$typev]['view'] = $_POST['view'] : $_SESSION[$typev]['view'] = $_SESSION[$typev]['view'];
				
				//GESTION DES FILTRES
				$brand = $_POST['brand'];
				$priceMin = (float)$_POST['pmin'];
				$priceMax = (float)$_POST['pmax'];
				$search = $_POST['search'];
				$order = $_POST['order'];
				
				//Choix du bon model par rapport à la tech en session
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				
				///////////////////////////////////////// GROSSISTE /////////////////////////////////////////////////
				if($typev == "gr"){
					
					$dbClass = "BusinessSoukeoGrossisteModel";
					$db = new $dbClass();
					
					$attr = $_POST['attr'];
					$listeProdFiltre = $db -> getAllUnassocProducts($_SESSION[$typev]['cat'], $_SESSION[$typev]['vendor'], $brand, $priceMin, $priceMax, $search, $_POST['context'], $order, $attr);
					
					foreach ($listeProdFiltre as &$prod) {
						if($skf -> productIsAssocGr($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}
				}
				///////////////////////////////////////// ECOMMERCANT ////////////////////////////////////////////////	
				else{
					
					$db = new $dbClass();
					
					//Récupération de la lite produits avec filtres de recherche, la catégorie est en session
					$listeProduits = $db -> getAllUnassocProducts($_SESSION[$typev]['cat'], $_SESSION[$typev]['vendor'], $brand, $priceMin, $priceMax, $search, $_POST['context'], $order);
					
					//Attributs reçus en ajax, tableau 2 dimensions avec les 3 attributs 
					//Fonction AJAX peut etre rendu dynamique pour X attributs
					$attr = $_POST['attr'];
					
					//GESTION DES PRODUITS FILTRES PAR ATTRIBUTS DYNAMIQUES
					$listeProdFiltre = array();
					
					if(empty($attr)){
						$listeProdFiltre = $listeProduits;
					}else{
						//Pour chaque produit on vérifie s'il correspond aux critères de recherche
						foreach ($listeProduits as $prod) {
						
							//Création d'un tableau de vérification il faut que toutes les valeurs soient vraies
							$verif = array();
							
							foreach ($attr as $a) {
								
								//Si on a bien entré ce critère et qu'il correspond
								if ($a['val'] != "--"){
									
									if ($db ->verifAttrForProduct($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'], $a['id'], $a['val'])){
										$verif[] = true;
									}else{
										$verif[] = false;
									}
								}		
							}
							
							if(!in_array(false, $verif)){
								//ON remplit le tableau des produits corrects si tout correspond
								$listeProdFiltre[] = $prod;
							}
						}	
					}
	
					foreach ($listeProdFiltre as &$prod) {

						if($skf -> productIsAssoc($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}						
					}					
					
				}

				/////////////////////////////////////////////// GENERIQUE //////////////////////////////////////////////////
				
				$listProdAssoc = $db -> getAllAssocProds($_SESSION[$typev]['vendor'], $_SESSION[$typev]['cat'], $brand, $priceMin, $priceMax, $search);				
				$listProdRefused = $db -> getAllRefusedProds($_SESSION[$typev]['vendor'], $_SESSION[$typev]['cat'], $brand, $priceMin, $priceMax, $search);
				$_SESSION[$typev]['listProdAssoc'] = $listProdAssoc ;
				$_SESSION[$typev]['listProdRefused'] = $listProdRefused ;
				
				
				//Mise en session de la liste produits ecommercant
				$_SESSION[$typev]['listProdEcom'] = $listeProdFiltre;
				
				$totalProd = count($listeProdFiltre);
				
				//PAGINATION
				$_SESSION[$typev]["currPage"] = 1;
				$_SESSION[$typev]['offset'] = 0;
				$limit = $_SESSION[$typev]['offset'];
				
				$_SESSION[$typev]['listProdEcom']=$listeProdFiltre;
				
				$out = $view->showProdList(array_slice($listeProdFiltre,$limit,$nbProdsByPage), $_SESSION[$typev]['view'], 'ecom', "", $totalProd);
				
			break;
			
			
			//Récupère les informations permettant l'affichage uniquement de la liste des produits selectionnés par l'utilisateur
			case '/showselectedprodlist' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = $_SESSION['gestion']=="produit"?SystemParams::getParam('system*nb_prods_page_avahis'):SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				isset($_POST['view'])? $_SESSION[$typev]['view'] = $_POST['view'] : $_SESSION[$typev]['view'] = $_SESSION[$typev]['view'];
				isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
				
				//Choix model en fonction de la tech ecom
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				$selectProdList = array();
				
				//On possède en session la liste des ID des produits que l'utilisateur a selection
				//A partir de tous ces id on va créer un tableau de produits
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					$prod = $db -> getProduct($_SESSION[$typev]['vendor'], $id);
					$prod = $prod[0];
					
					if($typev == "gr"){
						if($skf -> productIsAssocGr($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}elseif($typev == "ecom"){
						if($skf -> productIsAssoc($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}
					
					
					$selectProdList[] = $prod;
				}
				$totalProd = count($selectProdList);
				//PAGINATION
				$_SESSION[$typev]["currPage"] = 1;
				$_SESSION[$typev]['offset'] = 0;
				$limit = $_SESSION[$typev]['offset'];
				
				$_SESSION[$typev]['listProdEcom']=$selectProdList;
				
				$out = $view->showProdList(array_slice($selectProdList,$limit,$nbProdsByPage), $_SESSION[$typev]['view'], 'ecom', "", $totalProd);
				
			break;
			
			//Récupère les informations permettant l'affichage uniquement de la liste des produits selectionnés par l'utilisateur pour l'écran gestion/produit
			case '/showselectedprodlistAvGP' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				//echo"test de la page de sélection ";
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis');
				$dispatch = true;
				$typev = "ecom";
				
				isset($_POST['viewAv'])? $_SESSION[$typev]['viewAv'] = $_POST['viewAv'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
				
				
				$dbClass = "BusinessSoukeoModel";
				$db = new $dbClass();
				
				$selectProdList = array();
				//var_dump($_SESSION[$typev]['selectedProds']);
				//On possède en session la liste des ID des produits que l'utilisateur a selection
				//A partir de tous ces id on va créer un tableau de produits
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					$prod = $db -> getProductAlias($id);
					
					//$prod = $prod[0];
														
					$selectProdList[] = $prod;
				}
				//var_dump($_SESSION[$typev]);
				$totalProd = count($selectProdList);
				//PAGINATION
				$_SESSION[$typev]["currPage"] = 1;
				$_SESSION[$typev]['offset'] = 0;
				$limit = $_SESSION[$typev]['offset'];
				
				$_SESSION[$typev]['listProdAv'] = $selectProdList;
				//var_dump($selectProdList);
				
				$out = $view->showProdList(array_slice($selectProdList,$limit,$nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $totalProd);
				
			break;
			
			//Permet de traiter l'ajout d'un produit à la liste de produits selectionnés en vérifiant les doublons
			case '/selectproduct':
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Vérification de la présence de variable en session ou première fois
				isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
				 
				$dispatch = true;
				//Id du produit qu'on vient de recevoir à traiter
				$idProduct = $_POST['id'];
				//Vérifications si c'esst un doublon ou pas
				if(!in_array($idProduct, $_SESSION[$typev]['selectedProds'])){
					//Ajout en session de l'id du produit
					$_SESSION[$typev]['selectedProds'][]= $idProduct;	
				}
				//Renvoi pour affichage du nombre de produits selectionnés
				$out = count($_SESSION[$typev]['selectedProds']);
			break;
			
			
			//permet de déselectionner un produit de notre lite de selection
			case '/deselectproduct': 
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//ID du produit à deselectionner
				$idProduct = $_POST['id'];
				
				//Si l'id de ce produit existe bien on l'unset
				if(($key = array_search($idProduct, $_SESSION[$typev]['selectedProds'])) !== false) {
    				unset($_SESSION[$typev]['selectedProds'][$key]);
				}
				//Renvoi pour affichage du nombre de produits selectionnés
				$out = count($_SESSION[$typev]['selectedProds']);
			break;
			
			case '/selectallproduct':
				$dispatch = true;	
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//DESELECT ALL
				if(isset($_GET['a']) && $_GET['a'] == "undo"){
					unset($_SESSION[$typev]['selectedProds']);
					$out = count($_SESSION[$typev]['selectedProds'] = array());
				//SELECT ALL	
				}else{
					//Vérification de la présence de variable en session ou première fois
					isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
					
					if(!empty($_SESSION[$typev]['listProdEcom'])){
						//Vérifications si c'esst un doublon ou pas
						foreach ($_SESSION[$typev]['listProdEcom'] as $prod) {
							if(!in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
								//Ajout en session de l'id du produit
								$_SESSION[$typev]['selectedProds'][]= $prod['ID_PRODUCT'];	
							}	
						}
						
						//Renvoi pour affichage du nombre de produits selectionnés
						$out = count($_SESSION[$typev]['selectedProds']);
					}else{
						$out = 0;
					}	
				}
				
				
			break;
				case '/selectallproductAv':
				$dispatch = true;	
				//GROSSISTE
				$typev = "ecom";
				
				//DESELECT ALL
				if(isset($_GET['a']) && $_GET['a'] == "undo"){
					unset($_SESSION[$typev]['selectedProds']);
					$out = count($_SESSION[$typev]['selectedProds'] = array());
				//SELECT ALL	
				}else{
					//Vérification de la présence de variable en session ou première fois
					isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
					
					if(!empty($_SESSION[$typev]['listProdAv'])){
						//Vérifications si c'esst un doublon ou pas
						foreach ($_SESSION[$typev]['listProdAv'] as $prod) {
							if(!in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
								//Ajout en session de l'id du produit
								$_SESSION[$typev]['selectedProds'][]= $prod['ID_PRODUCT'];	
							}	
						}
						
						//Renvoi pour affichage du nombre de produits selectionnés
						$out = count($_SESSION[$typev]['selectedProds']);
					}else{
						$out = 0;
					}	
				}
				
				
			break;
			
			case '/selectallventesav':
				$dispatch = true;
				
				if(isset($_GET['a']) && $_GET['a'] == "undo" ){
					unset($_SESSION['selectedProdsGr']);
				}else{
					$grid = Grid::getObj('venteavahis');
					$rs = dbQueryall($grid->_getSqlQuery());
					foreach ($rs as $row) {
						if(!in_array($row['ID_PRODUCT'], $_SESSION['selectedProdsGr'])){
							//Ajout en session de l'id du produit
							$_SESSION['selectedProdsGr'][]= $row['ID_PRODUCT'];	
						}
					}
				}
				
				//unset($_SESSION['selectedProdsGr']);
			break;
			
			case '/selectoneventesav':
				$dispatch = true;
				
				if(isset($_GET['a']) && $_GET['a'] == "undo"){
					
					if(($key = array_search($_POST['id'], $_SESSION['selectedProdsGr'])) !== false) {
    					unset($_SESSION['selectedProdsGr'][$key]);
						//$out = $_POST['id']. " supprimé de la sélection !";
					}
				}
				else{
					if(!in_array($_POST['id'], $_SESSION['selectedProdsGr'])){
						//Ajout en session de l'id du produit
						$_SESSION['selectedProdsGr'][]= $_POST['id'];
						//$out = $_POST['id']." ajouté en sélection !";	
					}	
				}

			break;
			
			case '/savemodifventeav' :
				$dispatch = true;
				
				$db = new BusinessSoukeoGrossisteModel();
				
				$product = $db -> getProduct("", $_POST['id']);
				$product = $product[0];
				
				
				//CALCUL
				
				$prixAchatHT = $product['PRICE_PRODUCT'];				
				//pas de hauteur largeur longueur
				$poidsVolum = $product['VOLUMETRIC_WEIGHT'];
							
				$prixKilo = $prixAchatHT / $poidsVolum;			
				$livraisonIntra = 7.4865;				
				$aerien = 5.9 * $poidsVolum;
				
				$octroiMer = ( (float)$_POST['percentOctrmer'] / 100 ) * ($prixAchatHT + $aerien);
				
				var_dump($octroiMer);
				
				$prixRevient = $prixAchatHT 
							 + SystemParams::getParam('grossiste*forfaitlivreu')
							 + SystemParams::getParam('grossiste*forfaitstockparis')
							 + (float)SystemParams::getParam('grossiste*forfaitstockreunion')
							 + $livraisonIntra 
							 + $aerien
							 + $octroiMer;
							 
				$margeNette = $prixRevient * ( (float)$_POST['percentmarge'] / 100 ) ; // 25% marge nette à paramétrer, ici valeur par défaut
				
				$TVA = ( $prixRevient + $margeNette ) * ((float)$_POST['percentTVA'] / 100); //TVA 8,50 % à paramétrer plus tard, ici valeur par défaut
				
				
				$prixVenteLivre = $prixRevient + $margeNette + $TVA ;
				
				$coeff = $prixVenteLivre / $prixAchatHT ;  
				
				$product['PRIX_REVIENT'] = $prixRevient;
				$product['COEF'] = $coeff ;
				$product['DELIVERED_PRICE'] = $prixVenteLivre ;
				$product['MARGE_NET'] = $margeNette;
								
				$db -> updateValCalculProductGrossiste ($product);
				
				//MAJ SI PRODUIT EN VENTE
				if($db -> getStockPriceEcom($product['EAN'], 24)){
					$db -> addRowStockPrixProdVenteAv ($product);
				}
				
				//var_dump($_POST);
			break;
			
			case '/saveallmodifventeav':
				$dispatch = true;
				
				$db = new BusinessSoukeoGrossisteModel();
				
				$list = $_POST['prod'];

				$i = 0 ;
				
				foreach ($list as $key => $value) {
					
					//Vérification i chaine non vide
					$value['percentOctrmer'] == "" ? $value['percentOctrmer'] = 0 	: $value['percentOctrmer'] ;
					$value['percentTVA']	 == "" ? $value['percentTVA']     = 0 	: $value['percentTVA'] ;
					$value['percentmarge']   == "" ? $value['percentmarge']   = 0 	: $value['percentmarge'] ;
					
					//On récupère le produit grace à l'ID obtenu
					$product = $db -> getProduct("", $value['id']);
					$product = $product[0];
					
					//Série de calculs
					$prixAchatHT = $product['PRICE_PRODUCT'];				
					$poidsVolum = $product['VOLUMETRIC_WEIGHT'];
					$prixKilo = $prixAchatHT / $poidsVolum;			
					$livraisonIntra = 7.4865;				
					$aerien = 5.9 * $poidsVolum;					
					$octroiMer = ( (float)$value['percentOctrmer'] / 100 ) * ($prixAchatHT + $aerien);
					$prixRevient = $prixAchatHT 
								 + SystemParams::getParam('grossiste*forfaitlivreu')
								 + SystemParams::getParam('grossiste*forfaitstockparis')
								 + (float)SystemParams::getParam('grossiste*forfaitstockreunion')
								 + $livraisonIntra 
								 + $aerien
								 + $octroiMer;
								 
					$margeNette = $prixRevient * ( (float)$value['percentmarge'] / 100 ) ; 
					$TVA = ( $prixRevient + $margeNette ) * ((float)$value['percentTVA'] / 100);
					$prixVenteLivre = $prixRevient + $margeNette + $TVA ;
					$coeff = $prixVenteLivre / $prixAchatHT ;  
					
					$product['PRIX_REVIENT'] = $prixRevient;
					$product['COEF'] = $coeff ;
					$product['DELIVERED_PRICE'] = $prixVenteLivre ;
					$product['MARGE_NET'] = $margeNette;
					
					//Mise à jour dans la BDD				
					$db -> updateValCalculProductGrossiste ($product);
					
					//MAJ SI PRODUIT EN VENTE
					if($db -> getStockPriceEcom($product['EAN'], 24)){
						$db -> addRowStockPrixProdVenteAv ($product);
					}
					$i++;
				}

				$out = $i . " produits mis à jour !";
			break;
			
			case '/getallselectedventesav':
				$dispatch = true;
				if(!empty($_SESSION['selectedProdsGr']) && $_SESSION['selectedProdsGr'] != null){
					$json = json_encode($_SESSION['selectedProdsGr']) ;
					$out = $json;	
				} 
				//var_dump($json);
				
			break;
			
			case '/sellselectionventeav':
				$db = new BusinessSoukeoGrossisteModel();
				$dispatch = true;
				if(!empty($_SESSION['selectedProdsGr']) && $_SESSION['selectedProdsGr'] != null){
					
					if($_GET['a'] == "undo"){
						
						foreach ($_SESSION['selectedProdsGr'] as $id) {
							
							$rs = $db -> getProduct("", $id);
							$prodToSell = $rs[0];
							
							if($prodToSell['ACTIVE'] == 1 && $prodToSell['AJOUTE'] == 1 && $prodToSell['refus'] == 0){
								//AddRow
								$prodToSell['QUANTITY'] = 0;
								$log = $db ->addRowStockPrixProdVenteAv($prodToSell);
								/*if($log == 1)
									$add++;
								elseif ($log == 2)
									$updt++;
								elseif($log == 0)
									$err++;*/
							}
						}
						$out .= "" . count($_SESSION['selectedProdsGr']). " produit(s) sélectionné(s) ont été retirés de vos ventes !";
					}
					else{
						$add=0;
						$updt=0;
						$err = 0;
						$errStock = 0;
						
						foreach ($_SESSION['selectedProdsGr'] as $id) {
							
							$rs = $db -> getProduct("", $id);
							$prodToSell = $rs[0];
							
							if($prodToSell['ACTIVE'] == 1 && $prodToSell['AJOUTE'] == 1 && $prodToSell['refus'] == 0){
								//AddRow
								if($prodToSell['QUANTITY'] >= 1){
									$log = $db ->addRowStockPrixProdVenteAv($prodToSell);
									if($log == 1)
										$add++;
									elseif ($log == 2)
										$updt++;
									elseif($log == 0)
										$err++;
								}
								else{
									$errStock ++;
								}
							}
						}
						$out .= "Sur " . count($_SESSION['selectedProdsGr']). " produit(s) sélectionné(s) \n"
								.$add." ajouté(s) à vos ventes !\n"
								.$updt." produit(s) déjà en vente mis à jour !\n"
								.$err." erreur(s).\n"
								.$errStock." produits avec stock inférieur à 1 non vendu !\n";
					}
					
				}else{
					$out = "Vous n'avez séléctionné aucun produit";
				}	
			break;
			
			
			
			//Récupère information pour association d'un produit ecommercant
			case '/associateproduct' :
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix Model BDD
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//Id du produit à associer
				$idProduct = $_POST['id'];

				if($_GET['a']=="remove"){
					unset($_SESSION[$typev]['productToAssoc']);
					$out = "Nom du produit";
				}elseif($_GET['a']=="unassoc"){
					if($typev == "gr"){
						$skf -> removeAssocGr($_SESSION[$typev]['vendor'], $idProduct);
						$_SESSION[$typev]['nbAssoc'] --;
						$out = "Suppression de l'association effectuée.";
					}else{
						$skf -> removeAssoc($_SESSION[$typev]['vendor'], $idProduct);
						$_SESSION[$typev]['nbAssoc'] --;
						$out = "Suppression de l'association effectuée.";	
					}
					
				}else{
					
					
					//Récupère les information entière du produit à associer
					$prod = $db -> getProduct($_SESSION[$typev]['vendor'], $idProduct);
					$prod = $prod[0];
					//Mise en session du produit à associer
					$_SESSION[$typev]['productToAssoc'] = $prod ;
					
					$out = $prod['NAME_PRODUCT'];
				}
				
			break;
			
			
			//Récupère information pour association ou bien désassiocation d'un produit avahis 
			case '/associateproductav' :
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Association ANNULEE
				if($_GET['a']=="remove"){
					unset($_SESSION[$typev]['productToAssocAv']);
					$out = "Nom du produit";
					
				//Association
				}else{
					//Id du produit à associer
					$idProduct = $_POST['id'];
					
					//Récupère les information entière du produit à associer
					$prod = $skf -> getProduct($idProduct);
					
					//Mise en session du produit à associer
					$_SESSION[$typev]['productToAssocAv'] = $prod ;
					$out = $prod['name'];
				}
			break;
			
			
			
			//Va placer un flag un refus sur un produit ou bien annule un refus de produit 
			//Il y a renvoi vers Ajax de la catégorie en cours pour mettre à jour l'affichage et les
			//Arbres de navigation qui affiche le nombre de prods refusés etc 
			case '/refuseproduct':
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix BDD
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr")$dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//On a reçu en POST l'id du produit
				$idProduct = $_POST['id'];
				
				//Si on veux annuler le refus on a recu une action en GET	
				if($_GET['a']=="cancel"){
					$db -> setRefusProduct($_SESSION[$typev]['vendor'], $idProduct, 0);
					$_SESSION[$typev]['nbRefused'] -- ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];
					
				//On veux refuser le produit
				}else{	
					//Récupère les information entière du produit à associer
					$db -> setRefusProduct($_SESSION[$typev]['vendor'], $idProduct, 1);
					$_SESSION[$typev]['nbRefused'] ++ ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];;
				}
			break;
			
			
			//Permet de refuser tous les produits
			case '/refuseallselectedproduct':
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix BDD
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr")$dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//on récupère tous les ids des produits séléctionnés pour récupèrer leur infos
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					
					$db -> setRefusProduct($_SESSION[$typev]['vendor'], $id, 1);
					$_SESSION[$typev]['nbRefused'] ++ ;

				} 
				
				//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
				$out = $_SESSION[$typev]['catAv'];;
				
			break;
			
			//Va placer un flag delete sur un produit ou bien annule une supprésion de produit 
			//Arbres de navigation qui affiche le nombre de prods supprimé etc 
			case '/supproduct':
				//var_dump($idProduct);
				$dispatch = true;
				$typev = "ecom";
				
				//Choix BDD
				$dbClass = "BusinessSoukeoModel";
				$db = new $dbClass();
				
				//On a reçu en POST l'id du produit
				$idProduct = $_POST['id'];
				
				//Si on veux annuler la suppresion on a recu une action en GET	
				if($_GET['a']=="cancel"){
					$db -> setSupProduct($idProduct, 0);
					$_SESSION[$typev]['nbDeleted'] -- ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];
					
				//On veux refuser le produit
				}else{	
					//Récupère les information entière du produit à associer
					$db -> setSupProduct($idProduct, 1);
					$_SESSION[$typev]['nbDeleted'] ++ ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];;
				}
			break;	
			
			//Permet de supprimmer tous les produits sélectionnés
			case '/supallselectedproduct':
				$dispatch = true;
				$typev = "ecom";
				
				$db = new BusinessSoukeoModel();
				var_dump($_SESSION[$typev]['selectedProds']);
				//on récupère tous les ids des produits séléctionnés
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
						
					$db -> setSupProduct($id, 1);
					$_SESSION[$typev]['nbDeleted'] ++ ;

				}
				
				//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod supprimés
				$out = $_SESSION[$typev]['catAv'];;
				
			break;
			//Permet de dé-supprimmer tous les produits sélectionnés
			case '/unsupallselectedproduct':
				$dispatch = true;
				$typev = "ecom";
				
				$db = new BusinessSoukeoModel();
				var_dump($_SESSION[$typev]['selectedProds']);
				//on récupère tous les ids des produits séléctionnés 
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
						
					$db -> setSupProduct($id, 0);
					$_SESSION[$typev]['nbDeleted'] ++ ;

				}
				
				//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod désupprimé
				$out = $_SESSION[$typev]['catAv'];;
				
			break;
			//Appel au model permettant de valider l'association entre un produit avahis et un produit ecommercant
			case '/validassoc':
			
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Requete d'insertion
				$skf -> assocProduct($_POST['idEcom'], $_POST['idAv'], $_SESSION[$typev]['vendor']) ;
				$_SESSION[$typev]['nbAssoc'] ++ ;
				$out = "Les produits ".$_POST['idEcom']." et ".$_POST['idAv']. " ont bien été associés !";	
			break;
			
			//Validation des préférences mapping pour les champs statiques
			case '/validmodifcateg':
				$dispatch = true;
				
				$typev = "ecom";
				$id_produit = $_POST['caracId'][0];
				//var_dump($_POST['caracId']);
				//$longeurDeTable = strlen($_POST['caracId'][1]);
				$longeurDeTable = 0 ;
				foreach ($_POST['caracId'] as $caracts){
					$longeurDeTable ++;
					
				}
				
				//var_dump($longeurDeTable);
				if(isset($id_produit)&&$longeurDeTable>1){// si une catégorie a été coché 
					//var_dump($longeurDeTable);
					$cat_id =  array_pop ($_POST['caracId']);
					//var_dump($cat_id);					
					//var_dump($id_produit);
					$cat_id = $cat_id['catId'];					
					$id_produit = $id_produit['idProduit'] ;
					$skf -> updateCatProduct($id_produit,$cat_id) ? $mess = "Catégorie id sauvegarder" : $mess = "requéte sql a échoué" ;
					//$mess = "Catégorie id sauvegarder";
				}
				else{//sinon
					$mess = "Pas de catégorie sélectionné" ;
				} 
				echo $mess ;
				unset($_POST['caracId']);
								
				//$skf -> modifCategId($id_produit,$cat_id);
				
				//$out .= $cat_ecom."Préférences sauvegardées !";
			break;
			//Validation des préférences mapping pour les champs statiques
			case '/validallmodifcateg':
				$dispatch = true;
				
				$typev = "ecom";
				$id_produits = $_SESSION[$typev]['selectedProds'] ;
				//var_dump($_POST['caracId']);
				$cat_id =  array_pop ($_POST['caracId']);
				$cat_id = $cat_id['catId'];
								
				//var_dump($longeurDeTable);
				if(isset($_POST['caracId'])){// si une catégorie a été coché 
					foreach($id_produits as $id_produit){
					//var_dump($cat_id);					
					//var_dump($longeurDeTable);
					//var_dump($cat_id);					
					//var_dump($id_produit);
					$skf -> updateCatProduct($id_produit,$cat_id) ? $mess = "Catégorie id sauvegarder" : $mess = "requéte sql a échoué" ;
					//$mess = "Catégorie id sauvegarder";
					}
				}
				else{//sinon
					$mess = "Pas de catégorie sélectionné" ;
				} 
				echo $mess ;
				unset($_POST['caracId']);
								
				//$skf -> modifCategId($id_produit,$cat_id);
				
				//$out .= $cat_ecom."Préférences sauvegardées !";
			break;
			
			//Validation des préférences mapping pour les champs statiques
			case '/validprefcateg':
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				$cat_id = $_POST['staticFields'][0]['categories'];
				$vendor_id = $_POST['staticFields'][0]['vendor_id'];
				$cat_ecom = trim($_POST['staticFields'][0]['cat_ecom']);
				
				unset($_POST['vendor_id']);
				unset($_POST['categories']);
				unset($_POST['cat_ecom']);
				
				$skf -> mappingCategStaticField($vendor_id, $cat_id, $cat_ecom, $_POST['staticFields'][0]);
				
				//ATTRIBUTS DYNAMIQUES
				foreach($_POST['dynaFields'] as $attr){
					//Les id des attributs ajoutés par l'utilisateur sont de la forme new_labelattribut
					//Les id déja en base sont simplement des numéros
					$id_attr_av = explode("_", $attr['id_attr_av']);
					
					if($id_attr_av[0] != "new" && $id_attr_av[1] != "null"){
						
						//insert mapping attribut product
						$skf -> mappingCategDynaField($vendor_id, $cat_id, $cat_ecom, trim($attr['id_attr_av']), $attr['id_attr_ecom']);
						
					}elseif($id_attr_av[0] == "new"){
						$skf -> prepAttribut();
						//Si c'est un attribut ajouté pour le moment pas de solution car pour l'instant
						//Un attribut n'est pas directement lié à une catégorie. Un attribut est lié à un produit
						//Qui possède cet attribut et qui est dans une catégorie.
						$new_id_attr_av = $skf -> addAttribut(array('cat_id' => $cat_id, 'label_attr' => $id_attr_av[1], 'code_attr' => strtoupper($id_attr_av[1])));
						
						$skf -> mappingCategDynaField($vendor_id, $cat_id, $cat_ecom, $new_id_attr_av, $attr['id_attr_ecom']);
					}
				}
				
				$out = $cat_ecom."Préférences sauvegardées !";
			break;
			
			
			//Validation de l'ajout d'un produit ecommercant vers un produit Avahis
			case '/validaddproduct':
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				unset($_POST['staticFields']['vendor_id']);
				
				$skf -> prepInsertSfkCp();
				//l'ajout du produit nous renvoie le lastInsertId correspondant à l'ID du nouveau produit
				$id_product = $skf -> addRowCatProduct($_POST['staticFields'][0], $_SESSION[$typev]['vendor']);
				
				if(!empty($_POST['dynaFields'])){
					//ATTRIBUTS DYNAMIQUES
					foreach($_POST['dynaFields'] as $attr){
						//Les id des attributs ajoutés par l'utilisateur sont de la forme new_labelattribut
						//Les id déja en base sont simplement des numéros
						$id = explode("_", $attr['id_attr_av']);
						
						if($id[0] != "new" && $id[1] != null && $attr['value'] != "--"){
							
							//insert association attribut product
							$skf -> addAttributProduct(trim($id[1]), $id_product, $attr['value']);
							
						}
						//Nouveaux attributs à entrer
						elseif($id[0] == "new"){
							
							//insert nouvel attribut
							$skf -> prepAttribut();
							$id_attr = $skf -> addAttribut($V = array ( 'cat_id' => $_POST['staticFields'][0]['categories'],
														   				'code_attr' => strtoupper($id[1]),
														   				'label_attr' => $id[1]));
							//insert association attribut product
							$skf -> addAttributProduct($id_attr, $id_product, $attr['value']);
							$tmp .= "Création nouvel attribut ".$id[1]."\n";
						}
						//Produit qui n'avait pas d'attribut avant et l'id n'est alors pas composé d'underscore
						elseif(!isset($id[1]) && $id != ""){
							//insert association attribut product
							$skf -> addAttributProduct(trim($attr['id_attr_av']), $id_product, $attr['value']);
						}
					}
				}
				//On en profite pour associer directement le produit ecommercant avec le produit avahis qu'il vient de générer
				$skf -> assocProduct($_SESSION[$typev]['prodToAdd']['ID_PRODUCT'], $id_product, $_SESSION[$typev]['vendor']) ; 
				$out = "Produit ajouté ! ".$tmp;
			break;
			
			
			/*
			 * réponse a l'appel ajax lors du click sur ajouter tous les produits dans le contexte des grossistes 
			 * ou bien celui des e-commercants habituels. Il y a création en masse de fiche produit avahis avec création
			 * d'attributs et relation attr-produits à la volée ou bien en fonction du mapping enregistré pour la catégorie
			 * du produit concerné
			 */
			case '/addallproducttoav':
				$dispatch = true;
				
				//Cas des grossistes
				//La fonction addRowCatProduct est redéfinie dans BusinessSoukeoGrossisteModel et s'occupe de l'insertion
				//Des attributs, le mapping est statiques et commun à tous les produits du grossiste (csv statique) 
				if($_SESSION['typeVendor'] == "Grossiste"){
					$dbClass = "BusinessSoukeoGrossisteModel";
					$db = new $dbClass ();
					
					$db -> prepInsertSfkCp();
					if(!empty($_POST['cat_id_assoc'])){
						$i = 0;
						foreach($_SESSION['gr']['listProdEcom'] as $prod){
							//Si c'et bien insertion d'un nouveau produit
							if($idNewProduct = $db -> addRowCatProduct($prod, $_POST['cat_id_assoc'])){
								$i++;
								//Pour les autres grossistes insertion d'attributs
								if($prod['GROSSISTE_ID'] != '1'){
									//Récupération de tous les attributs pour ce produit
									$attrVal = $db -> getAttrValueGrossiste($prod['ID_PRODUCT']);
									//Pour chaque couple valeur/attribut
									$db -> prepAttribut();
									foreach ($attrVal as $couple) {
										$idAttrAvahis = $db -> getAttrByCatAndCode($couple['CODE_ATTR'], $_POST['cat_id_assoc']);
										//SI l'attribut n'existe pas dans avahis
										if($idAttrAvahis == false){
											//Création nouvel attribut avahis
											$idAttrAvahis = $db -> addAttribut($attr = array( "cat_id" => $_POST['cat_id_assoc'], 
																								"code_attr" => $couple['CODE_ATTR'], 
																								"label_attr" => str_replace("_", " ",strtolower($couple['CODE_ATTR']))));
										}
										//Ajout de la relation produit attribut avahis
										$db -> addAttributProduct($idAttrAvahis, $idNewProduct, $couple['VALUE']);
									}
								}				
							//Si ça ne fonctionne pas c'est que c'est un doublon déjà présent							
							}else{
								$nameCat = $skf -> getStrCateg($_POST['cat_id_assoc']);
								$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> est déjà ajouté dans <b>".substr($nameCat, 0, strlen($nameCat)-2)."</b><br/>";

							}
						}
						$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés !";
						$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
						$json = array ('message' => $message, 'id' => $skf ->getCatIdTree($_POST['cat_id_assoc']));
						$out = json_encode($json);
					}
					
				}
				//Cas des ecommercants
				else{
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					$db = new $dbClass ();				
					$i=0;
					$j=0;
					//Tous les produits de la session actuelle de produits à traiter
					foreach ($_SESSION['ecom']['listProdEcom'] as $prod) {
						
						//On va chercher la catégorie avahis associée pour la categ de CE produit
						if($cat_av_assoc = $skf -> isCatAssoc($_SESSION['ecom']['vendor'], $prod['CATEGORY'])){
							
							//On va maintenant chercher le mapping pour cette combinaison
							if($mapping = $skf -> getStaticFieldsForVendor($_SESSION['ecom']['vendor'], $cat_av_assoc, $prod['CATEGORY'])){
							
								$mappingStatic = array();
								
								foreach ($mapping as $field) {
									$mappingStatic[$field['field_avahis']] = $field['field_ecom'];
								}
								
								//Préparation requete insert fk_catalogue_product
								$skf ->prepInsertSfkCp();
								
								//INSERTION NOUVEAU PRODUIT ET RECUP DE SON ID
								if($idNewProduct = $skf -> addProductFromMappingFields($mappingStatic, $prod, $_SESSION['ecom']['vendor'])){
									$skf -> assocProduct($prod['ID_PRODUCT'], $idNewProduct, $_SESSION['ecom']['vendor']);
									$i++;
									
									////////////////////////////////////////////////////////////////////////////////////////////////
									//LES ATTRIBUTS
									//Récupération de tout les attributs ecommercant de ce produit
									if($listeAttr = $db -> getAllAttrFromProduct($prod['ID_PRODUCT'], $_SESSION['ecom']['vendor'])){
										
										foreach ($listeAttr as $attr) {
											//On va chercher si un idAttribut avahis a été mappé pour cet attribut
											if($idAttrAvahisMapped = $skf -> getMappingForAttrEcom($attr['ID_ATTR'], $_SESSION['ecom']['vendor'], $prod['CATEGORY'], $mappingStatic['categories'])){
												$skf -> addAttributProduct($idAttrAvahisMapped, $idNewProduct, $attr['VALUE']);	
												$j++;
											}
											//SINON ON AJOUTE DES ATTRIBUTS A LA VOLEE...
											else{
												
											}											
										}
										
									}
								}
								else{
									$nameCat = $skf -> getStrCateg($cat_av_assoc);
									$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> est déjà ajouté dans <b>".substr($nameCat, 0, strlen($nameCat)-2)."</b><br/>";
								}				
							}
						}
						else{
							$erreurs .= "<b>".$prod['NAME_PRODUCT']."</b> non inséré car sa catégorie <b>".$prod['CATEGORY']."</b> n'est pas associée !<br/>";
						}
					}
					$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés ! Et <b>".$j."</b> relations produits-attributs insérés !";
					$erreurs != "" ? $message .="<p><h3>Erreurs : </h3><b>Non associé : </b><br>".$erreurs : $message = $message;
					$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
					$json = array ('message' => $message, 'id' => array());
					$out = json_encode($json);	
				}	
				
			break;
			
			
			/*
			 * Permet d'ajouter dans avahis toute une selection de produits. Il y a gestion du log des produits bien inérés
			 * Gestion des doublon donc update de produits 
			 * Et gestion des erreurs de produits non ajoutés car leur catégorie n'est pas encore associée
			 * On précise lesquels de ces produits ont lancé une erreur et quelles catégories doivent être ajoutées.
			 * */ 
			case '/addallselectedproducttoav':
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				$db = new $dbClass ();				
				$i=0; //Insertion de produits
				$j=0; //Insertion de relations produit-attribut
				
				//on récupère tous les ids des produits séléctionnés pour récupèrer leur infos
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					
					$prod = $db -> getProduct($_SESSION[$typev]['vendor'], $id);
					
					//tableau des produits séléctionnés qu'on va utiliser enssuite
					$selectProdList[] = $prod[0];
				}
				
				if($typev == "ecom"){
					//Tous les produits de la session actuelle de produits à traiter
					foreach ($selectProdList as $prod) {
						
						//On va chercher la catégorie avahis associée pour la categ de CE produit
						if($cat_av_assoc = $skf -> isCatAssoc($_SESSION[$typev]['vendor'], $prod['CATEGORY'])){
	
							//On va maintenant chercher le mapping pour cette combinaison
							if($mapping = $skf -> getStaticFieldsForVendor($_SESSION[$typev]['vendor'], $cat_av_assoc, $prod['CATEGORY'])){
							
								$mappingStatic = array();
								
								foreach ($mapping as $field) {
									$mappingStatic[$field['field_avahis']] = $field['field_ecom'];
								}
								
								$skf ->prepInsertSfkCp();
								
								//INSERTION NOUVEAU PRODUIT ET RECUP DE SON ID
								if($idNewProduct = $skf -> addProductFromMappingFields($mappingStatic, $prod, $_SESSION[$typev]['vendor'])){
									$skf -> assocProduct($prod['ID_PRODUCT'], $idNewProduct, $_SESSION[$typev]['vendor']);
									$i++;
									//Si l'id de ce produit existe bien on l'unset
									if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
					    				unset($_SESSION[$typev]['selectedProds'][$key]);
									}
									////////////////////////////////////////////////////////////////////////////////////////////////
									//LES ATTRIBUTS
									//Récupération de tout les attributs ecommercant de ce produit
									if($listeAttr = $db -> getAllAttrFromProduct($prod['ID_PRODUCT'], $_SESSION[$typev]['vendor'])){
										
										foreach ($listeAttr as $attr) {
											//On va chercher si un idAttribut avahis a été mappé pour cet attribut
											if($idAttrAvahisMapped = $skf -> getMappingForAttrEcom($attr['ID_ATTR'], $_SESSION[$typev]['vendor'], $prod['CATEGORY'], $mappingStatic['categories'])){
												$skf -> addAttributProduct($idAttrAvahisMapped, $idNewProduct, $attr['VALUE']);	
												$j++;
											}
											//SINON ON AJOUTE DES ATTRIBUTS A LA VOLEE...
											else{
												
											}											
										}
									}	
								//Si ça ne fonctionne pas c'est que c'est un doublon déjà présent							
								}else{
									$nameCat = $skf -> getStrCateg($cat_av_assoc);
									$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> est déjà ajouté dans <b>".substr($nameCat, 0, strlen($nameCat)-2)."</b><br/>";
									//Si l'id de ce produit existe bien on l'unset
									if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
					    				unset($_SESSION[$typev]['selectedProds'][$key]);
									}
								}
							}
						//Produits avec catégorie non associée
						}else{
							$erreurs .= "<b>".$prod['NAME_PRODUCT']."</b> non inséré car sa catégorie <b>".$prod['CATEGORY']."</b> n'est pas associée !<br/>";
						}
					}
					$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés ! Et <b>".$j."</b> relations produits-attributs insérés !";
					$erreurs != "" ? $message .="<p><h3>Erreurs : </h3><b>Non associé : </b><br>".$erreurs : $message = $message;
					$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
					$json = array ('message' => $message, 'id' => array());
					$out = json_encode($json);
					
				}elseif($typev == "gr"){
					$db -> prepInsertSfkCp();
					foreach ($selectProdList as $prod) {
						//On va chercher la catégorie avahis associée pour la categ de CE produit
						if($cat_av_assoc = $skf -> isCatAssocGross($_SESSION[$typev]['vendor'], $prod['CATEGORY'])){
							
							//Si on a bien crée un nouveau produit on a un idNewProduct
							if($idNewProduct = $db -> addRowCatProduct($prod, $cat_av_assoc)){
								$i++;				
								//Si l'id de ce produit existe bien en selectedProds on l'unset
								if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
				    				unset($_SESSION[$typev]['selectedProds'][$key]);
								}
									
							//Si ça ne fonctionne pas c'est que c'est un doublon déjà présent							
							}else{
								$nameCat = $skf -> getStrCateg($cat_av_assoc);
								$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> est déjà ajouté dans <b>".substr($nameCat, 0, strlen($nameCat)-2)."</b><br/>";
								
								//Si l'id de ce produit existe bien on l'unset
								if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
				    				unset($_SESSION[$typev]['selectedProds'][$key]);
								}
							}
						}else{
							$erreurs .= "<b>".$prod['NAME_PRODUCT']."</b> non inséré car sa catégorie <b>".$prod['CATEGORY']."</b> n'est pas associée !<br/>";
						}
					}
					//Gestion des erreurs et du log renvoyé en JSON pour être traité par Javascript ensuite
					$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés !";
					$erreurs != "" ? $message .="<p><h3>Erreurs : </h3><b>Non associé : </b><br>".$erreurs : $message = $message;
					$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
					$json = array ('message' => $message, 'id' => array());
					$out = json_encode($json);
				}	
			break;
			
			
			//permet de récupérer le mapping des champs static pour un ecommercant et sa catégorie
			//Afin de renvoyer les données dans un JSON
			case '/getstaticfields':
				$dispatch = true;
				$result = $skf -> getStaticFieldsForVendor($_POST['id_vendor'], $_POST['cat_id'], trim($_POST['cat_ecom']));
				
				$fields = array();
				foreach ($result as $row) {
					$fields[$row['field_avahis']] = $row['field_ecom']; 
				}
				
				$json  = json_encode($fields);
				
				$out = $json;
				
			break;
			
			
			//route ajax permettant de récupérer un json contenant toutes les catégories par label et id
			case '/getlistcateg':
				$dispatch=true;
				//Récupération de tou les label et id de catégorie distinctes
				$catList = $skf -> getAllCatByLabel ($_GET['label']);
					
				if(!empty($catList)){
					
					foreach ($catList as $cat) {
						
						$data[] = array("id" => $cat['cat_id'], "text" => $cat['cat_label']);
					}		
				}
				else {
					// 0 résultat
					$data[] = array("id"=>"0","text"=>"Pas de résultat..");
				} 
				//on encode le tableau en json pour le réutiliser dans le javascript
				echo json_encode($data);
				
			break;
			
			
			//route utilisée par le plugin select2 permettant d'obtenir une chaine d'id de catégorie
			//sous forme 123-4561-12345 représentant les id des catégories et sous catégories pour une cat donnée
			case '/validselect2':
				$dispatch = true;
				
				//On transforme un id = 1234 (Cartes graphiques) par exemple en 
				//45-625-1234 représentant Informatique > Composant > Cartes graphique
				$tmp = $skf -> getCatIdTree($_POST['cat_id']);
				
				//On renvoie la chaine séparée par des tirets à Ajax
				$out = $tmp;
			break;
			
			
			
			//action dans le popup d'association permettant de charger dynamiquement en ajax
			//différentes information produit en fonction du champ selectionné 
			case '/loadfield':
				 $dispatch = true;
				
				 //GROSSISTE
				 if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				 //ECOMMERCANT	
				 else{ $typev = "ecom";}
				
				 //produit en cours d'ajout
				 $prod = $_SESSION[$typev]['prodToAdd'];
				 //champ sélectionné par l'utilisateur
				 $field = $_POST['field'];
				 //Renvoi des information pour le champ selectionné
				 $out = $prod[$field];	
			break;
			
			
			//Permet de charger les attributs Avahis et du produits ecommercant pour la bonne catégorie
			case '/findattrav':
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix de la base de données
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//ID catégorie Avahis qu'on a selectionné
				$cat_id = $_POST['cat_id'];
				
				//Produit de l'ecommercant
				$prod_id = $_POST['id_product'];
				
				//récup attributs du produit
				//Si l'id du produit est fourni on va chercher les attributs par rapport au produit
				if ($prod_id != "" && $prod_id != null){
					$attrProd = $db->getAllAttrFromProduct($prod_id, $_SESSION[$typev]['vendor']);
				}
				//Sinon on récupère tous les attributs présents dans la catégorie du vendeur
				else{
					$attrProd = $db -> getAllAttrFromVendorCateg($_SESSION[$typev]['cat'], $_SESSION[$typev]['vendor']);
				}
				//récup attributs avahis pour la catégorie
				$attrAvahis = $skf -> getAllAttrByProdFromCat($cat_id);
				
				if(empty($attrAvahis) | $attrAvahis == null){
					$attrAvahis = $skf -> getAllAttrDistFromCat($cat_id);
				}
				//Appel à businessView pour la mise en HTML des attributs ecommercant et avahis associés
				$out = $view -> showAttributsEcomAvPopup($attrProd, $attrAvahis, $_SESSION[$typev]['vendor'], $_SESSION[$typev]['cat'], $_POST['cat_id']);
				
			break;
			
			
			//Permet de changer l'onglet en cours A traiter / refusés / associés etc...
			case '/changecontext':
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//On place dans la bonne session le context choisis obtenu en POST
				$_SESSION[$typev]['context'] = $_POST['context'];
				//var_dump($_SESSION[$typev]['context']);	
			break;
			
			
			case    "/gestion/produit" :
				/*
				unset($_SESSION['ecom']['listProdDeleted']);
				unset($_SESSION['ecom']['tech']);
				unset($_SESSION['ecom']['vendor']);
				unset($_SESSION['ecom']['cat']);
				unset($_SESSION['ecom']['listProdEcom']);
				unset($_SESSION['ecom']['listProdAv']);
				unset($_SESSION['ecom']['catAv']);
				unset($_SESSION['ecom']['viewAv']);
				unset($_SESSION['ecom']['view']);
				unset($_SESSION['ecom']['attrVal']);
				unset($_SESSION['ecom']['attrValAv']);
				unset($_SESSION['ecom']['selectedProds']);
				unset($_SESSION['ecom']["currPage"]);
				unset($_SESSION['ecom']['offset']);
				unset($_SESSION['ecom']["currPageAv"]);
				unset($_SESSION['ecom']['offsetAv']);
				unset($_SESSION['ecom']['brands']);
				unset($_SESSION['ecom']['brandsAv']);
				unset($_SESSION['ecom']['listProdAssoc']);
				unset($_SESSION['ecom']['listProdRefused']);
				unset($_SESSION['ecom']['nbAssoc']);
				unset($_SESSION['ecom']['nbRefused']);
				unset($_SESSION['ecom']['nbSelected']);
				*/
				unset($_SESSION['ecom']);
				$_SESSION['ecom']['context'] ="todo"; // affiché tous
				$_SESSION['gestion'] = 'produit'; // définit un paramétre d'affichage ici écran gestion/produit
				$ACCESS_key = "gestion.produit..";
				
				$listProdDeleted = $skf -> getAllDeletedProds(); // on recherche tous les produits delete
				$_SESSION['ecom']['listProdDeleted'] = $listProdDeleted;
				
				$nbDeleted = count($_SESSION['ecom']['listProdDeleted']);  // on compte le nombre 
				$_SESSION['ecom']['nbDeleted'] = $nbDeleted;				

				$listVendorAss = $skf -> getAllVendorAssoc();
				$_SESSION['ecom']['listVendorAss'] = $listVendorAss ;
								
				$data = $skf->getSsCatByParentId();
				//--------- fonctionne pour le moment
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);
				$view -> assign("listProdDeleted", $listProdDeleted);
//  				
				//--------	En court de remise en forme spécifique a gestion de produit			  
				//$catAv = $skfView->showcat($data);
				//$skfView -> assign("catav", $catAv);
				//$skfView -> assign("listProdDeleted", $listProdDeleted);
 				
				$template = "listingAvahis";
				//$skfView -> assign("out", $out);
				/*
				$ACCESS_key = "gestion.produit..";
				
				//Actions à effectuer
				
				$view -> assign("out", $out);
				$template = "rendered";
				*/
				break;
				
			case '/validEditProd':
				$mess .= "<H1>EDITION DU PRODUIT</H1>";
				$dispatch = true;
				$attribut = array() ; // la table avec tous les attrinuts passé en POST
				$caract = $_POST['staticFields'][0]; // tous les caractéristiques récupéré en POST
				// on récupére maintenant les attributs dans $_POST['dynaFields']
				$nbrAttr = $_POST['dynaFields'][0]['nbrAttribut'] ; // le nombre d'attribut
				//var_dump ($_POST['dynaFields'][1]);
				for ($i = 1 ; $i <= $nbrAttr; $i++){ // on regarde sur tous le tableau de $_POST['dynaFields']  
					// on récupére les valeurs 
					$rows = $_POST['dynaFields'][$i] ;
					$rows =  explode("|",$rows['attribut']); // on sépare l'id de la valeur
					$attribut[$rows[0]] = $rows[1];			// on ajoute a la table attribut
					//$attribut = array_fill_keys($rows[0], $rows[1]);
				}
					
				// ATTRIBUTS STATIQUE
				$skf-> prepareStmteditProd();
				$mess .= $skf-> editRowProductAvahis($caract)."<BR>";
				//ATTRIBUTS DYNAMIQUES
				
				//var_dump($attribut);
				if ($nbrAttr>0){
					$nbdyna = 0 ;	
					foreach($attribut as $idAttr => $value){
					//echo ($idAttr." et ".$value." et ".$caract['id_produit']."<br>");	
						if($skf -> addAttributProduct($idAttr,$caract['id_produit'],$value) ){
							//echo ("sa c'est fait <br>"); 
							$nbdyna++ ;
						}
					}
				$mess .= "pour les ".$nbrAttr." attribut(s) de la fiche produit ".$nbdyna." attribut(s) on bien été traité(s) ";
				}
				//echo "<script>alert('test de fonction');</script>";
				$out = $mess;
				
			break;
	
			case    "/qualification/ecommercant" :
				$ACCESS_key = "qualification.ecommercant..";
				unset($_SESSION['gestion']);
				
				unset($_SESSION['ecom']['listProdDeleted']);
				unset($_SESSION['ecom']['tech']);
				unset($_SESSION['ecom']['vendor']);
				unset($_SESSION['ecom']['cat']);
				unset($_SESSION['ecom']['listProdEcom']);
				unset($_SESSION['ecom']['listProdAv']);
				unset($_SESSION['ecom']['catAv']);
				unset($_SESSION['ecom']['viewAv']);
				unset($_SESSION['ecom']['view']);
				unset($_SESSION['ecom']['attrVal']);
				unset($_SESSION['ecom']['attrValAv']);
				unset($_SESSION['ecom']['selectedProds']);
				unset($_SESSION['ecom']["currPage"]);
				unset($_SESSION['ecom']['offset']);
				unset($_SESSION['ecom']["currPageAv"]);
				unset($_SESSION['ecom']['offsetAv']);
				unset($_SESSION['ecom']['brands']);
				unset($_SESSION['ecom']['brandsAv']);
				unset($_SESSION['ecom']['listProdAssoc']);
				unset($_SESSION['ecom']['listProdRefused']);
				unset($_SESSION['ecom']['nbAssoc']);
				unset($_SESSION['ecom']['nbRefused']);
				unset($_SESSION['ecom']['nbSelected']);
				$_SESSION['ecom']['context'] ="todo";
				
				//Création de la grille
				include("./qualification/ecommercant/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./qualification/ecommercant/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

			break;
			
			
			/**
			 * Ecran de choix des produits grossistes que Avahis veux mettre en vente
			 */
			case    "/qualification/venteavahis" :
				$ACCESS_key = "qualification.venteavahis..";				
				$ACCESS_key ="qualification.venteavahis..";
				$security_domain = "qualification";
				$domain_categ = "venteavahis";
				$right_menu_visibility = true;
				
				$grid = new VenteAvahisGrid($query);
				// init step of SQL limit
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./qualification/venteavahis/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case    "/qualification/tracking" :
				$ACCESS_key = "qualification.tracking..";
				unset($_SESSION['gestion']);
				unset($_SESSION['ecom']);
				unset($_SESSION['gr']);
				
				
				//Création de la grille
				include("./qualification/tracking/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./qualification/tracking/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

			break;
			
			case "/deleteTracking" :
				$dispatch = true;
				ob_start();
				include ("./qualification/tracking/popup_delete/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;

			break;
			
			case "/addTrackingVendor" :
				$dispatch = true;
				ob_start();
				include ("./qualification/tracking/popup_addUrl/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;

			break;
			case "/deleteVendor" :
				$dispatch = true;
				ob_start();
				include ("./qualification/ecommercant/popup_delete/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;

			break;
			case "/actifVendor" :
				$dispatch = true;
				ob_start();
				include ("./qualification/ecommercant/popup_actif/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;

			break;
			
			case "/passifVendor" :
				$dispatch = true;
				ob_start();
				include ("./qualification/ecommercant/popup_inactif/index.php");
				$popup_inactift = ob_get_contents();
				ob_end_clean();
				$out =$popup_inactift ;
			break;
			
			case "/popup_infovend" :
				$ACCESS_key = "qualification.popup_infovend..";
				
				$dispatch = true;
				ob_start();
				include ("./qualification/ecommercant/popup_infovend/index.php");
				$popup_infovend = ob_get_contents();
				ob_end_clean();
				$out =$popup_infovend ;
				
				//$view -> assign("out", $grid -> display());
				//$template = "rendered";
			break;
				
				
			case    "/qualification/grossiste" :
				$ACCESS_key = "qualification.grossiste..";
				unset($_SESSION['gestion']);
				unset($_SESSION['gr']);
				$_SESSION['gr']['context'] ="todo";
				
				//Création de la grille
				//include("./qualification/grosssiste/index.php");
				
				$security_domain = "qualification";
				
				$domain_categ = "grossiste";
				
				$right_menu_visibility = true;
				
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$grid = new GrossisteGrid($query);
				$view -> assign("out", $grid -> display());
				$template = "rendered";

			break;
			
			
			case    "/qualification/ecommercant/listeProduits" :
				$ACCESS_key = "qualification.listeproduits..";
				include("./qualification/ecommercant/listeProduits/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

			break;
			
			//Mise en forme de la page des ecommercant avec création des treemap pour le coté ecommercants
			//et le coté Avahis
			case    "/qualification/ecommercant/assoc" :
				$_SESSION['typeVendor'] = "Ecom";
				$ACCESS_key = "qualification.assoc..";
				$template = "assoc";
				//include("./qualification/ecommercant/assoc/index.php");
				
				$class = "BusinessSoukeo".$_GET['tech']."Model";
				$_SESSION['ecom']['tech']=$_GET['tech'];
				$db = new $class();
				
				$_SESSION['ecom']['vendor'] = $_GET['id'];
				$categs = $db -> getDistinctCat($_GET['id']);
				
				$nomEcom = $db -> getValidVendor($_GET['id']);
				$view -> assign('vendName', $nomEcom['VENDOR_NOM']);
				
				//Création d'un tableau multidimension représentant l'arbre des catégories
				//On monte une chaine représentant le code permettant de faire des tableaux 
				//Imbriqués les uns dans les autres en fonction de l'arbre des catégories, la chaine est évaluée à la fin
				/*foreach ($categs as $catTree) {
					$tree = explode(">", $catTree['CATEGORY']);
				
					$tree = array_map('trim', $tree);
					
					$str ="\$arr [] = "; 
					$end = "";
					$i = 0;
					foreach ($tree as $k => $branch  ) {
						
						if ($branch == trim(end($tree))   ){
							$str .= " array('$branch' => array() )";
						}else{
							
							$str .= "array ('$branch' =>  ";
							$end .= ")";
						}
						
						$i++;
					}
					eval($str.$end.";");				
				}
				
				$data = array();
				foreach($arr as $a){
					$data = array_merge_recursive($data, $a);
				}*/
				//On établit directement un contexte de navigation qui sera ici "todo" ce qui veux dire
				//qu'on sera sur l'onglet produit "A traiter" dès le départ
				if(!isset($_SESSION['ecom']['context']))$_SESSION['ecom']['context'] = "todo";
				if(!empty($_POST['context']))$_POST['context']=$_SESSION['ecom']['context'];
				
				//Appel à la vu pour la création de l'arbre de catégories
				/*$catCli = $view->showcatCli($data, $out, "", $_POST['context'], true);*/
				$treemap = new BusinessTreemap ("ecom", $_GET['id'], $_SESSION['ecom']['context'], $_GET['tech']);
				//Renvoie des données à la vue
				$view -> assign('data', $data);
				$view -> assign('catcli', $treemap -> display());
				
				//CATEGORIES AVAHIS
				$data = $skf->getSsCatByParentId();
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);
				
				$listProdAssoc = $db -> getAllAssocProds($_SESSION['ecom']['vendor']);
				
				$listProdRefused = $db -> getAllRefusedProds($_SESSION['ecom']['vendor']);
				
				$view -> assign("listProdAssoc", $listProdAssoc);
				$view -> assign("listProdRefused", $listProdRefused);
			break;
			
			
			case '/showcatCli':
				
				$dispatch=true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				
				$class = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $class = "BusinessSoukeoGrossisteModel";
				$db = new $class();
				
				$categs = $db -> getDistinctCat($_SESSION[$typev]['vendor']);
				
				if($typev == "gr" && $_SESSION['gr']['vendor'] != '1'){
					foreach ($categs as $catTree) {
							
						if($oldLetter == "") $oldLetter = strtoupper(substr(ucfirst($catTree['CATEGORY']), 0,1));
						
						if(substr(ucfirst($catTree['CATEGORY']), 0,1) == $oldLetter){
							$data[strtoupper($oldLetter)][$catTree['CATEGORY']] = null;
						}
						else{
							$oldLetter = strtoupper(substr(ucfirst($catTree['CATEGORY']), 0,1));
							$data[strtoupper($oldLetter)][$catTree['CATEGORY']] = null;
						}
						//$data [$catTree['CATEGORY']] = null ;
					}
					
					if(!isset($_SESSION['gr']['context']))$_SESSION['gr']['context'] = "todo";
					if(!empty($_POST['context']))$_POST['context']=$_SESSION['gr']['context'];
			
					
					$treeview = $view->showcatCliGrossiste($data, $_POST['context']);
					
				}
				else{
					foreach ($categs as $catTree) {
						//On explose les chaines du type Root > Cat > ssCat
						$tree = explode(">", $catTree['CATEGORY']);
						
						//On applique un trim à tous les elements de l'arbre
						$tree = array_map('trim', $tree);
						
						//Début création de la chaine
						$str ="\$arr [] = "; 
						$end = "";
						$i = 0;
						foreach ($tree as $k => $branch  ) {
							
							if ($branch == trim(end($tree)) & $k == count($tree)-1  ){
								$branch = str_replace(array("'"), " ", $branch);
								$str .= " array('$branch' => array() )";
							}else{
								$branch = str_replace(array("'"), " ", $branch);
								$str .= "array ('$branch' =>  ";
								$end .= ")";
							}
							$old = $branch;
							$i++;
						}
	
						eval ($str.$end.";");
					}
				
					$data = array();
					foreach($arr as $a){
						$data = array_merge_recursive($data, $a);
					}
					
					if(empty($_POST['context']))$_POST['context']="all";
			
					$treeview = $view->showcatCli($data, $out, "", $_POST['context'], true);
				}

				$arr['treeview'] = $treeview; 
				if(isset($_SESSION[$typev]['cat'])){
					$arr['cat'] = $_SESSION[$typev]['cat'];
				};
				
				$out = json_encode($arr);
			break;			
			
			
			//Mise en forme de la page des grossistes avec création des treemap pour le coté grossiste
			//et le coté Avahis
			case    "/qualification/grossiste/assocdealer" :
				$_SESSION['typeVendor'] = "Grossiste";
				$ACCESS_key = "qualification.assocdealer..";
				$template = "assocGrossiste";
				//include("./qualification/ecommercant/assoc/index.php");
				if($_GET['tech']=='Carshop' | $_GET['tech'] == 'LDLC' | $_GET['tech'] == 'MAGINEA'){
					$db = new BusinessSoukeoGrossisteModel();
					$_SESSION['gr']['tech'] = $_GET['tech'];
				}
				
				$_SESSION['gr']['vendor'] = $_GET['id'];
				$categs = $db -> getDistinctCat($_GET['id']);
				
				$nomEcom = $db -> getValidGrossiste($_GET['id']);
				$view -> assign('vendName', $nomEcom['GROSSISTE_NOM']);
				
				//Création d'un tableau multidimension représentant l'arbre des catégories
				//On monte une chaine représentant le code permettant de faire des tableaux 
				//Imbriqués les uns dans les autres en fonction de l'arbre des catégories, la chaine est évaluée à la fin
				
				if($_SESSION['gr']['vendor'] !== '1'){
					foreach ($categs as $catTree) {
							
						if($oldLetter == "") $oldLetter = strtoupper(substr(ucfirst($catTree['CATEGORY']), 0,1));
						
						if(substr(ucfirst($catTree['CATEGORY']), 0,1) == $oldLetter){
							$data[strtoupper($oldLetter)][$catTree['CATEGORY']] = null;
						}
						else{
							$oldLetter = strtoupper(substr(ucfirst($catTree['CATEGORY']), 0,1));
							$data[strtoupper($oldLetter)][$catTree['CATEGORY']] = null;
						}
					}
					
					if(!isset($_SESSION['gr']['context']))$_SESSION['gr']['context'] = "todo";
					if(!empty($_POST['context']))$_POST['context']=$_SESSION['gr']['context'];
			
					
					$catCli = $view->showcatCliGrossiste($data, $_POST['context']);
					
				}
				else{
					foreach ($categs as $catTree) {
						//On explose les chaines du type Root > Cat > ssCat
						$tree = explode(">", $catTree['CATEGORY']);
						
						//On applique un trim à tous les elements de l'arbre
						$tree = array_map('trim', $tree);
						
						//Début création de la chaine
						$str ="\$arr [] = "; 
						$end = "";
						$i = 0;
						foreach ($tree as $k => $branch  ) {
							
							if ($branch == trim(end($tree)) & $k == count($tree)-1  ){
								$branch = str_replace(array("'"), " ", $branch);
								$str .= " array('$branch' => array() )";
							}else{
								$branch = str_replace(array("'"), " ", $branch);
								$str .= "array ('$branch' =>  ";
								$end .= ")";
							}
							$old = $branch;
							$i++;
						}
	
						eval ($str.$end.";");
					}
				
					$data = array();
					foreach($arr as $a){
						$data = array_merge_recursive($data, $a);
					}
					
					if(!isset($_SESSION['gr']['context']))$_SESSION['gr']['context'] = "todo";
					if(!empty($_POST['context']))$_POST['context']=$_SESSION['gr']['context'];
			
					$catCli = $view->showcatCli($data, $out, "", $_POST['context'], true);
				}

				

				$view -> assign('data', $data);
				$view -> assign('catcli', $catCli);
				
				//CATEGORIES AVAHIS
				$data = $skf->getSsCatByParentId();
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);
				
				//$listProdAssoc = $db -> getAllAssocProds($_SESSION['gr']['vendor']);
				
				//$listProdRefused = $db -> getAllRefusedProds($_SESSION['gr']['vendor']);
				
				$view -> assign("listProdAssoc", $listProdAssoc);
				$view -> assign("listProdRefused", $listProdRefused);
			break;
			
			
			case "/qualification/ecommercant/listeProduits/prod" :
				//Mise à jours des données.
				$ACCESS_key = "qualification.ficheproduit..";
				$template = "qualificationProduit";


				if(isset($_GET['t'])){
					switch ($_GET['t']) {
						case 'TH':
							$skf_ecom = new BusinessSoukeoTheliaModel();
							break;
						case 'PS':
							$skf_ecom = new BusinessSoukeoPrestashopModel();
						default:
							
							break;
					}
				}
				$data = $skf_ecom -> getAllDataProductByVendor($_GET['vendor'], $_GET['prod']);
				$vendorData = $skf_ecom -> getValidVendor($_GET['vendor']);
				
				$view -> assign('skfModelProd', $data);
				foreach ($data['prod'] as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);

				}
				
				foreach ($vendorData as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);

				}
				
			break;
				
			default :
				//   header("location:
				// http://".$_SERVER["SERVER_ADDR"]."/common/login/index.php");
				break;
			
			
			
			case '/o_admin/logxml' :
				include ("./o_admin/logxml/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/logxml/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

				break;

			case '/errForm' :
				$dispatch = true;
				$r = $data -> getLibCarac($_POST['code']);
				$out = $view -> getFormError($r, $_POST["tarif"], $_POST['code2']);
				break;
			case '/genfile' :
				$dispatch = true;
				$exXml = new BusinessExportXML( array( array("ID" => $_POST["siteid"])));
				$exXml -> exportToTmp();
				$out = '/opt/mpf/export/tmp/';
				break;

			case '/save' :
				$dispatch = true;
				$out = $data -> savedataValid($_POST["dataForm"], $_POST["siteid"]);
				$exXml = new BusinessExportXML( array( array("ID" => $_POST["siteid"])));
				$out2 = $exXml -> exportToSite();
				$out .= _("Données Validées et exportées avec succès") . "<br/>" . $out2;

				break;

			case "/o_admin/terrain" :
				//Mise à jours des données.
				$ACCESS_key = "admin.amaj..";
				$template = "gestMaj";

				$view -> assign('admin', TRUE);

				$var = $data -> getDataMajGest($_GET['id']);
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);

				}

				$var2 = $data -> getDataMajAdmin($_GET['id']);

				//var_dump($var);exit;
				// On envoi toutes les donnees a la vue
				//$view -> assign('mpfModelAdmin', $var2);
				foreach ($var2 as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
				break;
			default :
				//   header("location:
				// http://".$_SERVER["SERVER_ADDR"]."/common/login/index.php");
				break;
		}

	} elseif ($user -> getAdmin() === false) {

		switch ($_SERVER['PATH_INFO']) {
			case '/errForm' :
				$dispatch = true;
				$r = $data -> getLibCarac($_POST['code']);
				$out = $view -> getFormError($r, $_POST["tarif"], $_POST['code2']);
				break;
			case '/save' :
				$dispatch = true;
				// donnees modifiees par gestionnaire
				if ($_POST['act'] == 'saveForm') {
					$out = $data -> savedataGest($_POST["dataForm"], $_POST["siteid"]);

					$out .= _("Donnèes sauvegardées avec succès") . "<br/>";
					$out .= _("Vos données sont enregistrées provisoirement mais ne sont pas transmises à Motor presse") . '.<br/>' . _("N'oubliez de revenir pour les compléter et les envoyer");

				} elseif ($_POST['act'] == 'sendForm') {
					$xt_page = "Envoi_MAJ::" . strtoupper(SystemLang::getUrlCampLang());

					$out = $data -> senddataGest($_POST["dataForm"], $_POST["siteid"]);

					// Il est possible de desactiver l'export en mettant cette entree a N dans la table c_params
					if (SystemParams::getParam("system*gen_xml") == 'Y') {
						$exXml = new BusinessExportXML( array( array("ID" => $_POST["siteid"])));
						$exXml -> exportToSite();
					}
					if (!$test) {
						$myMail = new BusinessMail();
						$myMail -> sendmailGest($data -> getMailGest($_POST["siteid"]));
					}
					$out .= _("Donnèes envoyées avec succès");
				}
				break;

			case '/gest' :
				//ACCEUIL
				$xt_page = "Tableau_de_bord::" . strtoupper(SystemLang::getUrlCampLang());
				$ACCESS_key = "gest.acc..";
				$var = $data -> getDataAccGest($user -> getUserId());
				$template = 'gestAcceuil';
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
				break;
			case '/valid' :
				$xt_page = "ENVOI::" . strtoupper(SystemLang::getUrlCampLang());
				$ACCESS_key = "gest.acc..";
				$var = $data -> getDataAccGest($user -> getUserId());
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				$template = 'gestAcceuil';
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
				break;
			case '/gest_maj' :
				//Mise à jours des données.
				$xt_page = "MAJ::" . strtoupper(SystemLang::getUrlCampLang());
				$ACCESS_key = "gest.maj..";
				$template = "gestMaj";
				$var = $data -> getDataMajGest($user -> getUserId());
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
				break;

			default :
				$ACCESS_key = "FALSE";
				//   header("location:
				// http://".$_SERVER["SERVER_ADDR"]."/common/login/index.php");
				break;
		}

	}
	if (!$dispatch) {
		//  SystemAccess::canAccessPage2();
		//  SystemUserTracking::trackUser();

		require $root_path . 'inc/header.inc.php';
		echo $view -> render($template);
		if ($tempCol) {
			if ($tempCol == "colRight") {
				$info1 = $data -> getCol1content();
				$view -> assign('bloc1', $info1);
			}
			echo $view -> render($tempCol);
		}

		require $inc_path . 'footer.inc.php';

	} else {
		echo $out;
	}
} else {
	header("location: http://" . $_SERVER["SERVER_ADDR"] . "/common/login/index.php");
}
