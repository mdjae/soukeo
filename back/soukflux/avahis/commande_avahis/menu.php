<?php


// RECHERCHE FILTER CATEGORY
$statusfilter = new FilterList("statusfilter", ("Choix du statut :"), "all");
$sql = " SELECT DISTINCT order_status FROM  skf_orders ORDER BY order_status ASC";
$rs = dbQueryAll($sql);  
$statusfilter->addElement('all','all');

foreach( $rs as $R){
    $statusfilter->addElement($R['order_status'],$R['order_status'] );
}


// RECHERHCE PAR NOM DE PRODUIT
$numCommande  = new FilterText("numfilter", ("N° de commande")." :", $_GET['id'], false, 12);
 
//Litiges
$litige = new FilterList("litigefilter", ("Commande en litige :"),"all");
$litige->addElement('1','litige');
$litige->addElement('0','pas litige');
$litige->addElement('all','all');

//Date
$date = new FilterList("datefilter", ("Date :"),"all");
$date->addElement('2','Récent');
$date->addElement('3','3 derniers jours');
$date->addElement('7','7 derniers jours');
$date->addElement('all','Depuis le début');

//Date
$grossiste = new FilterList("grossistefilter", ("Du grossiste :"),"all");
$grossiste->addElement('1','FK');
$grossiste->addElement('2','LDLC');
$grossiste->addElement('3','MAGINEA');
$grossiste->addElement('all','all');

$filter = new Filter("commande_avahis");
$filter->addFilter($statusfilter);
$filter->addFilter($numCommande);
$filter->addFilter($litige);
$filter->addFilter($date);
$filter->addFilter($grossiste);

$filter->setGridToReload("commande_avahis");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "avahis.commande_avahis..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "avahis.commande_avahis..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "avahis.commande_avahis..",
                "picto" => "edit",
                "name" =>("Assigner à une commande grossiste"),
                "url" => "javascript: popupNumComGrossiste();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("avahis.commande..","","denied",("Déclarer un litige")),
				array("avahis.commande..","","check",("Annuler un litige")), 	
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
