<?php

//Litiges
$stockfilter = new FilterList("stockfilter", ("Produit en stock :"),"all");
$stockfilter->addElement('1','en stock');
$stockfilter->addElement('0','rupture de stock');
$stockfilter->addElement('all','all');


$filter1 = new Filter("item_grossiste_to_order1");
$filter1->addFilter($stockfilter);

$filter2 = new Filter("item_grossiste_to_order2");
$filter2->addFilter($stockfilter);

$filter3 = new Filter("item_grossiste_to_order3");
$filter3->addFilter($stockfilter);

$filter3->setGridToReload("item_grossiste_to_order3");
$filter2->setGridToReload("item_grossiste_to_order2");
$filter1->setGridToReload("item_grossiste_to_order1");

drawRMenuHeader(("Options de filtrage"),"FILTER","80");

echo $filter3->display();
echo $filter2->display();
echo $filter1->display();


drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "avahis.commande_avahis..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          
          array
          (
                "access" => "avahis.commande_avahis..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>