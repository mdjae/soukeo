<?php

class AttributConfigImporter 
{
	private $_debug = 1;
	private $_debugLevel = "info";
	private $_debugMode = PHP_SAPI;
	private $_countLines = 1;
	private $_dir = '/opt/soukeo/export/configs/';
	private $_file ='attributes.csv';
	
	
	function update(){
		try {
			
		  $pdo = new PDO('mysql:host=localhost;dbname=someDatabase', $username, $password);
		  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		 
		  $stmt = $pdo->prepare('UPDATE someTable SET name = :name WHERE id = :id');
		  $stmt->execute(array(
		    ':id'   => $id,
		    ':name' => $name
		  ));
		   
		} catch(PDOException $e) {
		  echo 'Error: ' . $e->getMessage();
		}
	}
	/**
	 * Get plugin info
	 *
	 * @return array
	 */

	public function getPluginInfo() 
	{
		return array("name" => "SOUKEO Import Config Atribute", 
		"author" => "Soukeo", 
		"version" => "0.1.1", 
		"url" => "http://sourceforge.net/apps/mediawiki/magmi/index.php?title=#");
	}

	/**
	 * Logger
	 * @param debug int
	 * @param debugLevel string
	 * @param debugMode string
	 * @param msg string
	 *
	 */
	public function debugMsg($debug, $debugLevel, $debugMode, $msg) 
	{
		if ($debug != 0) {
			if ($debugMode === "CLI") {
				echo '[' . $debugLevel . '] - ' . $msg . PHP_EOL;

			} else {
				$this -> log($msg . PHP_EOL, $debugLevel);
			}
		}
	}

	/**
	 * Get orders in datetime interval
	 * @param datetime $debut
	 * @param datetime $fin
	 * @return array
	 */
	public function getEntityTypeData() 
	{
		// Recuperer id_type pour product_catlaog
		$eet = $this -> tablename("eav_entity_type");
		$sqlEet = "SELECT * FROM $eet WHERE entity_type_code = ?";
		$rowset = $this -> selectAll($sqlEet, array('catalog_product'));
		$r = $rowset[0];
		return $r;
	}



	/**
	 * Get orders in datetime interval
	 * @param datetime $debut
	 * @param datetime $fin
	 * @return array
	 */
	public function infoLoading() {
		// Information sur les lignes imports
		//$csv = explode("=",$_SERVER['argv'][2]);
		//$res = $csv[1];
		//$lines = count(file($res));

		//TODO
		$lines = "-NC-";
		$res = "-NC-";
		$cnt = $this -> _countLines++;
		return $this -> debugMsg($this -> _debug, $this -> _debugLevel, $this -> _debugMode, "$cnt sur :: $lines lignes dans :: $res");

	}


	/**
	 * Get orders in datetime interval
	 * @param datetime $debut
	 * @param datetime $fin
	 * @return array
	 */
	public function attrExist($attribute_code) {
		$ea = $this -> tablename("eav_attribute");
		$q = "SELECT `attribute_code` FROM $ea WHERE `attribute_code` =?";
		$attrExist = count($this -> selectAll($q, $attribute_code));

		return $attrExist;
	}
	
	/**
	 * Read data from csv
	 * @param $file string
	 * @return array
	 */
	 
	public function readCSV($file){
		
		$hdle = fopen($file,'r');
		
		while (!feof($hdle)){
			$line = fgetcsv($file,1024);
		}
		
		fclose($file);
		
		return $line;

	}
	
	
	/**
	 * Process before get magento id
	 * @param datetime $debut
	 * @param datetime $fin
	 * @return array
	 */
	public function processItemBeforeId(&$item, $params = null) {

		$this -> infoLoading();
		$key = $item['attribute_code'];
		//$key fournie le nom de l'attribut
			if (!empty($key)) {
				$attrExist = $this -> attrExist($key);
			} else {
				$attrExist = 0;
			}
		
			
		// Si l'attribut existe charger sa configuration
		if($attrExist == 1)
		{
			$sqlEA = " UPDATE  `eav_attribute`
			SET 
			`entity_type_id` = ?,
			`attribute_code` = ? ,
			`attribute_model` = NULL,
			`backend_model` = '',
			`backend_type` = 'varchar',
			`backend_table` = '',
			`frontend_model` = '',
			`frontend_input` = 'multiselect',
			`frontend_label` = ?,
			`frontend_class` = '',
			`source_model` = '',
			`is_required` = 0,
			`is_user_defined` = 1,
			`default_value` = '',
			`is_unique` = 0,
			`note` = ''
			WHERE
			`attribute_code` = ? ";
			
			$dataEA = array('', '', '');
			$EALastId = $this -> insert($sqlEA, $dataEA);
			
					
			
			$sqlCEA = "UPDATE `catalog_eav_attribut` 
			SET 
			`attribute_id`  = ?,
			`frontend_input_renderer` = '',
			`is_global` = 0,
			`is_visible` = 1,
			`is_searchable` = 1,
			`is_filterable` = ?,
			`is_comparable` = 1,
			`is_visible_on_front` = 1,
			`is_html_allowed_on_front` = 0,
			`is_used_for_price_rules` = 0,
			`is_filterable_in_search` = 0,
			`used_in_product_listing` = 0,
			`used_for_sort_by` = 0, 
			`is_configurable` = 1,
			`apply_to` = '',
			`is_visible_in_advanced_search` = 0,
			`position` = 0,
			`is_wysiwyg_enabled` = 0,
			`is_used_for_promo_rules` = 0 
			WHERE
			attribute_id = ?";
			
			$dataCEA = array('', '', '');
			$CEALastId = $this -> insert($sqlCEA, $dataCEA);

		
		} else {
            $msg = "attribut inexistant dans magento chargement abandonnée";
            $this->debugMsg($this->_debug,$this->_debugLevel,$this->_debugMode,$msg);
            return true;
        } 

	}

}
?>
