
<div id="myModalLivraison" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modifier le mode de livraison de <?php echo $livraisonType->getShipping_code()?></h3>
    </div>
    
    <div class="modal-body">
        <div class="more-info">
            Si vous sélectionnez remboursés, les montants de frais de livraison
            seront <strong>remboursés</strong> au vendeur sur chaque PO utilisant ce mode de livraison lors de la facturation.
        </div>
        <br />
        
        <form class="form-horizontal" id="form_edit_livraison">

            <div class="control-group">          
                <label class="control-label" for="refund">Mode de livraison remboursé :</label>
                    <div class="controls">
                        <select name="refund" id="refund">
                            <option value="0" <?php echo $livraisonType->getRefund() == 0 ? "selected" : "" ?> >Non</option>
                            <option value="1" <?php echo $livraisonType->getRefund() == 1 ? "selected" : "" ?> >Oui</option>
                        </select>
                    </div>
            </div>

            <button class="btn btn-primary" onClick="validEditLivraisonType(<?php echo $livraisonType -> getShipping_id() ?>);return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Enregistrer</button>
            <button type="reset" class="btn btn-inverse">Reset</button> 
        </form>

    </div>
    
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
