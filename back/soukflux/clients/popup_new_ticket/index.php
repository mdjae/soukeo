<script type="text/javascript" charset="utf-8">
    tinymce.PluginManager.add('example', function(editor, url) {
        // Add a button that opens a window
        editor.addButton('example', {
            text: 'Lien produit',
            icon: false,
            onclick: function() {
                // Open window
                $("#modalAddProduct").modal();

            }
        });

    });
        tinymce.init({
            selector:'textarea',
            force_br_newlines : true,
            force_p_newlines : false,
            forced_root_block : '',
            plugins: 'example link',
            content_css : "css/custom_content.css",
            toolbar: 'example link undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'});
    
    
    function productFormatResult(product) {
        var markup = "<table class='product-result'><tr>";
        if (product.image !== undefined ) {
            markup += "<td class='product-image'><img src='" + product.image + "'/></td>";
        }
        markup += "<td class='product-info'><div class='product-title'>" + product.text + "</div>";
        
        if (product.shortdesc !== undefined) {
            markup += "<div class='product-synopsis'>" + product.shortdesc + "</div>";
        }
        //else if (movie.synopsis !== undefined) {
          //  markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
        //}
        markup += "</td></tr></table>";
        return markup;
    }

    function productFormatSelection(product) {
        return product.text;
    }
        
    $("#listprods").select2({
        minimumInputLength: 4,
        ajax: {
            url: "/app.php/getListProdSelect2",
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        },
        formatResult: productFormatResult, 
        formatSelection: productFormatSelection,  
        dropdownCssClass: "bigdrop", 
        escapeMarkup: function (m) { return m; }    
    }); 
    
    function addContentTinyMCE (content) {
      var $id_produit = $("#listprods").select2('data').id;  
      
      theData = {
          id_produit : $id_produit
      }
      
      var $html = "";
      
      jQuery.ajax({
        type : 'POST',
        url : "/app.php/getproduct",
        data : theData,
        success : function(data) {
            
            tinyMCE.activeEditor.dom.add(tinyMCE.activeEditor.getBody(), 'p', {title : 'Produit :'}, data);
        }
      });
    }            
</script>
<div id="myModalTicket" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
  <div class="modal-header navbar">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Nouveau ticket</h3>
  </div> <!-- modal-header -->
  
  <div class="modal-body">
    <form class="form-horizontal" id="form_add_ticket">
    
      <div class="control-group">
        <label class="control-label" for="inputTitle">Titre :</label>
        <div class="controls">
          <input type="text" id="inputTitle" placeholder="Titre..." name="inputTitle" size="80" >
        </div>
      </div><!-- control-group -->
      
      <div class="control-group">
        <label class="control-label" for="inputTexte">Texte :</label>
        <div class="controls">
          <textarea name="inputTexte" id"inputTexte" rows="8" cols="50" style="width: 150px; height: 150px;"></textarea>
        </div>
      </div> <!-- control-group -->
      
      <div class="row-fluid">
      <div class="span6">      
      <div class="control-group">
<?php 
        if($customer){ ?>
            <input type="hidden" id="inputEntity_id" name="inputEntity_id" value="<?php echo $customer -> getEntity_id() ?>">
            <label class="control-label" for="inputCustomer_id">Id Avahis Client :</label>
            <div class="controls">
                <input type="hidden" id="inputClient_type_id" name="inputClient_type_id" value="<?php echo $thisClientType->getType_id() ?>"> 
<?php       if($customer -> getCustomer_id()){ ?>
                <input type="text" id="inputCustomer_id" value="<?php echo $customer -> getCustomer_id()?>" disabled>    
            </div>    
<?php       }else{ ?>
                <input type="text" id="inputCustomer_id" placeholder="Laisser vide si non inscrit...">
            </div>    
<?php       } 

        }elseif($vendor){ ?>
            <input type="hidden" id="inputEntity_id" name="inputEntity_id" value="<?php echo $vendor -> getEntity_id() ?>">
            <label class="control-label" for="inputCustomer_id">Id Avahis Client :</label>
            <div class="controls">
                <input type="hidden" id="inputClient_type_id" name="inputClient_type_id" value="<?php echo $thisClientType->getType_id() ?>">
<?php       if($vendor -> getVendor_id()){ ?>
                <input type="text" id="inputVendor_id" value="<?php echo $vendor -> getVendor_id()?>" disabled>
            </div>        
<?php       }else{ ?>
                <input type="text" id="inputVendor_id" placeholder="Laisser vide si non inscrit...">
            </div>    
<?php       }
        }else{ ?>
            
            <label class="control-label" for="inputCustomer_id">Id Avahis Client:</label>
            <div class="controls">
                <input type="text" id="inputCustomer_id" placeholder="Laisser vide si non inscrit...">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputClient_type_id">Type Client :</label>
            <div class="controls">
                <select id="inputClient_type_id" name="inputClient_type_id">
                <?php foreach($typeList as $typeCli){ ?>
                    <option value="<?php echo $typeCli -> getType_id() ?>"><?php echo ucfirst($typeCli -> getLabel()) ?></option>
                <?php } ?>
                </select>
            </div>
<?php   } ?>
            
      </div> <!--control-group -->
      
      <div class="control-group"> 
        <label class="control-label" for="inputName">Nom Client :</label>
        <div class="controls">
<?php 
        if($customer && $customer -> getFirstname() && $customer -> getLastname()){ ?>
            <input type="text" id="inputName" value="<?php echo strtoupper($customer -> getFirstname())." ".strtoupper($customer -> getLastname())?>" disabled>
<?php   }
        elseif($vendor && $vendor -> getVendor_nom()){ ?>
            <input type="text" id="inputName" value="<?php echo ($vendor -> getVendor_nom()) ?>" disabled>

<?php   }
        else{ ?>
            <input type="text" id="inputName" placeholder="Nom client...">
<?php   }
?>  
        </div>
      </div> <!-- control-group -->


      <div class="control-group">
        <label class="control-label" for="TicketType">Type de ticket :</label>
        <div class="controls">  
          <?php echo BusinessReferentielUtils::getSelectList("TicketType", "Type de ticket...") ?>
        </div>
      </div><!-- control-group -->
            
    </div>
    <div class="span6">
      
      <div class="control-group">
        <label class="control-label" for="inputPriority_id">Prioritée :</label>
        <div class="controls">  
          <select id="inputPriority_id" name="inputPriority_id">
                <?php foreach($priorityList as $priority){ ?>
                    <option value="<?php echo $priority -> getPriority_id() ?>"><?php echo ucfirst($priority -> getLabel()) ?></option>
                <?php } ?>
          </select>
        </div>
      </div><!-- control-group -->
            
      
      
      <div class="control-group">
        <label class="control-label" for="listuser">Observateurs :</label>
        <div class="controls">
         <input type="hidden" id="listuser" />       
        </div>
      </div> <!-- control-group -->
       
      <div class="control-group">
        <label class="control-label" for="listassignuser">Assigner à (optionel) :</label>
        <div class="controls">
         <input type="hidden" id="listassignuser" />       
        </div>
      </div> <!-- control-group -->
    
     <div class="control-group">          
        <label class="control-label" for="ticket_visibility">Visible pour le client :</label>
        <div class="controls">
            <input type="checkbox" id="ticket_visibility" name="ticket_visibility" value="1"> 
        </div>
     </div>   
   </div> 
   </div>  
             <br /><br /><br />
        <button class="btn btn-primary" onClick="validFormAddTicket();return false;" data-dismiss="modal" aria-hidden="true">Enregistrer</button>
        <button type="reset" class="btn btn-inverse">Reset</button> 
    </form>
  </div> <!-- modal-body -->
  
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div> <!-- #myModal -->
<div id="modalAddProduct" class="modal hide fade"  role="dialog" aria-labelledby="addProductModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="addProductModalLabel">Ajouter lien produit</h4>
  </div>
  <div class="modal-body">
      <input type="hidden" id="listprods" name="listprods" style="width:95%;">
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" onClick="addContentTinyMCE(); return false;">Ajouter</button> 
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<script type="text/javascript" charset="utf-8">
    var list = jQuery.parseJSON('<?php echo $jsondata ?>');
    
    $('#listuser').select2({
        data:list,
        multiple: true,
        width: "250px"
    });
    
    $('#listuser').select2("val", <?php echo $user ->getUserId() ?>);

    $('#listassignuser').select2({
        data:list,
        multiple: false,
        width: "250px"
    });	
</script>