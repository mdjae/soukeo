<div class="well">
    <button class="btn btn-primary" onClick="showDialogAddTicket(); return false;"><i class="fa fa-add icon-black" style="padding-right: 0px" ></i>&nbsp;&nbsp;&nbsp;&nbsp;Créer ticket</button>
</div>
<?php

// RECHERHCE PAR num commande
$codefilter  = new FilterText("codefilter", ("Code ticket")." :", "", false, 12);
$clientfilter  = new FilterText("clientfilter", ("Nom/Prénom client")." :", "", false, 12);

//Date
$priorityfilter = new FilterList("priorityfilter", ("Priorité :"),"all");
BusinessReferentielUtils::getGridFilterList("TicketPriority", $priorityfilter) -> addElement('all', 'all');


$statutfilter = new FilterList("statutfilter", ("Statut :"),"all") ;
BusinessReferentielUtils::getGridFilterList("TicketState", $statutfilter) -> addElement('all', 'all');


$typefilter = new FilterList("typefilter", ("Type de ticket :"),"all") ;
BusinessReferentielUtils::getGridFilterList("TicketType", $typefilter) -> addElement('all', 'all');


$userassignedfilter = new FilterList("userassignedfilter", ("Assigné à :"), "all");
$userManager = new ManagerAppUser();
$list_user = $userManager->getAll();

$userassignedfilter->addElement('all','all');
foreach( $list_user as $user){
    $userassignedfilter->addElement($user->getUser_id(),$user->getName());
}



$filter = new Filter("tickets");
$filter->addFilter($codefilter);
$filter->addFilter($clientfilter);
$filter->addFilter($priorityfilter);
$filter->addFilter($statutfilter);
$filter->addFilter($userassignedfilter);
$filter->addFilter($typefilter);

$filter->setGridToReload("tickets");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "clients.listing_tickets..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "clients.listing_tickets..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(			
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>