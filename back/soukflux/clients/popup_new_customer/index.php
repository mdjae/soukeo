<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Nouveau client Acheteur</h3>
  </div>
  <div class="modal-body">
    <form class="form-horizontal" id="form_add_customer">
       
      <div class="control-group">
        <label class="control-label" for="inputLastname">Nom :</label>
        <div class="controls">
          <input type="text" id="inputLastname" placeholder="Nom..." name="inputLastname" >
        </div>
      </div>
      
      
      <div class="control-group">
        <label class="control-label" for="inputFirstname">Prénom :</label>
        <div class="controls">
          <input type="text" id="inputFirstname" placeholder="Prénom..." name="inputFirstname" >
        </div>
      </div>
      
      
      <div class="control-group">
        <label class="control-label" for="inputCompany">Entreprise :</label>
        <div class="controls">
          <input type="text" id="inputCompany" placeholder="Entreprise..." name="inputCompany" >
        </div>
      </div>
      
              
      <div class="control-group">
        <label class="control-label" for="inputEmail">Email :</label>
        <div class="controls">
          <input type="email" id="inputEmail" placeholder="Email..." name="inputEmail" >
        </div>
      </div>
          
      <div class="control-group">
        <label class="control-label" for="inputPhone">Tel 1 :</label>
        <div class="controls">
          <input type="text" id="inputPhone" placeholder="Tel 1..." name="inputPhone" >
        </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="inputPhone2">Tel 2 :</label>
        <div class="controls">
          <input type="text" id="inputPhone2" placeholder="Tel 2..." name="inputPhone2" >
        </div>
      </div>
      
         
      <div class="control-group">
        <label class="control-label" for="inputBirthd">Date de naissance :</label>
        <div class="controls">
         <input type="text" id="inputBirthd" placeholder="Année-mois-jour" name="inputBirthd"/>       
        </div>
      </div>   
            
      <div class="control-group">          
        <label class="control-label" for="inputStatut_id">Statut :</label>
            <div class="controls">
                <select id="inputStatut_id" name="inputStatut_id">
                    <?php foreach($statusList as $statutCli){ ?>
                        <option value="<?php echo $statutCli -> getStatut_id() ?>"><?php echo ucfirst($statutCli -> getLabel()) ?></option>
                    <?php } ?>
                </select>
            </div>
      </div>
     
      
        <button class="btn btn-primary" onClick="validFormAddCustomer();return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Enregistrer</button>
        <button type="reset" class="btn btn-inverse">Reset</button> 
    </form>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<script>
  $(function() {
    $( "#inputBirthd" ).datepicker({
      changeMonth: true,
      changeYear: true,
      defaultDate: "-20y",
      dateFormat : "yy-mm-dd",
      showOn: "button",
      buttonImage: "../../skins/common/images/picto/grid_ecom.png",
      
    }); 
  });
  </script>