<div id="myModalVendor" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Nouveau client Vendeur</h3>
  </div>
      <div class="modal-body">     
          
        <form class="form-horizontal" id="form_add_vendor">
            <div class="row-fluid">
             <div class="span6">   
                 
                 <div class="control-group">
                    <label class="control-label" for="inputCiv">Civilité :</label>
                    <div class="controls">
                      <select name="inputCiv" id="inputCiv">
                          <option value ="Mr">Mr</option>
                          <option value ="Mme">Mme</option>
                      </select>
                    </div>
                  </div>
                  
                 <div class="control-group">
                    <label class="control-label" for="inputLastname">Nom :</label>
                    <div class="controls">
                      <input type="text" id="inputLastname" placeholder="Nom..." name="inputLastname" >
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label" for="inputFirstname">Prénom :</label>
                    <div class="controls">
                      <input type="text" id="inputFirstname" placeholder="Prénom..." name="inputFirstname" >
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label" for="inputName">Entreprise :</label>
                    <div class="controls">
                      <input type="text" id="inputName" placeholder="Entreprise..." name="inputName" >
                    </div>
                  </div>
    
                     
                  <div class="control-group">
                    <label class="control-label" for="inputSiret">N° Siret :</label>
                    <div class="controls">
                     <input type="text" id="inputSiret" placeholder="Siret..." name="inputSiret"/>       
                    </div>
                  </div>   
                  
                  <div class="control-group">
                    <label class="control-label" for="inputEmail">Email :</label>
                    <div class="controls">
                      <input type="email" id="inputEmail" placeholder="Email..." name="inputEmail" >
                    </div>
                  </div>
    
              
                  <div class="control-group">
                    <label class="control-label" for="inputPhone">Tel 1 :</label>
                    <div class="controls">
                      <input type="text" id="inputPhone" placeholder="Tel 1..." name="inputPhone" >
                    </div>
                  </div>  

              
              </div> <!-- span6 -->
              <div class="span1"></div>
              <div class="span5">
                  
                  <div class="control-group">
                    <label class="control-label" for="inputType">Type vendeur :</label>
                    <div class="controls">
                      <select name="inputType" id="inputType">
                          <option value ="com">Commerçant</option>
                          <option value ="ecom">e-Commerçant</option>
                      </select>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="inputStreet">Rue :</label>
                    <div class="controls">
                      <input type="email" id="inputStreet" placeholder="Rue..." name="inputStreet" >
                    </div>
                  </div>
    
                  <div class="control-group">
                    <label class="control-label" for="inputCity">Ville :</label>
                    <div class="controls">
                      <input type="email" id="inputCity" placeholder="Ville..." name="inputCity" >
                    </div>
                  </div>
                                
                  <div class="control-group">
                    <label class="control-label" for="inputZip">Code Postal :</label>
                    <div class="controls">
                      <input type="email" id="inputZip" placeholder="Code Postal..." name="inputZip" >
                    </div>
                  </div>
                  
                  <div class="control-group">
                    <label class="control-label" for="inputCountryId">Pays :</label>
                    <div class="controls">
                      <select name="inputCountryId" id="inputCountryId">
                          <option value ="RE">Réunion</option>
                          <option value ="YT">Mayotte</option>
                          <option value ="MU">Maurice</option>
                          <option value ="FR">France</option>
                      </select>
                    </div>
                  </div>
                  
    
    
                  <div class="control-group">
                    <label class="control-label" for="inputVendorUrl">Site internet :</label>
                    <div class="controls">
                     <input type="text" id="inputVendorUrl" placeholder="Site Internet..." name="inputSiret"/>       
                    </div>
                  </div>   
                                      
                  <div class="control-group">          
                    <label class="control-label" for="inputStatut_id">Statut :</label>
                        <div class="controls">
                            <select id="inputStatut_id" name="inputStatut_id">
                                <?php foreach($statusList as $statutCli){ ?>
                                    <option value="<?php echo $statutCli -> getStatut_id() ?>"><?php echo ucfirst($statutCli -> getLabel()) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                  </div>
                  
              </div> <!-- span5 -->
              
        </div><!-- row-fluid -->
        
        <div class="row-fluid">
            <button class="btn btn-primary" onClick="validFormAddvendor();return false;" data-dismiss="modal" aria-hidden="true">Enregistrer</button>
            <button type="reset" class="btn btn-inverse">Reset</button>    
        </div>
         
    </form>
  </div> <!-- .modal-body -->
  <div class="modal-footer">
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<script>
  $(function() {
    $( "#inputBirthd" ).datepicker();
  });
  </script>