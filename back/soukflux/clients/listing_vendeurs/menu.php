<div class="well">
    <button class="btn btn-primary" onClick="showDialogAddVendor(); return false;"><i class="fa fa-add icon-black" style="padding-right: 0px" ></i>&nbsp;&nbsp;&nbsp;&nbsp;Créer vendeur</button>
</div>

<?php
// RECHERHCE PAR num vendeur/entity id/nom
$namefilter  = new FilterText("namefilter", ("Nom client")." :", "", false, 12);
$numclient  = new FilterText("numclient", ("Id interne client")." :", $_GET['id'], false, 12);
$vendor_id  = new FilterText("vendor_id", ("Id Avahis")." :", $_GET['vendor_id'], false, 12);

//Date
$date = new FilterList("datefilter", ("Date :"),"all");
$date->addElement('2','Récent');
$date->addElement('3','3 derniers jours');
$date->addElement('7','7 derniers jours');
$date->addElement('all','Depuis le début');

//Inscription
$inscriptionfilter = new FilterList("inscriptionfilter", ("Inscrit Avahis :"),"all");
$inscriptionfilter->addElement('all','Tous');
$inscriptionfilter->addElement('1','Oui');
$inscriptionfilter->addElement('0','Non');

//Avec ou sans mail
$mailfilter = new FilterList("mailfilter", ("Email :"),"all");
$mailfilter->addElement('all','Tous');
$mailfilter->addElement('1','Avec');
$mailfilter->addElement('0','Sans');


$filter = new Filter("vendors");
$filter->addFilter($namefilter);
$filter->addFilter($numclient);
$filter->addFilter($vendor_id);
$filter->addFilter($date);
$filter->addFilter($inscriptionfilter);
$filter->addFilter($mailfilter);




$filter->setGridToReload("vendors");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "clients.listing_vendeurs..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "clients.listing_vendeurs..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(			
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">
    $('table.paginated').each(function() {
        var currentPage = 0;
        var numPerPage = 10;
        var $table = $(this);
        $table.bind('repaginate', function() {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<div class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function(event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertBefore($table).find('span.page-number:first').addClass('active');
});
</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>