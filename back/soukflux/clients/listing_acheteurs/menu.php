<div class="well">
    <button class="btn btn-primary" onClick="showDialogAddCustomer(); return false;"><i class="fa fa-add icon-black" style="padding-right: 0px" ></i>&nbsp;&nbsp;&nbsp;&nbsp;Créer client</button>
</div>

<?php
// RECHERHCE PAR num commande
$namefilter  = new FilterText("namefilter", ("Nom client")." :", "", false, 12);
$numclient  = new FilterText("numclient", ("N° Interne")." :", $_GET['id'], false, 12);

//Date
$date = new FilterList("datefilter", ("Date :"),"all");
$date->addElement('2','Récent');
$date->addElement('3','3 derniers jours');
$date->addElement('7','7 derniers jours');
$date->addElement('all','Depuis le début');



$filter = new Filter("customers");
$filter->addFilter($namefilter);
$filter->addFilter($numclient);
$filter->addFilter($date);

$filter->setGridToReload("customers");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "clients.listing_acheteurs..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "clients.listing_acheteurs..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAll();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(			
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
<script type="text/javascript" charset="utf-8">

</script>
<style type="text/css" media="screen">
	#id_filtercat{width:150px}
	#id_filtercat option{width:150px}
</style>