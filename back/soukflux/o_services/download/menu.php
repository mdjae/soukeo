﻿<?php 
if(SystemAccess::canAccess("services.download..allusers"))
{
	$f1 = new FilterList("usercode", ("Utilisateur :"),"all");
	$res = dbQueryAll("SELECT distinct usercode FROM c_user ORDER BY usercode");
	$f1->addElement("all","Tous");
	foreach($res as $y)
	{
		$f1->addElement($y['usercode'],$y['usercode']);
	}
}
else
{
	$f1 = new FilterList("usercode", ("Utilisateur :"),"");
	$f1->addElement($_SESSION['usercode'],$_SESSION['usercode']);
}
$ffic = new FilterText("fichier", ("<b>Nom du fichier:</b><br/>(%recherche%) "), "", false, 12);
$fdate1 = new FilterText("date1", ("<b>Date de début :</b><br/>(jj/mm/aaaa) "), "", false, 10);
$fdate2 = new FilterText("date2", ("<b>Date de fin :</b><br/>(jj/mm/aaaa) "), "", false, 10);


$filter = new Filter("UserList");
$filter->addFilter($f1);
$filter->addFilter($ffic);
$filter->addFilter($fdate1);
$filter->addFilter($fdate2);
$filter->setGridToReload("download");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();

$expl_array = array(
                  array("services.download..","iconAction","download",("Téléchargement du fichier")),
          );
echo drawRMenuExplanation(("L�gende"),$expl_array);
?>
