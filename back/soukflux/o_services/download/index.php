﻿<?php 
$root_path = "../../";
$ACCESS_key = "services.download..";
$security_domain = "services";
$domain_categ = "download";
$right_menu_visibility = true;
require $root_path.'inc/header.inc.php';
//initialisation du tableau contenant [user][fichierdisponnibles]

//test d'accès a tous les fichiers disponnibles
if(SystemAccess::canAccess("services.download..allusers"))
{
	$usercode = "all";
}
//accès au données de la session en cour seulement
else if(SystemAccess::canAccess("services.download.."))
{
	$usercode = $_SESSION['smartlogin'];
}
$grid = new DownloadGrid($usercode);
echo $grid->display();
require $inc_path.'footer.inc.php';
?>