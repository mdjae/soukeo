﻿<?php
$root_path = "../../";
$ACCESS_key = "services.download..";
require $root_path . 'inc/webservice.header.inc.php';
if (array_key_exists('path', $_GET)) $path = $_GET['path'];
if (array_key_exists('file', $_GET)) $file = $_GET['file'];
if (array_key_exists('outfile', $_GET)) $filename = $_GET['outfile'];
else $filename = $file;
if(strpos($_SERVER['HTTP_USER_AGENT'], "MSIE"))
{
	session_cache_limiter("must-revalidate");
}

$fichier = $path. "/" .$file;

//we define the name to give to the download file
header("Content-disposition: attachment; filename=\"" . $filename . "\"");
//Processing for download
//header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
//header("Content-Transfer-Encoding: binary");
//header("Content-Length:".filesize($fichier));

//header("Expires: 0");
$updateQuery =  "INSERT INTO sfk_actions
					(
						usercode,
						logdate,
						logtype,
						logfilename,
						logaction,
						logstatus
					)
					VALUES
					(
						'" . $_SESSION['smartlogin'] . "', 
						now(),
						'DOWNLOAD_HTTP',
						'" . addslashes($fichier) . "', 
						'Téléchargement',
						'END'
					);";
$update = dbQuery($updateQuery); 

echo file_get_contents($fichier);
//require $root_path . "inc/webservice.footer.inc.php";