﻿<?php 
/**
 * Script utilisé par Nagios pour controler le bon fonctionner
 * du batch Padoc du matin
 * NE PAS SUPPRIMER
 */



$root_path = "../";
require_once $root_path .	'config/config.inc.php';
$config_file = $root_path . '/config/config.inc.php';
require_once $root_path .	'inc/libs/db.mysql.inc.php';
$db_conn = dbConnect();

/*
$kuery = "select *
		from c_batch_log b
		where b.prog_id = 4
		and status = 'END'
		and start_date between concat(YEAR(now()), '-', MONTH(now()), '-', DAY(now()), ' 05:00:00') AND now()";
		*/
$kuery = "select *
		from c_batch_log b
		where b.prog_id = 4
		and status = 'END'
		and 
		case when HOUR(now()) between 0 and 7
		  then concat(YEAR(start_date), '-', MONTH(start_date), '-', DAY(start_date)) = concat(YEAR(DATE_ADD(now(), INTERVAL -1 DAY)), '-', MONTH(DATE_ADD(now(), INTERVAL -1 DAY)), '-', DAY(DATE_ADD(now(), INTERVAL -1 DAY)))
		    else start_date between concat(YEAR(now()), '-', MONTH(now()), '-', DAY(now()), ' 05:00:00') AND now()
    		end";
$r = dbQueryAll($kuery);
//print_r($r); 
if (count($r) > 0)
{
	$r = $r[0];
	echo "Padoc batch log_id " . $r['log_id'] . ", prog_id " . $r['prog_id'] . ", class " . $r['class'] . ", start_date " . $r['start_date'] . ", end_date " . $r['end_date'] . "\n";
	exit(0);
}
else
{
	echo "No padoc batch!";
	exit(1);
}