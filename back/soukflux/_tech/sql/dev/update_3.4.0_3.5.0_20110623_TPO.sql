drop table padoc_transco;
ALTER TABLE padoc_transco_double ADD CTM VARCHAR( 9 ) NOT NULL;
ALTER TABLE padoc_transco_double ADD account_name VARCHAR( 50 ) NOT NULL;
ALTER TABLE padoc_transco_double DROP PRIMARY KEY ,
ADD PRIMARY KEY ( code_client , CTM );

CREATE TABLE padoc_log_blv (
blv_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'id du blv',
blv_num VARCHAR( 12 ) NOT NULL COMMENT 'Num�ro du blv',
type_client VARCHAR( 10 ) NOT NULL COMMENT 'type de client (do / non do ) ',
code_client VARCHAR( 9 ) NOT NULL COMMENT 'code client livr�',
erm_code VARCHAR( 9 ) NOT NULL COMMENT 'code franchis� ',
logfilename VARCHAR( 128 ) NOT NULL COMMENT 'nom du fichier du blv',
logdate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date de traitement'
) ENGINE = InnoDB COMMENT = 'table de log des blv';
ALTER TABLE `padoc`.`padoc_log_blv` ADD UNIQUE `unique_blv` ( `blv_num` );

CREATE TABLE IF NOT EXISTS `padoc_transco_double_tmp` (
  `code_client` varchar(9) NOT NULL,
  `RFARate_negoce` float(8,2) NOT NULL,
  `RFARate_PS` float(8,2) NOT NULL,
  `ccu` float(8,2) NOT NULL,
  `CTM` varchar(9) NOT NULL,
  `account_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`code_client`,`CTM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `padoc_pkg_tmp` (
  `AccountNum` varchar(16) NOT NULL,
  `regle_rfa` varchar(16) NOT NULL,
  PRIMARY KEY (`AccountNum`)
) ENGINE=innoDB DEFAULT CHARSET=latin1;

CREATE TABLE `padoc`.`padoc_log_file` (
`logid` INT NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'id du traitement',
`filename_log` VARCHAR( 150 ) NOT NULL COMMENT 'nom du fichier de log'
) ENGINE = InnoDB;



INSERT INTO `c_batch` (`prog_id`, `hour`, `day`, `month`, `year`, `class`, `params`, `email`, `comment`, `active`) VALUES
(9, '*', '*', '*', '*', 'BatchPadocReportProd', '*', '*', 'G�n�ration de rapport de production', 'FALSE');


ALTER TABLE c_batch ENGINE = InnoDB;
ALTER TABLE c_batch_log ENGINE = InnoDB;
ALTER TABLE c_box ENGINE = InnoDB;
ALTER TABLE c_group ENGINE = InnoDB;
ALTER TABLE c_group_security ENGINE = InnoDB;
ALTER TABLE c_group_users ENGINE = InnoDB;
-- ALTER TABLE c_obj_persistence ENGINE = InnoDB;
ALTER TABLE c_offipse_version ENGINE = InnoDB;
ALTER TABLE c_params ENGINE = InnoDB;
ALTER TABLE c_team_users ENGINE = InnoDB;
--ALTER TABLE c_user ENGINE = InnoDB;
ALTER TABLE c_user_auth_sources ENGINE = InnoDB;
ALTER TABLE c_user_logged_in ENGINE = InnoDB;
ALTER TABLE c_user_sessions ENGINE = InnoDB;
ALTER TABLE c_user_tracking ENGINE = InnoDB;
ALTER TABLE padoc_actions ENGINE = InnoDB;
ALTER TABLE padoc_ftp_transfert ENGINE = InnoDB;
-- ALTER TABLE padoc_histo_download ENGINE = InnoDB;
ALTER TABLE padoc_log_blv ENGINE = InnoDB;
ALTER TABLE padoc_log_sales ENGINE = InnoDB;
ALTER TABLE padoc_pkg ENGINE = InnoDB;
ALTER TABLE padoc_transco_double ENGINE = InnoDB;
