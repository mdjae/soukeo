
CREATE TABLE IF NOT EXISTS `SUIVI_FACT_EXP_FORM` (
  `ID_FORMAT` tinyint(3) unsigned NOT NULL,
  `LABEL_FORMAT` varchar(128) DEFAULT NULL,
  `MIME_TYPE` varchar(128) NOT NULL,
  `ico` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID_FORMAT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `SUIVI_FACT_EXP_FORM`
--

INSERT INTO `SUIVI_FACT_EXP_FORM` (`ID_FORMAT`, `LABEL_FORMAT`, `MIME_TYPE`, `ico`) VALUES
(1, 'CSV', 'text/csv', NULL),
(2, 'Excel', 'application/vnd.ms-excel', NULL),
(3, 'PDF', 'application/pdf', NULL);



--
-- Structure de la table `SUIVI_FACT_FILE_FSE_STATUS`
--

CREATE TABLE IF NOT EXISTS `SUIVI_FACT_FILE_FSE_STATUS` (
  `ID_STATUS` tinyint(3) unsigned NOT NULL,
  `STATUS_LABEL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID_STATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `SUIVI_FACT_FILE_FSE_STATUS`
--

INSERT INTO `SUIVI_FACT_FILE_FSE_STATUS` (`ID_STATUS`, `STATUS_LABEL`) VALUES
(0, 'inject�'),
(10, 'calcul en cours'),
(11, 'calcul�'),
(12, 'calcul� avec modification'),
(19, 'erreur de calcul'),
(20, 'envoie planifi�'),
(21, 'envoie en cours'),
(22, 'envoy�'),
(29, 'erreur d''envoie');


--
-- Structure de la table `SUIVI_FACT_FILE`
--


CREATE TABLE IF NOT EXISTS `SUIVI_FACT_FILE` (
  `MONTH` date NOT NULL,
  `ID_FILE` tinyint(3) unsigned NOT NULL,
  `DATE_UPLOAD_START` date DEFAULT NULL,
  `DATE_UPLOAD_END` date DEFAULT NULL,
  `USER_ID_UPLOAD` int(11) DEFAULT NULL,
  `FNAME_UPLOAD` varchar(128) DEFAULT NULL,
  `FNAME_ARCHIVE` varchar(128) DEFAULT NULL,
  `INTEGRATION` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`MONTH`,`ID_FILE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Structure de la table `SUIVI_FACT_FILE_FSE`
--

CREATE TABLE IF NOT EXISTS `SUIVI_FACT_FILE_FSE` (
  `MONTH` date NOT NULL,
  `ID_FILE` tinyint(3) unsigned NOT NULL,
  `USER_ID_FSE` int(11) NOT NULL,
  `ID_STATUS` tinyint(3) unsigned NOT NULL,
  `NB_BLV` int(11) DEFAULT NULL,
  `CONTREMARQUE` varchar(9) NOT NULL,
  `DATE_CHG_STATUS` date DEFAULT NULL,
  `CA_NEGO` float(10,2) DEFAULT NULL,
  `CA_PS` float(10,2) DEFAULT NULL,
  `CA_TOTAL_HT` float(10,2) DEFAULT NULL,
  `NEGO_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `PS_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `PU_HT` float(10,2) DEFAULT NULL,
  `BLV_TOTAL_HT_AMOUNT` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`MONTH`,`ID_FILE`,`USER_ID_FSE`),
  KEY `FK_STATUT` (`ID_STATUS`),
  KEY `FK_PRIMARY_MONTH_ID_FILE` (`MONTH`,`ID_FILE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Structure de la table `SUIVI_FACT_FILE_FSE_BLV`
--

CREATE TABLE IF NOT EXISTS `SUIVI_FACT_FILE_FSE_BLV` (
  `MONTH` date NOT NULL,
  `ID_FILE` tinyint(3) unsigned NOT NULL,
  `USER_ID_FSE` int(11) NOT NULL,
  `NUM_BLV` varchar(12) NOT NULL,
  `CA_NEGO` float(10,2) DEFAULT NULL,
  `CA_PS` float(10,2) DEFAULT NULL,
  `CA_TOTAL_HT` float(10,2) DEFAULT NULL,
  `NEGO_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `PS_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `PU_HT` float(10,2) DEFAULT NULL,
  `BLV_TOTAL_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `DATE_BLV` date DEFAULT NULL,
  PRIMARY KEY (`MONTH`,`NUM_BLV`,`ID_FILE`,`USER_ID_FSE`),
  KEY `FK_PRIMARY_MONTH_USER_ID_ID_FILE` (`MONTH`,`USER_ID_FSE`,`ID_FILE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




--
-- Structure de la table `SUIVI_FACT_FILE_FSE_LINE_IN`
--

CREATE TABLE IF NOT EXISTS `SUIVI_FACT_FILE_FSE_LINE_IN` (
  `MONTH` date NOT NULL,
  `ID_FILE` tinyint(3) unsigned NOT NULL,
  `USER_ID_FSE` int(11) NOT NULL,
  `ID_LINE` int(10) unsigned NOT NULL,
  `NUM_BLV` varchar(12) NOT NULL,
  `CLT_COM_CLT_MODUS` varchar(9) DEFAULT NULL,
  `CLT_COM_RS` varchar(128) DEFAULT NULL,
  `CLT_LIV_CLT_MODUS` varchar(9) DEFAULT NULL,
  `CLT_LIV_RS` varchar(128) DEFAULT NULL,
  `REF_ART_MODUS` varchar(45) DEFAULT NULL,
  `LIB_ART` varchar(128) DEFAULT NULL,
  `QTY` int(11) DEFAULT NULL,
  `CA_NEGO` float(10,2) DEFAULT NULL,
  `CA_PS` float(10,2) DEFAULT NULL,
  `CA_TOTAL_HT` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`MONTH`,`ID_FILE`,`USER_ID_FSE`,`ID_LINE`,`NUM_BLV`),
  KEY `FK_PRIMARY_MONTH_ID_FILE_USER_ID_FSE` (`MONTH`,`ID_FILE`,`USER_ID_FSE`,`NUM_BLV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `SUIVI_FACT_FSE_LINE_CALC`
--

CREATE TABLE IF NOT EXISTS `SUIVI_FACT_FSE_LINE_CALC` (
  `MONTH` date NOT NULL,
  `ID_FILE` tinyint(3) unsigned NOT NULL,
  `USER_ID_FSE` int(11) NOT NULL,
  `ID_LINE` int(10) unsigned NOT NULL,
  `NUM_BLV` varchar(12) NOT NULL,
  `RFA_RATE_NEGO` float(10,2) DEFAULT NULL,
  `RFA_AMOUNT_NEGO` float(10,2) DEFAULT NULL,
  `RFA_RATE_PS` float(10,2) DEFAULT NULL,
  `RFA_AMOUNT_PS` float(10,2) DEFAULT NULL,
  `CCU` float(10,2) DEFAULT NULL,
  `NEGO_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `PS_HT_AMOUNT` float(10,2) DEFAULT NULL,
  `PU_HT` float(10,2) DEFAULT NULL,
  `BLV_TOTAL_HT_AMOUNT` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`MONTH`,`ID_FILE`,`USER_ID_FSE`,`ID_LINE`,`NUM_BLV`),
  KEY `FK_PRIMARY_MONTH_ID_FILE_USER_ID_ID_LINE` (`MONTH`,`ID_FILE`,`USER_ID_FSE`,`ID_LINE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `c_user` ADD `ID_FORMAT` tinyint(3) unsigned DEFAULT NULL ;

INSERT INTO `c_batch` (
`prog_id` ,
`hour` ,
`day` ,
`month` ,
`year` ,
`class` ,
`params` ,
`email` ,
`comment` ,
`active`
)
VALUES (
'10', '*', '*', '*', '*', 'BatchPadocSuiviFactMail', '*', '*' , 'Envoie Email pour suivi facturation', 'TRUE'
);































ALTER TABLE `padoc_dev`.`SUIVI_FACT_FILE_FSE_BLV` 
ADD COLUMN `CCU` FLOAT(10,2) NULL DEFAULT NULL  AFTER `CA_TOTAL_HT`;
