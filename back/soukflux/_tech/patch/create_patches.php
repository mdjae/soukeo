﻿<?php 
if (count($argv) < 1)
{
	echo ("L'outil de création des patchs doit être exécuté en ligne de commande uniquement");
	exit;
}
require '../../config/config.inc.php';
require '../../inc/libs/global.inc.php';

echo "\n";
echo ("Génération des patchs") . "\n";

// Liste des fichiers sql de mise à niveau
$path = "../sql/dev/";
$dirs = scandir($path);
$old_version = "";
$new_version = "";
$file = "";
$file_content = "";
$query_list = array();
foreach($dirs as $d)
{
	//echo "Fichier " . $d . "\n";
	if ($d != ".." && $d != "." && $d != ".svn")
	{
		//echo $d . "\n";
		$tab = preg_split("/[_-]/", $d);
		$my_old_version = $tab[1];
		$my_new_version = $tab[2];
		
		if ($old_version == "" && $new_version == "")
		{
			$old_version = $my_old_version;
			$new_version = $my_new_version;
		}
		
		if ($my_old_version != $old_version || $my_new_version != $new_version)
		{	
			// Nouveau contenu
			$file_content = "<?php \n";
			$sov = str_replace(".", "", $old_version);
			$snv = str_replace(".", "", $new_version);
			$file_content .= "class Patch_" . $sov . "_" . $snv . "\n";
			$file_content .= "{\n";
			$file_content .= "\tpublic static function upgrade()\n";
			$file_content .= "\t{\n";
			$file_content .= "\t\t" . '$back = true;' . "\n";
			$file_content .= "\t\t// Upgrade\n";
			
			// Ecriture des requetes d'update
			$i = 0;
			foreach($query_list as $q)
			{
				$i++;
				$q = str_replace("\n", "\n\t\t\t", $q);
				$file_content .= "\t\t//Requete " . $i . "\n";;
				$file_content .= "\t\t" . '$query = "' . $q . '"' . ";\n";
				$file_content .= "\t\t" . 'if (!dbQuery($query))' . "\n";
				$file_content .= "\t\t" . '{' . "\n";
				$file_content .= "\t\t\t" . '$back = false;' . "\n";
				$file_content .= "\t\t\t" . 'echo "Erreur à l\'éxécution de la requêtes : " . $query;' . "\n";
				$file_content .= "\t\t" . '}' . "\n\n";
			}
			
			$file_content .= "\t\t" . 'return $back;' . "\n";
			$file_content .= "\t}\n";
			$file_content .= "\n";
			$file_content .= "\tpublic static function downgrade()\n";
			$file_content .= "\t{\n";
			$file_content .= "\t\t// Downgrade\n";
			$file_content .= "\t}\n";
			$file_content .= "}\n";
			$file_content .= "?>";
			
			// On doit écrire le fichier
			$file = "Patch_" . $old_version . "_" . $new_version . ".class.php";
			file_put_contents($file, $file_content);
			echo sprintf(("Ecriture du patch %s"), $file) . "\n";
			
			$query_list = array();
			
			// Nouveau patch
			$old_version = $my_old_version;
			$new_version = $my_new_version;
		}
		
		// Sinon on doit traiter le fichier
		$content = file_get_contents($path . "/" . $d);
		
		$queries = explode(";", $content);
		reset($queries);
		
		//foreach($queries as $query);
		for($i = 0; $i < count($queries); $i++)
		{
			$query = trim($queries[$i]);
			if ($query != "") $query_list[] = $query;
		}
		
		//print_r(explode(";", $content));
		//print_r($query_list);
	}
}

// Nouveau contenu
$file_content = "<?php \n";
$sov = str_replace(".", "", $old_version);
$snv = str_replace(".", "", $new_version);
$file_content .= "class Patch_" . $sov . "_" . $snv . "\n";
$file_content .= "{\n";
$file_content .= "\tpublic static function upgrade()\n";
$file_content .= "\t{\n";
$file_content .= "\t\t" . '$back = true;' . "\n";
$file_content .= "\t\t// Upgrade\n";

// Ecriture des requetes d'update
$i = 0;
foreach($query_list as $q)
{
	$i++;
	$q = str_replace("\n", "\n\t\t\t", $q);
	$file_content .= "\t\t//Requete " . $i . "\n";
	$file_content .= "\t\t" . '$query = "' . $q . '"' . ";\n";
	$file_content .= "\t\t" . 'if (!dbQuery($query))' . "\n";
	$file_content .= "\t\t" . '{' . "\n";
	$file_content .= "\t\t\t" . '$back = false;' . "\n";
	$file_content .= "\t\t\t" . 'echo "Erreur à l\'éxécution de la requêtes : " . $query;' . "\n";
	$file_content .= "\t\t" . '}' . "\n\n";

}

$file_content .= "\t\t" . 'return $back;' . "\n";
$file_content .= "\t}\n";
$file_content .= "\n";
$file_content .= "\tpublic static function downgrade()\n";
$file_content .= "\t{\n";
$file_content .= "\t\t// Downgrade\n";
$file_content .= "\t}\n";
$file_content .= "}\n";
$file_content .= "?>";

// On doit écrire le fichier
$sov = str_replace(".", "", $old_version);
$snv = str_replace(".", "", $new_version);
$file = "Patch_" . $old_version . "_" . $new_version . ".class.php";
file_put_contents($file, $file_content);
echo sprintf(("Ecriture du patch %s"), $file) . "\n";
?>
