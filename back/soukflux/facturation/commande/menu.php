<?php

$vendorfilter = new FilterList("vendorfilter", ("Choix du vendeur :"), "all");
$sql = "SELECT DISTINCT po.vendor_id, v.VENDOR_NOM FROM  skf_po po INNER JOIN sfk_vendor v on po.vendor_id = v.VENDOR_ID ORDER BY VENDOR_NOM ASC";
$rs = dbQueryAll($sql);  
$vendorfilter->addElement('all','all');

foreach( $rs as $R){
    $vendorfilter->addElement($R['vendor_id'],$R['VENDOR_NOM'] );
}

// RECHERCHE FILTER CATEGORY
$statusfilter = new FilterList("statusfilter", ("Choix du statut :"), "all");
$sql = " SELECT DISTINCT order_status FROM  skf_orders ORDER BY order_status ASC";
$rs = dbQueryAll($sql);  
$statusfilter->addElement('all','all');

foreach( $rs as $R){
    $statusfilter->addElement($R['order_status'],$R['order_status'] );
}


// RECHERHCE PAR num commande
$numCommande  = new FilterText("numfilter", ("N° de commande")." :", "", false, 12);
 
//Litiges
$litige = new FilterList("litigefilter", ("Commande en litige :"),"all");
$litige->addElement('1','litige');
$litige->addElement('0','pas litige');
$litige->addElement('all','all');

//Date
$date = new FilterList("datefilter", ("Date :"),"2");
$date->addElement('2','Récent');
$date->addElement('3','3 derniers jours');
$date->addElement('7','7 derniers jours');
$date->addElement('all','Depuis le début');



$filter = new Filter("commande");
$filter->addFilter($vendorfilter);
$filter->addFilter($statusfilter);
$filter->addFilter($numCommande);
$filter->addFilter($litige);
$filter->addFilter($date);

$filter->setGridToReload("commande");
drawRMenuHeader(("Options de filtrage"),"FILTER","80");
echo $filter->display();
drawRMenuFooter();
// Action
  $act_array = array(

          array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "check",
                "name" =>("Tout sélectionner"),
                "url" => "javascript: selectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          ),
          array
          (
                "access" => "qualification.venteavahis..",
                "picto" => "stop",
                "name" =>("Tout désélectionner"),
                "url" => "javascript: deSelectAllSession();",
                "width" => 1,
                "height" => 1,
                "popup" => ""
          )
  );
  echo drawRMenuAction2(("Actions"),$act_array);
  
//legende
 $expl_array = array(
				array("facturation.commande..","","denied",("Déclarer un litige")),
				array("facturation.commande..","","check",("Annuler un litige")), 				
          	   ); 
echo drawRMenuExplanation(("Légende"),$expl_array);
?>
