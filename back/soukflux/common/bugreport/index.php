﻿<?php 
$root_path = "../../";
$ACCESS_key = "common.login..";
require $root_path.'inc/popup.header.inc.php';
// Est-ce qu'on part sur du Redmine ou du Mantis
if (SystemParams::getParam("bugreport*system") == "mantis")
{
	// Controle de la présence de l'extension PHP SOAP
	if (class_exists("SoapClient"))	
	{
		$f = new FormBugReportMantis();
		echo $f->display();
	}
	else echo "<p>" . ("L'extension PHP Soap n'est pas installée, impossible de charger le formulaire.") . "</p>";
}
else if (SystemParams::getParam("bugreport*system") == "redmine")
{
	$f = new FormBugReportRedmine();
	echo $f->display();
}
require $root_path . "inc/popup.footer.inc.php";
?>