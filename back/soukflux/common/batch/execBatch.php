<?php 
// Le premier param�tre correspond � la class
if (count($argv) <= 1)
{
	echo "Aucun batch à exécuter...";
	exit;
}

$script = $argv[0];
echo "script : " . $script . "\n";
$last = strrpos($script, "/");
$main_path = substr($script, 0, $last + 1);
echo "main_path : " . $main_path . "\n";

$root_path = "../../";

// Fichier de config
// Si on passe un vhost, il faut charger le bon fichier de config
//if ($argv[2] != "")
//{
//        require $main_path."/".$root_path.'config/' . $argv[2] . '.config.inc.php';
//}
//else
//{
        require $main_path."/".$root_path.'config/config.inc.php';
//}
require $main_path."/".$root_path.'inc/libs/global.inc.php';
require $main_path."/".$root_path.'inc/libs/db.mysql.inc.php';
$offipse_conn = dbConnect();

// Chargement des classes de Batch
if ($handle = opendir($main_path."/".$root_path.'/inc/class/batchs'))
{	
    while (false !== ($file = readdir($handle)))
    {
    	if ($file != "." && $file != ".." && $file != ".svn" && $file != "archive")
    	{
		//echo "Chargement de la classe " . $file . "\n";
		require_once $main_path."/".$root_path."/inc/class/batchs/Batch.class.php";
    		require_once $main_path."/".$root_path."/inc/class/batchs/".$file;
    	}
    }
}

// Programmation � executer
$prog_id = $argv[1];
$query = "SELECT class FROM c_batch WHERE prog_id = " . $prog_id;
$res = dbQueryOne($query);

$class = $res['class'];

echo "Class à exécuter : " . $class . "\n\n";

$batch = new $class($prog_id);

if($argv[2] == "help"){
    echo $batch->help(); 
}else{
    
    if(count($argv) > 2){
        $batch->work($argv[2], $argv[3], $argv[4], $argv[5]);            
    }else{
        $batch->work(); 
    }    
}


?>