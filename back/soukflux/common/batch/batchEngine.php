<?php 
$script = $argv[0];
$last = strrpos($script, "\\");
$main_path = substr($script, 0, $last + 1);
//echo "toto " . $last . " " . $main_path . "\n";

$root_path = "../../";
require $main_path."/".$root_path.'inc/config.inc.php';
require $main_path."/".$root_path.'inc/libs/global.inc.php';
require $main_path."/".$root_path.'inc/libs/db.inc.php';
require $main_path."/".$root_path.'inc/libs/batchs/Batch.class.php';

$offipse_conn = dbConnect();

// R�cup�ration des programmations
$query = "SELECT prog_id, hour, day, month, year,class
			FROM c_batch
			WHERE active = 'TRUE'";

$res = dbQueryAll($query);



// Boucle sur les programmations
foreach($res as $prog)
{
	if (Batch::needBatchExecution($prog['prog_id'], $prog['year'], $prog['month'], $prog['day'], $prog['hour']))
	{
		// Besoin d'une execution
		echo $prog['class'] . " : exécution\n";
		// Chemin vers PHP
		$php_path = SystemParams::getParam('system*php_cli_path');
  
		// Execution
		$cmd = '"' . $php_path . '" ' . $main_path . "/execBatch.php " . $prog['prog_id'];
		//echo $cmd;
		passthru($cmd);
		//passthru("php execBatch.php " . $prog['prog_id']);
	}
	else
	{
		// Rien � faire pour ce batch
		echo $prog['class'] . " : pas d'exécution\n";
	}
}


?>