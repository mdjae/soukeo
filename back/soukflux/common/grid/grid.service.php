<?php 
$root_path = "../../";
if( isset($_GET['path'])) $real_path_pop = $_GET['path'];
require $root_path.'inc/grid.header.inc.php';
// Quel fonction est appel�e ?
if (array_key_exists('function',$_GET) && array_key_exists('name',$_GET))
{
	// R�cup�ration du tableau
	$grid = Grid::getObj($_GET['name']);
	// La cl� de fonction existe
	// Quelle fonction ?
	switch ($_GET['function'])
	{
		case 'nextPage':
			$grid->nextPage();
			$output = $grid->display();
		break;
		case 'lastPage':
			$grid->lastPage();
			$output = $grid->display();
		break;
		case 'previousPage':
			$grid->previousPage();
			$output = $grid->display();
		break;
		case 'firstPage':
			$grid->firstPage();
			$output = $grid->display();
		break;
		case 'sort':
			$grid->gridSort($_GET['col'],$_GET['sort']);
			$output = $grid->display();
		break;
		case 'reload':
			$grid->reload();
			$output = $grid->display();
		break;
		case 'delete':
			$grid->delete($_GET['id1'], $_GET['id2'], $_GET['id3']);
			$output = "true";
		break;
		
	}
	
	echo SystemCharacterSet::dealWithAjaxString($output);
}
else if (array_key_exists('function', $_POST) && array_key_exists('name', $_POST))
{
	// Récupération du tableau
	//var_dump($_POST['name']);
	//var_dump($_POST['function']);
	$grid = Grid::getObj($_POST['name']);
	
	switch ($_POST['function'])
	{
		case 'update':
			$grid->update($_POST);
			$output = $grid->display();
		break;
	}
	//echo htmlentities($output, null, 'utf-8');
	echo SystemCharacterSet::dealWithAjaxString($output);
}
else
{
	//print_r($_POST);
	echo "false";
}

require   "../../inc/grid.footer.inc.php";
?>
