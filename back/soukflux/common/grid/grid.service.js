/*
 * Page suivante
*/
function nextPage(name, root_path)
{
	//alert(name);
	roar.alert('Donn�es','Page suivante');
	var myRequest = new Request.HTML({method: 'get', url: root_path + "/common/grid/grid.service.php?path="+root_path,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//alert(txt);
			document.getElementById('grid_' + name).innerHTML = responseHTML;
			//document.getElementById('grid_' + name).set('html',responseHTML);
			$exec(responseJavaScript);
		},
		onFailure: function(){
			alert('Erreur dans la requéte Ajax...');
		}
	});
	
	myRequest.send("function=nextPage&name=" + name);
}

/*
 * Derni�re page
*/
function lastPage(name,root_path)
{
	//alert(name);
	roar.alert('Donn�es','Derni�re page');
	var myRequest = new Request.HTML({method: 'get', url: root_path + "/common/grid/grid.service.php?path="+root_path,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//alert(txt);
			document.getElementById('grid_' + name).innerHTML = responseHTML;
			//document.getElementById('grid_' + name).set('html',responseHTML);
			$exec(responseJavaScript);
		},
		onFailure: function(){
			alert('Erreur dans la requ�te Ajax...');
		}
	});
	
	myRequest.send("function=lastPage&name=" + name);
}

/*
 * Page pr�c�dente
*/
function previousPage(name,root_path)
{
	//alert(name);
	roar.alert('Donn�es','Page pr�c�dente');
	var myRequest = new Request.HTML({method: 'get', url: root_path + "/common/grid/grid.service.php?path="+root_path,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//alert(txt);
			document.getElementById('grid_' + name).innerHTML = responseHTML;
			//document.getElementById('grid_' + name).set('html',responseHTML);
			$exec(responseJavaScript);
		},
		onFailure: function(){
			alert('Erreur dans la requ�te Ajax...');
		}
	});
	
	myRequest.send("function=previousPage&name=" + name);
}

/*
 * Premi�re page
*/
function firstPage(name,root_path)
{
	//alert(name);
	roar.alert('Donn�es','Premi�re page');
	var myRequest = new Request.HTML({method: 'get', url: root_path + "/common/grid/grid.service.php?path="+root_path,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//alert(txt);
			document.getElementById('grid_' + name).innerHTML = responseHTML;
			//document.getElementById('grid_' + name).set('html',responseHTML);
			$exec(responseJavaScript);
		},
		onFailure: function(){
			alert('Erreur dans la requ�te Ajax...');
		}
	});
	
	myRequest.send("function=firstPage&name=" + name);
}

function gridSort(name, col, sort, root_path)
{
	//alert(name);
	roar.alert('Donn�es','Tri');
	var myRequest = new Request.HTML({method: 'get', url: root_path + "/common/grid/grid.service.php?path="+root_path,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//alert(txt);
			document.getElementById('grid_' + name).innerHTML = responseHTML;
			//document.getElementById('grid_' + name).set('html',responseHTML);
			$exec(responseJavaScript);
		},
		onFailure: function(){
			alert('Erreur dans la requ�te Ajax...');
		}
	});
	
	myRequest.send("function=sort&name=" + name + "&col=" + col + "&sort=" + sort);
}

function gridReload(name, root_path)
{
	//alert(name + ' ' + local_root_path + "/common/grid/grid.service.php");
	roar.alert('Donn�es','Rechargement');
	var myRequest = new Request.HTML({method: 'get', url: root_path + "/common/grid/grid.service.php?path="+root_path,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			//alert(txt);
			//alert(name);
			document.getElementById('grid_' + name).innerHTML = responseHTML;
			//document.getElementById('grid_' + name).set('html',responseHTML);
			$exec(responseJavaScript);
		},
		onFailure: function(xhr){
			alert('Erreur dans la requ�te Ajax... ' + xhr.responseText) ;
		}
	});
	
	myRequest.send('function=reload&name=' + name);
}

function confirmDelete(message, id1, id2, id3, grid_name, root_path)
{
	new Windoo.Confirm("<h2 align='center'>" + message + "</h2>", {
		'window': {
			'resizable': false,
			'theme': Windoo.Themes.alphacube,
			'title': 'Demande de confirmation'
	}	,
	'onConfirm': function()
				{
					//alert('A faire!');
					var myRequest = new Request({
	        				method: 'get', 
	        				url: root_path + "/common/grid/grid.service.php?path="+root_path,
	        				onSuccess: function(txt){
	        						//alert(txt);
									//document.getElementById('grid_' + grid_name).innerHTML = txt;
									//gridJavascript();
        							gridReload(grid_name, root_path);
	        					},
	        				onFailure: function(txt){
	        						alert('Une erreur est survenue lors de la suppression : ' + txt.responseText);
	        					}
	        				});
    				
					myRequest.send('function=delete&name=' + grid_name + '&id1=' + id1 + '&id2=' + id2 + '&id3=' + id3);
				},
	'onCancel': function()
				{
					// Rien
				}
	});
}
