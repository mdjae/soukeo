<?php 
$root_path = "../../";
$ACCESS_key = "reporting.%..";
require $root_path.'inc/popup.header.inc.php';
?>
<script>
	window.print();
</script>
<?php 
// Quel report ?
if (array_key_exists('name', $_GET))
{
	// Récupération du rapport
	$report = Report::getObj($_GET['name']);
	echo $report->display();
}
require $root_path . "inc/popup.footer.inc.php";
?>