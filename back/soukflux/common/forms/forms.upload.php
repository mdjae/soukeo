<?php 
// Init
$root_path = "../../";
require $root_path.'inc/grid.header.inc.php';
require $inc_path.'libs/image.inc.php';

echo '<style type="text/css">';
require_once $root_path."skins/common/offipse.css";
require_once $corp_path."more.css";
echo '</style>';

// Récupération du formulaire
$form = Form::getObj($_GET['name']);

// Est-ce qu'on est en retour de post
if (isset($_POST['MAX_FILE_SIZE'])) $form->dealWithInputFileUpload($_GET['item']);

echo $form->displayInputFileUpload($_GET['item']);
?>