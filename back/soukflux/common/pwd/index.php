﻿<?php 
//$root_path = "../../";
$ACCESS_key = "common.pwd..";
$security_domain = "common";
$domain_categ = "pwd";
$right_menu_visibility = false;
$current_url = "common/pwd";

//$main_url = "../../".$root_path."/index.php?list";
//$main_url = "/index.php?list";
//var_dump($root_path) ;
//require $root_path.'inc/header.inc.php';

//////////////
// TEXT
$select_pwd_txt = ("Mot de passe actuel:");
$select_pwd1_txt = ("Votre nouveau Mot de passe:");
$select_pwd2_txt = ("Retapez votre nouveau Mot de passe:");
//

$txt_info = '';

// fonction permettant de mettre en rouge les champs mal saisis
function setRed($my_field)
{
	return "<font color=\"red\">".$my_field."</font>";
}

// ---------------------------------------------------------
// Teste les valeurs saisies dans le formulaire

if ( isset($_POST['spwd']) )
{

	// le formulaire a été validé
	$validated = 1;

	// current pwd
	$good_pwd = dbQueryOne("SELECT password
							FROM c_user where user_id='" . $_SESSION['smartuserid'] . "'");
	$good_pwd = $good_pwd["password"];
	
//* *** DEBUG
//	$out .= '1>>  '.$good_pwd;
//	$out .= '<br>2>>  '.md5($_POST['spwd']);
//*** 

	if ( isEmpty($_POST['spwd']) || ($good_pwd != md5($_POST['spwd'])) )
	{
		$validated = 0;
		$select_pwd_txt = setRed($select_pwd_txt);
		$txt_info = "<img src=\"".$skin_path."images/picto_warning.gif\" align=\"absmiddle\" border=\"0\"/>&nbsp;&nbsp;<b>". sprintf(("Veuillez saisir les champs demandés"), SystemParams::getParam('security*pwd_min'),SystemParams::getParam('security*pwd_max')) ."</b>";
	}
	else{	
		// new pwd
		if ( isEmpty($_POST['spwd1']) )
		{
			$validated = 0;
			$select_pwd1_txt = setRed($select_pwd1_txt);
			$txt_info = "<img src=\"".$skin_path."images/picto_warning.gif\" align=\"absmiddle\" border=\"0\"/>&nbsp;&nbsp;<b>". sprintf(("Veuillez saisir votre nouveau mot de passe"), SystemParams::getParam('security*pwd_min'),SystemParams::getParam('security*pwd_max')) ."</b>";
			
		}
		else if (strlen($_POST['spwd1']) < SystemParams::getParam('security*pwd_min') || strlen($_POST['spwd1']) > SystemParams::getParam('security*pwd_max') )
		{
			// Le mot de passe doit faire entre security*pwd_min et security*pwd_min
			$validated = 0;
			$select_pwd1_txt = setRed($select_pwd1_txt);
			$select_pwd2_txt = setRed($select_pwd2_txt);
			$txt_info = "<img src=\"".$skin_path."images/picto_warning.gif\" align=\"absmiddle\" border=\"0\"/>&nbsp;&nbsp;<b>". sprintf(("Le mot de passe doit faire entre %s et %s de long"), SystemParams::getParam('security*pwd_min'),SystemParams::getParam('security*pwd_max')) ."</b>";
		}
		else
		{
			if ( isEmpty($_POST['spwd2']) )
			{
				$validated = 0;
				$select_pwd2_txt = setRed($select_pwd2_txt);
				$txt_info = "<img src=\"".$skin_path."images/picto_warning.gif\" align=\"absmiddle\" border=\"0\"/>&nbsp;&nbsp;<b>". sprintf(("le champ \"Retapez votre mdp\" n'a pas été complété"), SystemParams::getParam('security*pwd_min'),SystemParams::getParam('security*pwd_max')) ."</b>";
				
			}
			else
			{
				if ($_POST['spwd1'] != $_POST['spwd2'])
				{
					$validated = 0;
					$select_pwd2_txt = setRed($select_pwd2_txt);
					$select_pwd1_txt = setRed($select_pwd1_txt);
					$txt_info = "<img src=\"".$skin_path."images/picto_warning.gif\" align=\"absmiddle\" border=\"0\"/>&nbsp;&nbsp;<b>". sprintf(("le champ \"Retapez votre mdp\" est incorrecte"), SystemParams::getParam('security*pwd_min'),SystemParams::getParam('security*pwd_max')) ."</b>";
	
				}
			}
		}
	}
}

if (isset($validated) && ($validated == 1))
{

// update
		// Db access
		dbQuery("UPDATE c_user
					SET password='". md5($_POST['spwd1']) ."', 
						locked='0' 
					WHERE user_id='" . $_SESSION['smartuserid'] . "'");
		
		// et dans MANTIS
		//dbQuery("update mantis_user_table set password='".md5(md5($spwd1))."' WHERE username='$smartlogin'");

		// màj de la variable de session
		$_SESSION['smartpwd'] = md5($_POST['spwd1']);

		// Recherche de l'email de l'utilisateur
		$query = "SELECT email, usercode
					FROM c_user
					WHERE user_id = '" . $_SESSION['smartuserid'] . "'";
		$res = dbQueryOne($query);
		$email = stripslashes($res['email']);
		$usercode = stripslashes($res['usercode']);
					
		// msg
		$txt_info = "<img src=\"".$skin_path."images/picto_warning.gif\" align=\"absmiddle\" border=\"0\"/>&nbsp;&nbsp;<b>" . ("Votre nouveau 'mot de passe' a été pris en compte.") . "</b><br/>";
}


$out .='
<table cellpadding="10" cellspacing="0" border="0" align="center" width="400">
	<tr>
		<td>
		
		
<form name="form_main" action="" method="POST">
';
  
    // - - - - BOX - - 
    $mybox = new UIBox("pwd_box",_("Mise à jour de votre mot de passe"),"tools","NoBg"); 
$out.= $mybox->drawBoxHeaderStatic(); 

$out.='
	<table cellpadding="2" cellspacing="2" border="0" width="450" align="center">
		<tr><td colspan="2">	
			<p>'.setRed($txt_info).'</p>
		</td>
		</tr>
		<tr class="boxcontent">
			<td align="left" colspan="1"><p><b>'.$select_pwd_txt.'</b></p></td>
			<td><p>
				<input type="password" name="spwd" value="" MAXLENGTH="16" size="30">
			</p></td>
		</tr>

		<tr class="boxcontent">
			<td align="left" colspan="1"><p><b>'.$select_pwd1_txt.'</b></p></td>
			<td><p>
				<input type="password" name="spwd1" value="" MAXLENGTH="16" size="30">
			</p></td>
		</tr>
		
		<tr class="boxcontent">
			<td align="left" colspan="1"><p><b>'.$select_pwd2_txt.'</b></p></td>
			<td><p>
				<input type="password" name="spwd2" value="" MAXLENGTH="16" size="30">
			</p></td>
		</tr>
		
		
		<tr>
			<td colspan="2" align="center" style="height: 80px">
				<input type="submit" class="btn btn-large btn-primary" value="'.("valider").'">
			</td>
		</tr>
	</table>

';
$out .= $mybox->drawBoxFooter();
$out .='
    

</form>

</td>
</tr>
</table>

<p>&nbsp;</p>
';

// -- FOOTER
//require $inc_path.'footer.inc.php';

