<?php
//var_dump($_SERVER);

require_once $_SERVER['DOCUMENT_ROOT'].'/thirdparty/swiftmail/lib/swift_required.php';
function sendEmail2User($your_login, $your_email, $your_pwd)
{
    global $txt_email_sender;
    $transport = Swift_SendmailTransport::newInstance('/usr/sbin/exim -bs');
    $mailer = Swift_Mailer::newInstance($transport);
    
    $mail = Swift_Message::newInstance() ;
    
    $mail->setSubject(_("Gestionnaire de terrain > Mot de passe"));
    $mail->setFrom( SystemParams::getParam('system*admin_email'));
    
    $mail->setTo($your_email);
    $msg  = _("Bonjour,")."\n";
    $msg .= _("Vous avez oublié votre mot de passe") . ".\n\n";
    $msg .= _("Identifiant :") . " " . $your_login . "\n";
    $msg .= _("Mot de passe :") . " " . $your_pwd . "\n";
    // Fin du message
    // Signature
    $msg .= "\n\n--\r\n";
    $msg .= _("L'équipe Motor Presse France") . "\n";

    $mail->setBody($msg);
    
    $mailer->send($mail);

}

switch ($_POST['act'])
{
    case 'sendPassword' :
        $root_path = '../../';
        require_once $root_path . 'inc/offipse.inc.php';
        $ACCESS_key = "common.login..";

        /////////////
        // TEXT
        $check_your_emails = _("Pensez à vérifier vos e-mails !");
        //   $txt_email_sender = SystemParams::getParam('system*admin_email');
        //

        // ------------------------------------

        $login = strtolower(trim($_POST['login']));

        $data = new BusinessSoukeoModel();
        $my_result = $data->getIdMailFromSITE($login);
        if (count($my_result) >= 1)
        {
            // okay
            $txt_msg = _("Un message contenant votre mot de passe vous a été envoyé.");
            // Send e-mail to user
            sendEmail2User($my_result["ID"], $my_result["VALEUR"], $my_result["PASSWORD"]);
        }
        else
        {
            // l'identifiant ou l'email n'existe pas!
            $txt_msg = "<b>" . $login . "</b><br>" . _("Le nom d'utilisateur (ou e-mail) saisi n'existe pas.");
            $check_your_emails = "";
        }
        //dbDisconnect($my_conn);
        unset($my_result);
        echo "
            <div align='center'>
            <p>&nbsp;</p>
            <p>  $txt_msg</p>
            <p>&nbsp;</p>
            <p>  $check_your_emails</p>
            <p>&nbsp;</p>
            </div>
            ";

        break;
}
