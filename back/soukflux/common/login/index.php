<?php 
if (session_id() == "") session_start();
session_destroy(); // clean the session
$root_path = '../../';
$ACCESS_key = "common.login..";
$security_domain = "common";
$domain_categ = "login";
// Si aucun fichier de configuration, on part vers l'installation
if (!file_exists($root_path . "/config/config.inc.php"))
{
	header ("location: ../../01install/checksrv/");
}

require_once $root_path .	'inc/offipse.inc.php';

$top_body = _("Soukeo");

if (!isset($_GET['err'])||(trim($_GET['err'])==''))
{
	$onload = "onload=\"document.sup_form.authid.focus();\"";
}
else
{
	$onload = "onload=\"document.sup_form.authpwd.focus();\"";
}

// Si logu�, redirige vers la derni�re page vue
if (array_key_exists('last_seen',$_SESSION))
{
	// Redirection vers la derni�re page
	$url = $root_path . $_SESSION['last_seen'];
	header ("location: ../" . $url);
}

require $root_path.'inc/popup.header.inc.php';
?>


 
    <div class="related jPanelMenu-panel login">
    	
    	
    	
    	
        <div class="t container">
        	
        	<div class="alert alert-light">
			<i class="fa fa-comment"></i> <?php echo _("Bienvenue sur l'interface d'administration Soukflux")?>
		</div>
        	
        	<div class="tl"> 
        		<div class="tr" >
			<div class="related_in">
				<div  class=" login" id="con_login_box">
				<?php /*	<p><?php echo _('Saisissez vos paramètres de connexion dans les champs ci-dessous puis validez' )?> :</p> */ ?>
					   
				<div class="span6">
   <p> <img src="../../skins/soukeo/logo.jpg" alt="soukeo"/></p>
</div>  
					  
						
						<form class="form-signin form-horizontal span6" name="sup_form" method="post" action="../login.access/index.php">
				
				
				<div class="top-bar">
					<h3><i class="fa fa-home fa-fw"></i><?php echo _('Authentification');?></h3>
				</div>
				
				
				<div class="well no-padding tablogin">
				
				 <?php if ($_GET['err']) : ?>
				 	<div class="control-group">
				<div class="alert alert-error">
					<i class="fa fa-warning"></i><?php echo _("Identifiant ou email inconnu."); ?>
				 </div>			
				 </div>
					<?php endif;	?>
				
				
				<div class="control-group">
           			<label class="control-label lab_log" for="authid"><i class="fa fa-user"></i></label>
            		<div class="controls">
             			<input type="text" id="authid" name="authid" placeholder="<?php echo _("Identifiant") ?>">
            		</div>
          		</div>
				
				
				<div class="control-group">
           			<label class="control-label lab_log" for="authpwd"><i class="fa fa-key"></i></label>
            		<div class="controls">
             			<input type="password" id="authpwd" name="authpwd" placeholder="<?php echo _("Mot de passe")?>">
            		</div>
          		</div>
				
				<input type = "hidden" id="referer_link" name="referer_link" value="<?php echo $_SERVER['HTTP_REFERER'] ?>"/>		
							<?php /*	<label class="lab_log" for="authid"><?php echo _("Identifiant") ?> :</label>
								<input size="25" id="authid" name="authid" type="text"/>
							
								<label class="lab_log" for="authpwd"><?php echo _("Mot de passe")?> :</label>
								<input size="25" id="authpwd" name="authpwd" type="password"/> */ ?>
								
					<div class="padding">
         				<button class="btn btn-primary" type="submit" ><?php echo _("Connexion")?></button><br><br>
          				<a href="#" onclick="$('#dialog' ).dialog({ width: 500, title:'Mot de passe' });"><?php echo _("Identifiant / Mot de passe oublié")?> ?</a>
          			</div>
					
								
                                  <?php /*  <p><a href="#" onclick="$('#dialog' ).dialog({ width: 500, title:'Mot de passe' });"><?php echo _("Identifiant / Mot de passe oublié")?> ?</a></p>
                               
									<p>
									<input type="submit" value="<?php echo _("Connexion")?>" alt="Accéder au portail..." align="middle" class="but1"/>
									</p>	
								*/ ?>
							
						
						
					</div>	
					
						</form>

						 
					</div>
				</div></div>
			</div></div>
			
			
			 <?php
			
           //  if (is_file($_SERVER['DOCUMENT_ROOT']."/static/legal-".strtoupper(SystemLang::getUrlCampLang()).".htm")){
           //      $legal= "legal-".strtoupper(SystemLang::getUrlCampLang()).".htm";
           //  }else {
           //      $legal = "legal-EN.htm";
           //  }
             
           //  if (is_file($_SERVER['DOCUMENT_ROOT']."/static/cgu-".strtoupper(SystemLang::getUrlCampLang()).".htm")){
           //     $cgu = "cgu-".strtoupper(SystemLang::getUrlCampLang()).".htm";
           //  }else {
           //      $cgu = "cgu-EN.htm";
           //  }
             ?>
       <!--     <a href="#" onclick="showDialog('<?php echo $legal ?>');return false;" ><?php echo _("Mentions légales ") ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" onclick="showDialog('<?php echo $cgu ?>');return false;" ><?php echo _("Conditions générales d'utilisation ") ?></a> 
            -->
		</div>
		
	
	

<div id="dialog" title="Basic dialog" style="display:none">
    <p><?php  echo _("Entrer votre identifiant ou votre e-mail dans le champs ci-dessous.") ?><br/>
       <?php  echo _("Votre mot de passe vous sera envoyé par e-mail.") ?>
    </p> </p>&nbsp;</p>
    <div align="center" id="myrestore">
    <form action="" name="form_pwd" method="post" onsubmit="sendPassword(this);return false">
    <p><b><?php  echo _("Identifiant") ?> :</b>
    &nbsp;&nbsp;<input type="text" value="" name="your_login" id="your_login" size="15">
    &nbsp;&nbsp;<input type="image" src="<?php  echo $skin_path ?>images/button_submit.gif" align="absmiddle" alt="<?php  echo _("Valider") ?>" border="0"></p>
    <p><i><?php  echo _("(ou votre e-mail)") ?></i></p>
    </form>
    <div id="response"> &nbsp;</div>
    </div>
</div>

<div id="dialog2" title="">

</div>

<script>
    function sendPassword(form){
    if ( $('#your_login').val() ==''){ alert("<?php echo _('veuillez saisir votre identifiant ou votre email') ?>");return false};
    
    var theData = { 
            act   : 'sendPassword',
            login : $('#your_login').val(),
           };
    
    jQuery.ajax({
            type : 'POST',
            url: "dispatcher.php",
            data: theData,
            success: function(data){
                $('#response').html(data);
            }
    })
    
    }
    
    function showDialog(url) {
    jQuery.ajax({
        type : "GET",
        data : {act : "static"},
        url : "../../static/"+url , 
        success : function(data){
            $('#dialog2').html(data);
        }
        
    });
    

    $("#dialog2").dialog("open");
    return false;
    }
    
    $(document).ready(function() {
    $("#dialog2").dialog({
        autoOpen : false,
        modal : true,
        height : 600,
        width : 800
    });
    });
</script>
