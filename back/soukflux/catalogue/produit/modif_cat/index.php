<script type='text/javascript' charset='utf-8'>
    
        $('#checkboxtree').checkboxTree({
            onCheck: {
                ancestors: 'check',
                descendants: 'uncheck',
                others: 'uncheck'
            },
            onUncheck: {
                descendants: 'uncheck'
            }
        });
        
	$('#selectbox-popup').select2({
		minimumInputLength: 3,
		ajax: {
			url: '/app.php/getlistcateg',
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
	});	

</script>
<?php
 $typev = "ecom";

$class = "BusinessSoukeoModel";
$db = new $class();

$view = new BusinessView();
$skf = new BusinessSoukeoModel();
$data = $skf->getSsCatByParentId();
//echo $_POST['id'] ;
//var_dump($_POST);
//$categ = str_replace("-", " > ", $_POST['cat_ecom']);
//$categ = str_replace("+", " ", $categ);
$prod = $skf -> getProduct($_POST['id']);
$categ = $skf -> getStrCateg($prod['categories']); 

//On prend le premier produit pour exemple au hasard

//var_dump($prod);
$_SESSION[$typev]['prodToAdd'] = $prod;
//Si la catégorie a déja été associée
if ($prod['categories']){
	$categ_av_assoc = $skf -> getStrCateg($prod['categories']);
	$categ_av_assoc = substr($categ_av_assoc, 0, strlen($categ_av_assoc)-2);
}

$out = '';
$out .=

"<h1 id=''>Changement de catégorie</h1>
<span id='id_produit' style='visibility:hidden'>".$prod['id_produit']."</span> 
<p class='alert'>Produit : <b><span id='catEcom' class='label label-info'>".$prod['name']."</span></b> </p> ";
	if(isset($categ_av_assoc) && !empty($categ_av_assoc)){ 
		$out.="<p id='oldAssoc' class='alert alert-info'><em>Déja ssocié à : </em> <span class='label label-info'>".$categ_av_assoc."</span></p>";
} 
//"<p id='success'>Ré-associé avec : </p>
$out.=	
"
<div class='treeviewcheck'>
	<label hidden for='submit' class='btn btn-primary'>Valider</label><input hidden type='submit' name='submit' value='VALIDER' id='submit' onclick='validModifCateg();'/>
	
    <input type='hidden' name='optionvalue' id='selectbox-popup' class='input-xlarge' data-placeholder='Choisissez une catégorie' onchange='validSelect2Popup()' style='display: inline-block; '/>
	
	<fieldset id='' class='' style='margin-top: 20px;'>	
		<div class='top-bar'><h3>Catégories</h3></div>
		<div class='well'>
		".$view -> showTreeCheckbox($data, true, 0)."
		</div>
	</fieldset>
	<label hidden for='submit' class='btn btn-primary'>Valider</label><input hidden type='submit' name='submit' value='VALIDER' id='submit' onclick='validModifCateg();'/>
</div>

";

echo $out ;
