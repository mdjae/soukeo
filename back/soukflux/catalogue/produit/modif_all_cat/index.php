
<?php
 $typev = "avaGP";

$class = "BusinessSoukeoModel";
$db = new $class();

$view = new BusinessView();
$skf = new BusinessSoukeoModel();
$data = $skf->getSsCatByParentId();
$prod = $skf -> getProduct($_POST['id']);
$categ = $skf -> getStrCateg($prod['categories']); 

$_SESSION[$typev]['prodToAdd'] = $prod;
if ($prod['categories']){
	$categ_av_assoc = $skf -> getStrCateg($prod['categories']);
	$categ_av_assoc = substr($categ_av_assoc, 0, strlen($categ_av_assoc)-2);
}
$selectProd = $_SESSION[$typev]['selectedProds'] ;
$out = ''; ?>

    <script type='text/javascript' charset='utf-8'>
    
        $('#checkboxtree').checkboxTree({
            onCheck: {
                ancestors: 'check',
                descendants: 'uncheck',
                others: 'uncheck'
            },
            onUncheck: {
                descendants: 'uncheck'
            }
        });
        
    $('#selectbox-popup').select2({
        minimumInputLength: 3,
        ajax: {
            url: '/app.php/getlistcateg',
            dataType: 'json',
            data: function (search, page) {
                return {
                        label: search
                        };
                },
                results: function (data, page) {
                    return { results: data };
                }
        }
    }); 

</script>

<div id="myModalModifAllCat" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    
    <div class="modal-header navbar">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modification de catégorie</h3>
                
    </div> <!-- modal-header -->
  
    <div id ="dialogAssocCateg" class="modal-body">
        
        <div class='treeviewcheck'>
            Choisissez une nouvelle catégorie pour ces produits : <br /><br />
                <input type='button' class="btn btn-primary" href='#' onclick='validModifAllCateg();' value='Valider'></input>
        		<input type='hidden' name='optionvalue' id='selectbox-popup' class='input-xlarge' data-placeholder='Choisissez une catégorie' onchange='validSelect2Popup()'/>
        	    <div class="top-bar" style="margin-top: 15px;">
        	        <h3>Catégories</h3>
        	    </div>
        	    <div class="well">   
        		<?php echo $view -> showTreeCheckbox($data, true, 0) ?>
                </div>
        </div>
        
       
        	<input type='button' class="btn btn-primary" href='#' onclick='validModifAllCateg();' value='Valider'></input>
        </div>
    </div>


