<?php 
//$root_path = "../../../";
$top_body = ('Fiche Produit');

$skf = new BusinessSoukeoModel();
$tmp = $skf -> getProduct($_POST['prod_id']);
$prod =$tmp;
unset( $_POST['ENREGISTRER'] ) ;
$prod['categories_id'] = $prod['categories'] ;
$prod['categories'] = $skf -> getStrCateg($prod['categories']); // recherche le label ainsi que sa hiérarchisation
$prod['categories'] = substr($prod['categories'], 0, strlen($prod['categories'])-2); 
//ksort ($prod) ; // trie sur l'index
$out = "" ;
$listVendeur= "";

$vendeur = $skf ->getAllVendorAssoc($prod['id_produit']);
$nbVendeur = 0;
$typeVendor = "ecommercant";
$listVendeur .="<table class='data-table dataTable'>
				<thead>
					<th>Nom vendeur </th>	
					<th>Nom du produit</th>
					<th>Quantité (S/P)</th>
				</thead>
				" ;
foreach ($vendeur as $row) {
	$stockPrix = $skf -> getStockPriceEcomGP($prod['sku'], $row['vendor_id']);
	$infoVendeur = $skf -> getInfoTracking($row['vendor_id']);	
	//recherche de la catégorie du produit chez l'e-commerçant
	$class = "BusinessSoukeo".$infoVendeur['SOFTWARE']."Model";
	$db = new $class();
	$produitEcom = $db -> getProduct($row['vendor_id'],$row['id_prod_com']);

	$arr = array('url' 			=> '/qualification/'.$typeVendor.'/popup_assoc/index.php', 
						 'vendor_id' 	=> $row['vendor_id'],
						 'tech' 		=> $infoVendeur['SOFTWARE'],
						 'prod_id' 		=> $row['id_prod_com'],
						 'cat_ecom'     => str_replace(' ', '+',str_replace(' > ','-',$produitEcom['0']['CATEGORY'] )));
	$data = json_encode($arr);
	$action = "showDialogEcom('".$data."')";
	
	$listVendeur .= "<tr>
						<td>".$row['vendor_nom']."</td>
						<td><a href='#' onclick=".$action.">".$produitEcom['0']['NAME_PRODUCT']."</a></td>
						<td>".$stockPrix['0']['QUANTITY']."</td>
						
					</tr>	
					" ; 
}
//-------------------------------------- pour l'ecom soukeo 
$stockPrixSoukeo = $skf -> getStockPriceEcomGP($prod['sku'], '24');
//recherche de la catégorie du produit chez l'e-commerçant
$class = "BusinessSoukeoGrossisteModel";
$db = new $class();
$produitEcom = $db -> getProductByEAN('',$prod['sku']);
$arr = array('url' 	=> '/qualification/grossiste/popup_assoc/index.php', 
					 'vendor_id' 	=>$produitEcom['0']['GROSSISTE_ID'],
					 'tech' 		=> 'Carshop',
					 'prod_id' 		=> $produitEcom['0']['ID_PRODUCT'],
					 'cat_ecom'     => str_replace(' ', '+',str_replace(' > ','-',$produitEcom['0']['CATEGORY'] )));
$data = json_encode($arr);
$action = "showDialogEcom('".$data."')";	
if ($produitEcom!=null){
	$listVendeur .= "<tr>
						<td>Soukeo</td>
						<td><a href='#' onclick=".$action.">".$produitEcom['0']['NAME_PRODUCT']."</a></td>
						<td>".$stockPrixSoukeo['0']['QUANTITY']."</td>
					</tr>	
					" ; 
}
//-------------------------------------- fin
$listVendeur .="</table>"	
?>



<?php


//---------------- Commmence le formulaire -------------------//

$out .=			
//"<form method='post' name='editProd' action =''>
"		<h1><a href='/app.php/catalogue/produit/fiche?id=".$prod['id_produit']."' target='blank'>".$prod['name']."</a></h1>";
/*$out .= "<ul class='thumbnails'>
            <li class='span4'>
                <div class='thumbnail'>
                    <a href='".$prod['image']."' class='thumbnail'><img src='".$prod['image']."' alt='' style='width: 360px; height: 270px;'></a>
                    <div class='caption'>
                        Image
                    </div>
                </div>
            </li>
            <li class='span3'>
                <div class='thumbnail'>
                    <a href='".$prod['small_image']."' class='thumbnail'><img src='".$prod['small_image']."' alt='' style='max-width: 120px; max-height: 120px;'></a>
                </div>
            </li>
            <li class='span3'>
                <div class='thumbnail'>
                    <a href='".$prod['thumbnail']."' class='thumbnail'><img src='".$prod['thumbnail']."' alt='' style='max-width: 120px; max-height: 120px;'></a>
                </div>
            </li>
         </ul>"; */
$out .=	"<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\" align=\"center\" width=\"100%\">";
// sert a voir un visuel sur qu'elle produit on edit ainsi que les vendeurs qui vende se produit.
$out .= "<tr>
            
			<td class='thumbnail' width='180px'><img width='200' height='200' src=".$prod['image']." /></td>
			<td width='80%'><span class='badge'>Associé/Vendue par :</span>".$listVendeur."</td>
		</tr> ";
$out .= '<tr>
				<td align="center" colspan="2">
				<input type = "button" href="#" onclick="validEditProd();" value = "Sauvegarder l\'édition de produit" class="validEdit  btn btn-large btn-primary"></a>
				</td>
		</tr>
		</table>
		
		<form class="form-horizontal">';			
/*
* affiche tous les données de la table sfk_catalog_product avec categorie en référence avec son label et sa hiérarchisation
*
*/
  	foreach ($prod as $row => $value) {
  	    
        $out.= "<div class='control-group'>";
		//var_dump($value);	
		/* on filtre les données pour évité d'avoir des bugs d'affichage notamment dans la description dans l'input sauf pour description html */
		$row = filtre_string($row);
		if($row !='description_html' && $row !='description')$value =filtre_string($value); 
		$readOnly =""; //réinitialise le paramétre
		if (($row =='categories')||($row =='categories_id')||($row =='date_update')||($row =='date_create')||($row =='id_produit')||($row =='delete')){
			$readOnly='disabled'; // on définit ce qui serat bloqué a l'édition
		}
		else{
			$readOnly =""; //réinitialise le paramétre
		}
		// si c'est un champ longt comme description :
		if ($row =='description'||$row =='description_html'){
			$out .= 		
			"<label class='control-label' for='$row'><b>".$row." : </b></label>
			<div class='controls'>
			     <textarea ".$readOnly." rows='8' cols='96' name='".$row."' id='".$row."'>". $value ."</textarea>
			</div>";
		}
		else{
			$out .=		
			"<label class='control-label' for='$row'><b>".$row." : </b></label>
			 <div class='controls'>
                <input ".$readOnly." size=\"100\" maxlength='255'type='text' name='".$row."' id='".$row."' value='". $value ."'>
			 </div>";
		}
        
        $out.= "</div>";
		
	}
//---------------------- Titre des champs dynamiques ------------------------------//
$out .=
        "<h5>CHAMPS DYNAMIQUES</h5>";
$attr = $skf->getAllAttrFromProduct($prod['id_produit'], $prod['VENDOR_ID']); // on récupére tous les champs dynanique numéro et label
/*
 * on affiche tous ce qu'il y a comme données dynamique 
 */
		foreach ($attr as $attribut) {
$out .= "<div class='control-group'>";
$out .=	"<label class='control-label' for='".$attribut['LABEL_ATTR']."'><b>".$attribut['LABEL_ATTR']." : </b></label>
         <div class='controls'>   
			
			<input size='100' maxlength='255' type='text' name='".htmlentities($attribut['LABEL_ATTR'])."'  id='". $attribut['ID_ATTRIBUT']."' value='". htmlentities($attribut['VALUE']) ."'>
		</div>";
$out .= "</div>";
	}
//-------------------- Boutton de validation ---------------------------------------------//

$out .= "<button onclick='validEditProd(); return false;' class='validEdit btn btn-large btn-primary'> Sauvegarder l\'édition de produit </button>";

$out .= "</form>";
		

//</form>';	
echo $out ;	
// addAttributProduct
//addRowValAttr
/**
 * F Enléve les caractéres \n\r les balises et encode en UTF8
 * @param string a filtré
 * @return string filtré
 */
function filtre_string($string){
		$stringFiltrer = str_replace(array("\r\n","\n","\r"),'', $string);
		$stringFiltrer = str_replace(array("'",),' ', $string);	
		//$stringFiltrer = str_replace(array(";"),'>', $stringFiltrer);
		$stringFiltrer = strip_tags($stringFiltrer);
		return $stringFiltrer;
		
	}
/* function stripAccents($texte) {
		$texte = str_replace(
			array(
				'à', 'â', 'ä', 'á', 'ã', 'å',
				'î', 'ï', 'ì', 'í', 
				'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
				'ù', 'û', 'ü', 'ú', 
				'é', 'è', 'ê', 'ë',
				'ç', 'ÿ', 'ñ',
				'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
				'Î', 'Ï', 'Ì', 'Í', 
				'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
				'Ù', 'Û', 'Ü', 'Ú', 
				'É', 'È', 'Ê', 'Ë', 
				'Ç', 'Ÿ', 'Ñ' 
			),
			array(
				'a', 'a', 'a', 'a', 'a', 'a', 
				'i', 'i', 'i', 'i', 
				'o', 'o', 'o', 'o', 'o', 'o', 
				'u', 'u', 'u', 'u', 
				'e', 'e', 'e', 'e',
				'c', 'y', 'n', 
				'A', 'A', 'A', 'A', 'A', 'A', 
				'I', 'I', 'I', 'I', 
				'O', 'O', 'O', 'O', 'O', 'O', 
				'U', 'U', 'U', 'U', 
				'E', 'E', 'E', 'E', 
				'C', 'Y', 'N' 
			),$texte);
		return $texte;
	} */
?>
<script type="text/javascript" charset="utf-8">


  $('#checkboxtree').checkboxTree({
            onCheck: {
                ancestors: 'check',
                descendants: 'uncheck',
                others: 'uncheck'
            },
            onUncheck: {
                descendants: 'uncheck'
            }
        });
        
	$("#selectbox-popup").select2({
		minimumInputLength: 3,
		ajax: {
			url: "/app.php/getlistcateg",
			dataType: 'json',
			data: function (search, page) {
				return {
						label: search
						};
				},
				results: function (data, page) {
					return { results: data };
				}
		}
	});	
	
	/*$("#dialogAv input:text").keypress(function(event) {
		if ( event.which == 13 ) {
     		event.preventDefault();
  		};
 	 	alert('test');
	});*/	

</script>


