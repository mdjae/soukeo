
<div id="myModalFusionvalues" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Fusion de valeurs d'attribut :</h3>
    </div>
    
    <div class="modal-body">
        <div class="more-info">
            Vous vous apprétez à fusionner cette liste de valeur pour l'attribut <strong><?php echo $attribut -> getLabel_attr() ?></strong> : <br /> 
<?php 

            foreach ($_POST['values'] as $value) {
                
                echo "<strong>-".$value."</strong><br/>";
            }
                
?>
            Pour confirmer vous devez entrer le nouveau nom de la valeur et cliquer sur <strong>"Ok"</strong>
        </div>
        <br />


            <div class="control-group">          
                <label class="control-label" for="label_attr_value_fusion" >Nouveau nom de valeur :</label>
                    <div class="controls">
                        <input type="text" name="label_attr_value_fusion" value="" id="label_attr_value_fusion" required/>
                    </div>
            </div>

    </div>
    
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" id="valid_fusion_values" onClick="validFusionValues(<?php echo $_POST['id_attribut'] ?>);return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Ok</button>

    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
    
        $('#myModalFusionvalues').on('shown', function() {
            $("#label_attr_value_fusion").focus();
        });
        
        $("#label_attr_value_fusion").keyup(function(event){
            if(event.keyCode == 13){
                $("#valid_fusion_values").click();
            }
        });
    });
</script>