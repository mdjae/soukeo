
<div id="myModalFusionAttr" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Fusion d'attributs :</h3>
    </div>
    
    <div class="modal-body">
        <div class="more-info">
            Vous vous apprétez à fusionner cette liste d'attributs : <br /> 
<?php 
            if(isset($attribut_list)){
                
                foreach ($attribut_list as $attribut) {
                    
                    $value = $attribut->getLabel_attr() ;
                    echo "<strong>-".$attribut->getLabel_attr()." - ".$attribut->getCountProdsInListCatWithThis($categorie_childs)." produits</strong><br/>";
                }
            }            
?>
            Pour confirmer vous devez entrer le nouveau nom de l'attribut et cliquer sur <strong>"Ok"</strong>
        </div>
        <br />
        

            <div class="control-group">          
                <label class="control-label" for="label_attr_fusion" >Nouveau nom de l'attribut :</label>
                    <div class="controls">
                        <input type="text" name="label_attr_fusion" id="label_attr_fusion" required value="<?php echo $value ?>"/>
                    </div>
            </div>

            <div class="control-group">          
                <label class="control-label" for="dominant_id_attribut" >Attribut dominant en cas de conflit de valeur : * </label>
                
                    <div class="controls">
                        <select name="dominant_id_attribut" id="dominant_id_attribut">
 <?php 
                            if(isset($attribut_list)){
                                
                                foreach ($attribut_list as $attribut) {
                                    
                                    echo "<option value='".$attribut->getId_attribut()."'>".$attribut->getLabel_attr()." - ".$attribut->getCountProdsInListCatWithThis($categorie_childs)." produits</option>";
                                }
                            }            
?>                           
                        </select>
                        
                    </div>
            </div>
            <em>* Au cas où si un produit possédait plusieurs de ces attributs en même temps, quelle est la valeur d'attribut dominante.</em>

    </div>
    
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" class="btn btn-primary" id="valid_fusion_attr" onClick="validFusion();return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Ok</button>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
    
        $('#myModalFusionAttr').on('shown', function() {
            $("#label_attr_fusion").focus();
        })
        
        $("#label_attr_fusion").keyup(function(event){
            if(event.keyCode == 13){
                $("#valid_fusion_attr").click();
            }
        });
        
    });
</script>
