
<div id="myModalEditValAttr" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Edition de la valeur de l'attribut :</h3>
    </div>
    
    <div class="modal-body">
        <div class="more-info">
            Vous vous apprétez à modifier une valeur pour l'attribut <?php echo $attribut -> getLabel_attr() ?> : <br /> 

            <strong>-<?php echo trim($_POST['value']) ?></strong><br />
            
            Pour confirmer vous devez entrer le nouveau nom de la valeur et cliquer sur <strong>"Ok"</strong>
        </div>
        <br />


            <div class="control-group">          
                <label class="control-label" for="label_attr_value" >Nouveau nom de valeur :</label>
                <div class="controls">
                    <input type="text" name="label_attr_value"  id="label_attr_value" required autofocus value="<?php echo trim($_POST['value']) ?>"/>
                    <input type="hidden" name="old_label_attr_value" value="<?php echo $_POST['value'] ?>" id="old_label_attr_value" required/>
                    <input type="hidden" name="input_id_attribut" value="<?php echo $_POST['id_attribut'] ?>" id="input_id_attribut" required/>
                </div>
            </div>

    </div>
    
    <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" id="valid_edit_value_attr" onClick="editValueAttr();return false;" data-dismiss="modal" aria-hidden="true"><i class='fa fa-check ' style='padding-right: 0px'></i> Ok</button>

    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
    
        $('#myModalEditValAttr').on('shown', function() {
            $("#label_attr_value").focus();
        });
        
        $("#label_attr_value").keyup(function(event){
            if(event.keyCode == 13){
                $("#valid_edit_value_attr").click();
            }
        });
    });
</script>