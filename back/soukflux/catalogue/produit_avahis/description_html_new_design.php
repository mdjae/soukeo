<html>
    <head>
        <meta charset="utf-8">
        <title>Vue -  Avahis.com</title>
        <meta name="description" content="disques durs externes a-data, a-data hd710 500go (noir)">
        <meta name="keywords" content="Avahis Market Place">
        <meta name="robots" content="INDEX,FOLLOW">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>

        

    <meta charset="utf-8">
    <script src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/catalogue/produit_avahis/ckeditor/ckeditor.js"></script>
    <script src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/skins/produit_avahis/ckeditor/ckeditor.js"></script>
    <style>.cke{visibility:hidden;}</style>
    <link rel="stylesheet" type="text/css" href="http://192.168.1.200:82/skins/common/select2.css" />
    <script language="JavaScript" src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/inc/js/select2.js" type='text/javascript'></script>
    <script>
        
        // This code is generally not necessary, but it is here to demonstrate
        // how to customize specific editor instances on the fly. This fits well
        // this demo because we have editable elements (like headers) that
        // require less features.

        // The "instanceCreated" event is fired for every editor instance created.
        CKEDITOR.on( 'instanceCreated', function( event ) {
            
            var editor = event.editor,
                element = editor.element;

            // Customize editors for headers and tag list.
            // These editors don't need features like smileys, templates, iframes etc.

            
            
            if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
                // Customize the editor configurations on "configLoaded" event,
                // which is fired after the configuration file loading and
                // execution. This makes it possible to change the
                // configurations before the editor initialization takes place.
                editor.on( 'configLoaded', function() {

                    // Remove unnecessary plugins to make the editor simpler.
                    editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                        'forms,iframe,image,newpage,removeformat,' +
                        'smiley,specialchar,stylescombo,templates';

                    // Rearrange the layout of the toolbar.
                    editor.config.toolbarGroups = [
                        { name: 'editing',      groups: [ 'basicstyles', 'links' ] },
                        { name: 'undo' },
                        { name: 'clipboard',    groups: [ 'selection', 'clipboard' ] },
                        { name: 'about' }
                    ];
                });
            }
        });

    </script>
    <script type="text/javascript" src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/catalogue/produit_avahis/ckeditor/config.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/catalogue/produit_avahis/ckeditor/skins/moono/editor.css">
    <script type="text/javascript" src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/catalogue/produit_avahis/ckeditor/lang/fr.js"></script>
    <script type="text/javascript" src="<?php echo "http://" . $_SERVER['HTTP_HOST']; ?>/catalogue/produit_avahis/ckeditor/styles.js"></script>
    
    
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic|PT+Sans+Narrow:400,700" rel="stylesheet" type="text/css">    
    <link rel="stylesheet" type="text/css" href="http://www.avahis.com/skin/frontend/soukeo/foavahis/css/essential.css" media="all">
    <link rel="stylesheet" type="text/css" href="http://www.avahis.com/skin/frontend/soukeo/foavahis/css/style.css" media="all">
    <link rel="stylesheet" type="text/css" href="http://www.avahis.com/skin/frontend/soukeo/foavahis/css/responsive.css" media="all">
</head>
    
    <body class="product">
        <div class="span9">
            <div class="row-fluid border box-shadow">
                    <div class="bloc" id="slider-description">
    
                        <div class="bg-light-gray pag">
                            <ul class="nav_onglet">
                                <li id="product_tabs_description_tabbed" class="selected first">
                                    <span>
                                        Description du produit                 
                                    </span>
                                </li>
                                <li id="product_tabs_review_tabbed" class="">
                                    <span>
                                        Avis des utilisateurs                 
                                    </span>
                                </li>
                            </ul>
                        </div>

                        <div class="onglet_detail content-bloc slides">
                            <div id="product_tabs_description_tabbed_contentss" class="slide" contenteditable="true">
                                <?php echo $product->getDescription_html(); ?>
                            </div>
                            
                            
                            <div id="product_tabs_review_tabbed_contents" class="slide" style="display: none;">
                                <br>
                                <div class="form-add">
                                    <h2>Rédigez votre propre commentaire</h2>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
       </div>        
   </body>
</html>                         
