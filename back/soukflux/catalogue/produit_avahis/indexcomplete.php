<html>
    <head>
        <meta charset="utf-8">
        <title>TEST -  Avahis.com</title>
        <meta name="description" content="disques durs externes a-data, a-data hd710 500go (noir)">
        <meta name="keywords" content="Avahis Market Place">
        <meta name="robots" content="INDEX,FOLLOW">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>

    
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic|PT+Sans+Narrow:400,700" rel="stylesheet" type="text/css">     
    <link rel="stylesheet" type="text/css" href="http://www.avahis.com/skin/frontend/soukeo/foavahis/css/foavahis.css" media="all">
</head>
    
    <body >
        <div class="container">
            <header id="header" class="header  ">

                <div class="header-top"> 
                    <div class="iblock vtop Hleft">
                        <a href="#" title="Accueil Avahis.com" rel="home" class="logo">
                            <img src="http://www.avahis.com/skin/frontend/soukeo/foavahis/images/header_logo-beta.png" width="237" height="131" alt="avahis.com">
                        </a>
                    </div> 
                    <div class="iblock vtop Hcenter">
                        
                        <div id="region_select">
                           <p><span class="legend">Place de marché de l'Océan Indien</span>
                            <span class="region_fr">La Réunion</span>
                            <span class="region_yt">Mayotte</span>
                            <span class="region_mu">Ile Maurice</span>
                            <span class="region_mg">Madagascar</span>
                            </p> 
                        </div>


                    </div>
                    
                    <div class="iblock vtop Hright">
                        <div id="connexion">
                            <p id="welcome">
                                <span class="ico_login ico_login_off "></span>Bienvenue,<span class="orange">visiteur anonyme</span>
                            </p> 
                            <p><span><a href="#" class="blue">Connectez-vous</a> ou <a href="#" class="blue" style="font-weight:bold;">créez un compte</a> sur Avahis.com</span></p>
                        </div>
                        <div id="quicklinks">
                            <div class="panier links">
                                    <a href="#">
                                        <span class="s_panier"></span><span class="text">Mon panier</span>
                                        <span class="panier_compte">0</span>
                                    </a>
                                                                
                            </div>
                            <div class="links">
                                <a href="#">
                                    <span class="s_liste"></span><span class="text">Liste d'envies</span>
                                </a>
                            </div>
                            <div class="compte links">
                                <a href="#">
                                    <span class="s_compte"></span><span class="text">Mon compte</span>
                                </a>
                                                
                            </div>   
                        </div>
                    </div>
                </div>
            
                <div id="navigation" class="wrapper_insert">
                    <div id="mainmenu" class="iblock vmiddle mainmenu">
                        <span class="title">Tous nos produits</span>
                        <nav role="navigation">

                        </nav>
                        <div class="menuslide"></div>
                    </div>
                            
                    <div class="iblock vmiddle">
                        <ul id="ariane">
                            
            
                                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a itemprop="url" href="#" title="Aller à la page d'accueil"><span itemprop="title">Accueil</span></a>
                            <span></span>
                        </li>
                                                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <span itemprop="title"><?php echo $product->getName() ?></span>
                        </li>
                                
            
                        </ul>
                    </div>
                </div>
            
            </header> 
            <div id="recherche" class="wrapper_onleft">
                <div id="menu">
                    <div class="widget">
                        <a href="#"><h4>Univers</h4></a>
                        <ul class="categories">
                            <li class="nav1 active">
                                <a href="#">Sous catégorie</a>
                            </li>
                        </ul>
                    </div>

                    <div class="box">
                        <ul class="services">
                            <li class="paiement-3x">
                                <strong>Paiement en 3X : </strong>
                                Payez vos commandes en 3 fois à partir de 200€ d'achats. Ce service, réservé aux particuliers, vous est proposé pour le paiement de vos commandes.
                            </li>

                            <li class="paiement">
                                <strong>Paiment par CB : </strong>
                                Le paiement s'effectue sur les serveurs sécurisés de nos partenaires (Payline , Paypal).
                                Aucune information bancaire vous concernant ne transite via le site de Avahis.com.
                                Le paiement par carte bancaire est donc parfaitement sécurisé.
                                Votre commande sera ainsi validée dès l'acceptation du paiement par nos partenaires.
                            </li>

                            <li class="dom-tom">
                                <strong>Livraison à La Réunion, France Métropolitaine, Mayotte, Madagascar, Maurice: </strong>
                                Commandez tous vos produits sur Avahis.com et faîtes vous livrer dans les meilleurs délais.
                            </li>

                            <li class="transport">
                                <strong>Livraison by Avahis.com : </strong>
                                Nous vous garantissons une livraison à partir de 48H selon les vendeurs à partir de la préparation de votre commande.
                            </li>

                            <li class="ecologie">
                                <strong>Économie rime avec écologie : </strong>
                                Avahis.com s'engage dans la <em>protection de l'environnement</em> avec l'utilisation
                                d'<em>emballages recyclables (mêlant carton et papier kraft)
                                et robustes pour la sécurité de vos produits</em>.
                            </li>

                            <li class="securite">
                                <strong>100% sécurisé : </strong>
                                Nous mettons en œuvre les meilleures technologies pour vous garantir la <em>sécurisation de vos paiements et de vos données personnelles</em>.
                            </li>
                            <li class="retour">
                                <strong>Retour Produits : </strong>
                                Achetez en toute sécurité , vous avez 15 jours pour nous renvoyer votre produit. Avec Avahis.com , Achetez Zen, Soyez satisfaits ou remboursés !
                            </li>
                        </ul>
                    </div>

                </div>
                
                <div itemscope="" itemtype="http://schema.org/Product" class="content wrapper_menu">
                    <div id="messages_product_view"></div>
                
                    <div id="fiche_produit">
                        
                        <div style="float: right;">
                
                            <button class="light_action">
                                <a href="#">Ajouter à ma liste d'envies</a>
                            </button>
                
                        </div>
                        <h1 itemprop="name" > <?php echo $product->getName() ?> </h1>
                
                        <div id="visuel" class="">
                            <a id="image-link" href="#main-image"> 
                                <img itemprop="image" class="full" src="<?php echo $product->getLocal_image() ? $product->getLocal_image() : $product->getImage() ;?>" alt="<?php echo $product->getName()?>" title="<?php echo $product->getName()?>">    
                            </a>
                
                            <div id="main-image" style="display: none;">
                                <img src="<?php echo $product->getLocal_image() ? $product->getLocal_image() : $product->getImage() ;?>" alt="<?php echo $product->getName()?>" title="<?php echo $product->getName()?>">
                            </div>
                        </div>
                        <div id="focus">
                            <p itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="prix">
                                à partir de <strong itemprop="price"><?php echo $product->getPrice() ?> €</strong>
                
                            </p>
                            <p class="stock_ok">
                                en stock
                            </p>
                            <form action="#" method="post" id="product_addtocart_form">
                                <input type="hidden" name="product" value="27901">
                                <input type="hidden" name="related_product" id="related-products-field" value="">
                
                                <p class="avis">
                                    <a id="to_comment">Aucun commentaire sur ce produit.</a>
                                </p>
                
                                <p itemprop="description" class="description editable" >
                                    <?php echo $product->getShort_description() ?>
                                </p>
                                <meta itemprop="currency" content="EUR">
                
                                <p class="quantite">
                                    <label for="qty">quantité souhaitée : </label>
                                    <select name="qty" >
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    <input type="hidden" name="udropship_vendor" value="24">
                                    <button type="button" class="CTA_orange" >
                                        <a class="buy">Ajouter au panier</a>
                                    </button>
                                </p>
                            </form>
                        </div>
                
                    </div>
                
                    <div id="content_detail" class="wrapper_insert">
                
                        <ul class="nav_onglet">
                            <li id="product_tabs_description_tabbed" class="active first">
                                <span><h4><a href="#" onclick="return false;"> Description du produit </a></h4> </span>
                            </li>
                            <li id="product_tabs_additional_tabbed" class="">
                                <span><h4><a href="#" onclick="return false;"> Fiche technique </a></h4> </span>
                            </li>
                            <li id="product_tabs_review_tabbed">
                                <span><h4><a href="#" onclick="return false;"> Avis des utilisateurs </a></h4> </span>
                            </li>
                        </ul>
                        <div class="onglet_detail">
                            <div id="product_tabs_description_tabbed_contentss" style="width: 95%; margin: 0 auto;" >
                                   <?php echo $product->getDescription_html() ?>
                            </div>
                    
                        </div>
                        
                        
                        </div>
                        
                                <div id="content_detail" class="wrapper_insert">
                                                <ul class="nav_onglet">
                            <li id="product_tabs_description_tabbed" class=" first">
                                <span><h4><a href="#" onclick="return false;"> Description du produit </a></h4> </span>
                            </li>
                            <li id="product_tabs_additional_tabbed" class="active">
                                <span><h4><a href="#" onclick="return false;"> Fiche technique </a></h4> </span>
                            </li>
                            <li id="product_tabs_review_tabbed">
                                <span><h4><a href="#" onclick="return false;"> Avis des utilisateurs </a></h4> </span>
                            </li>
                        </ul>
                        <div class="onglet_detail">
                            <div id="product_tabs_additional_tabbed_contents" style="width: 95%; margin: 0 auto;">    <table class="fiche_technique">
                        <tbody><tr></tr>
                            
            <?php if(isset($prod_attr_list) && !empty($prod_attr_list)){
    
                                foreach ($prod_attr_list as $product_attr) {
                                    
                                    
                                    if($product_attr->getAttribut()){
                                        echo "<tr class='row-attribut'>";
                                        echo "<td><strong  id='attr-".$product_attr->getId_attribut()."' class='attribut'>".ucfirst($product_attr->getAttribut()->getLabel_attr())."<strong></td>";
                                        echo "  <td class='value' id='value-".$product_attr->getId_attribut()."'>".$product_attr->getValue()."</td>";
                                        echo "</tr>";    
                                    }
                                  //else{
                                  //    echo "  <td><strong style='color:red'>Attribut inexistant</strong></td>";
                                  //    echo "  <td> - </td>";
                                  //}
                                    
                                } 

                    }
               ?>
                </tbody></table>
                </div>
                            <div id="product_tabs_review_tabbed_contents">
                
                                <br>
                
                            </div>
                        </div>
                    </div>
                    
                    <div id="livraison_detail" class="wrapper_insert" style="margin-bottom: 60px; margin-top: 30px;">
                        <ul class="nav_onglet">
                            <ol class="legende">
                                <img src="<?php echo "" ?>">
                                <h4>Faîtes-vous livrer à...</h4>
                                <p>
                                    les prix et frais d’envoi dépendent du pays où vous souhaitez vous faire livrer
                                </p>
                            </ol>
                            <li id="RE_partenaires" class="active first">
                                <span> <h4><a href=""> <img src=""> Réunion</a></h4>
                                    <p>
                                        <a>Voir les conditions</a>
                                    </p> </span>
                            </li>
                            <li id="YT_partenaires" class="">
                                <span> <h4><a href="#" onclick="return false;"> <img src=""> Mayotte</a></h4>
                                    <p>
                                        <a>Voir les conditions</a>
                                    </p> </span>
                            </li>
                            <li id="MU_partenaires" class="">
                                <span> <h4><a href="#" onclick="return false;"> <img src=""> Maurice / Madagascar</a></h4>
                                    <p>
                                        <a>Voir les conditions</a>
                                    </p> </span>
                            </li>
                            <li id="FR_partenaires" class="">
                                <span> <h4><a href="#" onclick="return false;"> <img src=""> Métropole</a></h4>
                                    <p>
                                        <a>Voir les conditions</a>
                                    </p> </span>
                            </li>
                        </ul>
                
                        <div id="RE_partenaires_contents">
                            <div class="col3 explain">
                                <p>
                                    Produit vendu par
                                </p>
                            </div>
                            <div class="col3 explain">
                                <p>
                                    Modes de livraison
                                </p>
                            </div>
                            <div class="col3 explain">
                                <p style="text-align: right;">
                                    Prix final
                                </p>
                            </div>
                
                            <div class="partenaire">
                                <div class="col3">
                                    <h5>AVAHIS</h5>
                                    
                                    <div class="seller-rating">
                                        <div id="product_vendors_2_review_html">
                
                                            <div class="ratings">
                                                <div class="rating-box">
                                                    <div class="rating" style="width:70%"></div>
                                                </div>
                                                <p class="rating-links">
                                                    <a href="#">1 Commentaire(s)</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                
                                    <br>
                                    <br>
                
                                    <p class="prix_vente">
                                        prix de vente TTC <strong>796.91 € </strong>
                                    </p>
                
                                </div>
                
                                <div class="col3">
                                    <ul class="detail_livreur">
                
                                        <li class="details">
                                            <p class="prix_livraison">
                                                Livraison <strong>+ 0 €</strong>
                                            </p>
                                            <p style="font-style:italic">
                                                Offert à partir de 50 € d'achat
                                            </p>
                                            <p>
                                                Livré à partir de <strong>10 jours </strong>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                
                                <div class="col3">
                                    <p class="prix_final">
                
                                        prix final<strong>796.91 €</strong> TTC
                                    </p>
                                    <input id="product_vendors_24" type="radio" name="udropship_vendor" value="24" class="udropship_vendor" style="visibility: hidden; display:none;">
                                    <button type="button" title="Add to Cart" class="CTA_orange">
                                        <a>Choisir ce vendeur et <strong>ajouter au panier</strong></a>
                                    </button>
                
                                </div>
                            </div>
                            <hr>
                        </div>
                
                    
                    </div> 
                    <style type="text/css">
                        #product-vendors-states li {
                            float: left;
                            padding-right: 10px
                        }
                        #product-vendors-states:after {
                            display: block;
                            content: ".";
                            clear: both;
                            font-size: 0;
                            line-height: 0;
                            height: 0;
                            overflow: hidden;
                        }
                    </style>
                </div> 
            </div> 
        </div>    
    </body>
    
</html>

