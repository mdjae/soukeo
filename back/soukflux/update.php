#!/usr/local/bin/php -q
<?php 
$debug = false;

if (count($argv) < 1)
{
	echo ("L'outil de mise à jour doit etre exécuté en ligne de commande uniquement");
	exit;
}
// Fichier de config
// Si on passe un vhost, il faut charger le bon fichier de config
if (array_key_exists(1,$argv) && $argv[1] != "")
{
        require 'config/' . $argv[1] . '.config.inc.php';
}
else
{
        require 'config/config.inc.php';
}
require 'inc/libs/global.inc.php';
require 'inc/libs/db.mysql.inc.php';
require 'inc/version.php';
$offipse_conn = dbConnect();

// R�cup�ration de la base version de la base
$query = "SELECT version
			FROM c_offipse_version
			ORDER BY update_date DESC
			LIMIT 1";
$v = dbQueryOne($query);
$db_version = $v['version'];

echo "\n";
echo sprintf(("Version de la base : %s"), $db_version) . "\n";
echo sprintf(("Version de l'application : %s"), $version) . "\n";
if ($version != $db_version)
{
	echo "\n";
	echo "====> " . ("Une mise à niveau va etre réalisée...") . "\n";
	
	// On doit boucler pour faires les mises � niveau successives jusqu'� la version finale
	// On liste les patchs
	$update = true;
	$patches = scandir("_tech/patch");
	while ($db_version != $version && $update)
	{
		if ($debug) echo "DEBUG : Mise à jour...\n";
		$update = update_version();
	}
}

function update_version()
{
	global $db_version, $version, $patches, $debug;
	
	// On liste les scripts d'update
	foreach($patches as $p)
	{
		if ($p != "." && $p != ".." && $p != ".svn" && $p != "create_patches.php")
		{
			if ($debug) echo "DEBUG : dans la mise à jour... " . $p . "\n";
			$tab = preg_split("/[-_]/", $p);
			//print_r($tab);
			$this_old_version = $tab[1];
			$this_new_version = $tab[2];
			$tab = explode(".class.php", $this_new_version);
			$this_new_version = $tab[0];
			$target_version = $tab[0];
			
			if ($this_old_version == $db_version)
			{
				// On doit appliquer le patch
				echo sprintf(("Application du patch %s"), $p) . "\n";
				require_once "_tech/patch/" . $p;
				$this_old_version = str_replace(".", "", $this_old_version);
				$this_new_version = str_replace(".", "", $this_new_version);
				$class = "Patch_" . $this_old_version . "_" . $this_new_version;
				//echo $class;
				$obj = new $class;
				$back = $obj->upgrade();
				// Si ok, on met � jour la version
				if ($back)
				{
					echo sprintf(("La base a été mise à niveau en version %s"), $target_version) . "\n";
					dbQuery("INSERT INTO c_offipse_version (version, update_date) VALUES ('" . $target_version . "' , now())");
					$db_version = $target_version;
				}
				else
				{
					// Probl�me avec l'application du patch
					echo sprintf(("Erreur lors de l'application du patch %s, mise à jour stoppée"), $p) . "\n";
				}
			}
		}
	}
	
	return $back;
}