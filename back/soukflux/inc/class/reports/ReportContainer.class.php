<?php 
/**
 * Class permettant d'afficher un repport dans offipse
 * @author nsa
 *
 */
class ReportContainer
{
	/**
	 * Tableau contenant les �l�ments de la colonne de gauche
	 * @var array
	 */
	protected $_leftCol = array();
	
	/**
	 * Tableau contenant les �l�ments de la colonne de droite
	 * @var array
	 */
	protected $_rightCol = array();
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function __construct()
	{
	}
	
	/**
	 * Ajoute un �l�ment � la colonne de gauche
	 * @param $b
	 * @return unknown_type
	 */
	public function addLeftCol($b)
	{
		$this->_leftCol[] = $b;
	}
	
	/**
	 * Ajoute un �l�ment � la colonne de droite
	 * @param $b
	 * @return unknown_type
	 */
	public function addRightCol($b)
	{
		$this->_rightCol[] = $b;
	}
	
	/**
	 * Param�tre la largeur de la colonne de gauche
	 * @param $width
	 * @return unknown_type
	 */
	public function setLeftColWidth($width)
	{
		$this->_leftCol['width'] = $width;
	}
	
	/**
	 * Param�tre la largeur de la colonne de droite
	 * @param $width
	 * @return unknown_type
	 */
	public function setRightColWidth($width)
	{
		$this->_rightCol['width'] = $width;
	}
	
	/**
	 * Retourne la chaine qui permet d'afficher le report
	 * @return string
	 */
	public function display()
	{
		//echo $this->_title;
		$output = "";
		
		$output .= '<table border="0" width="100%">' . "\n";
		
		$output .= '<tr>';
		
		// Gauche
		$output .= '<td valign="top"><table border="0">' . "\n";
		foreach($this->_leftCol as $obj)
		{
			$output .= "<tr><td>\n";
			$output .= $obj->display();
			$output .= "</td></tr>\n";
		}
		$output .= '</table></td>' . "\n";
		
		// Droite
		$output .= '<td valign="top"><table border="0">' . "\n";
		foreach($this->_rightCol as $obj)
		{
			$output .= "<tr><td>\n";
			$output .= $obj->display();
			$output .= "</td></tr>\n";
		}
		$output .= '</table></td>' . "\n";
		
		$output .= "</tr>\n";
		$output .= '</table>' . "\n";
		
		return $output;
	}
	
	public function update($post)
	{
		// Boucle sur les objets et update des objets
		foreach($this->_leftCol as $obj)
		{
			$obj->update($post);
		}
		
		foreach($this->_rightCol as $obj)
		{
			$obj->update($post);
		}
	}
}
?>