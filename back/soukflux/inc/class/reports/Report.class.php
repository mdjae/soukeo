<?php 
/**
 * Class permettant d'afficher un repport dans offipse
 * @author nsa
 *
 */
class Report extends SystemObjectPersistence
{
	/**
	 * Titre du rapport
	 * @var unknown_type
	 */
	protected $_title = "";
	
	/**
	 * Tableau contenant les �l�ments du rapport
	 * @var array
	 */
	protected $_container = array();
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function __construct($name)
	{
		$this->_name;
	}
	
	/**
	 * 
	 * @param $title
	 * @return unknown_type
	 */
	protected function _setTitle($title)
	{
		$this->_title = $title;
	}
	
	/**
	 * Ajoute un �l�ment � la colonne de gauche
	 * @param $b
	 * @return unknown_type
	 */
	protected function _addContainer($c)
	{
		$this->_container[] = $c;
	}
	
	/**
	 * Retourne la chaine qui permet d'afficher le report
	 * @return string
	 */
	public function display()
	{
		global $root_path;
		// Sauvegarde avant affichage en cas de clique par l'utilisateur
		$this->_save();
		
		//echo $this->_title;
		$output = "";
		$output .= '<span id="report_' . $this->_name . '">' . "\n";
		
		// Est-ce qu'il y a un titre � afficher
		if ($this->_title != "")
		{
			$output .= $this->_title;
		}
		
		//$output .= popupLink($root_path . "/common/report/report.print.php?name=" . $this->_name,_("Version imprimable"),800,800,"") . _("Version imprimable") . "</a></p>\n";

		foreach($this->_container as $obj)
		{
			$output .= $obj->display();
		}
		
		$output .= '</span>' . "\n";
		return $output;
	}
	
	public function update($post)
	{
		// Mise � jour du titre
		$this->_updateTitle($post);
		
		// Boucle sur les objets et update des objets
		foreach($this->_container as $obj)
		{
			$obj->update($post);
		}
	}
	
	protected function _updateTitle($post)
	{
		
	}
	
	/**
	 * Retourne le nom du rapport
	 * 
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}
}
?>