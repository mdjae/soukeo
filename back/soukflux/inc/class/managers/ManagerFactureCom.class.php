<?php 
/**
 * 
 */
class ManagerFactureCom extends sdb {
	
	public function save(BusinessFactureCom $factureCom)
	{
		$factureCom->isNew() ? $this->add($factureCom) : $this->modify($factureCom);
	}


	public function getFactureComById($facture_com_id)
	{
		$sql = "SELECT * FROM skf_factures_com WHERE facture_id = :facture_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":facture_com_id", $facture_com_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessFactureCom');
	
		if ($factureCom = $stmt -> fetch()) {
			return $factureCom;
		}else{
			return null;
		}
	}

	public function getFactureComByFactureId($facture_id)
	{
		$sql = 'SELECT * FROM skf_factures_com WHERE facture_id = :facture_id';
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':facture_id', $facture_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessFactureCom');
	
		if ($facture = $stmt -> fetch()) {
			return $facture;
		}else{
			return null;
		}	
	}
	
	  public function add(BusinessFactureCom $factureCom)
	{
		$sql = "INSERT INTO skf_factures_com  ( facture_id, vendor_id, total_prix, total_comm,
												total_frais_banq, grand_total, mt_a_transferer, date_limit, email, tel) 
		        					   VALUES (:facture_id, :vendor_id, :total_prix, :total_comm,
									   			:total_frais_banq, :grand_total, :mt_a_transferer, :date_limit, :email, :tel) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':facture_id', 		$factureCom -> getFacture_id());
		$stmt -> bindValue(':vendor_id', 		$factureCom -> getVendor_id());
		$stmt -> bindValue(':total_prix', 		$factureCom -> getTotal_prix());
		$stmt -> bindValue(':total_comm', 		$factureCom -> getTotal_comm());
		$stmt -> bindValue(':total_frais_banq', $factureCom -> getTotal_frais_banq());
		$stmt -> bindValue(':grand_total', 		$factureCom -> getGrand_total());
		$stmt -> bindValue(':mt_a_transferer', 	$factureCom -> getMt_a_transferer());
		$stmt -> bindValue(':date_limit', 		$factureCom -> getDate_limit());
		$stmt -> bindValue(':email', 			$factureCom -> getEmail());
		$stmt -> bindValue(':tel', 				$factureCom -> getTel());
		$stmt -> execute();
		
		$factureCom -> setFacture_com_id ($this -> lastInsertId());
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessFactureCom $factureCom)
	{
		$sql = "UPDATE skf_factures_com SET facture_id = :facture_id, vendor_id = :vendor_id,
											total_prix = :total_prix, total_comm = :total_comm, 
											total_frais_banq = :total_frais_banq, 
											grand_total = :grand_total,
											mt_a_transferer = :mt_a_transferer,
											date_limit = :date_limit,
											email = :email,
											tel = :tel,
											email_send = :email_send   
				WHERE facture_com_id = :facture_com_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt -> bindValue(':facture_id', 		$factureCom -> getFacture_id());
		$stmt -> bindValue(':vendor_id', 		$factureCom -> getVendor_id());
		$stmt -> bindValue(':total_prix', 		$factureCom -> getTotal_prix());
		$stmt -> bindValue(':total_comm', 		$factureCom -> getTotal_comm());
		$stmt -> bindValue(':total_frais_banq', $factureCom -> getTotal_frais_banq());
		$stmt -> bindValue(':grand_total', 		$factureCom -> getGrand_total());
		$stmt -> bindValue(':mt_a_transferer', 	$factureCom -> getMt_a_transferer());
		$stmt -> bindValue(':date_limit', 		$factureCom -> getDate_limit());
		$stmt -> bindValue(':email', 			$factureCom -> getEmail());
		$stmt -> bindValue(':tel', 				$factureCom -> getTel());
		$stmt -> bindValue(':email_send',       $factureCom -> getEmail_send());
		$stmt -> bindValue(':facture_com_id', 	$factureCom -> getFacture_com_id(), \PDO::PARAM_INT);
		 
		$stmt->execute(); 
	}
	
	public function delete(BusinessFactureCom $factureCom)
	{
		$sql = "DELETE FROM skf_factures_com  WHERE facture_com_id = :facture_com_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':facture_com_id', $factureCom->getFacture_com_id(), \PDO::PARAM_INT);
		$stmt->execute();
		
        $sql = "DELETE FROM skf_factures  WHERE facture_id = :facture_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':facture_id', $factureCom->getFacture_id(), \PDO::PARAM_INT);
        $stmt->execute();
        
        $sql = "UPDATE skf_order_items SET facture_id = 0 WHERE facture_id = :facture_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':facture_id', $factureCom->getFacture_id(), \PDO::PARAM_INT);
        $stmt->execute();
		//Cascade... ?
	}
}
?>