<?php 
/**
 * 
 */
class ManagerTicketPriority extends Manager {
    
    protected $table = "skf_ticket_priorities";
    
    protected $id = array(  "priority_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>