<?php 
/**
 * 
 */
class ManagerTicketState extends Manager {
    
    protected $table = "skf_ticket_states";
    
    protected $id = array(  "state_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>