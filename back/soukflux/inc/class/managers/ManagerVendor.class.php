<?php 
/**
 * 
 */
class ManagerVendor extends Manager {
	
    protected $table = "sfk_vendor";
    
    protected $id = array("entity_id" => array());
    
    protected $fields = array( "vendor_id" => array( "default" => "0"),
                               "vendor_nom" => array(),
                               "vendor_url" => array(),
                               "active" => array("default" => 1),
                               "vendor_attn" => array(),
                               "email" => array(),
                               "street" => array(),
                               "city" => array(),
                               "zip" => array(),
                               "country_id" => array(),
                               "region" => array(),
                               "telephone" => array(),
                               "telephone2" => array(),
                               "fax" => array(),
                               "raisonsocial" => array(),
                               "siret" => array(),
                               "siteinternet" => array(),
                               "civilite" => array(),
                               "vendor_attn2" => array(),
                               "abonnement" => array(),
                               "product_limit" => array(),
                               "vendor_type" => array(),
                               "solde_credit" => array(),
                               "date_debut_abonnement" => array(),
                               "statut" => array(),
                               "birthd" => array(),
                               "livraison_type_id" => array(),
                               "created_at" => array("timestamp" => true, "no_update" => true, "default" => " NOW() "));
                               
   public function saveDbAvahis(BusinessVendor $vendor)
   {
        $db_avahis = new BusinessAvahisModel();
        
        
        $sql = "UPDATE udropship_vendor 
                SET vendor_name = :vendor_name, siret = :siret, 
                    vendor_attn = :vendor_attn, vendor_attn2 = :vendor_attn2,
                    email = :email, telephone = :telephone, telephone2 = :telephone2 
                WHERE vendor_id = :vendor_id ";
                                            
        $stmt = $db_avahis -> prepare($sql);
        
        $stmt -> bindValue(":vendor_name", $vendor -> getVendor_nom());
        $stmt -> bindValue(":siret", $vendor -> getSiret());
        $stmt -> bindValue(":vendor_attn", $vendor -> getVendor_attn());
        $stmt -> bindValue(":vendor_attn2", $vendor -> getVendor_attn2());
        $stmt -> bindValue(":email", $vendor -> getEmail());
        $stmt -> bindValue(":telephone", $vendor -> getTelephone());
        $stmt -> bindValue(":telephone2", $vendor -> getTelephone2());
        $stmt -> bindValue(":vendor_id", $vendor -> getVendor_id(), \PDO::PARAM_INT);
        
        $stmt -> execute(); 
   }
                               
   public function updateEntityId(BusinessVendor $vendor)
   {
        $sql = "UPDATE sfk_vendor SET entity_id = :entity_id WHERE email = :email ";
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(":entity_id", $vendor -> getEntity_id(), \PDO::PARAM_INT);
        $stmt -> bindValue(":email", $vendor -> getemail());
        
        $stmt -> execute();       
   }      
   
   public function getAllByFilters($entity_id="", $name="")
   {
        $sql = 'SELECT * FROM sfk_vendor ';
        
        $where = "";
        
        if (!empty($name)) {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $sql .= $where."( LOWER(vendor_nom) LIKE :name OR LOWER(vendor_attn) LIKE :name OR LOWER(vendor_attn2) LIKE :name )";  
        }   
        
        if (!empty($entity_id)) {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $sql .= $where." entity_id = :entity_id ";  
        }  
                  
        $stmt = $this -> prepare($sql);
       
        
        if (!empty($name)) {
            $stmt -> bindvalue(':name','%'.strtolower(trim($name)).'%',PDO::PARAM_STR);
        }
        if (!empty($entity_id)) {
            $stmt -> bindvalue(':entity_id',$entity_id);
        }
        
        $stmt -> execute();
             
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessVendor');
     
        $vendorList = $stmt -> fetchAll();

        return $vendorList;       
   }                         
   
}
?>