<?php 
/**
 * 
 */
class ManagerCatalogProduct extends Manager {
	
    protected $table = "sfk_catalog_product";
    
    protected $id = array("id_produit" => array("auto_inc" => true));
    
    protected $fields = array( "sku" => array(),
                               "categories" => array(),
                               "name" => array(),
                               "description" => array(),
                               "description_html" => array(),
                               "short_description" => array(),
                               "price" => array(),
                               "weight" => array(),
                               "country_of_manufacture" => array(),
                               "ean" => array(),
                               "meta_description" => array(),
                               "meta_keyword" => array(),
                               "meta_title" => array(),
                               "image" => array(),
                               "small_image" => array(),
                               "local_image" => array(),
                               "local_small_image" => array(),
                               "local_thumbnail" => array(),
                               "thumbnail" => array(),
                               "manufacturer" => array(),
                               "dropship_vendor" => array(),
                               "date_update" => array("timestamp" => true, "no_update" => true),
                               "date_create" => array("timestamp" => true, "no_update" => true, "default" => " NOW() "));
                               
   
}
?>