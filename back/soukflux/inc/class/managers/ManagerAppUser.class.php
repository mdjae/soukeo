<?php 
/**
 * 
 */
class ManagerAppUser extends Manager {
    
    protected $table = "c_user";
    
    protected $id = array(  "user_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "firstname" => array(),
                                "lastname" => array(),  
                                "usercode" => array(),
                                "email" => array(),
                                "login" => array());
                                
}
?>