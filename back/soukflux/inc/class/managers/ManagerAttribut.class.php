<?php 
/**
 * 
 */
class ManagerAttribut extends Manager {
    
    protected $table = "sfk_attribut";
    
    protected $id = array(  "id_attribut" => array("auto_inc" => true));
                            
    protected $fields = array("cat_id"        => array(),
                              "code_attr"     => array(),
                              "label_attr"    => array(),
                              "is_filterable" => array());
    
    
    public function delete(BusinessAttribut $attribut){


        //Cascade... ?
        $sql = "DELETE FROM sfk_product_attribut  WHERE id_attribut = :id_attribut";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':id_attribut', $attribut->getId_attribut(), \PDO::PARAM_INT);
        $stmt->execute();     
                
        $sql = "DELETE FROM sfk_attribut  WHERE id_attribut = :id_attribut";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':id_attribut', $attribut->getId_attribut(), \PDO::PARAM_INT);
        $stmt->execute();
        
   
    }
}
?>