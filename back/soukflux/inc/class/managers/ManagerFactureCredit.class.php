<?php 
/**
 * 
 */
class ManagerFactureCredit extends Manager {
	
    protected $table = "skf_factures_credits";
    
    protected $id = array("facture_credit_id" => array("auto_inc" => true));
    
    protected $fields = array( "facture_id" => array(),
                               "vendor_id" => array(),
                               "total_prix" => array(),
                               "grand_total" => array(),
                               "email" => array(),
                               "tel" => array(),
                               "email_send" => array(),
                               "payment_type_id" => array());
                               
}
?>