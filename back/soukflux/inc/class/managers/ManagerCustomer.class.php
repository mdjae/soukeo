<?php 
/**
 * 
 */
class ManagerCustomer extends Manager {
	
    protected $table = "skf_customers";
    
    protected $id = array("entity_id" => array());
    
    protected $fields = array( "customer_id" => array(),
                               "name" => array(),
                               "company" => array(),
                               "firstname" => array(),
                               "lastname" => array(),
                               "email" => array(),
                               "phone" => array(),
                               "phone2" => array(),
                               "birthd" => array(),
                               "type" => array(),
                               "statut" => array(),
                               "more_info" => array(),
                               "date_updated" => array("timestamp" => true, "no_update" => true),
                               "date_created" => array("timestamp" => true, "no_update" => true, "default" => " NOW() "));
                               
                               
   public function updateEntityId(BusinessCustomer $customer)
   {
        $sql = "UPDATE skf_customers SET entity_id = :entity_id WHERE email = :email ";
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(":entity_id", $customer -> getEntity_id(), \PDO::PARAM_INT);
        $stmt -> bindValue(":email", $customer -> getemail());
        
        $stmt -> execute();       
   }  
   
   public function getAllByFilters($entity_id="", $name="")
   {
        $sql = 'SELECT * FROM skf_customers ';
        
        $where = "";
        
        if (!empty($name)) {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $sql .= $where." LOWER(name) LIKE :name ";  
        }   
        
        if (!empty($entity_id)) {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $sql .= $where." entity_id = :entity_id ";  
        }  
                  
        $stmt = $this -> prepare($sql);
       
        
        if (!empty($name)) {
            $stmt -> bindvalue(':name','%'.strtolower(trim($name)).'%',PDO::PARAM_STR);
        }
        if (!empty($entity_id)) {
            $stmt -> bindvalue(':entity_id',$entity_id);
        }
        
        $stmt -> execute();
             
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessCustomer');
     
        $customerList = $stmt -> fetchAll();

        return $customerList;
   }
}
?>