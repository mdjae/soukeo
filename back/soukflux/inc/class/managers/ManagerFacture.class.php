<?php 
/**
 * 
 */
class ManagerFacture extends sdb {
	
	public function save(BusinessFacture $facture)
	{
		$facture->isNew() ? $this->add($facture) : $this->modify($facture);
	}
	
	

	public function getFactureById($facture_id)
	{
		$sql = "SELECT * FROM skf_factures WHERE facture_id = :facture_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":facture_id", $facture_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessFacture');
	
		if ($facture = $stmt -> fetch()) {
			return $facture;
		}else{
			return null;
		}
	}

	public function getFactureByCode($facture_code)
	{
		$sql = 'SELECT * FROM skf_factures WHERE facture_code = :facture_code';
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':facture_code', $facture_code);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessFacture');
	
		if ($facture = $stmt -> fetch()) {
			return $facture;
		}else{
			return null;
		}	
	}
	
	  public function add(BusinessFacture $facture)
	{
		$sql = "INSERT INTO skf_factures  (facture_code, emeteur_addr_id, recipient_addr_id,
											date_created) 
		        VALUES (:facture_code, :emeteur_addr_id, :recipient_addr_id,  
		        							NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':facture_code', 	$facture -> getFacture_code());
		$stmt -> bindValue(':emeteur_addr_id', 	$facture -> getEmeteur_addr_id());
		$stmt -> bindValue(':recipient_addr_id',$facture -> getRecipient_addr_id());
		$stmt -> execute();
		
		$facture -> setFacture_id ($this -> lastInsertId());
		$facture -> setDate_created(date('Y-m-d H:i:s'));
		$facture -> setDate_updated(date('Y-m-d H:i:s'));
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessFacture $facture)
	{
		$sql = "UPDATE skf_factures SET 	facture_code = :facture_code, 
											emeteur_addr_id = :emeteur_addr_id, 
											recipient_addr_id = :recipient_addr_id
				WHERE facture_id = :facture_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':facture_code', 		$facture->getFacture_code());
		$stmt->bindValue(':emeteur_addr_id', 	$facture->getEmeteur_addr_id());
		$stmt->bindValue(':recipient_addr_id', 	$facture->getRecipient_addr_id());
		$stmt->bindValue(':facture_id', 		$facture->getFacture_id(), \PDO::PARAM_INT);
		 
		$stmt->execute(); 
		
		$facture -> setDate_updated(date('Y-m-d H:i:s'));
	}
	
	public function delete(BusinessFacture $facture)
	{
		$sql = "DELETE FROM skf_factures  WHERE facture_id = :facture_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':facture_id', $facture->getFacture_id(), \PDO::PARAM_INT);
		$stmt->execute();
		
        $sql = "DELETE FROM skf_factures_com  WHERE facture_id = :facture_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':facture_id', $facture->getFacture_id(), \PDO::PARAM_INT);
        $stmt->execute();
        
        $sql = "UPDATE skf_order_items SET payment = 0  WHERE facture_id = :facture_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':facture_id', $facture->getFacture_id(), \PDO::PARAM_INT);
        $stmt->execute();
        
        $sql = "UPDATE skf_order_items SET facture_id = 0  WHERE facture_id = :facture_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':facture_id', $facture->getFacture_id(), \PDO::PARAM_INT);
        $stmt->execute();
        
		//Cascade... ?
	}
}
?>