<?php 
/**
 * 
 */
class ManagerFactureComLine extends sdb {
	
	public function save(BusinessFactureComLine $factureComLine)
	{
		$factureComLine->isNew() ? $this->add($factureComLine) : $this->modify($factureComLine);
	}
	
	

	public function getFactureComLineById($line_id, $facture_com_id)
	{
		$sql = "SELECT * FROM skf_factures_com_lines WHERE line_id = :line_id AND facture_com_id = :facture_com_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":line_id", $line_id);
		$stmt -> bindValue(":facture_com_id", $facture_com_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessFactureComLine');
	
		if ($factureComLine = $stmt -> fetch()) {
			return $factureComLine;
		}else{
			return null;
		}
	}

	public function getAllFactureComLineForFactureCom($facture_com_id)
	{
		$sql = 'SELECT * FROM skf_factures_com_lines WHERE facture_com_id = :facture_com_id ORDER BY line_id ASC';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':facture_com_id', 	  $facture_com_id);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessFactureComLine');
     
    	$listeFactureComLines = $stmt -> fetchAll();

    	return $listeFactureComLines;
	}
	
	  public function add(BusinessFactureComLine $factureComLine)
	{
		$sql = "INSERT INTO skf_factures_com_lines  (line_id, facture_com_id, item_id, order_increment_id, 
													 qty, designation, price_ttc,
													 category_id, percent_comm, mt_comm, 
													 mt_frais_banq, litiges, raison_litige, date_created) 
		        							 VALUES (:line_id, :facture_com_id, :item_id, :order_increment_id, 
		        							 		 :qty, :designation, :price_ttc,
		        							 		 :category_id, :percent_comm, :mt_comm,
		        							 		 :mt_frais_banq, :litiges, :raison_litige, NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':line_id', 			$factureComLine -> getLine_id());
		$stmt -> bindValue(':facture_com_id', 	$factureComLine -> getFacture_com_id());
		$stmt -> bindValue(':item_id',			$factureComLine -> getItem_id());
		$stmt -> bindValue(':order_increment_id',$factureComLine -> getOrder_increment_id());
		$stmt -> bindValue(':qty',				$factureComLine -> getQty());
		$stmt -> bindValue(':designation',		$factureComLine -> getDesignation());
		$stmt -> bindValue(':price_ttc',		$factureComLine -> getPrice_ttc());
		$stmt -> bindValue(':category_id',		$factureComLine -> getCategory_id());
		$stmt -> bindValue(':percent_comm',		$factureComLine -> getPercent_comm());
		$stmt -> bindValue(':mt_comm',			$factureComLine -> getMt_comm());
		$stmt -> bindValue(':mt_frais_banq',	$factureComLine -> getMt_frais_banq());
		$stmt -> bindValue(':litiges',			$factureComLine -> getLitiges());
		$stmt -> bindValue(':raison_litige',	$factureComLine -> getRaison_litige());
		$stmt -> execute();
		
		$factureComLine -> setDate_created(date('Y-m-d H:i:s'));
		$factureComLine -> setDate_updated(date('Y-m-d H:i:s'));
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessFactureComLine $factureComLine)
	{
		$sql = "UPDATE skf_factures_com_lines SET 	item_id = :item_id,
													order_increment_id = :order_increment_id,
													qty = :qty, 
													designation = :designation,
													price_ttc = :price_ttc, 
													category_id = :category_id,
													percent_comm = :percent_comm,
													mt_comm = :mt_comm,
													mt_frais_banq = :mt_frais_banq,
													litiges = :litiges,
													raison_litige = :raison_litige 
													
				WHERE line_id = :line_id AND facture_com_id = :facture_com_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt -> bindValue(':line_id', 			$factureComLine -> getLine_id());
		$stmt -> bindValue(':facture_com_id', 	$factureComLine -> getFacture_com_id());
		$stmt -> bindValue(':item_id',			$factureComLine -> getItem_id());
		$stmt -> bindValue(':order_increment_id',$factureComLine -> getOrder_increment_id());
		$stmt -> bindValue(':qty',				$factureComLine -> getQty());
		$stmt -> bindValue(':designation',		$factureComLine -> getDesignation());
		$stmt -> bindValue(':price_ttc',		$factureComLine -> getPrice_ttc());
		$stmt -> bindValue(':category_id',		$factureComLine -> getCategory_id());
		$stmt -> bindValue(':percent_comm',		$factureComLine -> getPercent_comm());
		$stmt -> bindValue(':mt_comm',			$factureComLine -> getMt_comm());
		$stmt -> bindValue(':mt_frais_banq',	$factureComLine -> getMt_frais_banq());
		$stmt -> bindValue(':litiges',			$factureComLine -> getLitiges());
		$stmt -> bindValue(':raison_litige',	$factureComLine -> getRaison_litige());
		 
		$stmt->execute(); 
		
		$factureComLine -> setDate_updated(date('Y-m-d H:i:s'));
	}
	
	public function delete(BusinessFactureComLine $factureComLine)
	{
		$sql = "DELETE FROM skf_factures_com_lines  WHERE line_id = :line_id AND facture_com_id = :facture_com_id";
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':line_id', 			$factureComLine -> getLine_id());
		$stmt -> bindValue(':facture_com_id', 	$factureComLine -> getFacture_com_id());
		$stmt->execute();
	}
}
?>