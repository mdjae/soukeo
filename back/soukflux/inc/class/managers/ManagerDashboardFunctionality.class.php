<?php 
/**
 * 
 */
class ManagerDashboardFunctionality extends Manager {
	
    protected $table = "skf_dashboard_functionalities";
    
    protected $id = array("dashboard_functionality_id" => array("auto_inc" => true));
    
    protected $fields = array("name" => array(),
                              "size" => array());
    
    
}
?>