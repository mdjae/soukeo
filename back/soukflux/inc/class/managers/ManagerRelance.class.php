<?php 
/**
 * 
 */
class ManagerRelance extends Manager {
    
    protected $table = "skf_relances";
    
    protected $id = array(  "relance_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "date_relance" => array(),
                                "type_relance_id" => array(),
                                "user_id" => array(),  
                                "ticket_id" => array(),
                                "date_termine" => array());
                                
                                
}
?>