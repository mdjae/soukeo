<?php 
/**
 * 
 */
class ManagerCreditsHistory extends sdb {
	
	public function save(BusinessCreditsHistory $credits_history)
	{
		$credits_history->isNew() ? $this->add($credits_history) : $this->modify($credits_history);
	}
	
	

	public function getCreditsHistoryById($history_id)
	{
		$sql = "SELECT * FROM skf_credits_history WHERE history_id = :history_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":history_id", $history_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessCreditsHistory');
	
		if ($credits_history = $stmt -> fetch()) {
			return $credits_history;
		}else{
			return null;
		}
	}

	public function getAllCreditsHistoryByVendor($vendor_id)
	{
		$sql = 'SELECT * FROM skf_credits_history WHERE vendor_id = :vendor_id ORDER BY date_created DESC';
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':vendor_id', $vendor_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessCreditsHistory');
	
        $credits_history_list = $stmt -> fetchAll();

        return $credits_history_list;
	}
	
	  public function add(BusinessCreditsHistory $credits_history)
	{
		$sql = "INSERT INTO skf_credits_history  (vendor_id, label, amount,
											type, date_created, pack_id, facture_id) 
		        VALUES (:vendor_id, :label, :amount, :type, NOW(), :pack_id, :facture_id) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':vendor_id',           $credits_history -> getVendor_id());
        $stmt -> bindValue(':label',               $credits_history -> getLabel());
		$stmt -> bindValue(':amount', 	           $credits_history -> getAmount());
		$stmt -> bindValue(':type',                $credits_history -> getType());
        $stmt -> bindValue(':pack_id',             $credits_history -> getPack_id());
        $stmt -> bindValue(':facture_id',          $credits_history -> getFacture_id());
		$stmt -> execute();
		
		$credits_history -> setHistory_id ($this -> lastInsertId());
		$credits_history -> setDate_created(date('Y-m-d H:i:s'));
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessCreditsHistory $credits_history)
	{
		$sql = "UPDATE skf_credits_history SET 	 label = :label, 
											     amount = :amount, 
											     type = :type,
											     pack_id = :pack_id, 
											     facture_id = :facture_id  
				WHERE history_id = :history_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt -> bindValue(':label', 	  $credits_history -> getLabel());
		$stmt -> bindValue(':amount',     $credits_history -> getAmount());
		$stmt -> bindValue(':type',       $credits_history -> getType());
        $stmt -> bindValue(':pack_id',    $credits_history -> getPack_id());
        $stmt -> bindValue(':facture_id', $credits_history -> getFacture_id());
		$stmt -> bindValue(':history_id', $credits_history -> getHistory_id(), \PDO::PARAM_INT);
		 
		$stmt->execute(); 
	}
	
	public function delete(BusinessCreditsHistory $credits_history)
	{
		$sql = "DELETE FROM skf_credits_history  
		        WHERE history_id = :history_id";
        
		$stmt = $this -> prepare($sql);
        
		$stmt->bindValue(':history_id', $credits_history->getHistory_id(), \PDO::PARAM_INT);
        
		$stmt->execute();
		
		//Cascade... ?
	}
}
?>