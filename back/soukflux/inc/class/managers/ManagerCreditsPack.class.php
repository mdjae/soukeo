<?php 
/**
 * 
 */
class ManagerCreditsPack extends Manager {
    
    protected $table = "skf_credits_packs";
    
    protected $id = array(  "pack_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array(),
                                "value" => array(),
                                "price" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>