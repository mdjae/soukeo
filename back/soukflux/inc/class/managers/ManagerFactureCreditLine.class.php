<?php 
/**
 * 
 */
class ManagerFactureCreditLine extends Manager {
	
    protected $table = "skf_factures_credits_lines";
    
    protected $id = array("facture_credits_id" => array(),
                          "line_id" => array());
    
    protected $fields = array( "pack_id" => array(),
                               "qty" => array(),
                               "designation" => array(),
                               "price_ht" => array(),
                               "price_ttc" => array(),
                               "date_updated" => array("timestamp" => true, "no_update" => true),
                               "date_created" => array("timestamp" => true, "no_update" => true, "default" => " NOW() "));
                               
}
?>