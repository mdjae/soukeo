<?php 
/**
 * 
 */
class ManagerTicketQuestionType extends Manager {
    
    protected $table = "skf_ticket_question_types";
    
    protected $id = array(  "type_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>