<?php 
/**
 * 
 */
class ManagerComment extends sdb {
	
	public function save(BusinessComment $comment)
	{
		$comment->isNew() ? $this->add($comment) : $this->modify($comment);
	}
	
	
	public function getAllCommentsForObject($commented_object_id, $commented_object_table)
	{
		$sql = 'SELECT * FROM skf_comments WHERE commented_object_id = :commented_object_id AND commented_object_table = :commented_object_table ORDER BY date_created DESC';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':commented_object_id', 	  $commented_object_id);
		$stmt -> bindValue(':commented_object_table', $commented_object_table);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessComment');
     
    	$listeComments = $stmt -> fetchAll();

    	return $listeComments;
	}

	public function getCommentById($id_comment)
	{
		$sql = "SELECT * FROM skf_comments WHERE id_comment	 = :id_comment";

		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":id_comment", $id_comment, \PDO::PARAM_INT);
		
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessComment');
	
		if ($comment = $stmt -> fetch()) {
			return $comment;
		}else{
			return null;
		}
	}
	
	  public function add(BusinessComment $comment)
	  {
	    $sql = "INSERT INTO skf_comments  (content, user_id, parent_id_comment,
										   commented_object_id, commented_object_table, date_created) 
		        VALUES (:content, :user_id, :parent_id_comment, :commented_object_id, :commented_object_table, NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':content', 					$comment -> getContent());
		$stmt -> bindValue(':user_id', 					$comment -> getUser_id());
		$stmt -> bindValue(':parent_id_comment', 		$comment -> getParent_id_comment());
		$stmt -> bindValue(':commented_object_id', 		$comment -> getCommented_object_id());
		$stmt -> bindValue(':commented_object_table', 	$comment -> getCommented_object_table());
		$stmt -> execute();
		
		$comment -> setId_comment ($this -> lastInsertId());
		$comment -> setDate_created(date('Y-m-d H:i:s'));
		$comment -> setDate_updated(date('Y-m-d H:i:s'));
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessComment $comment)
	{
		$sql = "UPDATE skf_comments  SET content = :content WHERE id_comment = :id_comment 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':id_comment', $comment -> getId_comment());
		$stmt -> bindValue(':content', $comment -> getContent());
		$stmt -> execute();  
		$comment -> setDate_updated(date('Y-m-d H:i:s'));
	}
	
	public function delete(BusinessComment $comment)
	{
		$sql = "DELETE FROM skf_comments WHERE id_comment = :id_comment";
		
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':id_comment', $comment->getId_comment(), \PDO::PARAM_INT);
		$stmt->execute();	
	}
}
?>