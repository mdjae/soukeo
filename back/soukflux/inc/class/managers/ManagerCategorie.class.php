<?php 
/**
 * 
 */
class ManagerCategorie extends Manager {
	
    protected $table = "sfk_categorie";
    
    protected $id = array(  "cat_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "cat_label" => array(),
                                "cat_data" => array(),
                                "parent_id" => array(),
                                "cat_position" => array(),
                                "cat_level" => array(),
                                "cat_active" => array(),
                                "count_prod" => array(),
                                "OM" => array(),
                                "MARGE" => array(),
                                "TVA" => array(),
                                "COMM" => array(),);
    
    
	public function save(BusinessCategorie $categorie)
	{
		$categorie->isNew() ? $this->add($categorie) : $this->modify($categorie);
	}
	
    public function getAllUnivers()
    {
        $sql = 'SELECT * FROM sfk_categorie WHERE cat_level = 2 AND cat_active = 1 ';
                  
        $stmt = $this -> prepare($sql);

        $stmt -> execute();
             
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessCategorie');
     
        $cat_list = $stmt -> fetchAll();

        return $cat_list;         
    }

	public function getCategorieByCategorieId($cat_id)
	{
		$sql = "SELECT * FROM sfk_categorie WHERE cat_id = :cat_id";

		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":cat_id", $cat_id, \PDO::PARAM_INT);
		
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessCategorie');
	    
		if ($categorie = $stmt -> fetch()) {
			return $categorie;
		}else{
			return null;
		}
	}
	
	  public function add(BusinessCategorie $categorie)
	  {
	   
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessCategorie $categorie)
	{
		$sql = "UPDATE sfk_categorie  SET OM = :OM, MARGE = :MARGE, TVA = :TVA, COMM = :COMM WHERE cat_id = :cat_id 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':cat_id', $categorie -> getCat_id());
		$stmt -> bindValue(':OM', $categorie -> getOm());
        $stmt -> bindValue(':MARGE', $categorie -> getMarge());
        $stmt -> bindValue(':TVA', $categorie -> getTva());
        $stmt -> bindValue(':COMM', $categorie -> getComm());
		$stmt -> execute();  
	}
	
	public function delete(BusinessCategorie $categorie)
	{

	}
}
?>