<?php 
/**
 * 
 */
class ManagerVendorCategorie extends Manager {
    
    protected $table = "skf_vendor_categories";
    
    protected $id = array(  "entity_id" => array(), 
                            "cat_id" => array());
                            
    protected $fields = array("cat_label" => array());
    
    
    public function delete(BusinessVendorCategorie $vendorCategorie)
    {
        $sql = "DELETE FROM skf_vendor_categories WHERE entity_id = :entity_id AND cat_id = :cat_id ";
        
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':entity_id',  $vendorCategorie->getEntity_id(), \PDO::PARAM_INT);
        $stmt->bindValue(':cat_id',     $vendorCategorie->getCat_id(), \PDO::PARAM_INT);
        $stmt->execute();
    }
}
?>