<?php 
/**
 * 
 */
class ManagerClientType extends Manager {
    
    protected $table = "skf_client_types";
    
    protected $id = array(  "type_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>