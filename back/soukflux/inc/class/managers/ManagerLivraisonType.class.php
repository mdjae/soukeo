<?php 
/**
 * 
 */
class ManagerLivraisonType extends Manager {
    
    protected $table = "skf_livraison_types";
    
    protected $id = array(  "shipping_id" => array());
                            
    protected $fields = array(  "shipping_code" => array(),
                                "shipping_title" => array(),
                                "days_in_transit" => array(),
                                "refund" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>