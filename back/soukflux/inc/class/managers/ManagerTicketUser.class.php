<?php 
/**
 * 
 */
class ManagerTicketUser extends Manager {
    
    protected $table = "skf_tickets_users";
    
    protected $id = array(  "ticket_id" => array(), 
                            "user_id" => array());
                            
    protected $fields = array("date_created" => array("no_update" => true),
                              "seen" => array());
    
    
    public function delete(BusinessTicketUser $ticketUser)
    {
        $sql = "DELETE FROM skf_tickets_users WHERE user_id = :user_id AND ticket_id = :ticket_id ";
        
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':ticket_id',  $ticketUser->getTicket_id(), \PDO::PARAM_INT);
        $stmt->bindValue(':user_id',    $ticketUser->getUser_id(), \PDO::PARAM_INT);
        $stmt->execute();
    }
}
?>