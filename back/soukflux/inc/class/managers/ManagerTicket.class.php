<?php 
/**
 * 
 */
class ManagerTicket extends Manager {
    
    protected $table = "skf_tickets";
    
    protected $id = array(  "ticket_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "ticket_code" => array(),
                                "ticket_creator_id" => array(),
                                "ticket_entity_id" => array(),  
                                "ticket_state_id" => array(),
                                "ticket_priority_id" => array(),
                                "ticket_type_id" => array(),
                                "ticket_title" => array(),
                                "ticket_text" => array(),
                                "ticket_visibility" => array(),
                                "client_type" => array(),
                                "client_id" => array(),
                                "client_name" => array(),
                                "assigned_user_id" => array(),
                                "ticket_token" => array(),
                                "ticket_question_type_id" => array(),
                                "date_relance" => array(),
                                "date_updated" => array("timestamp" => true, "no_update" => true),
                                "date_created" => array("timestamp" => true, "no_update" => true));
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
     
     public function delete(BusinessTicket $ticket)
     {
        $sql = "DELETE FROM skf_tickets  WHERE ticket_id = :ticket_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':ticket_id', $ticket->getTicket_id(), \PDO::PARAM_INT);
        $stmt->execute();
        
        //Cascade... ?
        $sql = "DELETE FROM skf_tickets_users  WHERE ticket_id = :ticket_id";
        $stmt = $this -> prepare($sql);
        $stmt->bindValue(':ticket_id', $ticket->getTicket_id(), \PDO::PARAM_INT);
        $stmt->execute();
     }
     
     public function getAllRecentTickets()
     {
        $sql = 'SELECT * FROM skf_tickets WHERE date_created > (NOW()  - INTERVAL 7 DAY) ';

                  
        $stmt = $this -> prepare($sql);

        $stmt -> execute();
             
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessTicket');
     
        $ticketList = $stmt -> fetchAll();

        return $ticketList; 
     }
     
     public function getAllFilter($filters)
     {
        $sql = 'SELECT * FROM skf_tickets';

        $where = "";
        
        if(!empty($filters['ticket_entity_id'])){
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            $sql .= "$where ticket_entity_id = :ticket_entity_id "; 
        }
        
        if(!empty($filters['ticket_code'])){
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            $sql .= "$where ticket_code = :ticket_code "; 
        }

        if(!empty($filters['ticket_priority_id'])){
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            $sql .= "$where ticket_priority_id = :ticket_priority_id "; 
        }
        
        if(!empty($filters['ticket_state_id'])){
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            $sql .= "$where ticket_state_id = :ticket_state_id "; 
        }
        
        if(!empty($filters['client_name'])){
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            $sql .= "$where client_name LIKE :client_name "; 
        }
                        
        $stmt = $this -> prepare($sql);
        

        if(!empty($filters['ticket_entity_id'])){
            $stmt -> bindValue(":ticket_entity_id",    $filters['ticket_entity_id']);
        }
        
        if(!empty($filters['ticket_code'])){
            $stmt -> bindValue(":ticket_code",    $filters['ticket_code']);
        }

        if(!empty($filters['ticket_priority_id'])){
            $stmt -> bindValue(":ticket_priority_id",    $filters['ticket_priority_id']); 
        }
        
        if(!empty($filters['ticket_state_id'])){
            $stmt -> bindValue(":ticket_state_id",    $filters['ticket_state_id']); 
        }
        
        if(!empty($filters['client_name'])){
            $stmt -> bindValue(":client_name",    "%".$filters['client_name']."%"); 
        }
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, 'BusinessTicket');
        
        $list = $stmt -> fetchAll();

        return $list;
     }
}
?>