<?php 
/**
 * 
 */
class ManagerTicketHistory extends Manager {
    
    protected $table = "skf_tickets_history";
    
    protected $id = array(  "history_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "ticket_id" => array(),
                                "commentaire" => array(),
                                "priority_id_change" => array(),
                                "priority_id_old" => array(),  
                                "state_id_change" => array(),
                                "state_id_old" => array(),
                                "ticket_assign_change" => array(),
                                "ticket_assign_old" => array(),
                                "creator_user_id" => array(),
                                "creator_client_entity_id" => array(),
                                "ticket_history_visibility" => array(),
                                "user_name" => array(),
                                "date_created" => array("timestamp" => true, "no_update" => true));
                                
                                

}
?>