<?php 
/**
 * 
 */
class ManagerEntityType extends Manager {
    
    protected $table = "skf_entity_types";
    
    protected $id = array(  "type_entity_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "table_name" => array(),
                                "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>