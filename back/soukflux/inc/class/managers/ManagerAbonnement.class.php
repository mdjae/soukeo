<?php 
/**
 * 
 */
class ManagerAbonnement extends Manager {

    protected $table = "skf_abonnement";
    
    protected $id = array(  "abonnement_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array(),
                                "abonnement_limit_product" => array());

}
?>