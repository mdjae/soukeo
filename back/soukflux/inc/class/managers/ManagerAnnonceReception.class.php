<?php 
/**
 * 
 */
class ManagerAnnonceReception extends sdb {
	
	public function save(BusinessAnnonceReception $annonceReception)
	{
		$annonceReception->isNew() ? $this->add($annonceReception) : $this->modify($annonceReception);
	}
	
	

	public function getAnnonceReceptionById($annonce_reception_id)
	{
		$sql = "SELECT * FROM skf_annonce_reception WHERE annonce_reception_id = :annonce_reception_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":annonce_reception_id", $annonce_reception_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessAnnonceReception');
	
		if ($annonce_reception = $stmt -> fetch()) {
			return $annonce_reception;
		}else{
			return null;
		}
	}

	public function getAnnonceReceptionByCode($annonce_reception_code)
	{
		$sql = 'SELECT * FROM skf_annonce_reception WHERE annonce_reception_code = :annonce_reception_code';
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':annonce_reception_code', $annonce_reception_code);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessAnnonceReception');
	
		if ($annonce_reception = $stmt -> fetch()) {
			return $annonce_reception;
		}else{
			return null;
		}	
	}
	
	  public function add(BusinessAnnonceReception $annonceReception)
	{
		$sql = "INSERT INTO skf_annonce_reception  (annonce_reception_code, commentaires, date_created) 
		        VALUES (:annonce_reception_code, :commentaires, NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':annonce_reception_code', $annonceReception -> getAnnonce_reception_code());
		$stmt -> bindValue(':commentaires', 		  $annonceReception -> getCommentaires());
		$stmt -> execute();
		
		$annonceReception -> setAnnonce_reception_id ($this -> lastInsertId());
		$annonceReception -> setDate_created(date('Y-m-d H:i:s'));
		$annonceReception -> setDate_updated(date('Y-m-d H:i:s'));
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessAnnonceReception $annonceReception)
	{
		$sql = "UPDATE skf_annonce_reception SET 	annonce_reception_code = :annonce_reception_code, 
													commentaires = :commentaires, 
													status = :status,
													date_reception = :date_reception  
				WHERE annonce_reception_id = :annonce_reception_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':annonce_reception_code', $annonceReception->getAnnonce_reception_code());
		$stmt->bindValue(':commentaires', 			$annonceReception->getCommentaires());
		$stmt->bindValue(':status', 				$annonceReception->getStatus());
		$stmt->bindValue(':date_reception',			$annonceReception->getDate_reception());
		$stmt->bindValue(':annonce_reception_id', 	$annonceReception->getAnnonce_reception_id(), \PDO::PARAM_INT);
		 
		$stmt->execute(); 
		
		$annonceReception -> setDate_updated(date('Y-m-d H:i:s'));
	}
	
	public function delete(BusinessAnnonceReception $annonceReception)
	{
		$sql = "DELETE FROM skf_annonce_reception  WHERE annonce_reception_id = :annonce_reception_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':annonce_reception_id', $annonceReception->getAnnonce_reception_id(), \PDO::PARAM_INT);
		$stmt->execute();
		
		//Cascade : reset champ annoncde_reception_id des items liés à cette annonce de réception
		$sql = "UPDATE skf_order_items SET annonce_reception_id = NULL WHERE annonce_reception_id = :annonce_reception_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':annonce_reception_id', $annonceReception->getAnnonce_reception_id(), \PDO::PARAM_INT);
		$stmt->execute();	
	}
}
?>