<?php 
/**
 * 
 */
class ManagerVendorStatut extends Manager {
    
    protected $table = "skf_vendor_status";
    
    protected $id = array(  "statut_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>