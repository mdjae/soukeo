<?php 
/**
 * 
 */
class ManagerEntityCrm extends Manager {
    
    protected $table = "skf_entity_crm";
    
    protected $id = array(  "entity_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "type_entity_id" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>