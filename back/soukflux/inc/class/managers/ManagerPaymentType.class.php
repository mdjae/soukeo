<?php 
/**
 * 
 */
class ManagerPaymentType extends Manager {
    
    protected $table = "skf_payment_types";
    
    protected $id = array(  "payment_type_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "label" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>