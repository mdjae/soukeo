<?php 
/**
 * 
 */
class ManagerGroup extends sdb {
	
	public function save(BusinessGroup $group)
	{
		$group->isNew() ? $this->add($group) : $this->modify($group);
	}
	
	

	public function getGroupById($group_id)
	{
		$sql = "SELECT * FROM c_group WHERE group_id = :group_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":group_id", $group_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessGroup');
	
		if ($group = $stmt -> fetch()) {
			return $group;
		}else{
			return null;
		}
	}

    public function add(BusinessGroup $group)
	{

	}

    protected function modify(BusinessGroup $group)
	{

	}
	
	public function delete(BusinessGroup $group)
	{

	}
}
?>