<?php 
/**
 * 
 */
class ManagerVendorCsv extends Manager {
    
    protected $table = "skf_vendor_csv";
    
    protected $id = array(  "vendor_csv_id" => array("auto_inc" => true));
                            
    protected $fields = array(  "vendor_csv_name" => array(),
                                "vendor_id" => array(),
                                "auto" => array());
                                
                                
    /**
     * Ici les fonctions spécifiques au métier pour l'accès au données
     */                                
}
?>