<?php 
/**
 * 
 */
class ManagerFactureAddress extends sdb {
	
	public function save(BusinessFactureAddress $factureAddress)
	{
		$factureAddress->isNew() ? $this->add($factureAddress) : $this->modify($factureAddress);
	}
	

	public function getFactureAddressById($facture_address_id)
	{
		$sql = "SELECT * FROM skf_factures_address WHERE facture_address_id = :facture_address_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":facture_address_id", $facture_address_id);
		$stmt -> execute();
		
		$stmt -> setFetchMode(\PDO::FETCH_CLASS , 'BusinessFactureAddress');
	
		if ($factureAddress = $stmt -> fetch()) {
			return $factureAddress;
		}else{
			return null;
		}
	}

	
	  public function add(BusinessFactureAddress $factureAddress)
	{
		$sql = "INSERT INTO skf_factures_address  (name, siret, street, city, zip_code, country,
											date_created) 
		        VALUES (:name, :siret, :street, :city, :zip_code, :country,   
		        							NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':name', 	$factureAddress -> getName());
		$stmt -> bindValue(':siret', 	$factureAddress -> getSiret());
		$stmt -> bindValue(':street', 	$factureAddress -> getStreet());
		$stmt -> bindValue(':city', 	$factureAddress -> getCity());
		$stmt -> bindValue(':zip_code',	$factureAddress -> getZip_code());
		$stmt -> bindValue(':country',	$factureAddress -> getCountry());
		$stmt -> execute();
		
		$factureAddress -> setFacture_address_id ($this -> lastInsertId());
		$factureAddress -> setDate_created(date('Y-m-d H:i:s'));
		$factureAddress -> setDate_updated(date('Y-m-d H:i:s'));
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify(BusinessFactureAddress $factureAddress)
	{
		$sql = "UPDATE skf_factures_address SET 	name = :name,
													siret = :siret,
													street = :street, 
													city = :city, 
													zip_code = :zip_code,
													country = :country
				WHERE facture_address_id = :facture_address_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':name', 			$factureAddress -> getName());
		$stmt -> bindValue(':siret', 			$factureAddress -> getSiret()); 
		$stmt -> bindValue(':street', 			$factureAddress->getStreet());
		$stmt -> bindValue(':city', 				$factureAddress->getCity());
		$stmt -> bindValue(':zip_code', 			$factureAddress->getZip_code());
		$stmt -> bindValue(':country',	$factureAddress -> getCountry());
		$stmt -> bindValue(':facture_address_id', $factureAddress->getFacture_address_id(), \PDO::PARAM_INT);
		 
		$stmt->execute(); 
		
		$factureAddress -> setDate_updated(date('Y-m-d H:i:s'));
	}
	
	public function delete(BusinessFactureAddress $factureAddress)
	{
		$sql = "DELETE FROM skf_factures_address  WHERE facture_address_id = :facture_address_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':facture_address_id', $factureAddress->getFacture_address_id(), \PDO::PARAM_INT);
		$stmt->execute();
	}
}
?>