<?php 
/**
 * 
 */
class Manager extends sdb {
    
    protected $entityName ;
    
    function __construct() {
       $this -> entityName = str_replace("Manager", "Business", get_class($this));
       parent::__construct();
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getFields()
    {
        return $this->fields;
    }
    
    public function getById(array $values)
    {
        $sql = "SELECT * FROM $this->table ";
        $where = "";
        
        foreach ($this -> id as $id_field => $options) {
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            
            $sql .= "$where $id_field = :$id_field ";
        }
        
        
        $stmt = $this -> prepare($sql);
        
        foreach ($this -> id as $id_field => $options) {
            $stmt -> bindValue(":$id_field",    $values[$id_field]);
        }
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, $this -> entityName);
        
        if ($entity = $stmt -> fetch()) {
            return $entity;
        }else{
            return null;
        }
    }

    public function getBy(array $fields = null)
    {
         $sql = "SELECT * FROM $this->table ";
         $where = "";
        
        if($fields){
            
            foreach ($fields as $key => $value) {
                if($where == ""){
                    $where = " WHERE ";
                }else{
                    $where = " AND ";
                }
                
                $sql .= "$where $key = :$key ";
            }
        }
        
        $sql .= " LIMIT 1";
        
        $stmt = $this -> prepare($sql);
        
        if($fields){
            foreach ($fields as $key => $value) {
                $stmt -> bindValue(":$key",    $value);
            }
        }
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, $this -> entityName);
        
        if ($entity = $stmt -> fetch()) {
            return $entity;
        }else{
            return null;
        }

    }	

	
	public function getAll(array $fields = null, $order = "")
	{
	     $sql = "SELECT * FROM $this->table ";
         $where = "";
        
        if($fields){
            
            foreach ($fields as $key => $value) {
                if($value){
                    if($where == ""){
                        $where = " WHERE ";
                    }else{
                        $where = " AND ";
                    }
                    
                    $sql .= "$where $key = :$key ";    
                }
            }
        }
        
        if($order){
            $sql .= " ".$order." ";
        }
        
        $stmt = $this -> prepare($sql);
        
        if($fields){
            foreach ($fields as $key => $value) {
                if($value){
                    $stmt -> bindValue(":$key",    $value);    
                }
            }
        }
        
        $stmt -> execute();
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, $this -> entityName);
        
        $list = $stmt -> fetchAll();

        return $list;

	}
    

    public function save($entity)
    {
        $entity->isNew() ? $this -> add($entity) : $this->modify($entity);
    }
    


	
	  public function add($obj)
	  {
	    $sql = "INSERT INTO $this->table  (";
        $first = true;
        foreach ($this -> fields as $field => $options) {
            $first ? $sql .= " $field " : $sql .= ", $field " ; 
            $first = false;
        }
        foreach ($this -> id as $id => $options) {    
            if(!$options['auto_inc']){
                $first ? $sql .= " $id " : $sql .= ", $id " ; 
                $first = false;    
            }    
        }
        
	    $sql.= " ) VALUES ( "; 
		$first = true;
        
        foreach ($this -> fields as $field => $options) {
            
            if(!$options['default'] && !$options['auto_inc']){
                $first ? $sql .= " :$field " : $sql .= ", :$field " ;    
            }else{
                $first ? $sql .= " ".$options['default']." " : $sql .= ", ".$options['default']." " ;
            }
            $first = false;
        }
        foreach ($this -> id as $id => $options) {    
            if(!$options['auto_inc']){
                $first ? $sql .= " :$id " : $sql .= ", :$id " ; 
                $first = false;    
            }    
        }
        
		$sql.= " ) ";
		
        $stmt = $this -> prepare($sql);
        
        $primary_ai = null;
        
        foreach ($this -> fields as $field => $options) {
            
            if(!$options['default'] && !$options['auto_inc']){
                
                $get = "get".ucfirst($field);
                
                $stmt -> bindValue(":$field", $obj -> $get());
            }
            
            if($options['timestamp']){
                $set = "set".ucfirst($field);
                $obj -> $set(date('Y-m-d H:i:s'));
            }
        }
           
        foreach ($this -> id as $id => $options) {    
            if(!$options['auto_inc']){
                $get = "get".ucfirst($id);
                $stmt -> bindValue(":$id", $obj -> $get());
            }else{
                $primary_ai = $id;
            }    
        }
        
        
		$stmt -> execute();
		
		if($primary_ai) {
            $set = "set".ucfirst($primary_ai);
            $obj -> $set($this -> lastInsertId());
        }
		
	  }
	
	  /**
	   * Permet l'update d'une news
	   */
	  protected function modify($obj)
	{
		
		$sql = "UPDATE $this->table SET ";
        
        $first = true;
        foreach ($this -> fields as $field => $options) {
            
            if(!$options['no_update']){
                
                $first ? $sql .= " $field = :$field " : $sql .= ", $field = :$field " ; 
                $first = false;
            }   
        }
        
        $where = "";
        foreach ($this -> id as $key => $value) {
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            
            $sql .= "$where $key = :$key ";
        }

		$stmt = $this -> prepare($sql);
        
        foreach ($this -> fields as $field => $options) {
            
            if(!$options['no_update']){
                
                $get = "get".ucfirst($field);
                $stmt -> bindValue(":$field", $obj -> $get());
            }
            
            if($options['timestamp'] && !$options['no_update']){
                $set = "set".ucfirst($field);
                $obj -> $set(date('Y-m-d H:i:s'));
            }
        }
        
        foreach ($this -> id as $id => $options) {
            $get = "get".ucfirst($id);
            $stmt -> bindValue(":$id", $obj -> $get());    
            if($options['auto_inc']){
                $primary_ai = $id;   
            }    
        }
        
		$stmt -> execute();  
	}
	
	public function delete($obj)
	{
		$sql = "DELETE FROM $this->table  ";
        
		$where = "";
        foreach ($this -> id as $key => $value) {
            if($where == ""){
                $where = " WHERE ";
            }else{
                $where = " AND ";
            }
            
            $sql .= "$where $key = :$key ";
        }
        
        $stmt = $this -> prepare($sql);
        
        foreach ($this -> id as $id => $options) {
            $get = "get".ucfirst($id);
            $stmt -> bindValue(":$id", $obj -> $get());    
            if($options['auto_inc']){
                $primary_ai = $id;   
            }    
        }
                
		return $stmt->execute();	
	}
}
?>