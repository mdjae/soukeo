﻿<?php 
/**
 * 
 * @author nsa
 *
 */
class Widget extends SystemObjectPersistence
{
	protected $_name = "";
	protected $_picto = "";
	protected $_output = "";
	protected $_box = true;
	protected $_widget_path = "";
	
	public function __construct($name,$title)
	{
		$this->_name = $name;
		$this->_title = $title;
	}
	
	protected function _makeOutput()
	{
		return "";
	}
	
	public function display()
	{
		//$this->_save();
		$b = new UIBox($this->_name,$this->_title,$this->_picto,"NoBg");
		if ($this->_box) $b->drawBoxHeader();
		echo "<div id='widget_" . $this->_name . "'>\n";
		echo $this->_makeOutput();
		echo "</div>\n";
			
		//echo "<script>\n
		//		$('reload_
		//	</script>\n";
	
		if ($this->_box) $b->drawBoxFooter();
	}
	
	public function reload()
	{
		return $this->_makeOutput();
	}
}
?>