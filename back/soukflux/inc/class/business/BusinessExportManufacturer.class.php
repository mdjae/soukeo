<?php 

/**
 * 
 */
class BusinessExportManufacturer extends BusinessExportCSV {
	
	protected 			$path = "/opt/soukeo/export/" ;
	protected static 	$db ;
	protected 			$name = "BusinessExportManufacturer";
	protected 			$description = "Fichier des marques";
	
	
	function __construct() {
		
		parent::__construct($this -> path);

		$this -> fileName = "manufacturers_".date("d-m-Y-H-i-s").".csv" ;		
		
	}
	
	public function makeContent()
	{
		self::$db = new BusinessSoukeoModel();
		
		$listManuf = self::$db -> getAllManufacturer();
		
        $this -> makeLine(array("Marque", "Nb Produits", "Nom fichier"));
		foreach ($listManuf as $manufacturer) {
			$line = array();
			
			
			$line[] = $manufacturer["manufacturer"];
			$line[] = $manufacturer["count"];
            
            $nomfichier = strtolower($manufacturer["manufacturer"]);
            $nomfichier = stripAccents($nomfichier);
            $nomfichier = str_replace(' ', '_', $nomfichier);
            $nomfichier = preg_replace('/[^A-Za-z0-9 _]/', '', $nomfichier);
            $nomfichier = $nomfichier.".jpg";
            
			$line[] = $nomfichier;

			
			$this -> makeLine($line);
		}
	}
	
	public function saveFileEntryInDB()
	{
		self::$db -> saveFileEntry($this->fileName, $this->name, $this->description);
	}
	
}

?>