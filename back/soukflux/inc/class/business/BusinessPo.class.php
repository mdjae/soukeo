<?php

class BusinessPo extends BusinessEntity {
	
	protected static 	$db ;
    protected static    $db_av;
	
	protected $po_id;
	protected $vendor_id;
	protected $order_id;
	protected $increment_id;
	protected $customer_id;
	protected $total_weight;
	protected $total_qty;
	protected $shipping_address_id;
	protected $base_total_value;
	protected $base_shipping_amount;
	protected $base_tax_amount;
    protected $udropship_method;
	protected $date_created;
	protected $date_updated;
	protected $litiges;
	protected $payment;

	

	public function __construct ($po_id = false) {
		
		self::$db = new BusinessSoukeoModel();	
        self::$db_av = new BusinessAvahisModel();
        
		if($po_id){
			$this->po_id = $po_id;
			$data = self::$db -> getPoByPoId($this->po_id);
			$data = $data[0];
			if (!$data){
				return false;
			}else{
				$this -> hydrate($data);
			} 	
		}	
	}
	
	
	public function setPo_id($po_id)
	{
		$this -> po_id = $po_id;
	}
	
	public function setVendor_id($vendor_id)
	{
		$this -> vendor_id = $vendor_id;
	}
	
	public function setOrder_id($order_id){
		$this -> order_id = $order_id;
	}
	
	public function setIncrement_id($increment_id)
	{
		$this -> increment_id = $increment_id ;
	}
	
	public function setCustomer_id($customer_id)
	{
		$this -> customer_id = $customer_id;
	}
	
	public function setTotal_weight($total_weight)
	{
		$this -> total_weight = $total_weight;
	}
	
	public function setTotal_qty($total_qty)
	{
		$this -> total_qty = $total_qty;
	}
	
	public function setShipping_address_id($shipping_address_id)
	{
		$this -> shipping_address_id = $shipping_address_id;
	}
	
	public function setBase_total_value($base_total_value)
	{
		$this -> base_total_value = $base_total_value;
	}
	
	public function setBase_shipping_amount($base_shipping_amount)
	{
		$this -> base_shipping_amount = $base_shipping_amount;
	}
	
	public function setBase_tax_amount($base_taxe_amount)
	{
		$this -> base_tax_amount = $base_taxe_amount;
	}
    
    public function setUdropship_method($udropship_method)
    {
        $this -> udropship_method = $udropship_method;
    }
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
	
	public function setLitiges($litiges)
	{
		if($litiges == "1" | $litiges == "0"){
			$this -> litiges = $litiges;	
		}		
	}

	public function setPayment($payment)
	{
		if($payment == "unpaid" | $payment == "complete" | $payment == "partiel"){
			$this -> payment = $payment;	
		}		
	}
		
	public function getPo_id() 
	{
		return $this -> po_id;
	}
	
	public function getVendor_id()
	{
		return $this -> vendor_id;
	}
	
	public function getOrder_id()
	{
		return $this -> order_id;
	}
	
		
	public function getIncrement_id()
	{
		return $this -> increment_id;
	}
	
	public function getCustomer_id()
	{
		return $this -> customer_id;
	}
	
	public function getTotal_weight()
	{
		return $this -> total_weight;
	}
	
	public function getTotal_qty()
	{
		return $this -> total_qty;
	}
	
	public function getShipping_address_id()
	{
		return $this -> shipping_address_id;
	}
	
	public function getBase_total_value()
	{
		return $this -> base_total_value;
	}

	public function getBase_shipping_amount()
	{
		return $this -> base_shipping_amount;
	}
	
	public function getBase_tax_amount()
	{
		return $this -> base_tax_amount;
	}	
    
    public function getUdropship_method()
    {
        return $this -> udropship_method ;
    }
			
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function getLitiges()
	{
		return $this -> litiges;
	}

	public function getPayment()
	{
		return $this -> payment;
	}
		
	public function save()
	{
		self::$db -> updatePo($this);
	}
	
	public function getAllItems()
	{
		return self::$db -> getAllItemsByPo($this -> getOrder_id(), $this -> getVendor_id());
	}
	
	public function checkPayment()
	{
		$itemList = $this -> getAllItems();
		$itemCount = count($itemList);
		$c = 0;
		foreach ($itemList as $item) {
			if( !$item -> getLitiges() && $item -> getPayment()){
				$c++;
			}
		}
		if($c == 0){
			$this -> setPayment("unpaid");
		}elseif($c > 0 && $c != $itemCount){
			$this -> setPayment("partiel");
		}elseif($c == $itemCount){
			$this -> setPayment("complete");
		}
	}
	
	public function checkFacture()
	{
		$itemList = $this -> getAllItems();
		$itemCount = count($itemList);
		$c = 0;
		foreach ($itemList as $item) {
			if( !$item -> getLitiges() && $item -> getFacture_id()){
				$c++;
			}
		}
		if($c == 0){
			return false;
			//$this -> setPayment("unpaid");
		}elseif($c > 0 && $c != $itemCount){
			return "facture partielle";
			//$this -> setPayment("partiel");
		}elseif($c == $itemCount){
			return "facturé";
			//$this -> setPayment("complete");
		}
	}
    
    public function isShippingRefund()
    {
        $tmp = explode("_", $this->udropship_method, 2);
        
        $shipping_code = $tmp[1];
        
        var_dump($this->udropship_method);
        
        $manager = new ManagerLivraisonType();
        
        if($livraisonType = $manager->getBy(array("shipping_code" => $shipping_code))){
            
            return $livraisonType -> getRefund();
        }else{
            return false;
        }
    }
	
    public function getShipping_info()
    {
        
        $tmp = explode("_", $this->udropship_method, 2);
        
        if($rs = self::$db_av->getUdropship_shipping($tmp[1])){
            
            $rs = $rs[0];
            return $rs['shipping_title']. " - ".$rs['days_in_transit']. " jours";
                
        }else{
            return $tmp[1];
        }
    }
    
	public function getCountComment()
	{
		return count(self::$db -> getAllCommentsForObject($this->getPo_id(), "skf_po"));
	}
}