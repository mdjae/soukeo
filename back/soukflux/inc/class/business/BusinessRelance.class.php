<?php

class BusinessRelance extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $relance_id;
    protected $date_relance;
	protected $type_relance_id;
    protected $user_id;
	protected $ticket_id; 
	protected $date_termine;
    protected $date_created;
    	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
        
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setRelance_id($relance_id)
	{
		$this -> relance_id = $relance_id ;
	}

    public function setDate_relance($date_relance)
    {
        $this -> date_relance = $date_relance ;
    }
    
	public function setType_relance_id($type_relance_id)
	{
		$this -> type_relance_id = $type_relance_id;
	}
    
    public function setUser_id($user_id)
    {
        $this -> user_id = $user_id;
    }	
    
	public function setTicket_id($ticket_id)
	{
		$this -> ticket_id = $ticket_id;
	}
	
	public function setDate_termine($date_termine)
	{
		$this -> date_termine = $date_termine;
	}

    public function setDate_created($date_created)
    {
        $this -> date_created = $date_created;
    }
    	
    public function getRelance_id()
    {
        return $this -> relance_id ;
    }

    public function getDate_relance()
    {
        return $this -> date_relance ;
    }
    
    public function getType_relance_id()
    {
        return $this -> type_relance_id;
    }
    
    public function getUser_id()
    {
        return $this -> user_id;
    }   
    
    public function getTicket_id()
    {
        return $this -> ticket_id;
    }
    
    public function getDate_termine()
    {
        return $this -> date_termine;
    }

    public function getDate_created()
    {
        return $this -> date_created;
    }
    
    public function getTicket()
    {
        $ticketManager = new ManagerTicket();
        
        return $ticketManager -> getById(array("ticket_id" => $this->ticket_id));
    }
    
	public function isNew()
	{
		if ($this -> relance_id){
			return false;
		}else{
			return true;
		}
	}
    
    
	public function save()
	{

	}
	
}