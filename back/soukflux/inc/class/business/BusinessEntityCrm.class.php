<?php

class BusinessEntityCrm extends BusinessEntity {

	protected $entity_id;
	protected $type_entity_id;

	public function __construct ($data = array()) {
	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setEntity_id($entity_id )
	{
		$this -> entity_id = $entity_id ;
	}

	public function setType_entity_id($type_entity_id)
	{
		$this -> type_entity_id = $type_entity_id;
	}
	
	public function getEntity_id()
	{
		return $this -> entity_id;
	}
	
    public function getType_entity_id()
    {
        return $this -> type_entity_id;
    }

	public function isNew()
	{
		if ($this -> getEntity_id()){
			return false;
		}else{
			return true;
		}
	}
       
	public function save()
	{

	}
	
}