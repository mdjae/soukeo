<?php 
/**
 * 
 */
class BusinessCustomerStatut extends BusinessEntity {
	
    
	protected $statut_id;
    protected $label;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setStatut_id($statut_id)
    {
        $this -> statut_id = $statut_id;
    }
    
    public function getStatut_id()
    {
        return $this -> statut_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function isNew()
    {
        $manager = new ManagerCustomerStatut();
        
        if (  $this -> statut_id || ($state = $manager -> getBy(array("label" => $this->label))) ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>