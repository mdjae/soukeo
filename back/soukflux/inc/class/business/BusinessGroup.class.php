<?php

class BusinessGroup extends BusinessEntity {
    
    protected static    $db ;
    
    protected $group_id;
    protected $name;
    protected $descr; 
    protected $email;
    protected $add_date;
    protected $mod_date;
    protected $del_date;
    protected $user_id;


    public function __construct ($data = array()) {
        
        self::$db = new BusinessSoukeoModel();  
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    
    public function setGroup_id($group_id ){
        $this -> group_id = $group_id ;
    }
    
    public function setName($name)
    {
        $this -> name = $name;
    }
    
    public function setDescr($descr)
    {
        $this -> descr = $descr;
    }
    
    public function setEmail($email)
    {
        $this -> email = $email;
    }
    
    public function setAdd_date($add_date)
    {
        $this -> add_date = $add_date;
    }
    
    public function setMod_date($mod_date)
    {
        $this -> mod_date = $mod_date;
    }
    
    public function setDel_date($del_date)
    {
        $this -> del_date = $del_date;
    }
    
    public function setUser_id($user_id)
    {
        $this -> user_id = $user_id;
    }
    
    public function getGroup_id(){
        return $this -> group_id ;
    }
    
    public function getName()
    {
        return $this -> name ;
    }
    
    public function getDescr()
    {
        return $this -> descr ;
    }
    
    public function getEmail()
    {
        return $this -> email ;
    }
    
    public function getAdd_date()
    {
        return $this -> add_date ;
    }
    
    public function getMod_date()
    {
        return $this -> mod_date ;
    }
    
    public function getDel_date()
    {
        return $this -> del_date ;
    }
    
    public function getUser_id()
    {
        return $this -> user_id;
    }
    
    public function isNew()
    {
        if ($this -> getGroup_id()){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}