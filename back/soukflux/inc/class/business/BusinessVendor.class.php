<?php

class BusinessVendor extends BusinessEntity {
	
	protected static 	$db ;
	
    protected $entity_id;
	protected $vendor_id;
	protected $vendor_nom;
	protected $vendor_url;
	protected $active;  
	protected $email;
	protected $street;
	protected $city;
	protected $zip;
    protected $fax;
    protected $region;
	protected $country_id;
	protected $telephone;
	protected $telephone2;
	protected $raisonsocial;
	protected $siret;
	protected $siteinternet;
	protected $civilite;
	protected $vendor_attn;
	protected $vendor_attn2;
	protected $abonnement;
	protected $product_limit;
    protected $vendor_type;
    protected $solde_credit;
    protected $date_debut_abonnement;
    protected $statut;
    protected $birthd;
    protected $livraison_type_id;
    protected $created_at;
    protected $new;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	

		if (!empty($data)) {
			$this->hydrate($data);
		}	
	}
	
    public function setEntity_id($entity_id)
    {
        $this -> entity_id = $entity_id;
    }
	
	public function setVendor_id($vendor_id)
	{
		$this -> vendor_id = $vendor_id;
	}

	public function setVendor_nom($vendor_nom)
	{
		$this -> vendor_nom = $vendor_nom;
	}
	
	public function setVendor_url($vendor_url)
	{
		$this -> vendor_url = $vendor_url;
	}
	
	public function setActive($active)
	{
		$this -> active = $active;
	}
	
	public function setEmail($email)
	{
	    if(trim($email) == ""){
	        $this->email = $this->entity_id;
	    }
        else{
            $this ->email = $email;    
        }
	}

	public function setStreet($street)
	{
		$this -> street = $street;
	}
	
	public function setCity($city)
	{
		$this -> city = $city;
	}
			
	public function setZip($zip)
	{
		$this -> zip = $zip;
	}

    public function setRegion($region)
    {
        $this -> region = $region;
    }
    	
	public function setCountry_id($country_id)
	{
		$this -> country_id = $country_id ;
	}
	
	public function setTelephone($telephone)
	{
		$this -> telephone = $telephone;
	}
    
    public function setFax($fax)
    {
        $this -> fax = $fax;
    }
    
    public function setNew($new)
    {
        $this -> new = $new;
    }
	
	public function setTelephone2($telephone2)
	{
		$this -> telephone2 = $telephone2;
	}
		
	public function setRaisonsocial($raisonsocial)
	{
		$this -> raisonsocial= $raisonsocial;
	}
	
	public function setSiret($siret)
	{
		$this -> siret = $siret;
	}
	
	public function setSiteinternet($siteinternet)
	{
		$this -> siteinternet = $siteinternet;
	}
	
	public function setCivilite($civilite)
	{
		$this -> civilite = $civilite;	
		
	}
	
	public function setVendor_attn($vendor_attn)
	{
		$this -> vendor_attn = $vendor_attn;
	}
	
	public function setVendor_attn2($vendor_attn2)
	{
		$this -> vendor_attn2 = $vendor_attn2;
	}
	
	public function setAbonnement($abonnement)
	{
		$this -> abonnement = $abonnement;
	}
	
	public function setProduct_limit($product_limit)
	{
		$this -> product_limit = trim($product_limit);
	}

    public function setVendor_type($vendor_type)
    {
        $this -> vendor_type = $vendor_type;
    }
    
    public function setSolde_credit($solde_credit)
    {
        $this -> solde_credit = $solde_credit;
    }
    
    public function setDate_debut_abonnement($date_debut_abonnement)
    {
        $this -> date_debut_abonnement = $date_debut_abonnement;
    }
    
    public function setStatut($statut)
    {
        $this -> statut = $statut;
    }
    
    public function setBirthd($birthd)
    {
        $this -> birthd = $birthd;
    }
    
    public function setLivraison_type_id($livraison_type_id)
    {
        $this -> livraison_type_id = $livraison_type_id;
    }
            
    public function setCreated_at($created_at)
    {
        $this -> created_at = $created_at;
    }
	
	public function getVendor_id()
	{
		return $this -> vendor_id ;
	}

    public function getEntity_id()
    {
        return $this -> entity_id ;
    }
    
	public function getVendor_nom()
	{
		return $this -> vendor_nom ;
	}
	
	public function getVendor_url()
	{
		return $this -> vendor_url ;
	}
	
	public function getActive()
	{
		return $this -> active ;
	}
	
	public function getEmail()
	{
        return $this ->email ;    
	}
    
    public function getEmail_str()
    {
        if($this->email == $this->entity_id){
            return "";
        }
        else{
            return $this->email;
        }
    }
    
	public function getStreet()
	{
		return $this -> street ;
	}
	
	public function getCity()
	{
		return $this -> city ;
	}
			
	public function getZip()
	{
		return $this -> zip ;
	}

    public function getRegion()
    {
        return $this -> region ;
    }
    
    public function getFax()
    {
        return $this -> fax;
    }
    	
	public function getCountry_id()
	{
		return $this -> country_id ;
	}
	
	public function getTelephone()
	{
		return $this -> telephone ;
	}
	
	public function getTelephone2()
	{
		return $this -> telephone2 ;
	}
		
	public function getRaisonsocial()
	{
		return $this -> raisonsocial;
	}
	
	public function getSiret()
	{
		return $this -> siret ;
	}
	
	public function getSiteinternet()
	{
		return $this -> siteinternet ;
	}
	
	public function getCivilite()
	{
		return $this -> civilite ;	
		
	}
	
	public function getVendor_attn()
	{
		return $this -> vendor_attn ;
	}
	
	public function getVendor_attn2()
	{
		return $this -> vendor_attn2 ;
	}
	
	public function getAbonnement()
	{
		return $this -> abonnement;
	}
	
	public function getProduct_limit()
	{
		return $this -> product_limit;
	}
    
    public function getVendor_type()
    {
        return $this -> vendor_type;
    }
    
    public function getSolde_credit()
    {
        return $this -> solde_credit;
    }
    
    public function getDate_debut_abonnement()
    {
        return $this -> date_debut_abonnement;
    }
    
    public function getStatut()
    {
        return $this -> statut;
    }
    
    public function getBirthd()
    {
        return $this -> birthd;
    }

    public function getLivraison_type_id()
    {
        return $this -> livraison_type_id;
    }
        
    public function getCreated_at()
    {
        return $this -> created_at;
    }

    public function getStatut_label()
    {
        $statutManager = new ManagerVendorStatut();
        return ucfirst($statutManager -> getById(array("statut_id" => $this -> statut)) -> getLabel());
    }

    public function getLivraison_type_label()
    {
        $manager = new ManagerLivraisonType();
        
        if ($type = $manager -> getById(array("type_id" => $this -> livraison_type_id))){
            return ucfirst($type -> getLabel());    
        }else{
            return "Non défini";
        }
    }
            	
	public function save()
	{
		
	}
	
    public function isNew()
    {
        $vendorManager = new ManagerVendor();

        if ( $this->entity_id || ($mail = $vendorManager -> getBy(array("email" => $this -> email))) ){
            
            return false;
        }else{
            return true;
        }  
    }
    
	public function getAllPo()
	{
		return self::$db -> getAllPoByVendorId($this -> vendor_id);
	}
    
    public function getCA()
    {
        $all_po = self::$db -> getAllPoCompleteByVendorId  ($this->vendor_id);
        
        $ca = 0;
        
        foreach ($all_po as $po) {
            $ca += (float)$po->getBase_total_value();
            $ca += (float)$po->getBase_tax_amount();
            $ca += (float)$po->getBase_shipping_amount();
        }
        
        return $ca;
    }
	
	public function getAllPoToPay()
	{
		$date = dbQueryOne('SELECT date_value FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
		$date = $date['date_value'];

				
		return $poList = self::$db -> getAllPoByVendorAfterDate($this -> getVendor_id(), $date); 
	}
    
    public function getAllShippingMethod()
    {
        if($rs = dbQueryAll('SELECT DISTINCT (udropship_method) FROM skf_po WHERE vendor_id = "'.$this->vendor_id.'"')){
            foreach ($rs as $shipping) {
                $method = explode("_",$shipping['udropship_method'],2);
                $method[1] != "" ? $str .= "[".$method[1] . "]<br/>" : "";    
            }
            return $str;    
        }
        else{
            return "<em>Aucun</em>";   
        }
    }
    
    public function getCreditsHistory()
    {
        $creditsManager = new ManagerCreditsHistory();
        
        return $creditsManager -> getAllCreditsHistoryByVendor($this->vendor_id);
    }
}