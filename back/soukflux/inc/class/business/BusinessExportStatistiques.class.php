<?php 

/**
 * 
 */
class BusinessExportStatistiques extends BusinessExportCSV {
	
	protected 			$path = "/opt/soukeo/export/attributs/" ;
	protected 			$name = "BusinessExportAttributsFromUnivers";
	protected 			$description = "Fichier contenant les attributs pour chaque catégories des univers";
	protected           $categorie;
	
	function __construct() {

		parent::__construct($this -> path);
        

		$this -> fileName = "Stats_".date("d-m-Y-H-i-s").".csv" ;		
		
	}
	
	public function makeContent()
	{
	    $november = new DateTime('November 2013');
        $interval = new DateInterval('P1M');
        $now = new DateTime();
        //Période de novembre 2013($november) à maintenant (new DateTime())
        $period   = new DatePeriod($november, $interval, $now->modify('+3 month'));

        $c = 1;
        $first = true; 
        
        $this->makeHeader();
        foreach($period as $dt) {
            $line = array();
            $line[] = $dt->format("M Y");
            
            $stats = new BusinessStatistiqueModel($dt->format('Y-m-d '), $dt->modify('+1 month')->modify('-1 days')->format('Y-m-d ')) ;

            $po_av = $stats->getCount_po_av();
            $po_v = $stats->getCount_po_v();
            $po_tot = $stats->getCount_po(); 
            $orders_tot = $stats->getCount_orders();
            $qty_item_tot = $stats->getTotal_qty_items();
            $price_tot_v = $stats->getTotal_value_po_v();
            $price_tot_av = $stats->getTotal_value_po_av();   
            
            $line[] = $po_tot;
            $line[] = numberByLang(($po_av / $po_tot*100))."%  (".$po_av.")";
            $line[] = numberByLang(($po_v / $po_tot*100))."%  (".$po_v.")"  ;
            $line[] = numberByLang($stats->getCa())." €" ;
            $line[] = numberByLang($stats->getCa_v())." €" ;
            $line[] = numberByLang($qty_item_tot / $orders_tot)." produits/panier" ;
            $line[] = numberByLang($price_tot_v / $po_v)." €" ;
            $line[] = numberByLang($price_tot_av / $po_av)." €" ;    
            $line[] = $stats->getCount_new_vendor() ;
            $line[] = $stats->getCount_panier_abdn();
            $line[] = numberByLang($stats->getTotal_comm())." €";

            $this -> makeLine($line);
        }

	}

    public function makeHeader()
    {
        $line[] = "Mois";
        $line[] = "Nb ventes totales";
        $line[] = "Nb ventes avahis";
        $line[] = "Nb ventes vendeur";
        $line[] = "CA plateforme";
        $line[] = "CA vendeurs";
        $line[] = "Panier moyen (nb produits)";
        $line[] = "Valeur panier moyen vendeur";
        $line[] = "Valeur panier moyen avahis";
        $line[] = "Nouveaux vendeurs";
        $line[] = "Nb paniers abandonnés";
        $line[] = "Total commissions";
        
        $this -> makeLine($line);
    }
	
	public function saveFileEntryInDB()
	{
		self::$db -> saveFileEntry($this->fileName, $this->name, $this->description);
	}
	
}

?>