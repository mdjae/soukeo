<?php 
/**
 * 
 */
class BusinessLivraisonType extends BusinessEntity {
	
    
	protected $shipping_id;
    protected $shipping_code;
    protected $shipping_title;
    protected $days_in_transit;
    protected $refund;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setShipping_id($shipping_id)
    {
        $this -> shipping_id = $shipping_id;
    }
    
    public function setShipping_code($shipping_code)
    {
        $this -> shipping_code = $shipping_code;
    }
    
    public function setShipping_title($shipping_title)
    {
        $this -> shipping_title = $shipping_title;
    }
    
    public function setDays_in_transit($days_in_transit)
    {
        $this -> days_in_transit = $days_in_transit;
    }
    
    public function setRefund($refund)
    {
        $this ->refund = $refund;
    }
    
    public function getShipping_id()
    {
        return $this -> shipping_id ;
    }
    
    public function getShipping_code()
    {
        return $this -> shipping_code;
    }
    
    public function getShipping_title()
    {
        return $this -> shipping_title;
    }
    
    public function getDays_in_transit()
    {
        return $this -> days_in_transit;
    }
    
    public function getRefund()
    {
        return $this ->refund;
    }
    
    
    public function isNew()
    {
        if ($this -> shipping_id ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>