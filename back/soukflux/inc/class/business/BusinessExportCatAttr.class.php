<?php 

/**
 * 
 */
class BusinessExportCatAttr extends BusinessExportCSV {
	
	protected 			$path = "/opt/soukeo/export/" ;
	protected static 	$db ;
	protected 			$name = "BusinessExportCatAttr";
	protected 			$description = "Fichier des catégories avec attributs";
	
	
	function __construct() {
		
		parent::__construct($this -> path);

		$this -> fileName = "cat_".date("d-m-Y-H-i-s").".csv" ;		
		
	}
	
	public function makeContent()
	{
		self::$db = new BusinessSoukeoGrossisteModel();
		
		$listCat = self::$db -> getAllCatInfo();
		
		foreach ($listCat as $cat) {
			$line = array();
			
		//	$count = self::$db -> countProdInCat($cat['cat_id']);
			
		//	$line[] = $count;
			
		//	if(self::$db -> checkIfCatLastLevel($cat['cat_id'])) {
		//		$last = "X";
		//	}else{
		//		$last = "0";
		//	}

		//	$line[] = $last;
			
			if(self::$db -> checkIfCatLastLevel($cat['cat_id'])){
				//On récupère la chaine de caractère du type Cat > SCat > SSCat
				$tmp = self::$db -> getStrCateg($cat['cat_id']);
				$tmp = explode(" > ", $tmp);
				
				foreach ($tmp as $scat) {
					$line[] = $scat;
				}
						$this -> makeLine($line);
			}else{
				//$line[] = $cat['cat_label'];
			}
			
	
		}
	}
	
	protected function search($array, $key, $value) 
	{ 
	    $results = array(); 
	
	    if (is_array($array)) 
	    { 
	        if (isset($array[$key]) && $array[$key] == $value) 
	            $results[] = $array; 
	
	        foreach ($array as $subarray) 
	            $results = array_merge($results, $this -> search($subarray, $key, $value)); 
	    } 
	
	    return $results; 
	} 
	
	public function saveFileEntryInDB()
	{
		self::$db -> saveFileEntry($this->fileName, $this->name, $this->description);
	}
	
}

?>