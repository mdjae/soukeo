<?php

class BusinessGrossisteECOPRESTO {

	protected $delimiter ;
	
	protected $errors = array();

	public function __construct ()
	{
		$this -> delimiter = ";";
		
	}


    /**
     * Fonction principal du batch
     */
    public function errorsExist($product) {

        if ($product->title == "") {
            $message .= " NOM PRODUIT INVALIDE ";
        } elseif ($product->poids_brut == "" | $product->poids_brut == 0) {
            $message .= " POIDS INVALIDE ";
        } elseif ($product->ean == "" && $product->reference == "") {
            $message .= " EAN ET REF INEXISTANT "; 
        } elseif ($product->stock == "" | $product->stock == "0") {
            $message .= " PRODUIT PAS EN STOCK"; 
        }


        if ($message != "") {
            $this->errors [] = $message . " POUR produit EAN : " . $product->ean . " RefGrossiste : " . $product->reference . "";
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fonction principal du batch
     */
    public function formatFields($product, $prodGrOld = "") {

		//Données récupérée si poids déjà présent auparavant (cas où on a entré un poids à la main
		//après avoir importé)
		/*if($prodGrOld != null){
			$product['weight'] = $prodGrOld['WEIGHT'];
			$product['long_desc'] = $prodGrOld['DESCRIPTION'];
			$product['short_desc'] = $prodGrOld['DESCRIPTION_SHORT'];
		}
		else{*/
			//Si le poids volumétrique est supérieur au poids classique on prend le poids volumétrique
			
			
			$product['weight'] 		= (float)$product->poids_brut;
			
			$product['long_desc'] 	= BusinessEncoding::toUTF8($product->description_longue_1);
			$product['short_desc'] 	= BusinessEncoding::toUTF8($product->description_courte_1);
			
		//}
		
		$product['manufacturer']= BusinessEncoding::toUTF8($product->marque);
		$product['price'] 		= (float)$product->prix_achat_ttc;
		$product['ref'] 		= $product->reference;
		
		$product['picture1'] 	= $product->url_img_1;
		$product->url_img_2 ? $product['picture2'] 	= $product->url_img_2 : $product['picture2'] 	=  $product['picture1'];
		$product->url_img_3 ? $product['picture3'] 	= $product->url_img_2 : $product['picture3'] 	=  $product['picture2'];
		
		$product['name'] 		= BusinessEncoding::toUTF8($product->title);
		$product['ean'] = $product->ean;
		$product['stock'] = $product->stock;
		
        //Gestion categories

		if($product->famille_2)
     		$tmp = BusinessEncoding::toUTF8( $product->famille_1 . " > " . $product->famille_2) ;
		else
			$tmp = BusinessEncoding::toUTF8( $product->famille_1 ); 
		$product['category'] = $tmp;


        return $product;
    }

    public function cleanDescription($product) {
		
		$product['ref'] 		= $product->reference;
        return $product;
    }
	
	public function getErrors() 
	{
		return $this -> errors;
	}
	
	public function getDelimiter()
	{
		return $this -> delimiter;
	}

}

?>
