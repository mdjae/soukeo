<?php 
/**
 * Model pour les tables soukflux dédiées à PRESTASHOP
 * 
 * @version 0.1.0
 * @author 	Philippe_LA
 */
class BusinessSoukeoCarshopModel extends BusinessSoukeoGrossisteModel {
	
	public function getDistinctAttr($cat)
	{
		$sql = "SELECT DISTINCT CAR_MODEL FROM 
				sfk_produit_grossiste WHERE CATEGORY LIKE :CATEGORY ORDER BY CAR_MODEL ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":CATEGORY", $cat."%");
		
		$rs['CAR_MODEL'] = $this->getAllPrepa($stmt);
		
		$sql = "SELECT DISTINCT COLOR FROM 
				sfk_produit_grossiste WHERE CATEGORY LIKE :CATEGORY ORDER BY COLOR ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":CATEGORY", $cat."%");
		
		$rs['COLOR'] = $this->getAllPrepa($stmt);
		
		return $rs;
	}
}
	