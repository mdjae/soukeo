<?php 
/**
 * Model pour les tables soukflux dédiées aux grossistes
 * 
 * @version 0.1.0
 * @author 	Philippe_LA
 */
class BusinessSoukeoGrossisteModel extends BusinessSoukeoModel {
		
	protected static $stmtProduitGR;
	protected static $stmtCountAll;
	protected $stmtProdAttrGR;
	protected $stmtDoublonIncorrect;
	protected $stmtPrixGrossiste;
	protected $stmtGetDoublonsEAN;
	protected $stmtMajPoidsGrossiste;
	
	/**
	 * Préparation de l'insertion / update des produits du vendeur dans sfk_produits_ps
	 * Il y a également une remise à 0 du champ Active avant la nouvelle insertion ou update
	 * ainsi, même si l'eCommercant a changé d'avis dans ce qu'il veux exporter les produits
	 * restent en base de donnée et seuls ceux actifs iront vers Avahis
	 */
	public function prepareInsertSfkProdGR($dealerID)
	{
		
		$sql = "INSERT INTO sfk_produit_grossiste  (GROSSISTE_ID, REF_GROSSISTE, EAN, UVP_PRICE,
												PRICE_PRODUCT, VOLUMETRIC_WEIGHT,
												NAME_PRODUCT, CATEGORY, DESCRIPTION,
												DESCRIPTION_SHORT, QUANTITY, URL, IMAGE_PRODUCT,IMAGE_PRODUCT_2,
												IMAGE_PRODUCT_3, MANUFACTURER, CAR_MODEL, BRAND, COLOR, WEIGHT, AJOUTE, ACTIVE, DATE_CREATE)
												
					 				VALUES (:GROSSISTE_ID, :REF_GROSSISTE, :EAN, :UVP_PRICE,
												:PRICE_PRODUCT, :VOLUMETRIC_WEIGHT, 
												:NAME_PRODUCT, :CATEGORY, :DESCRIPTION,
												:DESCRIPTION_SHORT, :QUANTITY, :URL, :IMAGE_PRODUCT,:IMAGE_PRODUCT_2,
												:IMAGE_PRODUCT_3, :MANUFACTURER, :CAR_MODEL, :BRAND, :COLOR, :WEIGHT, :AJOUTE, 1, NOW())
												
									ON DUPLICATE KEY UPDATE 
												
												GROSSISTE_ID			=VALUES(GROSSISTE_ID),
												REF_GROSSISTE 			=VALUES(REF_GROSSISTE),
												EAN						=VALUES(EAN),
												UVP_PRICE				=VALUES(UVP_PRICE),
												PRICE_PRODUCT			=VALUES(PRICE_PRODUCT),
												
												VOLUMETRIC_WEIGHT 		=VALUES(VOLUMETRIC_WEIGHT), 
												
												NAME_PRODUCT 			=VALUES(NAME_PRODUCT),
												CATEGORY 				=VALUES(CATEGORY),
												DESCRIPTION 			=VALUES(DESCRIPTION),
												DESCRIPTION_SHORT		=VALUES(DESCRIPTION_SHORT),
												QUANTITY				=VALUES(QUANTITY),
												URL 					=VALUES(URL),
												IMAGE_PRODUCT 			=VALUES(IMAGE_PRODUCT),
												IMAGE_PRODUCT_2 		=VALUES(IMAGE_PRODUCT_2),
												IMAGE_PRODUCT_3			=VALUES(IMAGE_PRODUCT_3),
												MANUFACTURER 			=VALUES(MANUFACTURER),
												CAR_MODEL 				=VALUES(CAR_MODEL),
												BRAND 					=VALUES(BRAND),
												COLOR 					=VALUES(COLOR),
												WEIGHT					=VALUES(WEIGHT),
												AJOUTE 					=VALUES(AJOUTE),
												ACTIVE					=VALUES(ACTIVE)";
												
		$this->stmtProduitGR = $this->prepare($sql);
	}
	
	/**
	 * Fonction permettant l'insertion ou l'update de produits dans la base de donnée soukflux 
	 * Si le couple GROSSISTE_ID/ID_PRODUCT existe déjà il y a juste update du produit.
	 * Changements à effectuer dans les champs récupérés, besoin d'affiner
	 */
	public function addRowSfkProdGR($product, $dealerID)
	{
		//$prodExistant = $this -> getProductByEAN($dealerID, $product['ean']);
		$prodExistant = $this -> getProductByREF($dealerID, $product['ref']);
		
		if($prodExistant != null){
			$alreadyExist = true;
		} 
		
		if($product['ean'] != ""){
			//SKU Avahis vient de l'EAN 
			if($id_avahis = $this -> getIdProduct($product['ean'])){
				$ajoute = 1;			
			}
			else {
				$ajoute = 0;
			}	
		}else{
			//SKU Avahis vient de la REF_GROSSISTE
			if($id_avahis = $this -> getIdProduct($product['ref'])){
				$ajoute = 1;			
			}
			else {
				$ajoute = 0;
			}
		}
		
		
		$product['weight'] ? $product['weight'] : $product['weight'] = 0;
		
		if($product['ean'] != "" | $product['ref'] != "" ) {
			$this->stmtProduitGR->bindValue(':GROSSISTE_ID', 	   		$dealerID);
			$this->stmtProduitGR->bindValue(':REF_GROSSISTE', 	   		$product['ref']);
			$this->stmtProduitGR->bindValue(':EAN', 					$product['ean'] ? $product['ean'] : 'NULL');
			$this->stmtProduitGR->bindValue(':UVP_PRICE', 				$product['uvp_price'] ? str_replace(",", ".", $product['uvp_price']) : 'NULL');
			$this->stmtProduitGR->bindValue(':PRICE_PRODUCT', 			str_replace(",", ".", $product['price']));
			$this->stmtProduitGR->bindValue(':VOLUMETRIC_WEIGHT', 		$product['poidsVolum'] ? $product['poidsVolum'] : str_replace(",", ".", $product['weight']));
			$this->stmtProduitGR->bindValue(':NAME_PRODUCT', 			$product['name'] != "" ? $product['name'] : $product['id']);
			$this->stmtProduitGR->bindValue(':CATEGORY', 				$product['category'] != "" ? $product['category'] : "Non classé");
			$this->stmtProduitGR->bindValue(':DESCRIPTION', 			$product['long_desc'] ? $product['long_desc'] : $product['name']);
			$this->stmtProduitGR->bindValue(':DESCRIPTION_SHORT',		$product['short_desc'] ? $product['short_desc'] : $product['name']);
			$this->stmtProduitGR->bindValue(':QUANTITY', 				$product['stock'] ? $product['stock'] : 'NULL');
			$this->stmtProduitGR->bindValue(':URL',						$product['url'] ? $product['url'] : 'NULL');
			$this->stmtProduitGR->bindValue(':IMAGE_PRODUCT',			$product['picture1'] ? $product['picture1'] : 'NULL');
			$this->stmtProduitGR->bindValue(':IMAGE_PRODUCT_2', 		$product['picture2'] ? $product['picture2'] : 'NULL');
			$this->stmtProduitGR->bindValue(':IMAGE_PRODUCT_3', 		$product['picture3'] ? $product['picture3'] : 'NULL');
			$this->stmtProduitGR->bindValue(':MANUFACTURER', 			$product['manufacturer'] ? ucfirst(strtolower($product['manufacturer'])) : 'NULL');
			$this->stmtProduitGR->bindValue(':CAR_MODEL', 				$product['car_model'] ? $product['car_model'] : 'NULL');
			$this->stmtProduitGR->bindValue(':BRAND', 					$product['Brand'] ? $product['Brand'] : 'NULL');
			$this->stmtProduitGR->bindValue(':COLOR', 					$product['Color'] ? $product['Color'] : 'NULL');
			$this->stmtProduitGR->bindValue(':WEIGHT', 					str_replace(",", ".", $product['weight']));
			$this->stmtProduitGR->bindValue(':AJOUTE', 					$ajoute);
			
			$sqlState = $this->stmtProduitGR->execute();
			
			$prod_id = $this -> lastInsertId();
			
			//Si nouveau produit
			if($prod_id && $ajoute){
				$this -> assocProductGrossiste($prod_id, $id_avahis, $dealerID) ;
			}
			elseif($ajoute && $prodExistant != null){
				$prodExistant = $prodExistant[0];
				$this -> assocProductGrossiste($prodExistant['ID_PRODUCT'], $id_avahis, $dealerID) ;
			}
			
			//UPDATE
			if( $alreadyExist && $sqlState == 1){
				return 2;
			}
			//FAIL
			elseif($sqlState == 0){
				return 0;
			}//INSERT
			elseif($sqlState == 1 && $prod_id){
				return 1;
			}				
		}

	}		


	public function updateValCalculProductGrossiste($product)
	{
		$sql = "UPDATE sfk_prix_grossiste  
				SET PRIX_LIVRE = :PRIX_LIVR, 
				    OM = :OM,
				    PERCENT_MARGE = :PERCENT_MARGE, 
				    PERCENT_TVA = :PERCENT_TVA,
				    PRIX_FINAL = :PRIX_FINAL,
				    COEF = :COEF,
				    RATIO_RUN_FR = :RATIO_RUN_FR,  
				    PRIX_LIVRAISON = :PRIX_LIVRAISON , 
				    RATIO_TRANSPORT = :RATIO_TRANSPORT   
				WHERE ID_PRODUCT = :ID_PRODUCT";
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':PRIX_LIVR', 		$product['PRIX_LIVR']);
		$stmt -> bindValue(':OM', 				$product['OM']);
		$stmt -> bindValue(':PERCENT_MARGE', 	$product['PERCENT_MARGE']);
		$stmt -> bindValue(':PERCENT_TVA', 		$product['PERCENT_TVA']);
		$stmt -> bindValue(':PRIX_FINAL', 		str_replace(",", ".", $product['PRIX_FINAL']));
		$stmt -> bindValue(':COEF', 			$product['COEF']);
		$stmt -> bindValue(':RATIO_RUN_FR', 	$product['RATIO_RUN_FR']);
		$stmt -> bindValue(':PRIX_LIVRAISON', 	$product['PRIX_LIVRAISON']);
		$stmt -> bindValue(':RATIO_TRANSPORT', 	$product['RATIO_TRANSPORT']);
		$stmt -> bindValue(':ID_PRODUCT', 		$product['ID_PRODUCT']);
		
		return $stmt -> execute();
	}
	
	public function setSpecificValProdGrossiste($spec)
	{
		$sql = "UPDATE sfk_prix_grossiste  
				SET OM_PROD = :OM_PROD,
				    MARGE_PROD = :MARGE_PROD, 
				    TVA_PROD = :TVA_PROD   
				WHERE ID_PRODUCT = :ID_PRODUCT";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':OM_PROD', 			$spec['OM_PROD'] ? $spec['OM_PROD'] : null);
		$stmt -> bindValue(':MARGE_PROD', 		$spec['MARGE_PROD'] ? $spec['MARGE_PROD'] : null);
		$stmt -> bindValue(':TVA_PROD', 		$spec['TVA_PROD'] ? $spec['TVA_PROD'] : null );
		$stmt -> bindValue(':ID_PRODUCT', 		$spec['ID_PRODUCT']);
		
		return $stmt -> execute();
	}
	
	/**
	 * Fonction permettant d'associer un produit d'un ecommercant avec une fiche produit Avahis
	 * @param idProdEcom 	id du produit de l'ecommercant
	 * @param idProdAv 		id du produit Avahis associé
	 * @param vendorID 		id de l'ecommercant
	 */
	public function assocProductGrossiste($idProdGr, $idProdAv, $grossisteId)
	{
		$sql = "INSERT INTO sfk_assoc_product_grossiste ( id_avahis , GROSSISTE_ID, id_produit_grossiste ) 
				values ( :id_avahis, :GROSSISTE_ID, :id_produit_grossiste) 
				ON DUPLICATE KEY UPDATE 
				id_avahis			=VALUES(id_avahis)";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':id_avahis', $idProdAv);
		$stmt -> bindValue(':id_produit_grossiste', $idProdGr);
		$stmt -> bindValue(':GROSSISTE_ID', $grossisteId);
		
		$stmt -> execute() ;
		
		return true;
	}
	
	
	
	public function getDistinctCat($idGrossiste)
	{
		$sql = "SELECT DISTINCT CATEGORY FROM sfk_produit_grossiste WHERE GROSSISTE_ID= :GROSSISTE_ID AND ACTIVE = 1 ORDER BY CATEGORY ";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idGrossiste);
		
		return $this->getAllPrepa($stmt);

	}	
	
	/**
	 * Renvoie un produit Grosiste pour un vendeur donné ou pas
	 */
	public function getProduct($grossisteID="", $idProduct)
	{
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE ID_PRODUCT = :ID_PRODUCT ";
		if($grossisteID != ""){
			$sql .= " AND GROSSISTE_ID = :GROSSISTE_ID ";
		}
		//$sql.= " LIMIT 1";
		
		$stmt = $this->prepare($sql);
		
		if($grossisteID != ""){
			$stmt -> bindValue(':GROSSISTE_ID' , $grossisteID);
		}
		$stmt -> bindValue(':ID_PRODUCT' , $idProduct);
		//$stmt -> execute() ;
		
		return $this -> getAllPrepa($stmt);
	}
	
	
	/**
	 * Renvoie un produit Grosiste pour un vendeur donné ou pas
	 */
	public function getProductByEAN($grossisteID="", $ean)
	{
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE EAN = :EAN ";
		if($grossisteID != ""){
			$sql .= " AND GROSSISTE_ID = :GROSSISTE_ID ";
		}
		
		
		$stmt = $this->prepare($sql);
		
		if($grossisteID != ""){
			$stmt -> bindValue(':GROSSISTE_ID' , $grossisteID);
		}
		$stmt -> bindValue(':EAN' , trim($ean));
		
		
		return $this -> getAllPrepa($stmt);
	}
	
	/**
	 * Renvoie un produit Grosiste pour un vendeur donné ou pas
	 */
	public function getProductByREF($grossisteID="", $ref)
	{
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE REF_GROSSISTE = :REF_GROSSISTE ";
		if($grossisteID != ""){
			$sql .= " AND GROSSISTE_ID = :GROSSISTE_ID ";
		}
		
		
		$stmt = $this->prepare($sql);
		
		if($grossisteID != ""){
			$stmt -> bindValue(':GROSSISTE_ID' , $grossisteID);
		}
		$stmt -> bindValue(':REF_GROSSISTE' , trim($ref));
		
		
		return $this -> getAllPrepa($stmt);
	}
	public function getAllUnassocProducts($cat, $idGrossiste, $brand=null, $priceMin=null, $priceMax=null, $search="", $context="", $order="", $attr= null)
	{
		$sql = "SELECT *, EAN AS REFERENCE_PRODUCT FROM sfk_produit_grossiste as p
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID AND ACTIVE = 1 
				";
				
		if($cat != ""){
			$sql.=" AND p.CATEGORY LIKE :CATEGORY ";
		}		
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= "AND p.AJOUTE <> 1 
					AND p.refus = 0";
		}
				
		if($context == "refused"){
			$sql.= " AND p.refus = 1 ";
		} 
		
		if($context == "assoc"){
			$sql.= "AND p.AJOUTE = 1 ";
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, DESCRIPTION_SHORT) AGAINST (:NAME_PRODUCT) ";	
		}
		
		
		
		//GESTION DES ATTRIBUTS POUR GROSSISTES
		if (is_array($attr) && !empty($attr)) {
			
			foreach ($attr as $a) {
								
				//Si on a bien entré ce critère et qu'il correspond
				if ($a['val'] != "--"){
					
					$sql .=" AND ".$a['id']." = '".$a['val']. "' ";
					
				}		
			}
		}
		
		if($order == ""){
			$sql .= " ORDER BY NAME_PRODUCT ";	
		}elseif($order == "asc"){
			$sql .= " ORDER BY CAST(PRICE_PRODUCT AS DECIMAL(10,5)) ASC ";
		}elseif($order == "desc"){
			$sql .= " ORDER BY CAST(PRICE_PRODUCT AS DECIMAL(10,5)) DESC ";
		}
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idGrossiste);
		if($cat != ""){
			$stmt -> bindValue(":CATEGORY", $cat."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		
		return $this->getAllPrepa($stmt);
	}
	
	
	public function getAllAssocProds($idGrossiste, $categ = "", $brand=null, $priceMin=null, $priceMax=null, $search=null)
	{
		$sql = "SELECT * FROM sfk_produit_grossiste as p
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID
				AND
				p.AJOUTE = 1 AND ACTIVE = 1 
				";
				
		if($categ!= ""){
			$sql .= " AND CATEGORY LIKE :CATEGORY ";
		} 	
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, DESCRIPTION_SHORT) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$stmt = $this -> prepare($sql); 
		
		$stmt -> bindValue(":GROSSISTE_ID", $idGrossiste);
		
		if($categ!= ""){
			$stmt -> bindValue(":CATEGORY", $categ."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		return $this->getAllPrepa($stmt);
	}
	
	
	public function getAllRefusedProds($idGrossiste, $categ = "", $brand=null, $priceMin=null, $priceMax=null, $search=null)
	{
		$sql = "SELECT * FROM sfk_produit_grossiste as p
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID
				AND refus = 1 AND ACTIVE = 1 
				";
				
		if($categ!= ""){
			$sql .= " AND CATEGORY LIKE :CATEGORY ";
		} 	
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, DESCRIPTION_SHORT) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$stmt = $this -> prepare($sql); 
		
		$stmt -> bindValue(":GROSSISTE_ID", $idGrossiste);
		
		if($categ!= ""){
			$stmt -> bindValue(":CATEGORY", $categ."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		return $this->getAllPrepa($stmt);
	}
	
	
	public function prepCountAllProductInCat( $context="all"){
		
		$sql = "SELECT count(*) as count FROM sfk_produit_grossiste WHERE CATEGORY LIKE CONCAT(:categories, '%') 
		AND GROSSISTE_ID = :GROSSISTE_ID AND ACTIVE = 1  ";
		
		if($context == "refused"){
			$sql.= " AND refus = 1 ";
		}
		
		if($context == "assoc"){
			$sql.= " AND AJOUTE = 1 ";
		}
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= " AND AJOUTE = 0 AND refus = 0 ";
		}
		
		self::$stmtCountAll = $this -> prepare($sql);
	}
	
	public function testtest ($tmp,$grossiste){
		
		$sql = "SELECT count(*) as count FROM sfk_produit_grossiste WHERE CATEGORY LIKE ( $tmp) 
		AND GROSSISTE_ID = $grossite AND ACTIVE = 1  ";
		
		if($context == "refused"){
			$sql.= " AND refus = 1 ";
		}
		
		if($context == "assoc"){
			$sql.= " AND AJOUTE = 1 ";
		}
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= " AND AJOUTE = 0 AND refus = 0 ";
		}
		
		//$this->stmtCountAll = $this -> prepare($sql);
		//$this->stmtCountAll -> bindValue(":categories", $cat);
		//$this->stmtCountAll -> bindValue(":GROSSISTE_ID", $grossisteId);
		//$this->stmtCountAll -> execute();
		
		//$res = $this->stmtCountAll -> fetch();
		//$count  += $res['count'];
		echo $sql;
				var_dump($this->getall($sql));
		return $count;
		
	}
	
	
		public function countAllProductInCat($cat, $grossisteId, $context="all")
	{
		/*if($grossisteId == 1){
			$sql = "SELECT count(*) as count FROM sfk_produit_grossiste WHERE CATEGORY LIKE ( '$cat%') 
					AND GROSSISTE_ID = :GROSSISTE_ID AND ACTIVE = 1  ";
		}
		else{
			$sql = "SELECT count(*) as count FROM sfk_produit_grossiste WHERE CATEGORY = '$cat'  
					AND GROSSISTE_ID = :GROSSISTE_ID AND ACTIVE = 1 ";	
		}
		
		
		if($context == "refused"){
			$sql.= " AND refus = 1 ";
		}
		
		if($context == "assoc"){
			$sql.= " AND AJOUTE = 1 ";
		}
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= " AND AJOUTE = 0 AND refus = 0 ";
		}
		
		$this->stmtCountAll = $this -> prepare($sql);*/
		self::$stmtCountAll -> bindValue(":categories", $cat);
		self::$stmtCountAll -> bindValue(":GROSSISTE_ID", $grossisteId);
		self::$stmtCountAll -> execute();
		
		$res = self::$stmtCountAll -> fetch();
		$count  = $res['count'];
		
		return $count;
	}	
	
	/**
	 * Récupère tous les produits d'une catégorie pour un vendeur donné
	 */
	public function getAllProdByCategVendor($cat, $idVendor)
	{	
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID AND CATEGORY LIKE :CATEGORY AND ACTIVE = 1 ";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);	
	}
	
	public function getAllCountDistinctCat ($idVendor, $context){
		$sql = "SELECT COUNT(*) as c, CATEGORY FROM `sfk_produit_grossiste` 
				WHERE GROSSISTE_ID = :GROSSISTE_ID AND ACTIVE = 1 ";
				//Non associé, non refusé		
		if($context == "todo"){
			$sql.= " AND AJOUTE <> 1 
			         AND refus <> 1 ";
		}
				
		if($context == "refused"){
			$sql.= " AND refus = 1 ";
		} 
		
		if($context == "assoc"){
			$sql.= "AND AJOUTE = 1 ";
		}
		
		$sql .= " GROUP BY CATEGORY";
		
		$stmt = $this -> prepare ($sql);
		
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		
		return $this->getAllPrepa($stmt);	
	}
	/**
	 * Récupère tous les produits d'un vendeur donné
	 */
	public function getAllProdByVendor($idVendor)
	{	
		$sql = "SELECT ID_PRODUCT, EAN, REF_GROSSISTE FROM sfk_produit_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID AND ACTIVE = 1";
		//$sql = "SELECT ID_PRODUCT, EAN, REF_GROSSISTE FROM sfk_produit_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID ";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		
		
		return $this->getAllPrepa($stmt);	
	}
	

	public function getAllProdByVendorToCrawl($idVendor, $limit = "")
	{
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID
		AND CRAWLED = 0";
		// A reméttre aprés la récupération des descriptions et poid de LDLC pro
		
		if($limit != ""){
			$sql .= " ".$limit;
		}
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		
		
		return $this->getAllPrepa($stmt);	
	}
	// fonciton de test pour 
	public function getAllProdByVendorToCrawlTest($idVendor)
	{
		$sql ="	SELECT * FROM sfk_produit_grossiste as pg  
				INNER JOIN sfk_assoc_product_grossiste as acg ON acg.id_produit_grossiste = pg.ID_PRODUCT 
				WHERE acg.GROSSISTE_ID = :GROSSISTE_ID 
				AND pg.GROSSISTE_ID = :GROSSISTE_ID  " ;
		$sql .= " AND WEIGHT != 0 LIMIT 0,50 ";		
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		
		
		return $this->getAllPrepa($stmt);	
	}
	
	public function getAllProdGrossiste ($limit = "")
	{
		$sql = "SELECT pg.EAN, pg.REF_GROSSISTE, pg.NAME_PRODUCT, g.GROSSISTE_NOM, pg.CATEGORY, pg.WEIGHT FROM sfk_produit_grossiste as pg 
				INNER JOIN sfk_grossiste as g ON g.GROSSISTE_ID = pg.GROSSISTE_ID 
				WHERE pg.WEIGHT <> '0'
				AND g.ACTIVE = 1 
				ORDER BY g.GROSSISTE_NOM ";
		
		if($limit){
			$sql .= " LIMIT 0, $limit ";
		}
		
		$stmt = $this -> prepare($sql);
		
		return $this -> getAllPrepa($stmt);
	}
	
	
	public function getAllActiveProdGrossiste ($limit = "")
	{
		$sql = "SELECT * FROM sfk_produit_grossiste  
				WHERE ACTIVE = 1 ";
		
		if($limit){
			$sql .= " LIMIT 0, $limit ";
		}
		
		$stmt = $this -> prepare($sql);
		
		return $this -> getAllPrepa($stmt);
	}
	
	public function getAllActiveProdGrossisteToCalcul ($limit = "")
	{
		$sql = "SELECT * FROM sfk_produit_grossiste  
				WHERE ACTIVE = 1 
				AND WEIGHT < ".SystemParams::getParam('grossiste*weightmax')."
			  	AND WEIGHT > ".SystemParams::getParam('grossiste*weightmin')." "; 
		
		if($limit){
			$sql .= " LIMIT 0, $limit ";
		}
		
		$stmt = $this -> prepare($sql);
		
		return $this -> getAllPrepa($stmt);
	}
		
	public function getAllActiveProdGrossisteToSell()
	{
		$sql='SELECT * FROM `sfk_produit_grossiste` pg 
			  INNER JOIN sfk_prix_grossiste prx ON pg.ID_PRODUCT = prx.ID_PRODUCT
			  INNER JOIN sfk_assoc_product_grossiste apg ON pg.ID_PRODUCT = apg.id_produit_grossiste
			  WHERE ACTIVE = 1 
			  AND prx.COEF < '.SystemParams::getParam('grossiste*coefmax').' 
			  AND prx.COEF > '.SystemParams::getParam('grossiste*coef').'
			  AND prx.POIDS < '.SystemParams::getParam('grossiste*weightmax').'
			  AND prx.POIDS > '.SystemParams::getParam('grossiste*weightmin').'
			   AND pg.EAN <> "8410779571106"  
			 ';
		
		$stmt = $this -> prepare($sql);
		
		return $this -> getAllPrepa($stmt);
	}
	
	public function setProductInactive($idProduct)
	{
		$sql = "UPDATE sfk_produit_grossiste SET ACTIVE = 0, QUANTITY = 0 WHERE ID_PRODUCT = :ID_PRODUCT"; //ID DU VENDEUR DYNAMIQUE BESOIN TRACKING
		$stmt = $this->prepare($sql);
		$stmt->bindValue(':ID_PRODUCT', $idProduct);
		$stmt->execute();
	}
	
	public function setProductQuantitySold($idProduct, $vendor_id)
	{
		$sql = "UPDATE sfk_product_ecommercant_stock_prix SET QUANTITY = 0 WHERE id_produit = :ID_PRODUCT AND VENDOR_ID = :VENDOR_ID"; //ID DU VENDEUR DYNAMIQUE BESOIN TRACKING
		$stmt = $this->prepare($sql);
		$stmt->bindValue(':ID_PRODUCT', $idProduct);
		$stmt->bindValue(':VENDOR_ID', $vendor_id);
		$stmt->execute();
	}
	
	/**
	 * récupère les produits déjà associées pour un vendeur et sa catégorie
	 */
	public function getCountAssocProdByCatVendor($catCli, $idVendor)
	{
		$sql = "SELECT COUNT(*) as count FROM sfk_produit_grossiste as ps
				INNER JOIN sfk_assoc_product_grossiste as ap 
				ON ps.GROSSISTE_ID = ap.GROSSISTE_ID AND ps.ID_PRODUCT = ap.id_produit_grossiste
				WHERE ps.GROSSISTE_ID = :GROSSISTE_ID AND ps.CATEGORY LIKE :CATEGORY AND ACTIVE = 1 ";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $catCli);
		$stmt -> execute();
		return $stmt -> fetch();
			
	}
	
	public function getAllDistinctManufFromCatVendor($cat, $idVendor)
	{

		$sql =" SELECT DISTINCT p.MANUFACTURER FROM sfk_produit_grossiste as p
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID 
				AND p.CATEGORY LIKE :CATEGORY
				AND MANUFACTURER <> '' AND ACTIVE = 1 
				ORDER BY MANUFACTURER ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);

		return $this->getAllPrepa($stmt);
	}
	
	////////////////////////////////////////////// NON UTILISABLE ///////////////////////////////////////
	
		/**
	 * Remonte tous les attributs pour une catégorie donnée de produits appartenant
	 * à un vendeur
	 */
	public function getAllAttrFromVendorCateg($cat, $idVendor)
	{
		/*$sql =" SELECT DISTINCT a.ID_ATTR, a.LABEL_ATTR FROM sfk_attribut_ps as a
			    INNER JOIN sfk_produit_attribut_ps as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_ps as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID 
				AND p.CATEGORY LIKE :CATEGORY";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);*/
	}
	
	
	/**
	 * Ne remonte que les 3 attributs les plus utilisés pour une catégorie et un vendeur donné
	 */
	public function getBestAttrFromVendorCateg($cat, $idVendor)
	{
		/*$sql =" SELECT COUNT(*) as count, a.ID_ATTR, a.LABEL_ATTR FROM sfk_attribut_ps as a
			    INNER JOIN sfk_produit_attribut_ps as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_ps as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID 
				AND p.CATEGORY LIKE :CATEGORY
				GROUP BY ID_ATTR
				ORDER BY count DESC
				LIMIT 3";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);*/
	}
	
	public function getAllDisctValueForAttrFromVendorCateg($cat, $idVendor, $idAttr)
	{
		/*$sql =" SELECT DISTINCT TRIM(pa.VALUE) as VALUE FROM sfk_attribut_ps as a
			    INNER JOIN sfk_produit_attribut_ps as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_ps as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.GROSSISTE_ID = :GROSSISTE_ID 
				AND p.CATEGORY LIKE :CATEGORY
				AND a.ID_ATTR = :ID_ATTR 
				ORDER BY VALUE ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		
		
		return $this->getAllPrepa($stmt);*/
	}
	
	
	/**
	 * Fonction permettant de savoir i cette catégorie ecommercant a déjà été associée à une catégorie
	 * Avahis. Si une association existe la fonction retourne l'ID de la catégorie avahis associée, sinon
	 * elle retourne false
	 * @param idVendor l'ID du grossiste
	 * @param cat_ecom la catégorie de l'exommercant sous forme Root > Cat > sCat
	 */
	public function isCatAssoc($idVendor, $cat_ecom)
	{
		$sql = "SELECT cat_id 
		 		FROM sfk_assoc_categ_grossiste  
		 		WHERE categ_grossiste = :categ_grossiste 
		 		AND GROSSISTE_ID = :GROSSISTE_ID 
		 		LIMIT 1";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', $idVendor);
		$stmt -> bindValue(':categ_grossiste', $cat_ecom);
		
		$stmt -> execute() ;
		$res = $stmt -> fetch();
		
		if($res != null){
			return $res['cat_id'];	
		}
		else{
			return false;
		}
	}
	
	/**
	 * Ajoute une association entre un vendeur une catégorie avahis et sa catégorie ecommercant
	 * @param idVendor 	ID de l'ecommercant
	 * @param cat_ecom 	catégorie de l'ecommercant
	 * @param cat_id 	id cat avahis avec lequel la cat ecom sera associée
	 */
	public function addAssocCatVendor($idVendor, $cat_ecom, $cat_id)
	{
		$sql = "INSERT INTO sfk_assoc_categ_grossiste (GROSSISTE_ID, categ_grossiste, cat_id) 
		        VALUES (:GROSSISTE_ID, :categ_grossiste, :cat_id) 
		        ON DUPLICATE KEY UPDATE
					cat_id = VALUES(cat_id)";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', $idVendor);
		$stmt -> bindValue(':categ_grossiste', $cat_ecom);
		$stmt -> bindValue(':cat_id', $cat_id);
		
		$stmt -> execute() ;
		
		return true;
	}
	/**
	* ajoute le couple ean et l'url de référence
	*
	*/
	public function insertToEanUrl($sku, $url)
	{
		$sql = "INSERT INTO sfk_ean_url (EAN, URL) 
		        VALUES (:EAN, :URL) 
		        ON DUPLICATE KEY UPDATE
					EAN = VALUES(EAN);
					URL = VALUES(URL)";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':EAN', $sku);
		$stmt -> bindValue(':URL', $url);
		
		$stmt -> execute() ;
		
		return true;
	}
	
	
	public function addRowCatProduct($V, $cat, $vendor_ID = 24) {
		
		
		
		if($V['GROSSISTE_ID'] == '1' ){
			$this -> prepAttribut() ;
			
			$idColor = $this -> getAttrByCatAndCode("COLOR", $cat);
			
			if($idColor == false)
				$idColor = $this -> addAttribut($attr = array("cat_id" => $cat, "code_attr" => "COLOR", "label_attr" => "color"));
			
			
			$idCarModel = $this -> getAttrByCatAndCode("CAR_MODEL", $cat);
			
			if($idCarModel == false)
				$idCarModel = $this -> addAttribut($attr = array("cat_id" => $cat, "code_attr" => "CAR_MODEL", "label_attr" => "car model"));
			
		}
		
		
		///////////////////////////////////////////// !!!! ATTENTION !!!! ///////////////////////////////////////////////
		if( ($V['EAN']!= "NULL" && $V['EAN']!= "") | ($V['REF_GROSSISTE']!= "" && $V['REF_GROSSISTE']!= "NULL") ){
				
			$null = null;
				
			if($V['EAN'] != "NULL" && $V['EAN'] != ""){
				$sku = $V['EAN'];
			}elseif($V['REF_GROSSISTE'] != "" && $V['REF_GROSSISTE']!= "NULL"){
				$sku = $V['REF_GROSSISTE'];
			}	
			
			$this -> stmtCatalogProduct -> bindParam(1, $sku);			
			$this -> stmtCatalogProduct -> bindParam(2, $cat);
			$this -> stmtCatalogProduct -> bindParam(3, $V['NAME_PRODUCT'] ? $V['NAME_PRODUCT'] : $null);
			$this -> stmtCatalogProduct -> bindParam(4, $V['DESCRIPTION'] ? $V['DESCRIPTION'] : $null);
			$this -> stmtCatalogProduct -> bindParam(5, $V['DESCRIPTION_SHORT'] ? $V['DESCRIPTION_SHORT'] : $null);
			$this -> stmtCatalogProduct -> bindParam(6, $V['PRICE_PRODUCT'] ? $V['PRICE_PRODUCT'] : $null);
			$this -> stmtCatalogProduct -> bindParam(7, $V['WEIGHT'] ? $V['WEIGHT'] : $null);
			$this -> stmtCatalogProduct -> bindParam(8, $V['MANUFACTURER'] ? ucfirst(strtolower($V['MANUFACTURER'])) : $null);
			$this -> stmtCatalogProduct -> bindParam(9, $V['meta_description'] ? $V['meta_description'] : $null);
			$this -> stmtCatalogProduct -> bindParam(10, $V['meta_keyword'] ? $V['meta_keyword'] : $null);
			$this -> stmtCatalogProduct -> bindParam(11, $V['meta_title'] ? $V['meta_title'] : $null);
			$this -> stmtCatalogProduct -> bindParam(12, $V['IMAGE_PRODUCT'] ? $V['IMAGE_PRODUCT'] : $null);
			$this -> stmtCatalogProduct -> bindParam(13, $V['IMAGE_PRODUCT_2'] ? $V['IMAGE_PRODUCT_2'] : $null);
			$this -> stmtCatalogProduct -> bindParam(14, $V['IMAGE_PRODUCT_3'] ? $V['IMAGE_PRODUCT_3'] : $null);
			$this -> stmtCatalogProduct -> bindParam(15, $V['MANUFACTURER'] ? ucfirst(strtolower($V['MANUFACTURER'])) : $null);
			$this -> stmtCatalogProduct -> bindParam(16, $vendor_ID ? $vendor_ID : $null);
			
			$this -> stmtCatalogProduct -> execute();
			
			//$prod_id = $this -> lastInsertId();
			//if(!$prod_id){
			$prod_id = $this -> getIdProduct($sku);
			//}
			if($V['GROSSISTE_ID'] == '1' ){
				$this -> addAttributProduct($idColor, $prod_id, $V['COLOR']);
				$this -> addAttributProduct($idCarModel, $prod_id, $V['CAR_MODEL']);
			}
			$this -> setAjouteProduct($V['GROSSISTE_ID'], $V['ID_PRODUCT'], 1);
			
			$this -> assocProductGrossiste($V['ID_PRODUCT'], $prod_id, $V['GROSSISTE_ID']);
			return $prod_id;
			//return $this -> lastInsertId();			
		}

	}

	public function setAjouteProduct($grossisteId, $idProduct, $ajoute)
	{
		$sql = "UPDATE sfk_produit_grossiste SET AJOUTE = :AJOUTE
				WHERE ID_PRODUCT = :ID_PRODUCT 
				AND GROSSISTE_ID = :GROSSISTE_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":GROSSISTE_ID", $grossisteId);
		$stmt -> bindValue(":AJOUTE", $ajoute);
		
		$stmt -> execute();
	}


	public function getAttrValueGrossiste($idProduct)
	{
		$sql = "SELECT a.CODE_ATTR, pa.VALUE FROM `sfk_attribut_grossiste` a 
				INNER JOIN sfk_produit_attribut_grossiste pa ON a.ID_ATTR = pa.ID_ATTR 
				WHERE pa.ID_PRODUCT = :ID_PRODUCT";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':ID_PRODUCT', $idProduct);
		
		return $this -> getAllPrepa($stmt);
	}

	public function setRefusProduct($vendorId, $idProduct, $refus)
	{
		$sql = "UPDATE sfk_produit_grossiste SET refus = :refus
				WHERE ID_PRODUCT = :ID_PRODUCT 
				AND GROSSISTE_ID = :GROSSISTE_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":GROSSISTE_ID", $vendorId);
		$stmt -> bindValue(":refus", $refus);
		
		$stmt -> execute();
	}	
	
	
	public function setCrawledProduct($vendorId, $idProduct, $crawled)
	{
		$sql = "UPDATE sfk_produit_grossiste SET CRAWLED = :CRAWLED
				WHERE ID_PRODUCT = :ID_PRODUCT 
				AND GROSSISTE_ID = :GROSSISTE_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":GROSSISTE_ID", $vendorId);
		$stmt -> bindValue(":CRAWLED", $crawled);
		
		$stmt -> execute();		
	}
	

	
	public function addRowStockPrixProdVenteAv($prod)
	{
		if($sku_av = $this -> getProdAvahisAssocieGrossiste($prod['ID_PRODUCT'])){
			
			$alreadyExist = $this -> getStockPriceEcom($sku_av['EAN'], 24);
		
			$sql = "INSERT INTO  sfk_product_ecommercant_stock_prix 
									( 	id_produit, VENDOR_ID, ref_ecommercant, PRICE_PRODUCT,
										PRICE_TTC, QUANTITY, PRICE_HT, REDUCTION_FROM, REDUCTION_TO,
										PRICE_REDUCTION, POURCENTAGE_REDUCTION, ETAT_PROMO, ETAT, date_create)
								VALUES (:id_produit, :VENDOR_ID, :ref_ecommercant, :PRICE_PRODUCT, 
										:PRICE_TTC, :QUANTITY, :PRICE_HT, :REDUCTION_FROM, :REDUCTION_TO,
										:PRICE_REDUCTION, :POURCENTAGE_REDUCTION, :ETAT_PROMO, :ETAT, NOW()) 
								ON DUPLICATE KEY 
								UPDATE 	ref_ecommercant 		= VALUES(ref_ecommercant),
										PRICE_PRODUCT 			= VALUES(PRICE_PRODUCT),
										PRICE_TTC 				= VALUES(PRICE_TTC),
										QUANTITY 				= VALUES(QUANTITY),
										PRICE_HT 				= VALUES(PRICE_HT),
										REDUCTION_FROM 			= VALUES(REDUCTION_FROM),
										REDUCTION_TO 			= VALUES(REDUCTION_TO),
										PRICE_REDUCTION 		= VALUES(PRICE_REDUCTION),
										POURCENTAGE_REDUCTION 	= VALUES(POURCENTAGE_REDUCTION),
										ETAT_PROMO 				= VALUES(ETAT_PROMO),
										ETAT 					= VALUES(ETAT)";
										
			$stmt = $this -> prepare($sql);
			
			$stmt -> bindValue(':id_produit', $sku_av['EAN']);
			$stmt -> bindValue(':VENDOR_ID', 24);
			$stmt -> bindValue(':ref_ecommercant', $prod['ID_PRODUCT']);
			$stmt -> bindValue(':PRICE_PRODUCT', str_replace(",", ".", $prod['PRIX_FINAL']));
			$stmt -> bindValue(':PRICE_TTC', str_replace(",", ".", $prod['PRIX_FINAL']));
			$stmt -> bindValue(':QUANTITY', $prod['QUANTITY']);
			$stmt -> bindValue(':PRICE_HT', str_replace(",", ".", $prod['PRIX_FINAL']));
			$stmt -> bindValue(':REDUCTION_FROM', 'NULL');
			$stmt -> bindValue(':REDUCTION_TO', 'NULL');
			$stmt -> bindValue(':PRICE_REDUCTION', 'NULL');
			$stmt -> bindValue(':POURCENTAGE_REDUCTION', 'NULL');
			$stmt -> bindValue(':ETAT_PROMO', 'NULL');
			$stmt -> bindValue(':ETAT', 'NEW');
			
			//Si le produit est déja vendu par un commerçant autre que Avahis
			if($this -> productIsAlreadySold($sku_av['EAN'])){
				$stmt -> bindValue(':QUANTITY', 0);	
				$sqlState = $stmt -> execute();
				return 4;
			}
			$sqlState = $stmt -> execute();
			
			
			//UPDATE
			if( $alreadyExist && $sqlState == 1){
				return 2;
			//FAIL
			}elseif($sqlState == 0){
				return 0;
			//INSERT
			}elseif($sqlState == 1){
				return 1;
			}	
		}
		else{
			return 3;	
		}
		
	}

	public function prepAddRowStockPrixGrossiste()
	{
		$sql = "INSERT INTO  sfk_product_ecommercant_stock_prix 
								( 	id_produit, VENDOR_ID, ref_ecommercant, PRICE_PRODUCT,
									PRICE_TTC, QUANTITY, PRICE_HT, REDUCTION_FROM, REDUCTION_TO,
									PRICE_REDUCTION, POURCENTAGE_REDUCTION, ETAT_PROMO, ETAT, date_create)
							VALUES (:id_produit, :VENDOR_ID, :ref_ecommercant, :PRICE_PRODUCT, 
									:PRICE_TTC, :QUANTITY, :PRICE_HT, :REDUCTION_FROM, :REDUCTION_TO,
									:PRICE_REDUCTION, :POURCENTAGE_REDUCTION, :ETAT_PROMO, :ETAT, NOW()) 
							ON DUPLICATE KEY 
							UPDATE 	ref_ecommercant 		= VALUES(ref_ecommercant),
									PRICE_PRODUCT 			= VALUES(PRICE_PRODUCT),
									PRICE_TTC 				= VALUES(PRICE_TTC),
									QUANTITY 				= VALUES(QUANTITY),
									PRICE_HT 				= VALUES(PRICE_HT),
									REDUCTION_FROM 			= VALUES(REDUCTION_FROM),
									REDUCTION_TO 			= VALUES(REDUCTION_TO),
									PRICE_REDUCTION 		= VALUES(PRICE_REDUCTION),
									POURCENTAGE_REDUCTION 	= VALUES(POURCENTAGE_REDUCTION),
									ETAT_PROMO 				= VALUES(ETAT_PROMO),
									ETAT 					= VALUES(ETAT)";
									
		$this -> stmtAddRowStockPrixGr = $this -> prepare($sql);		
	}
	
	public function addRowStockPrixGrossiste($prod, $sku_avahis)
	{
		if($sku_av = $this -> getSkuAssocProdGr($prod['ID_PRODUCT'])){
		
			$alreadyExist = $this -> getStockPriceEcom($sku_av, 24);
	
			$this -> stmtAddRowStockPrixGr -> bindValue(':id_produit', 				$sku_av);
			$this -> stmtAddRowStockPrixGr -> bindValue(':VENDOR_ID', 				24);
			$this -> stmtAddRowStockPrixGr -> bindValue(':ref_ecommercant', 		$prod['ID_PRODUCT']);
			$this -> stmtAddRowStockPrixGr -> bindValue(':PRICE_PRODUCT', 			str_replace(",", ".", $prod['PRIX_FINAL']));
			$this -> stmtAddRowStockPrixGr -> bindValue(':PRICE_TTC', 				str_replace(",", ".", $prod['PRIX_FINAL']));
			$this -> stmtAddRowStockPrixGr -> bindValue(':QUANTITY', 				$prod['QUANTITY']);
			$this -> stmtAddRowStockPrixGr -> bindValue(':PRICE_HT', 				str_replace(",", ".", $prod['PRIX_FINAL']));
			$this -> stmtAddRowStockPrixGr -> bindValue(':REDUCTION_FROM', 			'NULL');
			$this -> stmtAddRowStockPrixGr -> bindValue(':REDUCTION_TO', 			'NULL');
			$this -> stmtAddRowStockPrixGr -> bindValue(':PRICE_REDUCTION', 		'NULL');
			$this -> stmtAddRowStockPrixGr -> bindValue(':POURCENTAGE_REDUCTION', 	'NULL');
			$this -> stmtAddRowStockPrixGr -> bindValue(':ETAT_PROMO', 				'NULL');
			$this -> stmtAddRowStockPrixGr -> bindValue(':ETAT', 					'NEW');
			
			//Si le produit est déja vendu par un commerçant autre que Avahis
			if($this -> productIsAlreadySold($sku_av)){
				$this -> stmtAddRowStockPrixGr -> bindValue(':QUANTITY', 0);	
				$sqlState = $this -> stmtAddRowStockPrixGr -> execute();
				return 4;
			}
			$sqlState = $this -> stmtAddRowStockPrixGr -> execute();	
			
			//UPDATE
			if( $alreadyExist && $sqlState == 1){
				return 2;
			//FAIL
			}elseif($sqlState == 0){
				return 0;
			//INSERT
			}elseif($sqlState == 1){
				return 1;
			}
		}	
		
	}

	public function deleteVenteAv($prod)
	{
		//on recherche le produit associé
		$sku_av = $this -> getProdAvAssocGr($prod['GROSSISTE_ID'], $prod['ID_PRODUCT']);
		
		$sql = "DELETE FROM  sfk_product_ecommercant_stock_prix 
				WHERE id_produit = :id_produit AND VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':id_produit', $sku_av['EAN']);
		$stmt -> bindValue(':VENDOR_ID', 24);
									
		return $stmt -> execute();
	}
	
	public function getDistinctAttr($value='')
	{
		return false;
	}

	/**
	 * Va chercher si l'attribut existe déja dans la table s'il existe retourne l'ID_ATTRIBUT de cet
	 * attribut sinon la fonction Crée ce nouvel Attribut et renvoie le dernier ID_ATTRIBUT auto incrémenté
	 * correspondant
	 */
	public function checkIfAttributeExistsGR($codeattr, $labelattr)
	{
		$sql = "SELECT ID_ATTR FROM sfk_attribut_grossiste WHERE CODE_ATTR = :CODE_ATTR";
		
		$stmt = $this->prepare($sql);
		$stmt->bindValue(":CODE_ATTR", $codeattr);
		$stmt->execute();
		
		if ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			return $result['ID_ATTR'];
		}
		else{
			$stmt = $this->prepare("INSERT INTO sfk_attribut_grossiste (LABEL_ATTR, CODE_ATTR) 
												VALUES (:LABEL_ATTR, :CODE_ATTR)");
			$stmt->bindValue(":LABEL_ATTR", $labelattr);
			$stmt->bindValue(":CODE_ATTR", $codeattr);
			$stmt->execute();
			
			return $this->lastInsertId();
		}
	}
	
	/**
	 * Préparation des insertions des relations produits_attributs dans les tables dédiées à 
	 * Prestashop de Soukflux
	 */
	public function prepareInsertSfkProdAttrGR()
	{									
		$sql = "INSERT INTO sfk_produit_attribut_grossiste (ID_PRODUCT, GROSSISTE_ID, ID_ATTR, VALUE)
													VALUES (:ID_PRODUCT, :GROSSISTE_ID, :ID_ATTR, :VALUE)
													ON DUPLICATE KEY UPDATE
													ID_ATTR 	=VALUES(ID_ATTR),
													VALUE 		=VALUES(VALUE)";
														
		$this->stmtProdAttrGR = $this->prepare($sql);											
	}
	
	/**
	 * Insertion des relations produits attributs dans les tables dédiées à 
	 * Prestashop de Soukflux 
	 */
	public function addRowSfkProdAttrGR($idProd, $vendorID, $idAttr, $value)
	{
		$this->stmtProdAttrGR->bindValue(':ID_PRODUCT',	$idProd);
		$this->stmtProdAttrGR->bindValue(':GROSSISTE_ID', 	$vendorID);
		$this->stmtProdAttrGR->bindValue(':ID_ATTR', 		$idAttr);
		$this->stmtProdAttrGR->bindValue(':VALUE', 		$value);
		return $this->stmtProdAttrGR->execute();
	}
	
	public function updateDescriptProduct($idProduct , $description)
	{
		$sql = "UPDATE sfk_produit_grossiste SET DESCRIPTION = :DESCRIPTION WHERE ID_PRODUCT = :ID_PRODUCT";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":DESCRIPTION", $description);
		$stmt -> bindValue (":ID_PRODUCT", $idProduct);
		
		$stmt -> execute();
	}
	/**
	 * update la description html de la table catalogue product
	 */
	public function updateDescriptProductCatProd($sku , $description)
	{
		$sql = "UPDATE sfk_catalog_product SET description_html = :DESCRIPTION WHERE sku = :SKU";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":DESCRIPTION", $description);
		$stmt -> bindValue (":SKU", $sku);
		
		$stmt -> execute();
	}
	
	/**
	 * update la description de la table catalogue product
	 */
	public function updateDescriptProductCatProdNotHtml($sku , $description)
	{
		$sql = "UPDATE sfk_catalog_product SET description = :DESCRIPTION WHERE sku = :SKU";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":DESCRIPTION", $description);
		$stmt -> bindValue (":SKU", $sku);
		
		$stmt -> execute();
	}

	public function updatePoidsProduct($idProduct , $poids)
	{
		$sql = "UPDATE sfk_produit_grossiste SET WEIGHT = :WEIGHT, VOLUMETRIC_WEIGHT = :WEIGHT WHERE ID_PRODUCT = :ID_PRODUCT";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":WEIGHT", $poids);
		$stmt -> bindValue (":ID_PRODUCT", $idProduct);
		
		$stmt -> execute();
	}	
		public function updatePoidsProductCatProd($sku , $poids)
	{
		$sql = "UPDATE sfk_catalog_product SET weight = :WEIGHT WHERE sku = :SKU";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":WEIGHT", $poids);
		$stmt -> bindValue (":SKU", $sku);
		
		$stmt -> execute();
	}	
		
	public function getAllAttrFromProduct($idProduct, $grossisteID)
	{
		$sql = "SELECT pa.VALUE, a.LABEL_ATTR, a.ID_ATTR FROM sfk_produit_attribut_grossiste as pa 
				INNER JOIN sfk_attribut_grossiste as a ON a.ID_ATTR = pa.ID_ATTR 
				WHERE pa.ID_PRODUCT = :ID_PRODUCT 
				AND pa.GROSSISTE_ID = :GROSSISTE_ID"; 
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":GROSSISTE_ID", $grossisteID);  
		
		return $this->getAllPrepa($stmt);
	}
	/**
	 * update le poid  de la table catalogue product
	 */
	public function updatetWeightToCatProd($sku , $weight)
	{
		$sql = "UPDATE sfk_catalog_product SET weight = :WEIGHT WHERE sku = :SKU";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":WEIGHT", $weight);
		$stmt -> bindValue (":SKU", $sku);
		
		$stmt -> execute();
	}	
	/**
	 * retourne les infos de la table sfk_produit suivant un vendeur donné
	 * @param $idVendor l'ID grossiste
	 * @param $limit la limit d'affichage des produit écrire "LIMIT 0,10"
	 * @param $poiddiffzero condition logique pour l'affichage des produits différent de zero
	 */
	public function getAllProdByVendorComplete($idVendor, $limit = "",$poiddiffzero = "")
	{
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID ";
		
		if($poidzero != ""&& $poiddiffzero == "1"){
				
			$sql .= " AND WEIGHT !=0";
		}
		
		if($limit != ""){
			$sql .= " ".$limit;
		}
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":GROSSISTE_ID", $idVendor);
		
		
		return $this->getAllPrepa($stmt);	
	}
	
	/**
	 * Verifie si le poid de catalog prod est supérieur a zero
	 */
	public function verifPoidCatProdSupZero($sku){
		$sql = "SELECT weight FROM sfk_catalog_product WHERE sku = :SKU "; 
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":SKU", $sku);
		
		$testWeight = $this->getAllPrepa($stmt);
		if ($testWeight['weight']>0){
			return TRUE ;
		}
		else{
			return FALSE ;
		}
	}	

	public function getPrixCalcul($id_product)
	{
		$sql = "SELECT * FROM sfk_prix_grossiste WHERE ID_PRODUCT = :ID_PRODUCT";
		
		$stmt = $this->prepare($sql);
		$stmt -> bindValue(':ID_PRODUCT', $id_product);
		
		return $this -> getAllPrepa($stmt);
	}
	
		/**
	 * Préparation des insertions des relations produits_attributs dans les tables dédiées à 
	 * Prestashop de Soukflux
	 */
	public function prepareInsertSfkPrixGrossiste()
	{									
		$sql = "INSERT INTO sfk_prix_grossiste (ID_PRODUCT, PRIX_ACHAT, POIDS, PRIX_KILO, PRIX_METRO,
												FRAIS_LIVR, AERIEN, OM, PRIX_LIVRE, PERCENT_MARGE, 
												PERCENT_TVA, PRIX_FINAL, COEF, RATIO_RUN_FR, PRIX_LIVRAISON, RATIO_TRANSPORT)
												VALUES (:ID_PRODUCT, :PRIX_ACHAT, :POIDS, :PRIX_KILO, :PRIX_METRO,
														:FRAIS_LIVR, :AERIEN, :OM, :PRIX_LIVRE, :PERCENT_MARGE, 
														:PERCENT_TVA, :PRIX_FINAL, :COEF, :RATIO_RUN_FR, :PRIX_LIVRAISON, :RATIO_TRANSPORT)
												ON DUPLICATE KEY UPDATE
													PRIX_ACHAT 		=VALUES(PRIX_ACHAT),
													POIDS 			=VALUES(POIDS),
													PRIX_KILO 		=VALUES(PRIX_KILO),
													PRIX_METRO 		=VALUES(PRIX_METRO),
													FRAIS_LIVR 		=VALUES(FRAIS_LIVR),
													AERIEN 			=VALUES(AERIEN),
													OM 				=VALUES(OM),
													PRIX_LIVRE 		=VALUES(PRIX_LIVRE),
													PERCENT_MARGE 	=VALUES(PERCENT_MARGE),
													PERCENT_TVA 	=VALUES(PERCENT_TVA),
													PRIX_FINAL  	=VALUES(PRIX_FINAL),
													COEF 			=VALUES(COEF),
													RATIO_RUN_FR 	=VALUES(RATIO_RUN_FR),
													PRIX_LIVRAISON 	=VALUES(PRIX_LIVRAISON),
													RATIO_TRANSPORT =VALUES(RATIO_TRANSPORT)";
														
		$this->stmtPrixGrossiste = $this->prepare($sql);											
	}
	
	/**
	 * Insertion des relations produits attributs dans les tables dédiées à 
	 * Prestashop de Soukflux 
	 */
	public function addRowSfkPrixGrossiste($V)
	{
		$this->stmtPrixGrossiste->bindValue(':ID_PRODUCT',		$V['ID_PRODUCT']);
		$this->stmtPrixGrossiste->bindValue(':PRIX_ACHAT', 		str_replace(",", ".", $V['PRIX_ACHAT']));
		$this->stmtPrixGrossiste->bindValue(':POIDS', 			str_replace(",", ".", $V['POIDS']));
		$this->stmtPrixGrossiste->bindValue(':PRIX_KILO', 		str_replace(",", ".", $V['PRIX_KILO']));
		$this->stmtPrixGrossiste->bindValue(':PRIX_METRO', 		str_replace(",", ".", $V['PRIX_METRO']));
		$this->stmtPrixGrossiste->bindValue(':FRAIS_LIVR',		str_replace(",", ".", $V['FRAIS_LIVR']));
		$this->stmtPrixGrossiste->bindValue(':AERIEN', 			$V['AERIEN']);
		$this->stmtPrixGrossiste->bindValue(':OM', 				$V['OM']);
		$this->stmtPrixGrossiste->bindValue(':PRIX_LIVRE', 		str_replace(",", ".", $V['PRIX_LIVRE']));	
		$this->stmtPrixGrossiste->bindValue(':PERCENT_MARGE',	$V['PERCENT_MARGE']);
		$this->stmtPrixGrossiste->bindValue(':PERCENT_TVA', 	$V['PERCENT_TVA']);
		$this->stmtPrixGrossiste->bindValue(':PRIX_FINAL', 		str_replace(",", ".", $V['PRIX_FINAL']));
		$this->stmtPrixGrossiste->bindValue(':COEF', 			$V['COEF']);
		$this->stmtPrixGrossiste->bindValue(':RATIO_RUN_FR', 	$V['RATIO_RUN_FR']);
		$this->stmtPrixGrossiste->bindValue(':PRIX_LIVRAISON', 	str_replace(",", ".", $V['PRIX_LIVRAISON']));
		$this->stmtPrixGrossiste->bindValue(':RATIO_TRANSPORT',	$V['RATIO_TRANSPORT']);

		
		return $this->stmtPrixGrossiste->execute();
	}

	public function getQuantityProdGr($id)
	{
		$sql = "SELECT QUANTITY FROM sfk_produit_grossiste WHERE ID_PRODUCT = :ID_PRODUCT";
		
		$stmt = $this->prepare($sql);
		$stmt -> bindValue(':ID_PRODUCT', $id);
		
		return $this -> getOnePrep($stmt);
	}
	
	public function razQuantityVentesAvahis()
	{
		$sql = "UPDATE sfk_product_ecommercant_stock_prix SET QUANTITY = 0 WHERE VENDOR_ID = 24 ";
		
		$stmt = $this->prepare($sql);
		
		return $stmt -> execute();
	}
	
	public function setUpdateGrossiste($grossiste_id)
	{
		$sql = "UPDATE sfk_grossiste SET DATE_UPDATE_PRODUCT =  NOW() WHERE GROSSISTE_ID = :GROSSISTE_ID";
		
		$stmt = $this->prepare($sql);
		$stmt -> bindValue(':GROSSISTE_ID', $grossiste_id);
		
		return $stmt -> execute();
	}

	public function prepGetDoublonsEAN()
	{
		$sql = "SELECT ID_PRODUCT, WEIGHT FROM sfk_produit_grossiste WHERE EAN = :EAN AND ACTIVE = 1 AND WEIGHT > 0";
		
		$this -> stmtGetDoublonsEAN = $this -> prepare($sql);
	}

	public function getDoublonsEAN($ean)
	{
		$this -> stmtGetDoublonsEAN -> bindValue(':EAN', $ean);
		
		return $this->getAllPrepa($this -> stmtGetDoublonsEAN);
	}
	
	public function prepDoublonIncorrect()
	{
		$sql = "UPDATE sfk_produit_grossiste SET DOUBLON_INCORRECT = 1 WHERE ID_PRODUCT = :ID_PRODUCT ";
		
		$this -> stmtDoublonIncorrect = $this->prepare($sql);		
	}
	public function setDoublonIncorrect($id_produit){
		
		$this -> stmtDoublonIncorrect -> bindValue(':ID_PRODUCT', $id_produit);
		
		return $this -> stmtDoublonIncorrect -> execute();
	}
	
	public function prepMajPoidsGrossiste()
	{
		$sql = "UPDATE `sfk_produit_grossiste` 
				SET WEIGHT = :WEIGHT, `VOLUMETRIC_WEIGHT` = :WEIGHT
				WHERE GROSSISTE_ID = :GROSSISTE_ID 
				AND CATEGORY LIKE :CATEGORY 
				AND (WEIGHT = 0 OR WEIGHT = 'NULL' OR WEIGHT IS NULL)";
		
		$this -> stmtMajPoidsGrossiste = $this -> prepare($sql);
	}
	
	public function majPoidsGrossiste($cat, $weight, $grossiste_id)
	{
		$this -> stmtMajPoidsGrossiste -> bindValue(':CATEGORY', 	 $cat);
		$this -> stmtMajPoidsGrossiste -> bindValue(':WEIGHT', 		 $weight);
		$this -> stmtMajPoidsGrossiste -> bindValue(':GROSSISTE_ID', $grossiste_id);
		
		return $this -> stmtMajPoidsGrossiste -> execute () ;
	}
}
	