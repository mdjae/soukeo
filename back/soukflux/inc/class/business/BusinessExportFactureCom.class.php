<?php 
/**
 * 
 */
class BusinessExportFactureCom extends FPDF {
	
	protected $facture_id ;
	
	public function __construct($facture_id) {
		
		$this -> facture_id = $facture_id;
		parent::__construct();
	}
	
	public function Header()
	{
	    // Logo
	    $this->Image('logo.jpg',10,6,30);
	    // Police Arial gras 15
	    $this->SetFont('Arial','B',15);
	    // Décalage à droite
	    $this->Cell(80);
	    // Titre
	    $this->Cell(30,10,'Titre',1,0,'C');
	    // Saut de ligne
	    $this->Ln(20);
	}

	// Pied de page
	public function Footer()
	{
	    // Positionnement à 1,5 cm du bas
	    $this->SetY(-15);
	    // Police Arial italique 8
	    $this->SetFont('Arial','I',8);
	    // Numéro de page
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
	public function ImprovedTable($header, $data)
	{
	    // Largeurs des colonnes
	    $w = array(40, 35, 45, 40);
	    // En-tête
	    for($i=0;$i<count($header);$i++)
	        $this->Cell($w[$i],7,$header[$i],1,0,'C');
	    $this->Ln();
	    // Données
	    foreach($data as $line)
	    {
	        $this->Cell($w[0],6,$line -> getDesignation(),'LR');
	        $this->Cell($w[1],6,$line -> getPrice_ttc(),'LR');
	        $this->Cell($w[2],6,number_format($line -> getMt_comm(),0,',',' '),'LR',0,'R');
	        $this->Cell($w[3],6,number_format($line -> getMt_frais_banq(),0,',',' '),'LR',0,'R');
	        $this->Ln();
	    }
	    // Trait de terminaison
	    $this->Cell(array_sum($w),0,'','T');
	}
}

 ?>