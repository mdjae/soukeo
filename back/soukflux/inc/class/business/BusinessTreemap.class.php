<?php 

/**
 * Treemap pour les catégories des ecommercants ou des
 * grossistes
 */
class BusinessTreemap extends SystemObjectPersistence {
	

	private $type 		= "";
	private $vendor_id 	= "";
	private $context 	= "";
	private $tech 		= "";
	private $view 		= "";
	private $data 		= array();
	
	private $arrCount 	= array();
	private $HTML  		= "";
	private $test 		= array();
	
	
	protected function __construct($type, $vendor_id, $context, $tech ="", $view) {
		$this->_name 		= "treemap_".$type."_".$vendor_id."_".$context;
		$this->type 		= $type;
		$this->context 		= $context;
		$this->vendor_id 	= $vendor_id;
		$this->tech 		= $tech;
		$this->view 		= $view;
	
	}

	/**
	 * Singleton permettant de récupérer une instance de l'objet BusinessTreemap et dans certains cas
	 * de récupérer l'objet directement en base de données
	 * 
	 * @param 	string 	$type 		le type vendor ou grossiste
	 * @param 	string 	$vendor_id 	l'ID du vendeur
	 * @param 	string 	$context 	Le contexte de navigation dans lequel on est (assoc/a traiter/tous etc...)
	 * @param 	string 	$tech 		La tech du commercant ou du grossiste
	 * @param 	string 	$view 		La view dans laquelle on se trouve grid ou list
	 */
	public static function getInstance($type, $vendor_id, $context, $tech ="", $view){

		if (parent::getObj("treemap_".$type."_".$vendor_id."_".$context)  ){
			return parent::getObj("treemap_".$type."_".$vendor_id."_".$context)  ;
		}else{
			return new self($type, $vendor_id, $context, $tech, $view);
		}
		
	}

		
	/**
	 * Gestion de l'envoi de l'output HTML stocké en propriété d'objet afin de pouvoir sérializer 
	 * l'objet en état avec propriétés déja remplies. Le systeme de cache sera plus performant
	 * (Uniquement utilisé si FK Automotive)
	 *  
	 * @return string $this->HTML 	l'output HTML à afficher
	 */
	public function display()
	{
		$this -> data = $this -> getData();
		
		if($this -> HTML == ""){
			if($this->type == "ecom" | ($this->type == "gr" && $this->vendor_id == "1") | ($this->type == "gr" && $this->vendor_id == "4") | ($this->type == "gr" && $this->vendor_id == "5") ){
				//Cas grossistes et ecom ou chaque catégorie et sous cat subi un COUNT récursivement
				$this ->HTML = $this -> generateHTMLrecursive ($this->data);
			
			}
			else{
				//Cas des grossiste catégories sans enbranchement, cpt des produits à part plus rapide dans getData()
				$this ->HTML = $this -> generateHTML ($this->data);
			}
		}
		
		//Persistence objet uniquement pour FK automotive
		if( ($this->vendor_id == "1" && $this->type == "gr") | ($this->vendor_id == "4" && $this->type == "gr") | ($this->type == "gr" && $this->vendor_id == "5") ){
			$this -> _save();
		}
		return $this ->HTML;
	}
	
	
	/**
	 * Permet de former un tableau multi-dimension avec dimensions imbriquées les unes dans les autres
	 * en fonction de la profondeur de chaque catégorie du vendeur. Les catégories sont obtenues de façon
	 * "plates" du type CatMère > Cat > SousCat et sont ensuite transformées en tableau
	 * 
	 * @return 	array 	$data 	l'array final contenant les catégories
	 */
	private function getData()
	{
		if($this->type == "gr"){
			$db = new BusinessSoukeoGrossisteModel();
		}elseif($this->type == "ecom"){
			$dbClass = "BusinessSoukeo".$this->tech."Model";
			$db = new $dbClass ();
		}
		

		//Ecommercants et grossiste FK
		if($this->type == "ecom" | ($this->type == "gr" && $this->vendor_id == "1") | ($this->type == "gr" && $this->vendor_id == "4") | ($this->type == "gr" && $this->vendor_id == "5") ){
			
			$categs = $db->getDistinctCat($this->vendor_id);
			//Création d'un tableau multidimension représentant l'arbre des catégories
			//On monte une chaine représentant le code permettant de faire des tableaux 
			//Imbriqués les uns dans les autres en fonction de l'arbre des catégories, la chaine est évaluée à la fin
			
			foreach ($categs as $catTree) {
				
				$tree = explode(">", $catTree['CATEGORY']);
				$tree = array_map('trim', $tree);
				$str ="\$arr [] = "; 
				$end = "";
				$i = 0;
				
				foreach ($tree as $k => $branch  ) {
					
					if ($branch == trim(end($tree))  & $k == count($tree)-1 ){
						$branch = str_replace(array("'"), "\'", $branch);
						$str .= " array('$branch' => array() )";
					}else{
						$branch = str_replace(array("'"), "\'", $branch);
						$str .= "array ('$branch' =>  ";
						$end .= ")";
					}
					$i++;
				}
				eval($str.$end.";");				
			}
			
			$data = array();
			
			foreach($arr as $a){
				$data = array_merge_recursive($data, $a);			
			}
		}
		//Autres grossistes
		else{
			
			$categs = $db->getAllCountDistinctCat ($this->vendor_id, $this->context);
			//Classement alphabétique
			foreach ($categs as $cat) {
							
				if($oldLetter == "") $oldLetter = strtoupper(substr(ucfirst($cat['CATEGORY']), 0,1));
				
				if(substr(ucfirst($cat['CATEGORY']), 0,1) == $oldLetter){
					$data[strtoupper($oldLetter)][$cat['CATEGORY']] = null;
					$this -> alphaCount[strtoupper($oldLetter)] += $cat['c'];
				}
				else{
					$oldLetter = strtoupper(substr(ucfirst($cat['CATEGORY']), 0,1));
					$data[strtoupper($oldLetter)][$cat['CATEGORY']] = null;
					$this -> alphaCount[strtoupper($oldLetter)] += $cat['c'];
				}
				
				$this -> arrCount[$cat['CATEGORY']] = $cat['c'];
			}
		}
		return $data;
	}
	
	
	
	/**
	 * Permet de générer un arbre de catégorie imbriqué les unes dans les autres sous forme 
	 * de ul li imbriqués en fonction d'un array multidimensionnel dont on ne connait pas la 
	 * profondeur. Pour ce faire la fonction est récursive et va générer un arbre de catégorie
	 * aussi profond que le tableau fourni en paramètre
	 * 
	 * @param 	array 	$data 		contient les différentes catégories sous forme d'array multidimension
	 * @param 	string 	$parentKey 	la clé catégorie parente de la fonction récursive
	 * @param 	string 	$out 		le flux HTML précedent de la fonction récursive
	 * @return 	string 	$out 		le flux HTML sous forme de chaine 	
	 */
	private function generateHTMLrecursive($data, $parentKey= "", $out ="")
	{
		if($this->type == "gr"){
			$db = new BusinessSoukeoGrossisteModel();
		}elseif($this->type == "ecom"){
			$dbClass = "BusinessSoukeo".$this->tech."Model";
			$db = new $dbClass ();
		}
		
		
		if($this -> type == "gr")$db->prepCountAllProductInCat($this->context);

		if($out =="")
			$out.=  "<ul> "; 
		else {
			$out.= "<ul style='display:none'>";
		}
		
		foreach ($data as $key => $v) {
			
			$nextData = $data[$key];
			
			if(is_array($nextData) & !empty($nextData)) {
				
				//Condition permettant de reformer récursivement la catégorie sous forme Root > Cat > Scat
				$parentKey != "" ? $childkey = $parentKey. " > ". $key : $childkey = $key ; 
				//Fonction bdd permettant de compter les produits dans la catégorie et sous cat avec LIKE
				$count = $db -> countAllProductInCat($childkey, $this->vendor_id, $this->context);
				
				$out.= "<li>
							<span class='tree-expander' onclick='showSubCatIcon(this);'></span>
							<span id='" . htmlentities($childkey, ENT_QUOTES, 'UTF-8') . "' onclick='loadEnteteEcommercant(this); 
																  loadProductList(\"".$this->view."\"); 
																  loadFilter".$filter."(this); 
																  loadBrandFilter(this); 
																  showSubCat(this);'>" . htmlentities($key, ENT_QUOTES, 'UTF-8') ."
							<span class='countProdContext'>(".$count. ")</span></span>  
						</li>";
				
				if(!in_array($branch, $this->test)){
					//Ajout en session de l'id du produit
					$this -> test[]= $childkey;	
				}			
				$out  = $this->generateHTMLrecursive($nextData, $childkey, $out);
				
			}elseif (empty($nextData)) {
				
				$parentKey != "" ? $childkey = $parentKey. " > ". $key : $childkey = $key;
				
				$count = $db -> countAllProductInCat($childkey, $this->vendor_id, $this->context);
				
				if($childkey == $_SESSION[$typev]['categ']){
					$out.= "<li>
								<span class='tree-expander-empty' onclick='showSubCatIcon(this);'></span>
								<span id='" . htmlentities($childkey, ENT_QUOTES, 'UTF-8') . "' class='active' onclick='loadEnteteEcommercant(this); 
																					 loadProductList(\"".$this->view."\"); 
																					 loadFilter".$filter."(this); 
																					 loadBrandFilter(this);
																					 showSubCat(this);'>" . htmlentities($key, ENT_QUOTES, 'UTF-8') ." 
								<span class='countProdContext'>(".$count. ")</span></span> 
							</li>";
					if(!in_array($branch, $this->test)){
						//Ajout en session de l'id du produit
						$this -> test[]= $childkey;	
					}	
				}else{
					$out.= "<li>
								<span class='tree-expander-empty' onclick='showSubCatIcon(this);'></span>
								<span id='" . htmlentities($childkey, ENT_QUOTES, 'UTF-8') . "' onclick='loadEnteteEcommercant(this); 
																	  loadProductList(\"".$this->view."\"); 
																	  loadFilter".$filter."(this); 
																	  loadBrandFilter(this); 
																	  showSubCat(this);'>" . htmlentities($key, ENT_QUOTES, 'UTF-8') ."
								<span class='countProdContext'>(".$count. ")</span></span>  
							</li>";	
					if(!in_array($branch, $this->test)){
						//Ajout en session de l'id du produit
						$this -> test[]= $childkey;	
					}
				}
				
			}else {
				break;
			}
		}
		$out.= "</ul>";

		return $out;
	}


	/**
	 * Dans le cas de catégories "à plat" sans enbranchement systeme de classement 
	 * alphabétique le compte des produits est fait avec des tableaux beaucoup plus
	 * performant
	 * @param 	array 	$data 	contient les différentes catégories sous forme d'array multidimension
	 * @return 	string 	$out 	le flux HTML sous forme de chaine 	
	 */
	private function generateHTML ($data)
	{

		$db = new BusinessSoukeoGrossisteModel();

		
		$db->prepCountAllProductInCat($this -> context);

		$out.=  "<ul> "; 

		foreach ($data as $key => $value) {


			$this -> alphaCount[$key] ? $count = $this -> alphaCount[$key] : $count = '0' ; 
			
			$out.= "<li><span class='tree-expander' onclick='showSubCatIcon(this);'>
						</span><span id='" . htmlentities($key, ENT_QUOTES, 'UTF-8') . "' 
								onclick='loadEnteteEcommercant(this); loadProductList(\"".$this->view."\"); 
										 loadFilter".$filter."(this); loadBrandFilter(this); showSubCat(this);'>" . htmlentities($key, ENT_QUOTES, 'UTF-8') ."
						
						<span class='countProdContext'>(".$count. ")</span></span>  
					</li>";
					
			$out .= "<ul style='display:none'>";
			
			foreach ($value as $cat => $scat) {
				
				$this -> arrCount[$cat] ? $count = $this -> arrCount[$cat] : $count = '0';

				$out.= "<li>
							<span class='tree-expander' onclick='showSubCatIcon(this);'></span>
							<span id='" . htmlentities($cat, ENT_QUOTES, 'UTF-8') . "' 
										onclick='loadEnteteEcommercant(this); loadProductList(\"".$this->view."\"); 
												 loadFilter".$filter."(this); loadBrandFilter(this); 
												 showSubCat(this);'>" . htmlentities($cat, ENT_QUOTES, 'UTF-8') ."
							
							<span class='countProdContext'>(". $count . ")</span></span>  
						</li>";
			}
			
			$out .= "</ul>";	
		}

		$out.= "</ul>";
		
		return $out;
	}

}
 
?>