<?php 

/**
 * Clase type de script qui parse CSV à Remplir
 */
class BusinessImportThelia extends BusinessImportPlugin{
	
	
	protected static $nbRun 		= 0; 		//Compte le nombre de fois que ce batch a été executé

	protected static $db;

	
	/**
	 * Fonction qui parse le CSV ,produit des tableaux associatifs 
	 * et appelle le fonctions d'insertion en BDD
	 */
	public function run()
	{
		//On parse le CSV de l'e-commercant dispo à l'URL fourni
		$this->parseCSV($this->url_to_parse, 21, ";", true);
		
		self::$db = new BusinessSoukeoTheliaModel ();
		
		//Préparation des requetes
		self::$db->prepareInsertSfkProdTH($this->vendorID);
		self::$db->prepareInsertSfkProdAttrTH();
		
		foreach ($this->productListAttribute as $product) {
			//Insert du produit
			
			//Insertion produit
			$state = self::$db->addRowSfkProdTH($product, $this->vendorID) ;
			
			if( $state == 1){ //Si la requete et bien passée
				$this->nbInsert++;
			}elseif($state == 2 ){
				$this->nbUpdate++;
			}elseif($state == 0){
				$this->nbErrors++;
			}
			
			foreach ($this->attributeList as $attribute) {
				
				//Produit possède cet attribut?
				if ($product[$attribute] != "") {

					//On vérifie si l'attribut existe déja 
					$id_attribute = self::$db->checkIfAttributeExistsTH($attribute);
					//Relation produit_Attribut
					if(self::$db->addRowSfkProdAttrTH($product['ID_PRODUCT'], $this->vendorID, $id_attribute,$product['TITRE_DECLINAISON'], $product[$attribute]) == true){

						$this->nbInsertAttr++;
					}	
				}
			}
		}
		self::$nbRun++;		
		
		return true;
	}

	
	/**
	 * @author Philippe_LA/FHKC
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param url 				String 		L'url qui va contenir le CSV
	 * @param nbChampsFixes 	Int 		Le nombre de champs fixe qui sont communs à tous produits
	 * @param delimiter 		String 		Le délimiteur choisi dans le CSV
	 */
	protected function parseCSV($url, $nbChampsFixes, $delimiter)
	{
		$file = file_get_contents($url);
		//$data = str_getcsv(utf8_encode(trim($file)), "\r\n"); //parse les lignes 
		$data = str_getcsv($file, "\r\n"); //parse les lignes 
		
		$data=str_replace(array("\r\n","\n","\r"),'',$data);
		//var_dump ($data) ;
		//Pour chaque ligne on crée un produit
		foreach ($data as $row) {
			$productList[] = str_getcsv($row, $delimiter, '"');
		}
		
		//L'entete est le "premier produit" de la lite
		$headerList = $productList[0];
		//$headerList = str_replace(array("\r\n","\n","\r"),'',$headerList) ;
		//var_dump($headerList);
		$headerList[0]=utf8_encode($headerList[0]);
		$headerList[0] = str_replace(array("\r\n","\n","\r","ï»¿"),'',$headerList[0]) ;
		//var_dump($headerList);
		//On enlève l'entête de la liste produits
		$productList = array_slice($productList, 1);
		
		//Création du tableau associatif
		foreach ($productList as $p) {
			$this->productListAttribute[] = array_combine($headerList, $p);
		}
		
		//Les attributs commencent dans l'entete après les 21 premiers champs fixes
		$this->attributeList = array_slice($headerList,$nbChampsFixes);
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'insert/update effectués pour les
	 * produits
	 * @return nbInsert Int nb de Produits insert/update
	 */
	public function getNbInsert()
	{
		return $this->nbInsert;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'insert/update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbInsertAttr()
	{
		return $this->nbInsertAttr;
	}
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * produits
	 * @return nbInsert Int nb de Produits insert/update
	 */
	public function getNbUpdate()
	{
		return $this->nbUpdate;
	}
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbUpdateAttr()
	{
		return $this->nbUpdateAttr;
	}	
	/**
	 * @return url_to_parse String l'url de l'e-commerçant
	 */
	public function getUrl_to_parse()
	{
		return $this->url_to_parse;	
	}
	
	
	/**
	 * @return vendorID Int l'id du vendeur
	 */
	public function getVendorID()
	{
		return $this->vendorID;
	}
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbErrors()
	{
		return $this->nbErrors;
	}	
	
		/**
	 * @return nbRun le nombre de fois que ce Script a été effectué
	 */
	public static function getNbRun()
	{
		return self::$nbRun;
	}
		
}
?>