<?php

class BusinessFacture extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $facture_id;
	protected $facture_code;
	protected $emeteur_addr_id; 
	protected $recipient_addr_id;
	protected $date_created;
	protected $date_updated;


	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setFacture_id($facture_id ){
		$this -> facture_id = $facture_id ;
	}

	public function setFacture_code($facture_code)
	{
		$this -> facture_code = $facture_code;
	}
	
	public function setEmeteur_addr_id($emeteur_addr_id)
	{
		$this -> emeteur_addr_id = $emeteur_addr_id;
	}
	
	public function setRecipient_addr_id($recipient_addr_id)
	{
		$this -> recipient_addr_id = $recipient_addr_id;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}

	public function getFacture_id(){
		return $this -> facture_id;
	}

	public function getFacture_code()
	{
		return $this -> facture_code ;
	}
	
	public function getEmeteur_addr_id()
	{
		return $this -> emeteur_addr_id;
	}
    
    public function getEmeteur_addr()
    {
        $factureAddressManager = new ManagerFactureAddress ();
        
        return $factureAddressManager -> getFactureAddressById($this -> emeteur_addr_id);
    }
	
	public function getRecipient_addr_id()
	{
		return $this -> recipient_addr_id;
	}
	
    public function getRecipient_addr()
    {
        $factureAddressManager = new ManagerFactureAddress ();
        
        return $factureAddressManager -> getFactureAddressById($this -> recipient_addr_id);
    }
    
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function isNew()
	{
		if ($this -> getFacture_id()){
			return false;
		}else{
			return true;
		}
	}
    
    public function getAllItems()
    {
        return self::$db -> getAllItemsByFacture($this -> facture_id);
    }
    
    public function getFactureCom()
    {
        $factureComManager = new ManagerFactureCom();
        
        return $factureComManager -> getFactureComByFactureId($this -> facture_id);
    }   

    public function getFactureCredit()
    {
        $factureCreditManager = new ManagerFactureCredit();
        
        return $factureCreditManager -> getBy(array("facture_id" => $this -> facture_id));
    } 
           
	public function save()
	{

	}
	
}