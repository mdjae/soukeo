<?php

class BusinessAbonnement extends BusinessEntity {
	
	protected $abonnement_id;
	protected $label;
	protected $abonnement_limit_product; 
	

	public function __construct ($data = array()) {
		
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setAbonnement_id($abonnement_id ){
		$this -> abonnement_id = $abonnement_id;
	}

	public function setLabel($label)
	{
		$this -> label = $label;
	}
	
	public function setAbonnement_limit_product($abonnement_limit_product)
	{
		$this -> abonnement_limit_product = $abonnement_limit_product;
	}
	
	public function getAbonnement_id( ){
		return $this -> abonnement_id;
	}

	public function getLabel()
	{
		return $this -> label;
	}
	
	public function getAbonnement_limit_product()
	{
		return $this -> abonnement_limit_product;
	}
	
	public function isNew()
	{
		if ($this -> getAbonnement_id()){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}