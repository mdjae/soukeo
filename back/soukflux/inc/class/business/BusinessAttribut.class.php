<?php

class BusinessAttribut extends BusinessEntity {
	
	protected static 	$db ;
	
    protected $id_attribut;
	protected $cat_id;
	protected $code_attr;
	protected $label_attr;
    protected $current_cat_id;
    protected $is_filterable;

	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	

		if (!empty($data)) {
			$this->hydrate($data);
		}	
	}
	
    public function setId_attribut($id_attribut)
    {
        $this -> id_attribut = $id_attribut;
    }
	
	public function setCat_id($cat_id)
	{
		$this -> cat_id = $cat_id;
	}

	public function setCode_attr($code_attr)
	{
		$this -> code_attr = $code_attr;
	}
	
    public function setIs_filterable($is_filterable)
    {
        $this -> is_filterable = $is_filterable;    
    }
    
	public function setLabel_attr($label_attr)
	{
		$this -> label_attr = $label_attr;
	}
	
    public function getId_attribut()
    {
        return $this -> id_attribut ;
    }
    
    public function getCat_id()
    {
        return $this -> cat_id;
    }

    public function getCode_attr()
    {
        return $this -> code_attr;
    }
    
    public function getLabel_attr()
    {
        return $this -> label_attr;
    }
    
    public function getIs_filterable()
    {
        return $this -> is_filterable;
    }
    
    public function getAttribut_category($cat_id)
    {
        $manager = new ManagerAttributCategory();
        
        if($attribut_category = $manager -> getBy(array("id_attribut" => $this->id_attribut, "cat_id" => $cat_id))){
            
            return $attribut_category;
        }
        else{
            
            return false;
        }
    }
    
    public function getAllProductAttributes()
    {
        $productAttributManager = new ManagerProductAttribut();
        
        $product_attr_list = $productAttributManager -> getAll(array("id_attribut" => $this -> id_attribut, "active" => 1));
        
        
        return $product_attr_list;
    }

    public function getCountProdsWithThis()
    {
        return self::$db -> getCountProdsWithAttr($this->id_attribut);
    }

    public function getCountProdsInCatWithThis($cat_id)
    {
        return self::$db -> getCountProdsWithAttrInCat($this->id_attribut, $cat_id);
    }

    public function getCountProdsInListCatWithThis($cat_list)
    {
        return self::$db -> getCountProdsWithAttrInListCat($this->id_attribut, $cat_list);
    }
    
    public function getCountProdsInCurrentCatWithThis()
    {
        return self::$db -> getCountProdsWithAttrInCat($this->id_attribut, $this->current_cat_id);
    }


    public function getDistinctValues()
    {
      
        return self::$db -> getDistinctValuesAttribut($this->id_attribut);
    }
    
    public function setCurrent_cat_id($current_cat_id)
    {
        $this -> current_cat_id = $current_cat_id;
    }

    public function getDistinctValuesForCat($cat_id) 
    { 
      
        return self::$db -> getDistinctValuesAttributForCat($this->id_attribut, $cat_id);
    }

    public function getDistinctValuesInCatList($cat_list) 
    { 
      
        return self::$db -> getDistinctValuesAttributInCatList($this->id_attribut, $cat_list);
    }
                            	
	public function save()
	{
		
	}
	
    public function isNew()
    {
        $attributManager = new ManagerAttribut();
        
        if ( $this -> id_attribut || ($attr = $attributManager -> getBy(array("code_attr" => trim($this -> code_attr), "cat_id" => $this->cat_id))) ){
            
            return false;
        }else{
            return true;
        }
    }
   
}