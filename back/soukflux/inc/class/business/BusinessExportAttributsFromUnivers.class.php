<?php 

/**
 * 
 */
class BusinessExportAttributsFromUnivers extends BusinessExportCSV {
	
	protected 			$path = "/opt/soukeo/export/attributs/" ;
	protected static 	$db ;
	protected 			$name = "BusinessExportAttributsFromUnivers";
	protected 			$description = "Fichier contenant les attributs pour chaque catégories des univers";
	protected           $categorie;
	
	function __construct(BusinessCategorie $categorie) {

		parent::__construct($this -> path);
        
        $this->categorie = $categorie;

		$this -> fileName = $this->categorie->getCat_label()."_".date("d-m-Y-H-i-s").".csv" ;		
		
	}
	
	public function makeContent()
	{
        self::$db = new BusinessSoukeoModel();
        $result = self::$db->getAllLastLevelCatOfUniverse($this->categorie->getCat_id());
        
        $rs = explode("-",$result);
        
        foreach ($rs as $cat_id) {
            
            $line = array();
            $tmp = self::$db->getStrCateg($cat_id);
             
            $line[] = substr($tmp, 0, strlen($tmp) -2);
            
            $attr_list = self::$db -> getAllCodeAttrByProdFromCat($cat_id);

            foreach ($attr_list as $attr_code) {
                $line[] = $attr_code["code_attr"];
            }
            
            $this -> makeLine($line); 
        }
	}
	
	public function saveFileEntryInDB()
	{
		self::$db -> saveFileEntry($this->fileName, $this->name, $this->description);
	}
	
}

?>