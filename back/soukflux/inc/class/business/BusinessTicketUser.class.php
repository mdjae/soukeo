<?php

class BusinessTicketUser extends BusinessEntity {
	
	protected static    $ticketUserManager;
    
	protected $ticket_id;
	protected $user_id;
    protected $date_created;
    protected $seen;

	public function __construct ($data = array()) {
		
        self::$ticketUserManager = new ManagerTicketUser();
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setTicket_id($ticket_id )
	{
		$this -> ticket_id = $ticket_id ;
	}

	public function setUser_id($user_id)
	{
		$this -> user_id = $user_id;
	}
	
    public function setDate_created($date_created)
    {
        $this -> date_created = $date_created;
    }
    
    public function setSeen($seen)
    {
        $this -> seen = $seen;
    }
    
    public function getTicket_id()
    {
        return $this -> ticket_id;
    }

    public function getUser_id()
    {
        return $this -> user_id;
    }	
    
    public function getDate_created()
    {
        return $this -> date_created;
    }
    
    public function getSeen()
    {
        return $this -> seen;
    }
	
	public function isNew()
	{
        
		if (self::$ticketUserManager -> getById(array("ticket_id" => $this -> ticket_id,
		                                        "user_id" => $this -> user_id ))){
			return false;
		}else{
			return true;
		}
	}
    

	public function save()
	{

	}
	
}