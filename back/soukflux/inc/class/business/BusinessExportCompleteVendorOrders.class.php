<?php 

/**
 * 
 */
class BusinessExportCompleteVendorOrders extends BusinessExportCSV {
	
	protected 			$path = "/opt/soukeo/export/";
	protected static 	$db;
	protected 			$name = "BusinessExportCompleteVendorOrders";
	protected 			$description = "Fichier de facturation commerçants";
	
	
	function __construct() {
		
		parent::__construct($this -> path);
		
		$this -> fileName = "export_vendor_orders".date("d-m-Y-H-i-s").".csv" ;			
		
	}
	
	
	public function makeContent()
	{
		self::$db = new BusinessSoukeoModel();
		
		$this -> makeLine($this -> getHeaderChpsFixes());
		
		$pos = self::$db->getAllCompleteVendorPo();
		
		$totCom = 0;
		$totFBanq = 0;
		
		$start = true ;
		foreach ($pos as $po) {
			//On change de vendeur on créer une rutpture
			
			if($po['udropship_vendor'] != $oldPoVendor && !$start){
				$this->makeRupture();
				
			}
				$this -> makeLine($this->getPoChampsFixes($po));
				$this->totCom +=  $po['montant_commission'];
				$this->totFBanq += $po['frais_banquaires']; 
				$oldPoVendor = $po['udropship_vendor'];
				$start = false;
		}
		$this->makeRupture();
				
	}

	
	protected function makeRupture(){
		$ligne1 = array(	"", "",	"",	"",	"", "", "", "",
			"Total HT",
			$this->totCom,
			$this->totFBanq);
				
		$this->totComTTC = $this->totCom*1.085;
		$this->totFBanqTTC = $this->totFBanq*1.085;
				 
		$ligne2 = array(	"", "",	"",	"",	"", "", "", "",
			"Total TTC",
			$this->totComTTC,
			$this->totFBanqTTC);
		
		$ligne3 = array(	"", "",	"",		"",	"", "", "", "",
			"Total",
			"",
			$this->totComTTC + $this->totFBanqTTC);
		
		$ligne4 = array(	"", "",	"",	"",		"", "", "", "",
				"",
				"",
				"");
		
		
		$this -> makeLine($ligne1);
		$this -> makeLine($ligne2);
		$this -> makeLine($ligne3);
		$this -> makeLine($ligne4);
		$this->totFBanqTTC  = $this->totComTTC = $this->totCom = $this->totFBanq = 0;
	}


	/**
	 * Retourne un array contenant les labels des champs fixes 
	 */
	protected function getHeaderChpsFixes()
	{
		return array(	"COMMANDE",
						"BON_DE_COMMANDE",
						"DATE_CREATE",
						"SKU",
						"NOM_PRODUIT",
						"CATEGORIE",
					//	"ID_VENDEUR",
						"NOM_VENDEUR",
						"PRIX_DE_VENTE",
						"COMMISSION",
						"MONTANT_COMMISSION",
						"FRAIS_BANQUAIRES");
	}


	/**
	 * Fonctionnant retournant un array contenant les données pour les champs fixes
	 * du produit
	 */
	protected function getPoChampsFixes($po)
	{

		return array(	$po['commande'],
						$po['bon_de_commande'],
						$po['date_created'],
						$po['sku'],
						$po['name'],
						$po['cat_label'],
					//	$po['udropship_vendor'],
						$po['VENDOR_NOM'],
						str_replace(".", ",", $po['base_row_total_incl_tax']),
						str_replace(".", ",", $po['COMM']),
						str_replace(".", ",", $po['montant_commission']),
						str_replace(".", ",", $po['frais_banquaires'])
						);
	}
	
	public function saveFileEntryInDB()
	{
		self::$db -> saveFileEntry($this->fileName, $this->name, $this->description);
	}
		
}

?>