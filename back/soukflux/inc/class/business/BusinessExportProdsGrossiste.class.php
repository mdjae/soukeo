<?php 

/**
 * 
 */
class BusinessExportProdsGrossiste extends BusinessExportCSV {
	
	protected 			$path = "/opt/soukeo/export/";
	protected static 	$db;
	protected 			$name = "BusinessExportProdsGrossiste";
	protected 			$description = "Fichier des produits grossistes en vente";
	
	
	function __construct() {
		
		parent::__construct($this -> path);
		
		$this -> fileName = "exportgrossiste_".date("d-m-Y-H-i-s").".csv" ;			
		
	}
	
	
	public function makeContent()
	{
		self::$db = new BusinessSoukeoGrossisteModel();
		
		$this -> makeLine($this -> getHeaderChpsFixes());
		
		$productList = self::$db->getAllProdGrossiste();
				
		foreach ($productList as $product) {
			$this -> makeLine($this->getProductChpsFixes($product));
		}
		
	}


	/**
	 * Retourne un array contenant les labels des champs fixes 
	 */
	protected function getHeaderChpsFixes()
	{
		return array(	"ART_ID",
						"ART_ID_CLIENT",
						"ART_LIBELLE",
						"ART_STOCK",
						"ART_GAMME",
						"ART_ID_UC",
						"ART_ID_CLI",
						"ART_POIDS",
						"ART_PA",
						"ART_ID_OPE");
	}


	/**
	 * Fonctionnant retournant un array contenant les données pour les champs fixes
	 * du produit
	 */
	protected function getProductChpsFixes($product)
	{
		if($product['EAN'] != "NULL"){
			$ean = $product['EAN'];
		}else{
			$ean = $product['REF_GROSSISTE'];
		}
		
		
		$cat = $product['CATEGORY'];
		$cat = explode('>', $cat);
		$cat = trim($cat[0]);
		
		
		return array(	$ean,
						$product['REF_GROSSISTE'],
						$product['NAME_PRODUCT'],
						$product['GROSSISTE_NOM'],
						$cat,
						"SKO",
						"",
						$product['WEIGHT'],
						"",
						""
						);
	}
	
	public function saveFileEntryInDB()
	{
		self::$db -> saveFileEntry($this -> fileName, $this->name, $this->description);
	}		
}

?>