<?php 
/**
 * Classe de gestion sspécifique aux import de MAGINEA
 * Permet de formatter correctement les champs de données en fonctions des champs fournis par MAGINEA
 * Permet également de nettoyer les descriptions et de gérer les erreurs en cas d'EAN non présent 
 * ou Stock vide spécifique à MAGINEA
 */
class BusinessGrossisteMAGINEA 
{
	
	protected $delimiter ; 			//Délimiteur CSV pour LDLC
	
	protected $errors = array(); 	//Arry d'erreurs et messages d'erreurs

	
	/**
	 * Fonction __construct basique profitant de définir le délimiteur
	 * pour le CSV de MAGINEA
	 */
	public function __construct ()
	{
		$this -> delimiter = "|";
		
	}
	

	/**
	 * Gestion des erreurs de stock et d'EAN tout en fabriquant un tableau contenant
	 * les différentes erreurs sur les produits avec leur id_produit ainsi que la cause des erreurs.
	 * @param 	array 		$product 	l'array du produit à analyser
	 * @return 	boolean 				Retourne true si une erreur existe ou bien false
	 */
	public function errorsExist($product)
	{
		if(  $product['stock'] == "" | $product['ean'] =="" | $product['poids'] == ""){
							
			$message = "";
			

			if($product['ean'] == ""){
				$message .= " EAN INVALIDE ";
			}
			if($product['stock'] == "" ){
				$message .= " STOCK INVALIDE " ;
			}
			if($product['poids'] == "" || $product['poids'] == 0){
				$message .= " POIDS INVALIDE " ;
			}
			$this -> errors [] = $message." POUR produit EAN : ".$product['ean']. " RefGrossiste : ".$product['id']."";
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Permet de nettoyer la description ou autres champs du produit MAGINEA
	 * @param 	array 	$product 	contenant les infos du produit
	 * @return 	array 	$product 	contenant les infos nettoyées du produit
	 */
	public function cleanDescription($product)
	{
		$product['ref'] = $product['id'];
		return $product;
	}



	/**
	 * Formatte les champ MAGINEA afin de recréer un array avec des clés de tableau générique
	 * permettant d'avoir des fonctions uniques d'insertions une fois du coté du modèle.
	 * Il y a aussi la gestion du stock par défaut du grossiste et la création d'URL d'image permettant
	 * d'obtenir différentes qualités d'image sur le site MAGINEA
	 * @param 	array 	$product 	 	contenant le produit parsé du CSV
	 * @param 	array  	$productOld 	contenant le produit grossiste s'il existe déja avant le parsing
	 * @return 	array 	$product 		contenant le produit traité
	 */
	public function formatFields($product, $prodGrOld=""){
		
		//Si des valeurs existaient avant dans le cas où il y a eu un Crawling de la description on ne veux pas écraser
		if(isset($prodGrOld) && $prodGrOld != null){
			$product['long_desc'] = $prodGrOld['DESCRIPTION'];
			$product['short_desc'] = $prodGrOld['DESCRIPTION_SHORT'];
			$product['weight'] = $prodGrOld['WEIGHT'];
		}
		else{
			$product['weight'] = $product['poids'];
		}
		
		if($product['stock'] == "1"){
			//echo SystemParams::getParam('grossiste*stockdefaultgrossiste'). " ";
			$product['stock'] = SystemParams::getParam('grossiste*stockdefaultgrossiste');
		}else{
			$product['stock'] = 0;
		}
		
		
		
		$product['manufacturer'] = ucfirst(strtolower($product['constructeur']));
		$product['category'] = substr_replace($product['catgorie'], stripAccents(substr($product['catgorie'], 0, 1)), 0, 1);
		$product['price'] = $product['prixHT'];
		$product['ref'] = $product['id'];
		$product['picture2'] = 'http://media.maginea.com'.$product['image'];
		
		//traitement image 
		//Les images hautes résolution ont presque la même adresse sauf deux derniers chuiffres vant "_" ont +2 rajoutés
		$tmp = explode("_", $product['image']);
		$chiffre = substr($tmp[0], -4);
		$chiffrePicHD = $chiffre +2 ; //Récup image HD
		$chiffrePicLD = $chiffre +1 ; //Récup image LD
		
		//Création images HD et LD
		$product['picture1'] = 'http://media.maginea.com' . substr_replace($tmp[0], $chiffrePicHD, -4) . '_2.jpg';
		$product['picture3'] = 'http://media.maginea.com' . substr_replace($tmp[0], $chiffrePicLD, -4) . '_1.jpg';
		$product['name'] = $product['designation'];
		return $product;
	}


	/**
	 * Permet de récupérer le tableau contenant les erreurs
	 * rencontrés lors du parsing du CSV notament grace à la fonction errorsExists
	 * @see errorsExist
	 * @return array 	errors  contenant les erreurs et messages d'erreurs
	 */		
	public function getErrors() 
	{
		return $this -> errors;
	}
	
	
	/**
	 * Permet de récupérer le délimiteur associé au CSV actuel
	 * @return delimiter String représente le délimiteur utilisé dans le CSV
	 */	
	public function getDelimiter()
	{
		return $this -> delimiter;
	}

}

?>