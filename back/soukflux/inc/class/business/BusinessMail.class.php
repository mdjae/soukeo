<?php
date_default_timezone_set('Europe/Paris');
/**
 * Batch de traitement des fichiers Euromaster pour mise � disposition des franchis�s
 */
if (isset($main_path)) {
    require_once $main_path . "/" . $root_path .'thirdparty/swiftmail/lib/swift_required.php';
    require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
} else {
   	require_once $root_path . 'thirdparty/lib/swift_required.php';
    require_once $root_path . "inc/offipse.inc.php";
}
class BusinessMail {
    
    
    public function BusinessMail(){
        
        
    }
        
    protected function mailBodyGest(){
     return $body = _(" Bonjour,")."\n\n".
                _(" Nous vous remercions d'avoir mis à jour votre terrain sur my-campsite.eu.")."\n".
                _(" Vos modifications ont bien été enregistrées et seront modérées dans les prochains jours.")."\n".
                _(" Si votre terrain est publié sur l'un de nos sites Internet, vous pourrez les visualiser d'ici quelques instants.")."\n\n".
                _(" Si vous souhaitez nous contacter directement ").": martine.duparc@motorpresse.fr "."\n\n".

                _(" Très cordialement,")."\n".
                _("L'équipe Guides de Motor Presse France")."\n".
                "www.my-campsite.eu";
       
    }
    
    public function sendmailGest($email){
        $transport = Swift_SendmailTransport::newInstance('/usr/sbin/exim -bs');
        $mailer = Swift_Mailer::newInstance($transport);
        
        $mail = Swift_Message::newInstance() ;
        $mail->setSubject(_("My-campsite.eu - Mise à jour de votre terrain"));
        $mail->setFrom(SystemParams::getParam("system*email_from"));
        $mail->setTo($email);
        $mail->setBody($this->mailBodyGest());
        
        $result = $mailer->send($mail);
   /*     
       $mail = new Mail();
       $mail -> From(SystemParams::getParam("system*email_from"));
       $mail -> To($email);
       $mail -> Subject( _("my-campsite.eu : envoie de vos données") );
       $mail -> Body($this->mailBodyGest(), 'text/html', 'utf-8' );
      // $mail -> Attach($TMP_dir . "/" . $this->alert_file , "text/csv");
   
       $mail -> Send() ;
        
  */
    }
    
}
