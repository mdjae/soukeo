<?php 
/**
 * 
 */
class BusinessTicketPriority extends BusinessEntity {
	
    
	protected $priority_id;
    protected $label;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setPriority_id($priority_id)
    {
        $this -> priority_id = $priority_id;
    }
    
    public function getPriority_id()
    {
        return $this -> priority_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function isNew()
    {
        $manager = new ManagerTicketPriority();
        
        if ($this -> priority_id || ($priority = $manager -> getBy(array("label" => $this->label) )) ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>