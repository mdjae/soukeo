<?php

/*
 * EXPORT XML A DESTINATION DES SITES WEBS
 * PREND EN PARAMETRE UN ARRAY
 * PEUT TRAITER DE 1 A N TERRAIN.
 */

/*
 * Structure XML :
 * ROOT (vide)
 *   sites (vide)
 *     pays (id , nom abréviation)
 *       region (id , nom, nomeditionmaj)
 *          decoupage (id, code, typedecoupage , nom , nomeditionmaj, carte)
 *              commune ( id paysid, regionid, decoupageid , insee, nom, nomclassement, nom
 * editionmaj , nomeditionmin, situation, altitude, nbhabitants, cartecampingcaravanaing,
 * paysnom, regionnom  , decoupagenom, codepostal, fpassion , pavillonbleu)
 *                 site ( liste to export ... ( database ))
 *
 * */

class BusinessExportXML
{

    protected static $db;

	/**
	 * Tableau d'ID de sites a exporter
	 */
    protected $ids;

	/**
	 * dossier exports
	 */
    protected $rep = "/opt/mpf/export/";
	
	/**
	 * dossier archive des fichiers exportes
	 */
    protected $archiRep = "/opt/mpf/archive/export/";

	/**
	 * nom du site de camping pour le nom de fichier xml genere
	 */
	//var $addFilename = ""; 



    protected $repTmp ;

	/**
	 * Type de caracteristique (1 = Gestionnaire, 3 = Guide)
	 * @var int
	 */
    //var $typedata = null;
	
	/**
	 * IDs des sites concernes par l'export
	 */
	//var $allIds = null;
	
	/**
	 * Requete de selection des sites a exporter
	 */
	//var $stmtExportSite = null;
	//var $stmtExportSiteBis = null;
	
	/**
	 * Requete de selection des donnees a exporter
	 */
	//var $stmtData = null;
	
	/**
	 * Code du site internet concerne par l'export
	 */
	//var $exportsite = null;
	
	/**
	 * timestamp
	 */
    protected $timestp ;

    protected $filename = "exportCamping";
    //array des id a traiter    

    /**
	 * Renvoi un tableau d'ID par rapport a un tableau de base
	 */
    protected function formatarr($rs)
    {
        $arr = array();
        foreach ($rs as $R)
        {
            $arr[] = $R['ID'];
        }
        return $arr;
    }

    /**
	 * @param array $ids : array des id site a traiter.
	 */
    public function BusinessExportXML($ids)
    {

        self::$db = new sdb();
        $this -> timestp = date("Ymd") . "_" . date("His");
        if ($ids == "ALL")
        {
            $this -> allIds = "ALL";
            $this -> addFilename = "Guide". $this -> timestp .".xml";     
            
        }elseif($ids == 'ALLSITE'){
            
            $this -> allIds = "ALL";
            $this -> addFilename = ".xml"; 
        }
        else
        {
            $this -> allIds = $ids;
            $this -> addFilename = $ids[0]['ID'] . ".xml";
        } 
    }

	/**
	 * Prepare les requetes pour selectionner l'export site :
	 *  - une requete stmtExportSite : select les ID des sites voulus ($this -> allIds) pour le guidecode voulu
	 *  - une requete stmtExportSiteBis : idem pour 2 guidecodes
	 *  - place la query dans $this -> stmtData : toutes les caracteristiques
	 *  - place la query de log dans $this -> stmtLogExport	: log export
	 */
    protected function initQuerySite()
    {
        $query = "SELECT DISTINCT SITEID AS ID  FROM PUBLICATIONSITE 
                  WHERE   SITEID in (" . implode(",", $this -> formatarr($this -> allIds)) . ") 
                  AND GUIDECODE = ?";
        $this -> stmtExportSite = self::$db -> prepare($query);

        $query = "SELECT DISTINCT SITEID AS ID  FROM PUBLICATIONSITE WHERE   SITEID in (" . implode(",", $this -> formatarr($this -> allIds)) . ") 
                  AND ( GUIDECODE = ? OR GUIDECODE = ? ) ";
        $this -> stmtExportSiteBis = self::$db -> prepare($query);
        $this -> initQueryGuide();

    }

	/**
	 * Prepare les query pour l'export GUIDE
	 * place la query dans $this -> stmtData : toutes les caracteristiques
	 * place la query de log dans $this -> stmtLogExport : log export
	 */
    public function initQueryGuide()
    {
        $this -> prepDataSite();

        $query = "INSERT INTO c_export_log (usercode, filename, path, filenamearch, patharch, logdate, logaction, logtype) values ( ?, ?, ?, ?, ? , NOW(), ?, ?)";
        $this -> stmtLogExport = self::$db -> prepare($query);
    }

    /**
	 * Ajoute l'info export dans la table des logs d'exports
	 */
    protected function logExport()
    {         
        $filename = $this -> filename . $this -> addFilename;
        $path = $this -> rep . $this -> addRep;
        $filenamearch = $this -> filename . $this -> timestp  . $this -> addFilename;
        $patharch = $this -> archiRep . $this -> addRep ;
        $logaction = 'EXPORT';
        
        if ($_SESSION['smartlogin']  ) $user = $_SESSION['smartlogin'] ;
        else $user = "Batch";

        $this -> stmtLogExport -> bindParam(1, $_SESSION['smartlogin'] ? $_SESSION['smartlogin'] : "Batch");
        $this -> stmtLogExport -> bindParam(2, $filename);
        $this -> stmtLogExport -> bindParam(3, $path);
        $this -> stmtLogExport -> bindParam(4, $filenamearch);
        $this -> stmtLogExport -> bindParam(5, $patharch);
        $this -> stmtLogExport -> bindParam(6, $logaction);
        $this -> stmtLogExport -> bindParam(7, $this -> typeExport);
        $this -> stmtLogExport -> execute();

    }

	/**
	 * Export pour un site internet
	 */
    public function exportToSite()
    {
        $this->typedata = '1';
        $this -> initQuerySite();
        if (is_file($this -> rep . "FTP_OK"))
        {
            unlink($this -> rep . "FTP_OK");
        }
        $out = $this -> exportXMLSite();
        if (!is_file($this -> rep . "FTP_OK"))
            file_put_contents($this -> rep . "FTP_OK", "FTP_OK");
        return $out;
    }
    
	/**
	 * Renvoi les ID des sites dont la derniere modification est posterieure au dernier export GUIDE
	 * @return array $rs : tableau d'ID de sites
	 * @param bool $validatedData : Si on exporte les sites validés depuis le dernier export (sinon sites modifies)
	 */
    public function getIdsForExportAll($validatedData = true){
        
        //VALIDATION AU LIEU DE MODIFICATION. 
         $query = "SELECT DISTINCT S.ID
                  FROM
                  SITE S INNER JOIN PAYS P  ON S.PAYSGEOID = P.ID
                  INNER JOIN COMMUNE C      ON S.COMMUNEGEOID = C.ID
                  INNER JOIN DECOUPAGEGEO D ON C.DECOUPAGEID = D.ID
                  INNER JOIN REGION R       ON D.REGIONID = R.ID
                  INNER JOIN CARACTERISTIQUESITE CA ON CA.SITEID = S.ID" ;
                  
        if($validatedData){          
        	$query .= " WHERE S.DATEVALID > ( SELECT max(logdate) FROM c_export_log c WHERE logaction='EXPORT' AND logtype='export_guide')" ;
        } else {
        	$query .= " WHERE S.DATEMODIF > ( SELECT max(logdate) FROM c_export_log c WHERE logaction='EXPORT' AND logtype='export_guide')";
			//$query .= " WHERE S.DATEMODIF > ( SELECT max(logdate) FROM c_export_log c WHERE logaction='EXPORT' AND logtype='export_guide')";		
        }     
             
        $query .= " ORDER BY S.PAYSGEOID,  R.ID , D.ID , C.ID";
        return $rs = self::$db -> getall($query);
        //     AND S.DATEMODIF > (SELECT max(logdate) From c_export_log where logtype ='import_guide' ) 
    }
    
	/**
	 * Export XML pour les publications, de tous les sites modifiés depuis le dernier export GUIDE
	 * caracteristiques de type 1 (Gestionnaire)
	 * @param bool $validatedData : Si on exporte les sites validés depuis le dernier export (sinon sites modifies)
	 */
    public function exportToAllSite($validatedData = true)
    {
        //EXPORT DES DONNEES DE TYPE 1 ( GESTIONNAIRE ) ONT ETE MODIFIER PAR L IMPORT GUIDE
        $this->typedata = '1';
        
        $this -> allIds = $this->getIdsForExportAll($validatedData);
        
        $this -> initQuerySite();        
        $out = $this -> exportXMLSite();

        return $out;
    }

    /**
	 * Exporte XML pour Guide (que les site ayant ete modifies depuis le dernier export GUIDE)
	 * caracteristiques de type 3 (Guide)
	 */
    public function exportToGuide()
    {
        $this->typedata = '3';
        $this -> addRep = "guides/";
        //$this->addFilename = "ExportCampingGuide.xml";
        $this -> typeExport = "export_guide";

        if (!is_file($this -> rep . $this -> addRep . "IN_PROGRESS"))
        {
            file_put_contents($this -> rep . $this -> addRep . "IN_PROGRESS", "in_progress");
        }

        $this -> ids = $this -> getIdsForExportAll();
        $this -> initQueryGuide();
        $this -> makeXml();

        if (is_file($this -> rep . $this -> addRep . "IN_PROGRESS"))
        {
            unlink($this -> rep . $this -> addRep . "IN_PROGRESS");
        }

    }
	
	/**
	 * export dans dossier temporaire
	 */
    public function exportToTmp(){
        $this->typedata = '1';
        $this -> filename =  "import";
        $this -> ids = $this->allIds;
        $this -> addRep = "tmp/";
        $this -> typeExport = "export_tmp";
        $this -> initQuerySite();
        
        $this -> makeXml();
        
    }

	/**
	 * Genere le XML a exporter pour le site defini dans $this -> exportsite
	 */
    public function exportXMLSite()
    {

        //   $temps_debut = microtime(true);
        $out = _("Terrain exporté vers ") . ": ";

        //EXPORT TO EASYCAMPING
        $this -> exportsite = "BELAIRWEB";
        $this -> stmtExportSite -> bindParam(1, $this -> exportsite);
        $this -> stmtExportSite -> execute();
        $idsite = $this -> stmtExportSite -> fetchall(PDO::FETCH_ASSOC);

        //SOME Site to export to easy camping let's make the xml
        if (is_array($idsite[0]))
        {
            $out .= 'France Camping ';
            $this -> filename =  "importeasy";
            $this -> ids = ($idsite);
            $this -> addRep = "easycamping/";
            $this -> typeExport = "export_easy_camping";
            $res = $this -> makeXml();
			$out .= '\n --------- ' . $res ;
        }
        $idsite = "";

        //EXPORT TO camping France
        $this -> exportsite = "GUIDEOFFCC";
        $this -> stmtExportSite -> bindParam(1, $this -> exportsite);
        $this -> stmtExportSite -> execute();
        $idsite = $this -> stmtExportSite -> fetchall(PDO::FETCH_ASSOC);

        //SOME Site to export to camping france let's make the xml
        if (is_array($idsite[0]))
        {
            $out .= "Camping France ";
             $this -> filename =  "importCampings";
            $this -> ids = ($idsite);
            $this -> addRep = "campingfrance/";
            $this -> typeExport = "export_site_camping_france";
            $res = $this -> makeXml();
			$out .= '\n --------- ' . $res ;
        }
        $idsite = "";

        //EXPORT TO Campingcar
      //  $this -> exportsite = "BELAIRCC";
      //  $this -> exportsiteBis = "AIRESERVCC";
      //  $this -> stmtExportSiteBis -> bindParam(1, $this -> exportsite);
      //  $this -> stmtExportSiteBis -> bindParam(2, $this -> exportsiteBis);
      //  $this -> stmtExportSiteBis -> execute();
      //  $idsite = $this -> stmtExportSiteBis -> fetchall(PDO::FETCH_ASSOC);
        
        $this -> exportsite = "AIRESERVCC";
        $this -> stmtExportSite -> bindParam(1, $this -> exportsite);
        $this -> stmtExportSite -> execute();
        $idsite = $this -> stmtExportSite -> fetchall(PDO::FETCH_ASSOC);       

        //SOME Site to export to Campingcar let's make the xml
        if (is_array($idsite[0]))
        {
            $out .= "Camping-car ";
            $this -> ids = ($idsite);
            $this -> addRep = "campingcar/";
            $this -> typeExport = "export_site_campingcar";
           	$res = $this -> makeXml();
			$out .= '\n --------- ' . $res ;
        }

        //$temps_fin = microtime(true);
        return $out;
        //echo('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

    }

	/**
	 * 
	 */
	protected function xmlentities($string) {
	  //  return str_replace(array("&", "<", ">", "\"", "'"),
	  //      array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), $string);
	    return $string;
	}

	/**
	 * Selectionne tous les sites avec infos : pays, region, commune ...
	 * @return array $rs : tableau d'enregistrements site
	 */
    protected function allsite()
    {
        $query = "SELECT DISTINCT S.ID
                  FROM
                  SITE S INNER JOIN PAYS P  ON S.PAYSGEOID = P.ID
                  INNER JOIN COMMUNE C      ON S.COMMUNEGEOID = C.ID
                  INNER JOIN DECOUPAGEGEO D ON C.DECOUPAGEID = D.ID
                  INNER JOIN REGION R       ON D.REGIONID = R.ID
                  INNER JOIN CARACTERISTIQUESITE CA ON CA.SITEID = S.ID
                  WHERE 
                  CA.TYPEDATA = '3'
                  ORDER BY S.PAYSGEOID,  R.ID , D.ID , C.ID";
        return $rs = self::$db -> getall($query);
    }

	/**
	 * 
	 */
    public function internalProcess()
    {

    }

	/**
	 * Cree le XML d'export pour les ids selectionnes
	 * Pour les sites contenus dans $this -> ids, on va rechercher les donnees a exporter par $this -> stmtData
	 * @return string $myLogs : logs : normalement, le nombre de sites traites
	 */
    public function makeXml()
    {
    	$myLogs = '';
        $myXml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><root></root>");

        $allSites = $myXml -> addChild('Sites');

        $pays = $region = $commune = $decoupage = $site = 0;
        if (!is_array($this-> ids ) ) {
            echo "Aucun site à exporter";
            break;
        }
		$compteModifs = 0;
        foreach ($this -> ids as $R2)
        {
            $this -> stmtData -> bindParam(1, $R2['ID']);
            $this -> stmtData -> execute();
            $rs2 = $this -> stmtData -> fetchall(PDO::FETCH_ASSOC);

            foreach ($rs2 as $R)
            {
                if ($pays != $R["PAYSGEOID"])
                {
                    $myPays = $allSites -> addChild('Pays');
                    $myPays -> addAttribute('Id', $R["PAYSGEOID"]);
                    $myPays -> addAttribute('Nom', $R["PNOM"]);
                    $myPays -> addAttribute('Abreviation', $R["PABREVATION"]);
                }
                $pays = $R["PAYSGEOID"];

                if ($region != $R["RID"])
                {
                    $myRegion = $myPays -> addChild('Region');
                    $myRegion -> addAttribute('Id', $R["RID"]);
                    $myRegion -> addAttribute('Nom', $R["RNOM"]);
                    $myRegion -> addAttribute('NomEditionMaj', $R["RNOMEDITIONMAJ"]);

                }
                $region = $R["RID"];

                if ($decoupage != $R["DID"])
                {
                    $myDecoup = $myRegion -> addChild('Decoupage');
                    $myDecoup -> addAttribute('Id', $R["DID"]);
                    $myDecoup -> addAttribute('Code', $R["DCODE"]);
                    $myDecoup -> addAttribute('TypeDecoupage', $R["DTYPEDECOUPAGE"]);
                    $myDecoup -> addAttribute('Nom', $R["DNOM"]);
                    $myDecoup -> addAttribute('NomEditionMaj', $R["DNOMEDITIONMAJ"]);
                    $myDecoup -> addAttribute('Carte', $R["DCARTE"]);
                    if ($R["ANNEXE"])
                        $myDecoup -> addAttribute('Annexe', $R["ANNEXE"]);
                    
                }
                $decoupage = $R["DID"];

                if ($commune != $R["CID"])
                {
                    $myCommun = $myDecoup -> addChild('Commune');
                    $myCommun -> addAttribute('Id', $R["CID"]);
                    if ($R["CPAYSID"])
                        $myCommun -> addAttribute('PaysId', $R["CPAYSID"]);
                    if ($R["CREGIONID"])
                        $myCommun -> addAttribute('RegionId', $R["CREGIONID"]);
                    if ($R["CDECOUPAGEID"])
                        $myCommun -> addAttribute('DecoupageId', $R["CDECOUPAGEID"]);
                    if ($R["CINSEE"])
                        $myCommun -> addAttribute('Insee', $R["CINSEE"]);
                    if ($R["CNOM"])
                        $myCommun -> addAttribute('Nom', $R["CNOM"]);
                    if ($R["CNOMCLASSEMENT"])
                        $myCommun -> addAttribute('NomClassement', $R["CNOMCLASSEMENT"]);
                    if ($R["CNOMEDITIONMAJ"])
                        $myCommun -> addAttribute('NomEditionMaj', $R["CNOMEDITIONMAJ"]);
                    if ($R["CNOMEDITIONMIN"])
                        $myCommun -> addAttribute('NomEditionMin', $R["CNOMEDITIONMIN"]);
                    if ($R["CSITUATION"])
                        $myCommun -> addAttribute('Situation', $R["CSITUATION"]);
                    if ($R["CALTITUDE"])
                        $myCommun -> addAttribute('Altitude', $R["CALTITUDE"]);
                    if ($R["CNBHABITANTS"])
                        $myCommun -> addAttribute('NbHabitants', $R["CNBHABITANTS"]);
                    if ($R["CCARTECAMPINGCARAVANING"])
                        $myCommun -> addAttribute('CarteCampingCaravaning', $R["CCARTECAMPINGCARAVANING"]);
                    if ($R["PNOM"])
                        $myCommun -> addAttribute('PaysNom', $R["PNOM"]);
                    if ($R["RNOM"])
                        $myCommun -> addAttribute('RegionNom', $R["RNOM"]);
                    if ($R["DNOM"])
                        $myCommun -> addAttribute('DecoupageNom', $R["DNOM"]);
                    if ($R["CCODEPOSTAL"])
                        $myCommun -> addAttribute('CodePostal', $R["CCODEPOSTAL"]);
                    if ($R["CFPASSION"])
                        $myCommun -> addAttribute('FPassion', $R["CFPASSION"]);
                    if ($R["CPAVILLONBLEU"])
                        $myCommun -> addAttribute('PavillonBleu', $R["CPAVILLONBLEU"]);
                }
                $commune = $R["CID"];

                if ($site != $R['ID'])
                {
                    $mySite = $myCommun -> addChild('Site');
                    if ($R["ID"])
                        $mySite -> addAttribute('Id', $R["ID"]);
                    if ($R["TYPEDESITECODE"])
                        $mySite -> addAttribute('TypeDeSiteCode', $R["TYPEDESITECODE"]);
                    if ($R["NOM"])
                        $mySite -> addAttribute('Nom', $R["NOM"]);
                    if ($R["NOMCLASSEMENT"])
                        $mySite -> addAttribute('NomClassement', $R["NOMCLASSEMENT"]);
                    if ($R["NOMPOSTE"])
                        $mySite -> addAttribute('NomPoste', $R["NOMPOSTE"]);
                    if ($R["COMPLNOMPOSTE"])
                        $mySite -> addAttribute('ComplNomPoste', $R["COMPLNOMPOSTE"]);
                    if ($R["COMPLADRESSEPOSTE"])
                        $mySite -> addAttribute('ComplAdressePoste', $R["COMPLADRESSEPOSTE"]);
                    if ($R["ADRESSEPOSTE"])
                        $mySite -> addAttribute('AdressePoste', $R["ADRESSEPOSTE"]);
                    if ($R["COMPLVILLEPOSTE"])
                        $mySite -> addAttribute('ComplVillePoste', $R["COMPLVILLEPOSTE"]);
                    if ($R["CPPOSTE"])
                        $mySite -> addAttribute('CpPoste', $R["CPPOSTE"]);
                    if ($R["VILLEPOSTE"])
                        $mySite -> addAttribute('VillePoste', $R["VILLEPOSTE"]);
                    if ($R["PAYSPOSTE"])
                        $mySite -> addAttribute('PaysPoste', $R["PAYSPOSTE"]);
                    if ($R["NOMGEO"])
                        $mySite -> addAttribute('NomGeo', $R["NOMGEO"]);
                    if ($R["COMPLNOMGEO"])
                        $mySite -> addAttribute('ComplNomGeo', $R["COMPLNOMGEO"]);
                    if ($R["COMPLADRESSEGEO"])
                        $mySite -> addAttribute('ComplAdresseGeo', $R["COMPLADRESSEGEO"]);
                    if ($R["ADRESSEGEO"])
                        $mySite -> addAttribute('AdresseGeo', $R["ADRESSEGEO"]);
                    if ($R["COMPLVILLEGEO"])
                        $mySite -> addAttribute('ComplVilleGeo', $R["COMPLVILLEGEO"]);
                    if ($R["CPGEO"])
                        $mySite -> addAttribute('CpGeo', $R["CPGEO"]);
                    if ($R["VILLEGEO"])
                        $mySite -> addAttribute('VilleGeo', $R["VILLEGEO"]);
                    if ($R["PAYSGEO"])
                        $mySite -> addAttribute('PaysGeo', $R["PAYSGEO"]);
                    if ($R["RESPONSABLE"])
                        $mySite -> addAttribute('Responsable', $R["RESPONSABLE"]);
                    if ($R["GESTIONNAIRE"])
                        $mySite -> addAttribute('Gestionnaire', $R["GESTIONNAIRE"]);
                    if ($R["WEB"])
                        $mySite -> addAttribute('Web', $R["WEB"]);
                    if ($R["MAIL"])
                        $mySite -> addAttribute('Mail', $R["MAIL"]);
                    if ($R["ACCES"])
                        $mySite -> addAttribute('Acces', $R["ACCES"]);
                    if ($R["LANGUE"])
                        $mySite -> addAttribute('Langue', $R["LANGUE"]);
                    if ($R["FFCCID"])
                        $mySite -> addAttribute('FFCCID', $R["FFCCID"]);
                    if ($R["ANNEECREATION"])
                        $mySite -> addAttribute('AnneeCreation', $R["ANNEECREATION"]);
                    if ($R["RENSEIGNEMENTS"])
                        $mySite -> addAttribute('Renseignements', $this->xmlentities( $R["RENSEIGNEMENTS"]));
                    if ($R["LATITUDE"])
                        $mySite -> addAttribute('Latitude', $R["LATITUDE"]);
                    if ($R["LONGITUDE"])
                        $mySite -> addAttribute('Longitude', $R["LONGITUDE"]);
                    if ($R["PAYSGEOID"])
                        $mySite -> addAttribute('PaysGeoId', $R["PAYSGEOID"]);
                    if ($R["PAYSPOSTEID"])
                        $mySite -> addAttribute('PaysPosteId', $R["PAYSPOSTEID"]);
                    if ($R["COMMUNEGEOID"])
                        $mySite -> addAttribute('CommuneGeoId', $R["COMMUNEGEOID"]);
                    if ($R["PASSWORD"])
                        $mySite -> addAttribute('Password', $R["PASSWORD"]);
                    if ($R["FERMETURE"] && $R["FERMETURE"] != "0000-00-00 00:00:00")
                        $mySite -> addAttribute('Fermeture', $R["FERMETURE"]);
                    if ($R["FERMETURE"] && $R["FERMETURE"] != "0000-00-00 00:00:00"){
                        $fer = $mySite -> addChild('FERMETURE');
                        $fer-> addAttribute('Val',$this->xmlentities(  $R["FERMETURE"] ));
                    }
                }
                $site = $R['ID'];

                //INTERNE CARAC ...
                $myCarac = $mySite -> addChild($R["CARACTERISTIQUECODE"]);
                if ($R["VALEUR"])
                {
                    switch ($R["CARACTERISTIQUECODE"])
                    {
                        case 'STATHIVER' :
                        case 'ETANG' :
                        case 'BASELOISIR' :
                        case 'FORET' :
                        case 'MONTAGNE' :
                        case 'PARCNAT' :
                        case 'RIVIERE' :
                        case 'LAC' :
                            $myCarac -> addAttribute('Nom',$this->xmlentities( $R['VALEUR'] )) ;

                            break;

                        default :
                            $myCarac -> addAttribute('Val', $this->xmlentities(  $R['VALEUR']) );

                            break;
                    }
                }
                if ($R["ACCESDIRECT"])
                    $myCarac -> addAttribute('AD', "true");
                if ($R["REMARQUE"])
                    $myCarac -> addAttribute('Remarque', $this->xmlentities( $R['REMARQUE'] ));
                if ($R["SUPERFICIE"])
                    $myCarac -> addAttribute('Superficie', $this->xmlentities( $R['SUPERFICIE']));
                if ($R["DISTANCE"])
                    $myCarac -> addAttribute('Distance', $this->xmlentities( $R['DISTANCE']));
                if ($R["QTE1"])
                    $myCarac -> addAttribute('Quantite', $this->xmlentities( $R['QTE1']));
                if ($R["TARIF1"])
                    $myCarac -> addAttribute('Tarif', $this->xmlentities( $R['TARIF1']));
                if ($R["QTE2"])
                    $myCarac -> addAttribute('Quantite2', $this->xmlentities( $R['QTE2']));
                if ($R["TARIF2"])
                    $myCarac -> addAttribute('Tarif2', $this->xmlentities( $R['TARIF2']));
				if ($R["DUREE"])
                    $myCarac -> addAttribute('Duree', $this->xmlentities( $R['DUREE']));
				
				// Cas particulier, les champs vides pour ces caracteristiques sont en fait des "Non", et doivent etre exportes en "N"
				if(in_array($R["CARACTERISTIQUECODE"],array("FECARAVANE", "FEVOITURE", "FETENTE", "FECAMPCAR", "FEDOUCHE", "FEELEC", "FETCC", "FETGCC")))
				{				
					if ($R["VALTOURISME"]){
						$R['VALTOURISME'] = ($R['VALTOURISME'] == 'on' || $R['VALTOURISME'] == 'O')?'O':'N';
	                    $myCarac -> addAttribute('Tourisme', $this->xmlentities( $R['VALTOURISME']));
					}
	                if ($R["VALLOISIRS"]){
						$R['VALLOISIRS'] = ($R['VALLOISIRS'] == 'on' || $R['VALLOISIRS'] == 'O')?'O':'N';
	                    $myCarac -> addAttribute('Loisirs', $this->xmlentities( $R['VALLOISIRS']));
					}
	                if ($R["VALCAMPINGCAR"]){
						$R['VALCAMPINGCAR'] = ($R['VALCAMPINGCAR'] == 'on' || $R['VALCAMPINGCAR'] == 'O')?'O':'N';
	                    $myCarac -> addAttribute('CampingCar',$this->xmlentities(  $R['VALCAMPINGCAR']));
					}
	                if ($R["VALTENTE"]){
						$R['VALTENTE'] = ($R['VALTENTE'] == 'on' || $R['VALTENTE'] == 'O')?'O':'N';
	                    $myCarac -> addAttribute('Tente', $this->xmlentities( $R['VALTENTE']));		
					}
				} else {
				
	                if ($R["VALTOURISME"])
						$myCarac -> addAttribute('Tourisme', $this->xmlentities( $R['VALTOURISME']));
	                if ($R["VALLOISIRS"])
						$myCarac -> addAttribute('Loisirs', $this->xmlentities( $R['VALLOISIRS']));
	                if ($R["VALCAMPINGCAR"])
						$myCarac -> addAttribute('CampingCar',$this->xmlentities(  $R['VALCAMPINGCAR']));
	                if ($R["VALTENTE"])
						$myCarac -> addAttribute('Tente', $this->xmlentities( $R['VALTENTE']));
				}
            }
			$compteModifs++ ;
        }
        
     	//   file_put_contents(sys_get_temp_dir()."/".$this -> filename . $this -> addFilename, html_entity_decode($myXml -> asXML(), ENT_NOQUOTES, 'UTF-8'));
        file_put_contents(sys_get_temp_dir()."/".$this -> filename . $this -> addFilename, $myXml -> asXML());
        try{		
	        copy( sys_get_temp_dir()."/".$this -> filename . $this -> addFilename , $this -> rep . $this -> addRep . $this -> filename . $this -> addFilename);
	        copy( sys_get_temp_dir()."/".$this -> filename . $this -> addFilename , $this -> archiRep . $this -> addRep . $this -> filename . $this -> timestp  . $this -> addFilename);
	        
	        unlink (sys_get_temp_dir()."/".$this -> filename . $this -> addFilename);
	        	        
	        //.file_put_contents($this -> rep . $this -> addRep . $this -> filename . $this -> addFilename, html_entity_decode($myXml -> asXML(), ENT_NOQUOTES, 'UTF-8'));
	        //file_put_contents($this -> archiRep . $this -> addRep . $this -> filename . date("Ymd") . "_" . date("his") . $this -> addFilename, html_entity_decode($myXml -> asXML(), ENT_NOQUOTES, 'UTF-8'));
	        $this -> logExport();
		}catch(Exception $e){
			$myLogs .= ' ERREUR  : ' . $e -> getMessage() ;
		}
		$myLogs .= '(' . $compteModifs . ')';
		//$myLogs .= ' \r\n temp_file : ' . sys_get_temp_dir()."/".$this -> filename . $this -> addFilename ;
		//$myLogs .= ' \r\n copy 1 vers : ' . $this -> rep . $this -> addRep . $this -> filename . $this -> addFilename ;
		//$myLogs .= ' \r\n copy 2 vars : ' . $this -> archiRep . $this -> addRep . $this -> filename . $this -> timestp  . $this -> addFilename ;
		return $myLogs;
    }

    /**
     * Prepare la requete pour l'export GUIDE
     * toutes les caracteristiques de type 3 (Guide), avec infos pays, regions... associees
	 * place la query dans $this -> stmtData
     */
    protected function prepDataSite()
    {
        $query = "SELECT S.*,  P.ID as PID, P.NOM as PNOM, P.ABREVIATION as PABREVATION, R.ID as RID , R.NOM as RNOM , R.NOMEDITIONMAJ as RNOMEDITIONMAJ,
      D.ID as DID , D.CODE as DCODE , D.TYPEDECOUPAGE as DTYPEDECOUPAGE, D.NOM as DNOM , D.NOMEDITIONMAJ as DNOMEDITIONMAJ, D.CARTE as DCARTE, D.ANNEXE as ANNEXE,
      C.ID as CID, C.PAYSID as CPAYSID, C.REGIONID as CREGIONID, C.DECOUPAGEID as CDECOUPAGEID, C.INSEE as CINSEE, C.NOM as CNOM,
      C.NOMCLASSEMENT as CNOMCLASSEMENT, C.NOMEDITIONMAJ as CNOMEDITIONMAJ, C.NOMEDITIONMIN as CNOMEDITIONMIN,
      C.SITUATION as CSITUATION , C.ALTITUDE as CALTITUDE, C.NBHABITANTS as CNBHABITANTS, C.CARTECAMPINGCARAVANING as CCARTECAMPINGCARAVANING,
      C.CODEPOSTAL as CCODEPOSTAL, C.FPASSION as CFPASSION, C.PAVILLONBLEU as CPAVILLONBLEU, CA.*
                  FROM
                  SITE S INNER JOIN PAYS P  ON S.PAYSGEOID = P.ID
                  INNER JOIN COMMUNE C      ON S.COMMUNEGEOID = C.ID
                  INNER JOIN DECOUPAGEGEO D ON C.DECOUPAGEID = D.ID
                  INNER JOIN REGION R       ON D.REGIONID = R.ID
                  INNER JOIN CARACTERISTIQUESITE CA ON CA.SITEID = S.ID
                  INNER JOIN TYPEDESITECARACTERISTIQUE TSCA ON TSCA.TYPEDESITECODE = S.TYPEDESITECODE AND TSCA.CARACTERISTIQUECODE = CA.CARACTERISTIQUECODE
                  WHERE 
                  S.ID = ?
                  AND CA.TYPEDATA = '".$this->typedata ."'
                  
                  ORDER BY S.PAYSGEOID,  R.ID , D.ID , C.ID
                  ";
                 // echo($query);
        $this -> stmtData = self::$db -> prepare($query);
        // echo $query;
        //$rs = self::$db -> getall($query);
        // return $rs;
    }
}