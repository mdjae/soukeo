<?php 
/**
 * Classe de gestion d'images permettant de gérer le téléchargement local sur media-avahis.com
 * des images des grossistes ou des ecommerçants
 */
class BusinessPictureManager {

	protected $vendor_id;
	
	protected $path = "/var/www/media.avahis/media/catalog/product/";
	protected $local_url = "http://media.avahis.com/media/catalog/product/";
		
	private static $db;
	
	function __construct($vendor_id) {
		$this -> vendor_id = $vendor_id;
		self::$db = new BusinessSoukeoCSVModel ();
		
		//Préparation des update
		self::$db -> prepUpdateImageSfkCp();
		self::$db -> prepUpdateLocalImage();
		self::$db -> prepUpdateLocalSmallImage();
		self::$db -> prepUpdateLocalThumbnail();
	}
	
	
	/**
	 * Fonction permettant de sauvegarder les différentes images sous forme  de fichier local
	 * 3 types d'images Basique, Small, Thumbnail
	 * Il y a d'abord update des champs image du produit avahis en fonction du produit ecommercant ou grossiste
	 * Ensuite il y a update des champs local_image, local_small_image et local_thumbnail du produit Avahis
	 * @param array 	$prodEcom 		Le produit ecommerçant duquel on pourrait récupérer des url d'image manquants
	 * @param array 	$prodAv 		Le produit Avahis qu'on va update avec les nouvelles URL d'images locales
	 */
	public function savePictures($prodEcom = null, $prodAv)
	{
		//UPDATE des champs image, small_image, thumbnail de la fiche Avahis si jamais on a une fiche ecom ou grossiste
		//Plus fournie
		if($prodEcom !== null ){
			
			$image 			= $prodEcom['IMAGE_PRODUCT'];
			$small_image 	= $prodEcom['IMAGE_PRODUCT_2'] ? $prodEcom['IMAGE_PRODUCT_2'] : $prodEcom['IMAGE_PRODUCT'];
			$thumbnail 		= $prodEcom['IMAGE_PRODUCT_3'] ? $prodEcom['IMAGE_PRODUCT_3'] : $prodEcom['IMAGE_PRODUCT'];
		
			self::$db -> updateImageSfkCp($image, $small_image, $thumbnail, $prodAv['id_produit']);	
		}	
		

		//Création dossier
		mkdir( $this->path.$prodAv['categories'], 0777);
		
		//Traitement nom produit
		$nom = strtoupper(str_replace(array(",", " ", ".", '"', "'"), "", stripAccents($prodAv['name'])));
		
		//Construction
		$url_image 			= $prodAv['image'];
		$url_small_image 	= $prodAv['small_image'];
		$url_thumbnail 		= $prodAv['thumbnail'];

		
		$tmp = explode("/", $url);
		$tmp = end($tmp);
		
		$local_image 		= $prodAv['categories']."/".substr($nom, 0, 2).'_b_'.$prodAv['categories'].'_'.$prodAv['id_produit'].'_'.substr($nom, 0, 5).'.jpg';
		$local_image_small 	= $prodAv['categories']."/".substr($nom, 0, 2).'_s_'.$prodAv['categories'].'_'.$prodAv['id_produit'].'_'.substr($nom, 0, 5).'.jpg';
		$local_thumbnail 	= $prodAv['categories']."/".substr($nom, 0, 2).'_t_'.$prodAv['categories'].'_'.$prodAv['id_produit'].'_'.substr($nom, 0, 5).'.jpg';

		echo $this -> downloadImg ($url_image, $prodAv, $local_image, "B") . PHP_EOL;
		echo $this -> downloadImg ($url_small_image, $prodAv, $local_image_small, "S") . PHP_EOL;
		echo $this -> downloadImg ($url_thumbnail, $prodAv, $local_thumbnail, "T") . PHP_EOL;
		
		echo PHP_EOL;
	}
	

	/**
	 * Fonction spécifique au download d'image des produits Avahis
	 * Renvoie un message récapitulatif des opérations ou erreurs.
	 * Enregistre les url locales des images sauvegardées dans la base de donnée pour chaque fiche produit
	 * @param  string 	$url 			L'url contenant l'image
	 * @param  array 	$prod 			Le produit sous forme de tableau associatif
	 * @param  string 	$local_name 	Nom final du fichier local avec chemin complet
	 * @param  string 	$size 			Représente le type d'image B(asique), S(mall), T(humbnail)
	 * @return string 	$message 		Log de retour d'opération
	 */
	protected function downloadImg($url, $prod, $local_name, $size)
	{
		// remplacement des espaces par %20	
		$url = str_replace(" ", "%20", $url);
		//TELECHARFGEMENT
		if($url != "" && $url != "NULL" && $url !== null ){
			
			//Récup info erreur pour voir si page existe etc...
			$headers = $this -> getHeaders($url);
			
			//download_content_lenght est en bytes
			if ($headers['http_code'] === 200 and $headers['download_content_length'] < 1024*1024) {
				if ($this -> download($url, $this->path.$local_name)){
					$state =  "COMPLETE ! "; 
			  	}
			}
			else {
				$state =  "#HTTP-ERROR {$headers['http_code'] }  pour : {$headers['url']}";
			}				
		}
		else{
			$state =  "AUCUNE URL FOURNIE ! ";
		}

		//GESTION FICHIER
		if ( filesize( $this->path . $local_name ) == 0 ) {
			
			unlink($this->path . $local_name);
			$message = $prod['id_produit'] . " - ".$prod['name']." - $size $state";
			
		}else{
			//Sauvegarde en base de donnée
			switch ($size) {
				case 'B':
					self::$db -> updateLocalImage($prod['id_produit'], $this -> local_url . $local_name);
					break;
				case 'S' :
					self::$db -> updateLocalSmallImage($prod['id_produit'], $this -> local_url . $local_name);
					break;		
				case 'T' :
					self::$db -> updateLocalThumbnail($prod['id_produit'], $this -> local_url . $local_name);
					break;

			}	
			//Message récapitulatif
			$message = $prod['id_produit'] . " - ".$prod['name']." - $size COMPLETE - URL_IMAGE : ".$this -> local_url . $local_name;
		}
		
		return $message;		
	}
	
	
	/**
	 * Fonction basique de download permettant de récupérer un fichier avec cUrl
	 * @param 	string 	$url 	L'url contenant le fichier
	 * @param 	string 	$path 	Le chemin du fichier local + son nom et extension 
	 * @return 	boolean true if filesize final > 0
	 */
	protected function download($url, $path)
	{
		// Ouverture fichier à écrire
	  	$fp = fopen ($path, 'w+');
	  	// start curl
	  	$ch = curl_init();
	  	curl_setopt( $ch, CURLOPT_URL, $url );
	  	// On set return transfer to false
	  	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
	  	curl_setopt( $ch, CURLOPT_BINARYTRANSFER, true );
	  	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	  	
	  	// Augmente du temps de timeOut
	  	curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
	  	
	  	// Ecriture des données sur fichier local
	  	curl_setopt( $ch, CURLOPT_FILE, $fp );
	  	
		//Exec cUrl
	  	curl_exec( $ch );
	  	// close curl
	  	curl_close( $ch );
	  	// close local file
	  	fclose( $fp );
	
	  	if (filesize($path) > 0) 
	  		return true;
	}
	
	/**
	 * Check les headers de l'adresse demander pour ressortir les codes d'erreurs
	 * et vérifier si le telechargement sera possible ou non
	 * @param  string 	$url 		l'url dont on veux les headers
	 * @return array 	$headers 	le header de la page
	 */
	protected function getHeaders($url)
	{
	  $ch = curl_init($url);
	  curl_setopt( $ch, CURLOPT_NOBODY, true );
	  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
	  curl_setopt( $ch, CURLOPT_HEADER, false );
	  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	  curl_setopt( $ch, CURLOPT_MAXREDIRS, 3 );
	  curl_exec( $ch );
	  $headers = curl_getinfo( $ch );
	  curl_close( $ch );
	
	  return $headers;
	}


}

 ?>