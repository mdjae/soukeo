<?php 
/**
 * Objet utilisateur compatible ORM
 *
 */
class BusinessAppUser extends BusinessEntity
{

	protected $usercode = "";
	protected $user_id;
	protected $login;
	protected $firstname;
	protected $lastname;
	protected $email;
	protected static $db;
    

	public function __construct($data = array())
	{
        self::$db = new BusinessSoukeoModel();  

        if (!empty($data)) {
            $this->hydrate($data);
        }   
	}
    
    public function setUser_id($user_id)
    {
        $this -> user_id = $user_id;
    }
    
    public function setUsercode($usercode)
    {
        $this -> usercode = $usercode;
    }
    
    public function setLogin($login)
    {
        $this -> login = $login;
    }
    
    public function setFirstname($firstname)
    {
        $this -> firstname = $firstname;
    }
    
    public function setLastname($lastname)
    {
        $this -> lastname = $lastname;
    }
    
    public function setEmail($email)
    {
        $this -> email = $email;
    }

	public function getUser_id()
	{
		return $this->user_id;
	}

	public function getUser_code()
	{
		return $this->usercode;
	}

	public function getLogin()
	{
		return $this->login;
	}

	public function getFirstname()
	{
		return $this->firstname;
	}

	public function getLastname()
	{
		return $this->lastname;
	}

	public function getName()
	{
		return $this->firstname . ' ' . $this->lastname;
	}

	public function getEmail()
	{
		return $this->email;
	}

    public function checkUnseenTickets(){
        
        $query = "SELECT ticket_id FROM skf_tickets_users WHERE user_id = ? AND seen = 0";
        
        $countTickets = self::$db -> getAllPrep($query, array($this->getUserId()));
        
        return count($countTickets);
    }
    
    public function getAllTickets($unseen=false)
    {
        $ticketManager = new ManagerTicket();
        
        $query = "SELECT ticket_id FROM skf_tickets_users WHERE user_id = ? ";
        
        if($unseen)
            $query .= " AND seen = 0";
        
        $ticketsIdList = self::$db -> getAllPrep($query, array($this->getUserId()));
        
        $ticketList = array();
        
        foreach ($ticketsIdList as $id) {
            $ticketList[] = $ticketManager -> getById(array("ticket_id" => $id['ticket_id']));
        }
        
        return $ticketList;
    }
    
    public function save()
    {
        
    }
}
?>