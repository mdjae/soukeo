<?php 
/**
 * Objet utilisateur
 * @author nsa
 *
 */
class BusinessUser
{
	/*
	 * array Groupe des utilisateur
	 */
	protected $_Agroup; 
	/*
	 * Code de l'utilisateur
	 */
	protected $_usercode = "";
	
	/*
	 * Liste des commandes
	 */
	protected $_orders = array();
	
	/*
	 * Liste des clients
	 */
	protected $_clients = array();
	
	/*
	 * Liste des utilisateurs de la m�me �quipe
	 */
	protected $_team_users = array();
	
	/*
	 * Propri�t�s de l'utilisateurs
	 */
	protected $_user_id;
	protected $_login;
	protected $_firstname;
	protected $_lastname;
	protected $_email;
	protected $_auth_source_id;
	
	/*
	 *  Liste des utilisateurs dont on est le boss 
	 */
	protected $_boss_team_users = array();
	
	/*
	 * Liste des patrons
	 */
	protected $_boss_users = array();
	
	protected $_admin;
	
	protected static $db;
	/**
	 * Contructeur
	 * @param string $usercode ou le login si $login = true
	 * @param boolean $login indique le code pass� correspond au login
	 * @return unknown_type
	 */
	public function __construct($usercode, $login = false)
	{
		if ($login) $this->_login = $usercode;
		else $this->_usercode = $usercode;
		$this->_login = $usercode;
		self::$db = new sdb();
		// Récupération des infos de l'utilisateur
		$query = "SELECT user_id, usercode,	login, firstname,lastname, email, auth_source_id FROM c_user".
				 " WHERE login = ? ";
        
        $res = self::$db -> getRowPrep($query, array($this->_login));
 

		if(!$res){ 
		$this->_admin = false;
        }
        else $this->_admin =  true;
		  
		
		$this->_usercode = stripslashes($res['usercode']);
		$this->_login = stripslashes($res['login']);
		
		$this->_user_id = $res['user_id'];
		$this->_firstname = stripslashes($res['firstname']);
		$this->_lastname = stripslashes($res['lastname']);
		$this->_email = stripslashes($res['email']);
		$this->_auth_source_id = $res['auth_source_id'];
		
	}
    

	
    public static function getClassUser($usercode){
   //     self::$db = new sdb();
   //     $query = "SELECT user_id, usercode, login, firstname,lastname, email, auth_source_id FROM c_user".
   //              " WHERE login = ?";
   //    
    //    $res = self::$db -> getRowPrep($query, array($usercode));
        
  //      if(!$res){ 
    //        return new BusinessGest($usercode, true);
    //    }
     //  else  
        return new BusinessUser($usercode, true);
        
        
    }
	
	protected function setSessionsUser($password){
		$_SESSION['smartlogin']  = $this -> getLogin();
		$_SESSION['smartfirst']  = $this -> getFirstname();
		$_SESSION['smartlast']	 = $this -> getLastname();
		$_SESSION['smartuserid'] = $this -> getUserId();
		$_SESSION['smarttri'] 	 = $this -> getUsercode();
		$_SESSION['usercode'] 	 = $this -> getUsercode();
		$_SESSION['smartlog']  	 = true;
		$_SESSION['smartlang'] 	 = $lang;
		$_SESSION['smartmenu'] 	 = true;
		$_SESSION['smartpwd'] 	 = $password;
		$_SESSION['isauth']		 = true;
		
		$_SESSION['smartgroup']  = Array();
		
		foreach ($this->getGroup() as $grp) {
			$_SESSION['smartgroup'][] = $grp['group_id'];
		}
	}
	
	/**
	 * Retourne le user_id
	 * @return unknown_type
	 */
	public function getUserId()
	{
		return $this->_user_id;
	}
	
	public function getAdmin(){
		return $this->_admin;
	}
	/**
	 * Retourne le usercode
	 * @return unknown_type
	 */
	public function getUsercode()
	{
		return $this->_usercode;
	}
	
	/**
	 * Retourne le login
	 * @return unknown_type 
	 */
	public function getLogin()
	{
		return $this->_login;
	}
	
	/**
	 * Retourne le pr�nom
	 * @return unknown_type
	 */
	public function getFirstname()
	{
		return $this->_firstname;
	}
	
	/**
	 * Retourne le nom
	 * @return unknown_type
	 */
	public function getLastname()
	{
		return $this->_lastname;
	}
	
	/**
	 * Retourne le nom complet de l'utilisateur
	 * @return unknown_type
	 */
	public function getName()
	{
		return $this->_firstname . ' ' . $this->_lastname;
	}
	
	/**
	 * Retourne l'email de l'utilisateur
	 * @return unknown_type
	 */
	public function getEmail()
	{
		return $this->_email;
	}
	
	
	/**
	 * 
	 * @param string $password
	 * @return boolean
	 */
	public function doAuth($password)
	{
		if( SystemAuth::Auth($this->_usercode, $password) ){
			$this->setSessionsUser($password);
			return true;
		}else 
			return false;
	}
	
	public function getGroup(){
       	// R�cup�ration des groupes de l'utilisateur
		$query = "SELECT group_id FROM c_group_users WHERE user_id = ? ";
        
        
		$this->_Agroup = self::$db -> getAllPrep($query, array($this->getUserId()));
        
		return $this->_Agroup;
	}
    
    public function checkUnseenTickets(){
        
        $query = "SELECT ticket_id FROM skf_tickets_users WHERE user_id = ? AND seen = 0 ";
        
        $countTickets = self::$db -> getAllPrep($query, array($this->getUserId()));
        
        return count($countTickets);
    }
    
    public function getAllTickets($unseen=false)
    {
        $ticketManager = new ManagerTicket();
        
        $query = "SELECT ticket_id FROM skf_tickets_users WHERE user_id = ? ";
        
        if($unseen)
            $query .= " AND seen = 0";
        
        $ticketsIdList = self::$db -> getAllPrep($query, array($this->getUserId()));
        
        $ticketList = array();
        
        foreach ($ticketsIdList as $id) {
            $ticketList[] = $ticketManager -> getById(array("ticket_id" => $id['ticket_id']));
        }
        
        return $ticketList;
    }
    
    public function getAllRelanceTickets($unseen=false)
    {
        $relanceManager = new ManagerRelance();
        
        $query = "SELECT distinct r.relance_id FROM skf_relances r  
                  INNER JOIN skf_tickets_users tu ON r.ticket_id = tu.ticket_id
                  INNER JOIN skf_tickets t ON r.ticket_id = t.ticket_id  
                  WHERE (tu.user_id = ? OR t.assigned_user_id = ? ) 
                  AND r.date_termine IS NULL
                  ORDER BY r.date_relance ASC ";
        
        if($unseen)
            $query .= " AND tu.seen = 0";
        
        $relancesIdList = self::$db -> getAllPrep($query, array($this->getUserId(), $this->getUserId()));
        
        $relanceList = array();
        
        foreach ($relancesIdList as $id) {
            $relanceList[] = $relanceManager -> getById(array("relance_id" => $id['relance_id']));
        }
        
        return $relanceList;
    }
    
    public function getAllRelanceTicketsFilter($filters)
    {
        $relanceManager = new ManagerRelance();
        
        $query = "SELECT distinct r.relance_id FROM skf_relances r  
                  INNER JOIN skf_tickets_users tu ON r.ticket_id = tu.ticket_id
                  INNER JOIN skf_tickets t ON r.ticket_id = t.ticket_id  
                  WHERE (tu.user_id = ? OR t.assigned_user_id = ? ) 
                  AND r.date_termine IS NULL 
                  ";
        
        if($unseen)
            $query .= " AND seen = 0 ";
        
        if($filters['client_name']){
            $query .= " AND t.client_name LIKE '%".htmlentities($filters['client_name'])."%' ";
        }
        
        if($filters['relance_statut']){
            
            $now  = new DateTime();
            $now2 = new DateTime();
            $now2 -> add(new DateInterval('P3D'));
            
            switch ($filters['relance_statut']) {
                //DEPASSE
                case 1:
                    $query .= " AND r.date_relance < '". $now->format("Y-m-d H:i:s") ."' ";
                break;
                //BIENTOT
                case 2:
                    $query .= " AND r.date_relance > '". $now->format("Y-m-d H:i:s") ."' AND r.date_relance < '". $now2->format("Y-m-d H:i:s")."' ";
                break;
                //Lointain
                case 3:
                    $query .= " AND r.date_relance > '". $now2->format("Y-m-d H:i:s") ."' ";
                break;
                //TOUS
                case 0:
                    $query .= "";
                break;
                default:
                break;
            }
        }
        
        $query.= " ORDER BY r.date_relance ASC ";
        
        $relancesIdList = self::$db -> getAllPrep($query, array($this->getUserId(), $this->getUserId()));
        
        $relanceList = array();
        
        foreach ($relancesIdList as $id) {
            $relanceList[] = $relanceManager -> getById(array("relance_id" => $id['relance_id']));
        }
        
        return $relanceList;
    }
}
?>