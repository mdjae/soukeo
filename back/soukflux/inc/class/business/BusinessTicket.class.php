<?php

class BusinessTicket extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $ticket_id;
    protected $ticket_code;
	protected $ticket_creator_id;
    protected $ticket_entity_id;
	protected $ticket_state_id; 
	protected $ticket_priority_id;
    protected $ticket_type_id;
    protected $ticket_title;
	protected $ticket_text;
    protected $ticket_visibility;
	protected $client_type;
    protected $client_id;
    protected $client_name;
    protected $assigned_user_id;
    protected $ticket_token;
    protected $ticket_question_type_id;
    protected $date_relance;
    protected $date_updated;
    protected $date_created;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setTicket_id($ticket_id){
		$this -> ticket_id = $ticket_id ;
	}

    public function setTicket_code($ticket_code){
        $this -> ticket_code = $ticket_code ;
    }
    
	public function setTicket_creator_id($ticket_creator_id)
	{
		$this -> ticket_creator_id = $ticket_creator_id;
	}
    
    public function setTicket_entity_id($ticket_entity_id)
    {
        $this -> ticket_entity_id = $ticket_entity_id;
    }	
    
	public function setTicket_state_id($ticket_state_id)
	{
		$this -> ticket_state_id = $ticket_state_id;
	}
	
	public function setTicket_priority_id($ticket_priority_id)
	{
		$this -> ticket_priority_id = $ticket_priority_id;
	}

    public function setTicket_type_id($ticket_type_id)
    {
        $this -> ticket_type_id = $ticket_type_id;
    }
    
    public function setTicket_title($ticket_title)
    {
        $this -> ticket_title = $ticket_title;
    }
    	
	public function setTicket_text($ticket_text)
	{
		$this -> ticket_text = $ticket_text;
	}
        
    public function setTicket_visibility($ticket_visibility)
    {
        $this -> ticket_visibility = $ticket_visibility;
    }
    	
	public function setClient_type($client_type)
	{
		$this -> client_type = $client_type;
	}

	public function setClient_id($client_id){
		$this -> client_id = $client_id;
	}

    public function setClient_name($client_name){
        $this -> client_name = $client_name;
    }
    
    public function setAssigned_user_id($assigned_user_id)
    {
        $this -> assigned_user_id = $assigned_user_id;
    }
     
    public function setTicket_token($ticket_token)
    {
        $this -> ticket_token = $ticket_token;
    }
     
    public function setTicket_question_type_id($ticket_question_type_id)
    {
        $this -> ticket_question_type_id = $ticket_question_type_id;
    }
           
    public function setDate_relance($date_relance){
        $this -> date_relance = $date_relance;
    }
    
    public function setDate_updated($date_updated){
        $this -> date_updated = $date_updated;
    }    
     
    public function setDate_created($date_created){
        $this -> date_created = $date_created;
    }        

    public function getTicket_id(){
        return $this -> ticket_id;
    }

    public function getTicket_code(){
        return $this -> ticket_code;
    }
    
    public function getTicket_creator_id()
    {
        return $this -> ticket_creator_id;
    }
    
    public function getTicket_creator_user()
    {
        $manager = new ManagerAppUser();
        
        return $manager -> getById(array("user_id" => $this -> ticket_creator_id));
    }

    public function getTicket_entity_id()
    {
        return $this -> ticket_entity_id;
    }   
        
    public function getTicket_state_id()
    {
        return $this -> ticket_state_id;
    }
    
    public function getTicket_priority_id()
    {
        return $this -> ticket_priority_id;
    }

    public function getTicket_type_id()
    {
        return $this -> ticket_type_id;
    }
        
    public function getTicket_text()
    {
        return $this -> ticket_text;
    }

    public function getTicket_visibility()
    {
        return $this -> ticket_visibility;
    }
    
    public function getTicket_title()
    {
        return $this -> ticket_title;
    }
    
    public function getClient_type()
    {
        return $this -> client_type; 
    }

    public function getClient_id(){
        return $this -> client_id ;
    }

    public function getClient_name(){
        return $this -> client_name;
    }
    
    public function getAssigned_user_id()
    {
        return $this -> assigned_user_id;
    }
    
    public function getTicket_token()
    {
        return $this -> ticket_token;
    }

    public function getTicket_question_type_id()
    {
        return $this -> ticket_question_type_id;
    }
        
    public function getAssigned_user()
    {
        $manager = new ManagerAppUser();
        
        return $manager -> getById(array("user_id" => $this -> assigned_user_id));
    }

    public function getTicket_type()
    {
        $manager = new ManagerTicketType();
        
        if($type = $manager -> getById(array("type_id" => $this -> ticket_type_id))){
            return $type -> getLabel();
        }
        else {
            return "Type inconnu";
        }
    }
    
    /**
     * Fonction permettant de retourner le lien <a> HTML du client menant vers sa fiche
     * client
     */
    public function getClient()
    {
        $entityManager = new ManagerEntityCrm();
        $entityTypeManager = new ManagerEntityType();
        $customerManager = new ManagerCustomer();
        $vendorManager = new ManagerVendor();
        
        if($this -> getTicket_entity_id()){
            $entity = $entityManager -> getById(array("entity_id" => $this -> getTicket_entity_id()));
            $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
            
            if($type -> getLabel() == "customer"){
                $customer = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                $ticketClient = "<a href='clients/client?type=customer&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($customer->getName())."</a> (A)";
            }elseif($type -> getLabel() == "vendor"){
                $vendor = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                $ticketClient = "<a href='clients/client?type=vendor&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($vendor->getVendor_nom())."</a> (V)"; 
            }
            
        }else{
            $ticketClient = "<em> Global </em>";
        } 
        
        return $ticketClient;
    }
            
    public function getDate_relance(){
        return $this -> date_relance ;
    }
    
    public function getDate_updated(){
        return $this -> date_updated;
    }    
     
    public function getDate_created(){
        return $this -> date_created;
    }
    
    public function getTicket_state_label()
    {
        $manager = new ManagerTicketState();
        
        return $manager -> getById(array("state_id" => $this -> ticket_state_id))-> getLabel();
    }

    public function getTicket_priority_label()
    {
        $manager = new ManagerTicketPriority();
        
        return $manager -> getById(array("priority_id" => $this -> ticket_priority_id))-> getLabel();
    }
          	
    public function getHistory()
    {
        $manager = new ManagerTicketHistory();    
        
        return $manager -> getAll(array("ticket_id" => $this -> ticket_id));
    }        
    
    
    /**
     * Permet de récupérer tous les observateurs du ticket sous forme d'array
     * contenant les objets AppUsers correspondants
     */
    public function getObservateurs()
    {
        $manager = new ManagerTicketUser();    
        $managerAppuser = new ManagerAppUser();
        $ticketUserList = $manager -> getAll(array("ticket_id" => $this -> ticket_id));
        
        $userList = array();
        
        foreach ($ticketUserList as $ticketUser) {
            $userList[] = $managerAppuser -> getById(array("user_id" => $ticketUser -> getUser_id() ));
        }
        
        return $userList;
    }
    
    /**
     * Permet d'envoyer l'email aux différentes personnes concernées par le ticket
     * lors de la mise à jour ou la création de celui ci.
     */
    public function sendMail($maj = "")
    {
        global $applicationMode;
        
        $entityManager = new ManagerEntityCrm();
        $entityTypeManager = new ManagerEntityType();
        $customerManager = new ManagerCustomer();
        $vendorManager = new ManagerVendor();
        
        $transport    = Swift_MailTransport::newInstance();
        $message      = Swift_Message::newInstance();
        
        $message -> setFrom(array("noreply@avahis.com" => "Soukflux"));
        
        if($applicationMode == "DEV" ) {
            $message -> setFrom(array('philippe.lattention@silicon-village.fr' => "Soukflux"));
 
        }
        else{
        }
        
        $userList = $this -> getObservateurs();
        
        $user_mails = array();
        
        foreach ($userList as $user) {
            $user_mails[] = $user -> getEmail();
        }
        
        if($user = $this -> getAssigned_user()){
            if(!in_array($user->getEmail(), $user_mails)){
                $user_mails[] = $user->getEmail();
            }    
        }
        
        
        if($creator = $this -> getTicket_creator_user()){
            $creator_name = $creator -> getName();
        }else{
            $creator_name = "Client";
        }
        
        $message -> setTo($user_mails);
        
        $message -> setSubject("Ticket ".$this -> ticket_code. " : ".$this -> ticket_title);
        
        if($this -> ticket_entity_id){
            $entity = $entityManager -> getById(array("entity_id" => $this -> getTicket_entity_id()));
            $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
            
            if($type -> getLabel() == "customer"){
                $customer = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                $ticketClient = "<a href='/app.php/clients/client?type=customer&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($customer->getName())."</a> (A)";
            }elseif($type -> getLabel() == "vendor"){
                $vendor = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                $ticketClient = "<a href='/app.php/clients/client?type=vendor&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($vendor->getVendor_nom())."</a> (V)"; 
            }
            
        }else{
            $ticketClient = "<em> Global </em>";
        }
        
        $text_debut = "";
        
        if($maj){
            $text_debut = $maj;
        }else{
            $text_debut = "<p>Le ticket ".$this -> ticket_code." a été créé par ".$creator -> getName()."</p>";
        }
        
        $message -> setBody(" $text_debut  <hr/>
                                <a href = 'http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$this -> ticket_id."' ><h2>Ticket ".$this -> ticket_code. " : ".$this -> ticket_title."</h2></a>
                                         <ul>
                                            <li>Auteur : ".ucfirst($creator_name)."</li>
                                            <li>Statut : ".ucfirst($this->getTicket_state_label())."</li>
                                            <li>Priorité : ".ucfirst($this->getTicket_priority_label())."</li>
                                            <li>Client : ".$ticketClient."</li>
                                         </ul>".$this -> ticket_text ,'text/html');
                                         
        $mailer = Swift_Mailer::newInstance($transport);

        if($mailer -> send($message)) 
            return true;
        else
            return false; 
    }

    
    /**
     * Permet d'envoyer le mail au client si le message mis à jour dans le ticket est
     * en visibilité publique
     */
    public function sendMailClient($user = "")
    {
        global $applicationMode;
        
        $entityManager = new ManagerEntityCrm();
        $entityTypeManager = new ManagerEntityType();
        $customerManager = new ManagerCustomer();
        $vendorManager = new ManagerVendor();
        
        $transport    = Swift_MailTransport::newInstance();
        $message      = Swift_Message::newInstance();
        
        $message -> setFrom(array("noreply@avahis.com" => "Soukflux"));
        
        if($applicationMode == "DEV" ) {
            $message -> setFrom(array('philippe.lattention@silicon-village.fr' => "Soukflux"));
            $addr_avahis = "192.168.1.200/magento";
        }
        elseif($applicationMode == "REC" ) {
            $addr_avahis = "www.rec.avahis.com";
        }
        else{
            $addr_avahis = "www.avahis.com";
        }
        
        $add_text = "";
        if($this -> ticket_entity_id){
            $entity = $entityManager -> getById(array("entity_id" => $this -> getTicket_entity_id()));
            $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
            
            if($type -> getLabel() == "customer"){
                $client = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                if($client->getCustomer_id()){
                    $clientType = "a_inscr";
                    $lastname = $client -> getLastname();
                    $add_text = "Connectez vous avec votre compte Avahis et vous verrez toutes vos demandes.";
                    $link = "http://".$addr_avahis."/contactform/index/show/id/".$this->ticket_id;
                }else{
                    $clientType = "a_guest";
                    $link = "http://".$addr_avahis."/contactform/guest/show/id/".$this->ticket_token;    
                }
            }elseif($type -> getLabel() == "vendor"){
                $client = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                if($client->getVendor_id()){
                    $clientType = "v_inscr";
                    $civ = $client->getCivilite();
                    $lastname = $client -> getVendor_attn();
                    $link = "http://".$addr_avahis."/contactform/vendor/show/id/".$this->ticket_id;
                }else{
                    $clientType = "v_guest";
                    
                    $link = "http://".$addr_avahis."/contactform/guest/show/id/".$this->ticket_token;    
                }
            }
        }else{
            exit;
        }
        
        
        $message -> setTo($client->getEmail());
        
        $message -> setSubject("[Service client Avahis] Re : ".$this -> ticket_title);
        
        $message -> setBody($this->getMailMessage($clientType, $link, $civ, $lastname, $user) ,'text/html');

        /*$message -> setBody("<h2>Votre demande : ".$this -> ticket_title." a été mise à jour</h2>
                              Pour visualiser la réponse rendez vous à cette adresse :     $ticketClient <br/>
                              <br/>
                              Ceci est un mail automatique merci de ne pas y répondre directement." ,'text/html'); */                               
                                         
        $mailer = Swift_Mailer::newInstance($transport);

        if($mailer -> send($message)) 
            return true;
        else
            return false; 
    }

    /**
     * Récupère le corps de mail en fonction du type de client
     */
    public function getMailMessage($clientType, $link, $civ="", $lastname = "", $user = "")
    {
        $css_link = 'title="Inscription" style="border: 1px solid rgb(245, 179, 68); margin: 0px; padding: 3px 14px; 
                                                          cursor: pointer; border-top-left-radius: 4px; border-top-right-radius: 4px; 
                                                          border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; 
                                                          font-size: 0.875em; font-weight: bold; line-height: 20px; 
                                                          color: rgb(24, 28, 28); 
                                                          background-image: linear-gradient(rgb(255, 218, 20) 0%, rgb(255, 188, 12) 100%); 
                                                          -webkit-box-shadow: rgba(0, 0, 0, 0.247059) 0px 1px 1px 0px, rgba(255, 255, 255, 0.498039) 0px 1px 10px 0px inset; 
                                                          box-shadow: rgba(0, 0, 0, 0.247059) 0px 1px 1px 0px, rgba(255, 255, 255, 0.498039) 0px 1px 10px 0px inset; 
                                                          transition: all 0.3s ease; -webkit-transition: all 0.3s ease;text-decoration : none;" 
                                                          class="btn btn-primary" ';
        switch ($clientType) {
            
            case 'a_guest':
                $message = "Bienvenue chez Avahis.com. <br/>
                            ".ucfirst(strtolower($user))." votre conseiller, a répondu à votre demande N° ".$this->ticket_code." <br/><br/>
                            Pour consulter votre réponse et suivre vos demandes, n'hésitez pas à vous inscrire (ou créer votre compte)<br>
                            
                            <a href='http://www.avahis.com/customer/account/create/' $css_link>Créer mon compte</a><br/><br/>, connectez-vous à votre compte Avahis.com. <br/> 
                            Vous pouyvez également consulter votre réponse en <a href='$link'>cliquant ici</a><br/><br/>
                            Notre équipe vous remercie de votre cisite.<br/>
                            A bientôt sur Avahis.com";
            break;

            case 'v_guest':
               $message = "Bienvenue chez Avahis.com. <br/>
                            ".ucfirst(strtolower($user))." votre conseiller, a répondu à votre demande N° ".$this->ticket_code." <br/><br/>
                            Pas encore inscrit ? Allez-y c'est gratuit et sans engagement !<br><br/>
                            <a href='http://www.avahis.com/umicrosite/vendor/register/' $css_link>Créer mon compte</a><br/><br/>, connectez-vous à votre compte Avahis.com. <br/> 
                            Vous pouyvez également consulter votre réponse en <a href='$link'>cliquant ici</a><br/><br/>
                            'Ensemble, valorisons le commerce Réunionnais !' ";
            break;
                
            case 'a_inscr':
                $message = "Bonjour Mr/Mme ".$lastname.", <br/>
                            ".ucfirst(strtolower($user))." votre conseiller, a répondu à votre demande N° ".$this->ticket_code." <br/><br/>
                            Pour consulter votre réponse, connectez-vous à votre compte Avahis.com. <br/> <br/>
                            <a href='$link' $css_link>Me connecter</a> <br/> <br/>
                            L'équipe d'Avahis.com est heureuse de vous compter parmi ses précieux clients !<br/><br/>
                            Bonne journée,<br/>
                            Le service client Avahis.com";
            break;
            
            case 'v_inscr':
                $message = "Bonjour ".ucfirst(strtolower($civ))." ".ucfirst(strtolower($lastname)).", <br/>
                            ".ucfirst(strtolower($user))." votre conseiller, a répondu à votre demande N° ".$this->ticket_code." <br/><br/>
                            Pour consulter votre réponse, connectez-vous à votre compte Avahis.com. <br/> <br/>
                            <a href='$link' $css_link>Me connecter</a> <br/> <br/>
                            L'équipe d'Avahis.com est heureuse de vous compter parmi ses partenaires !<br/><br/>
                            'Ensemble, valorisons le commerce Réunionnais !' ";
            break;       
                                             
            default:
            break;
        }
        
        return $message;
    }
    
    /**
     * Permet d'envoyer le mail au client si le ticket créé est
     * en visibilité publique
     */
    public function sendMailClientCreation()
    {
        global $applicationMode;
        
        $entityManager = new ManagerEntityCrm();
        $entityTypeManager = new ManagerEntityType();
        $customerManager = new ManagerCustomer();
        $vendorManager = new ManagerVendor();
        
        $transport    = Swift_MailTransport::newInstance();
        $message      = Swift_Message::newInstance();
        
        $message -> setFrom(array("noreply@avahis.com" => "Soukflux"));
        
        if($applicationMode == "DEV" ) {
            $message -> setFrom(array('philippe.lattention@silicon-village.fr' => "Soukflux"));
            $addr_avahis = "192.168.1.200/magento";
        }
        elseif($applicationMode == "REC" ) {
            $addr_avahis = "www.rec.avahis.com";
        }
        else{
            $addr_avahis = "www.avahis.com";
        }
        
        $add_text = "";
        if($this -> ticket_entity_id){
            $entity = $entityManager -> getById(array("entity_id" => $this -> getTicket_entity_id()));
            $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
            
            if($type -> getLabel() == "customer"){
                $client = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                if($client->getCustomer_id()){
                    $add_text = "Connectez vous avec votre compte Avahis et vous verrez toutes vos demandes.";
                    $ticketClient = "<a href='http://".$addr_avahis."/contactform/index/show/id/".$this->ticket_id."'>Répondre à la demande</a>";
                }else{
                    $ticketClient = "<a href='http://".$addr_avahis."/contactform/guest/show/id/".$this->ticket_token."' >Répondre à la demande</a>";    
                }
            }elseif($type -> getLabel() == "vendor"){
                $client = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                if($client->getVendor_id()){
                    $ticketClient = "<a href='http://".$addr_avahis."/contactform/vendor/show/id/".$this->ticket_id."'>Répondre à la demande</a>";
                }else{
                    $ticketClient = "<a href='http://".$addr_avahis."/contactform/guest/show/id/".$this->ticket_token."' >Répondre à la demande</a>";    
                }
            }
        }else{
            exit;
        }
        
        
        $message -> setTo($client->getEmail());
        //$message -> setTo('philippe.lattention@silicon-village.fr');
        
        $message -> setSubject("Service client Avahis.com : ".$this -> ticket_title);
        
        $message -> setBody("Bonjour cher client, le service d'Avahis.com souhaite vous contacter pour vous poser une question.
                              <h2>".$this -> ticket_title."</h2>
                              ".$this -> ticket_text."<br/><br/>
                              Pour répondre au service client veuillez-vous rendre à cette adresse :     $ticketClient <br/>
                              <br/>
                              Ceci est un mail automatique merci de ne pas y répondre directement." ,'text/html');
                                         
        $mailer = Swift_Mailer::newInstance($transport);

        if($mailer -> send($message)) 
            return true;
        else
            return false; 
    }
    
	public function isNew()
	{
		if ($this -> ticket_id){
			return false;
		}else{
			return true;
		}
	}
    
    public function getAll_comments()
    {
        $commentManager = new ManagerComment();
        return $commentManager -> getAllCommentsForObject($this->ticket_id, "skf_tickets");
    }
    
	public function save()
	{

	}
	
}