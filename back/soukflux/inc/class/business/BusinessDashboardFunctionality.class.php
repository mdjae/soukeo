<?php

class BusinessDashboardFunctionality extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $dashboard_functionality_id;
	protected $name;
	protected $size;


	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setDashboard_functionality_id($dashboard_functionality_id )
	{
		$this -> dashboard_functionality_id = $dashboard_functionality_id ;
	}

	public function setName($name)
	{
		$this -> name = $name;
	}
	
	public function setSize($size)
	{
		$this -> size = $size;
	}
	
    public function getDashboard_functionality_id( )
    {
        return $this -> dashboard_functionality_id ;
    }

    public function getName()
    {
        return $this -> name ;
    }
    
    public function getSize()
    {
        return $this -> size ;
    }	
	
	public function isNew()
	{
		if ($this -> getDashboard_functionality_id()){
			return false;
		}else{
			return true;
		}
	}
    
       
	public function save()
	{

	}
	
}