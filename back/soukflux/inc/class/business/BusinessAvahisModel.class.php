<?php
require_once("/var/www/soukflux/inc/libs/log4php/Logger.php");

class BusinessAvahisModel extends sdb_avahis {
	
	public $log;
	public $class;
	
	protected $stmtCheckProductSoldAv ;
	
	
	public function getallVendor(){
		$sql = "select * from udropship_vendor";
		return $this->getall($sql);
		
	}
	
	
	public function prepCheckProductIsSold()
	{

		$sql = "SELECT * 
				FROM udropship_vendor_product uvp
				INNER JOIN catalog_product_entity cpe ON cpe.entity_id = uvp.product_id
				WHERE cpe.sku = :sku
				AND uvp.vendor_id <> 24
				AND uvp.stock_qty >0";
				
		$this -> stmtCheckProductSoldAv = $this -> prepare($sql);
	}
	
	
	public function productIsAlreadySoldAvahis($sku)
	{
		$this -> stmtCheckProductSoldAv -> bindValue(":sku", $sku);

		return  $this -> getAllPrepa($this -> stmtCheckProductSoldAv ) ? true : false ;
	}
    
    public function getUdropship_shipping($shipping_code)
    {
        $sql = "SELECT shipping_title, days_in_transit FROM udropship_shipping WHERE shipping_code = '$shipping_code'";
        
        return $this->getall($sql);
    }
	
    public function getAllShippingMethod()
    {
        $sql = "SELECT * FROM udropship_shipping ";
        $stmt = $this->prepare($sql);
        
        return $this->getAllPrepa($stmt);
    }
    
    
    public function __construct() 
	    {
	         //$this->log = Logger::getRootLogger();
			 //$this->log->setLevel(LoggerLevel::INFO);
			 parent::__construct();
			 $this->log = Logger::getLogger("main");
			 $this->class = get_class($this);
	         //$this->log->info(" $this->class - Logger principal ON <br/>");
			 
		 	 //$this->logger->info("foo <br/>");
		 	 //$this->logger->warn("I'm not feeling so good...<br/>");
	     }
		
    
	/**
	 * Recupere les informaitons sur un attribut par son code
	 * @author mdjae
	 *
	 */
	public function getAttributeInfoByCode($attribute_code)
		{
		//	$this->log->info('getAttributeInfoByCode <br/>');
			$q = "SELECT * FROM  `eav_attribute` WHERE attribute_code = '$attribute_code'";
			
			//$stmt = $this->prepare($q);
			//$stmt->bindParam(':attribute_code',$attribute_code);
			//var_dump($stmt);
			//$stmt->execute();
			
	
			$r = $this->getAll($q);
			
			return $r[0];
		}
	
	/**
	 * Update des donnees de l'attribut
	 * @author mdjae
	 *
	 */
	public function updateEavAttribute($data)
		{
			
			$q = "UPDATE `eav_attribute`
				SET 
				`entity_type_id` = '{$data[':entity_type_id']}',
				`attribute_code` = '{$data[':attribute_code']}' ,
				`attribute_model` = NULL,
				`backend_model` = '',
				`backend_type` = 'varchar',
				`backend_table` = '',
				`frontend_model` = '',
				`frontend_input` = '{$data[':frontend_input']}',
				`frontend_label` = '{$data[':frontend_label']}',
				`frontend_class` = '',
				`source_model` = '',
				`is_required` = 0,
				`is_user_defined` = 1,
				`default_value` = '',
				`is_unique` = 0,
				`note` = ''
				WHERE
				`attribute_code` = '{$data[':attribute_code']}'";
			
			$stmt = $this->exec($q);
			//$stmt->bindValue($data);
			//var_dump($stmt);
			//$this->log->info('updateEavAttribute <br/>');
			//$this->log->info($q);
			return $stmt;
		}
	
	
	
	/**
	 * Update de la configuraiton de l'attribut dans le catalog
	 * @author mdjae
	 *
	 */
	public function updateCatalogEavAttribute($data)
		{
			
			$q = "UPDATE `catalog_eav_attribute` 
				SET 
				`attribute_id`  = '{$data[':attribute_id']}',
				`frontend_input_renderer` = '',
				`is_global` = 0,
				`is_visible` = 1,
				`is_searchable` = 1,
				`is_filterable` = '{$data[':is_filterable']}',
				`is_comparable` = 1,
				`is_visible_on_front` = 1,
				`is_html_allowed_on_front` = 0,
				`is_used_for_price_rules` = 0,
				`is_filterable_in_search` = 0,
				`used_in_product_listing` = 0,
				`used_for_sort_by` = 0, 
				`is_configurable` = 1,
				`apply_to` = '',
				`is_visible_in_advanced_search` = 0,
				`position` = 0,
				`is_wysiwyg_enabled` = 0,
				`is_used_for_promo_rules` = 0 
				WHERE
				attribute_id = '{$data[':attribute_id']}'";
			
			$stmt = $this->prepare($q);
			//$stmt->bindValue($data);
			//$this->log->info('updateCatalogEavAttribute <br/>');
			//$this->log->info($q);
			return $stmt->execute();
		}
		
	
	public function updateEavAttributeLabel($data)
		{
			
			$q = "UPDATE  `eav_attribute_label` 
					SET  `value` =  {$data[':label']} 
					WHERE `attribute_id` ={$data[':attribute_id']}";
			
			$stmt = $this->prepare($q);
			//$stmt->bindValue($data);
			//$this->log->info('updateEavAttributeLabel <br/>');
			//$this->log->info($q);
			
			return $stmt->execute();
		}
	
	 public function readCsv($file, $delimiter)
	 {
	    	//$this->log->info('readCSV <br/>');
	    	//try{
	    			 //var_dump(fopen($file, "r"));
	    		
			        if (($handle = fopen($file, "r")) !== FALSE) {
							//$handle = fopen($file, "r");
				            $header = array();
				            $elem = array();
				            $row = 0;
				            
							//$this->log->info('readCSV : boucle sur les datas <br/>');
				            while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {       
					                $num = count($data);
					                $config = array();
					
						                for ($i = 0; $i < $num; $i++) {
							                    ($row == 0) ? $header[] = $data[$i] : $config[] = $data[$i];
							                }
					
					                //Si on est pas sur l'entete
					                if($row != 0) { $elem [] = $config;}
					                $row++;
					            }
	
				            //$this->log->info('readCSV : Création du tableau associatif <br/>');
							
				            //Création du tableau associatif
				            foreach ($elem as $e) {
				                $r[] = array_combine($header, $e);
				            }

				            return $r;
				        }


		     //   } catch(Exception $e){
		        	
		        	//$this->log->info($e->getMessage());
		       //     return false;
		       // }
	    }
}