<?php

class BusinessOrder extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $order_id;
	protected $order_state;
	protected $order_status;
	protected $order_customer_id;  
	protected $recipient_name;
	protected $recipient_email;
	protected $recipient_phone;
	protected $base_grand_total;
	protected $increment_id;
	protected $shipping_address_id;
	protected $weight;
	protected $date_created;
	protected $date_updated;
	protected $litiges;

	

	public function __construct ($order_id = false) {
		
		self::$db = new BusinessSoukeoModel();	
		if($order_id){
			$this->order_id = $order_id;
			$data = self::$db -> getOrderByNumOrder($this->order_id);
			$data = $data[0];
			if (!$data){
				return false;
			}else{
				$this -> hydrate($data);
			} 	
		}	
	}
	
	
	public function setOrder_id($order_id){
		$this -> order_id = $order_id;
	}

	public function setOrder_status($order_status)
	{
		$this -> order_status = $order_status;
	}
	
	public function setOrder_state($order_state)
	{
		$this -> order_state = $order_state;
	}
	
	public function setOrder_customer_id($order_customer_id)
	{
		$this -> order_customer_id = $order_customer_id;
	}
	
	public function setRecipient_name($recipient_name)
	{
		$this -> recipient_name = $recipient_name;
	}

	public function setRecipient_email($recipient_email)
	{
		$this -> recipient_email = $recipient_email;
	}
	
	public function setRecipient_phone($recipient_phone)
	{
		$this -> recipient_phone = $recipient_phone;
	}
			
	public function setBase_grand_total($base_grand_total)
	{
		$this -> base_grand_total = $base_grand_total;
	}
	
	public function setIncrement_id($increment_id)
	{
		$this -> increment_id = $increment_id ;
	}
	
	public function setShipping_address_id($shipping_address_id)
	{
		$this -> shipping_address_id = $shipping_address_id;
	}
	
	public function setWeight($weight)
	{
		$this -> weight = $weight;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
	
	public function setLitiges($litiges)
	{
		if($litiges == "1" | $litiges == "0"){
			$this -> litiges = $litiges;	
		}
	}
	
	public function getOrder_id()
	{
		return $this -> order_id;
	}
	
	public function getOrder_status()
	{
		return $this -> order_status;
	}
	
	public function getOrder_state()
	{
		return $this -> order_state;
	}
	
	public function getOrder_customer_id()
	{
		return $this -> order_customer_id ;
	}
	
	public function getRecipient_name()
	{
		return $this -> recipient_name;
	}

	public function getRecipient_email()
	{
		return $this -> recipient_email;
	}
	
	public function getRecipient_phone()
	{
		return $this -> recipient_phone;
	}
		
	public function getBase_grand_total()
	{
		return $this -> base_grand_total;
	}
	
	public function getIncrement_id()
	{
		return $this -> increment_id;
	}
	
	public function getShipping_address_id()
	{
		return $this -> shipping_address_id;
	}	
	
	public function getWeight()
	{
		return $this -> weight;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function getLitiges()
	{
		return $this -> litiges;
	}
	
	public function save()
	{
		self::$db -> updateOrder($this);
	}
	
	public function getAllPo()
	{
		return self::$db -> getAllPoByOrder($this -> getOrder_id());
	}
}