<?php 

/**
 * Clase type de script qui parse CSV à Remplir
 */
class BusinessImportVirtuemart extends BusinessImportPlugin {
	
	
	protected static $nbRun 		= 0; 		//Compte le nombre de fois que ce batch a été executé

	protected $versionSft			= "";

	protected static $db;
	// pour le crawl des images
	//private static $pictureManager ;

	/**
	 * Constructeur, cette classe prend en paramètre l'URL et l'ID du vendeur
	 */
	function __construct($url, $vendorID, $versionSft, $logfile) {
	    
        parent::__construct($url, $vendorID, $logfile);
        
		$this->versionSft		= $versionSft ; 
	}
	
	
	/**
	 * Fonction qui parse le CSV ,produit des tableaux associatifs 
	 * et appelle le fonctions d'insertion en BDD
	 */
	public function run()
	{
		
		//On parse le CSV de l'e-commercant dispo à l'URL fourni
		if(!$this->parseCSV($this->url_to_parse, 34, ";", true)){
		    
		    return false;
		}
        else{
            self::$db = new BusinessSoukeoVirtuemartModel ();
            // déclaration suivante pour le crawl des images
            //self::$pictureManager = new BusinessPictureManager ($this->vendorID);
			
            //Préparation des requetes
            self::$db -> prepareStmtProdEcommercant($this->vendorID);
            self::$db -> prepareInsertSfkProdVM($this->vendorID);
            self::$db -> prepareInsertSfkProdAttrVM();
            
            foreach ($this -> productListAttribute as $product) {
                //Insert du produit
    
                //////////////////////////////////////// TMP CORRECTION IMAGES VM /////////////////////////////////////
                if($this -> vendorID == "39"){
                    $tmp = $product['IMAGE_PRODUCT'];
                    $tmp = explode(":", $tmp);
                    $tmp = $tmp[2];
                    $product['IMAGE_PRODUCT'] = "http:" . $tmp ;    
                }
                //////////////////////////////////////// TMP CORRECTION IMAGES VM /////////////////////////////////////
                

                //////////////////////////////////////// TMP CORRECTION IMAGES VM /////////////////////////////////////
                if($this -> vendorID == "39"){
                    $tmp = $product['THUMBNAIL'];
                    $tmp = explode(":", $tmp);
                    $tmp = $tmp[2];
                    $product['THUMBNAIL'] = "http:" . $tmp ;    
                }
                //////////////////////////////////////// TMP CORRECTION IMAGES VM /////////////////////////////////////
                    
                //Insertion produit
                $state = self::$db->addRowSfkProdVM($product, $this->vendorID) ;
                
                if( $state == 1){ //Si la requete et bien passée
                    $this->nbInsert++;
                }elseif($state == 2 ){
                    $this->nbUpdate++;
                }elseif($state == 0){
                    $this->nbErrors++;
                }
                
                
                foreach ($this->attributeList as $attribute) {
                    
                    //Produit possède cet attribut?
                    if ($product[$attribute] != "") {
    
                        //On vérifie si l'attribut existe déja 
                        $id_attribute = self::$db->checkIfAttributeExistsVM($attribute);
                        //Relation produit_Attribut
                        if(self::$db->addRowSfkProdAttrVM($product['ID_PRODUCT'], $this->vendorID, $id_attribute,$product['TITRE_DECLINAISON'], $product[$attribute]) == true){

                            $this->nbInsertAttr++;
                        }   
                    }
                }
                
                // si associé mise a jour de la table produit stocke prix et Images
                if ($prodAv = self::$db->getProductAvAssoc($this->vendorID,$product['ID_PRODUCT'])){// a rajouté aprés pour verif la date &&self::$db->verifDate_upBddLocalImport($product)){
                       
                    //Telechargement des images localement   
                    //!$prodAv['local_image'] ? self::$pictureManager -> savePictures($product, $prodAv) : "";
                        
                    $test = self::$db->addRowSfkProdEcomSP($product, $this->vendorID);  
                        
                }
                
            }
            self::$nbRun++;     
            
            return true;            
        }

	}

	
		/**
	 * @return nbRun le nombre de fois que ce Script a été effectué
	 */
	public static function getNbRun()
	{
		return self::$nbRun;
	}
	
}
?>