<?php 

/**
 * Class type de script qui fait la mise a jour de la table stock/prix suivant la table association
 */
class BusinessSyncPrestashopSP {
	
	//protected static $nbRun 		= 0; 		//Compte le nombre de fois que ce batch a été executé
	
	protected $url_to_parse 		= "";		//URL de l'ecommerçant à parser
	protected $vendorID 			= ""; 		//VENDOR_ID de l'ecommercant
	protected $productListAttribute = null; 	//Liste produits 
	protected static $nbSyncSP 		= 0; 		//log
	
	protected static $db;
	
	/**
	 * Constructeur, cette classe prend en paramètre l'URL et l'ID du vendeur
	 */
	function __construct($url, $vendorID) {
		$this->url_to_parse 	= $url;
		$this->vendorID 		= $vendorID;
		self::$nbSyncSP = 0;
	}
	
	/**
	 * Appelle les fonctions d'insertion en BDD
	 */
	public function run()
	{
		//$vendorManager = new ManagerVendor();
		//$vendor = $vendorManager -> getVendorByVendorId($this -> vendorID);
		
		//On parse le CSV de l'e-commercant dispo à l'URL fourni
		$this->parseCSV($this->url_to_parse, 9, "|");
	    
        self::$db = new BusinessSoukeoPrestashopModel ();
        
        //Préparation des requetes
        self::$db->prepareStmtProdEcommercant($this->vendorID);
		
		//var_dump($this->productListAttribute);
		foreach ($this->productListAttribute as $product) {
			//Insert du produit
			//on vérifie si ce produit est présent dans la table association, et que les dates concordent
		    
			if (self::$db->productIsAssoc($this->vendorID,$product['ID_PRODUCT'])){// a rajouté aprés pour verif la date &&self::$db->verifDate_upBddLocalImport($product)){
					
				//Si la quantité est supérieur à 0 et que l'on a toujours pas dépassé la limite de produits alors
	        	$test = self::$db->addRowSfkProdEcomSP($product, $this->vendorID); 	
		    	if ($test)
		    	self::$nbSyncSP++;
				
			}
		}

	}
	/**
	 * @author Philippe_LA/FHKC
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param url 				String 		L'url qui va contenir le CSV
	 * @param nbChampsFixes 	Int 		Le nombre de champs fixe qui sont communs à tous produits
	 * @param delimiter 		String 		Le délimiteur choisi dans le CSV
	 */
	protected function parseCSV($url, $nbChampsFixes, $delimiter)
	{
        $max_trys = SystemParams::getParam('system*plugin_max_trys');
        
        for ($i=1; $i <= $max_trys ; $i++) { 
            
            $file = curl_file_get_contents($url);    
        
            if($file === false){
                echo PHP_EOL."ERREUR tentative $i Le contenu de l'URL n a pas pu etre recupere !".PHP_EOL;
                
                if($i == $max_trys){
                    echo PHP_EOL."NOMBRE MAX DE TENTATIVES ATTEINTS Le contenu de l'URL n a pas pu etre recupere !".PHP_EOL;
                    return false;
                }
            }
            else{
                echo PHP_EOL."Contenu de l'URL recupere, debut du parsing CSV ... !".PHP_EOL;
                
        		$data = str_getcsv($file, "\r\n"); //parse les lignes 
        
        		//Pour chaque ligne on crée un produit
        		foreach ($data as $row) {
        			$productList[] = str_getcsv(str_replace("\n", "", $row), $delimiter, '"');
        		}
        		
        		//L'entete est le "premier produit" de la lite
        		$headerList = $productList[0];
        		
        		//On enlève l'entête de la liste produits
        		$productList = array_slice($productList, 1);
        		
        		//Création du tableau associatif
        		foreach ($productList as $p) {
        			$this->productListAttribute[] = array_combine($headerList, $p);
        		}
                
                return true;
            }
        }
	}	
	
	/**
	 * @return $nbSyncSP le nombre de fois que la syncho SPP a été effectué
	 */
	public static function getNbSyncSP()
	{
		return self::$nbSyncSP;
	}
	
}
?>