<?php 

/**
 * 
 */
class BusinessExportProdsNonVendus extends BusinessExportCSV {
	
	protected $path = "/opt/soukeo/export/";
	protected static $db;
	protected $nbProds;
	
	
	function __construct() {
		parent::__construct($this -> path);
		$this -> makeContent();

		
		//self::$db -> saveFileEntry($file_name, "BusinessExportProdsNonVendus", "Fichier des produits grossistes en vente");
	}
	
	public function makeContent()
	{
		self::$db = new BusinessSoukeoModel();
		
		$this -> makeLine($this -> getHeaderChpsFixes());
		
		$productList = self::$db->getAllProdsNonVendus();
		
		$count = count($productList);
		
				
		foreach ($productList as $product) {
			
			$this -> makeLine($this->getProductChpsFixes($product));
			
			$this -> nbProds ++ ;
			if( ($this -> nbProds != 0) && ( $this -> nbProds % 500 ==  0 )){
				$this -> fileName = "non-vendus_". ($this->nbProds - 500). "_a_".$this->nbProds. ".csv";
				$this -> generateCSV(";", true);
				$this -> arrCSV = array();
				$this -> makeLine($this -> getHeaderChpsFixes());
				$last = $this->nbProds;
			}
		}
		$this -> fileName = "non-vendus_". ($last). "_a_".$this->nbProds. ".csv";
		$this -> generateCSV(";", true);
		
		
	}

/**
	 * Retourne un array contenant les labels des champs fixes pour entête 
	 * @return array 	contenant les champs fixes du CSV.
	 */
	protected function getHeaderChpsFixes()
	{
		return array(	"id_produit",
						"name",
						"EAN",
						"categorie",
						"manufacturer",
						"short_description",
						"prix_grossiste",
						"poids" );
	}


	/**
	 * Fonctionnant retournant un array contenant les données pour les champs fixes
	 * du produit
	 * @param 	array 	$product 	 	contenant les informations du produit
	 * @param  	string 	$categories  	contenant l'arborescence de la catégorie sous forme cat/sscat/sssscat
	 * @return 	array 					contenant les différents champs fixes remplis pour aller dans le CSV
	 */
	protected function getProductChpsFixes($product, $categories)
	{

		
		return array(	$product['id_produit'],
						$product['name'],
						$product['sku'],
						$product['cat_label'],
						$product['manufacturer'],
						$product['short_description'],
						$product['PRICE_PRODUCT'],
						$product['weight'],);
	}
			
}

?>