<?php

class BusinessCreditsHistory extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $history_id;
	protected $vendor_id;
	protected $label;
	protected $amount;
	protected $type;
    protected $pack_id  ;
    protected $facture_id;
	protected $date_created;
	
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	public function setHistory_id($history_id)
	{
		$this -> history_id = $history_id;
	}
	
	public function setVendor_id($vendor_id)
	{
		$this -> vendor_id = $vendor_id;
	}
	
	public function setLabel($label)
	{
		$this -> label = $label;
	}
	
	public function setAmount($amount)
	{
		$this -> amount = $amount;
	}
	
	public function setType($type)
	{
		$this -> type = $type;
	}
	
    public function setPack_id($pack_id)
    {
        $this -> pack_id = $pack_id;
    }
    
    public function setFacture_id($facture_id)
    {
        $this -> facture_id = $facture_id;
    }
    
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
    public function getHistory_id()
    {
        return $this -> history_id ;
    }
    
    public function getVendor_id()
    {
        return $this -> vendor_id ;
    }
    
    public function getLabel()
    {
        return $this -> label ;
    }
    
    public function getAmount()
    {
        return $this -> amount ;
    }
    
    public function getType()
    {
        return $this -> type ;
    }		
    
    public function getPack_id()
    {
        return $this -> pack_id ;
    }
    
    public function getFacture_id()
    {
        return $this -> facture_id;
    }
    
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	
	public function isNew()
	{
		if ($this -> getHistory_id()){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}