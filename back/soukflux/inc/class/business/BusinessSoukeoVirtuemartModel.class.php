<?php
/**
 * Classe Model à remplir avec les requetes propres aux tables
 * Virtuemart de Soukflux
 */
class BusinessSoukeoVirtuemartModel extends BusinessSoukeoModel {

	protected $stmtProduitVM;
	protected $stmtProdAttrVM;
	protected $stmtProdEcommercant;
	/**
	 * Préparation de l'insertion / update des produits du vendeur dans sfk_produits_vm
	 * Il y a également une remise à 0 du champ Active avant la nouvelle insertion ou update
	 * ainsi, même si l'eCommercant a changé d'avis dans ce qu'il veux exporter les produits
	 * restent en base de donnée et seuls ceux actifs iront vers Avahis
	 */
	public function prepareInsertSfkProdVM($vendorID) {
		//Remise à 0 actif de tous les produits
		$sql = "UPDATE sfk_produit_vm SET ACTIVE = 0 WHERE VENDOR_ID = :VENDOR_ID";
		//ID DU VENDEUR DYNAMIQUE BESOIN TRACKING
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':VENDOR_ID', $vendorID);
		$stmt -> execute();

		$sql = "INSERT INTO sfk_produit_vm  (	ID_PRODUCT, VENDOR_ID, REFERENCE_PRODUCT, CATEGORY, NAME_PRODUCT, 
												DESCRIPTION, DESCRIPTION_SHORT,	PRICE_PRODUCT,											
												PRICE_TTC, PRICE_REDUCTION, REDUCTION_FROM, REDUCTION_TO, 
												WEIGHT, WEIGHT_UNIT, LENGHT, WIDHT, HEIGHT, LWH_UNIT, 
												META_DESCRIPTION, META_KEYWORD, META_TITLE, IMAGE_PRODUCT, 
												THUMBNAIL, ETAT_PROMO, SPECIAL_PRODUCT, QUANTITY, 
												AVAILABLE_PRODUCT, DEVISE, MANUFACTURER, PRODUCT_PRICE_PUBLISH_UP,
												PRODUCT_PRICE_PUBLISH_DOWN, PRICE_QUANTITY_START, PRICE_QUANTITY_END, 
												ATTRIBUTE, CUSTOM_ATTRIBUTE ,
												ACTIVE, DATE_CREATE)
					 				VALUES (	:ID_PRODUCT, :VENDOR_ID, :REFERENCE_PRODUCT, :CATEGORY, :NAME_PRODUCT, 
												:DESCRIPTION, :DESCRIPTION_SHORT, :PRICE_PRODUCT ,												
												:PRICE_TTC, :PRICE_REDUCTION, :REDUCTION_FROM, :REDUCTION_TO, 
												:WEIGHT, :WEIGHT_UNIT, :LENGHT, :WIDHT, :HEIGHT, :LWH_UNIT, 
												:META_DESCRIPTION, :META_KEYWORD, :META_TITLE, :IMAGE_PRODUCT, 
												:THUMBNAIL, :ETAT_PROMO, :SPECIAL_PRODUCT, :QUANTITY, 
												:AVAILABLE_PRODUCT, :DEVISE, :MANUFACTURER, :PRODUCT_PRICE_PUBLISH_UP,
												:PRODUCT_PRICE_PUBLISH_DOWN, :PRICE_QUANTITY_START, :PRICE_QUANTITY_END, 
												:ATTRIBUTE, :CUSTOM_ATTRIBUTE ,
												:ACTIVE , NOW())
									ON DUPLICATE KEY UPDATE 
												
												REFERENCE_PRODUCT			=	VALUES(REFERENCE_PRODUCT),
												CATEGORY					=	VALUES(CATEGORY),
												NAME_PRODUCT				=	VALUES(NAME_PRODUCT),
												DESCRIPTION					=	VALUES(DESCRIPTION),
												DESCRIPTION_SHORT			=	VALUES(DESCRIPTION_SHORT),
												PRICE_PRODUCT				=	VALUES(PRICE_PRODUCT),
												PRICE_TTC					=	VALUES(PRICE_TTC),
												PRICE_REDUCTION				=	VALUES(PRICE_REDUCTION),
												REDUCTION_FROM				=	VALUES(REDUCTION_FROM),
												REDUCTION_TO				=	VALUES(REDUCTION_TO),
												WEIGHT						=	VALUES(WEIGHT),
												WEIGHT_UNIT					=	VALUES(WEIGHT_UNIT),
												LENGHT						=	VALUES(LENGHT),
												WIDHT						=	VALUES(WIDHT),
												HEIGHT						=	VALUES(HEIGHT),
												LWH_UNIT					=	VALUES(LWH_UNIT),
												META_DESCRIPTION			=	VALUES(META_DESCRIPTION),
												META_KEYWORD				=	VALUES(META_KEYWORD),
												META_TITLE					=	VALUES(META_TITLE),
												IMAGE_PRODUCT				=	VALUES(IMAGE_PRODUCT),
												THUMBNAIL					=	VALUES(THUMBNAIL),
												ETAT_PROMO					=	VALUES(ETAT_PROMO),
												SPECIAL_PRODUCT				=	VALUES(SPECIAL_PRODUCT),
												QUANTITY					=	VALUES(QUANTITY),
												AVAILABLE_PRODUCT			=	VALUES(AVAILABLE_PRODUCT),
												DEVISE						=	VALUES(DEVISE),
												MANUFACTURER				=	VALUES(MANUFACTURER),
												PRODUCT_PRICE_PUBLISH_UP	=	VALUES(PRODUCT_PRICE_PUBLISH_UP),
												PRODUCT_PRICE_PUBLISH_DOWN	=	VALUES(PRODUCT_PRICE_PUBLISH_DOWN),
												PRICE_QUANTITY_START		=	VALUES(PRICE_QUANTITY_START),
												PRICE_QUANTITY_END			=	VALUES(PRICE_QUANTITY_END),
												ATTRIBUTE					=	VALUES(ATTRIBUTE),
												CUSTOM_ATTRIBUTE			=	VALUES(CUSTOM_ATTRIBUTE),
												ACTIVE 						=   1";
			
												//ID_PRODUCT					=	VALUES(ID_PRODUCT),

		$this -> stmtProduitVM = $this -> prepare($sql);
	}

	/**
	 * Fonction permettant l'insertion ou l'update de produits dans la base de donnée soukflux
	 * Si le couple VENDOR_ID/ID_PRODUCT existe déjà il y a juste update du produit.
	 * Changements à effectuer dans les champs récupérés, besoin d'affiner
	 */
	public function addRowSfkProdVM($product, $vendorID) {
		
		$prodExistant = $this -> getProduct($vendorID, $product['ID_PRODUCT']);
		if($prodExistant != null) $alreadyExist = true;
		if($vendorID == "39"){ // en attente de la finalisation de la fonction verifUnit
			$product['WEIGHT']=$product['WEIGHT']/1000;	
		}
		
		
		$this -> stmtProduitVM -> bindValue(':ID_PRODUCT'					,$product['ID_PRODUCT'], PDO::PARAM_STR);
		$this -> stmtProduitVM -> bindValue(':VENDOR_ID'					,$vendorID);
		$this -> stmtProduitVM -> bindValue(':REFERENCE_PRODUCT'			,$product['REFERENCE_PRODUCT']);
		$this -> stmtProduitVM -> bindValue(':CATEGORY'						,$product['CATEGORY']);
		$this -> stmtProduitVM -> bindValue(':NAME_PRODUCT'					,$product['NAME_PRODUCT']);
		$this -> stmtProduitVM -> bindValue(':DESCRIPTION'					,$product['DESCRIPTION'] ? $product['DESCRIPTION'] : $product['DESCRIPTION_SHORT']);
		$this -> stmtProduitVM -> bindValue(':DESCRIPTION_SHORT'			,$product['DESCRIPTION_SHORT'] ? $product['DESCRIPTION_SHORT'] : $product['MANUFACTURER'] . " - " . $product['NAME_PRODUCT']);
		$this -> stmtProduitVM -> bindValue(':PRICE_PRODUCT'				,str_replace(",", ".", $product['PRICE_PRODUCT']));
		$this -> stmtProduitVM -> bindValue(':PRICE_TTC'					,$product['PRICE_TTC']);
		$this -> stmtProduitVM -> bindValue(':PRICE_REDUCTION'				,$product['PRICE_REDUCTION']);
		$this -> stmtProduitVM -> bindValue(':REDUCTION_FROM'				,$product['REDUCTION_FROM']);
		$this -> stmtProduitVM -> bindValue(':REDUCTION_TO'					,$product['REDUCTION_TO']);
		$this -> stmtProduitVM -> bindValue(':WEIGHT'						,$product['WEIGHT']? str_replace(",", ".", $product['WEIGHT']) : 0);
		//$this -> stmtProduitVM -> bindValue(':WEIGHT'						,$product['WEIGHT']? str_replace(",", ".", $this -> verifUnit($product['WEIGHT']),$product['WEIGHT_UNIT']) : 0); //attente finatlisaiotn verifUnit
		$this -> stmtProduitVM -> bindValue(':WEIGHT_UNIT'					,$product['WEIGHT_UNIT']);
		$this -> stmtProduitVM -> bindValue(':LENGHT'						,$product['LENGHT']);
		$this -> stmtProduitVM -> bindValue(':WIDHT'						,$product['WIDHT']);
		$this -> stmtProduitVM -> bindValue(':HEIGHT'						,$product['HEIGHT']);
		$this -> stmtProduitVM -> bindValue(':LWH_UNIT'						,$product['LWH_UNIT']);
		$this -> stmtProduitVM -> bindValue(':META_DESCRIPTION'				,$product['META_DESCRIPTION']);
		$this -> stmtProduitVM -> bindValue(':META_KEYWORD'					,$product['META_KEYWORD']);
		$this -> stmtProduitVM -> bindValue(':META_TITLE'					,$product['META_TITLE']);
		$this -> stmtProduitVM -> bindValue(':IMAGE_PRODUCT'				,$product['IMAGE_PRODUCT']);
		$this -> stmtProduitVM -> bindValue(':THUMBNAIL'					,$product['THUMBNAIL']);
		$this -> stmtProduitVM -> bindValue(':ETAT_PROMO'					,$product['ETAT_PROMO']);
		$this -> stmtProduitVM -> bindValue(':SPECIAL_PRODUCT'				,$product['SPECIAL_PRODUCT']);
		$this -> stmtProduitVM -> bindValue(':QUANTITY'						,$product['QUANTITY']);
		$this -> stmtProduitVM -> bindValue(':AVAILABLE_PRODUCT'			,$product['AVAILABLE_PRODUCT']);
		$this -> stmtProduitVM -> bindValue(':DEVISE'						,$product['DEVISE']);
		$this -> stmtProduitVM -> bindValue(':MANUFACTURER'					,ucfirst(strtolower($product['MANUFACTURER'])));
		$this -> stmtProduitVM -> bindValue(':PRODUCT_PRICE_PUBLISH_UP'		,$product['PRODUCT_PRICE_PUBLISH_UP']);
		$this -> stmtProduitVM -> bindValue(':PRODUCT_PRICE_PUBLISH_DOWN'	,$product['PRODUCT_PRICE_PUBLISH_DOWN']);
		$this -> stmtProduitVM -> bindValue(':PRICE_QUANTITY_START'			,$product['PRICE_QUANTITY_START']);
		$this -> stmtProduitVM -> bindValue(':PRICE_QUANTITY_END'			,$product['PRICE_QUANTITY_END']);
		$this -> stmtProduitVM -> bindValue(':ATTRIBUTE'					,$product['ATTRIBUTE']);
		$this -> stmtProduitVM -> bindValue(':CUSTOM_ATTRIBUTE'				,$product['CUSTOM_ATTRIBUTE']);
		$this -> stmtProduitVM -> bindValue(':ACTIVE'				,'1');
		
		$sqlState = $this->stmtProduitVM->execute();
		
		//UPDATE
		if( $alreadyExist && $sqlState == 1){
			return 2;
		//FAIL
		}elseif($sqlState == 0){
			return 0;
		//INSERT
		}elseif($sqlState == 1){
			return 1;
		}	
	}

	/**
	 * Préparation des insertions des relations produits_attributs dans les tables dédiées à
	 * Virtuemart de Soukflux
	 */
	public function prepareInsertSfkProdAttrVM() {
		$sql = "INSERT INTO sfk_produit_attribut_vm (ID_PRODUCT, VENDOR_ID, ID_ATTR, VALUE)
													VALUES (:ID_PRODUCT, :VENDOR_ID, :ID_ATTR, :VALUE)
													ON DUPLICATE KEY UPDATE
													ID_ATTR 	=VALUES(ID_ATTR),
													VALUE 		=VALUES(VALUE)";

		/*ID_PRODUCT =VALUES(ID_PRODUCT),
		 VENDOR_ID =VALUES(VENDOR_ID),
		 TITRE_DECLINAISON =VALUES(TITRE_DECLINAISON),*/
		$this -> stmtProdAttrVM = $this -> prepare($sql);
	}

	/**
	 * Insertion des relations produits attributs dans les tables dédiées à
	 * Virtuemart de Soukflux
	 */
	public function addRowSfkProdAttrVM($idProd, $vendorID, $idAttr, $titreDeclinaison, $value) {

		$this -> stmtProdAttrVM -> bindValue(':ID_PRODUCT', $idProd);
		$this -> stmtProdAttrVM -> bindValue(':VENDOR_ID', $vendorID);
		$this -> stmtProdAttrVM -> bindValue(':ID_ATTR', $idAttr);
		$this -> stmtProdAttrVM -> bindValue(':VALUE', $value);
		return $this -> stmtProdAttrVM -> execute();
	}

	/**
	 * Va chercher si l'attribut existe déja dans la table s'il existe retourne l'ID_ATTRIBUT de cet
	 * attribut sinon la fonction Crée ce nouvel Attribut et renvoie le dernier ID_ATTRIBUT auto incrémenté
	 * correspondant
	 */
	public function checkIfAttributeExistsVM($attribute) {
		//echo $attribute."<br>";
		$sql = "SELECT ID_ATTR FROM sfk_attribut_vm WHERE CODE_ATTR = :CODE_ATTR";

		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":CODE_ATTR", $attribute);
		$stmt -> execute();

		if ($result = $stmt -> fetch(PDO::FETCH_ASSOC)) {
			return $result['ID_ATTR'];
		} else {
			$stmt = $this -> prepare("INSERT INTO sfk_attribut_vm (LABEL_ATTR, CODE_ATTR) 
												VALUES (:LABEL_ATTR, :CODE_ATTR)");
			$stmt -> bindValue(":LABEL_ATTR", str_replace("_", " ", ucfirst(strtolower($attribute))));
			$stmt -> bindValue(":CODE_ATTR", $attribute);
			$stmt -> execute();

			return $this -> lastInsertId();
		}
	}

	/**
	 * Permet de renvoyer un attribut Virtuemart particulier à partir de son ID_ATTR
	 * @param idAttr int l'id de l'attribut recherché
	 */
	public function getAttributeVM($idAttr) {
		$sql = "SELECT ID_ATTR, LABEL_ATTR, CODE_ATTR FROM sfk_attribut_vm WHERE ID_ATTR = '$idAttr' ";

		return $this -> getOne($sql);
	}

	/**
	 * Prépare requete de selection de tous les attributs Virtuemart
	 */
	public function getAllAttributesVM() {
		$sql = 'SELECT * FROM sfk_attribut_vm';
		return $this -> getAll($sql);
	}

	/**
	 * Renvoie toutes les relations produit-attribut pour un idProduit donné
	 * @param $idProductVirtuemart 	String 	idProduit Pretashop dont on veux les attributs
	 */
	public function getAttributeProductVM($idProductVM) {
		$sql = "SELECT * FROM sfk_produit_attribut_vm WHERE ID_PRODUCT = '" . $idProductVM . "'";

		return $this -> getAll($sql);
	}

	/**
	 * @param idProduct ID du produit recherché
	 */
	public function getProductVM($idProduct) {
		$sql = "SELECT * FROM sfk_produit_vm WHERE ID_PRODUCT = '" . $idProduct . "'";

		return $this -> getOne($sql);
	}

	/**
	 * Renvoie tous les produits Virtuemart
	 */
	public function getAllProductsVM() {
		$sql = "SELECT * FROM sfk_produit_vm";

		return $this -> getAll($sql);
	}

	/**
	 * Renvoie tous les produits Virtuemart pour un vendeur donné
	 */
	public  function getAllProductsVMByVendor($vendorID) {
		
		$sql = "SELECT * FROM sfk_produit_vm WHERE VENDOR_ID = :VENDOR_ID";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> execute() ;
		$res =  $stmt-> fetchAll();
		return $res;
		
	}
/*----------------------------Synchronisation SP--------------------------------------------*/
	/**
	 * Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif dans SON Thelia
	 */
	public function purgeInactiveProduct() {
		//Si le produit a été passé en inactif chez le commercant, on l'a recu en inactif coté soukflux
		//si la mise a jour a bien eu lieu alors on va simplement supprimer les produits inactifs de cet ecommercant
		$sql = "	DELETE  spe  
				FROM sfk_product_ecommercant_stock_prix spe
				INNER JOIN  sfk_produit_th sp 
				ON spe.VENDOR_ID = sp.VENDOR_ID AND spe.id_produit = sp.ID_PRODUCT
				WHERE sp.ACTIVE = 0;";

		$this -> exec($sql);
	}



	/**
	 * Fonction permettant l'insertion ou la mise a jour de la table stock/prix
	 */
	public function addRowSfkProdEcomSP($product, $vendorID) {
	
		
		//var_dump($product);
		$idProduit = $product['ID_PRODUCT'];
		
		$prodAvAssoc = $this->getProdAvAssoc($vendorID, $idProduit);
		// ETAT spécial ou pas ?
		if ($product['SPECIAL_PRODUCT']==1){
			$ETAT = "SPECIAL";
		}
		else {
			$ETAT = "";
		}
		//var_dump($idProduit);
		//var_dump($vendorID);
		$this -> stmtProdEcommercant -> bindValue(':id_produit',			 	$prodAvAssoc['EAN']); //sku de Avahis
		$this -> stmtProdEcommercant -> bindValue(':VENDOR_ID', 				$vendorID);
		$this -> stmtProdEcommercant -> bindValue(':ref_ecommercant', 			$product['REFERENCE_PRODUCT']);
		$this -> stmtProdEcommercant -> bindValue(':PRICE_PRODUCT', 			$this -> verifNumNC($product['PRICE_PRODUCT']));
		$this -> stmtProdEcommercant -> bindValue(':PRICE_TTC', 				$this -> verifNumNC($product['PRICE_TTC']));
		$this -> stmtProdEcommercant -> bindValue(':QUANTITY', 					$product['QUANTITY']);
		$this -> stmtProdEcommercant -> bindValue(':PRICE_HT', 					'');
		$this -> stmtProdEcommercant -> bindValue(':REDUCTION_FROM', 			$this -> verifNumNC($product['REDUCTION_FROM']));
		$this -> stmtProdEcommercant -> bindValue(':REDUCTION_TO', 				$this -> verifNumNC($product['REDUCTION_TO']));
		//$this -> stmtProdEcommercant -> bindValue(':PRICE_QUANTITY_START',$product['PRICE_QUANTITY_START']);
		//$this -> stmtProdEcommercant -> bindValue(':PRICE_QUANTITY_END',$product['PRICE_QUANTITY_END']);
		$this -> stmtProdEcommercant -> bindValue(':PRICE_REDUCTION', 			$this -> verifNumNC($product['PRICE_REDUCTION']));
		$this -> stmtProdEcommercant -> bindValue(':POURCENTAGE_REDUCTION', 	'');
		$this -> stmtProdEcommercant -> bindValue(':ETAT_PROMO', 				$product['ETAT_PROMO']);
		$this -> stmtProdEcommercant -> bindValue(':ETAT', 						$ETAT );

		return $this -> stmtProdEcommercant -> execute();
	}
	
	/**
	 * Vérifie si ce produit est bien associé dans la table assoc_product
	 * @param string l'id produit
	 * @param string l'id vendeur
	 * @return boolean oui si elle est bien associé
	 */
	public function verifAssocProduct(){
	
		$sql = "SELECT * FROM sfk_produit_th WHERE VENDOR_ID = :VENDOR_ID AND ID_PRODUCT = :ID_PRODUCT";
			if($infoTracking = $this -> getRow($sql)) {
			return true ;
		}
		else {
			return false;
		}
		
	}
	
/*------------------------------------------------------------------------*/	
	
		public function getDistinctCat($idVendor)
	{
		$sql = "SELECT DISTINCT CATEGORY FROM sfk_produit_vm WHERE VENDOR_ID= :VENDOR_ID";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		
		$stmt -> execute();
		
		return $stmt -> fetchAll();
	}
	
	/**
	 * Récupère tous les produits d'une catégorie pour un vendeur donné
	 */
	public function getAllProdByCategVendor($cat, $idVendor)
	{	
		$sql = "SELECT * FROM sfk_produit_vm WHERE VENDOR_ID = :VENDOR_ID AND CATEGORY LIKE :CATEGORY";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);	
	}
	
	/**
	 * récupère les produits déjà associées pour un vendeur et sa catégorie
	 */
	public function getCountAssocProdByCatVendor($catCli, $idVendor)
	{
		$sql = "SELECT COUNT(*) as count FROM sfk_produit_vm as pm
				INNER JOIN sfk_assoc_product as ap 
				ON pm.VENDOR_ID = ap.VENDOR_ID AND pm.ID_PRODUCT = ap.id_produit_ecommercant
				WHERE pm.VENDOR_ID = :VENDOR_ID AND pm.CATEGORY LIKE :CATEGORY";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $catCli);
		$stmt -> execute();
		return $stmt -> fetch();
			
	}
	
	/**
	 * Renvoie un produit Virtuemart pour un vendeur donné
	 */
	public function getProduct($vendorID, $idProduct)
	{
		$sql = "SELECT * FROM sfk_produit_vm WHERE VENDOR_ID = :VENDOR_ID AND ID_PRODUCT = :ID_PRODUCT";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> bindValue(':ID_PRODUCT' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		return $this -> getAllPrepa($stmt);
	}
	
	public function getAllUnassocProducts($cat, $idVendor, $brand=null, $priceMin=null, $priceMax=null, $search="", $context="")
	{
		$sql = "SELECT * FROM sfk_produit_vm as p
				WHERE p.VENDOR_ID = :VENDOR_ID
				";
				
		if($cat != ""){
			$sql.=" AND p.CATEGORY LIKE :CATEGORY ";
		}		
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= "AND ID_PRODUCT NOT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) 
					AND p.refus = 0";
		}
				
		if($context == "refused"){
			$sql.= " AND p.refus = 1 ";
		} 
		
		if($context == "assoc"){
			$sql.= "AND ID_PRODUCT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) ";
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, REFERENCE_PRODUCT, EAN) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$sql .= " ORDER BY NAME_PRODUCT ";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		if($cat != ""){
			$stmt -> bindValue(":CATEGORY", $cat."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		
		return $this->getAllPrepa($stmt);
	}
	
	
	/**
	 * Remonte tous les attributs pour une catégorie donnée de produits appartenant
	 * à un vendeur
	 */
	public function getAllAttrFromVendorCateg($cat, $idVendor)
	{
		$sql =" SELECT DISTINCT a.ID_ATTR, a.LABEL_ATTR FROM sfk_attribut_vm as a
			    INNER JOIN sfk_produit_attribut_vm as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_vm as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	
	/**
	 * Ne remonte que les 3 attributs les plus utilisés pour une catégorie et un vendeur donné
	 */
	public function getBestAttrFromVendorCateg($cat, $idVendor)
	{
		$sql =" SELECT COUNT(*) as count, a.ID_ATTR, a.LABEL_ATTR FROM sfk_attribut_vm as a
			    INNER JOIN sfk_produit_attribut_vm as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_vm as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY
				GROUP BY ID_ATTR
				ORDER BY count DESC
				LIMIT 3";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	public function getAllDisctValueForAttrFromVendorCateg($cat, $idVendor, $idAttr)
	{
		$sql =" SELECT DISTINCT TRIM(pa.VALUE) as VALUE FROM sfk_attribut_vm as a
			    INNER JOIN sfk_produit_attribut_vm as pa ON a.ID_ATTR = pa.ID_ATTR
				INNER JOIN sfk_produit_vm as p ON pa.ID_PRODUCT = p.ID_PRODUCT
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY
				AND a.ID_ATTR = :ID_ATTR 
				ORDER BY VALUE ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		
		
		return $this->getAllPrepa($stmt);
	}
	
	public function getAllDistinctManufFromCatVendor($cat, $idVendor)
	{

		$sql =" SELECT DISTINCT p.MANUFACTURER FROM sfk_produit_vm as p
				WHERE p.VENDOR_ID = :VENDOR_ID 
				AND p.CATEGORY LIKE :CATEGORY
				AND MANUFACTURER <> ''
				ORDER BY MANUFACTURER ASC";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":CATEGORY", $cat);

		return $this->getAllPrepa($stmt);
	}
	
	public function verifAttrForProduct($idVendor, $idProduct, $idAttr, $valAttr)
	{
		$sql = "SELECT * FROM sfk_produit_attribut_vm 
				WHERE 	ID_PRODUCT = :ID_PRODUCT 
				AND 	VENDOR_ID  = :VENDOR_ID 
				AND 	ID_ATTR    = :ID_ATTR
				AND 	VALUE      = :VALUE ";
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct); 
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		$stmt -> bindValue(":VALUE", $valAttr);
		
		$stmt -> execute();
				
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;		
	}
	
	public function getAllAttrFromProduct($idProduct, $vendorId)
	{
		$sql = "SELECT pa.VALUE, a.LABEL_ATTR, a.ID_ATTR FROM sfk_produit_attribut_vm as pa 
				INNER JOIN sfk_attribut_vm as a ON a.ID_ATTR = pa.ID_ATTR 
				WHERE pa.ID_PRODUCT = :ID_PRODUCT 
				AND pa.VENDOR_ID = :VENDOR_ID"; 
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);  
		
		return $this->getAllPrepa($stmt);
	}
	
	public function countAllProductInCat($cat, $vendorId, $context="all")
	{
		$sql = "SELECT count(*) as count FROM sfk_produit_vm WHERE CATEGORY LIKE CONCAT(:categories, '%') 
				AND VENDOR_ID = :VENDOR_ID";
		
		if($context == "refused"){
			$sql.= " AND refus = 1 ";
		}
		
		if($context == "assoc"){
			$sql.= " AND ID_PRODUCT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) ";
		}
		
		//Non associé, non refusé		
		if($context == "todo"){
			$sql.= " AND ID_PRODUCT NOT IN 
					(SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID ) 
					AND refus = 0";
		}
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":categories", $cat);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);
		$stmt -> execute();
		
		$res = $stmt -> fetch();
		$count = $count + $res['count'];
		
		return $count;
	}

	public function setRefusProduct($vendorId, $idProduct, $refus)
	{
		$sql = "UPDATE sfk_produit_vm SET refus = :refus
				WHERE ID_PRODUCT = :ID_PRODUCT 
				AND VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		$stmt -> bindValue(":VENDOR_ID", $vendorId);
		$stmt -> bindValue(":refus", $refus);
		
		$stmt -> execute();
	}		
	/*------------------------------------En attente d'exportation ou de mise a jour------------------------------------------------*/
    /**
	* Préparation de la requete qui va insérer les données dans la table sfk_product_ecommercant_stock_prix
	* cette Table correspond Ã  l'association entre vendeur, un produit Avahis type , la réference de ce produit
	* dans le Thelia du vendeur ainsi que les différents prix (promotions) et le stock
	* C'est donc l'insertion représentant les donnÃ©es juste avant leur transfert vers Avahis marketplace
	*/
	public function prepareStmtProdEcommercant($vendorID) {
			
		$sql = "UPDATE sfk_product_ecommercant_stock_prix SET QUANTITY = 0 WHERE VENDOR_ID = :VENDOR_ID";
		//ID DU VENDEUR DYNAMIQUE BESOIN TRACKING
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':VENDOR_ID', $vendorID);
		$stmt -> execute();	
		$sql = "INSERT INTO `sfk_product_ecommercant_stock_prix` 
							(`id_produit`, `VENDOR_ID`, `ref_ecommercant`, `PRICE_PRODUCT`, `PRICE_TTC` , `QUANTITY`, `PRICE_HT`,
							 `REDUCTION_FROM`, `REDUCTION_TO`, `PRICE_REDUCTION`, `POURCENTAGE_REDUCTION`,`ETAT_PROMO`, `ETAT`,`date_create`)
							 
							VALUES (:id_produit, :VENDOR_ID, :ref_ecommercant, :PRICE_PRODUCT, :PRICE_TTC , :QUANTITY, :PRICE_HT,
							 :REDUCTION_FROM, :REDUCTION_TO, :PRICE_REDUCTION, :POURCENTAGE_REDUCTION, :ETAT_PROMO , :ETAT, NOW() )
							 
							ON DUPLICATE KEY UPDATE
								ref_ecommercant 		=VALUES(ref_ecommercant),
								PRICE_PRODUCT 			=VALUES(PRICE_PRODUCT),
								PRICE_TTC				=VALUES(PRICE_TTC),
								QUANTITY 				=VALUES(QUANTITY),
								PRICE_HT 				=VALUES(PRICE_HT),
								REDUCTION_FROM 			=VALUES(REDUCTION_FROM),
								REDUCTION_TO 			=VALUES(REDUCTION_TO),
								PRICE_REDUCTION 		=VALUES(PRICE_REDUCTION),
								POURCENTAGE_REDUCTION 	=VALUES(POURCENTAGE_REDUCTION),
								ETAT_PROMO				=VALUES(ETAT_PROMO),
								ETAT 					=VALUES(ETAT)";

		$this -> stmtProdEcommercant = $this -> db -> prepare($sql);
	}
	/**
	 * Vérifie si la valeur test est null
	 * @param String $valTest 
	 * @return String retourne sa valeur ou nc si elle est null
	 */
	public function verifStringNC($valTest){
			
		if($valTest!=null){
			return $valTest;
		}
		else{
			return "nc";
		}
		
	}
	/**
	 * Vérifie si la valeur test est 0
	 * @param String $valTest 
	 * @return String retourne sa valeur ou nc si elle est null
	 */
	public function verifNumNC($valTest){
			
		if($valTest>0){
			return $valTest;
		}
		else{
			return '';
		}
		
	}
	/*--------------------------------------------------------------------------------------*/
		/**
	 * Vérifie l'unité de poid et la retourne en Kg
	 * @param String $valRef 
	 * @return String retourne sa en Kg ou 0
	 */
	public function verifUnit($valRef,$valUnit){
			
		$valUnit = trim(strtolower($valUnit)) ;
		if($valUnit == "g"){
			return ($valRef/1000);
		}
		elseif ($valUnit == "kg"){
			return $valRef;
		}
		else{
			return 0;
		}
	}
	public function getAllAssocProds($idVendor, $categ = "", $brand=null, $priceMin=null, $priceMax=null, $search=null)
	{
		$sql = "SELECT * FROM sfk_produit_vm as p
				WHERE p.VENDOR_ID = :VENDOR_ID
				AND
				ID_PRODUCT IN (SELECT id_produit_ecommercant FROM sfk_assoc_product as ap where ap.VENDOR_ID = :VENDOR_ID )
				";
				
		if($categ!= ""){
			$sql .= " AND CATEGORY LIKE :CATEGORY ";
		} 	
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, REFERENCE_PRODUCT, EAN) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$stmt = $this -> prepare($sql); 
		
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		
		if($categ!= ""){
			$stmt -> bindValue(":CATEGORY", $categ."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		return $this->getAllPrepa($stmt);
	}
	
	public function getAllRefusedProds($idVendor, $categ = "", $brand=null, $priceMin=null, $priceMax=null, $search=null)
	{
		$sql = "SELECT * FROM sfk_produit_vm as p
				WHERE p.VENDOR_ID = :VENDOR_ID
				AND refus = 1
				";
				
		if($categ!= ""){
			$sql .= " AND CATEGORY LIKE :CATEGORY ";
		} 	
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND MANUFACTURER = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
		
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(PRICE_PRODUCT AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(NAME_PRODUCT, DESCRIPTION, REFERENCE_PRODUCT, EAN) AGAINST (:NAME_PRODUCT) ";	
		}
		
		$stmt = $this -> prepare($sql); 
		
		$stmt -> bindValue(":VENDOR_ID", $idVendor);
		
		if($categ!= ""){
			$stmt -> bindValue(":CATEGORY", $categ."%");
		}
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		return $this->getAllPrepa($stmt);
	}
	
}

?>