<?php

class BusinessSoukeoModel extends sdb {

	protected $stmtAttrByCatId;
	protected $stmtAttrDynaByProd;
	protected $stmtCatalogProduct;
	protected $stmtAttr;
	protected $stmtVendor ;
    protected $stmtVendorUpdate;
	protected $stmteditProd ;
	protected $stmtLocalImg ;
	protected $stmtLocalSmallImg;
	protected $stmtLocalThumbnail;
	protected $stmtGetProdGrAssoc ;
	protected $stmtUpdateImageSfkCp;
	protected $stmtMetaKeyword;
	protected $stmtMetaDescrip;
	protected $stmtMetaTitle;
	protected $stmtManufacturer;
    protected $stmtLivraisonType;

	
	public function getAllCat() {

		$sql = 'SELECT cat_id, cat_label, cat_data FROM sfk_categorie';
		return $this -> getall($sql);

	}
	
	public function getAllCatInfo()
	{
		$sql = 'SELECT * FROM sfk_categorie WHERE cat_active = 1 ORDER BY `cat_level` ASC';
		return $this -> getall($sql);
	}
	
	public function countProdInCat($cat_id)
	{
		$sql = "SELECT COUNT(categories) FROM sfk_catalog_product WHERE categories = $cat_id ";
		return $this -> getOne($sql);
	}
	
	public function getAllCatByLabel($label)
	{
		$sql="SELECT cat_id,cat_label FROM sfk_categorie WHERE cat_label LIKE :label ORDER BY cat_label ASC LIMIT 0,40";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindvalue(':label','%'.$label.'%',PDO::PARAM_STR);
		
		return $this->getAllPrepa($stmt);
	}

    public function getAllAttrByCategoriesProduct($categories)
    {
        $sql="SELECT id_attribut, label_attr FROM sfk_attribut 
              WHERE id_attribut IN (SELECT DISTINCT id_attribut FROM sfk_product_attribut pa
                                    INNER JOIN sfk_catalog_product cp ON pa.id_produit = cp.id_produit 
                                    WHERE cp.categories = :categories AND active = 1) ORDER BY label_attr ASC ";
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindvalue(':categories',$categories);
        
        return $this->getAllPrepa($stmt);
    }
    
	public function checkIfCatLastLevel($cat_id)
	{
		$sql = "SELECT cat_id FROM sfk_categorie WHERE parent_id = $cat_id";
		
		if($this -> getOne($sql)){
			return false;
		}else{
			return true;
		}
	}
	
	public function PrepAttrByCatId() {
		$sql = "SELECT id_attribut,cat_id, code_attr, label_attr FROM sfk_attribut WHERE cat_id = ? ORDER BY id_attribut";
		$this -> stmtAttrByCatId = $this -> prepare($sql);
	}

	public function GetAttrByCatId($catid) {
		$null = null;

		$this -> stmtAttrByCatId -> bindParam(1, $catid);

		$this -> stmtAttrByCatId -> execute();
		return ($rs = $this -> stmtAttrByCatId -> fetchall(PDO::FETCH_ASSOC)) ? $rs : array();
	}

    
	/**
	 * exemple en CREATE :
	 * 	SELECT * 
		FROM sfk_catalog_product
		WHERE categories =  '1039'
		AND CAST( weight AS DECIMAL( 10, 5 ) ) >0
		AND DATE_CREATE > ( 
		SELECT MAX( start_date ) 
		FROM c_batch_log
		WHERE class =  'BatchMassImportProduitCreate' ) 
		AND sku
		IN (
			SELECT DISTINCT id_produit
			FROM sfk_product_ecommercant_stock_prix )
	 */
	public function getAllProdByCatId($catid , $date=false, $limit = false , $mode = "update") {
		$sql = "select * from sfk_catalog_product where categories ='$catid' and CAST(weight AS DECIMAL(10,5)) > 0  ";
		
		if($mode == "create"){
			if ($date){
				$add .= "AND  ".
						" DATE_CREATE > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitCreate'  )";
				
			}
		}
		else{
			if ($date){
				$add .= "AND  ".
						" DATE_UPDATE > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitUpdate'  )";
				
			}	
		}
		
		
		if ($limit){
			$add .= "AND  ".
					" sku in  (select distinct id_produit from sfk_product_ecommercant_stock_prix  where QUANTITY >0 OR vendor_id <> 24 )";
			
		}
		
		//echo $sql.$add;
		return $this -> getall($sql.$add);

	}


	public function getAllProdByCatIdAndVendor($catid , $vendor_id, $date = false, $mode = "update") {
		
		$sql = "SELECT * FROM sfk_catalog_product cp  
				WHERE categories ='$catid' 
				AND sku IN  (SELECT DISTINCT id_produit FROM sfk_product_ecommercant_stock_prix WHERE VENDOR_ID = '$vendor_id') ";
		
		if($date){
			if($mode == "create"){
				$sql .= " AND cp.date_create > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitCreate'  )";
			}elseif($mode == "update"){
				$sql .= " AND cp.date_update > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitUpdate'  )";
			}
		}	
		
		return $this -> getall($sql.$add);

	}
	
	public function prepAttrDynaByProd(){
		$sql = "SELECT * FROM sfk_product_attribut WHERE id_produit = ? ";
		$this -> stmtAttrDynaByProd =  $this->prepare($sql) ;
	}

	public function getAttrDynaByProd($idproduit) {
		$this -> prepAttrDynaByProd();
		$this -> stmtAttrDynaByProd  -> bindParam(1, $idproduit ? $idproduit : $null);
		return $this -> getAllPrep($this -> stmtAttrDynaByProd );

	}

	public function getAttrDynaByProdAndAttr($idproduit, $idattribut) {
		$sql = "SELECT value as val  FROM sfk_product_attribut WHERE id_produit = ? AND id_attribut = ? ";
		
				$this -> AttrDynaByProdAndAttr =  $this->prepare($sql) ;
		$this -> AttrDynaByProdAndAttr -> bindParam(1, $idproduit ? $idproduit : $null);
		$this -> AttrDynaByProdAndAttr -> bindParam(2, $idattribut ? $idattribut : $null);
		return $this -> getOnePrep($this -> AttrDynaByProdAndAttr);
		
		
		//return $this -> getOne($sql);

	}
	
	public function getAttrByCatAndCode($codeAttr, $cat)
	{
		$sql = "SELECT id_attribut,cat_id, code_attr, label_attr  
				FROM sfk_attribut 
				WHERE cat_id = :cat_id 
				AND code_attr = :code_attr 
				ORDER BY id_attribut";
				
		$stmt = $this -> prepare($sql);
		
		//$codeAttr = str_replace(" ", "_", $codeAttr);
		//$codeAttr = $this -> stripAccents($codeAttr);
		//$codeAttr = strtoupper($codeAttr);
		
		$stmt -> bindValue(':code_attr', $codeAttr);
		$stmt -> bindValue(':cat_id', $cat);
		
		$stmt -> execute();
		
		if ($res = $stmt -> fetch()){
			return $res['id_attribut'];
		}else{
			return false;
		}
	}
	
	public function prepInsertCat(){
			$sql = "INSERT INTO sfk_categorie (
					cat_id, cat_label  , parent_id , cat_position , cat_level, cat_active )
					VALUES ( ?, ?, ?, ?, ?, 1 )  ON DUPLICATE KEY UPDATE
					cat_label = VALUES(cat_label),
					parent_id = VALUES(parent_id),
					cat_position = VALUES(cat_position),
					cat_level = VALUES(cat_level), 
					cat_active = VALUES(cat_active) 
					
										 ";
			$this->stmtInsertCat = $this->prepare($sql) ;
	}
	
	public function AddCategory($V){
		$null = null;
				
		$this -> stmtInsertCat  -> bindParam(1, $V['cateory_id'] ? $V['cateory_id'] : $null);
		$this -> stmtInsertCat  -> bindParam(2, $V['name']  ? $V['name']  : $null);
		$this -> stmtInsertCat  -> bindParam(3, $V['parent_id'] ? $V['parent_id'] : $null);
		$this -> stmtInsertCat  -> bindParam(4, $V['position'] ? $V['position'] : $null);
		$this -> stmtInsertCat  -> bindParam(5, $V['level'] ? $V['level'] : $null);
		return $this -> stmtInsertCat  -> execute();
	
	}
	
	
	protected function addProductCategorie($id_product, $cat_id) {
		$null = null;
		$this -> stmtProductCat -> bindParam(1, $cat_id ? $cat_id : $null);
		$this -> stmtProductCat -> bindParam(2, $id_product ? $id_product : $null);
		$this -> stmtProductCat -> execute();
	}

	protected function prepProductCategorie() {
		$sql = "INSERT INTO sfk_cat_product (cat_id, id_produit) VALUES ( ?, ?) ";
		$this -> stmtProductCat = self::$db -> prepare($sql);

	}

	public function prepInsertValAttr() {
		$insValattr = "INSERT INTO sfk_product_attribut  (id_produit, id_attribut, value )
					 				VALUES (?,?,?) ON DUPLICATE KEY UPDATE
					 				value = VALUES(value)
					 
					 ";
		$this -> stmtValAttr = self::$db -> prepare($insValattr);
	}

	public function getIdProduct($sku) {
		$sql = "SELECT id_produit from sfk_catalog_product where sku = '$sku' ";
		return $this -> getone($sql);
	}

	public function addRowValAttr($V) {
		$null = null;
		$this -> stmtValAttr -> bindParam(1, $V['id_produit'] ? $V['id_produit'] : $null);
		$this -> stmtValAttr -> bindParam(2, $V['id_attribut'] ? $V['id_attribut'] : $null);
		$this -> stmtValAttr -> bindParam(3, $V['value'] ? $V['value'] : $null);

		$this -> stmtValAttr -> execute();

	}

	public function prepInsertSfkCp() {

		$sql = "INSERT INTO `sfk_catalog_product` (
					`sku`,`categories`,`name`,`description`,`short_description`,`price`,
					`weight`,`country_of_manufacture`,`meta_description`,`meta_keyword`,
					`meta_title`,`image`,`small_image`,`thumbnail`,`manufacturer`,`dropship_vendor`, `date_create`)
				VALUES ( ?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?,?,NOW() )
				ON DUPLICATE KEY UPDATE 
					categories	=VALUES(categories),
					name		=VALUES(name),
					description	=VALUES(description),
					short_description=VALUES(short_description),
					price		=VALUES(price),
					weight		=VALUES(weight),
					country_of_manufacture	=VALUES(country_of_manufacture),
					meta_description=VALUES(meta_description),
					meta_keyword	=VALUES(meta_keyword),
					meta_title		=VALUES(meta_title),
					image			=VALUES(image),
					small_image		=VALUES(small_image),
					thumbnail		=VALUES(thumbnail),
					manufacturer    =VALUES(manufacturer),
					dropship_vendor =VALUES(dropship_vendor)
					
				";
		$this -> stmtCatalogProduct = $this -> prepare($sql);

	}

	public function addRowCatProduct($V, $vendor_ID = 24) {
			
		$metadescrip = $this -> getMetaDescrip($V);
		$metakeyword = $this -> getMetaKeyword($V);
		$metatitle   = $this -> getMetaTitle($V);
		
		$null = null;
		if(!$V['sku'])return false;
		$this -> stmtCatalogProduct -> bindParam(1, $V['sku'] ? $V['sku'] : $null);
		$this -> stmtCatalogProduct -> bindParam(2, $V['categories'] ? $V['categories'] : $null);
		$this -> stmtCatalogProduct -> bindParam(3, $V['name'] ? $V['name'] : $null);
		$this -> stmtCatalogProduct -> bindParam(4, $V['description'] ? $V['description'] : $V['short_description'] );
		$this -> stmtCatalogProduct -> bindParam(5, $V['short_description'] ? $V['short_description'] : $V['manufacturer'] . "-" . $V['name'] );
		$this -> stmtCatalogProduct -> bindParam(6, $V['price'] ? str_replace(",", ".", $V['price']) : $null);
		$this -> stmtCatalogProduct -> bindParam(7, $V['weight'] ? str_replace(",", ".", $V['weight'] ) : $null);
		$this -> stmtCatalogProduct -> bindParam(8, $V['country_of_manufacture'] ? $V['country_of_manufacture'] : $null);
		$this -> stmtCatalogProduct -> bindParam(9, $metadescrip);
		$this -> stmtCatalogProduct -> bindParam(10, $metakeyword);
		$this -> stmtCatalogProduct -> bindParam(11, $metatitle);
		$this -> stmtCatalogProduct -> bindParam(12, $V['image'] ? $V['image'] : $null);
		$this -> stmtCatalogProduct -> bindParam(13, $V['small_image'] ? $V['small_image'] : $V['image']);
		$this -> stmtCatalogProduct -> bindParam(14, $V['thumbnail'] ? $V['thumbnail'] : $V['small_image']);
		$this -> stmtCatalogProduct -> bindParam(15, $V['manufacturer'] ? ucfirst(strtolower($V['manufacturer'])) : $null);
		$this -> stmtCatalogProduct -> bindParam(16, $vendor_ID ? $vendor_ID : $null);

		$this -> stmtCatalogProduct -> execute();
		return $this -> lastInsertId();
	}
	
	public function addProductFromMappingFields($mappingStatic, $prod, $vendor_ID)
	{
		$null = null;
		$this -> stmtCatalogProduct -> bindParam(1, $prod[$mappingStatic['sku']] ? $prod[$mappingStatic['sku']] : $null);
		$this -> stmtCatalogProduct -> bindParam(2, $mappingStatic['categories'] ? $mappingStatic['categories'] : $null);
		$this -> stmtCatalogProduct -> bindParam(3, $prod[$mappingStatic['name']] ? $prod[$mappingStatic['name']]  : $null);
		$this -> stmtCatalogProduct -> bindParam(4, $prod[$mappingStatic['description']] ?  $prod[$mappingStatic['description']] : $null);
		$this -> stmtCatalogProduct -> bindParam(5, $prod[$mappingStatic['short_description']] ? $prod[$mappingStatic['short_description']] : $null);
		$this -> stmtCatalogProduct -> bindParam(6, $prod[$mappingStatic['price']] ? str_replace(",", ".", $prod[$mappingStatic['price']]) : $null);
		$this -> stmtCatalogProduct -> bindParam(7, $prod['WEIGHT'] ? str_replace(",", ".", $prod['WEIGHT']) : $null);
		$this -> stmtCatalogProduct -> bindParam(8, $prod[$mappingStatic['country_of_manufacture']] ?  $prod[$mappingStatic['country_of_manufacture']] : $null);
		$this -> stmtCatalogProduct -> bindParam(9, $prod[$mappingStatic['meta_description']] ? $prod[$mappingStatic['meta_description']] : $null);
		$this -> stmtCatalogProduct -> bindParam(10, $prod[$mappingStatic['meta_keyword']] ? $prod[$mappingStatic['meta_keyword']] : $null);
		$this -> stmtCatalogProduct -> bindParam(11, $prod[$mappingStatic['meta_title']] ? $prod[$mappingStatic['meta_title']] : $null);
		$this -> stmtCatalogProduct -> bindParam(12, $prod[$mappingStatic['image']] ? $prod[$mappingStatic['image']] : $null);
		$this -> stmtCatalogProduct -> bindParam(13, $prod[$mappingStatic['small_image']] ? $prod[$mappingStatic['small_image']] : $null);
		$this -> stmtCatalogProduct -> bindParam(14, $prod[$mappingStatic['thumbnail']] ? $prod[$mappingStatic['thumbnail']] : $null);
		$this -> stmtCatalogProduct -> bindParam(15, $prod[$mappingStatic['manufacturer']] ? ucfirst(strtolower($prod[$mappingStatic['manufacturer']])) : $null);
		$this -> stmtCatalogProduct -> bindParam(16, $vendor_ID ? $vendor_ID : $null);
		
		$this -> stmtCatalogProduct -> execute();
		return $this -> lastInsertId();
	}

	public function getAllDistinctVendors()
	{
		$sql = "SELECT DISTINCT VENDOR_ID FROM `sfk_product_ecommercant_stock_prix` WHERE VENDOR_ID <> 0";
		
		$stmt = $this -> prepare($sql);
		
		return  $this -> getAllPrepa($stmt);
	} 
	
	public function getAllStockPriceEcom($vendor_id=null)
	{
		$sql = "SELECT * FROM sfk_product_ecommercant_stock_prix ";
		
		if($vendor_id) 
			$sql.= " WHERE VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		
		if($vendor_id)
			$stmt -> bindValue(':VENDOR_ID', $vendor_id);	
		
		return $this->getAllPrepa($stmt);
	}
    
    public function getAllStockPriceEcomREC()
    {
        $sql = "SELECT sku, price FROM sfk_catalog_product ";
        
        $stmt = $this -> prepare($sql);  
             
        return $this->getAllPrepa($stmt);
    }
	
	public  function formatarr($rs) {
		$arr = array();
		foreach ($rs as $R) {
			$arr[] = $R['ID'];
		}
		return $arr;
	}

	public function prepAttribut() {
		$sql = "INSERT INTO sfk_attribut ( cat_id , code_attr, label_attr ) 
				values ( ?, ? , ? )";
		$this -> stmtAttr = $this -> prepare($sql);

	}

	public function addAttribut($V) {
		$null = null;
		$this -> stmtAttr -> bindParam(1, $V['cat_id'] ? $V['cat_id'] : $null);
		$this -> stmtAttr -> bindParam(2, $V['code_attr'] ? $V['code_attr'] : $null);
		$this -> stmtAttr -> bindParam(3, $V['label_attr'] ? $V['label_attr'] : $null);
		$this -> stmtAttr -> execute();
        
        
        $sql = "SELECT id_attribut FROM sfk_attribut WHERE cat_id = ".$V['cat_id']." AND code_attr = '".$V['code_attr']."' ";
        $id = $this -> getOne($sql);    
        
        
		return $id;
		
	}
	/**
	 * fonction permettant d'avoir tous les id vendeur ainsi que leur état actif ou pas
	 */
	public function getAllVendor()
	{
		$sql = "SELECT VENDOR_ID,ACTIVE FROM sfk_vendor ";
		// WHERE VENDOR_ID <> 61";
		$result = $this->getAll($sql);
		
		return $result;
	}
	
	
	/**
	 * fonction permettant d'avoir tous les id grossistes ainsi que leur état actif ou pas
	 */
	public function getAllGrossiste()
	{
		$sql = "SELECT * FROM sfk_grossiste WHERE ACTIVE = 1";
		$result = $this->getAll($sql);
		
		return $result;
	}

	/**
	 * fonction permettant d'avoir tous les id grossistes ainsi que leur état actif ou pas
	 */
	public function getGrossiste($id_grossiste)
	{
		$sql = "SELECT * FROM sfk_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID";
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', $id_grossiste);

		return $this -> getAllPrepa($stmt);
	}
			
	/**
	 * Cette fonction permet de vérifier si les informations entre les tables
	 * sfk_vendor et sfk_tracking_vendor concordent et si c'est le cas les informations 
	 * de la table sfk_tracking_vendor à propos de ce vendeur sont renvoyés.
	 * @param vendorID l'id du vendeur
	 * @return $result array le résultat de la requete, les informations de tracking
	 */
	public function getInfoTracking($vendorID)
	{
		$sql = "SELECT * FROM sfk_tracking_vendor WHERE VENDOR_ID =".$vendorID." ORDER BY DATE_INSERT DESC LIMIT 1";
		
		if($infoTracking = $this -> getRow($sql)) {
			return $infoTracking;
		}
		else {
			return false;
		}
	}
	
    public function getInfoCsv($vendor_id)
    {
        $sql = "SELECT * FROM skf_vendor_csv WHERE vendor_id = :vendor_id";
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(':vendor_id', $vendor_id);

        return $this -> getAllPrepa($stmt);
    }	
	/**
	 * Fonction récursive permettant de retourner sous forme de chaine de caractère 
	 * l'arborescence complète de la catégorie. On va chercher si la catégorie a un parent
	 * pour en chercher le parent, tant que le parent n'a pas l'id "2" qui correspond à 
	 * l'id de la catégorie Racine.
	 * @author 	Philippe_LA
	 * @param 	cat_id Int 		l'id de la catégorie 
	 * @param 	result String 	Le résultat sous forme de chaine de caractère
	 * @return 	result String 	Le résultat sous forme de chaine de caractère
	 */
	public function getStrCategB($cat_id, $result="")
	{
		$sql = "SELECT parent_id, cat_label FROM sfk_categorie WHERE cat_id = $cat_id";
		
		$info = $this -> getRow($sql);

		//2 est la catégorie mère point d'arret de la récursive
		if($info['parent_id'] != 2 && $info != null) {
			$result = $this -> getStrCategB($info['parent_id'], $info['cat_label'] . "/" . $result) ;
		}
		else {
			$result = $info['cat_label'] . "/" . $result;
		}
		
		return $result;
	}
	
	/**
	 * Fonction récursive permettant de retourner sous forme de chaine de caractère 
	 * l'arborescence complète de la catégorie. On va chercher si la catégorie a un parent
	 * pour en chercher le parent, tant que le parent n'a pas l'id "2" qui correspond à 
	 * l'id de la catégorie Racine.
	 * @author 	Philippe_LA
	 * @param 	cat_id Int 		l'id de la catégorie 
	 * @param 	result String 	Le résultat sous forme de chaine de caractère
	 * @return 	result String 	Le résultat sous forme de chaine de caractère
	 */
	public function getStrCateg($cat_id, $result="")
	{
		$sql = "SELECT parent_id, cat_label FROM sfk_categorie WHERE cat_id = $cat_id";
		
		$info = $this -> getRow($sql);

		//2 est la catégorie mère point d'arret de la récursive
		if($info['parent_id'] != 2 && $info != null) {
			$result = $this -> getStrCateg($info['parent_id'], $info['cat_label'] . " > " . $result) ;
		}
		else {
			$result = $info['cat_label'] . " > " . $result;
		}
		
		return $result;
	}
	
	
	/**
	 * Permet d'obtenir tous les attributs présents pour une catégorie pour tous les produits
	 * de cette catégorie.
	 */
	public function getAllAttrByCat($cat_id)
	{
		$sql = "SELECT label_attr FROM sfk_attribut WHERE cat_id = $cat_id ";
		$result = $this->getAll($sql);
		
		return $result;
	}
	
	public function getAllAttrByProd($idProd)
	{
		$sql = "SELECT a.label_attr ,pa.value FROM `sfk_product_attribut` as pa 
				INNER JOIN sfk_attribut as a ON a.id_attribut = pa.id_attribut
				WHERE pa.id_produit = $idProd AND pa.active = 1";
				
		$result = $this->getAll($sql);
		
		$aAttrVal = array();
		
		foreach ($result as $row) {
			$aAttrVal[$row['label_attr']] = $row['value'];
		}
		
		return $aAttrVal;
	}
	
	/**
	 * Retourne toutes les catégories possédant des produits
	 * 
	 * Exemple pour Create : 
	 * 
	 * 	SELECT DISTINCT categories
		FROM sfk_catalog_product
		WHERE DATE_CREATE > ( 
		SELECT MAX( start_date ) 
		FROM c_batch_log
		WHERE class =  'BatchMassImportProduitCreate' ) 
		AND weight >0
		AND sku
		IN (
		SELECT DISTINCT id_produit
		FROM sfk_product_ecommercant_stock_prix
		)
	 */
	public function getAllCatWithProducts($limit=false , $date = false, $mode = "update", $specif_cat = "")
	{
		$sql = "SELECT DISTINCT categories FROM sfk_catalog_product cp ";
        
        $where = "";
        
        if($limit){
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            
            $where .= " weight > 0 ";
            
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            
            $where .= " sku in  (select distinct id_produit from sfk_product_ecommercant_stock_prix) ";
        }
        
        if($date){
            
            if($mode == "create"){
                
                if ($where == "") $where = " WHERE ";
                else $where .= " AND ";
                
                $where .= " cp.date_create > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitCreate') ";
            }
            elseif($mode == "update"){
                
                if ($where == "") $where = " WHERE ";
                else $where .= " AND ";
                
                $where .= " cp.date_update > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitUpdate') ";
            }
        }
		
        if($specif_cat){
            
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            
            $where .= " categories = $specif_cat " ;
        }
            
		
		return $result = $this->getAll($sql.$where);
	}
	
	public function getAllCatSoldByEcom($vendor_id, $date = false, $mode = "update", $specif_cat = "" )
	{
		$sql = "SELECT DISTINCT categories FROM sfk_catalog_product cp 
		        INNER JOIN sfk_product_ecommercant_stock_prix sp ON sp.id_produit = cp.sku ";
                
		$where = "";
		
		if($date){
			if($mode == "create") {
                if ($where == "") $where = " WHERE ";
                else $where .= " AND ";
				$where .= " cp.date_create > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitCreate')";
			}	
			elseif($mode == "update") {
                if ($where == "") $where = " WHERE ";
                else $where .= " AND ";
				$where .= " cp.date_update > (SELECT max(start_date) from c_batch_log where class='BatchMassImportProduitUpdate')";		
			}
		}
        
        if($specif_cat){
            
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            
            $where .= " cp.categories = $specif_cat " ;
        }
                
        if ($where == "") $where = " WHERE ";
        else $where .= " AND ";
        
        $where .= " sp.VENDOR_ID = :VENDOR_ID ";
        
		$stmt = $this->prepare($sql.$where);
		
		$stmt -> bindValue(':VENDOR_ID', $vendor_id);
		
		return $this->getAllPrepa($stmt);

	}
	
	
	public function getAllAttrByProdFromCat($cat_id)
	{

		$sql ="SELECT distinct a.id_attribut, a.label_attr, a.code_attr
                FROM sfk_attribut as a
                INNER JOIN sfk_product_attribut as pa ON a.id_attribut = pa.id_attribut
                INNER JOIN sfk_catalog_product as cp ON pa.id_produit = cp.id_produit
                WHERE cp.categories = $cat_id and active = 1 
                AND a.cat_id = $cat_id
                ORDER BY label_attr
		";

		$result = $this->getAll($sql);
		return $result ;
	}
	
    public function getAllCodeAttrByProdFromCat($cat_id)
    {

        $sql ="SELECT distinct a.code_attr 
                FROM sfk_catalog_product cp
                INNER JOIN sfk_product_attribut pa ON cp.id_produit = pa.id_produit
                INNER JOIN sfk_attribut a ON a.id_attribut = pa.id_attribut
                WHERE pa.active =1 
                AND cp.categories = $cat_id and active = 1 
                AND a.cat_id = $cat_id
                ORDER BY a.code_attr
        ";

        $result = $this->getAll($sql);
        return $result ;
    }
    	
	public function getAllAttrDistFromCat($cat_id)
	{
		$sql = "SELECT DISTINCT label_attr, id_attribut FROM sfk_attribut 
		WHERE cat_id = $cat_id 
		ORDER BY label_attr ASC";
		
		$result = $this->getAll($sql);
		return $result ;
	}
	
	
	/**
	* F Retourne tous les produits associé avahis/soukflux
	* 
	*/
	public function getAllProdAssoc()
	{
		$sql = "SELECT VENDOR_ID, id_produit_ecommercant FROM sfk_assoc_product";
		
		$result = $this->getAll($sql);
		return $result;
	}
      
      
	public function getAllProducts($limit = "")
	{
		$sql = "SELECT * FROM sfk_catalog_product 
				WHERE (local_image IS NULL OR local_image = '' ) 
				AND image IS NOT NULL 
				AND image <> 'NULL' 
				AND image <> '' 
				AND dropship_vendor = 24";
		
		if($limit) $sql.= $limit;
		
		return $this->getAll($sql);
	} 
	
	public function getCountProductsInSaleByVendor($vendor_id)
	{
		$sql = "SELECT COUNT(*) FROM sfk_product_ecommercant_stock_prix 
                WHERE QUANTITY > 0 
                AND VENDOR_ID = $vendor_id";
        
        return $this->getOne($sql);
	} 
	  
	public function getStockPriceProduct($id_produit)
	{
	    $sql = "SELECT * FROM sfk_product_ecommercant_stock_prix WHERE id_produit = '$id_produit' ";
        
        
        return $this->getAll($sql);
	}  
    
	public function getAllProductsImage()
	{
		$sql = "SELECT * FROM sfk_catalog_product WHERE (local_image IS NOT NULL AND local_image <> '' AND local_image <> 'NULL')";
		
		
		return $this->getAll($sql);
	}	  
	  
	 
	public function getAllProductsManufacturer()
	{
		$sql = "SELECT * FROM `sfk_catalog_product` ";
		
		return $this->getAll($sql);
	}
	
	public function prepUpdateManufacturer()
	{
		$sql = "UPDATE sfk_catalog_product SET manufacturer = :manufacturer WHERE id_produit = :id_produit";
		
		$this -> stmtManufacturer = $this -> prepare($sql);
	}
	
	public function prepUpdateMetaKeyword()
	{
		$sql = "UPDATE sfk_catalog_product SET meta_keyword = :meta_keyword WHERE id_produit = :id_produit";
		
		$this -> stmtMetaKeyword = $this -> prepare($sql);
	}

	public function prepUpdateMetaTitle()
	{
		$sql = "UPDATE sfk_catalog_product SET meta_title = :meta_title WHERE id_produit = :id_produit";
		
		$this -> stmtMetaTitle = $this -> prepare($sql);
	}
		
	public function prepUpdateMetaDescrip()
	{
		$sql = "UPDATE sfk_catalog_product SET meta_description = :meta_description WHERE id_produit = :id_produit";
		
		$this -> stmtMetaDescrip = $this -> prepare($sql);
	}
	
	public function updateManufacturer($id, $manufacturer)
	{

		$this -> stmtManufacturer -> bindValue(':manufacturer', $manufacturer);
		$this -> stmtManufacturer -> bindValue(':id_produit', $id);
		
		return $this -> stmtManufacturer -> execute();
		
	}
	   
	public function updateMetaKeyword($id, $string)
	{

		$this -> stmtMetaKeyword -> bindValue(':meta_keyword', $string);
		$this -> stmtMetaKeyword -> bindValue(':id_produit', $id);
		
		return $this -> stmtMetaKeyword -> execute();
		
	}
	
	public function updateMetaDescrip($id, $string)
	{

		$this -> stmtMetaDescrip -> bindValue(':meta_description', $string);
		$this -> stmtMetaDescrip -> bindValue(':id_produit', $id);
		
		return $this -> stmtMetaDescrip -> execute();
		
	}
		
	public function updateMetaTitle($id, $string)
	{

		$this -> stmtMetaTitle -> bindValue(':meta_title', $string);
		$this -> stmtMetaTitle -> bindValue(':id_produit', $id);
		
		return $this -> stmtMetaTitle -> execute();
		
	}
        
    /**
	* F Cette fonction permet de vérifier si les informations entre les tables
	* sfk_assoc_product et sfk_product concordent et si c'est le cas les informations 
	* de la table sfk_product à propos de ce produit sont renvoyés.
	* @param vendorID l'id du vendeur
	* @return $result array le résultat de la requete, les informations de tracking
	*/
	public function getInfoTrackingAssoc($produit,$infoTrackingVendor)
	{
            $software = $infoTrackingVendor['SOFTWARE'];
            $idVendeur = $produit['VENDOR_ID'];
            $idProduitEcom = $produit['id_produit_ecommercant'];
            //suivant la techno utilisé on choisirat la table adéquate 
            if($software=='Prestashop'){//si prestashop
                $sql = "SELECT * FROM sfk_produit_ps WHERE VENDOR_ID ='".$idVendeur."' AND ID_PRODUCT ='".$idProduitEcom."' AND ACTIVE ='1' AND ORDER BY DATE_INSERT LIMIT 1";
                return false; // a complété
            }
            elseif ($software=='Thelia') {//si Thelia
                $sql = "SELECT * FROM sfk_produit_th WHERE VENDOR_ID ='".$idVendeur."' AND ID_PRODUCT ='".$idProduitEcom."' AND ACTIVE ='1' ORDER BY DATE_UPDATE LIMIT 1";
                // Pour la prochaine rectification $sql = "SELECT * FROM sfk_produit_th WHERE VENDOR_ID ='".$idVendeur."' AND ID_PRODUCT_SF ='".$idProduitEcom."' AND ACTIVE ='1' ORDER BY DATE_INSERT LIMIT 1";
            }
            else{}
            if($infoTrackingAssocProduit = $this->getRow($sql)){
                return $infoTrackingAssocProduit;
            }else
            return false;
	}
    /**
	* F Cette fonction permet de vérifier si les informations entre les tables
	* sfk_product_ecommercant_stock_prix et sfk_product concordent(date_update) et si c'est le cas les informations 
	* de la table sfk_product_ecommercant_stock_prix à propos de ce produit sont renvoyés.
	* @param $produit les infos sur ce produit de la table sfk_produit
	* @return $result array le résultat de la requete, les informations de tracking
	*/
	public function verifDate_upBddLocalImport($produit)
	{
            $idVendeur 			= $produit['VENDOR_ID'];
            $date_update_ecom 	= time();
            $idProduitEcom 		= $produit['ID_PRODUCT'];
            
            $sql 				= "SELECT * FROM sfk_product_ecommercant_stock_prix 
            					   WHERE VENDOR_ID ='".$idVendeur."' 
            					   AND id_produit='".$idProduitEcom."' 
            					   ORDER BY DATE_UPDATE LIMIT 1";
								   
            $infotrackingMajSP = $this -> getRow($sql);
            
            if(($date_update_ecom>$infotrackingMajSP['date_update'])||($infotrackingMajSP['date_update']==null)) {//si la date ecom est supérieur a la date table commercan_stock_prix ou si elle n'existe pas 
                return true;
            }else
                return false;
	}
	
	/**
	 * vérifie si un produit est associé
	 * @param $vendorID String l'id vendeur
	 * @param $idProduct String l'id produit
	 * @return boolean
	 */
	public function productIsAssoc($vendorID, $idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product WHERE VENDOR_ID = :VENDOR_ID AND id_produit_ecommercant = :id_produit_ecommercant";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> bindValue(':id_produit_ecommercant' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;
	}
	
	/**
	 * vérifie si un produit est associé
	 * @param $vendorID String l'id vendeur
	 * @param $idProduct String l'id produit
	 * @return boolean
	 */
	public function productIsAssocGr($grossisteID, $idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID AND id_produit_grossiste = :id_produit_grossiste";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID' , $grossisteID);
		$stmt -> bindValue(':id_produit_grossiste' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;
	}
	
	public function getProductAvAssoc($vendorID, $idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product WHERE VENDOR_ID = :VENDOR_ID AND id_produit_ecommercant = :id_produit_ecommercant";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> bindValue(':id_produit_ecommercant' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		$rs = $stmt -> fetch();
		
		$prod = $this -> getProductSfkCp($rs['id_avahis']); 
		return $prod[0];
	}
		
	public function getProdAvAssoc($vendorID, $idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product WHERE VENDOR_ID = :VENDOR_ID AND id_produit_ecommercant = :id_produit_ecommercant";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID' , $vendorID);
		$stmt -> bindValue(':id_produit_ecommercant' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		$rs = $stmt -> fetch();
		
		return $this -> getProductAlias($rs['id_avahis']);
	}
	
	public function getProdAvAssocGr($grossisteID, $idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID AND id_produit_grossiste = :id_produit_grossiste";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID' , $grossisteID);
		$stmt -> bindValue(':id_produit_grossiste' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		$rs = $stmt -> fetch();
		
		if($rs['id_avahis'])return $this -> getProductAlias($rs['id_avahis']);
		else return null;
	}

	public function getProdAvahisAssocieGrossiste($idProduct)
	{
		$sql = "SELECT id_avahis FROM sfk_assoc_product_grossiste WHERE id_produit_grossiste = :id_produit_grossiste";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit_grossiste' , $idProduct, PDO::PARAM_STR);
		$stmt -> execute() ;
		
		$rs = $stmt -> fetch();
		
		if($rs['id_avahis'])return $this -> getProductAlias($rs['id_avahis']);
		else return null;
	}
	
	
	
	public function getSkuAssocProdGr($idProduct)
	{
		$sql = "SELECT cp.sku FROM sfk_assoc_product_grossiste apg
				INNER JOIN sfk_catalog_product cp ON apg.id_avahis = cp.id_produit 
				WHERE apg.id_produit_grossiste = :id_produit_grossiste";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit_grossiste' , $idProduct, PDO::PARAM_STR);
		
		
		return $this -> getOnePrep($stmt);
	}
    
    public function getAssocProductGrossiste($id_avahis)
    {
        $sql = "SELECT * FROM sfk_assoc_product_grossiste apg
                WHERE apg.id_avahis = :id_avahis";
        $stmt = $this->prepare($sql);
        
        $stmt -> bindValue(':id_avahis' , $id_avahis, PDO::PARAM_STR);
        
        
        return $this -> getAllPrepa($stmt);
    }
	
	public function getProdAvahisAssocProdGr($idProduct)
	{
		$sql = "SELECT * FROM sfk_assoc_product_grossiste apg
				INNER JOIN sfk_catalog_product cp ON apg.id_avahis = cp.id_produit 
				WHERE apg.id_produit_grossiste = :id_produit_grossiste";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit_grossiste' , $idProduct, PDO::PARAM_STR);
		
		
		return $this -> getAllPrepa($stmt);
	}
	
	public function getValidVendor($vendorID)
	{
		$sql = "SELECT * FROM sfk_tracking_vendor as tv
				INNER JOIN sfk_vendor as v 
				WHERE v.VENDOR_ID = :VENDOR_ID ORDER BY tv.DATE_INSERT LIMIT 1";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID', $vendorID, PDO::PARAM_INT);
		$stmt -> execute();
		
		if($infoTracking = $stmt -> fetch()) {
			return $infoTracking;
		}
		else {
			return false;
		}
	}
	
	
	public function getNomVendor($id_vendor)
	{
		$sql = "SELECT VENDOR_NOM FROM sfk_vendor WHERE VENDOR_ID = :VENDOR_ID";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":VENDOR_ID", $id_vendor);
		
		return $this->getOnePrep($stmt);
	}
	
	
	public function getValidGrossiste($grossisteID)
	{
		$sql = "SELECT * FROM sfk_grossiste
				WHERE GROSSISTE_ID = :GROSSISTE_ID";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', $grossisteID, PDO::PARAM_INT);
		$stmt -> execute();
		
		if($info = $stmt -> fetch()) {
			return $info;
		}
		else {
			return false;
		}
	}
	
	
	/**
	 * F Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif de tous les ecommerçants
	 */
	public function purgeAllInactiveProduct() {
		//Si le produit a été passé en inactif chez le commercant, on l'a recu en inactif coté soukflux
		//si la mise a jour a bien eu lieu alors on va simplement supprimer les produits inactifs de cet ecommercant
		$this -> purgeTheliaInactiveProduct() ;
		$this -> purgePrestashopInactiveProduct() ;
	}
	/**
	 * F Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif dans son Thelia
	 */
	public function purgeTheliaInactiveProduct() {
		$sql = "DELETE  spe  
				FROM sfk_product_ecommercant_stock_prix spe
				INNER JOIN  sfk_produit_th sp 
				ON spe.VENDOR_ID = sp.VENDOR_ID AND spe.id_produit = sp.ID_PRODUCT
				WHERE sp.ACTIVE = 0;";
			
		$this -> exec($sql);
	}
	
	/**
	 * Fonction de nettoyage permettant de supprimer de la liste de vente d'un ecommerçant, les produits
	 * qu'il a passé en inactif dans son Prestatshop
	 */
	public function purgePrestashopInactiveProduct() {
		$sql = "DELETE  spe  
				FROM sfk_product_ecommercant_stock_prix spe
				INNER JOIN  sfk_produit_ps sp 
				ON spe.VENDOR_ID = sp.VENDOR_ID AND spe.id_produit = sp.ID_PRODUCT
				WHERE sp.ACTIVE = 0;";
			
		$this -> exec($sql);
	
	}
	
	
	/**
	 * Idem que fonction getAllProdByCatId sauf que l'on utilie des alias pour le rendre générique
	 * et utiliable dan la vue de la liste de produits. Il y a la possibilité d'appliquer un certain
	 * nombre de filtres de recherche notamment par marque, prix ou bien recherche personnalisée
	 * @param catId 		id de la catégorie dont on veux les produits
	 * @param brand 		marque que l'on cherche
	 * @param priceMin 		prix minimum recherche
	 * @param priceMax 		prix manimum recherché
	 * @param search 		recherche personnalisé sur de champs indexés FULL TEXT
	 */
	public function getAllProdByCatIdAliastest($catid, $brand=null, $priceMin=null, $priceMax=null, $search=null, $order="") {
		$sql = " SELECT id_produit as ID_PRODUCT, name as NAME_PRODUCT, image as IMAGE_PRODUCT, local_image as LOCAL_IMAGE, price as PRICE_PRODUCT, description as DESCRIPTION_SHORT, 
				sku as REFERENCE_PRODUCT, manufacturer as MANUFACTURER, categories as CATEGORY, `delete`, weight as WEIGHT, 
				 FROM sfk_catalog_product 
				 WHERE categories IN ($catid) ";
		
		
		if(!empty($brand) && $brand != "Choix de marque :"){
			$sql .= " AND manufacturer = :MANUFACTURER ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			$sql .= " AND CAST(price AS DECIMAL(10,5)) > :pricemin ";
		}else{
			$priceMin = "";
		}
				
		if(is_float($priceMax) && $priceMax >0 ){
			$sql .= " AND CAST(price AS DECIMAL(10,5)) < :pricemax ";
		}else{
			$priceMax = "";
		}
		
		if (!empty($search)) {
			$sql .= " AND MATCH(name, description, sku) AGAINST (:NAME_PRODUCT IN BOOLEAN MODE) ";	
		}
		
		if($order == ""){
			$sql .= " ORDER BY name ";	
		}elseif($order == "asc"){
			$sql .= " ORDER BY CAST(price AS DECIMAL(10,5)) ASC ";
		}elseif($order == "desc"){
			$sql .= " ORDER BY CAST(price AS DECIMAL(10,5)) DESC ";
		}
		
						 
		$stmt = $this -> prepare($sql); 
		//$stmt -> bindValue(":categories", $catid);

		if(!empty($brand) && $brand != "Choix de marque :"){
			$stmt -> bindValue(":MANUFACTURER", $brand);
		}
		
		if(is_float($priceMin) | is_int($priceMin)){
			$stmt -> bindValue(":pricemin" , $priceMin);
		}
		
		if(is_float($priceMax) | is_int($priceMax)){
			$stmt -> bindValue(":pricemax" , $priceMax);
		}
		
		if (!empty($search)) {
			$stmt -> bindValue(":NAME_PRODUCT", $search, PDO::PARAM_STR);
		}
		
		return $this->getAllPrepa($stmt);

	}
	/**
	 * Idem que fonction getAllProdByCatId sauf que l'on utilie des alias pour le rendre générique
	 * et utiliable dan la vue de la liste de produits. Il y a la possibilité d'appliquer un certain
	 * nombre de filtres de recherche notamment par marque, prix ou bien recherche personnalisée
	 * @param catId 		id de la catégorie dont on veux les produits
	 * @param brand 		marque que l'on cherche
	 * @param priceMin 		prix minimum recherche
	 * @param priceMax 		prix manimum recherché
	 * @param search 		recherche personnalisé sur de champs indexés FULL TEXT
	 * @param $idVendor		recherche sur l'id vendor
	 * @param $deleted		recherche si un produit est deleted ou pas (1 ou 0 )
	 */
	public function getAllProdByCatIdAlias($catid=null, $brand=null, $priceMin=null, $priceMax=null, $search=null, $order="", 
	                                       $idVendor=null, $deleted=null , $assoc=null , $poid=null , $vendu = null, $actif = true ) 
    {
        
		$sql = " SELECT scp.id_produit as ID_PRODUCT, scp.name as NAME_PRODUCT, scp.image as IMAGE_PRODUCT, scp.local_image as LOCAL_IMAGE, scp.price as PRICE_PRODUCT, scp.description as DESCRIPTION_SHORT, 
				scp.sku as REFERENCE_PRODUCT, scp.manufacturer as MANUFACTURER, scp.categories as CATEGORY, scp.`delete`, scp.weight as WEIGHT , 
				scp.local_image as LOCAL_IMAGE, scp.local_small_image as LOCAL_SMALL_IMAGE, scp.local_thumbnail as LOCAL_THUMBNAIL , 
				 sap.VENDOR_ID as VENDOR_ID , sap.id_produit_ecommercant as ID_PRODUCT_ECOM, MATCH (scp.name, scp.sku) AGAINST ('$search*' IN BOOLEAN MODE) as SCORE
				 FROM sfk_catalog_product as scp 
				 LEFT JOIN sfk_assoc_product as sap ON sap.id_avahis = scp.id_produit 
				 LEFT JOIN sfk_product_ecommercant_stock_prix as sp ON sp.id_produit = scp.sku 
				 ";
		//if ($delete==null)
		//$where = "WHERE categories IN ($catid)" ;
		if(!empty($catid ) && $catid != null){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
			 //$where .= " scp.categories IN ( $catid ) ";
			 $where .= " scp.categories = $catid  ";
		}
		 
		 
		if(isset($brand) && $brand != "Choix de marque :"){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
			 $where .= " scp.manufacturer = '$brand' ";
		}
		
		if(is_float($priceMin) && $priceMin >0 ){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
			 $where .= " CAST(scp.price AS DECIMAL(10,5)) > $priceMin ";
		}else{
			$priceMin = "";
		}
				
		if(is_float($priceMax) && $priceMax >0 ){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
			 $where .= " CAST(scp.price AS DECIMAL(10,5)) < $priceMax ";
		}else{
			$priceMax = "";
		}
		
		if (isset($search) && $search != null) {
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
			 $where .= " MATCH (scp.name, scp.sku) AGAINST ('$search*' IN BOOLEAN MODE) ";	
		}
	/*	désactivé pour choix vendu ou  associé
		if(isset($idVendor) && $idVendor != null && $idVendor != "Choix de l'e-commerçant :"){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
		 	//si vendeur soukeo id = 24 
		 	//alors on cherche les produits associé dans la table assoc grossiste
		 	
		 	$where .= " VENDOR_ID = '$idVendor' ";
		}
	 */ 
		if(isset($deleted) && $deleted != null){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
		 	$where .= " `delete` = '$deleted' ";
		}

        if(isset($actif) && $actif == true){
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " sp.QUANTITY > 0 ";
        }
        		
		if($order == ""){
			$whereOrder .=  " ORDER BY SCORE DESC";	
		}elseif($order == "asc"){
			$whereOrder .=  " ORDER BY CAST(price AS DECIMAL(10,5)) ASC ";
		}elseif($order == "desc"){
			$whereOrder .= " ORDER BY CAST(price AS DECIMAL(10,5)) DESC ";
		}
		
		// associé	
		if(isset($assoc) && $assoc != null ){
			if ($idVendor == "Choix de l'e-commerçant :") return 'null';		
				if ($where == "") $where = " WHERE ";
				else $where .= " AND ";
				$where .= " scp.id_produit IN 
						(SELECT DISTINCT sap.id_avahis FROM sfk_assoc_product as sap "; 
								if(isset($idVendor) && $idVendor != null && $idVendor != "Choix de l'e-commerçant :" && $idVendor != 'allvendor'){
				$where .=  " where sap.VENDOR_ID = '$idVendor' "; 
								}
		  						if(isset($idVendor) && $idVendor != null && $idVendor != "Choix de l'e-commerçant :" ){
		  							if($idVendor == '24' || $idVendor == 'allvendor')
		  		$where .= " UNION  SELECT DISTINCT sapg.id_avahis FROM sfk_assoc_product_grossiste as sapg ";
								}
				$where .="			) ";

				//		$where .=  " where scp.VENDOR_ID = '$idVendor' ";  
				//}

//			$where .=		 
//				 "GROUP BY scp.id_avahis )"; 

		}
		// pour les produits qui sont vendu quantité supérieur a 0
		if(isset($vendu) && $vendu != null ){
			if ($idVendor == "Choix de l'e-commerçant :") return 'null';		
				if ($where == "") $where = " WHERE ";
				else $where .= " AND ";
				// tous les id_avhis qui sont dans la table sfk_catalog_product en relation avec la table sfk_product_ecommercant_stock_prix 
				// dont le sku de sfk_catalog_product est égale a l' id_produit de la table  sfk_catalog_product
					$where .= " scp.id_produit IN 
								(SELECT scp.id_produit FROM sfk_catalog_product AS scp INNER JOIN sfk_product_ecommercant_stock_prix as spesp ON spesp.id_produit = scp.sku ";
				// et avec un id_vendor spécifique
					if($idVendor != 'allvendor' && isset($idVendor) && $idVendor != null && $idVendor != "Choix de l'e-commerçant :" ){
						$where .=  " where spesp.VENDOR_ID = '$idVendor' AND spesp.QUANTITY != '0' ) "; 
					}
					else {
						$where .=  " where spesp.QUANTITY != '0' ) "; 
						
					}
				/* Quand id_produit de sfk_product_ecommercant_stock_prix était le 
				// tous id_avahis qui sont dans les produit id avahis de la table sfk_assoc_product qui sont dans la table sfk_product_ecommercant_stock_prix dont 
				//   le id produit ecom correstponde dans les deux table
				$sql = "(scp.id_produit IN (scp.id_produit FROM sfk_catalog_product AS scp INNER JOIN sfk_product_ecommercant_stock_prix as spesp ON spesp.id_produit = scp.id_produit_ecommercant ";
				// et avec un id_vendor spécifique
				// Union avec
				// si c'est le vendeur 24 (soukeo) ou tous les vendeurs
				// tous id avahis qui sont dans les produit id avahis de la table sfk_assoc_product qui sont dans la table sfk_assoc_product_grossiste
				 */ 
		}
		
		
		// pour distingué ce qui n'ont pas de poid
		if(isset($poid) && $poid != null){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
			$where .= " scp.weight <='0' ";
		}
		
		$groupBy = " GROUP BY ID_PRODUCT " ;
		$sql = $sql.$where.$groupBy.$whereOrder;
		//echo "<br>".$sql ;				 
		$stmt = $this -> prepare($sql); 
	
		
		return $this->getAllPrepa($stmt);

	}

    public function getAllProdByName($label)
    {
        $sql="SELECT id_produit, name, image, short_description,  
              MATCH (name, meta_keyword, description, sku) AGAINST ('%".$label."%') as SCORE  
              FROM sfk_catalog_product 
              WHERE MATCH (name, meta_keyword, description, sku) AGAINST ('%".$label."%')
              ORDER BY SCORE DESC LIMIT 0,40";
        
        $sql="SELECT id_produit, name, image, short_description 
              FROM sfk_catalog_product 
              WHERE name LIKE '%".$label."%' OR sku LIKE '%".$label."%'  
              ORDER BY name ASC LIMIT 0,40";
              
        $stmt = $this -> prepare($sql);
        
        //$stmt -> bindvalue(':label', $label, PDO::PARAM_STR);
        
        return $this->getAllPrepa($stmt);
    }
    	
	public function getAllsScatId($catId, $out ="")
	{
		if($out == "") $out = "'".$catId."', ";
		
		$sql = 'SELECT cat_id, count_prod FROM sfk_categorie';
	
		$sql .= ' where parent_id = '.$catId;
		


		$list = $this -> getall($sql);
		if(!empty($list)){

			foreach ($list as $cat) {
				if($cat['count_prod'] == 0){
			
					$out .= "'".$cat['cat_id']."', ";			
				}else{
					$out .= $this->getAllsScatId($cat['cat_id']);
				}
				
			}
				
		}
		return $out;
		
		
	}
	
	
	/**
	 * Fonction permettant d'obtenir les informations sur un produit Avahis grace à son ID.
	 * La différence ici est que l'on renomme chaque champs avec un alias afin de rendre le tout générique
	 * pour l'utilisation dans la vue
	 * @param idProduct 	Id du produit Avahis voulu
	 */
	public function getProductAlias($idProduct)
	{
		$sql = " SELECT id_produit as ID_PRODUCT, name as NAME_PRODUCT, image as IMAGE_PRODUCT, price as PRICE_PRODUCT, short_description as DESCRIPTION_SHORT, 
				 sku as REFERENCE_PRODUCT, manufacturer as MANUFACTURER, description as DESCRIPTION, weight as WEIGHT, 
				 date_create as DATE_CREATE, date_update as DATE_UPDATE , sku as EAN, categories as CATEGORY, 
				 local_image as LOCAL_IMAGE, local_small_image as LOCAL_SMALL_IMAGE, local_thumbnail as LOCAL_THUMBNAIL  
				 FROM sfk_catalog_product 
				 WHERE id_produit = :id_produit";

		$stmt = $this->prepare($sql);
		$stmt -> bindValue(":id_produit", $idProduct);
		
		$stmt -> execute() ;
		
		return $stmt -> fetch(PDO::FETCH_ASSOC);
	}
	
	public function updateWeightSfkCp($id_produit, $weight)
	{
		$sql = "UPDATE sfk_catalog_product SET weight = '$weight' WHERE id_produit = '$id_produit'";
		
		return $this -> exec($sql);
		
	}
	
	/**
	 * Fonction permettant d'obtenir toutes les marques (Manufacturer) uniques de produits
	 * dans une catégorie Avahis
	 * @param catId 	id de la catégorie dont on veux les marques
	 */
	public function getAllDistinctBrandFromCat($catId)
	{
		$sql =" SELECT DISTINCT manufacturer as MANUFACTURER FROM sfk_catalog_product
				WHERE categories = :categories
				AND MANUFACTURER <> '' 
				ORDER BY manufacturer ASC";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":categories", $catId);

		return $this->getAllPrepa($stmt);
	}
	
	
	
	/**
	 * Fonction permettant d'obtenir les attributs présents dans une catégorie Avahis
	 * Ne remonte que les 3 attributs les plus utilisés pour une catégorie 
	 * @param cat 		id de la catégorie 
	 */
	public function getBestAttrFromCateg($cat)
	{
		$sql =" SELECT COUNT(*) as count, a.id_attribut as ID_ATTR, a.label_attr as LABEL_ATTR FROM sfk_attribut as a
			    INNER JOIN sfk_product_attribut as pa ON a.id_attribut= pa.id_attribut
			    INNER JOIN sfk_catalog_product as p ON pa.id_produit= p.id_produit AND p.categories = a.cat_id 
				WHERE  p.categories = :cat_id
				AND a.code_attr <> 'REFERENCE' 
				AND a.code_attr <> 'EAN'
				GROUP BY a.id_attribut
				ORDER BY count DESC
				LIMIT 3";
		/*$sql =" SELECT COUNT(*) as count, a.id_attribut as ID_ATTR, a.label_attr as LABEL_ATTR FROM sfk_attribut as a
			    INNER JOIN sfk_product_attribut as pa ON a.id_attribut= pa.id_attribut
			    INNER JOIN sfk_catalog_product as p ON pa.id_produit= p.id_produit
				WHERE  p.categories IN ($cat)
				AND a.code_attr <> 'REFERENCE' 
				AND a.code_attr <> 'EAN'
				GROUP BY a.label_attr
				ORDER BY count DESC
				LIMIT 3";*/
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":cat_id", $cat);
		
		
		return $this->getAllPrepa($stmt);
	}

	
	/**
	 * Cette fonction permet d'obtenir toutes les valeurs distinctes pour un attribut pour tous
	 * les produits présents dans une catégorie Avahis. Afin d'obtenir une liste de selection de valeurs toutes
	 * uniques pour UN attribut
	 * @param cat 		id de la catégorie avahis
	 * @param idAttr 	id de l'attribut avahis
	 */
	public function getAllDisctValueForAttrFromCateg($cat, $idAttr)
	{
		$sql =" SELECT DISTINCT TRIM(pa.value) as VALUE FROM sfk_product_attribut as pa
			    INNER JOIN sfk_attribut as a ON a.id_attribut= pa.id_attribut
				INNER JOIN sfk_catalog_product as p ON pa.id_produit= p.id_produit
				WHERE p.categories =  :categories
				AND a.id_attribut = :id_attribut
				ORDER BY pa.value ASC";
		/*$sql =" SELECT DISTINCT TRIM(pa.value) as VALUE FROM sfk_product_attribut as pa
			    INNER JOIN sfk_attribut as a ON a.id_attribut= pa.id_attribut
				INNER JOIN sfk_catalog_product as p ON pa.id_produit= p.id_produit
				WHERE p.categories IN ($cat)
				AND a.id_attribut = :id_attribut
				ORDER BY pa.value ASC";*/
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":categories", $cat);
		$stmt -> bindValue(":id_attribut", $idAttr);
		
		
		return $this->getAllPrepa($stmt);
	}
	
    public function getDistinctValuesAttribut($id_attribut)
    {
        $sql =" SELECT TRIM(pa.value) as value, count(pa.value) as c 
                FROM sfk_product_attribut as pa
                INNER JOIN sfk_attribut as a ON a.id_attribut= pa.id_attribut 
                WHERE a.id_attribut = :id_attribut 
                AND pa.active = 1 
                GROUP BY TRIM(pa.value)
                ORDER BY pa.value ASC";
                
        $stmt = $this -> prepare($sql); 

        $stmt -> bindValue(":id_attribut", $id_attribut);
        
        return $this->getAllPrepa($stmt);
    }
	
	/**
	 * Vérifie si un attribut est présent avec une certaine valeur pour un produit avahis
	 * @param idProduct 	id du produit Avahis
	 * @param idAttr 		id de l'attribut Avahis qu'on vérifie
	 * @param value 		valeur pour l'association produit attribut
	 */
	public function verifAttrForProductSkf($idProduct, $idAttr, $valAttr)
	{
		$sql = "SELECT * FROM sfk_product_attribut 
				WHERE 	id_produit = :ID_PRODUCT 
				AND 	id_attribut    = :ID_ATTR
				AND 	value      = :VALUE ";
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct); 
		$stmt -> bindValue(":ID_ATTR", $idAttr);
		$stmt -> bindValue(":VALUE", $valAttr);
		
		$stmt -> execute();
				
		if ($stmt -> fetch() == null)
			return false;
		else 
			return true;
	}
	
	
	/**
	 * Fonction permettant d'obtenir toutes les sous catégories en fonction
	 * d'un id catégorie présent en "parent id" pour d'autress categ
	 * @param id 	id de la catégorie
	 */
	public function getSsCatByParentId($id=2) {

		$sql = 'SELECT * FROM sfk_categorie';
		if ($id == 'null'){
			$sql .= ' where parent_id is '.$id;
		}else{
			$sql .= ' where parent_id = '.$id;
		}
		$sql .= ' AND  cat_active = 1 ORDER BY cat_label';
		//echo $sql;
		return $this -> getall($sql);
	
	}
	
	
	/**
	 * Fonction permettant de compter tous les produits dans une catégorie de façon récursive afin que les catégories
	 * prennent en compte le nombre de produits dans ses sous catégories et les sous catégories de celle ci...
	 * @param catId 	Id de la catégorie Avahis
	 * @param count 	Nombre de produits dans la catégorie
	 */
	public function countAllProductInCatRecursive($catId, $count)
	{
		//On va chercher les cat filles
		$sql = "SELECT DISTINCT cat_id FROM sfk_categorie WHERE parent_id = :parent_id";
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":parent_id", $catId);

		$childs = $this->getAllPrepa($stmt);
		
		if(!empty($childs) ){
			foreach ($childs as $child) {
				$tmp += $this->countAllProductInCatRecursive($child['cat_id'], $count);
			}	
		}
		
		//On compte tous les produits dans cette catégorie
		$sql = "SELECT count(*) as count FROM sfk_catalog_product WHERE categories = :categories";
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":categories", $catId);
		$stmt -> execute();
		
		$res = $stmt -> fetch();
		$count = $count + $res['count'];

		return $count + $tmp;
	}
    
    public function getAllLastLevelCatOfUniverse($cat_id = "", $cat_label = "")
    {
        $sql = "SELECT cat_label, cat_id FROM sfk_categorie WHERE parent_id = :parent_id and cat_active = 1";
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(":parent_id", $cat_id);
        
        $childs = $this->getAllPrepa($stmt);
        
        $result = "";
        
        if(!empty($childs) ){

            foreach ($childs as $child) {
                $result .=  $this->getAllLastLevelCatOfUniverse($child['cat_id'], $child['cat_label']);
            }   
        }
        else{
            $result.= $cat_id . "-";
        }
        
        return $result;        
    }	

    
    public function getAllCatChildId($cat_id = "")
    {
        $sql = "SELECT  cat_id FROM sfk_categorie WHERE parent_id = :parent_id and cat_active = 1";
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(":parent_id", $cat_id);
        
        $childs = $this->getAllPrepa($stmt);
        
        $result = "";

        if(!empty($childs) ){

            foreach ($childs as $child) {
                $result .=  $this->getAllCatChildId($child['cat_id']);
            }   
        }
        
        $result .= $cat_id . ", ";
        
        return $result;        
    }
	
	/**
	 * Fonction permettant de mettre a jour le champ count_prod représentant
	 * le nombre de produit présent dans une catégorie Avahis
	 * @param catId 	id de la catégorie Avahis
	 * @param count 	nombre de produits dans cette catégorie
	 */
	public function insertCountProdForCat($catId, $count)
	{
		$sql = "UPDATE sfk_categorie SET count_prod = $count WHERE cat_id = :cat_id";
		
		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":cat_id", $catId);	
		$stmt -> execute();
	}
	
	
	/**
	 * Fonction permettant d'obtenir toutes les informations pour un produit avahis 
	 * grace à son id produit
	 * @param idProd 	l'id du produit dont on veux les informations
	 */
	public function getProduct($idProd)
	{
		$sql = "SELECT * FROM sfk_catalog_product WHERE id_produit = :id_produit
				GROUP BY id_produit ";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit' , $idProd);
		$stmt -> execute() ;
		
		$res = $stmt -> fetch(PDO::FETCH_ASSOC);
		
		return $res;
	}
	
	public function getProductSfkCp($idProduct)
	{
		$sql = "SELECT * FROM sfk_catalog_product WHERE id_produit = :id_produit";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit' , $idProduct);
		$stmt -> execute() ;
		
		return $this -> getAllPrepa($stmt);
	}
	
	
	public function getStockPriceEcom($idProd, $vendorId)
	{
		$sql = "SELECT * FROM sfk_product_ecommercant_stock_prix WHERE id_produit = :id_produit AND VENDOR_ID = :VENDOR_ID";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit' , $idProd);
		$stmt -> bindValue(':VENDOR_ID' ,  $vendorId);
		$stmt -> execute() ;
		
		$res = $stmt -> fetch();

		if($res['id_produit'] != "" && !empty($res['id_produit']) && isset($res['id_produit'])){
			
			return true;	
		}
		else{
			return false;
		}
		
	}
	
	//Vérifie si le produit n'est pas déja vendu par un autre ecommerçant
	public function productIsAlreadySold($idProd)
	{
		$sql = "SELECT id_produit FROM sfk_product_ecommercant_stock_prix WHERE id_produit = :id_produit AND VENDOR_ID <> 24 AND QUANTITY <> 0 LIMIT 1";
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit' , $idProd);
		$stmt -> execute() ;
		
		$res = $stmt -> fetch();

		if($res['id_produit'] != "" && !empty($res['id_produit']) && isset($res['id_produit'])){
			
			return true;	
		}
		else{
			return false;
		}
		
	}
	
	/**
	 * Fonction permettant d'associer un produit d'un ecommercant avec une fiche produit Avahis
	 * @param idProdEcom 	id du produit de l'ecommercant
	 * @param idProdAv 		id du produit Avahis associé
	 * @param vendorID 		id de l'ecommercant
	 */
	public function assocProduct($idProdEcom, $idProdAv, $vendorId)
	{
		$sql = "INSERT INTO sfk_assoc_product ( id_avahis , VENDOR_ID, id_produit_ecommercant ) 
				values ( :id_avahis, :VENDOR_ID, :id_produit_ecommercant) 
				ON DUPLICATE KEY UPDATE 
				id_avahis			=VALUES(id_avahis)";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':id_avahis', $idProdAv);
		$stmt -> bindValue(':id_produit_ecommercant', $idProdEcom);
		$stmt -> bindValue(':VENDOR_ID', $vendorId);
		
		$stmt -> execute() ;
		
		return true;
	}
	
	public function removeAssoc($vendorId, $idProduct)
	{
		$sql = "DELETE FROM sfk_assoc_product 
				WHERE id_produit_ecommercant = :id_produit_ecommercant 
				AND VENDOR_ID = :VENDOR_ID";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':id_produit_ecommercant', $idProduct);
		$stmt -> bindValue(':VENDOR_ID', $vendorId);
		
		$stmt -> execute() ;
		
		return true;
	}
	
		public function removeAssocGr($vendorId, $idProduct)
	{
		$sql = "DELETE FROM sfk_assoc_product 
				WHERE id_produit_grossiste = :id_produit_grossiste 
				AND GROSSISTE_ID = :GROSSISTE_ID";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':id_produit_grossiste', $idProduct);
		$stmt -> bindValue(':GROSSISTE_ID', $vendorId);
		
		$stmt -> execute() ;
		
		return true;
	}
	
	
	/**
	 * Fonction permettant d'obtenir tou le attributs pour un produit Avahis
	 * @param 	idProduct l'id du produit avahis dont on veux le attributs
	 */
	public function getAllAttrFromProduct($idProduct, $vendorId=null)
	{
		$sql = "SELECT pa.value as VALUE, a.label_attr as LABEL_ATTR, pa.id_attribut as ID_ATTRIBUT FROM sfk_product_attribut as pa 
				INNER JOIN sfk_attribut as a ON a.id_attribut = pa.id_attribut
				INNER JOIN sfk_catalog_product as p ON p.categories = a.cat_id AND pa.id_produit = p.id_produit 
				WHERE pa.id_produit = :ID_PRODUCT";
					
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":ID_PRODUCT", $idProduct);
		
		return $this->getAllPrepa($stmt);
	}
	
	
	/**
	 * Fonction permettant d'obtenir toutes les informations d'une catégorie Avahis grace à
	 * son ID.
	 * @param catId 	l'ID de la catégorie Avahis
	 */
	public function getCatbyId($catId)
	{
		$sql = "SELECT * FROM sfk_categorie WHERE cat_id = :cat_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':cat_id' , $catId);
		$stmt -> execute() ;
		
		$res = $stmt -> fetch();
		
		return $res;
	}
	
	
	/**
	 * Fonction permettant d'obtenir toutes les informations d'une catégorie Avahis grace à
	 * son label.
	 * @param $label 	label de la catégorie Avahis
	 */
	public function getCatbyLabel($label)
	{
		$sql = "SELECT * FROM sfk_categorie WHERE cat_label = :cat_label";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':cat_label' , $label);
		$stmt -> execute() ;
		
		return $this->getAllPrepa($stmt);
	}
	
	/**
	 * Ajoute une association entre un vendeur une catégorie avahis et sa catégorie ecommercant
	 * @param idVendor 	ID de l'ecommercant
	 * @param cat_ecom 	catégorie de l'ecommercant
	 * @param cat_id 	id cat avahis avec lequel la cat ecom sera associée
	 */
	public function addAssocCatVendor($idVendor, $cat_ecom, $cat_id)
	{
		$sql = "INSERT INTO sfk_assoc_categ (VENDOR_ID, categ_vendor, cat_id) 
		        VALUES (:VENDOR_ID, :categ_vendor, :cat_id) 
		        ON DUPLICATE KEY UPDATE
					cat_id = VALUES(cat_id)";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID', $idVendor);
		$stmt -> bindValue(':categ_vendor', $cat_ecom);
		$stmt -> bindValue(':cat_id', $cat_id);
		
		$stmt -> execute() ;
		
		return true;
	}
	
	/**
	 * Fonction permettant de savoir i cette catégorie ecommercant a déjà été associée à une catégorie
	 * Avahis. Si une association existe la fonction retourne l'ID de la catégorie avahis associée, sinon
	 * elle retourne false
	 * @param idVendor l'ID de l'ecommercant
	 * @param cat_ecom la catégorie de l'exommercant sous forme Root > Cat > sCat
	 */
	public function isCatAssoc($idVendor, $cat_ecom)
	{
		$sql = "SELECT cat_id 
		 		FROM sfk_assoc_categ 
		 		WHERE categ_vendor = :categ_vendor 
		 		AND VENDOR_ID = :VENDOR_ID 
		 		LIMIT 1";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID', $idVendor);
		$stmt -> bindValue(':categ_vendor', $cat_ecom);
		
		$stmt -> execute() ;
		$res = $stmt -> fetch();
		
		if($res != null){
			return $res['cat_id'];	
		}
		else{
			return false;
		}
	}
	
	/**
	 * Fonction permettant de savoir i cette catégorie ecommercant a déjà été associée à une catégorie
	 * Avahis. Si une association existe la fonction retourne l'ID de la catégorie avahis associée, sinon
	 * elle retourne false
	 * @param idVendor l'ID du grossiste
	 * @param cat_ecom la catégorie de l'exommercant sous forme Root > Cat > sCat
	 */
	public function isCatAssocGross($idGrossiste, $cat_ecom)
	{
		$sql = "SELECT cat_id 
		 		FROM sfk_assoc_categ_grossiste 
		 		WHERE categ_grossiste = :categ_grossiste 
		 		AND GROSSISTE_ID = :GROSSISTE_ID 
		 		LIMIT 1";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', $idGrossiste);
		$stmt -> bindValue(':categ_grossiste', $cat_ecom);
		
		$stmt -> execute() ;
		$res = $stmt -> fetch();
		
		if($res != null){
			return $res['cat_id'];	
		}
		else{
			return false;
		}
	}
	
	/**
	 * Fonction récursive permettant d'obtenir la suite d'embranchements  des id pour une catégorie
	 * La totalité des id catégorie est remonté sous la forme idCat-idSCat-idSScat
	 * @param cat_id 	id catégorie qu'on traite à ce moment
	 * @param out 		la chaine de caractère qui sera composé des ID de chaque categ et ssouss categ
	 */
	public function getCatIdTree($cat_id, $out ="")
	{
		$sql = "SELECT parent_id 
		 		FROM sfk_categorie 
		 		WHERE cat_id = :cat_id ";
		
		$stmt = $this -> prepare($sql); 		
		$stmt -> bindValue(':cat_id', $cat_id);
		
		$stmt -> execute() ;
		$res = $stmt -> fetch();
		
		if($res['parent_id'] != 2 && $res['parent_id']){
			$out= $cat_id."|".$this->getCatIdTree($res['parent_id'], $out).$out;
			return $out;
		}
		else{
			$out .= $cat_id;
			return $out;
		}
	}
	
		/**
	 * Fonction récursive permettant d'obtenir la suite d'embranchements  des id pour une catégorie
	 * La totalité des id catégorie est remonté sous la forme idCat-idSCat-idSScat
	 * @param cat_id 	id catégorie qu'on traite à ce moment
	 * @param out 		la chaine de caractère qui sera composé des ID de chaque categ et ssouss categ
	 */
	public function getCatIdTreeForMeta($cat_id, $out ="")
	{
		$sql = "SELECT *
		 		FROM sfk_categorie 
		 		WHERE cat_id = :cat_id ";
		
		$stmt = $this -> prepare($sql); 		
		$stmt -> bindValue(':cat_id', $cat_id);
		
		$stmt -> execute() ;
		$res = $stmt -> fetch();
		
		if($res['parent_id'] != 2 && $res['parent_id']){
			$out= $res['cat_label'].", ".$this->getCatIdTreeForMeta($res['parent_id'], $out).$out;
			return $out;
		}
		else{
			$out .= $res['cat_label'];
			return $out;
		}
	}
	
	/**
	 * Fonction permettant de mapper les champs statiques pour un ecommercant et une catégorie avahis, avec sa
	 * catégorie a lui. Chaque champs (exemple : sku, name, description) sera associé à un champ de l'ecommercant
	 * que l'utilisateur aura choisi au moment de auvegarder ses préférences
	 * @param vendor_id 	l'ID de l'ecommercant 
	 * @param cat_id 		l'id de la catégorie avahis associée
	 * @param cat_ecom 		chaine correpondant a la catégorie ecommercant
	 * @param fields 		array contenant en clé le champ avahis et en valeur sa correspondance ecommercant
	 */
	public function mappingCategStaticField($vendor_id, $cat_id, $cat_ecom, $fields)
	{
		$sql = "INSERT INTO sfk_mapping_static_fields (VENDOR_ID, cat_id, cat_ecom, field_avahis, field_ecom) 
		        VALUES (:VENDOR_ID, :cat_id, :cat_ecom, :field_avahis, :field_ecom) 
		        ON DUPLICATE KEY UPDATE
					field_ecom = VALUES(field_ecom)";
		
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':cat_id', $cat_id);
		$stmt -> bindValue(':VENDOR_ID', $vendor_id);
		$stmt -> bindValue(':cat_ecom', $cat_ecom);
		
		foreach ($fields as $key => $value) {
			if($value != "--"){
				$stmt -> bindValue(':field_avahis', $key);
				$stmt -> bindValue(':field_ecom', $value);	
			}
			
			
			$stmt -> execute();
		}	
	}
	
	
	/**
	 * Fonction permettant de mapper les attributs dynamiques d'un produit ecommercant avec des attributs
	 * dynamiques présent dans la base avahis et correspondant à la catégorie avahis auquel on a associé les
	 * produits de cet ecommercant.
	 * @param vendor_id 	l'ID de l'ecommercant
	 * @param cat_id 		l'ID de la catégorie Avahis associée 
	 * @param cat_ecom 		La chaine représentant la catégorie ecommercant
	 * @param id_attr_av 	l'id de l'attribut coté avahis qu'on a selectionné
	 * @param id_attr_ecom 	l'id de l'attribut dan les tables attributs de l'ecommercant 
	 */
	public function mappingCategDynaField($vendor_id, $cat_id, $cat_ecom, $id_attr_av, $id_attr_ecom)
	{
		$sql = "INSERT INTO sfk_mapping_dyna_fields (VENDOR_ID, cat_id_av, cat_id_ecom, id_attr_av, id_attr_ecom) 
		        VALUES (:VENDOR_ID, :cat_id, :cat_ecom, :id_attr_av, :id_attr_ecom) 
		        ON DUPLICATE KEY UPDATE
					id_attr_av = VALUES(id_attr_av)";
		
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':cat_id', $cat_id);
		$stmt -> bindValue(':VENDOR_ID', $vendor_id);
		$stmt -> bindValue(':cat_ecom', $cat_ecom);
		$stmt -> bindValue(':id_attr_av', $id_attr_av);
		$stmt -> bindValue(':id_attr_ecom', $id_attr_ecom);
		
		$stmt -> execute();	
	}
	
	
	/**
	 * Fonction permettant d'obtenir le mapping s'il existe pour un attribut, pour un ecommercant
	 * dans la catégorie voulue déjà associée. Si le mapping existe on renvoi l'id de l'attribut
	 * avahis mappé. Sinon la fonction renvoie false
	 * @param id_attr_ecom 	l'id attribut de l'ecommercant dont on veux la correspondance
	 * @param vendor_id 	l'id de l'ecommercant
	 * @param cat_ecom 		chaine représentant la catégorie ecommercant
	 * @param cat_av 		id catégorie Avahis
	 */
	public function getMappingForAttrEcom($id_attr_ecom, $vendor_id, $cat_ecom, $cat_av)
	{
		$sql = "SELECT id_attr_av 
				FROM sfk_mapping_dyna_fields 
				WHERE id_attr_ecom = :id_attr_ecom 
				AND VENDOR_ID = :VENDOR_ID  
				AND cat_id_av = :cat_id_av 
				AND cat_id_ecom = :cat_id_ecom ";
				
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':cat_id_av', $cat_av);
		$stmt -> bindValue(':cat_id_ecom', trim($cat_ecom));
		$stmt -> bindValue(':VENDOR_ID', $vendor_id);
		$stmt -> bindValue(':id_attr_ecom', $id_attr_ecom);
		
		$stmt -> execute();
		
		$res = $stmt -> fetch();
		
		if($res != null){
			return $res['id_attr_av'];	
		}
		else{
			return false;
		}
		
	}
	
	/**
	 * Permet d'obtenir tous les champ statiques déja mappés pour un ecommercant et sa catégorie
	 * déja associée à une catégorie avahis
	 * @param vendor_id 	l'id de l'ecommercant
	 * @param cat_id 		id de la catégorie Avahis
	 * @param cat_ecom 		chaine représentant la catégorie ecommercant
	 */
	public function getStaticFieldsForVendor($vendor_id, $cat_id, $cat_ecom)
	{
		$sql = "SELECT field_avahis, field_ecom 
				FROM sfk_mapping_static_fields 
				WHERE VENDOR_ID = :VENDOR_ID 
				AND cat_id = :cat_id 
				AND cat_ecom = :cat_ecom";
		
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':cat_id', $cat_id);
		$stmt -> bindValue(':cat_ecom', $cat_ecom);
		$stmt -> bindValue(':VENDOR_ID', $vendor_id);
		
		return $this->getAllPrepa($stmt);
	}
	
	
	/**
	 * Fonction permettant d'ajouter un attribut à un produit grace à la table sfk_produc_attribut
	 * représentant l'association entre un attribut et un produit avahis ainsi que la valeur de cette association (couleur : bleu)
	 * @param id_attr 		id de l'attribut à associer
	 * @param id_product 	id du produit à associer
	 * @param value 		valeur liée à l'assoc produit attribut
	 */
	public function addAttributProduct($id_attr, $id_product, $value)
	{
		
		$sql = "INSERT INTO sfk_product_attribut (id_produit, id_attribut, value, active) 
				VALUE (:id_produit, :id_attribut, :value, 1) 
				ON DUPLICATE KEY UPDATE 
					value  = VALUES(value),
					active = VALUES(active)";
					
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':id_produit', $id_product);
		$stmt -> bindValue(':id_attribut', $id_attr);
		$stmt -> bindValue(':value', $value);
		
		return $stmt -> execute();
	}
	
	public function resetProductAttrActive($id_product)
	{
		$sql = "UPDATE sfk_product_attribut SET active = 0 WHERE id_produit = $id_product";
		
		return $this -> exec($sql);
	}
	
	public function catExists($catId)
	{
		$sql = "SELECT cat_id FROM sfk_categorie WHERE cat_id = :cat_id ";
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':cat_id', $catId);
		
		$stmt -> execute();
		$res = $stmt -> fetch();
		
		if($res != null){
			return true;	
		}
		else{
			return false;
		}
	}
    public function checkIfVendorExist($vendor_id)
    {
        $sql = "SELECT COUNT(*) as c FROM sfk_vendor WHERE vendor_id = $vendor_id";
        
        return $this -> getOne($sql);
    }
    
    public function prepUpdateSfkVd()
    {
    $sql = "UPDATE sfk_vendor SET ";
    $sql .="    `VENDOR_ID` = :VENDOR_ID,`VENDOR_NOM` = :VENDOR_NOM, `VENDOR_ATTN` = :VENDOR_ATTN,
                `EMAIL` = :EMAIL, `STREET` = :STREET,`CITY` = :CITY ,`ZIP` = :ZIP,`COUNTRY_ID` = :COUNTRY_ID ,`REGION_ID` = :REGION_ID,
                `REGION` = :REGION,`TELEPHONE` = :TELEPHONE ,`FAX` = :FAX,`STATUS` = :STATUS,`PASSWORD` = :PASSWORD,`VENDOR_PASSWORD` = :VENDOR_PASSWORD,
                `PASSWORD_ENC` = :PASSWORD_ENC,`CARRIER_CODE`= :CARRIER_CODE,
                `NOTIFY_NEW_ORDER` = :NOTIFY_NEW_ORDER,`LABEL_TYPE` = :LABEL_TYPE,`TEST_MODE` = :TEST_MODE,`HANDLING_FEE` = :HANDLING_FEE,
                `UPS_SHIPPER_NUMBER` = :UPS_SHIPPER_NUMBER,`CUSTOM_DATA_COMBINED` = :CUSTOM_DATA_COMBINED,
                `CUSTOM_VARS_COMBINED` = :CUSTOM_VARS_COMBINED,`EMAIL_TEMPLATE` = :EMAIL_TEMPLATE,`URL_KEY` = :URL_KEY,`RANDOM_HASH` = :RANDOM_HASH,`CREATED_AT` = :CREATED_AT,`NOTIFY_LOWSTOCK` = :NOTIFY_LOWSTOCK,
                `NOTIFY_LOWSTOCK_QTY` = :NOTIFY_LOWSTOCK_QTY,`USE_HANDLING_FEE` = :USE_HANDLING_FEE,
                `USE_RATES_FALLBACK` = :USE_RATES_FALLBACK,`ALLOW_SHIPPING_EXTRA_CHARGE` = :ALLOW_SHIPPING_EXTRA_CHARGE,
                `DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX` = :DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX,`DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE` = :DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE,
                `DEFAULT_SHIPPING_EXTRA_CHARGE` = :DEFAULT_SHIPPING_EXTRA_CHARGE,
                `IS_EXTRA_CHARGE_SHIPPING_DEFAULT` = :IS_EXTRA_CHARGE_SHIPPING_DEFAULT,
                `DEFAULT_SHIPPING_ID` = :DEFAULT_SHIPPING_ID,`BILLING_USE_SHIPPING` = :BILLING_USE_SHIPPING,
                `BILLING_EMAIL` = :BILLING_EMAIL,
                `BILLING_TELEPHONE` = :BILLING_TELEPHONE,`BILLING_FAX` = :BILLING_FAX,`BILLING_VENDOR_ATTN` = :BILLING_VENDOR_ATTN,
                `BILLING_STREET` = :BILLING_STREET,`BILLING_CITY` = :BILLING_CITY,`BILLING_ZIP` = :BILLING_ZIP,
                `BILLING_COUNTRY_ID` = :BILLING_COUNTRY_ID,`BILLING_REGION_ID` = :BILLING_REGION_ID,
                `BILLING_REGION` = :BILLING_REGION,`BATCH_EXPORT_ORDERS_METHOD` = :BATCH_EXPORT_ORDERS_METHOD,
                `BATCH_EXPORT_ORDERS_SCHEDULE` = :BATCH_EXPORT_ORDERS_SCHEDULE,
                `BATCH_IMPORT_ORDERS_METHOD` = :BATCH_IMPORT_ORDERS_METHOD,`BATCH_IMPORT_ORDERS_SCHEDULE` = :BATCH_IMPORT_ORDERS_SCHEDULE,
                `BATCH_IMPORT_INVENTORY_METHOD` = :BATCH_IMPORT_INVENTORY_METHOD,
                `BATCH_IMPORT_INVENTORY_SCHEDULE` = :BATCH_IMPORT_INVENTORY_SCHEDULE,`BATCH_IMPORT_INVENTORY_TS` = :BATCH_IMPORT_INVENTORY_TS,
                `BATCH_IMPORT_ORDERS_TS` = :BATCH_IMPORT_ORDERS_TS,`CONFIRMATION` = :CONFIRMATION,
                `CONFIRMATION_SENT` = :CONFIRMATION_SENT,`REJECT_REASON` = :REJECT_REASON,`PAYOUT_TYPE`= :PAYOUT_TYPE,
                `PAYOUT_METHOD` = :PAYOUT_METHOD,`PAYOUT_SCHEDULE` = :PAYOUT_SCHEDULE,`BACKORDER_BY_AVAILABILITY` = :BACKORDER_BY_AVAILABILITY,
                `USE_RESERVED_QTY` = :USE_RESERVED_QTY,`UDPROD_TEMPLATE_SKU` = :UDPROD_TEMPLATE_SKU,
                `TIERCOM_RATES` = :TIERCOM_RATES,`TIERCOM_FIXED_RULE` = :TIERCOM_FIXED_RULE,`TIERCOM_FIXED_RATES` = :TIERCOM_FIXED_RATES,
                `TIERCOM_FIXED_CALC_TYPE` = :TIERCOM_FIXED_CALC_TYPE,`VENDOR_LOGIN` = :VENDOR_LOGIN,`CUSTOMER_ID` = :CUSTOMER_ID,
                `ACCOUNT_TYPE` = :ACCOUNT_TYPE,`IS_FEATURED` = :IS_FEATURED,`TIERSHIP_RATES` = :TIERSHIP_RATES,
                `TIERSHIP_SIMPLE_RATES` = :TIERSHIP_SIMPLE_RATES,`VACATION_MODE` = :VACATION_MODE,`VACATION_END` = :VACATION_END,
                `VACATION_MESSAGE` = :VACATION_MESSAGE,`ALLOW_UDRATINGS` = :ALLOW_UDRATINGS,`FORMJURIDIQUE` = :FORMJURIDIQUE,
                `RAISONSOCIAL` = :RAISONSOCIAL,`SIRET` = :SIRET,`CAPITAL` = :CAPITAL,`SITEINTERNET` = :SITEINTERNET,
                `TVAINTRA` = :TVAINTRA,`RIBDOMICILIATION` = :RIBDOMICILIATION,`RIBCODEBANQUE` = :RIBCODEBANQUE,
                `RIBCODEGUICHET` = :RIBCODEGUICHET,
                `RIBNUMCOMPTE` = :RIBNUMCOMPTE,`RIBCLE` = :RIBCLE,`RIBIBAN` = :RIBIBAN,
                `RIBBIC` = :RIBBIC,`RIBTITULAIRE` = :RIBTITULAIRE,`TELEPHONE2` = :TELEPHONE2,
                `CIVILITE` = :CIVILITE,`VENDOR_ATTN2` = :VENDOR_ATTN2, ACTIVE = 1  
                 WHERE VENDOR_ID = :VENDOR_ID";
       $this -> stmtVendorUpdate = $this -> prepare($sql);

    }
	/*
	 * F préparation de la requéte d'insertion des données venant d'avahis vers 
	 * Soukflux
	 */
	public function prepInsertSfkVd() {

	$sql = "INSERT INTO sfk_vendor ( ";
	$sql .="	`VENDOR_ID`,`VENDOR_NOM`,`VENDOR_ATTN`,`EMAIL`,	`STREET`,`CITY`,`ZIP`,`COUNTRY_ID`,`REGION_ID`,
 				`REGION`,`TELEPHONE`,`FAX`,`STATUS`,`PASSWORD`,`VENDOR_PASSWORD`,`PASSWORD_ENC`,`CARRIER_CODE`,
				`NOTIFY_NEW_ORDER`,`LABEL_TYPE`,`TEST_MODE`,`HANDLING_FEE`,`UPS_SHIPPER_NUMBER`,`CUSTOM_DATA_COMBINED`,
				`CUSTOM_VARS_COMBINED`,`EMAIL_TEMPLATE`,`URL_KEY`,`RANDOM_HASH`,`CREATED_AT`,`NOTIFY_LOWSTOCK`,
				`NOTIFY_LOWSTOCK_QTY`,`USE_HANDLING_FEE`,`USE_RATES_FALLBACK`,`ALLOW_SHIPPING_EXTRA_CHARGE`,
				`DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX`,`DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE`,`DEFAULT_SHIPPING_EXTRA_CHARGE`,
				`IS_EXTRA_CHARGE_SHIPPING_DEFAULT`,`DEFAULT_SHIPPING_ID`,`BILLING_USE_SHIPPING`,`BILLING_EMAIL`,
				`BILLING_TELEPHONE`,`BILLING_FAX`,`BILLING_VENDOR_ATTN`,`BILLING_STREET`,`BILLING_CITY`,`BILLING_ZIP`,
				`BILLING_COUNTRY_ID`,`BILLING_REGION_ID`,`BILLING_REGION`,`BATCH_EXPORT_ORDERS_METHOD`,`BATCH_EXPORT_ORDERS_SCHEDULE`,
				`BATCH_IMPORT_ORDERS_METHOD`,`BATCH_IMPORT_ORDERS_SCHEDULE`,`BATCH_IMPORT_INVENTORY_METHOD`,
				`BATCH_IMPORT_INVENTORY_SCHEDULE`,`BATCH_IMPORT_INVENTORY_TS`,`BATCH_IMPORT_ORDERS_TS`,`CONFIRMATION`,
				`CONFIRMATION_SENT`,`REJECT_REASON`,`PAYOUT_TYPE`,`PAYOUT_METHOD`,`PAYOUT_SCHEDULE`,`BACKORDER_BY_AVAILABILITY`,
				`USE_RESERVED_QTY`,`UDPROD_TEMPLATE_SKU`,`TIERCOM_RATES`,`TIERCOM_FIXED_RULE`,`TIERCOM_FIXED_RATES`,
				`TIERCOM_FIXED_CALC_TYPE`,`VENDOR_LOGIN`,`CUSTOMER_ID`,`ACCOUNT_TYPE`,`IS_FEATURED`,`TIERSHIP_RATES`,
				`TIERSHIP_SIMPLE_RATES`,`VACATION_MODE`,`VACATION_END`,`VACATION_MESSAGE`,`ALLOW_UDRATINGS`,`FORMJURIDIQUE`,
				`RAISONSOCIAL`,`SIRET`,`CAPITAL`,`SITEINTERNET`,`TVAINTRA`,`RIBDOMICILIATION`,`RIBCODEBANQUE`,`RIBCODEGUICHET`,
				`RIBNUMCOMPTE`,`RIBCLE`,`RIBIBAN`,`RIBBIC`,`RIBTITULAIRE`,`TELEPHONE2`,`CIVILITE`,`VENDOR_ATTN2`,ACTIVE
				)";

	
	$sql .="	VALUES ( ";
	$sql .="	:VENDOR_ID,:VENDOR_NOM ,:VENDOR_ATTN ,:EMAIL ,:STREET ,:CITY ,:ZIP ,:COUNTRY_ID ,:REGION_ID, 
				:REGION ,:TELEPHONE ,:FAX ,:STATUS ,:PASSWORD ,:VENDOR_PASSWORD ,:PASSWORD_ENC ,:CARRIER_CODE ,
				:NOTIFY_NEW_ORDER ,:LABEL_TYPE ,:TEST_MODE ,:HANDLING_FEE ,:UPS_SHIPPER_NUMBER ,:CUSTOM_DATA_COMBINED ,
				:CUSTOM_VARS_COMBINED ,:EMAIL_TEMPLATE , :URL_KEY ,	:RANDOM_HASH ,:CREATED_AT , :NOTIFY_LOWSTOCK ,
				:NOTIFY_LOWSTOCK_QTY ,:USE_HANDLING_FEE ,:USE_RATES_FALLBACK ,:ALLOW_SHIPPING_EXTRA_CHARGE ,
				:DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX ,:DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE ,:DEFAULT_SHIPPING_EXTRA_CHARGE ,
				:IS_EXTRA_CHARGE_SHIPPING_DEFAULT ,:DEFAULT_SHIPPING_ID ,:BILLING_USE_SHIPPING ,:BILLING_EMAIL ,
				:BILLING_TELEPHONE ,:BILLING_FAX ,:BILLING_VENDOR_ATTN ,:BILLING_STREET ,:BILLING_CITY ,:BILLING_ZIP ,
				:BILLING_COUNTRY_ID ,:BILLING_REGION_ID ,:BILLING_REGION ,:BATCH_EXPORT_ORDERS_METHOD ,:BATCH_EXPORT_ORDERS_SCHEDULE ,
  				:BATCH_IMPORT_ORDERS_METHOD ,:BATCH_IMPORT_ORDERS_SCHEDULE , :BATCH_IMPORT_INVENTORY_METHOD ,
 				:BATCH_IMPORT_INVENTORY_SCHEDULE ,:BATCH_IMPORT_INVENTORY_TS ,:BATCH_IMPORT_ORDERS_TS ,:CONFIRMATION ,
  				:CONFIRMATION_SENT ,:REJECT_REASON ,:PAYOUT_TYPE ,:PAYOUT_METHOD ,:PAYOUT_SCHEDULE ,:BACKORDER_BY_AVAILABILITY ,
  				:USE_RESERVED_QTY ,:UDPROD_TEMPLATE_SKU ,:TIERCOM_RATES ,:TIERCOM_FIXED_RULE ,:TIERCOM_FIXED_RATES ,
 				:TIERCOM_FIXED_CALC_TYPE ,:VENDOR_LOGIN ,:CUSTOMER_ID ,:ACCOUNT_TYPE ,:IS_FEATURED ,:TIERSHIP_RATES ,
 				:TIERSHIP_SIMPLE_RATES ,:VACATION_MODE ,:VACATION_END ,:VACATION_MESSAGE ,:ALLOW_UDRATINGS ,:FORMJURIDIQUE ,
 				:RAISONSOCIAL ,:SIRET ,:CAPITAL ,:SITEINTERNET ,:TVAINTRA ,:RIBDOMICILIATION ,:RIBCODEBANQUE ,:RIBCODEGUICHET ,
 				:RIBNUMCOMPTE ,:RIBCLE , :RIBIBAN ,:RIBBIC ,:RIBTITULAIRE ,:TELEPHONE2 ,:CIVILITE ,:VENDOR_ATTN2 , 1
				)";

/*	$sql .="	ON DUPLICATE KEY UPDATE "; 
	$sql .="	VENDOR_ID 								=VALUES(VENDOR_ID ),
				VENDOR_NOM 								=VALUES(VENDOR_NOM ),
				VENDOR_ATTN 							=VALUES(VENDOR_ATTN ),
				EMAIL 									=VALUES(EMAIL ),
				STREET 									=VALUES(STREET ),
				CITY 									=VALUES(CITY ),
				ZIP 									=VALUES(ZIP ),
				COUNTRY_ID 								=VALUES(COUNTRY_ID ),
				REGION_ID 								=VALUES(REGION_ID ),
				REGION 									=VALUES(REGION ),
				TELEPHONE 								=VALUES(TELEPHONE ),
				FAX 									=VALUES(FAX ),
				STATUS 									=VALUES(STATUS ),
				PASSWORD 								=VALUES(PASSWORD ),
				VENDOR_PASSWORD 						=VALUES(VENDOR_PASSWORD ),
				PASSWORD_ENC 							=VALUES(PASSWORD_ENC ),
				CARRIER_CODE 							=VALUES(CARRIER_CODE ),
				NOTIFY_NEW_ORDER 						=VALUES(NOTIFY_NEW_ORDER ),
				LABEL_TYPE 								=VALUES(LABEL_TYPE ),
				TEST_MODE 								=VALUES(TEST_MODE ),
				HANDLING_FEE 							=VALUES(HANDLING_FEE ),
				UPS_SHIPPER_NUMBER 						=VALUES(UPS_SHIPPER_NUMBER ),
				CUSTOM_DATA_COMBINED 					=VALUES(CUSTOM_DATA_COMBINED ),
				CUSTOM_VARS_COMBINED 					=VALUES(CUSTOM_VARS_COMBINED ),
				EMAIL_TEMPLATE 							=VALUES(EMAIL_TEMPLATE ),
				URL_KEY 								=VALUES(URL_KEY ),
				RANDOM_HASH 							=VALUES(RANDOM_HASH ),
				CREATED_AT 								=VALUES(CREATED_AT ),
				NOTIFY_LOWSTOCK 						=VALUES(NOTIFY_LOWSTOCK ),
				NOTIFY_LOWSTOCK_QTY 					=VALUES(NOTIFY_LOWSTOCK_QTY ),
				USE_HANDLING_FEE 						=VALUES(USE_HANDLING_FEE ),
				USE_RATES_FALLBACK 						=VALUES(USE_RATES_FALLBACK ),
				ALLOW_SHIPPING_EXTRA_CHARGE 			=VALUES(ALLOW_SHIPPING_EXTRA_CHARGE ),
				DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX	=VALUES(DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX ),
				DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE 		=VALUES(DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE ),
				DEFAULT_SHIPPING_EXTRA_CHARGE 			=VALUES(DEFAULT_SHIPPING_EXTRA_CHARGE ),
				IS_EXTRA_CHARGE_SHIPPING_DEFAULT 		=VALUES(IS_EXTRA_CHARGE_SHIPPING_DEFAULT ),
				DEFAULT_SHIPPING_ID 					=VALUES(DEFAULT_SHIPPING_ID ),
				BILLING_USE_SHIPPING 					=VALUES(BILLING_USE_SHIPPING ),
				BILLING_EMAIL 							=VALUES(BILLING_EMAIL ),
				BILLING_TELEPHONE 						=VALUES(BILLING_TELEPHONE ),
				BILLING_FAX 							=VALUES(BILLING_FAX ),
				BILLING_VENDOR_ATTN 					=VALUES(BILLING_VENDOR_ATTN ),
				BILLING_STREET 							=VALUES(BILLING_STREET ),
				BILLING_CITY 							=VALUES(BILLING_CITY ),
				BILLING_ZIP 							=VALUES(BILLING_ZIP ),
				BILLING_COUNTRY_ID 						=VALUES(BILLING_COUNTRY_ID ),
				BILLING_REGION_ID 						=VALUES(BILLING_REGION_ID ),
				BILLING_REGION 							=VALUES(BILLING_REGION ),
				BATCH_EXPORT_ORDERS_METHOD	 			=VALUES(BATCH_EXPORT_ORDERS_METHOD ),
				BATCH_EXPORT_ORDERS_SCHEDULE 			=VALUES(BATCH_EXPORT_ORDERS_SCHEDULE ),
				BATCH_IMPORT_ORDERS_METHOD 				=VALUES(BATCH_IMPORT_ORDERS_METHOD ),
				BATCH_IMPORT_ORDERS_SCHEDULE 			=VALUES(BATCH_IMPORT_ORDERS_SCHEDULE ),
				BATCH_IMPORT_INVENTORY_METHOD 			=VALUES(BATCH_IMPORT_INVENTORY_METHOD ),
				BATCH_IMPORT_INVENTORY_SCHEDULE 		=VALUES(BATCH_IMPORT_INVENTORY_SCHEDULE ),
				BATCH_IMPORT_INVENTORY_TS 				=VALUES(BATCH_IMPORT_INVENTORY_TS ),
				BATCH_IMPORT_ORDERS_TS 					=VALUES(BATCH_IMPORT_ORDERS_TS ),
				CONFIRMATION 							=VALUES(CONFIRMATION ),
				CONFIRMATION_SENT 						=VALUES(CONFIRMATION_SENT ),
				REJECT_REASON 							=VALUES(REJECT_REASON ),
				PAYOUT_TYPE 							=VALUES(PAYOUT_TYPE ),
				PAYOUT_METHOD 							=VALUES(PAYOUT_METHOD ),
				PAYOUT_SCHEDULE 						=VALUES(PAYOUT_SCHEDULE ),
				BACKORDER_BY_AVAILABILITY 				=VALUES(BACKORDER_BY_AVAILABILITY ),
				USE_RESERVED_QTY						=VALUES(USE_RESERVED_QTY ),
				UDPROD_TEMPLATE_SKU 					=VALUES(UDPROD_TEMPLATE_SKU ),
				TIERCOM_RATES 							=VALUES(TIERCOM_RATES ),
				TIERCOM_FIXED_RULE 						=VALUES(TIERCOM_FIXED_RULE ),
				TIERCOM_FIXED_RATES 					=VALUES(TIERCOM_FIXED_RATES ),
				TIERCOM_FIXED_CALC_TYPE 				=VALUES(TIERCOM_FIXED_CALC_TYPE ),
				VENDOR_LOGIN 							=VALUES(VENDOR_LOGIN ),
				CUSTOMER_ID 							=VALUES(CUSTOMER_ID ),
				ACCOUNT_TYPE 							=VALUES(ACCOUNT_TYPE ),
				IS_FEATURED 							=VALUES(IS_FEATURED ),
				TIERSHIP_RATES 							=VALUES(TIERSHIP_RATES ),
				TIERSHIP_SIMPLE_RATES 					=VALUES(TIERSHIP_SIMPLE_RATES ),
				VACATION_MODE 							=VALUES(VACATION_MODE ),
				VACATION_END 							=VALUES(VACATION_END ),
				VACATION_MESSAGE 						=VALUES(VACATION_MESSAGE ),
				ALLOW_UDRATINGS 						=VALUES(ALLOW_UDRATINGS ),
				FORMJURIDIQUE 							=VALUES(FORMJURIDIQUE ),
				RAISONSOCIAL 							=VALUES(RAISONSOCIAL ),
				SIRET 									=VALUES(SIRET ),
				CAPITAL 								=VALUES(CAPITAL ),
				SITEINTERNET 							=VALUES(SITEINTERNET ),
				TVAINTRA 								=VALUES(TVAINTRA ),
				RIBDOMICILIATION 						=VALUES(RIBDOMICILIATION ),
				RIBCODEBANQUE 							=VALUES(RIBCODEBANQUE ),
				RIBCODEGUICHET 							=VALUES(RIBCODEGUICHET ),
				RIBNUMCOMPTE 							=VALUES(RIBNUMCOMPTE ),
				RIBCLE 									=VALUES(RIBCLE ),
				RIBIBAN 								=VALUES(RIBIBAN ),
				RIBBIC 									=VALUES(RIBBIC ),
				RIBTITULAIRE 							=VALUES(RIBTITULAIRE ),
				TELEPHONE2 								=VALUES(TELEPHONE2 ),
				CIVILITE				 				=VALUES(CIVILITE ),
				VENDOR_ATTN2 							=VALUES(VENDOR_ATTN2 ),
				ACTIVE 									=VALUES(ACTIVE) 
				";
    */
	$this -> stmtVendor = $this -> prepare($sql);

	}
	/**
	 * F Insertin des enregistrement vendeur Avahis dans sfk_vendor Soukflux
	 * @param object array des enregistrement vendeur Avahis 
	 * @return boolean true si requéte éxécuté
	 */
	public function addRowValVendor($vendor) {
	$null = null;
	$vendor['custom_vars_combined'] = $this->filtre_string($vendor['custom_vars_combined']);
	
	
	$this -> stmtVendor -> bindValue(':VENDOR_ID',$vendor['vendor_id'] ? $vendor['vendor_id']:"666");
 	$this -> stmtVendor -> bindValue(':VENDOR_NOM',$vendor['vendor_name'] ? $vendor['vendor_name']:$null);
	$this -> stmtVendor -> bindValue(':VENDOR_ATTN',$vendor['vendor_attn'] ? $vendor['vendor_attn']:$null);
	$this -> stmtVendor -> bindValue(':EMAIL',$vendor['email'] ? $vendor['email']:$null);
	$this -> stmtVendor -> bindValue(':STREET',$vendor['street'] ? $vendor['street']:$null);
	$this -> stmtVendor -> bindValue(':CITY',$vendor['city'] ? $vendor['city']:$null);
	$this -> stmtVendor -> bindValue(':ZIP',$vendor['zip'] ? $vendor['zip']:$null);
	$this -> stmtVendor -> bindValue(':COUNTRY_ID',$vendor['country_id'] ? $vendor['country_id']:$null);
	$this -> stmtVendor -> bindValue(':REGION_ID',$vendor['region_id'] ? $vendor['region_id']:$null);
	$this -> stmtVendor -> bindValue(':REGION',$vendor['region'] ? $vendor['region']:$null);
	$this -> stmtVendor -> bindValue(':TELEPHONE',$vendor['telephone'] ? $vendor['telephone']:$null);
	$this -> stmtVendor -> bindValue(':FAX',$vendor['fax'] ? $vendor['fax']:$null);
	$this -> stmtVendor -> bindValue(':STATUS',$vendor['status'] ? $vendor['status']:$null);
	$this -> stmtVendor -> bindValue(':PASSWORD',$vendor['password'] ? $vendor['password']:$null);
	$this -> stmtVendor -> bindValue(':VENDOR_PASSWORD',$vendor['password_hash'] ? $vendor['password_hash']:$null);
	$this -> stmtVendor -> bindValue(':PASSWORD_ENC',$vendor['password_enc'] ? $vendor['password_enc']:$null);
	$this -> stmtVendor -> bindValue(':CARRIER_CODE',$vendor['carrier_code'] ? $vendor['carrier_code']:$null);
	$this -> stmtVendor -> bindValue(':NOTIFY_NEW_ORDER',$vendor['notify_new_order'] ? $vendor['notify_new_order']:$null);
	$this -> stmtVendor -> bindValue(':LABEL_TYPE',$vendor['label_type'] ? $vendor['label_type']:$null);
	$this -> stmtVendor -> bindValue(':TEST_MODE',$vendor['test_mode'] ? $vendor['test_mode']:$null);
	$this -> stmtVendor -> bindValue(':HANDLING_FEE',$vendor['handling_fee'] ? $vendor['handling_fee']:$null);
	$this -> stmtVendor -> bindValue(':UPS_SHIPPER_NUMBER',$vendor['ups_shipper_number'] ? $vendor['ups_shipper_number']:$null);
	$this -> stmtVendor -> bindValue(':CUSTOM_DATA_COMBINED',$vendor['custom_data_combined'] ? $vendor['custom_data_combined']:$null);
	$this -> stmtVendor -> bindValue(':CUSTOM_VARS_COMBINED',$vendor['custom_vars_combined'] ? $vendor['custom_vars_combined']:$null);
	$this -> stmtVendor -> bindValue(':EMAIL_TEMPLATE',$vendor['email_template'] ? $vendor['email_template']:$null);
	$this -> stmtVendor -> bindValue(':URL_KEY',$vendor['url_key'] ? $vendor['url_key']:$null);
	$this -> stmtVendor -> bindValue(':RANDOM_HASH',$vendor['random_hash'] ? $vendor['random_hash']:$null);
	$this -> stmtVendor -> bindValue(':CREATED_AT',$vendor['created_at'] ? $vendor['created_at']:$null);
	$this -> stmtVendor -> bindValue(':NOTIFY_LOWSTOCK',$vendor['notify_lowstock'] ? $vendor['notify_lowstock']:$null);
 	$this -> stmtVendor -> bindValue(':NOTIFY_LOWSTOCK_QTY',$vendor['notify_lowstock_qty'] ? $vendor['notify_lowstock_qty']:$null);
	$this -> stmtVendor -> bindValue(':USE_HANDLING_FEE',$vendor['use_handling_fee'] ? $vendor['use_handling_fee']:$null);
	$this -> stmtVendor -> bindValue(':USE_RATES_FALLBACK',$vendor['use_rates_fallback'] ? $vendor['use_rates_fallback']:$null);
	$this -> stmtVendor -> bindValue(':ALLOW_SHIPPING_EXTRA_CHARGE',$vendor['allow_shipping_extra_charge'] ? $vendor['allow_shipping_extra_charge']:$null);
	$this -> stmtVendor -> bindValue(':DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX',$vendor['default_shipping_extra_charge_suffix'] ? $vendor['default_shipping_extra_charge_suffix']:$null);
	$this -> stmtVendor -> bindValue(':DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE',$vendor['default_shipping_extra_charge_type'] ? $vendor['default_shipping_extra_charge_type']:$null);
	$this -> stmtVendor -> bindValue(':DEFAULT_SHIPPING_EXTRA_CHARGE',$vendor['default_shipping_extra_charge'] ? $vendor['default_shipping_extra_charge']:$null);
	$this -> stmtVendor -> bindValue(':IS_EXTRA_CHARGE_SHIPPING_DEFAULT',$vendor['is_extra_charge_shipping_default'] ? $vendor['is_extra_charge_shipping_default']:$null);
	$this -> stmtVendor -> bindValue(':DEFAULT_SHIPPING_ID',$vendor['default_shipping_id'] ? $vendor['default_shipping_id']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_USE_SHIPPING',$vendor['billing_use_shipping'] ? $vendor['billing_use_shipping']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_EMAIL',$vendor['billing_email'] ? $vendor['billing_email']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_TELEPHONE',$vendor['billing_telephone'] ? $vendor['billing_telephone']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_FAX',$vendor['billing_fax'] ? $vendor['billing_fax']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_VENDOR_ATTN',$vendor['billing_vendor_attn'] ? $vendor['billing_vendor_attn']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_STREET',$vendor['billing_street'] ? $vendor['billing_street']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_CITY',$vendor['billing_city'] ? $vendor['billing_city']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_ZIP',$vendor['billing_zip'] ? $vendor['billing_zip']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_COUNTRY_ID',$vendor['billing_country_id'] ? $vendor['billing_country_id']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_REGION_ID',$vendor['billing_region_id'] ? $vendor['billing_region_id']:$null);
	$this -> stmtVendor -> bindValue(':BILLING_REGION',$vendor['billing_region'] ? $vendor['billing_region']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_EXPORT_ORDERS_METHOD',$vendor['batch_export_orders_method'] ? $vendor['batch_export_orders_method']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_EXPORT_ORDERS_SCHEDULE',$vendor['batch_export_orders_schedule'] ? $vendor['batch_export_orders_schedule']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_IMPORT_ORDERS_METHOD',$vendor['batch_import_orders_method'] ? $vendor['batch_import_orders_method']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_IMPORT_ORDERS_SCHEDULE',$vendor['batch_import_orders_schedule'] ? $vendor['batch_import_orders_schedule']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_IMPORT_INVENTORY_METHOD',$vendor['batch_import_inventory_method'] ? $vendor['batch_import_inventory_method']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_IMPORT_INVENTORY_SCHEDULE',$vendor['batch_import_inventory_schedule'] ? $vendor['batch_import_inventory_schedule']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_IMPORT_INVENTORY_TS',$vendor['batch_import_inventory_ts'] ? $vendor['batch_import_inventory_ts']:$null);
	$this -> stmtVendor -> bindValue(':BATCH_IMPORT_ORDERS_TS',$vendor['batch_import_orders_ts'] ? $vendor['batch_import_orders_ts']:$null);
	$this -> stmtVendor -> bindValue(':CONFIRMATION',$vendor['confirmation'] ? $vendor['confirmation']:$null);
	$this -> stmtVendor -> bindValue(':CONFIRMATION_SENT',$vendor['confirmation_sent'] ? $vendor['confirmation_sent']:$null);
	$this -> stmtVendor -> bindValue(':REJECT_REASON',$vendor['reject_reason'] ? $vendor['reject_reason']:$null);
	$this -> stmtVendor -> bindValue(':PAYOUT_TYPE',$vendor['payout_type'] ? $vendor['payout_type']:$null);
	$this -> stmtVendor -> bindValue(':PAYOUT_METHOD',$vendor['payout_method'] ? $vendor['payout_method']:$null);
	$this -> stmtVendor -> bindValue(':PAYOUT_SCHEDULE',$vendor['payout_schedule'] ? $vendor['payout_schedule']:$null);
	$this -> stmtVendor -> bindValue(':BACKORDER_BY_AVAILABILITY',$vendor['backorder_by_availability'] ? $vendor['backorder_by_availability']:$null);
	$this -> stmtVendor -> bindValue(':USE_RESERVED_QTY',$vendor['use_reserved_qty'] ? $vendor['use_reserved_qty']:$null);
	$this -> stmtVendor -> bindValue(':UDPROD_TEMPLATE_SKU',$vendor['udprod_template_sku'] ? $vendor['udprod_template_sku']:$null);
	$this -> stmtVendor -> bindValue(':TIERCOM_RATES',$vendor['tiercom_rates'] ? $vendor['tiercom_rates']:$null);
	$this -> stmtVendor -> bindValue(':TIERCOM_FIXED_RULE',$vendor['tiercom_fixed_rule'] ? $vendor['tiercom_fixed_rule']:$null);
	$this -> stmtVendor -> bindValue(':TIERCOM_FIXED_RATES',$vendor['tiercom_fixed_rates'] ? $vendor['tiercom_fixed_rates']:$null);
	$this -> stmtVendor -> bindValue(':TIERCOM_FIXED_CALC_TYPE',$vendor['tiercom_fixed_calc_type'] ? $vendor['tiercom_fixed_calc_type']:$null);
	$this -> stmtVendor -> bindValue(':VENDOR_LOGIN',$vendor['username'] ? $vendor['username']:$null);
	$this -> stmtVendor -> bindValue(':CUSTOMER_ID',$vendor['customer_id'] ? $vendor['customer_id']:$null);
	$this -> stmtVendor -> bindValue(':ACCOUNT_TYPE',$vendor['account_type'] ? $vendor['account_type']:$null);
	$this -> stmtVendor -> bindValue(':IS_FEATURED',$vendor['is_featured'] ? $vendor['is_featured']:$null);
	$this -> stmtVendor -> bindValue(':TIERSHIP_RATES',$vendor['tiership_rates'] ? $vendor['tiership_rates']:$null);
	$this -> stmtVendor -> bindValue(':TIERSHIP_SIMPLE_RATES',$vendor['tiership_simple_rates'] ? $vendor['tiership_simple_rates']:$null);
	$this -> stmtVendor -> bindValue(':VACATION_MODE',$vendor['vacation_mode'] ? $vendor['vacation_mode']:$null);
	$this -> stmtVendor -> bindValue(':VACATION_END',$vendor['vacation_end'] ? $vendor['vacation_end']:$null);
	$this -> stmtVendor -> bindValue(':VACATION_MESSAGE',$vendor['vacation_message'] ? $vendor['vacation_message']:$null);
	$this -> stmtVendor -> bindValue(':ALLOW_UDRATINGS',$vendor['allow_udratings'] ? $vendor['allow_udratings']:$null);
	$this -> stmtVendor -> bindValue(':FORMJURIDIQUE',$vendor['formjuridique'] ? $vendor['formjuridique']:$null);
	$this -> stmtVendor -> bindValue(':RAISONSOCIAL',$vendor['raisonsocial'] ? $vendor['raisonsocial']:$null);
	$this -> stmtVendor -> bindValue(':SIRET',$vendor['siret'] ? $vendor['siret']:$null);
	$this -> stmtVendor -> bindValue(':CAPITAL',$vendor['capital'] ? $vendor['capital']:$null);
	$this -> stmtVendor -> bindValue(':SITEINTERNET',$vendor['siteinternet'] ? $vendor['siteinternet']:$null);
	$this -> stmtVendor -> bindValue(':TVAINTRA',$vendor['tvaintra'] ? $vendor['tvaintra']:$null);
	$this -> stmtVendor -> bindValue(':RIBDOMICILIATION',$vendor['ribdomiciliation'] ? $vendor['ribdomiciliation']:$null);
	$this -> stmtVendor -> bindValue(':RIBCODEBANQUE',$vendor['ribcodebanque'] ? $vendor['ribcodebanque']:$null);
	$this -> stmtVendor -> bindValue(':RIBCODEGUICHET',$vendor['ribcodeguichet'] ? $vendor['ribcodeguichet']:$null);
	$this -> stmtVendor -> bindValue(':RIBNUMCOMPTE',$vendor['ribnumcompte'] ? $vendor['ribnumcompte']:$null);
	$this -> stmtVendor -> bindValue(':RIBCLE',$vendor['ribcle'] ? $vendor['ribcle']:$null);
	$this -> stmtVendor -> bindValue(':RIBIBAN',$vendor['ribiban'] ? $vendor['ribiban']:$null);
	$this -> stmtVendor -> bindValue(':RIBBIC',$vendor['ribbic'] ? $vendor['ribbic']:$null);
	$this -> stmtVendor -> bindValue(':RIBTITULAIRE',$vendor['ribtitulaire'] ? $vendor['ribtitulaire']:$null);
	$this -> stmtVendor -> bindValue(':TELEPHONE2',$vendor['telephone2'] ? $vendor['telephone2']:$null);
	$this -> stmtVendor -> bindValue(':CIVILITE',$vendor['civilite'] ? $vendor['civilite']:$null);
	$this -> stmtVendor -> bindValue(':VENDOR_ATTN2',$vendor['vendor_attn2'] ? $vendor['vendor_attn2']:$null);


  	
	return $this -> stmtVendor -> execute();
	

	}

    public function addRowValVendorUpdate($vendor) {
        $null = null;
        $vendor['custom_vars_combined'] = $this->filtre_string($vendor['custom_vars_combined']);
        
        
        $this -> stmtVendorUpdate -> bindValue(':VENDOR_ID',$vendor['vendor_id'] ? $vendor['vendor_id']:"666");
        $this -> stmtVendorUpdate -> bindValue(':VENDOR_NOM',$vendor['vendor_name'] ? $vendor['vendor_name']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VENDOR_ATTN',$vendor['vendor_attn'] ? $vendor['vendor_attn']:$null);
        $this -> stmtVendorUpdate -> bindValue(':EMAIL',$vendor['email'] ? $vendor['email']:$null);
        $this -> stmtVendorUpdate -> bindValue(':STREET',$vendor['street'] ? $vendor['street']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CITY',$vendor['city'] ? $vendor['city']:$null);
        $this -> stmtVendorUpdate -> bindValue(':ZIP',$vendor['zip'] ? $vendor['zip']:$null);
        $this -> stmtVendorUpdate -> bindValue(':COUNTRY_ID',$vendor['country_id'] ? $vendor['country_id']:$null);
        $this -> stmtVendorUpdate -> bindValue(':REGION_ID',$vendor['region_id'] ? $vendor['region_id']:$null);
        $this -> stmtVendorUpdate -> bindValue(':REGION',$vendor['region'] ? $vendor['region']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TELEPHONE',$vendor['telephone'] ? $vendor['telephone']:$null);
        $this -> stmtVendorUpdate -> bindValue(':FAX',$vendor['fax'] ? $vendor['fax']:$null);
        $this -> stmtVendorUpdate -> bindValue(':STATUS',$vendor['status'] ? $vendor['status']:$null);
        $this -> stmtVendorUpdate -> bindValue(':PASSWORD',$vendor['password'] ? $vendor['password']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VENDOR_PASSWORD',$vendor['password_hash'] ? $vendor['password_hash']:$null);
        $this -> stmtVendorUpdate -> bindValue(':PASSWORD_ENC',$vendor['password_enc'] ? $vendor['password_enc']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CARRIER_CODE',$vendor['carrier_code'] ? $vendor['carrier_code']:$null);
        $this -> stmtVendorUpdate -> bindValue(':NOTIFY_NEW_ORDER',$vendor['notify_new_order'] ? $vendor['notify_new_order']:$null);
        $this -> stmtVendorUpdate -> bindValue(':LABEL_TYPE',$vendor['label_type'] ? $vendor['label_type']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TEST_MODE',$vendor['test_mode'] ? $vendor['test_mode']:$null);
        $this -> stmtVendorUpdate -> bindValue(':HANDLING_FEE',$vendor['handling_fee'] ? $vendor['handling_fee']:$null);
        $this -> stmtVendorUpdate -> bindValue(':UPS_SHIPPER_NUMBER',$vendor['ups_shipper_number'] ? $vendor['ups_shipper_number']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CUSTOM_DATA_COMBINED',$vendor['custom_data_combined'] ? $vendor['custom_data_combined']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CUSTOM_VARS_COMBINED',$vendor['custom_vars_combined'] ? $vendor['custom_vars_combined']:$null);
        $this -> stmtVendorUpdate -> bindValue(':EMAIL_TEMPLATE',$vendor['email_template'] ? $vendor['email_template']:$null);
        $this -> stmtVendorUpdate -> bindValue(':URL_KEY',$vendor['url_key'] ? $vendor['url_key']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RANDOM_HASH',$vendor['random_hash'] ? $vendor['random_hash']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CREATED_AT',$vendor['created_at'] ? $vendor['created_at']:$null);
        $this -> stmtVendorUpdate -> bindValue(':NOTIFY_LOWSTOCK',$vendor['notify_lowstock'] ? $vendor['notify_lowstock']:$null);
        $this -> stmtVendorUpdate -> bindValue(':NOTIFY_LOWSTOCK_QTY',$vendor['notify_lowstock_qty'] ? $vendor['notify_lowstock_qty']:$null);
        $this -> stmtVendorUpdate -> bindValue(':USE_HANDLING_FEE',$vendor['use_handling_fee'] ? $vendor['use_handling_fee']:$null);
        $this -> stmtVendorUpdate -> bindValue(':USE_RATES_FALLBACK',$vendor['use_rates_fallback'] ? $vendor['use_rates_fallback']:$null);
        $this -> stmtVendorUpdate -> bindValue(':ALLOW_SHIPPING_EXTRA_CHARGE',$vendor['allow_shipping_extra_charge'] ? $vendor['allow_shipping_extra_charge']:$null);
        $this -> stmtVendorUpdate -> bindValue(':DEFAULT_SHIPPING_EXTRA_CHARGE_SUFFIX',$vendor['default_shipping_extra_charge_suffix'] ? $vendor['default_shipping_extra_charge_suffix']:$null);
        $this -> stmtVendorUpdate -> bindValue(':DEFAULT_SHIPPING_EXTRA_CHARGE_TYPE',$vendor['default_shipping_extra_charge_type'] ? $vendor['default_shipping_extra_charge_type']:$null);
        $this -> stmtVendorUpdate -> bindValue(':DEFAULT_SHIPPING_EXTRA_CHARGE',$vendor['default_shipping_extra_charge'] ? $vendor['default_shipping_extra_charge']:$null);
        $this -> stmtVendorUpdate -> bindValue(':IS_EXTRA_CHARGE_SHIPPING_DEFAULT',$vendor['is_extra_charge_shipping_default'] ? $vendor['is_extra_charge_shipping_default']:$null);
        $this -> stmtVendorUpdate -> bindValue(':DEFAULT_SHIPPING_ID',$vendor['default_shipping_id'] ? $vendor['default_shipping_id']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_USE_SHIPPING',$vendor['billing_use_shipping'] ? $vendor['billing_use_shipping']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_EMAIL',$vendor['billing_email'] ? $vendor['billing_email']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_TELEPHONE',$vendor['billing_telephone'] ? $vendor['billing_telephone']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_FAX',$vendor['billing_fax'] ? $vendor['billing_fax']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_VENDOR_ATTN',$vendor['billing_vendor_attn'] ? $vendor['billing_vendor_attn']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_STREET',$vendor['billing_street'] ? $vendor['billing_street']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_CITY',$vendor['billing_city'] ? $vendor['billing_city']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_ZIP',$vendor['billing_zip'] ? $vendor['billing_zip']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_COUNTRY_ID',$vendor['billing_country_id'] ? $vendor['billing_country_id']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_REGION_ID',$vendor['billing_region_id'] ? $vendor['billing_region_id']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BILLING_REGION',$vendor['billing_region'] ? $vendor['billing_region']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_EXPORT_ORDERS_METHOD',$vendor['batch_export_orders_method'] ? $vendor['batch_export_orders_method']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_EXPORT_ORDERS_SCHEDULE',$vendor['batch_export_orders_schedule'] ? $vendor['batch_export_orders_schedule']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_IMPORT_ORDERS_METHOD',$vendor['batch_import_orders_method'] ? $vendor['batch_import_orders_method']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_IMPORT_ORDERS_SCHEDULE',$vendor['batch_import_orders_schedule'] ? $vendor['batch_import_orders_schedule']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_IMPORT_INVENTORY_METHOD',$vendor['batch_import_inventory_method'] ? $vendor['batch_import_inventory_method']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_IMPORT_INVENTORY_SCHEDULE',$vendor['batch_import_inventory_schedule'] ? $vendor['batch_import_inventory_schedule']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_IMPORT_INVENTORY_TS',$vendor['batch_import_inventory_ts'] ? $vendor['batch_import_inventory_ts']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BATCH_IMPORT_ORDERS_TS',$vendor['batch_import_orders_ts'] ? $vendor['batch_import_orders_ts']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CONFIRMATION',$vendor['confirmation'] ? $vendor['confirmation']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CONFIRMATION_SENT',$vendor['confirmation_sent'] ? $vendor['confirmation_sent']:$null);
        $this -> stmtVendorUpdate -> bindValue(':REJECT_REASON',$vendor['reject_reason'] ? $vendor['reject_reason']:$null);
        $this -> stmtVendorUpdate -> bindValue(':PAYOUT_TYPE',$vendor['payout_type'] ? $vendor['payout_type']:$null);
        $this -> stmtVendorUpdate -> bindValue(':PAYOUT_METHOD',$vendor['payout_method'] ? $vendor['payout_method']:$null);
        $this -> stmtVendorUpdate -> bindValue(':PAYOUT_SCHEDULE',$vendor['payout_schedule'] ? $vendor['payout_schedule']:$null);
        $this -> stmtVendorUpdate -> bindValue(':BACKORDER_BY_AVAILABILITY',$vendor['backorder_by_availability'] ? $vendor['backorder_by_availability']:$null);
        $this -> stmtVendorUpdate -> bindValue(':USE_RESERVED_QTY',$vendor['use_reserved_qty'] ? $vendor['use_reserved_qty']:$null);
        $this -> stmtVendorUpdate -> bindValue(':UDPROD_TEMPLATE_SKU',$vendor['udprod_template_sku'] ? $vendor['udprod_template_sku']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TIERCOM_RATES',$vendor['tiercom_rates'] ? $vendor['tiercom_rates']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TIERCOM_FIXED_RULE',$vendor['tiercom_fixed_rule'] ? $vendor['tiercom_fixed_rule']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TIERCOM_FIXED_RATES',$vendor['tiercom_fixed_rates'] ? $vendor['tiercom_fixed_rates']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TIERCOM_FIXED_CALC_TYPE',$vendor['tiercom_fixed_calc_type'] ? $vendor['tiercom_fixed_calc_type']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VENDOR_LOGIN',$vendor['username'] ? $vendor['username']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CUSTOMER_ID',$vendor['customer_id'] ? $vendor['customer_id']:$null);
        $this -> stmtVendorUpdate -> bindValue(':ACCOUNT_TYPE',$vendor['account_type'] ? $vendor['account_type']:$null);
        $this -> stmtVendorUpdate -> bindValue(':IS_FEATURED',$vendor['is_featured'] ? $vendor['is_featured']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TIERSHIP_RATES',$vendor['tiership_rates'] ? $vendor['tiership_rates']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TIERSHIP_SIMPLE_RATES',$vendor['tiership_simple_rates'] ? $vendor['tiership_simple_rates']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VACATION_MODE',$vendor['vacation_mode'] ? $vendor['vacation_mode']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VACATION_END',$vendor['vacation_end'] ? $vendor['vacation_end']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VACATION_MESSAGE',$vendor['vacation_message'] ? $vendor['vacation_message']:$null);
        $this -> stmtVendorUpdate -> bindValue(':ALLOW_UDRATINGS',$vendor['allow_udratings'] ? $vendor['allow_udratings']:$null);
        $this -> stmtVendorUpdate -> bindValue(':FORMJURIDIQUE',$vendor['formjuridique'] ? $vendor['formjuridique']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RAISONSOCIAL',$vendor['raisonsocial'] ? $vendor['raisonsocial']:$null);
        $this -> stmtVendorUpdate -> bindValue(':SIRET',$vendor['siret'] ? $vendor['siret']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CAPITAL',$vendor['capital'] ? $vendor['capital']:$null);
        $this -> stmtVendorUpdate -> bindValue(':SITEINTERNET',$vendor['siteinternet'] ? $vendor['siteinternet']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TVAINTRA',$vendor['tvaintra'] ? $vendor['tvaintra']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBDOMICILIATION',$vendor['ribdomiciliation'] ? $vendor['ribdomiciliation']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBCODEBANQUE',$vendor['ribcodebanque'] ? $vendor['ribcodebanque']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBCODEGUICHET',$vendor['ribcodeguichet'] ? $vendor['ribcodeguichet']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBNUMCOMPTE',$vendor['ribnumcompte'] ? $vendor['ribnumcompte']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBCLE',$vendor['ribcle'] ? $vendor['ribcle']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBIBAN',$vendor['ribiban'] ? $vendor['ribiban']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBBIC',$vendor['ribbic'] ? $vendor['ribbic']:$null);
        $this -> stmtVendorUpdate -> bindValue(':RIBTITULAIRE',$vendor['ribtitulaire'] ? $vendor['ribtitulaire']:$null);
        $this -> stmtVendorUpdate -> bindValue(':TELEPHONE2',$vendor['telephone2'] ? $vendor['telephone2']:$null);
        $this -> stmtVendorUpdate -> bindValue(':CIVILITE',$vendor['civilite'] ? $vendor['civilite']:$null);
        $this -> stmtVendorUpdate -> bindValue(':VENDOR_ATTN2',$vendor['vendor_attn2'] ? $vendor['vendor_attn2']:$null);
    
    
        
        return $this -> stmtVendorUpdate -> execute();
    

    }

	public function setInactiveVendor($listID){
		$sqllist = implode(',', $listID);
		$sql = "UPDATE sfk_vendor SET ACTIVE = 0 WHERE VENDOR_ID NOT IN ($sqllist)";
		$this->getone($sql);
		$sql2 = 'SELECT COUNT(*) FROM sfk_vendor where ACTIVE = 0';
		return $this->getone($sql2);

	}
	
	public function getCountVenteAvahis()
	{
		$sql = "SELECT COUNT(*) as c FROM sfk_product_ecommercant_stock_prix WHERE QUANTITY > 0 AND VENDOR_ID = 24";
		
		return $this->getone($sql);
	}


	/**
	 * F Enléve les caractéres \n\r les balises et encode en UTF8
	 * @param string a filtré
	 * @return string filtré
	 */
	 public function filtre_string($string){
		$stringFiltrer=	str_replace(array("\r\n","\n","\r"),'', $string);
		$stringFiltrer=	str_replace(array(";"),'>', $stringFiltrer);
		$stringFiltrer = utf8_encode(str_replace(array("\r\n","\n","\r"),'', strip_tags($stringFiltrer)));
		return $stringFiltrer;
		
	}
	 /**
	 * F Fonction permettant d'obtenir toutes les informations pour un Vendeur
	 * grace à son id produit
	 * @param $vendor 	l'id du vendeur dont on veux les informations
	 */
	public function getInfoVendor($vendor)
	{
		$sql = "SELECT * FROM sfk_vendor WHERE VENDOR_ID = '$vendor'";
		//$stmt = $this->prepare($sql);
		
		$stmt = $this -> prepare($sql); 
		return $this->getAllPrepa($stmt);
	}
	 /**
	* Préparation de la requete qui va insérer les données dans la table  sfk_catalog_product
	*
	*/
	public function prepareStmteditProd() {
		
	 	
		$sql =
		"UPDATE  sfk_catalog_product SET  
		sku = :sku , 
		name = :name , 
		description = :description , 
		description_html = :description_html ,
		short_description = :short_description , 
		price = :price , 
		weight = :weight , 
		country_of_manufacture = :country_of_manufacture ,
		ean = :ean ,  
		meta_description = :meta_description , 
		meta_keyword = :meta_keyword , 
		meta_title = :meta_title , 
		image= :image , 
		small_image = :small_image , 
		thumbnail = :thumbnail ,
		local_image = :local_image,
		local_small_image = :local_small_image,
		local_thumbnail = :local_thumbnail, 
		manufacturer = :manufacturer , 
		dropship_vendor = :dropship_vendor 
		WHERE id_produit = :id_produit ";	
			
		$this -> stmteditProd = $this -> prepare($sql);
		
	}
	/**
	 *  Insert des enregistrement des modification de la la table  sfk_catalog_product
	 * @param object array des enregistrement vendeur Avahis 
	 * @return boolean true si requéte éxécuté
	 */
	 public function editRowProductAvahis($caract) {
	     
		$null = null;
		$this -> stmteditProd -> bindValue(':id_produit',				$caract['id_produit']);
		$this -> stmteditProd -> bindValue(':sku',						$caract['sku'] ? $caract['sku']:$null);
		$this -> stmteditProd -> bindValue(':name',						$caract['name'] ? $caract['name']:$null);
		$this -> stmteditProd -> bindValue(':description',				$caract['description'] ? $caract['description']:$null);
		$this -> stmteditProd -> bindValue(':description_html',			$caract['description_html'] ? $caract['description_html']:$null);
		$this -> stmteditProd -> bindValue(':short_description',		$caract['short_description'] ? $caract['short_description']:$null);
		$this -> stmteditProd -> bindValue(':price',					$caract['price'] ? str_replace(",", ".", $caract['price']):$null);
		$this -> stmteditProd -> bindValue(':weight',					$caract['weight'] ? str_replace(",",".",$caract['weight']) : $null);
		$this -> stmteditProd -> bindValue(':country_of_manufacture',	$caract['country_of_manufacture'] ? $caract['country_of_manufacture']:$null);
        $this -> stmteditProd -> bindValue(':ean',                      $caract['ean'] ? $caract['ean']:$null);
		$this -> stmteditProd -> bindValue(':meta_description',			$caract['meta_description'] ? $caract['meta_description']:$null);
		$this -> stmteditProd -> bindValue(':meta_keyword',				$caract['meta_keyword'] ? $caract['meta_keyword']:$null);
		$this -> stmteditProd -> bindValue(':meta_title',				$caract['meta_title'] ? $caract['meta_title']:$null);
		$this -> stmteditProd -> bindValue(':image',					$caract['image'] ? $caract['image']:$null);
		$this -> stmteditProd -> bindValue(':small_image',				$caract['small_image'] ? $caract['small_image']:$null);
		$this -> stmteditProd -> bindValue(':thumbnail',				$caract['thumbnail'] ? $caract['thumbnail']:$null);
        $this -> stmteditProd -> bindValue(':local_image',              $caract['local_image'] ? $caract['local_image']:$null);
        $this -> stmteditProd -> bindValue(':local_small_image',        $caract['local_small_image'] ? $caract['local_small_image']:$null);
        $this -> stmteditProd -> bindValue(':local_thumbnail',          $caract['local_thumbnail'] ? $caract['local_thumbnail']:$null);
		$this -> stmteditProd -> bindValue(':manufacturer',				$caract['manufacturer'] ? ucfirst(strtolower($caract['manufacturer'])) : $null);
		$this -> stmteditProd -> bindValue(':dropship_vendor',			$caract['dropship_vendor'] ? $caract['dropship_vendor']:'24');

		if ($this -> stmteditProd -> execute()){
			$mess = "les caractéristiques ont été sauvegardées <br>";
			return $mess ;
		}
		$mess = "les caractéristiques n'ont pas été sauvegardées <br>";
		return $mess ;
	}


	/**
	 * F set delete le produit selectionné
	 * @param $idProduct l'id du produit a modifié
	 * @param $delete a suprimé ou pas (1 ou 0) 
	 */
	public function setSupProduct($idProduct, $delete)
	{
		
		$sql = "UPDATE sfk_catalog_product SET `delete` = :delete
				WHERE id_produit = :id_produit 
				";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":id_produit", $idProduct);
		$stmt -> bindValue(":delete", $delete);
		
		$stmt -> execute();
	}
	public function getAllDeletedProds()
	{
		$sql = "SELECT * FROM sfk_catalog_product
				WHERE 
				`delete` = 1 
				";
		
		$stmt = $this -> prepare($sql); 
		
		return $this->getAllPrepa($stmt);
	}
	/**
	 * F Fonction permettant de mettre a jour le champ categories 
	 * @param $idProd	id produit Avahis
	 * @param $catId 	nombre de produits dans cette catégorie
	 */
	public function updateCatProduct($idProd, $catId)
	{
		$sql = "UPDATE sfk_catalog_product SET categories = :catId  WHERE id_produit = :idProd";


		$stmt = $this -> prepare($sql); 
		$stmt -> bindValue(":idProd", $idProd);	
		$stmt -> bindValue(":catId", $catId);
			
		if($stmt -> execute())return true;
		return false ;
	}
	/**
	 * F Donne tous les nom et l'id vendeur qui est associé a un produit ou pour un produit spécifié
	 * @param $idProduit spécifie l'association sur un produit donné.
	 */
	public function getAllVendorAssoc($idProduit = null) {
		$sql = " SELECT sv.VENDOR_ID AS vendor_id , sv.VENDOR_NOM AS vendor_nom , sa.id_produit_ecommercant AS id_prod_com FROM  sfk_vendor AS sv
				INNER JOIN  sfk_assoc_product AS sa ON sa.VENDOR_ID = sv.VENDOR_ID
				";
		$where ="";
		if(isset($idProduit) && $idProduit != null){
			if ($where == "") $where = " WHERE ";
			else $where .= " AND ";
		 	$where .= " sa.id_avahis = '$idProduit' ";
		}
		$where .=  " GROUP BY vendor_nom ";	
		$sql .= $where ;
					
		$stmt = $this -> prepare($sql); 
		return $this->getAllPrepa($stmt);		 
		
	}
		public function getStockPriceEcomGP($idProd, $vendorId)
	{
		$sql = "SELECT * FROM sfk_product_ecommercant_stock_prix WHERE id_produit = $idProd AND VENDOR_ID = $vendorId";
		$stmt = $this->prepare($sql);
		
		return $this->getAllPrepa($stmt);
		
	}
	
	public function getGrilleChronopost()
	{
		$sql = "SELECT * FROM sfk_chronopost";
		$stmt = $this->prepare($sql);
		
		return $this->getAllPrepa($stmt);
		
	}
	
	public function getProdByName($name)
	{
		$sql = "SELECT id_produit FROM sfk_catalog_product WHERE name = :name";
		$stmt = $this->prepare($sql);
		$stmt -> bindValue(':name', $name);
		
		return $this->getOnePrep($stmt);
	}
	
	public function getProdBySku($sku)
	{
		$sql = "SELECT id_produit FROM sfk_catalog_product WHERE sku = :sku";
		$stmt = $this->prepare($sql);
		$stmt -> bindValue(':sku', $sku);
		
		return $this->getOnePrep($stmt);
	}
	
	public function getOneCateg()
	{
		$sql = "SELECT cat_id FROM sfk_categorie WHERE cat_active = 1 AND cat_level=2 ORDER BY cat_label LIMIT 1";
		$stmt = $this->prepare($sql);
		
		return $this->getOnePrep($stmt);
	}
	
	/**
	 * Va chercher si l'attribut existe déja dans la table s'il existe retourne l'ID_ATTRIBUT de cet
	 * attribut sinon la fonction Crée ce nouvel Attribut et renvoie le dernier ID_ATTRIBUT auto incrémenté
	 * correspondant
	 */
	public function checkIfAttributeExists($attribute)
	{
		$sql = "SELECT id_attribut FROM sfk_attribut WHERE code_attr = :code_attr";
		
		$stmt = $this->prepare($sql);
		$stmt->bindValue(":code_attr", $attribute);
		$stmt->execute();
		
		if ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			return $result['id_attribut'];
		}
		else{
			$stmt = $this->prepare("INSERT INTO sfk_attribut (label_attr, code_attr) 
												VALUES (:label_attr, :code_attr)");
			$stmt->bindValue(":label_attr", str_replace("_", " ", ucfirst(strtolower($attribute))));
			$stmt->bindValue(":code_attr", $attribute);
			$stmt->execute();
			
			return $this->lastInsertId();
		}
		
		
	}
	
	
	/**
	 * Va chercher si l'attribut existe déja dans la table s'il existe retourne l'ID_ATTRIBUT de cet
	 * attribut sinon la fonction Crée ce nouvel Attribut et renvoie le dernier ID_ATTRIBUT auto incrémenté
	 * correspondant
	 */
	public function checkIfAttributeExistsActive($attribute, $cat, $label)
	{
		$sql = "SELECT id_attribut FROM sfk_attribut WHERE code_attr = :code_attr AND cat_id = :cat_id";
		
		$stmt = $this->prepare($sql);
		$stmt->bindValue(":code_attr", $attribute);
		$stmt->bindValue(":cat_id", $cat);
		$stmt->execute();
		
		if ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
			
			$sql = "UPDATE sfk_attribut SET label_attr = :label_attr WHERE code_attr = :code_attr AND cat_id = '$cat'";
			$stmt = $this->prepare($sql);
			$stmt->bindValue(":label_attr", $label);
			$stmt->bindValue(":code_attr", $attribute);
			$stmt->execute();
			
			return $result['id_attribut'];
		}
		else{
			if($cat != ""){
				$stmt = $this->prepare("INSERT INTO sfk_attribut (label_attr, code_attr, cat_id)
										VALUES (:label_attr, :code_attr, :cat_id)
										ON DUPLICATE KEY UPDATE label_attr = VALUES(label_attr)
										");
			}else{
				$stmt = $this->prepare("INSERT INTO sfk_attribut (label_attr, code_attr) 
												VALUES (:label_attr, :code_attr)");	
			}
			
			$stmt->bindValue(":label_attr", $label);
			$stmt->bindValue(":code_attr", $attribute);
			if($cat != ""){
				$stmt->bindValue(":cat_id", $cat);
			}
			$stmt->execute();
			
			return $this->lastInsertId();
		}
	}
	
	
	public function prepUpdateLocalImage()
	{
		$sql = "UPDATE sfk_catalog_product SET local_image = :local_image WHERE id_produit = :id_produit";
		
		$this -> stmtLocalImg = $this -> prepare($sql);
	}
	
	public function prepUpdateLocalSmallImage()
	{
		$sql = "UPDATE sfk_catalog_product SET local_small_image = :local_small_image WHERE id_produit = :id_produit";
		
		$this -> stmtLocalSmallImg = $this -> prepare($sql);
	}
	
	public function prepUpdateLocalThumbnail()
	{
		$sql = "UPDATE sfk_catalog_product SET local_thumbnail = :local_thumbnail WHERE id_produit = :id_produit";
		
		$this -> stmtLocalThumbnail = $this -> prepare($sql);
	}
	
	public function updateLocalImage($id, $local_image)
	{

		$this -> stmtLocalImg -> bindValue(':local_image', $local_image);
		$this -> stmtLocalImg -> bindValue(':id_produit', $id);
		
		return $this -> stmtLocalImg -> execute();
		
	}
	
	public function updateLocalSmallImage($id, $local_small_image)
	{

		$this -> stmtLocalSmallImg -> bindValue(':local_small_image', $local_small_image);
		$this -> stmtLocalSmallImg -> bindValue(':id_produit', $id);
		
		return $this -> stmtLocalSmallImg -> execute();
		
	}
	
	public function updateLocalThumbnail($id, $local_thumbnail)
	{

		$this -> stmtLocalThumbnail -> bindValue(':local_thumbnail', $local_thumbnail);
		$this -> stmtLocalThumbnail -> bindValue(':id_produit', $id);
		
		return $this -> stmtLocalThumbnail -> execute();
		
	}
	/**
	 * Va recherché tous les produits avec poid a virgules
	 */
	public function getAllprodAvecvirgule($tablelab,$poidlab)
	{
		$sql = "SELECT *  FROM `".$tablelab."` WHERE `".$poidlab."` LIKE '%,%' ";
		//echo $sql;
		$stmt = $this->prepare($sql);
		$stmt->execute();
		return $this->getAllPrepa($stmt);		
	}
	/**
	 * update le poid  de la table catalogue product
	 * A refaire en fonction plus générale
	 */
	public function updatetWeightToCatProd($sku , $weight ,$tablelab,$poidlab,$eanlab)
	{
		$sql = "UPDATE `".$tablelab."` SET `".$poidlab."` = :WEIGHT WHERE `".$eanlab."` = :SKU ";
		//echo $sql;
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue (":WEIGHT", $weight);
		$stmt -> bindValue (":SKU", $sku);
		
		$stmt -> execute();
	}	
	
		public function prepGetProdGrossisteAssoc()
	{
		$sql = "SELECT id_produit_grossiste FROM sfk_assoc_product_grossiste WHERE id_avahis = :id_avahis LIMIT 1";
		
		$this -> stmtGetProdGrAssoc = $this -> prepare($sql); 
	}
	
	public function getProdGrossisteAssoc($id_produit)
	{
		
		
		$this -> stmtGetProdGrAssoc -> bindValue(':id_avahis', $id_produit);
		
		return $this -> getOnePrep($this -> stmtGetProdGrAssoc);
	}
	
	public function getProduitGrossiste($id)
	{
		$sql = "SELECT * FROM sfk_produit_grossiste WHERE ID_PRODUCT = :ID_PRODUCT";
		
		$stmt = $this->prepare($sql);
		
		$stmt -> bindValue(':ID_PRODUCT', $id);
		
		return $this->getAllPrepa($stmt);
	}
	
	
	public function prepUpdateImageSfkCp()
	{
		$sql = "UPDATE sfk_catalog_product SET image = :image, small_image = :small_image, thumbnail = :thumbnail WHERE id_produit = :id_produit ";
		
		$this -> stmtUpdateImageSfkCp = $this -> prepare($sql);
	}
	
	public function updateImageSfkCp($image, $small_image, $thumbnail, $id)
	{
		$this -> stmtUpdateImageSfkCp -> bindValue(':image', 		$image);
		$this -> stmtUpdateImageSfkCp -> bindValue(':small_image', 	$small_image);
		$this -> stmtUpdateImageSfkCp -> bindValue(':thumbnail', 	$thumbnail);
		$this -> stmtUpdateImageSfkCp -> bindValue(':id_produit', 	$id);
		
		return $this -> stmtUpdateImageSfkCp -> execute();
	}
	
	public function setGrossisteActiveValue($grossiste_id, $value) {
		
		$sql = "UPDATE sfk_grossiste SET ACTIVE = $value WHERE GROSSISTE_ID = $grossiste_id";
		
		return $this -> exec($sql);
	}
		
	public function setProductGrossisteActiveValue($grossiste_id, $value) {
		
		$sql = "UPDATE sfk_produit_grossiste SET ACTIVE = $value WHERE GROSSISTE_ID = $grossiste_id";
		
		return $this -> exec($sql);
	}
	
	public function setQuantityProductGrossisteZero($grossiste_id)
	{
		$sql = "UPDATE `sfk_product_ecommercant_stock_prix` as sp
				INNER JOIN sfk_produit_grossiste as pg ON pg.ID_PRODUCT = sp.ref_ecommercant 
				SET sp.QUANTITY = 0 
				WHERE pg.GROSSISTE_ID = $grossiste_id";
				
		return $this -> exec($sql);
	}
	/**
	 * fonction qui verifie si c'est un vendeur CSV et non pas vindirect
	 */
	public function verifVendorCSV($sell){
	    
		if(($vendor = $this -> getSofWare($sell['VENDOR_ID'])) || $sell['VENDOR_ID'] == 26){
		    
            return true;
            
            // si c'est un vendeur avec fichier CSV et qu'il ne s'agit pas de vindirect (26) a rajouté si d'autre vendeur update stock prix 
            /*if ($vendor['VERSION'] != 'AUTO' && $vendor['VERSION'] == NULL){
                // verifie si ce produit est actif
                if($this -> verifProdActifCSV($sell)){
                    return true ; //on return true
                }
                else{
                    return false ;  //sinon false
                }
            }*/		    
		}
		else{
		    return false;
		}

		
	}
	/**
	 * Vérifie si un produit csv vendeur est actif
	 * 
	 */
	public function verifProdActifCSV($sell)
	{
		$sql = "SELECT ACTIVE FROM sfk_produit_csv pc
				INNER JOIN sfk_assoc_product ap ON ap.id_produit_ecommercant = pc.ID_PRODUCT AND ap.VENDOR_ID = pc.VENDOR_ID
				INNER JOIN sfk_catalog_product cp ON cp.id_produit = ap.id_avahis
				WHERE 	cp.sku = :sku 
				AND 	pc.VENDOR_ID    = :ID_VENDOR
				";
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":sku", $sell['id_produit']); 
		$stmt -> bindValue("ID_VENDOR", $sell['VENDOR_ID']);
		
		$active = $this->getAllPrepa($stmt);
		
		
		return $active['0']['ACTIVE'] ;		
		
	}
	/**
	 * Retourne le type de software de l'ecomm
	 * 
	 */
	public function getSofWare($vendorID)
	{
		$sql = "SELECT * FROM sfk_tracking_vendor 
				WHERE VENDOR_ID = :VENDOR_ID ORDER BY DATE_INSERT LIMIT 1";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':VENDOR_ID', $vendorID, PDO::PARAM_INT);
		$stmt -> execute();
		
		$infoTracking = $stmt -> fetch() ;
		return $infoTracking;
		
	}
	
	public function prepMajPoidsSfkCp()
	{
		$sql = "UPDATE  sfk_produit_grossiste pg
				INNER JOIN sfk_assoc_product_grossiste apg
				ON apg.id_produit_grossiste = pg.ID_PRODUCT AND apg.GROSSISTE_ID = pg.GROSSISTE_ID 
				INNER JOIN `sfk_catalog_product` cp
				ON cp.id_produit = apg.id_avahis
				SET cp.weight = :weight
				WHERE pg.GROSSISTE_ID = :GROSSISTE_ID 
				AND pg.CATEGORY LIKE :CATEGORY
				AND (cp.weight = 0 OR cp.weight = 'NULL' OR cp.weight IS NULL)";
				
		$this -> stmtMajPoidsSfkCp = $this -> prepare($sql);
		
	}
	
	public function majPoidsSfkCp($cat, $weight, $grossiste_id)
	{
		$this -> stmtMajPoidsSfkCp -> bindValue(":CATEGORY", $cat);
		$this -> stmtMajPoidsSfkCp -> bindValue(":weight", $weight);
		$this -> stmtMajPoidsSfkCp -> bindValue(":GROSSISTE_ID", $grossiste_id);
		
		$this -> stmtMajPoidsSfkCp -> execute();
	}
	
	public function getListWeightByCat($grossiste_id)
	{
		$sql = "SELECT * FROM sfk_cat_poids WHERE GROSSISTE_ID = :GROSSISTE_ID";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', $grossiste_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getIdCatAssocGrossiste($cat, $grossiste_id)
	{
		$sql = "SELECT cat_id FROM sfk_assoc_categ_grossiste WHERE GROSSISTE_ID = :GROSSISTE_ID AND categ_grossiste = :categ_grossiste";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(':GROSSISTE_ID', 	$grossiste_id);
		$stmt -> bindValue(':categ_grossiste', 	$cat);
		
		return $this->getOnePrep($stmt); 
	}
	

	protected function getMetaKeyword($product)
	{
		if($product['meta_keyword'] != "" && $product['meta_keyword'] !== null && $product['meta_keyword'] != "NULL"){
			
			$metakeyword = $product['meta_keyword'] ;
		}
		else{
			$categories = $this -> getStrCateg($product['categories']);
			
			$manuf = $product['manufacturer'];
			$manuf = ucfirst(strtolower(str_replace("Ú", "e", $manuf)));
				
			$metakeyword = $product['name'].", ".$manuf. ", ";
			$categories = str_replace("/", ", ", $categories);
			
			$metakeyword .= $categories . ", " . $product['sku'];
				
		}
		
		return $metakeyword;
	}
	
	protected function getMetaDescrip($product)
	{
		if($product['meta_description'] != "" && $product['meta_description'] !== null && $product['meta_description'] != "NULL"){
			
			$metadescrip = $product['meta_description'] ;
		}
		else{
			
			$metadescrip = $product['name'];
		
			if($product['short_description'] != "" && $product['short_description'] != "NULL" && $product['short_description'] !== null){
				$metadescrip .= ", " . $product['short_description'];
			}
			
			if($product['meta_title'] != "" && $product['meta_title'] != "NULL" && $product['meta_title'] !== null){
				$metadescrip .= ", " . $product['meta_title'];
			}
				
		}
		
		return strip_tags($metadescrip);	
	}
	
	
	protected function getMetaTitle($product)
	{
		if($product['meta_title'] != "" && $product['meta_title'] !== null && $product['meta_title'] != "NULL"){
			
			$metatitle = $product['meta_title'] ;
		}
		else{
			
			$metatitle = $product['name'];
		}
		
		return $metatitle;		
	}
	
	public function setAutoAssoc($cat, $grossiste_id, $value)
	{
		$sql = "UPDATE sfk_cat_poids SET AUTO_ASSOC = $value WHERE GROSSISTE_ID = :GROSSISTE_ID AND CATEGORY = :CATEGORY";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":GROSSISTE_ID", $grossiste_id);
		$stmt -> bindValue(":CATEGORY", 	$cat);
		
		$stmt -> execute();
	}
	
	public function setPoidsForCat( $grossiste_id, $cat, $poids)
	{
		$sql = "UPDATE sfk_cat_poids SET POIDS_DEFAULT = :POIDS_DEFAULT WHERE GROSSISTE_ID = :GROSSISTE_ID AND CATEGORY = :CATEGORY";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":GROSSISTE_ID", 	$grossiste_id);
		$stmt -> bindValue(":CATEGORY", 		$cat);
		$stmt -> bindValue(":POIDS_DEFAULT", 	$poids);
		
		$stmt -> execute();
	}
	
	public function getAllProdLDLCLocalImage()
	{
		$sql = "SELECT cp.id_produit, cp.local_image, cp.categories
				FROM  `sfk_produit_grossiste` pg
				INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_produit_grossiste = pg.ID_PRODUCT
				INNER JOIN sfk_catalog_product cp ON apg.id_avahis = cp.id_produit
				WHERE pg.GROSSISTE_ID =2
				AND cp.local_image <>  ''";
			
		$stmt = $this -> prepare($sql);
		
		return $this -> getAllPrepa($stmt);
	}	
	
	public function saveFileEntry($file_name, $class, $description)
	{
		$sql= "INSERT INTO c_export_files (FILE_NAME, CLASS, DESCRIPTION) VALUES('$file_name', '$class', '$description' )";
		
		return $this -> exec($sql);
	}
	
	public function getAllFileEntry()
	{
		$sql = "SELECT DISTINCT CLASS FROM c_export_files";
		
		return $this -> getall($sql);
	}
	
	public function getAllProdsNonVendus()
	{
		$sql = "SELECT cp.id_produit, cp.sku, c.cat_label, cp.manufacturer, cp.name, cp.short_description, cp.price, sp.PRICE_PRODUCT, cp.weight  FROM `sfk_catalog_product` cp
				INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit
				INNER JOIN sfk_product_ecommercant_stock_prix sp ON sp.id_produit = cp.sku
				INNER JOIN sfk_categorie c ON cp.categories = c.cat_id
				WHERE cp.weight < 0.15";
		
		return $this -> getall($sql);
	}
	
	public function getAllCompleteVendorPo()
	{
		$sql = "SELECT 	so.increment_id as commande, spo.increment_id as bon_de_commande, so.date_created, soi.sku, scp.name, c.cat_label, 
						soi.udropship_vendor, v.VENDOR_NOM, soi.base_row_total_incl_tax, c.COMM , ROUND((soi.base_row_total_incl_tax * c.COMM),2) as montant_commission, ROUND((soi.base_row_total_incl_tax * 0.005 ),2) as frais_banquaires 
				FROM skf_orders so
				INNER JOIN skf_order_items soi ON so.order_id = soi.order_id 
				INNER JOIN skf_po spo ON spo.order_id = so.order_id AND spo.vendor_id = soi.udropship_vendor
				INNER JOIN sfk_vendor v ON v.VENDOR_ID = soi.udropship_vendor 
				LEFT JOIN sfk_catalog_product scp ON scp.sku = soi.sku
				LEFT JOIN sfk_categorie c ON c.cat_id = scp.categories
				WHERE soi.litiges <> 1 
				AND soi.udropship_vendor <> 24
				AND so.order_status = 'complete' 
				AND so.date_created >= '2013-12-01' 
				AND so.date_created <= '2013-12-15'  
				ORDER BY soi.udropship_vendor, so.date_created";
		
		return $this -> getall($sql);
	}
	
	public function getOrderByNumOrder($order_id)
	{
		$sql = "SELECT * FROM skf_orders WHERE order_id = :order_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":order_id", $order_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getOrderGrossisteByNumOrder($order_id_grossiste)
	{	
		$sql = "SELECT * FROM skf_orders_grossiste WHERE order_id_grossiste = :order_id_grossiste";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":order_id_grossiste", $order_id_grossiste);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getPoByPoId($po_id)
	{
		$sql = "SELECT * FROM skf_po WHERE po_id = :po_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":po_id", $po_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
       public function getPoByOrderIdVendorId($order_id, $vendor_id)
    {
        $sql = "SELECT po_id FROM skf_po WHERE order_id = :order_id AND vendor_id = :vendor_id";
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(":order_id", $order_id);
        $stmt -> bindValue(":vendor_id", $vendor_id);
        
        return $this->getOnePrep($stmt); 
    }
    
	public function getItemByItemId($item_id)
	{
		$sql = "SELECT * FROM skf_order_items WHERE item_id = :item_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":item_id", $item_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getCustomerById($customer_id)
	{
		$sql = "SELECT * FROM skf_customers WHERE customer_id = :customer_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":customer_id", $customer_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getAddressById($address_id)
	{
		$sql = "SELECT * FROM skf_address WHERE address_id = :address_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":address_id", $address_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getItemByIdProduct($id_product)
	{

		$sql = "SELECT  id_product, pg.grossiste_id, ref_grossiste, quantity, ean, price_product,
						name_product, description, description_short, url, image_product, 
						image_product_2, image_product_3, category, manufacturer, weight, pg.active, crawled, g.grossiste_nom   
				FROM sfk_produit_grossiste pg
				INNER JOIN sfk_grossiste g ON g.grossiste_id = pg.grossiste_id 
				WHERE id_product = :id_product";
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":id_product", $id_product);
		
		return $this->getAllPrepa($stmt); 
	}
	public function getVendorByVendorId($vendor_id)
	{
		$sql = "SELECT VENDOR_ID as vendor_id, VENDOR_NOM as vendor_nom, VENDOR_URL as vendor_url, ACTIVE as active, VENDOR_ATTN as vendor_attn, EMAIL as email, STREET as street, CITY as city,
						ZIP as zip, COUNTRY_ID as country_id, REGION as region, TELEPHONE as telephone, TELEPHONE2 as telephone2, FAX as fax, RAISONSOCIAL as raisonsocial, SIRET as siret,
						SITEINTERNET as siteinternet, CIVILITE as civilite, VENDOR_ATTN2 as vendor_attn2 
				FROM sfk_vendor WHERE VENDOR_ID = :vendor_id";
				
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":vendor_id", $vendor_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function getAnnonceReceptionById($annonce_reception_id)
	{
		$sql = "SELECT * FROM skf_annonce_reception WHERE annonce_reception_id = :annonce_reception_id";
		
		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":annonce_reception_id", $annonce_reception_id);
		
		return $this->getAllPrepa($stmt); 
	}
	
	public function updateOrder(BusinessOrder $order)
	{
		$sql = "UPDATE skf_orders SET litiges = :litiges WHERE order_id = :order_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':litiges', 	$order->getLitiges());
		$stmt->bindValue(':order_id', 	$order->getOrder_id(), \PDO::PARAM_INT);
		 
		$stmt->execute();
	}
	
	public function updatePo(BusinessPo $po)
	{
		$sql = "UPDATE skf_po SET litiges = :litiges, payment = :payment WHERE po_id = :po_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':litiges', 	$po->getLitiges());
		$stmt->bindValue(':payment', 	$po->getPayment());
		$stmt->bindValue(':po_id',  	$po->getPo_id(), \PDO::PARAM_INT);
		 
		$stmt->execute();
	}
	
	public function updateItem(BusinessItem $item)
	{
		$sql = "UPDATE skf_order_items SET litiges = :litiges, order_id_grossiste = :order_id_grossiste, annonce_reception_id = :annonce_reception_id, payment = :payment , facture_id = :facture_id WHERE item_id = :item_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':litiges', $item->getLitiges());
		$stmt->bindValue(':payment', $item->getPayment());
		$stmt->bindValue(':order_id_grossiste', $item->getOrder_id_grossiste());
		$stmt->bindValue(':annonce_reception_id', $item->getAnnonce_reception_id());
		$stmt->bindValue(':facture_id', $item->getFacture_id());
		$stmt->bindValue(':item_id', $item->getItem_id(), \PDO::PARAM_INT);
		 
		$stmt->execute();
	}
	
	public function updateOrderGrossiste(BusinessOrderGrossiste $orderGrossiste)
	{
		$sql = "UPDATE skf_orders_grossiste SET litiges = :litiges, estimated_price = :estimated_price, estimated_weight = :estimated_weight, order_status = :order_status WHERE order_id_grossiste = :order_id_grossiste";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':litiges', $orderGrossiste->getLitiges());
		$stmt->bindValue(':estimated_price', $orderGrossiste->getEstimated_price());
		$stmt->bindValue(':estimated_weight', $orderGrossiste->getEstimated_weight());
		$stmt->bindValue(':order_status', strtolower($orderGrossiste->getOrder_status()));
		$stmt->bindValue(':order_id_grossiste', $orderGrossiste->getOrder_id_grossiste(), \PDO::PARAM_INT);
		 
		$stmt->execute();
	}
	
	public function updateAnnonceReception(BusinessAnnonceReception $annonceReception)
	{
		$sql = "UPDATE skf_annonce_reception SET 	annonce_reception_code = :annonce_reception_code, 
													commentaires = :commentaires, 
													status = :status,
													date_reception = :date_reception  
				WHERE annonce_reception_id = :annonce_reception_id";
		
		$stmt = $this -> prepare($sql);
		 
		$stmt->bindValue(':annonce_reception_code', $annonceReception->getAnnonce_reception_code());
		$stmt->bindValue(':commentaires', 			$annonceReception->getCommentaires());
		$stmt->bindValue(':status', 				$annonceReception->getStatus());
		$stmt->bindValue(':date_reception',			$annonceReception->getDate_reception());
		$stmt->bindValue(':annonce_reception_id', 	$annonceReception->getAnnonce_reception_id(), \PDO::PARAM_INT);
		 
		$stmt->execute();
	}
	
	public function updateProductGrossiste(BusinessProductGrossiste $productGrossiste)
	{
		$sql = "UPDATE sfk_produit_grossiste
				SET grossiste_id = :grossiste_id,
					ref_grossiste = :ref_grossiste,
					quantity = :quantity,
					ean = :ean,
					price_product = :price_product,
					name_product = :name_product,
					description = :description,
					description_short = :description_short,
					url = :url,
					image_product = :image_product,
					image_product_2 = :image_product_2,
					image_product_3 = :image_product_3,
					category = :category,
					manufacturer = :manufacturer,
					weight = :weight,
					active = :active,
					crawled = :crawled  
				WHERE id_product = :id_product";
		
		$stmt = $this -> prepare($sql);
		
		$stmt->bindValue(':grossiste_id', 		$productGrossiste -> getGrossiste_id());
		$stmt->bindValue(':ref_grossiste', 		$productGrossiste -> getRef_grossiste());
		$stmt->bindValue(':quantity', 			$productGrossiste -> getQuantity());
		$stmt->bindValue(':ean', 				$productGrossiste -> getEan());
		$stmt->bindValue(':price_product', 		$productGrossiste -> getPrice_product());
		$stmt->bindValue(':name_product', 		$productGrossiste -> getName_product());
		$stmt->bindValue(':description', 		$productGrossiste -> getDescription());
		$stmt->bindValue(':description_short', 	$productGrossiste -> getDescription_short());
		$stmt->bindValue(':url', 				$productGrossiste -> getUrl());
		$stmt->bindValue(':image_product', 		$productGrossiste -> getImage_product());
		$stmt->bindValue(':image_product_2', 	$productGrossiste -> getImage_product_2());
		$stmt->bindValue(':image_product_3', 	$productGrossiste -> getImage_product_3());
		$stmt->bindValue(':category', 			$productGrossiste -> getCategory());
		$stmt->bindValue(':manufacturer', 		$productGrossiste -> getManufacturer());
		$stmt->bindValue(':weight', 			$productGrossiste -> getWeight());
		$stmt->bindValue(':active', 			$productGrossiste -> getActive());
		$stmt->bindValue(':crawled', 			$productGrossiste -> getCrawled());
		$stmt->bindValue(':id_product', 		$productGrossiste -> getId_product(), \PDO::PARAM_INT);
		 
		$stmt->execute();
	}
	
	public function deleteOrderGrossiste(BusinessOrderGrossiste $orderGrossiste)
	{
		$sql = "DELETE FROM skf_orders_grossiste  WHERE order_id_grossiste = :order_id_grossiste";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':order_id_grossiste', $orderGrossiste->getOrder_id_grossiste(), \PDO::PARAM_INT);
		$stmt->execute();
		
		//Cascade : reset champ order_id_grossiste des items liés à cette commande grossisste
		$sql = "UPDATE skf_order_items SET order_id_grossiste = NULL WHERE order_id_grossiste = :order_id_grossiste";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':order_id_grossiste', $orderGrossiste->getOrder_id_grossiste(), \PDO::PARAM_INT);
		$stmt->execute();
	}

	public function deleteAnnonceReception(BusinessAnnonceReception $annonceReception)
	{
		$sql = "DELETE FROM skf_annonce_reception  WHERE annonce_reception_id = :annonce_reception_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':annonce_reception_id', $annonceReception->getAnnonce_reception_id(), \PDO::PARAM_INT);
		$stmt->execute();
		
		//Cascade : reset champ annoncde_reception_id des items liés à cette annonce de réception
		$sql = "UPDATE skf_order_items SET annonce_reception_id = NULL WHERE annonce_reception_id = :annonce_reception_id";
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':annonce_reception_id', $annonceReception->getAnnonce_reception_id(), \PDO::PARAM_INT);
		$stmt->execute();
	}
		
	public function getAllPoByOrder($order_id)
	{
		$sql = 'SELECT * FROM skf_po WHERE order_id = :order_id';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':order_id', $order_id);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessPo');
     
    	$listePo = $stmt -> fetchAll();

    	return $listePo;
	}

	public function getAllPoByVendor($vendor_id)
	{
		$sql = 'SELECT po_id, vendor_id, po.increment_id, po.order_id, po.customer_id, po.total_weight, po.shipping_address_id, po.billing_address_id,
					   po.base_total_value, po.base_shipping_amount, po.base_tax_amount, po.date_created, po.date_updated, po.litiges, po.payment FROM skf_po po 
				INNER JOIN skf_orders o ON o.order_id = po.order_id 
				WHERE po.vendor_id = :vendor_id AND o.order_state = "complete" ORDER BY po.increment_id DESC'; 

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':vendor_id', $vendor_id);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessPo');
     
    	$listePo = $stmt -> fetchAll();
    	return $listePo;
	}
    
    public function getAllPoByVendorId($vendor_id)
    {
        $sql = 'SELECT po_id, vendor_id, po.increment_id, po.order_id, po.customer_id, po.total_weight, po.shipping_address_id, po.billing_address_id,
                       po.base_total_value, po.base_shipping_amount, po.base_tax_amount, po.date_created, po.date_updated, po.litiges, po.payment, po.udropship_method FROM skf_po po 
                INNER JOIN skf_orders o ON o.order_id = po.order_id 
                WHERE po.vendor_id = :vendor_id  ORDER BY po.increment_id DESC'; 

     
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':vendor_id', $vendor_id);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessPo');
     
        $listePo = $stmt -> fetchAll();
        return $listePo;        
    }

    public function getAllPoCompleteByVendorId($vendor_id)
    {
        $sql = 'SELECT po_id, vendor_id, po.increment_id, po.order_id, po.customer_id, po.total_weight, po.shipping_address_id, po.billing_address_id,
                       po.base_total_value, po.base_shipping_amount, po.base_tax_amount, po.date_created, po.date_updated, po.litiges, po.payment, po.udropship_method FROM skf_po po 
                INNER JOIN skf_orders o ON o.order_id = po.order_id 
                WHERE po.vendor_id = :vendor_id AND o.order_state = "complete"   
                ORDER BY po.increment_id DESC'; 

     
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':vendor_id', $vendor_id);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessPo');
     
        $listePo = $stmt -> fetchAll();
        return $listePo;        
    }
    
	public function getAllPoByVendorAfterDate($vendor_id, $date)
	{
		$sql = 'SELECT po_id, vendor_id, po.increment_id, po.order_id, po.customer_id, po.total_weight, po.shipping_address_id, po.billing_address_id,
					   po.base_total_value, po.base_shipping_amount, po.base_tax_amount, po.date_created, po.date_updated, po.litiges, po.payment, po.udropship_method 
				FROM skf_po po 
				INNER JOIN skf_orders o ON o.order_id = po.order_id 
				WHERE po.vendor_id = :vendor_id AND o.order_state = "complete" AND po.payment <> "complete"';
	   if($date) 
	       $sql .= ' AND po.date_created < ("'.$date.'" - INTERVAL 15 DAY) ';
       
       $sql .= "ORDER BY po.increment_id DESC";

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':vendor_id', $vendor_id);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessPo');
     
    	$listePo = $stmt -> fetchAll();

    	return $listePo;
	}
    
    public function getAllPoAvahisToOrder()
    {
        $sql = 'SELECT DISTINCT po_id, vendor_id, po.increment_id, po.order_id, po.customer_id, po.total_weight, po.shipping_address_id, po.billing_address_id,
                       po.base_total_value, po.base_shipping_amount, po.base_tax_amount, po.date_created, po.date_updated, po.litiges, po.payment 
                    FROM   
                    skf_po  po INNER JOIN skf_orders o ON o.order_id = po.order_id  
                    INNER JOIN skf_order_items soi ON soi.order_id = po.order_id AND po.vendor_id = soi.udropship_vendor    
                    
                    WHERE po.vendor_id = "24" AND soi.order_id_grossiste IS NULL 
                    AND (o.order_state = "complete" OR o.order_state = "processing" ) 
                    AND soi.litiges <> 1 
                    AND po.date_created > (NOW() - INTERVAL 15 DAY)';
        
        $stmt = $this -> prepare($sql);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessPo');
     
        $listePo = $stmt -> fetchAll();

        return $listePo;
    }
			
	public function getAllItemsByPo($order_id, $udropship_vendor)
	{
		$sql = 'SELECT * FROM skf_order_items WHERE order_id = :order_id AND udropship_vendor = :udropship_vendor';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':order_id', 		$order_id);
		$stmt -> bindValue(':udropship_vendor', $udropship_vendor);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessItem');
     
    	$listeItems = $stmt -> fetchAll();

    	return $listeItems;
	}

    public function getAllItemsByFacture($facture_id)
    {
        $sql = 'SELECT * FROM skf_order_items WHERE item_id IN 
                    (SELECT fcl.item_id FROM skf_factures f 
                        INNER JOIN skf_factures_com fc ON f.facture_id = fc.facture_id 
                        INNER JOIN skf_factures_com_lines fcl ON fcl.facture_com_id = fc.facture_com_id 
                        WHERE f.facture_id = :facture_id)';

     
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':facture_id',         $facture_id);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessItem');
     
        $listeItems = $stmt -> fetchAll();

        return $listeItems;
    }
    	
	public function getAllItemsByOrderGrossiste($order_id_grossiste)
	{
		$sql = 'SELECT * FROM skf_order_items WHERE order_id_grossiste = :order_id_grossiste ';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':order_id_grossiste', 	$order_id_grossiste);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessItem');
     
    	$listeItems = $stmt -> fetchAll();

    	return $listeItems;
	}
	
	public function getAllItemsByAnnonceReception($annonce_reception_id)
	{
		$sql = 'SELECT * FROM skf_order_items WHERE annonce_reception_id = :annonce_reception_id ';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':annonce_reception_id', $annonce_reception_id);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessItem');
     
    	$listeItems = $stmt -> fetchAll();

    	return $listeItems;		
	}
	
    public function getAllItemsToOrder()
    {
        $sql = "SELECT soi.* FROM `skf_order_items` soi 
                INNER JOIN skf_orders o ON o.order_id = soi.order_id
                WHERE soi.`udropship_vendor` = 24 
                AND (o.order_state = 'complete' OR o.order_state = 'processing') 
                AND soi.order_id_grossiste IS NULL 
                AND soi.litiges <> 1 
                AND soi.date_created  > (NOW() - INTERVAL 31 DAY)";

        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':annonce_reception_id', $annonce_reception_id);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessItem');
     
        $listeItems = $stmt -> fetchAll();

        return $listeItems; 
    }

	public function getAllOrdersByCustomer($order_customer_id)
	{
		$sql = 'SELECT * FROM skf_orders WHERE order_customer_id = :order_customer_id ORDER BY increment_id DESC';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':order_customer_id', $order_customer_id);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessOrder');
     
    	$listeOrders = $stmt -> fetchAll();

    	return $listeOrders;
	}

	   public function getAllOrdersPaidByCustomer($order_customer_id)
    {
        $sql = 'SELECT * FROM skf_orders 
                WHERE order_customer_id = :order_customer_id 
                AND order_state LIKE "complete" 
                ORDER BY increment_id DESC';

     
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':order_customer_id', $order_customer_id);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessOrder');
     
        $listeOrders = $stmt -> fetchAll();

        return $listeOrders;
    }
    
	public function getAllOrdersByCustomerMail($email, $name)
    {
        $sql = 'SELECT * FROM skf_orders WHERE recipient_email = :recipient_email OR recipient_name = :name ORDER BY increment_id DESC';

     
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':recipient_email', $email);
        $stmt -> bindValue(':name', $name);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessOrder');
     
        $listeOrders = $stmt -> fetchAll();

        return $listeOrders;
    }
    
    public function getAllOrdersGrossisteByStatus($order_status)
    {
        $sql = 'SELECT * FROM skf_orders_grossiste WHERE order_status = :order_status AND litiges = 0 ORDER BY date_created DESC';

     
        $stmt = $this -> prepare($sql);
        $stmt -> bindValue(':order_status', $order_status);
        $stmt -> execute();
        
        $stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessOrderGrossiste');
     
        $listeOrders = $stmt -> fetchAll();
        
        return $listeOrders;
    }
    	
	public function getAllCommentsForObject($commented_object_id, $commented_object_table)
	{
		$sql = 'SELECT * FROM skf_comments WHERE commented_object_id = :commented_object_id AND commented_object_table = :commented_object_table ORDER BY date_created DESC';

     
    	$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':commented_object_id', 	  $commented_object_id);
		$stmt -> bindValue(':commented_object_table', $commented_object_table);
		$stmt -> execute();
		
    	$stmt -> setFetchMode(PDO::FETCH_CLASS , 'BusinessComment');
     
    	$listeComments = $stmt -> fetchAll();

    	return $listeComments;
	}

	public function getCommentById($id_comment)
	{

		$sql = "SELECT * FROM skf_comments WHERE id_comment	 = :id_comment";

		$stmt = $this -> prepare($sql);
		
		$stmt -> bindValue(":id_comment", $id_comment);
		
		return $this->getAllPrepa($stmt);
	}

	public function createComment(BusinessComment $comment)
	{
		$sql = "INSERT INTO skf_comments  (content, user_id, parent_id_comment,
										   commented_object_id, commented_object_table, date_created) 
		        VALUES (:content, :user_id, :parent_id_comment, :commented_object_id, :commented_object_table, NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':content', 					$comment -> getContent());
		$stmt -> bindValue(':user_id', 					$comment -> getUser_id());
		$stmt -> bindValue(':parent_id_comment', 		$comment -> getParent_id_comment());
		$stmt -> bindValue(':commented_object_id', 		$comment -> getCommented_object_id());
		$stmt -> bindValue(':commented_object_table', 	$comment -> getCommented_object_table());
		$stmt -> execute();
		
		return $this -> lastInsertId();
	}
	
	public function createAnnonceReception(BusinessAnnonceReception $annonceReception)
	{
		$sql = "INSERT INTO skf_annonce_reception  (annonce_reception_code, commentaires, date_created) 
		        VALUES (:annonce_reception_code, :commentaires, NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':annonce_reception_code', $annonceReception -> getAnnonce_reception_code());
		$stmt -> bindValue(':commentaires', 		  $annonceReception -> getCommentaires());
		$stmt -> execute();
		
		return $this -> lastInsertId();
	}
	
	public function deleteComment(BusinessComment $comment)
	{
		$sql = "DELETE FROM skf_comments WHERE id_comment = :id_comment";
		
		$stmt = $this -> prepare($sql);
		$stmt->bindValue(':id_comment', $comment->getId_comment(), \PDO::PARAM_INT);
		$stmt->execute();
	}
	
	
	public function updateComment(BusinessComment $comment)
	{
		$sql = "UPDATE skf_comments  SET content = :content WHERE id_comment = :id_comment 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':id_comment', $comment -> getId_comment());
		$stmt -> bindValue(':content', $comment -> getContent());
		$stmt -> execute();
	}
		
	public function getOrderIdGrossisteByCode($order_external_code)
	{
		$sql = 'SELECT order_id_grossiste FROM skf_orders_grossiste WHERE order_external_code = :order_external_code';
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':order_external_code', 		$order_external_code);

    	return $this->getOnePrep($stmt);
	}
	
	public function getAnnonceReceptionIdByCode($annonce_reception_code)
	{
		$sql = 'SELECT annonce_reception_id FROM skf_annonce_reception WHERE annonce_reception_code = :annonce_reception_code';
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':annonce_reception_code', $annonce_reception_code);

    	return $this->getOnePrep($stmt);		
	}
	
	public function insertOrderGrossiste($order_grossiste)
	{
		$sql = "INSERT INTO skf_orders_grossiste  (order_status, order_external_code, grossiste_id,
													estimated_price, date_created, date_updated, litiges) 
		        VALUES (:order_status, :order_external_code, :grossiste_id, :estimated_price, NOW(), NOW(), :litiges) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':order_status', 		$order_grossiste['order_status']);
		$stmt -> bindValue(':order_external_code', 	$order_grossiste['order_external_code']);
		$stmt -> bindValue(':grossiste_id', 		$order_grossiste['grossiste_id']);
		$stmt -> bindValue(':estimated_price', 		$order_grossiste['estimated_price']);
		$stmt -> bindValue(':litiges', 				$order_grossiste['litiges']);
		$stmt -> execute();
		
		return $this -> lastInsertId();
	}

	public function insertAnnonceReception($annonce_reception)
	{
		$sql = "INSERT INTO skf_annonce_reception  (annonce_reception_code, commentaires, date_created, date_updated) 
		        VALUES (:annonce_reception_code, :commentaires, NOW(), NOW()) 
		        ";
		
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(':annonce_reception_code', 	$annonce_reception['annonce_reception_code']);
		$stmt -> bindValue(':commentaires', 			$annonce_reception['commentaires']);
		$stmt -> execute();
		
		return $this -> lastInsertId();
	}
	
	public function getUserCode($user_id)
	{
		$sql = "SELECT usercode FROM c_user WHERE user_id = '".$user_id."'";
			
		return $this -> getOne($sql);
	}
	
	public function getProductGrossisteAssocBySKU($sku)
	{
		$sql = "SELECT * FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$item['sku']."' ";
			
		$result = $this -> getall($sql);

		return $result[0];	
	}
	
	public function getIdProductGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.ID_PRODUCT FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
	
	public function getRefGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.REF_GROSSISTE FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
	
	public function getPrixGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.PRICE_PRODUCT FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
	
	public function getPoidsGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.WEIGHT FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
	
	public function getQtyGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.QUANTITY FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}

	public function getActiveGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.ACTIVE FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
	
	public function getUrlGrossisteFromItem($sku)
	{
		$sql = "SELECT pg.URL FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
			
	public function getNomGrossisteFromItem($sku)
	{
		$sql = "SELECT g.GROSSISTE_NOM FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}	
	
	public function getCategAvFromItem($sku)
	{
		$sql = "SELECT cat_label FROM sfk_catalog_product cp INNER JOIN sfk_categorie ON cat_id = cp.categories WHERE cp.sku = '".$sku."' ";
			
		return $this -> getOne($sql);
	}
	
	
	public function getProductWithPicture($picture_file, $id ="")
	{
		$sql = "SELECT id_produit 
				FROM  sfk_catalog_product 
				WHERE   id_produit = '$id' AND (local_image LIKE  '%".trim($picture_file)."%'
					 OR local_small_image LIKE  '%".trim($picture_file)."%'
					 OR local_thumbnail LIKE  '%".trim($picture_file)."%' )";
		return $this -> getOne($sql);
	}
	
	public function getLineFactuCom($line_id, $facture_com_id)
	{
		$sql = "SELECT count(*) as c FROM skf_factures_com_lines WHERE line_id = $line_id AND facture_com_id = $facture_com_id";
		
		return $this -> getOne($sql);
	}

    public function getLineFactuCredits($line_id, $facture_credits_id)
    {
        $sql = "SELECT count(*) as c FROM skf_factures_credits_lines WHERE line_id = $line_id AND facture_credits_id = $facture_credits_id";
        
        return $this -> getOne($sql);
    }
    
    public function getIdMailFromSITE($login)
    {
        $sql = "SELECT count(*) as c FROM skf_factures_credits_lines WHERE line_id = $line_id AND facture_credits_id = $facture_credits_id";
        
        return $this -> getOne($sql);
    }	
    	
	public function getLastFactureFromVendor($vendor_id)
	{
		$sql = "SELECT fc.facture_id
				FROM  `skf_factures` f
				INNER JOIN skf_factures_com fc ON fc.facture_id = f.facture_id
				WHERE fc.vendor_id = :vendor_id
				AND fc.date_limit = ( SELECT ( MIN( date_value ) - INTERVAL 15 DAY ) 
									  FROM skf_calendar
								      WHERE date_type_id =1
									  AND date_value > (NOW() - INTERVAL 5 DAY)
									)
				AND f.date_created = (SELECT MAX( date_created ) FROM skf_factures sf 
									  INNER JOIN skf_factures_com sfc ON sfc.facture_id = sf.facture_id WHERE sfc.vendor_id = :vendor_id)";
				
		$stmt = $this -> prepare($sql);
		$stmt -> bindValue(":vendor_id", $vendor_id);
		
		return $this -> getOnePrep($stmt);
	}
    
    public function getCountProductGrossisteToQualify()
    {
        $sql = "SELECT pg.GROSSISTE_ID, GROSSISTE_NOM, COUNT( * ) as c 
                FROM  `sfk_produit_grossiste` pg
                INNER JOIN sfk_grossiste g ON g.GROSSISTE_ID = pg.GROSSISTE_iD
                WHERE pg.ACTIVE =1
                AND pg.AJOUTE =0
                GROUP BY GROSSISTE_ID
                LIMIT 0 , 30";
        
        return $this -> getAll($sql);
    }
    
    public function getTotalPo($sql_month)
    {
        $sql = "SELECT count(*) FROM `skf_po` po 
                INNER JOIN skf_orders o 
                ON o.order_id = po.order_id
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')  
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);
    }
    
    public function getTotalPoAvahis($sql_month)
    {
        $sql = "SELECT count(*) FROM `skf_po` po 
                INNER JOIN skf_orders o 
                ON o.order_id = po.order_id
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                AND vendor_id = 24 
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);
    }
    
    public function getTotalPoVendeur($sql_month)
    {
        $sql = "SELECT count(*) FROM `skf_po` po 
                INNER JOIN skf_orders o 
                ON o.order_id = po.order_id
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                AND vendor_id <> 24  
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);
    }
    
    public function getCaMonth($sql_month)
    {
        $sql = "SELECT SUM(  po.`base_total_value` +  po.`base_tax_amount` +  po.`base_shipping_amount` ) 
                FROM `skf_po` po 
                INNER JOIN skf_orders o 
                ON o.order_id = po.order_id
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);
    }

    public function getCaVendeurMonth($sql_month)
    {
        $sql = "SELECT SUM(  po.`base_total_value` +  po.`base_tax_amount` +  po.`base_shipping_amount` ) 
                FROM `skf_po` po 
                INNER JOIN skf_orders o 
                ON o.order_id = po.order_id
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                AND vendor_id <> 24  
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);
    }
        
    public function getCountOrdersMonth($sql_month)
    {
        $sql = "SELECT count(DISTINCT o.order_id) 
                FROM skf_orders o 
                LEFT JOIN skf_po po ON po.order_id = o.order_id 
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND order_status <>  'canceled') 
                AND po.order_id IS NOT NULL 
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);         
    }
    
    public function getTotalPanierMonth($sql_month)
    {
        $sql = "SELECT SUM(qty) FROM (
                                        SELECT SUM(oi.qty_ordered) as qty
                                        FROM `skf_order_items` oi 
                                        INNER JOIN skf_orders o 
                                        ON o.order_id = oi.order_id
                                        WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                                        AND (o.date_created ".$sql_month. " ) 
                                        GROUP BY oi.order_id) 
                                        as panier";
        
        return $this -> getOne($sql);        
    }

    public function getTotalPanierVendeurMonth($sql_month)
    {
        $sql = "SELECT SUM(po.`base_total_value` +  po.`base_tax_amount` +  po.`base_shipping_amount`) as tot
                FROM skf_po po 
                INNER JOIN  skf_orders o
                ON po.order_id = o.order_id 
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                AND po.vendor_id <> 24
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);        
    }

    public function getTotalPaylineNXEncours($sql_month)
    {
        $sql = "SELECT SUM(pay.`shipping_amount` +  pay.`amount_ordered`) as tot
                FROM skf_payments pay 
                INNER JOIN  skf_orders o
                ON pay.parent_id = o.order_id 
                WHERE method LIKE 'PaylineNX' 
                AND (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled') 
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);        
    }
        
    public function getTotalPanierAvahisMonth($sql_month)
    {
        $sql = "SELECT SUM(po.`base_total_value` +  po.`base_tax_amount` +  po.`base_shipping_amount`) as tot
                FROM skf_po po 
                INNER JOIN  skf_orders o
                ON po.order_id = o.order_id 
                WHERE (o.order_state <> 'canceled' AND o.order_state <> 'holded' AND o.order_state <> 'closed' AND o.order_state <> 'pending_payment' AND order_status <>  'canceled')
                AND po.vendor_id = 24
                AND (o.date_created ".$sql_month. " )";
        
        return $this -> getOne($sql);        
    }
    
    public function getNbNewVendeur($sql_month)
    {
        $sql = "SELECT count(*) as nbvd
                FROM sfk_vendor 
                WHERE ( created_at ".$sql_month. " ) 
                AND VENDOR_ID > 0";
        
        return $this -> getOne($sql);           
    }
    
    public function getPanierAbandonneMonth($sql_month)
    {
       $sql = " SELECT count(*)
                FROM skf_orders o
                LEFT JOIN skf_po po ON o.order_id = po.order_id
                WHERE po.po_id IS NULL 
                AND (o.date_created ".$sql_month. " )"; 
                
       return $this -> getOne($sql);     
    }
    
    public function getTotalCommMonth($sql_month)
    {
       $sql = " SELECT SUM(total_comm)
                FROM skf_factures_com fc 
                INNER JOIN skf_factures f 
                ON f.facture_id = fc.facture_id 
                WHERE (f.date_created ".$sql_month. " )"; 
                
       return $this -> getOne($sql);        
    }
    
    public function checkSkuUnique($sku)
    {
        $sql = "SELECT count(*) FROM sfk_catalog_product cp
                WHERE cp.sku = :sku";
                
        $stmt = $this->prepare($sql);
        
        $stmt -> bindValue(':sku' , $sku, PDO::PARAM_STR);
        
        
        return $this -> getOnePrep($stmt);        
    }
    
    public function razActiveProdCSV($vendor_id)
    {
        $sql = "UPDATE sfk_produit_csv SET ACTIVE = 0 WHERE VENDOR_ID = :VENDOR_ID"; 
        $stmt = $this->prepare($sql);
        $stmt->bindValue(':VENDOR_ID', $vendor_id);
        $stmt->execute();
    }
    
    public function razStockPrixCSV($vendor_id)
    {
        $sql = "UPDATE sfk_product_ecommercant_stock_prix SET QUANTITY = 0 WHERE VENDOR_ID = :VENDOR_ID"; 
        $stmt = $this->prepare($sql);
        $stmt->bindValue(':VENDOR_ID', $vendor_id);
        $stmt->execute();        
    }
    
    public function getAllAttrForCat($cat_id, $sort_code_attr)
    {
        $sql = "SELECT DISTINCT a.id_attribut, a.cat_id, a.code_attr, a.label_attr, a.is_filterable     
                    FROM   
                    sfk_attribut a 
                    INNER JOIN  sfk_product_attribut pa on pa.id_attribut = a.id_attribut 
                    INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit 
                    WHERE cp.categories = ".$cat_id." AND pa.active = 1 
                    ";
        if($sort_code_attr){
            $sql .= " ORDER BY a.code_attr $sort_code_attr ";
        }            
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;
    }

    public function getAllAttrInCat($cat_list, $sort_code_attr="")
    {
        $sql = "SELECT DISTINCT a.id_attribut, a.cat_id, a.code_attr, a.label_attr, a.is_filterable     
                    FROM   
                    sfk_attribut a 
                    INNER JOIN  sfk_product_attribut pa on pa.id_attribut = a.id_attribut 
                    INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit 
                    WHERE pa.active = 1  AND cp.categories IN ( ".$cat_list." ) 
                    ";
        if($sort_code_attr){
            $sql .= " ORDER BY a.code_attr $sort_code_attr ";
        }            
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;
    }
    
    public function getAllProductAttrForCat($cat_id, $id_attribut)
    {
        $sql = "SELECT DISTINCT pa.id_produit, pa.id_attribut, pa.value, pa.active 
                    FROM   
                    sfk_product_attribut pa 
                    INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit  
                    WHERE cp.categories = ".$cat_id." 
                    AND pa.id_attribut = ".$id_attribut." AND active = 1"
                    ;
                    
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessProductAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;
    }
    
    public function getAllProductAttrInCatList($cat_list, $id_attribut)
    {
        $sql = "SELECT DISTINCT pa.id_produit, pa.id_attribut, pa.value, pa.active 
                    FROM   
                    sfk_product_attribut pa 
                    INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit  
                    WHERE cp.categories IN ( ".$cat_list." )  
                    AND pa.id_attribut = ".$id_attribut." AND active = 1"
                    ;
                    
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessProductAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;
    }
        
    public function getAllProductAttrForCatAndValue($cat_id, $id_attribut, $value)
    {
        $sql = "SELECT DISTINCT pa.id_produit, pa.id_attribut, pa.value, pa.active 
                    FROM   
                    sfk_product_attribut pa 
                    INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit  
                    WHERE cp.categories = ".$cat_id." 
                    AND pa.id_attribut = ".$id_attribut." 
                    AND pa.value = '".$value."' 
                    AND active = 1"
                    ;
                    
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessProductAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;        
    }
    
    public function getAllProductAttrInCatListAndValue($cat_list, $id_attribut, $value)
    {
        $sql = "SELECT DISTINCT pa.id_produit, pa.id_attribut, pa.value, pa.active 
                    FROM   
                    sfk_product_attribut pa 
                    INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit  
                    WHERE cp.categories IN ( ".$cat_list." ) 
                    AND pa.id_attribut = ".$id_attribut." 
                    AND pa.value = '".$value."' 
                    AND active = 1"
                    ;
                    
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessProductAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;        
    }
            
    public function getCountProdsWithAttr ($id_attribut) {
            
        $sql = "SELECT COUNT(DISTINCT id_produit) as c 
                    FROM sfk_product_attribut 
                    WHERE id_attribut = :id_attribut 
                    AND active = 1";
        $stmt = $this -> prepare($sql);
        
        $stmt->bindValue(':id_attribut', $id_attribut);         
        
        return $this -> getOnePrep($stmt);
        
    }
    
    public function getCountProdsWithAttrInCat($id_attribut, $cat_id)
    {
            
        $sql = "SELECT COUNT(DISTINCT pa.id_produit) as c 
                    FROM sfk_product_attribut pa
                    INNER JOIN sfk_catalog_product cp ON pa.id_produit = cp.id_produit 
                    WHERE pa.id_attribut = :id_attribut 
                    AND cp.categories = :categories
                    AND pa.active = 1";
        $stmt = $this -> prepare($sql);
        
        $stmt->bindValue(':id_attribut', $id_attribut);
        $stmt->bindValue(':categories', $cat_id);         
        
        return $this -> getOnePrep($stmt);        
    }

    public function getCountProdsWithAttrInListCat($id_attribut, $cat_list)
    {
            
        $sql = "SELECT COUNT(DISTINCT pa.id_produit) as c 
                    FROM sfk_product_attribut pa
                    INNER JOIN sfk_catalog_product cp ON pa.id_produit = cp.id_produit 
                    WHERE pa.id_attribut = :id_attribut 
                    AND cp.categories IN ($cat_list)
                    AND pa.active = 1";
        $stmt = $this -> prepare($sql);
        
        $stmt->bindValue(':id_attribut', $id_attribut);         
        
        return $this -> getOnePrep($stmt);        
    }
         
    public function getDistinctValuesAttributForCat($id_attribut, $cat_id)
    {
        $sql =" SELECT TRIM(pa.value) as value, count(pa.value) as c 
                FROM sfk_product_attribut as pa
                INNER JOIN sfk_attribut as a ON a.id_attribut= pa.id_attribut
                INNER JOIN sfk_catalog_product as cp ON cp.id_produit = pa.id_produit   
                WHERE a.id_attribut = :id_attribut
                AND cp.categories = :categories  
                AND pa.active = 1 
                GROUP BY TRIM(pa.value)
                ORDER BY pa.value ASC";
                
        $stmt = $this -> prepare($sql); 

        $stmt -> bindValue(":id_attribut", $id_attribut);
        $stmt->bindValue(':categories', $cat_id);    
        
        return $this->getAllPrepa($stmt);
    }

    public function getDistinctValuesAttributInCatList($id_attribut, $cat_list)
    {
        $sql =" SELECT TRIM(pa.value) as value, count(pa.value) as c 
                FROM sfk_product_attribut as pa
                INNER JOIN sfk_attribut as a ON a.id_attribut= pa.id_attribut
                INNER JOIN sfk_catalog_product as cp ON cp.id_produit = pa.id_produit   
                WHERE a.id_attribut = :id_attribut
                AND cp.categories IN ( $cat_list )  
                AND pa.active = 1 
                GROUP BY TRIM(pa.value)
                ORDER BY pa.value ASC";
                
        $stmt = $this -> prepare($sql); 

        $stmt -> bindValue(":id_attribut", $id_attribut);
        
        return $this->getAllPrepa($stmt);
    }
        
    public function supprAssocGr($id_product, $id_produit)
    {
        $sql = "DELETE FROM sfk_assoc_product_grossiste 
                WHERE id_produit_grossiste = :id_produit_grossiste 
                AND id_avahis = :id_avahis ";
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(":id_produit_grossiste", $id_product);
        $stmt -> bindValue(":id_avahis", $id_produit);
        
        return $this -> getOnePrep($stmt);
    }
    
    public function prepInsertLivraisonTypes() {

        $sql = "INSERT INTO skf_livraison_types (
                    `shipping_id`,`shipping_code`,`shipping_title`,`days_in_transit`)
                    
                VALUES ( 
                    :shipping_id, :shipping_code, :shipping_title, :days_in_transit )
                
                ON DUPLICATE KEY UPDATE 
                    shipping_id     =VALUES(shipping_id),
                    shipping_code   =VALUES(shipping_code),
                    shipping_title  =VALUES(shipping_title),
                    days_in_transit =VALUES(days_in_transit)
                    
                ";
        $this -> stmtLivraisonType = $this -> prepare($sql);
    }    
    
    public function insertLivraisonTypes($livraison_type)
    {
        $this -> stmtLivraisonType -> bindValue(":shipping_id",     $livraison_type["shipping_id"]);
        $this -> stmtLivraisonType -> bindValue(":shipping_code",   $livraison_type["shipping_code"]);
        $this -> stmtLivraisonType -> bindValue(":shipping_title",  $livraison_type["shipping_title"]);
        $this -> stmtLivraisonType -> bindValue(":days_in_transit", $livraison_type["days_in_transit"]);
        
        $this -> stmtLivraisonType -> execute();
    }
    
    
    public function getUniversForCat($cat_id){
            
        $sql = "SELECT cat_id, parent_id FROM sfk_categorie 
                WHERE cat_id = :cat_id ";
        
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(":cat_id", $cat_id);
        
        $rs = $this -> getAllPrepa($stmt);
        
        $rs = $rs[0];
        
        if($rs["parent_id"] == 2){
            return $rs["cat_id"];
        }
        else{
            return $this -> getUniversForCat($rs["parent_id"]);
        }
    }
    
    public function getProductVendorList($sku)
    {
        $sql = "SELECT sp.VENDOR_ID, v.VENDOR_NOM 
                FROM sfk_product_ecommercant_stock_prix sp
                INNER JOIN sfk_vendor v ON v.VENDOR_ID = sp.VENDOR_ID  
                WHERE id_produit = :sku 
                AND QUANTITY > 0";;
                
        $stmt = $this -> prepare($sql);
        
        $stmt -> bindValue(":sku", $sku);
        
        return $this -> getAllPrepa($stmt);
    }       
    
    public function getAllActiveAttributes()
    {
        $sql = "SELECT DISTINCT a.id_attribut, a.code_attr, a.label_attr, a.is_filterable  
                FROM sfk_attribut a 
                INNER JOIN sfk_product_attribut pa ON pa.id_attribut = a.id_attribut  
                INNER JOIN sfk_catalog_product cp ON cp.id_produit = pa.id_produit
                INNER JOIN sfk_product_ecommercant_stock_prix sp ON sp.id_produit = cp.sku
                WHERE pa.active = 1 
                AND sp.QUANTITY > 0 ";
                
                
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $stmt -> setFetchMode(\PDO::FETCH_CLASS, "BusinessAttribut");
        
        $list = $stmt -> fetchAll();

        return $list;  
    }
    
    public function getAllManufacturer()
    {
        $sql = "SELECT manufacturer, count(manufacturer) as count  FROM `sfk_catalog_product` cp 
                INNER JOIN sfk_product_ecommercant_stock_prix sp ON sp.id_produit = cp.sku 
                WHERE manufacturer <> '' 
                AND manufacturer IS NOT NULL
                GROUP BY manufacturer
                ORDER BY count DESC";
                
                
        $stmt = $this -> prepare($sql);
        
        $stmt -> execute();
        
        $list = $stmt -> fetchAll();

        return $list;          
    }
}


