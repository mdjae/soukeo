<?php

class BusinessAddress extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $address_id;
	protected $street1;
	protected $street2; 
	protected $postcode;
	protected $city;
	protected $region_code;
	protected $country;
	protected $date_created;
	protected $date_updated;


	

	public function __construct ($address_id = false) {
		
		self::$db = new BusinessSoukeoModel();	
		if($address_id){
			$this->address_id = $address_id;
			$data = self::$db -> getAddressById($this->address_id );
			$data = $data[0];
			if (!$data){
				return false;
			}else{
				$this -> hydrate($data);
			} 	
		}	
	}
	
	
	public function setAddress_id($address_id ){
		$this -> address_id  = $address_id ;
	}

	public function setStreet1($street1)
	{
		$this -> street1 = $street1;
	}
	
	public function setStreet2($street2)
	{
		$this -> street2 = $street2;
	}
	
	public function setPostcode($postcode)
	{
		$this -> postcode = $postcode;
	}
	
	public function setCity($city)
	{
		$this -> city = $city ;
	}
	
	public function setRegion_code($region_code)
	{
		$this -> region_code = $region_code;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
	
	public function setCountry($country)
	{
		$this -> country = $country;	
	}
	
	public function getAddress_id()
	{
		return $this -> address_id;
	}
	
	public function getStreet1()
	{
		return $this -> street1;
	}
	
	public function getStreet2()
	{
		return $this -> street2;
	}
	
	public function getPostcode()
	{
		return $this -> postcode;
	}
	
	public function getCity()
	{
		return $this -> city;
	}
	
	public function getRegion_code()
	{
		return $this -> region_code;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function getCountry()
	{
		return $this -> country;
	}
	
	public function save()
	{
		self::$db -> updateAddress($this);
	}
	
}