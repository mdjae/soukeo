<?php

class BusinessCatalogProduct extends BusinessEntity {
	
	protected static 	$db ;
	
    protected $id_produit;
	protected $sku;
	protected $categories;
	protected $name;
	protected $description;  
	protected $description_html;
	protected $short_description;
	protected $price;
	protected $weight;
    protected $country_of_manufacture;
    protected $ean;
	protected $meta_description;
	protected $meta_keyword;
	protected $meta_title;
	protected $image;
	protected $small_image;
	protected $local_image;
	protected $local_small_image;
	protected $local_thumbnail;
	protected $thumbnail;
	protected $manufacturer;
	protected $dropship_vendor;
    protected $attribute_set;
    protected $date_create;
    protected $date_update;
    protected $delete;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	

		if (!empty($data)) {
			$this->hydrate($data);
		}	
	}
	
    public function setId_produit($id_produit)
    {
        $this -> id_produit = $id_produit;
        
        return $this;
    }
	
	public function setSku($sku)
	{
		$this -> sku = $sku;
        
        return $this;
	}

	public function setCategories($categories)
	{
		$this -> categories = $categories;
        
        return $this;
	}
	
	public function setName($name)
	{
		$this -> name = $name;
        
        return $this;
	}
	
	public function setDescription($description)
	{
		$this -> description = $description;
        
        return $this;
	}
	
	public function setDescription_html($description_html)
	{
		$this -> description_html = $description_html;
        
        return $this;
	}

	public function setShort_description($short_description)
	{
		$this -> short_description = $short_description;
        
        return $this;
	}
	
	public function setPrice($price)
	{
		$this -> price = $price;
        
        return $this;
	}
			
	public function setWeight($weight)
	{
		$this -> weight = $weight;
        
        return $this;
	}

    public function setCountry_of_manufacture($country_of_manufacture)
    {
        $this -> country_of_manufacture = $country_of_manufacture ;
        
        return $this;
    }
    	
	public function setEan($ean)
	{
		$this -> ean = $ean ;
        
        return $this;
	}
	
	public function setMeta_description($meta_description)
	{
		$this -> meta_description = $meta_description ;
        
        return $this;
	}
    
    public function setMeta_keyword($meta_keyword)
    {
        $this -> meta_keyword = $meta_keyword ;
        
        return $this;
    }
	
	public function setMeta_title($meta_title)
	{
		$this -> meta_title = $meta_title;
        
        return $this;
	}
		
	public function setImage($image)
	{
		$this -> image = $image;
        
        return $this;
	}
	
	public function setSmall_image($small_image)
	{
		$this -> small_image = $small_image;
        
        return $this;
	}
	
	public function setLocal_image($local_image)
	{
		$this -> local_image = $local_image;
        
        return $this;
	}
	
	public function setLocal_small_image($local_small_image)
	{
		$this -> local_small_image = $local_small_image;	
        
        return $this;
	}
	
	public function setThumbnail($thumbnail)
	{
		$this -> thumbnail = $thumbnail;
        
        return $this;
	}

    public function setLocal_thumbnail($local_thumbnail)
    {
        $this -> local_thumbnail = $local_thumbnail;
        
        return $this;
    }
    	
	public function setManufacturer($manufacturer)
	{
		$this -> manufacturer = $manufacturer;
        
        return $this;
	}
	
	public function setDropship_vendor($dropship_vendor)
	{
		$this -> dropship_vendor = $dropship_vendor;
        
        return $this;
	}
	
	public function setAttribute_set($attribute_set)
	{
		$this -> attribute_set = $attribute_set;
        
        return $this;
	}

    public function setDate_create($date_create)
    {
        $this -> date_create = $date_create;
        
        return $this;
    }
    
    public function setDate_update($date_update)
    {
        $this -> date_update = $date_update;
        
        return $this;
    }
    
    public function setDelete($delete)
    {
        $this -> delete = $delete;
        
        return $this;
    }
    
    //GETTERS ///// // / / / // / / / /
    
    public function getId_produit()
    {
        return $this -> id_produit ;
    }
    
    public function getSku()
    {
        return  $this -> sku;
    }

    public function getCategories()
    {
        return $this -> categories ;
    }
    
    public function getName()
    {
        return $this -> name ;
    }
    
    public function getDescription()
    {
        return $this -> description ;
    }
    
    public function getDescription_html()
    {
        return $this -> description_html ;
    }

    public function getShort_description()
    {
        return $this -> short_description ;
    }
    
    public function getPrice()
    {
        return $this -> price ;
    }
            
    public function getWeight()
    {
        return $this -> weight ;
    }

    public function getCountry_of_manufacture()
    {
        return $this -> country_of_manufacture ;
    }
        
    public function getEan()
    {
        return $this -> ean ;
    }
    
    public function getMeta_description()
    {
        return $this -> meta_description ;
    }
    
    public function getMeta_keyword()
    {
        return $this -> meta_keyword  ;
    }
    
    public function getMeta_title()
    {
        return $this -> meta_title ;
    }
        
    public function getImage()
    {
        return $this -> image ;
    }
    
    public function getSmall_image()
    {
        return $this -> small_image ;
    }
    
    public function getLocal_image()
    {
        return $this -> local_image ;
    }
    
    public function getLocal_small_image()
    {
        return $this -> local_small_image ;    
        
    }
    
    public function getThumbnail()
    {
        return $this -> thumbnail ;
    }

    public function getLocal_thumbnail()
    {
        return $this -> local_thumbnail ;
    }
        
    public function getManufacturer()
    {
        return $this -> manufacturer ;
    }
    
    public function getDropship_vendor()
    {
        return $this -> dropship_vendor ;
    }
    
    public function getAttribute_set()
    {
        return $this -> attribute_set;
    }

    public function getDate_create()
    {
        return $this -> date_create ;
    }
    
    public function getDate_update()
    {
        return $this -> date_update ;
    }
    
    public function getDelete()
    {
        return $this -> delete ;
    }
    
    public function getCategory()
    {
        $categorieManager = new ManagerCategorie();
        
        return $categorieManager -> getBy(array("cat_id" => $this -> categories));
    }
    
    public function getCategory_label()
    {
        $categorieManager = new ManagerCategorie();
        
        return ucfirst($categorieManager -> getCategorieByCategorieId( $this -> categories));
    }
    
    public function getDropship_vendor_obj()
    {
        $manager = new ManagerVendor();
        return $manager -> getBy(array("vendor_id" => $this -> dropship_vendor));
    }    
    
    public function getStock_prix($value='')
    {
        return self::$db->getStockPriceProduct($this->sku);
    }
    
    public function getProduct_grossiste()
    {
        if($list = self::$db -> getAssocProductGrossiste($this->id_produit)){
            
            $rs = array();
            
            foreach ($list as $assoc) {
                $rs[] = new BusinessProductGrossiste($assoc['id_produit_grossiste']);    
            }
            
            return $rs;
        }
        else{
            return false;
        }
    }
    
    public function getAllProductAttributes()
    {
        $productAttributManager = new ManagerProductAttribut();
        
        $product_attr_list = $productAttributManager -> getAll(array("id_produit" => $this -> id_produit, "active" => 1));
        
        
        return $product_attr_list;
    }
        	
    public function getValueForAttribut($id_attribut)
    {
        $productAttributManager = new ManagerProductAttribut();
        
        if($product_attribut = $productAttributManager -> getBy(array("id_produit" => $this -> id_produit, "id_attribut" => $id_attribut))){
            
            return $product_attribut -> getValue();
        }
    }
    
    public function getVendorList()
    {
        return self::$db -> getProductVendorList($this -> sku);
    }
            
	public function save()
	{
		
	}
	
    public function isNew()
    {
        $catalogProductManager = new ManagerCatalogProduct();
        if ( $this -> id_produit || ($sku = $catalogProductManager -> getBy(array("sku" => trim($this -> sku)))) ){
            
            return false;
        }else{
            return true;
        }
    }
   
}