<?php 
/**
 * 
 */
class BusinessCreditsPack extends BusinessEntity {
	
    
	protected $pack_id;
    protected $label;
    protected $value;
    protected $price;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setPack_id($pack_id)
    {
        $this -> pack_id = $pack_id;
    }
    
    public function getPack_id()
    {
        return $this -> pack_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function getValue()
    {
        return $this -> value;
    }
    
    public function setValue($value)
    {
        $this -> value = $value;
    }
    
    public function setPrice($price)
    {    
        $this -> price = $price;
    }
    
    public function getPrice()
    {
        return $this -> price;
    }
    
    public function isNew()
    {
        $manager = new ManagerCreditsPack();
        
        if ( $this -> pack_id || ($state = $manager -> getBy(array("label" => $this->label) ) )){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>