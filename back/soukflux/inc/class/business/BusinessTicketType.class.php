<?php 
/**
 * 
 */
class BusinessTicketType extends BusinessEntity {
	
    
	protected $type_id;
    protected $label;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setType_id($type_id)
    {
        $this -> type_id = $type_id;
    }
    
    public function getType_id()
    {
        return $this -> type_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function isNew()
    {
        $manager = new ManagerTicketType();
        
        if ($this -> type_id || ($type = $manager -> getBy(array("label" => $this->label) )) ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>