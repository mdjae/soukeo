<?php 
/**
 * Classe permettant la gestion des différents référentiels en offrant différentes
 * forme d'affichage rapide HTML comme des tableaux récapitulatif ou des select<option>
 * permettant d'afficher des listes déroulantes rapidement
 */
class BusinessReferentielUtils{
	
    /**
     * Cette fonction permet de remplir en HTML le contenu d'un onglet dans la section réferentiels de 
     * Soukflux. Cet onglet aura pour identifiant le nom de l'entité qu'il représente + "_tab" exemple : "#VendorType_tab" donc le 
     * Lien permettant d'accéder à cet onglet devra être écrit dans le HTML de la sorte : <a href="#VendorStatut_tab" data-toggle="tab">
     * @param  string  $entity             Le nom de l'entité, partie du nom de classe exemple "VendorStatut" comme dans BusinessVendorStatut avec les majuscules
     * @param  string  $alt_entity_name    Nom alternatif de l'entité pour affichage html
     * @param  string  $label_placeholder  Placeholder pour l'input de type Select
     * @param  string  $active             Permet de définir la classe active sur la tab pour rendre l'onglet actif par défaut
     * @return string  $html               Retour html contenant toute la div class"tab-pane" contenant le contenu d'un seul onglet il ne faut pas oublier de déclarer
     *                                     le lien vers cet onglet dans le HTML externe
     */
    public static function getTabPaneReferentiel($entity, $alt_entity_name="", $label_placeholder="", $active="")
    {
        $html = "";
        
        
        $html .= ' <div class="tab-pane '.$active.'" id="'.$entity.'_tab"> ';
                    
        //MessageBoxError
        $html .= '    <div id="messagebox_'.$entity.'"></div>';
        
        $html .= '    <div id="ref_'.$entity.'">';

        $html .= self::showReferentielsTable($entity); 
                                 
        $html .= '    </div>';
                    
        $html .= '<div class="accordion" id="collapse_add_'.$entity.'">
                      <div class="accordion-group">
                        <div class="accordion-heading">
                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse_add_'.($entity).'" href="#collapse'.($entity).'">
                            <button class="btn btn-primary"> + Ajouter '.($alt_entity_name).'</button>
                          </a>
                        </div>
                        
                        <div id="collapse'.$entity.'" class="accordion-body collapse">
                          <div class="accordion-inner">
                            <form class="form-horizontal" id="form_add_'.($entity).'" name="form_add_'.($entity).'">
                                <strong id="form_errors_'.($entity).'" style="color:red;"></strong>
                                <div class="control-group">          
                                    <label class="control-label" for="input_label_'.($entity).'">'.$alt_entity_name.' : </label>
                                        <div class="controls">
                                            <input type="text" id="input_label_'.($entity).'" placeholder="'.$label_placeholder.'" name="input_label_'.($entity).'" required>
                                        </div>
                                </div>
                                
                                <button class="btn btn-primary" onClick="validAddReferentiel(&#34;'.$entity.'&#34;);return false;" data-dismiss="modal" aria-hidden="true"><i class="fa fa-check " style="padding-right: 0px"></i> Enregistrer</button>
                                <button type="reset" class="btn btn-inverse">Reset</button> 
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                </div>';
                
       return $html;       
  
    }


    /**
     * Affiche un tableau contenant les différentes valeur pour l'entitée donnée
     * @param   string  $entity     le nom de l'entité exemple "VendorType"
     * @return  string  $out        le tableau sous forme de HTML avec le JS contenu permettant les modif à la volée
     */
    public static function showReferentielsTable($entity)
    { 
        
        $class= "Manager".$entity;
        $manager = new $class();
        
        $list = $manager->getAll();
        
        $id = $manager->getId();
        $id = array_keys($id);
        $id = $id[0];
        
        $func_id = "get".ucfirst($id);
        
        $out = '<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° </th>
                                <th>Libellé</th>
                                <td>Action </td>
                            </tr>
                        </thead>
                        
                        <tbody>';
        foreach ($list as $item) {
            $out .= '       <tr>
                                <td>'.$item -> $func_id().'</td>
                                <td>'.$item -> getLabel().'</td> 
                                <td>
                                    <button class="btn" onClick="showDialogEditReferentiel( &#34;'.$entity.' &#34;, '.$item -> $func_id().'); return false;"> Modifier </button>
                                    <button class="btn btn-danger" onClick="showDialogSupprReferentiel( &#34;'.$entity.' &#34;, '.$item -> $func_id().'); return false;"> Suppr </button>
                                </td>
                            </tr>';
        }
        
        $out .= '       </tbody>
                </table>';
        return $out;        
    }	


    /**
     * Fonction permettant d'obtenir une liste déroulate html pour l'entité donnée avec 
     * toutes les valeurs existantes. On peut également préselectionner une valeur donnée grace
     * à son identifiant unique. Le <select></select> qui en ressort aura pour id et pour nom 
     * "select_EntityName" exemple "select_VendorType"
     * @param   string  $entity         Le nom de l'entitée exemple "VendorType"
     * @param   strong  $place_holder   Le placeholder du select qui sera la première option avec pour valeur 0
     * @param   string  $selected       L'ID primaire de la valeur de l'entité voulue pour la préselectionner
     * @return  string  $html           Le <select></select> avec toutes ses options en forme HTML 
     */
    public static function getSelectList($entity, $place_holder, $selected = "")
    {
        $class= "Manager".$entity;
        $manager = new $class();
        
        $list = $manager->getAll();
        
        $id = $manager->getId();
        $id = array_keys($id);
        $id = $id[0];
        
        $func_id = "get".ucfirst($id);
        
        $html = "<select id='select_".$entity."' name='select_".$entity."'>";
        $selected ? $html .= "   <option value='0'>$place_holder</option>" : $html .= "   <option value='0' selected>$place_holder</option>";
        foreach ($list as $item) {
            
            $html .= "<option value='".$item->$func_id()."' ";
            
            if($selected == $item->$func_id())
                $html .= " selected ";
            
            $html .= " >".$item->getLabel()."</option>";
            
        }
        $html .= "</select>";
        
        return $html;
    }
    
    public function getGridFilterList($entity, $filter_obj)
    {
        $class= "Manager".$entity;
        $manager = new $class();
        
        $list = $manager->getAll();
        
        $id = $manager->getId();
        $id = array_keys($id);
        $id = $id[0];
        
        $func_id = "get".ucfirst($id);
        
        foreach ($list as $item) {
            $filter_obj->addElement($item->$func_id(), $item->getLabel());
        }
        
        return $filter_obj;
    }
}

 ?>