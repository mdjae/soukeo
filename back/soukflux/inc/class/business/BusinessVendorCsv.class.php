<?php 
/**
 * 
 */
class BusinessVendorCsv extends BusinessEntity {
	
    
	protected $vendor_csv_id;
    protected $vendor_csv_name;
    protected $vendor_id;
    protected $auto;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setVendor_csv_id($vendor_csv_id)
    {
        $this -> vendor_csv_id = $vendor_csv_id;
    }
    
    public function setVendor_csv_name($vendor_csv_name)
    {
        $this -> vendor_csv_name = $vendor_csv_name;
    }
    
    public function setVendor_id($vendor_id)
    {
        $this -> vendor_id = $vendor_id;
    }
    
    public function setAuto($auto)
    {
        $this -> auto = $auto;
    }
    
    public function getVendor_csv_id()
    {
        return $this -> vendor_csv_id;
    }
    
    public function getVendor_csv_name()
    {
        return $this -> vendor_csv_name;
    }
    
    public function getVendor_id()
    {
        return $this -> vendor_id;
    }
    
    public function getAuto()
    {
        return $this -> auto;
    }
    
    public function isNew()
    {
        $manager = new ManagerVendorCsv();
        
        if (  $this -> vendor_csv_id || ($vendor_csv = $manager -> getBy(array("vendor_csv_name" => $this->vendor_csv_name))) ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>