<?php

class BusinessAttributCategory extends BusinessEntity {
	
	protected static    $attributCategoryManager;
    
	protected $id_attribut;
    protected $cat_id;
    protected $is_filter;

	public function __construct ($data = array()) {
		
        self::$attributCategoryManager = new ManagerAttributCategory();
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	

	public function setId_attribut($id_attribut)
	{
		$this -> id_attribut = $id_attribut;
	}
    
    public function setCat_id($cat_id)
    {
        $this -> cat_id = $cat_id;
    }
	
    public function setIs_filter($is_filter)
    {
        $this -> is_filter = $is_filter;
    }


    public function getId_attribut()
    {
        return $this -> id_attribut;
    }

    public function getCat_id( )
    {
        return $this -> cat_id;
    }
    
    public function getIs_filter()
    {
        return $this -> is_filter;
    }

    
    public function getAttribut()
    {
        $attributManager = new ManagerAttribut();
        
        if($attribut = $attributManager -> getById(array("id_attribut"=> $this->id_attribut))){
            return $attribut;
        }else{
            return null;
        }
    }
	
	public function isNew()
	{
		if (self::$attributCategoryManager -> getById(array("cat_id" => $this -> cat_id,
		                                                   "id_attribut" => $this -> id_attribut ))){
			return false;
		}else{
			return true;
		}
	}
    

	public function save()
	{

	}
	
}