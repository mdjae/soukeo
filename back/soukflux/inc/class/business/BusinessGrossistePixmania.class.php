<?php

class BusinessGrossistePixmania {

	protected $delimiter ;
	
	protected $errors = array();

	public function __construct ()
	{
		$this -> delimiter = ";";
		
	}

    /**
     * Fonction principal du batch
     */
    public function errorsExist($product) {

        if ($product['Label'] == "") {
            $message .= " NOM PRODUIT INVALIDE ";
        } elseif ($product['Weight'] == "" | $product['Weight'] == 0) {
            $message .= " POIDS INVALIDE ";
        } elseif ($product['EAN'] == "" && $product['PIXpro SKU'] == "") {
            $message .= " EAN + REF INEXISTANTES "; 
        } elseif ($product['Availability'] != "in stock") {
            $message .= " PRODUIT NON DISPONIBLE ";
        }



        if ($message != "") {
            $this->errors [] = $message . " POUR produit EAN : " . $product['EAN'] . " RefGrossiste : " . $product['PIXpro SKU'] . "";
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fonction principal du batch
     */
    public function formatFields($product, $prodGrOld = "") {

		//Données récupérée si poids déjà présent auparavant (cas où on a entré un poids à la main
		//après avoir importé)
		if($prodGrOld != null){
			$product['weight'] = $prodGrOld['WEIGHT'];
			$product['long_desc'] = $prodGrOld['DESCRIPTION'];
			$product['short_desc'] = $prodGrOld['DESCRIPTION_SHORT'];
		}
		else{
			//Si le poids volumétrique est supérieur au poids classique on prend le poids volumétrique
			
			if( (float)$product["Weight-Volume"] > (float)$product["Weight"]){
				$product['weight'] 		= $product["Weight-Volume"];
			}
			else{
				$product['weight'] = $product['Weight'];	
			}
			$product['long_desc'] 	= BusinessEncoding::toUTF8($product['Description']);
			$product['short_desc'] 	= BusinessEncoding::toUTF8($product['Description']);
			
		}
		
		if($product['Availability'] == "in stock") $product['stock'] = SystemParams::getParam('grossiste*stockdefaultgrossiste');
		else $product['stock'] = 0;
		
		$product['manufacturer']= BusinessEncoding::toUTF8($product['Brand']);
		$product['price'] 		= $product['PIXpro price'];
		$product['ref'] 		= $product['PIXpro SKU'];
		
		$product['picture1'] 	= $product['Picture'];
		
		$tmp = explode("_", $product['Picture']);
		$debut = $tmp[0] ;
		$fin = $tmp[1];
		$debut = substr($debut, 0, strlen($debut) - 2);
		
		$product['picture2'] 	= $debut."s_".$fin;
		$product['picture3'] 	= $debut."s_".$fin;
		
		$product['name'] 		= BusinessEncoding::toUTF8($product['Label']);
		
		
        //Gestion categories
       	//$tmp = str_replace("/", ">", BusinessEncoding::toUTF8($product['Market'])) ;
		//var_dump($product['Catgory']);
		trim($product['Category']) == "" | $product['Category']== " " ? $product['Category'] = "Non classé" : $product['Category'] = $product['Category'];
		
     	$tmp = BusinessEncoding::toUTF8( $product['Category'] . " > " . $product['Segment']) ;
		$product['category'] = $tmp;
		//$product['category'] = BusinessEncoding::toUTF8($product['Segment']) ;
 

        return $product;
    }

    public function cleanDescription($product) {
		
		$product['ref'] 		= $product['PIXpro SKU'];
        return $product;
    }

	public function getErrors() 
	{
		return $this -> errors;
	}
	
	public function getDelimiter()
	{
		return $this -> delimiter;
	}
}

?>
