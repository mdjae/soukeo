<?php 

/**
 * 
 */
abstract class BusinessExportCSV {
	
	protected $arrCSV 		= array();
	protected $path 		= "";
	protected $fileName 	= "";
	
	function __construct($path) {
		$this -> path = $path;
	}
	
	protected function makeLine($line = array())
	{
		$this -> arrCSV[] = $line ;
		
	}
	
	
	public function generateCSV($delimiter, $withBOM = false, $streaming = false)
	{
		if($streaming){
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Description: File Transfer');
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename={$this -> fileName}");
			header("Expires: 0");
			header("Pragma: no-cache");
			
			$fp = @fopen( 'php://output', 'w' );
		}
		else{
			file_put_contents($this -> path . $this -> fileName , "");
			
			if($fp = fopen($this -> path . $this -> fileName, "w+")){
            
                if($withBOM){
                    fputs($fp, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));   
                }   			    
			}else{
			    echo "!!!!! Fichier a ecrire inaccessible !!!!!".PHP_EOL;
			}
		
		}
		
		//Genere CSV
		foreach ($this -> arrCSV as $fields) {
		    fputcsv($fp, $fields, $delimiter);
		}
		
		fclose($fp);
	}
	
	/**
	 * Abstract method à rédéfinir pour créer contenu du CSV 
	 */
	public abstract function makeContent() ; 
	
	public function getPath()
	{
		return $this -> path ;
	}
	
	public function getFileName()
	{
		return $this -> fileName;
	}
	
}

 ?>