<?php 
/**
 * Classe de gestion sspécifique aux import de FK
 * Permet de formatter correctement les champs de données en fonctions des champs fournis par FK
 * Permet également de nettoyer les descriptions et de gérer les erreurs en cas d'EAN non présent 
 * ou Stock vide spécifique à FK
 */
class BusinessGrossisteFK  
{
	
	protected $delimiter ;
	
	protected $errors = array();
	

	/**
	 * Fonction __construct basique profitant de définir le délimiteur
	 * pour le CSV de FK
	 */
	public function __construct ()
	{
		$this -> delimiter = ";";
		
	}
	

	/**
	 * Gestion des erreurs de stock et d'EAN tout en fabriquant un tableau contenant
	 * les différentes erreurs sur les produits avec leur id_produit ainsi que la cause des erreurs.
	 * @param 	array 		$product 	l'array du produit à analyser
	 * @return 	boolean 				Retourne true si une erreur existe ou bien false
	 */
	public function errorsExist($product)
	{
		if( ($product['title'] == "-- no title --" | $product['title'] == "") | 
			($product['Weight']=="" | $product['Weight'] == 0) |
			($product['ean'] =="") | ($product['short_desc'] =="" && $product['long_desc'] == "")){
							
			$message = "";
			
			if($product['title'] == "-- no title --" | $product['title'] == ""){
				$message .= " NOM PRODUIT INVALIDE ";
			}
			if($product['Weight']=="" | $product['Weight'] == 0){
				$message .= " POIDS INVALIDE ";
			}
			if($product['ean']==""){
				$message .= " EAN INVALIDE ";
			}
			if($product['short_desc'] =="" && $product['long_desc'] == ""){
				$message .= " DESCRIPTION INVALIDE " ;
			}
			$this -> errors [] = $message." POUR produit EAN : ".$product['ean']. " RefGrossiste : ".$product['article_nr']."";
			return true;
		}else{
			return false;
		}
	}


	/**
	 * Formatte les champ FK afin de recréer un array avec des clés de tableau générique
	 * permettant d'avoir des fonctions uniques d'insertions une fois du coté du modèle.
	 * Il y a aussi la gestion du stock par défaut du grossiste et la création d'URL d'image permettant
	 * d'obtenir différentes qualités d'image sur le site FK
	 * @param 	array 	$product 	 	contenant le produit parsé du CSV
	 * @param 	array  	$productOld 	contenant le produit grossiste s'il existe déja avant le parsing
	 * @return 	array 	$product 		contenant le produit traité
	 */	
	public function formatFields($product, $prodGrOld=""){

		if($prodGrOld != null){
			$product['weight'] = $prodGrOld['WEIGHT'];
		}
		else{
			$product['weight'] = $product['Weight'] ;
		}
				
		$product['manufacturer'] = $product['car_manufacturer'];
		$product['price'] = $product['shop_price'];
		$product['ref'] = $product['article_nr'];
		
		$product['name'] = $product['title'];
		
		//Gestion categories
		if(substr($product['category'], -1) ==">")
			$product['category'] = substr($product['category'], 0,strlen($product['category'])-2) ;
			
		if(substr($product['category'], -2) =="> " || substr($product['category'], -2) =="> " )
			$product['category'] = substr($product['category'], 0,strlen($product['category'])-3) ;
		
		$product['category'] = str_replace("'", " ", $product['category'] );
		
		return $product;
	}

	
	/**
	 * Permet de nettoyer la description ou autres champs du produit FK
	 * @param 	array 	$product 	contenant les infos du produit
	 * @return 	array 	$product 	contenant les infos nettoyées du produit
	 */
	public function cleanDescription($product)
	{
		//Nettoyage chaines FK AUTOMOTIVE
		$product['long_desc'] = preg_replace(array('/\bFK\b/u', '/\bAutomotive\b/u'), array('Avahis', ' '), $product['long_desc']);
		$product['short_desc'] = preg_replace(array('/\bFK\b/u', '/\bAutomotive\b/u'), array('Avahis', ' '), $product['long_desc']);
		$product['ref'] = $product['article_nr'];
		
		return $product;
	}


	/**
	 * Permet de récupérer le tableau contenant les erreurs
	 * rencontrés lors du parsing du CSV notament grace à la fonction errorsExists
	 * @see errorsExist
	 * @return array 	errors  contenant les erreurs et messages d'erreurs
	 */	
	public function getErrors() 
	{
		return $this -> errors;
	}

	
	/**
	 * Permet de récupérer le délimiteur associé au CSV actuel
	 * @return string 	delimiter  représente le délimiteur utilisé dans le CSV
	 */		
	public function getDelimiter()
	{
		return $this -> delimiter;
	}
}

?>