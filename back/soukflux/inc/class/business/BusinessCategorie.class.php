<?php

class BusinessCategorie extends BusinessEntity {
    
    protected static    $db ;
    
    protected $cat_id;
    protected $cat_label;
    protected $cat_data;
    protected $parent_id;
    protected $cat_position;
    protected $cat_level;
    protected $cat_active;
    protected $count_prod;
    protected $om;
    protected $marge;
    protected $tva;
    protected $comm;
    

    public function __construct ($data = array()) {
        
        self::$db = new BusinessSoukeoModel();

        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    
    public function setCat_id($cat_id)
    {
        $this -> cat_id = $cat_id;
    }
    
    public function setCat_label($cat_label)
    {
        $this -> cat_label = $cat_label;
    }

    public function setParent_id($parent_id)
    {
        $this -> parent_id = $parent_id;
    }
        
    public function setCat_data($cat_data)
    {
        $this -> cat_data = $cat_data;
    }
    
    public function setCat_position($cat_position)
    {
        $this -> cat_position = $cat_position ;
    }

    public function setCat_level($cat_level)
    {
        $this -> cat_level = $cat_level;
    }
    
    public function setCat_active($cat_active)
    {
        $this -> cat_active = $cat_active;
    }
    
    public function setCount_prod($count_prod)
    {
        $this -> count_prod = $count_prod;
    }
        
    public function setOm($om)
    {
        $this -> om = $om ;
    }    
    
    public function setMarge($marge)
    {
        $this -> marge = $marge;
    }
    
    public function setTva($tva)
    {
        $this -> tva = $tva ;
    }
    
    public function setComm($comm)
    {
        $this -> comm = $comm;
    }
    
    //////////////////////////// GETTER ////////////////////////////    
    
    public function getCat_id()
    {
        return $this -> cat_id ;
    }
    
    public function getCat_label()
    {
        return $this -> cat_label ;
    }

    public function getParent_id()
    {
        return $this -> parent_id ;
    }
        
    public function getCat_data()
    {
        return $this -> cat_data ;
    }
    
    public function getCat_position()
    {
        return $this -> cat_position ;
    }

    public function getCat_level()
    {
        return $this -> cat_level ;
    }
    
    public function getCat_active()
    {
        return $this -> cat_active ;
    }
    
    public function getCount_prod()
    {
        return $this -> count_prod ;
    }
        
    public function getOm($om)
    {
        return $this -> om ;
    }    
    
    public function getMarge()
    {
        return $this -> marge;
    }
    
    public function getTva()
    {
        return $this -> tva;
    }
    
    public function getCat_html()
    {
        $cat_string = self::$db ->getStrCateg($this->cat_id);
        
        return substr($cat_string, 0, strlen($cat_string) - 2);
    }
    
    public function hasChild()
    {
        $catManager = new ManagerCategorie();
        
        if($cat_list = $catManager -> getBy(array("parent_id" => $this->cat_id))){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function getComm()
    {
        return $this -> comm;
    }
    
    public function isNew()
    {
        if ($this -> getCat_id()){
            return false;
        }else{
            return true;
        }
    }
    
    public function save()
    {
        
    }

}