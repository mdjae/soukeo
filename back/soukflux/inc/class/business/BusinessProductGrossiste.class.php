<?php

class BusinessProductGrossiste extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $id_product;
	protected $grossiste_id;
	protected $ref_grossiste;
	protected $quantity;
	protected $ean;
	protected $price_product;
	protected $name_product;
	protected $description;
	protected $description_short;
	protected $url;
	protected $image_product;
	protected $image_product_2;
	protected $image_product_3;
	protected $category;
	protected $manufacturer;
	protected $weight;
	protected $active;
	protected $crawled;
	protected $grossiste_nom;
	

	public function __construct ($id_product = false) {
		
		self::$db = new BusinessSoukeoModel();
			
		if($id_product){
			$this->id_product = $id_product;
			$data = self::$db -> getItemByIdProduct($this->id_product);
			$data = $data[0];
			if (!$data){
				return false;
			}else{
				$this -> hydrate($data);
			} 	
		}elseif(!empty($data)){
			
		}
	}
	
	
	public function setId_product($id_product)
	{
		$this -> id_product = $id_product;
	}
	
	public function setGrossiste_id($grossiste_id)
	{
		$this -> grossiste_id = $grossiste_id;
	}

	public function setRef_grossiste($ref_grossiste)
	{
		$this -> ref_grossiste = $ref_grossiste;
	}
		
	public function setQuantity($quantity)
	{
		$this -> quantity = $quantity;
	}
	
	public function setEan($ean)
	{
		$this -> ean = $ean;
	}
	
	public function setPrice_product($price_product){
		$this -> price_product = $price_product;
	}
	
	public function setName_product($name_product){
		$this -> name_product = $name_product;
	}
	
	public function setDescription($description)
	{
		$this -> description = $description ;
	}
	
	public function setDescription_short($description_short)
	{
		$this -> description_short = $description_short;
	}
	
	public function setUrl($url)
	{
		$this -> url = $url;
	}
	
	public function setImage_product($image_product)
	{
		$this -> image_product = $image_product;
	}
	
	public function setImage_product_2($image_product_2)
	{
		$this -> image_product_2 = $image_product_2;
	}
	
	public function setImage_product_3($image_product_3)
	{
		$this -> image_product_3 = $image_product_3;
	}
	
	public function setCategory($category)
	{
		$this -> category = $category;
	}
	
	public function setManufacturer($manufacturer)
	{
		$this -> manufacturer = $manufacturer;
	}
	
	public function setWeight($weight)
	{
		$this -> weight = $weight;
	}
	
	public function setActive($active)
	{
		$this -> active = $active;
	}
	
	public function setCrawled($crawled)
	{
		$this -> crawled = $crawled;
	}
	
	public function setGrossiste_nom($grossiste_nom)
	{
		$this -> grossiste_nom = $grossiste_nom;
	}
	
	//GET
	public function getId_product()
	{
		return $this -> id_product ;
	}
	
	public function getGrossiste_id()
	{
		return $this -> grossiste_id ;
	}

	public function getRef_grossiste()
	{
		return $this -> ref_grossiste;
	}
		
	public function getQuantity()
	{
		return $this -> quantity ;
	}
	
	public function getEan()
	{
		return $this -> ean;
	}
	
	public function getPrice_product()
	{
		return $this -> price_product ;
	}
	
	public function getName_product()
	{
		return $this -> name_product ;
	}
	
	public function getDescription()
	{
		return $this -> description ;
	}
	
	public function getDescription_short()
	{
		return $this -> description_short ;
	}
	
	public function getUrl()
	{
		return $this -> url ;
	}
	
	public function getImage_product()
	{
		return $this -> image_product ;
	}
	
	public function getImage_product_2()
	{
		return $this -> image_product_2 ;
	}
	
	public function getImage_product_3()
	{
		return $this -> image_product_3 ;
	}
	
	public function getCategory()
	{
		return $this -> category ;
	}
	
	public function getManufacturer()
	{
		return $this -> manufacturer ;
	}
	
	public function getWeight()
	{
		return $this -> weight ;
	}
	
	public function getActive()
	{
		return $this -> active ;	
	}
	
	public function getCrawled()
	{
		return $this -> crawled ;
	}
	
	public function getGrossiste_nom()
	{
		return $this -> grossiste_nom ;
	}	
				
	public function save()
	{
		self::$db -> updateProductGrossiste($this);
	}
}