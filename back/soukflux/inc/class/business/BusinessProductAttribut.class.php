<?php

class BusinessProductAttribut extends BusinessEntity {
	
	protected static    $productAttributManager;
    
	protected $id_produit;
	protected $id_attribut;
    protected $value;
    protected $active;

	public function __construct ($data = array()) {
		
        self::$productAttributManager = new ManagerProductAttribut();
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setId_produit($id_produit )
	{
		$this -> id_produit = $id_produit;
	}

	public function setId_attribut($id_attribut)
	{
		$this -> id_attribut = $id_attribut;
	}
	
    public function setValue($value)
    {
        $this -> value = $value;
    }
    
    public function setActive($active)
    {
        $this -> active = $active ;
    }

    public function getId_produit( )
    {
        return $this -> id_produit;
    }

    public function getId_attribut()
    {
        return $this -> id_attribut;
    }
    
    public function getValue()
    {
        return $this -> value;
    }
    
    public function getActive()
    {
        return $this -> active;
    }
    
    public function getAttribut()
    {
        $attributManager = new ManagerAttribut();
        
        if($attribut = $attributManager -> getById(array("id_attribut"=> $this->id_attribut))){
            return $attribut;
        }else{
            return null;
        }
    }
	
	public function isNew()
	{
		if (self::$productAttributManager -> getById(array("id_produit" => $this -> id_produit,
		                                                   "id_attribut" => $this -> id_attribut ))){
			return false;
		}else{
			return true;
		}
	}
    

	public function save()
	{

	}
	
}