<?php

class BusinessVendorCategorie extends BusinessEntity {
	
	protected static    $vendorCategorieManager;
    
	protected $entity_id;
	protected $cat_id;
    protected $cat_label;

	public function __construct ($data = array()) {
		
        self::$vendorCategorieManager = new ManagerVendorCategorie();
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setEntity_id($entity_id )
	{
		$this -> entity_id = $entity_id ;
	}

	public function setCat_id($cat_id)
	{
		$this -> cat_id = $cat_id;
	}
	
    public function setCat_label($cat_label)
    {
        $this -> cat_label = $cat_label;
    }
    
    public function getEntity_id()
    {
        return $this -> entity_id;
    }

    public function getCat_id()
    {
        return $this -> cat_id;
    }	
    
    public function getCat_label()
    {
        return $this -> cat_label;
    }
	
	public function isNew()
	{
        
		if (self::$vendorCategorieManager -> getById(array("entity_id" => $this -> entity_id,
		                                                   "cat_id"    => $this -> cat_id ))){
			return false;
		}else{
			return true;
		}
	}
    

	public function save()
	{

	}
	
}