<?php

class BusinessFactureCredit extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $facture_credit_id;
	protected $facture_id;
	protected $vendor_id;
	protected $total_prix;
    protected $grand_total;
	protected $email;
	protected $tel;
    protected $email_send;
    protected $payment_type_id;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	public function setFacture_credit_id($facture_credit_id)
	{
		$this -> facture_credit_id = $facture_credit_id;
	}
		
	public function setFacture_id($facture_id ){
		$this -> facture_id = $facture_id ;
	}
	
	public function setVendor_id($vendor_id)
	{
		$this -> vendor_id = $vendor_id;
	}
	
	public function setTotal_prix($total_prix)
	{
		$this -> total_prix = $total_prix;
	}
    
    public function setGrand_total($grand_total)
    {
        $this -> grand_total = $grand_total;
    }
	
	public function setEmail($email)
	{
		$this -> email = $email;
	}
	
	public function setTel($tel)
	{
		$this -> tel = $tel;
	}
	   
    public function setEmail_send($email_send)
    {
        $this -> email_send = $email_send;
    }
    
    public function setPayment_type_id($payment_type_id)
    {
        $this -> payment_type_id = $payment_type_id;
    }

	public function getFacture_credit_id()
	{
		return $this -> facture_credit_id ;
	}
	
	public function getFacture_id()
	{
		return $this -> facture_id;
	}
	
	public function getVendor_id()
	{
		return $this -> vendor_id;
	}

	public function getTotal_prix()
	{
		return $this -> total_prix;
	}
	
    public function getGrand_total()
    {
        return $this -> grand_total;
    }

	public function getEmail()
	{
		return $this -> email ;
	}
	
	public function getTel()
	{
		return $this -> tel;
	}
    
    public function getEmail_send()
    {
        return $this -> email_send;
    }
    
    public function getPayment_type_id()
    {
        return $this -> payment_type_id;
    }
    
    public function getPayment_type_label()
    {
        $manager = new ManagerPaymentType();
        
        if($payment_type = $manager -> getById(array("payment_type_id" => $this -> payment_type_id))){
            return  $payment_type -> getLabel();    
        }else{
            return "Type de paiement inconnu";
        }
        
    }
    
    public function getAllLines()
    {
        $factureCreditLineManager = new ManagerFactureCreditLine();
        
        return $factureCreditLineManager -> getAll(array("facture_credits_id" => $this -> facture_credit_id));
    }
	
	public function isNew()
	{
		if ($this -> getFacture_credit_id()){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}