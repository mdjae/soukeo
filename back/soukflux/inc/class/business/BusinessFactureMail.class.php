<?php 
/**
 * 
 */
class BusinessFactureMail {
	
    protected $facture_type;
    protected $vendor;
    protected $facture;
    protected $factureCom;
    protected $factureCredit;
    protected $transport;
    protected $message;
    protected $mail_client;
    
    protected static $vendorManager;
    protected static $factureManager;
    
    /**
     * le consctructeur de base permettant d'instancier les différents objets nécessaires au mail
     * @param  $facture_id  l'identifiant de la facture à régenerer et envoyer dans le mail
     */
	function __construct($facture_id, $facture_type = "com") {
	    
        global $applicationMode;
        
        self::$vendorManager         = new ManagerVendor();
        self::$factureManager        = new ManagerFacture();
        
        $this -> facture_type = $facture_type;
        
        $this -> facture      = self::$factureManager -> getFactureById($facture_id);
        
        if($facture_type == "com"){
            $this -> factureCom   = $this -> facture -> getFactureCom();
            $this -> vendor       = self::$vendorManager -> getBy(array("vendor_id" => $this -> factureCom -> getVendor_id() ));
        }
        elseif($facture_type == "credit"){
            $this -> factureCredit   = $this -> facture -> getFactureCredit();
            $this -> vendor       = self::$vendorManager -> getBy(array("vendor_id" => $this -> factureCredit -> getVendor_id() ));
        }

        $this -> transport    = Swift_MailTransport::newInstance();
        $this -> message      = Swift_Message::newInstance();
        
        $this -> message -> setFrom(array(SystemParams::getParam("marketplace*directionmail") => "Direction Avahis.com"));
        
        if($applicationMode == "DEV" ) {
            $this -> message -> setFrom(array('philippe.lattention@silicon-village.fr' => "Direction Avahis.com"));
            $this -> mail_client = 'philippe.lattention@silicon-village.fr' ;
            //$this -> message -> setCc('philippe.lattention@silicon-village.fr');
            $this -> message -> setCc('edsoukeo@gmail.com');
            
            //$this -> mail_client = $this -> vendor -> getEmail() ;
            
        }
        else{
            $this -> mail_client = $this -> vendor -> getEmail() ;
            $this -> message -> setCc(SystemParams::getParam("marketplace*directionmail"));
        }
        
        $this -> message -> setTo(array($this -> mail_client));
        
        $this -> message -> setSubject("Paiement Avahis.com");
	}
    
    /**
     * Rempli le corps du message avec un message prédefini ou bien un défini à la volée
     */
    public function fillBody($custom_text = null)
    {
        if(!$custom_text){
            
            $cid = $this -> message -> embed(Swift_Image::fromPath(realpath(__DIR__ . '/../../..') .'/skins/soukeo/img/direction_avahis.jpg') );
            
            $this -> message -> setBody("
                                         <p>Bonjour,<br/>
                                            Veuillez trouver ci-joint le récapitulatif de vos ventes jusqu'au ".smartDateNoTime($this -> factureCom -> getDate_limit()).", ainsi que la facture correspondant aux commissions perçues par Avahis.com durant la période en référence.<br/>
                                            Votre paiement a été effectué par virement bancaire sur votre compte enregistré.<br/>
                                            Merci de votre confiance.<br/>
                                            Facture N° : <b>".$this-> facture -> getFacture_code()."</b>
                                         </p><img src='".$cid."' alt='Logo Avahis' />" ,'text/html');
        }
        else {
            $this -> message -> setBody($custom_text, 'text/html');
        }
    }
    
    public function fillBodyCredit($custom_text = null)
    {
        if(!$custom_text){
            
            $cid = $this -> message -> embed(Swift_Image::fromPath(realpath(__DIR__ . '/../../..') .'/skins/soukeo/img/direction_avahis.jpg') );
            
            $this -> message -> setBody("
                                         <p>Bonjour,<br/>
                                            Veuillez trouver ci-joint la facture correspondant à votre achat de crédits sur Avahis.com.<br/>
                                            Votre paiement a été effectué par....<br/>
                                            Merci de votre confiance.<br/>
                                            Facture N° : <b>".$this-> facture -> getFacture_code()."</b>
                                         </p><img src='".$cid."' alt='Logo Avahis' />" ,'text/html');
        }
        else {
            $this -> message -> setBody($custom_text, 'text/html');
        }
    }
    
    /**
     * Attache l'objet PDF au mail pour l'envoi
     */
    public function attachFacture($other_attachments = null)
    {
        if($this -> facture_type == "com"){
            $data =  $this->generatePDF();    
        }elseif($this -> facture_type == "credit"){
            $data =  $this->generatePDFcredit();
        } 
        
        
        $attachment = Swift_Attachment::newInstance( $data, $this -> facture -> getFacture_code().'.pdf', 'application/pdf' );
           
        $this -> message -> attach($attachment );
    }

        
    /**
     * Envoi le mail et permet de savoir si tout s'est bien déroulé
     * @return boolean  true si mail bien envoyé sinon false
     */
    public function sendMail()
    {
        $mailer = Swift_Mailer::newInstance($this -> transport);

        if($mailer -> send($this -> message)) 
            return true;
        else
            return false;   
    }
    
    
    /**
     * Permet la génération du pdf grâce à la bibliothèque 
     * HTML2PDF
     * @return $content_PDF    Le pdf généré
     */
    protected function generatePDF()
    {
        $facture    = $this -> facture;

        $emeteur    = $this -> facture -> getEmeteur_addr();
        
        $recipient  = $this -> facture -> getRecipient_addr();
        
        $factureCom = $this -> factureCom;
        
        $lines      = $factureCom -> getAllLines();

        ob_get_clean();
        ob_start();
        include(realpath(__DIR__ . '/../../..') .'/commercants/facturation/factu.php');
        $content = ob_get_clean();

        // convert in PDF
        require_once(realpath(__DIR__ . '/../../..') . '/thirdparty/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('P', 'A4', 'fr');
            // $html2pdf->setModeDebug();
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            
            $content_PDF = $html2pdf->Output('', 'S');

            return $content_PDF;
            
        }   
        catch(HTML2PDF_exception $e) {
            echo $e;
            exit;
        } 
    }

    public function generatePDFcredit()
    {
        $facture    = $this -> facture;

        $emeteur    = $this -> facture -> getEmeteur_addr();
        
        $recipient  = $this -> facture -> getRecipient_addr();
        
        $factureCredit = $this -> factureCredit;
        
        $lines      = $factureCredit -> getAllLines();

        ob_get_clean();
        ob_start();
        include(realpath(__DIR__ . '/../../..') .'/commercants/facturation/factucredit.php');
        $content = ob_get_clean();

        // convert in PDF
        require_once(realpath(__DIR__ . '/../../..') . '/thirdparty/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('P', 'A4', 'fr');
            // $html2pdf->setModeDebug();
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            
            $content_PDF = $html2pdf->Output('', 'S');

            return $content_PDF;
            
        }   
        catch(HTML2PDF_exception $e) {
            echo $e;
            exit;
        }  
    }

    public function getMessage()
    {
        return $this -> message;
    }
    
    public function getMail_client()
    {
        return $this -> mail_client;
    }
    
    public function getVendor()
    {
        return $this -> vendor;
    }
}

 ?>