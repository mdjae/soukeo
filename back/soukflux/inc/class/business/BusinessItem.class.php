<?php

class BusinessItem extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $item_id;
	protected $sku;
	protected $vendor_sku;
	protected $udropship_vendor;
	protected $vendor_id;
	protected $order_id;
	protected $order_id_grossiste;
	protected $increment_id;
	protected $annonce_reception_id;
	protected $name;
	protected $base_row_total_incl_tax;
	protected $row_weight;
	protected $qty_ordered;
	protected $category_id;
	protected $date_created;
	protected $date_updated;
	protected $litiges;
	protected $payment;
	protected $facture_id;
	
	protected $categ;
	protected $productGrossiste;


	public function __construct ($item_id = false) {
		
		self::$db = new BusinessSoukeoModel();
			
		if($item_id){
			$this->item_id = $item_id;
			$data = self::$db -> getItemByItemId($this->item_id);
			$data = $data[0];
			if (!$data){
				return false;
			}else{
				$this -> hydrate($data);
			} 	
		}
		$this -> setCateg();
		$this -> setProductGrossiste();
	}
	
	
	public function setItem_id($item_id)
	{
		$this -> item_id = $item_id;
	}
	
	public function setSku($sku)
	{
		$this -> sku = $sku;
	}
	
	public function setVendor_sku($vendor_sku)
	{
		$this -> vendor_sku = $vendor_sku;
	}
	
	public function setUdropship_vendor($udropship_vendor)
	{
		$this -> udropship_vendor = $udropship_vendor;
	}
	
	public function setOrder_id($order_id){
		$this -> order_id = $order_id;
	}
	
	public function setOrder_id_grossiste($order_id_grossiste){
		$this -> order_id_grossiste = $order_id_grossiste;
	}
	
	public function setIncrement_id($increment_id)
	{
		$this -> increment_id = $increment_id ;
	}
	
	public function setAnnonce_reception_id($annonce_reception_id)
	{
		$this -> annonce_reception_id = $annonce_reception_id;
	}
	
	public function setName($name)
	{
		$this -> name = $name;
	}
	
	public function setBase_row_total_incl_tax($base_row_total_incl_tax)
	{
		$this -> base_row_total_incl_tax = $base_row_total_incl_tax;
	}
	
	public function setRow_weight($row_weight)
	{
		$this -> row_weight = $row_weight;
	}
	
	public function setQty_ordered($qty_ordered)
	{
		$this -> qty_ordered = $qty_ordered;
	}
	
	public function setCategory_id($category_id)
	{
		$this -> category_id = $category_id;	
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
	
	public function setLitiges($litiges)
	{
		if($litiges == "1" | $litiges == "0"){
			$this -> litiges = $litiges;	
		}
	}
	
	public function setPayment($payment)
	{
		if($payment == "1" | $payment == "0"){
			$this -> payment = $payment;	
		}
	}
	
	public function setFacture_id($facture_id)
	{
		$this -> facture_id = $facture_id;
	}
	
	public function getItem_id() 
	{
		return $this -> item_id;
	}
	
	public function getSku()
	{
		return $this -> sku ;
	}
	
	public function getUdropship_vendor()
	{
		return $this -> udropship_vendor;
	}
	
	public function getOrder_id()
	{
		return $this -> order_id;
	}
	
	public function getOrder_id_grossiste()
	{
		return $this -> order_id_grossiste;
	}
		
	public function getIncrement_id()
	{
		return $this -> increment_id;
	}

	public function getAnnonce_reception_id()
	{
		return $this -> annonce_reception_id;
	}
		
	public function getName()
	{
		return $this -> name ;
	}
	
	public function getBase_row_total_incl_tax()
	{
		return $this -> base_row_total_incl_tax;
	}
	
	public function getRow_weight()
	{
		return $this -> row_weight;
	}
	
	public function getQty_ordered()
	{
		return $this -> qty_ordered ;
	}		
	
	public function getCategory_id()
	{
		return $this -> category_id;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function getLitiges()
	{
		return $this -> litiges;
	}
	
	public function getPayment()
	{
		return $this -> payment;
	}
	
	public function getFacture_id()
	{
		return $this -> facture_id;
	}
	
	public function setProductGrossiste()
	{
		$this -> productGrossiste = new BusinessProductGrossiste(self::$db -> getIdProductGrossisteFromItem($this->getSku()));
		
	}
	
	public function getProductGrossiste()
	{
		return $this -> productGrossiste;
	}
	
	public function setCateg($categ)
	{
		if($categ ==''){
			$this -> categ = self::$db -> getCategAvFromItem($this->getSku());
		}else{
			$this -> categ = $categ;
		}
	}

	public function getCateg()
	{
		return $this -> categ ;
	}
	
    public function getPo()
    {
        return new BusinessPo(self::$db -> getPoByOrderIdVendorId($this->order_id, $this->udropship_vendor));      
    }
    
	public function save()
	{
		self::$db -> updateItem($this);
	}
}