<?php 
/**
 * 
 */
class BusinessPaymentType extends BusinessEntity {
	
    
	protected $payment_type_id;
    protected $label;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setPayment_type_id($payment_type_id)
    {
        $this -> payment_type_id = $payment_type_id;
    }
    
    public function getPayment_type_id()
    {
        return $this -> payment_type_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function isNew()
    {
        $manager = new ManagerPaymentType();
        
        if ( $this -> payment_type_id || ($state = $manager -> getBy(array("label" => $this->label))) ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>