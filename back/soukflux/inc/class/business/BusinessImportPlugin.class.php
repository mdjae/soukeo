<?php 

/**
 * Classe abstraite contenant les fonctions et propriétés communes
 * à toutes les classes d'import des différents plugins
 * 
 */
abstract class BusinessImportPlugin {
	
	
	protected $url_to_parse 		= "";		//URL de l'ecommerçant à parser
	protected $vendorID 			= ""; 		//VENDOR_ID de l'ecommercant
	protected $productListAttribute = null; 	//Liste produits avec ses attributs finale array()
	protected $attributeList 		= null; 	//Liste des différents attributs (entete) array()
	
	protected $logfile              = "";
	
	protected $nbInsert 			= 0; 		//log
	protected $nbInsertAttr 		= 0;		//log
	protected $nbUpdate 			= 0;		//log  A IMPLEMENTER
	protected $nbUpdateAttr 		= 0;		//log  A IMPLEMENTER
	protected $nbErrors 			= 0;
    
	
	private static $db;
	
	/**
	 * Constructeur, cette classe prend en paramètre l'URL et l'ID du vendeur
	 */
	function __construct($url, $vendorID, $logfile ="") 
	{
		$this->url_to_parse 	= $url;
		$this->vendorID 		= $vendorID;
        $this->logfile          = $logfile;
	}
	
    abstract function run();
	
	/**
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param url 				String 		L'url qui va contenir le CSV
	 * @param nbChampsFixes 	Int 		Le nombre de champs fixe qui sont communs à tous produits
	 * @param delimiter 		String 		Le délimiteur choisi dans le CSV
	 */
	protected function parseCSV($url, $nbChampsFixes, $delimiter, $bom = false)
	{
        $max_trys = SystemParams::getParam('system*plugin_max_trys');
        
        for ($i=1; $i <= $max_trys ; $i++) { 
            
            $file = curl_file_get_contents($url);    
        
            if($file === false){
                $this->_appendLog("ERREUR tentative $i Le contenu de l'URL n a pas pu etre recupere !");
                
                if($i == $max_trys){
                    $this->_appendLog("NOMBRE MAX DE TENTATIVES ATTEINTS Le contenu de l'URL n a pas pu etre recupere !");
                    $this->sendWarningMail();
                    return false;
                }
            }
            else{
                $this->_appendLog("Contenu de l'URL recupere, debut du parsing CSV ... !");
                $data = str_getcsv($file, "\r\n"); //parse les lignes 
                
                if($bom){
                    $data = str_replace(array("\r\n","\n","\r"),'',$data);
                }
        
                //Pour chaque ligne on crée un produit
                foreach ($data as $row) {
                    $productList[] = str_getcsv(str_replace("\n", "", $row), $delimiter, '"');
                }
                
                //L'entete est le "premier produit" de la lite
                $headerList = $productList[0];
                
                if($bom){
                    $headerList[0] = utf8_encode($headerList[0]);
                    $headerList[0] = str_replace(array("\r\n","\n","\r","ï»¿"),'',$headerList[0]) ;
                }
                
                //On enlève l'entête de la liste produits
                $productList = array_slice($productList, 1);
                
                //Création du tableau associatif
                foreach ($productList as $p) {
                    $this->productListAttribute[] = array_combine($headerList, $p);
                }
                
                //Les attributs commencent dans l'entete après les 30 premiers champs fixes
                $this->attributeList = array_slice($headerList,$nbChampsFixes);            
                
                return true;
            }
        }
	}

    
    public function sendWarningMail()
    {
        global $applicationMode;
        
        $vendorManager = new ManagerVendor() ;
        $vendor = $vendorManager -> getBy(array("vendor_id" => $this->vendorID));
        
        $transport = Swift_MailTransport::newInstance();
        
        //Le mail
        $message = Swift_Message::newInstance();
        $message -> setSubject('ERREUR de récupération produits '.$vendor->getVendor_nom().' N° '.$this->vendorID.' ');
        
        if($applicationMode == "DEV" ) {
            $message -> setFrom('philippe.lattention@silicon-village.fr');
            $message -> setTo(array('philippe.lattention@silicon-village.fr'));
        }else{
            $message -> setFrom(SystemParams::getParam("system*email_from"));
            $message -> setTo(array('philippe.lattention@silicon-village.fr', 'frederic.hewkianchong@silicon-village.fr', 'thomas@silicon-village.fr'));    
        }   

        $message -> setBody('Après <strong>'.SystemParams::getParam('system*plugin_max_trys').
                            " tentatives </strong>le contenu de l'url du commerçant n'a pas pu être
                            récupérée.<br/>
                            Url injoignable : ".$this->url_to_parse."<br/>
                             
                            <br/><em>Vous pouvez toujours augmenter le nombre de tentatives possibles : le paramètre est modifiable dans les options système Soukflux. 
                                    Ainsi que le temps avant time-out lors des connexions cURL actuellement à : <strong>".SystemParams::getParam('system*plugin_curl_timeout')." seconde(s)</strong></em>"
                             
                             , 'text/html');
        
        $mailer = Swift_Mailer::newInstance($transport);
        
        $mailer -> send($message); 
    }
    
	/**
	 * Pour le log, retourne le nombre d'insert effectués pour les
	 * produits
	 * @return nbInsert Int nb de Produits insert/update
	 */
	public function getNbInsert()
	{
		return $this->nbInsert;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'insert effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbInsertAttr()
	{
		return $this->nbInsertAttr;
	}
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * produits
	 * @return nbInsert Int nb de Produits insert/update
	 */
	public function getNbUpdate()
	{
		return $this->nbUpdate;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbUpdateAttr()
	{
		return $this->nbUpdateAttr;
	}
	
	/**
	 * @return url_to_parse String l'url de l'e-commerçant
	 */
	public function getUrl_to_parse()
	{
		return $this->url_to_parse;	
	}
	
	
	/**
	 * @return vendorID Int l'id du vendeur
	 */
	public function getVendorID()
	{
		return $this->vendorID;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbErrors()
	{
		return $this->nbErrors;
	}
		
    /**
      * Ajoute du texte � la log du batch
     */
    protected function _appendLog($txt)
    {
        
        //$this->_log .= $txt;
        file_put_contents($this->_logfile, $txt. "\r\n", FILE_APPEND);
        echo $txt . "\r\n";
        
        
    }
}
?>