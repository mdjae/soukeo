<?php

class BusinessFactureAddress extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $facture_address_id;
	protected $name;
	protected $siret;
	protected $street;
	protected $city; 
	protected $zip_code;
	protected $country;
	protected $date_created;
	protected $date_updated;


	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setFacture_address_id($facture_address_id ){
		$this -> facture_address_id = $facture_address_id ;
	}
	
	public function setName($name)
	{
		$this -> name = $name;
	}
	
	public function setSiret($siret)
	{
		$this -> siret = $siret;
	}
	
	public function setStreet($street)
	{
		$this -> street = $street;
	}
	
	public function setCity($city)
	{
		$this -> city = $city;
	}
	
	public function setZip_code($zip_code)
	{
		$this -> zip_code = $zip_code;
	}
	
	public function setCountry($country)
	{
		$this -> country = $country;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}

	public function getFacture_address_id(){
		return $this -> facture_address_id;
	}

	public function getName()
	{
		return $this -> name ;
	}
	
	public function getSiret()
	{
		return $this -> siret;
	}
	
	public function getStreet()
	{
		return $this -> street ;
	}
	
	public function getCity()
	{
		return $this -> city;
	}
	
	public function getZip_code()
	{
		return $this -> zip_code;
	}
	
	public function getCountry()
	{
		return $this -> country;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function isNew()
	{
		if ($this -> getFacture_address_id()){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}