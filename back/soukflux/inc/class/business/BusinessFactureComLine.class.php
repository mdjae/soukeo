<?php

class BusinessFactureComLine extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $line_id;
	protected $facture_com_id;
	protected $item_id;
	protected $order_increment_id;
	protected $qty;
	protected $designation;
	protected $price_ttc; 
	protected $category_id;
	protected $percent_comm;
	protected $mt_comm;
	protected $mt_frais_banq;
	protected $litiges;
	protected $raison_litige;
	protected $date_created;
	protected $date_updated;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setLine_id($line_id ){
		$this -> line_id = $line_id ;
	}

	public function setFacture_com_id($facture_com_id)
	{
		$this -> facture_com_id = $facture_com_id;
	}
	
	public function setItem_id($item_id)
	{
		$this -> item_id = $item_id;
	}
	
	public function setOrder_increment_id($order_increment_id)
	{
		$this -> order_increment_id = $order_increment_id;
	}
	
	public function setQty($qty)
	{
		$this -> qty = $qty;
	}

	public function setDesignation($designation)
	{
		$this -> designation = $designation;
	}
		
	public function setPrice_ttc($price_ttc)
	{
		$this -> price_ttc = $price_ttc;
	}
	
	public function setCategory_id($category_id)
	{
		$this -> category_id = $category_id;
	}
	
	public function setPercent_comm($percent_comm)
	{
		$this -> percent_comm = $percent_comm;
	}
	
	public function setMt_comm($mt_comm)
	{
		$this -> mt_comm = $mt_comm;
	}
	
	public function setMt_frais_banq($mt_frais_banq)
	{
		$this -> mt_frais_banq = $mt_frais_banq;
	}
	
	public function setlitiges($litiges)
	{
		$this -> litiges = $litiges;
	}
	
	public function setRaison_litige($raison_litige)
	{
		$this -> raison_litige = $raison_litige;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}

	public function getLine_id(){
		return $this -> line_id;
	}

	public function getFacture_com_id()
	{
		return $this -> facture_com_id;
	}
	
	public function getItem_id()
	{
		return $this -> item_id;
	}

	public function getOrder_increment_id()
	{
		return $this -> order_increment_id ;
	}
	
	public function getQty()
	{
		return $this -> qty ;
	}
		
	public function getDesignation()
	{
		return $this -> designation;
	}
	
	public function getPrice_ttc()
	{
		return $this -> price_ttc;
	}
	
	public function getCategory_id()
	{
		return $this -> category_id;
	}
	
	public function getPercent_comm()
	{
		return $this -> percent_comm;
	}
	
	public function getMt_comm()
	{
		return $this -> mt_comm;
	}
	
	public function getMt_frais_banq()
	{
		return $this -> mt_frais_banq;
	}	

	public function getlitiges()
	{
		return $this -> litiges;
	}
	
	public function getRaison_litige()
	{
		return $this -> raison_litige;
	}
		
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function isNew()
	{
		if (self::$db -> getLineFactuCom($this -> getLine_id(), $this -> getFacture_com_id())){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}