<?php

class BusinessGroupDashboardFunctionality extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $group_id;
	protected $dashboard_functionality_id;

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setGroup_id($group_id )
	{
		$this -> group_id = $group_id ;
	}

	public function setDashboard_functionality_id($dashboard_functionality_id)
	{
		$this -> dashboard_functionality_id = $dashboard_functionality_id;
	}
	
    public function getGroup_id()
    {
        return $this -> group_id;
    }

    public function getDashboard_functionality_id()
    {
        return $this -> dashboard_functionality_id;
    }	
	
	public function isNew()
	{
	    $groupFunctionalityManager  = new ManagerGroupDashboardFunctionality();
        
		if ($groupFunctionalityManager -> getById(array("group_id" => $this -> group_id,
		                                                "dashboard_functionality_id" => $this -> dashboard_functionality_id ))){
			return false;
		}else{
			return true;
		}
	}
    

	public function save()
	{

	}
	
}