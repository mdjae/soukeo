<?php

class BusinessComment extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $id_comment;
	protected $content;
	protected $user_id;
	protected $parent_id_comment;
	protected $commented_object_id;
	protected $commented_object_table;
	protected $date_created;
	protected $date_updated;
	protected $author;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();

		if (!empty($data)) {
			$this->hydrate($data);
		}
		$this -> setAuthor();
	}
	
	
	public function setId_comment($id_comment)
	{
		$this -> id_comment = $id_comment;
	}
	
	public function setContent($content)
	{
		$this -> content = $content;
	}

	public function setUser_id($user_id)
	{
		$this -> user_id = $user_id;
	}
		
	public function setParent_id_comment($parent_id_comment)
	{
		$this -> parent_id_comment = $parent_id_comment;
	}
	
	public function setCommented_object_id($commented_object_id)
	{
		$this -> commented_object_id = $commented_object_id ;
	}

	public function setCommented_object_table($commented_object_table)
	{
		$this -> commented_object_table = $commented_object_table ;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
		
	public function getId_comment()
	{
		return $this -> id_comment ;
	}
	
	public function getContent(){
		return $this -> content;
	}
	
	public function getUser_id(){
		return $this -> user_id;
	}
	
	public function getParent_id_comment()
	{
		return $this -> parent_id_comment;
	}

	public function getCommented_object_id()
	{
		return $this -> commented_object_id ;
	}

	public function getCommented_object_table()
	{
		return $this -> commented_object_table ;
	}

	public function getDate_created()
	{
		return $this -> date_created ;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated ;
	}
	
	public function setAuthor()
	{
		$this -> author = self::$db -> getUserCode($this -> getUser_id());
	}
	
	public function getAuthor()
	{
		return $this -> author;
	}
				
	public function isNew()
	{
		if ($this -> getId_comment()){
			return false;
		}else{
			return true;
		}
	}
	
	public function save()
	{
		
	}

}