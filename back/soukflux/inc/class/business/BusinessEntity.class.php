<?php 

abstract class BusinessEntity 
{

	/**
	 * Méthode assignant les valeurs spécifiées aux attribut de l'objet
	 */
	public  function hydrate($donnees) {

		foreach ($donnees as $key => $value) {
			$methode = 'set'.ucfirst($key);

			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}
	
	abstract function save();
	
	
}
 ?>