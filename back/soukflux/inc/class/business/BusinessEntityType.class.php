<?php

class BusinessEntityType extends BusinessEntity {

	protected $type_entity_id;
	protected $table_name;
    protected $label;

	public function __construct ($data = array()) {
	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setType_entity_id($type_entity_id )
	{
		$this -> type_entity_id = $type_entity_id ;
	}

	public function setTable_name($table_name)
	{
		$this -> table_name = $table_name;
	}
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
	
    public function getType_entity_id()
    {
        return $this -> type_entity_id;
    }
    
    public function getTable_name()
    {
        return $this -> table_name;
    }
    
    public function getLabel()
    {
        return $this -> label; 
    }
    
	public function isNew()
	{
		if ($this -> getType_entity_id()){
			return false;
		}else{
			return true;
		}
	}
       
	public function save()
	{

	}
	
}