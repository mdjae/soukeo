<?php

class BusinessOrderGrossiste extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $order_id_grossiste;
	protected $order_status;
	protected $order_external_code;  
	protected $grossiste_id;
	protected $estimated_price;
	protected $estimated_weight;
	protected $date_created;
	protected $date_updated;
	protected $litiges;

	

	public function __construct ($order_id_grossiste = false) {
		
		self::$db = new BusinessSoukeoModel();	
		if($order_id_grossiste){
			$this->order_id_grossiste = $order_id_grossiste;
			$data = self::$db -> getOrderGrossisteByNumOrder($this->order_id_grossiste);
			$data = $data[0];
			if (!$data){
				return false;
			}else{
				$this -> hydrate($data);
			} 	
		}	
	}
	
	
	public function setOrder_id_grossiste($order_id_grossiste){
		$this -> order_id_grossiste = $order_id_grossiste;
	}

	public function setOrder_status($order_status)
	{
		$this -> order_status = $order_status;
	}
	
	public function setOrder_external_code($order_external_code)
	{
		$this -> order_external_code = trim($order_external_code);
	}
	
	public function setGrossiste_id($grossiste_id)
	{
		$this -> grossiste_id = $grossiste_id;
	}
	
	public function setEstimated_price($estimated_price)
	{
		$this -> estimated_price = $estimated_price;
	}
	
	public function setEstimated_weight($estimated_weight)
	{
		$this -> estimated_weight = $estimated_weight;
	}
		
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
	
	public function setLitiges($litiges)
	{
		if($litiges == "1" | $litiges == "0"){
			$this -> litiges = $litiges;	
		}
	}
	
	public function getOrder_id_grossiste()
	{
		return $this -> order_id_grossiste;
	}
	
	public function getOrder_status()
	{
		return $this -> order_status;
	}
	
	public function getOrder_external_code()
	{
		return $this -> order_external_code;
	}
	
	public function getGrossiste_id()
	{
		return $this -> grossiste_id;
	}
	
	public function getEstimated_price()
	{
		return $this -> estimated_price;
	}
	
	public function getEstimated_weight()
	{
		return $this -> estimated_weight;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function getLitiges()
	{
		return $this -> litiges;
	}
	
	public function save()
	{
		self::$db -> updateOrderGrossiste($this);
	}
	
	public function delete()
	{
		self::$db -> deleteOrderGrossiste($this);	
	}
	
	public function getAllItems()
	{
		return self::$db -> getAllItemsByOrderGrossiste($this -> getOrder_id_grossiste());
	}
}