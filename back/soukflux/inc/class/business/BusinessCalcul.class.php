<?php 
/**
 * 
 */
class BusinessCalcul {
	
	protected $param_aerienChrono ;
	protected $param_percentOM;
	protected $param_percentTVA;
	protected $param_percentMarge;
	
	protected static $db;
	
	
	function __construct() {
		$this -> param_aerienChrono = (float)SystemParams::getParam('grossiste*aerienchrono');
		$this -> param_percentOM    = (float)SystemParams::getParam('grossiste*percentOM');
		$this -> param_percentTVA   = (float)SystemParams::getParam('grossiste*percentTVA');
		$this -> param_percentMarge = (float)SystemParams::getParam('grossiste*percentMarge');
		
		self::$db = new BusinessSoukeoGrossisteModel();
	}
	
	/**
	 * Calcul le prix des produits grossistes
	 * @param $product 	
	 * @param $prodOld 	
	 */
	public function calculPrixGrossiste($product, $prodOld = null , $grossiste_id)
	{
		
		/*if(isset($prodGrOld) && $prodGrOld !== null){
			if($calcul_values = self::$db -> getPrixCalcul($prodGrOld['ID_PRODUCT'])){
				$calcul_values = $calcul_values[0];
				///////////////////////////////////////////////////////////////////////////////////////////////
				//RECUP ANCIENS PARAMETRES/////////////////////////////////////////////////////////////////////
				///////////////////////////////////////////////////////////////////////////////////////////////
				
				$aerien = $this -> param_aerienChrono * $calcul_values['POIDS'];
				

				//On retrouve alors le taux TVA param entré
				if(!is_null($calcul_values['TVA_PROD'])){
					$paramTauxTVA = $calcul_values['TVA_PROD'];
				}
				 
				//Retrouver le 20 % ou les changements
				if(!is_null($calcul_values['MARGE_PROD'])){
					$percentMarge = $calcul_values['MARGE_PROD'];
				}
				
	
				if(!is_null($calcul_values['OM_PROD'])){
					$percentOctroi = $calcul_values['OM_PROD'];
				}
			}
		}*/

			

		///////////////////////////////////////////////////////////////////////////////////////////////
		//RECALCUL AVEC LE NOUVEAU PRIX ///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////
		$prixAchatHT = (float)$product['PRICE_PRODUCT'];
		
		//pas de hauteur largeur longueur
		//$product['VOLUMETRIC_WEIGHT'] ? $poidsVolum = $product['VOLUMETRIC_WEIGHT'] : $poidsVolum = (float)$product['WEIGHT'];
		$poidsVolum = (float)$product['WEIGHT'];
		
		$prixKilo = (float)$prixAchatHT / (float)$poidsVolum;
		
		//FRAIS livraison 10% si prx/kg > 50 sinon 0
		if((float)$prixKilo > 50){
			$frais_livr = 0.10 * $prixAchatHT;
		}else{
			$frais_livr = 0;
		}
				
			
		//On va chercher le produit Avahis correspondant pour Octroi de mer catégorie
		if($prod_av = self::$db -> getProdAvAssocGr($prodGrOld['GROSSISTE_ID'], $prodGrOld['ID_PRODUCT'])){
		
			//On vérifie si sa catégorie a un octroi de mer
			$infoCat = self::$db -> getCatbyId($prod_av['CATEGORY']);
			
			//Param sur le produit prioritaire au param catégorie
			if(!is_null($infoCat['OM'])){
				$percentOctroi != null ? $percentOctroi = $percentOctroi : $percentOctroi = $infoCat['OM'];
			}else{
				$percentOctroi != null ? $percentOctroi = $percentOctroi : $percentOctroi = $this -> param_percentOM;
			}
			

			
			if(!is_null($infoCat['MARGE']) ){
				$percentMarge != null ? $percentMarge = $percentMarge : $percentMarge = $infoCat['MARGE'];
	
			}else{
				$percentMarge != null ? $percentMarge = $percentMarge : $percentMarge = $this -> param_percentMarge;
			}
			
			if(!is_null($infoCat['TVA'])  ){
				$paramTauxTVA != null ? $paramTauxTVA = $paramTauxTVA : $paramTauxTVA = $infoCat['TVA'];
				
			}else{
				$paramTauxTVA != null ? $paramTauxTVA = $paramTauxTVA : $paramTauxTVA = $this -> param_percentTVA;
			}			
			
		}
		
		$aerien = $this -> param_aerienChrono * (float)$poidsVolum;
		
		//Ici taux ocroit est par défaut à 25% si pas de valeur avant sinon on garde la même
		$percentOctroi != null ? $percentOctroi = $percentOctroi : $percentOctroi = $this -> param_percentOM;
		if($percentOctroi == null) $percentOctroi = $this -> param_percentOM;
		if($percentMarge  == null) $percentMarge  = $this -> param_percentMarge;
		if($paramTauxTVA  == null) $paramTauxTVA  = $this -> param_percentTVA;
		
		$octroiMer = $percentOctroi * ($prixAchatHT + $aerien);
		
		$prixRevient = $prixAchatHT 
					 + $aerien
					 + $frais_livr
					 + $octroiMer;
		
		$percentMarge != null ? $percentMarge = $percentMarge : $percentMarge = $this -> param_percentMarge;			 
		$margeNette = $prixRevient * $percentMarge ; // 20% marge nette à paramétrer, ici valeur par défaut ou ancienne val
		
		
		$paramTauxTVA != null ? $paramTauxTVA = $paramTauxTVA : $paramTauxTVA = $this -> param_percentTVA;
		$TVA = ( $prixRevient + $margeNette ) * $paramTauxTVA; //TVA 8,50 % à paramétrer plus tard, ici valeur par défaut ou ancienne val
		
		if($grossiste_id === "1"){
			$prixVenteLivre = $prixRevient + $margeNette + $TVA + 8;
		}
		else{
			$prixVenteLivre = $prixRevient + $margeNette + $TVA ;
		}
			
		$coeff = $prixVenteLivre / $prixAchatHT ;  
		
		$product['PRIX_ACHAT'] 		= $prixAchatHT;
		$product['POIDS'] 			= $poidsVolum;
		$product['PRIX_KILO'] 		= $prixKilo;
		$product['PRIX_METRO'] 		= $prixAchatHT + ($prixAchatHT * 0.196);
		$product['FRAIS_LIVR'] 		= $frais_livr;
		$product['AERIEN']			= $aerien;
		$product['OM'] 				= $octroiMer;
		$product['PRIX_LIVRE'] 		= $prixRevient;
		$product['PERCENT_MARGE'] 	= $percentMarge;
		$product['PERCENT_TVA'] 	= $paramTauxTVA;
		

		$product['PRIX_FINAL'] 		= $prixVenteLivre ;	
		
		
		$product['COEF'] 			= $coeff ;
		$product['RATIO_RUN_FR'] 	= $prixVenteLivre / $product['PRIX_METRO'];
		$product['PRIX_LIVRAISON'] 	= $aerien + $frais_livr;
		$product['RATIO_TRANSPORT'] = $product['PRIX_LIVRAISON'] / $prixAchatHT;

		//var_dump($product);
		return $product;		
	}
}

 ?>