<?php 

/**
 * Classe de gestion sspécifique aux import de LDLC
 * Permet de formatter correctement les champs de données en fonctions des champs fournis par LDLC
 * Permet également de nettoyer les descriptions et de gérer les erreurs en cas d'EAN non présent 
 * ou Stock vide spécifique à LDLC
 */
class BusinessGrossisteLDLC  
{

	protected $delimiter ; 			//Délimiteur CSV pour LDLC
	protected $errors = array(); 	//Arry d'erreurs et messages d'erreurs
	
	
	/**
	 * Fonction __construct basique profitant de définir le délimiteur
	 * pour le CSV de LDLC
	 */
	public function __construct ()
	{
		$this -> delimiter = "|";
		
	}
	
	
	/**
	 * Gestion des erreurs de stock et d'EAN tout en fabriquant un tableau contenant
	 * les différentes erreurs sur les produits avec leur id_produit ainsi que la cause des erreurs.
	 * @param 	array 	$product 	l'array du produit à analyser
	 * @return 	boolean 			Retourne true si une erreur existe ou bien false
	 */
	public function errorsExist($product)
	{
		if(  $product['stock'] == "" | $product['ean'] =="" ){
							
			$message = "";
			

			if($product['ean']==""){
				$message .= " EAN INVALIDE ";
			}
			if($product['stock'] =="" ){
				$message .= " STOCK INVALIDE " ;
			}
			$this -> errors [] = $message." POUR produit EAN : ".$product['ean']. " RefGrossiste : ".$product['id']."";
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Permet de nettoyer la description ou autres champs du produit LDLC
	 * @param 	array  	$product 		contenant les infos du produit
	 * @return 	array  	$product 		contenant les infos nettoyées du produit
	 */
	public function cleanDescription($product)
	{
		$product['ref'] = $product['id'];
		return $product;
	}


	/**
	 * Formatte les champ LDLC afin de recréer un array avec des clés de tableau générique
	 * permettant d'avoir des fonctions uniques d'insertions une fois du coté du modèle.
	 * Il y a aussi la gestion du stock par défaut du grossiste et la création d'URL d'image permettant
	 * d'obtenir différentes qualités d'image sur le site LDLC
	 * @param 	array 	$product 		contenant le produit parsé du CSV
	 * @param 	array 	$productOld 	contenant le produit grossiste s'il existe déja avant le parsing
	 * @return 	array 	$product 		contenant le produit traité
	 */
	public function formatFields($product, $prodGrOld = ""){
		
		if($prodGrOld != null){
			$product['weight'] = $prodGrOld['WEIGHT'];
			$product['long_desc'] = $prodGrOld['DESCRIPTION'];
			$product['short_desc'] = $prodGrOld['DESCRIPTION_SHORT'];
		}
		else{
			$product['weight'] = 0;
		}
		
		$product['manufacturer'] = ucfirst(strtolower($product['constructeur']));
		$product['category'] = $product['catgorie'];
		$product['price'] = $product['prixHT'];
		$product['ref'] = $product['id'];
		
		if($product['stock'] == "1"){
			$product['stock'] = SystemParams::getParam('grossiste*stockdefaultgrossiste');
		}else{
			$product['stock'] = 0;
		}
		
		$tmp = explode("_", $product['image']);
		$product['picture1'] = $tmp[0] . "_2.jpg";
		$product['picture2'] = $tmp[0] . "_1.jpg";
		$product['name'] = $product['designation'];
		
		return $product;
	}
	
	
	/**
	 * Permet de récupérer le tableau contenant les erreurs
	 * rencontrés lors du parsing du CSV notament grace à la fonction errorsExists
	 * @see errorsExist
	 * @return array 	errors  contenant les erreurs et messages d'erreurs
	 */
	public function getErrors() 
	{
		return $this -> errors;
	}
	
	
	/**
	 * Permet de récupérer le délimiteur associé au CSV actuel
	 * @return string delimiter représente le délimiteur utilisé dans le CSV
	 */
	public function getDelimiter()
	{
		return $this -> delimiter;
	}

}

?>