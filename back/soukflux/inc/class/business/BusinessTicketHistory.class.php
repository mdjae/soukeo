<?php

class BusinessTicketHistory extends BusinessEntity {
	
	protected static 	$db ;
	
    protected $history_id;
	protected $ticket_id;
    protected $commentaire;
	protected $priority_id_change;
    protected $state_id_change;
	protected $ticket_assign_change;
    protected $priority_id_old;
    protected $state_id_old;
    protected $ticket_assign_old;
    protected $creator_client_entity_id;
    protected $ticket_history_visibility;
    protected $user_name; 
    protected $creator_user_id;
    protected $date_created;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
    public function setHistory_id($history_id)
    {
        $this -> history_id = $history_id;
    }
	
	public function setTicket_id($ticket_id){
		$this -> ticket_id = $ticket_id ;
	}

    public function setCommentaire($commentaire){
        $this -> commentaire = $commentaire ;
    }
    
	public function setPriority_id_change($priority_id_change)
	{
		$this -> priority_id_change = $priority_id_change;
	}

    public function setPriority_id_old($priority_id_old)
    {
        $this -> priority_id_old = $priority_id_old;
    }
    
    public function setState_id_change($state_id_change)
    {
        $this -> state_id_change = $state_id_change;
    }	
    
	public function setTicket_assign_change($ticket_assign_change)
	{
		$this -> ticket_assign_change = $ticket_assign_change;
	}

    public function setState_id_old($state_id_old)
    {
        $this -> state_id_old = $state_id_old;
    }   
    
    public function setTicket_assign_old($ticket_assign_old)
    {
        $this -> ticket_assign_old = $ticket_assign_old;
    }

    public function setCreator_client_entity_id($creator_client_entity_id)
    {
        $this -> creator_client_entity_id = $creator_client_entity_id;
    }    
    
    public function setTicket_history_visibility($ticket_history_visibility)
    {
        $this -> ticket_history_visibility = $ticket_history_visibility;
    }
    
    public function setUser_name($user_name)
    {
        $this -> user_name = $user_name;
    }
    
    public function setCreator_user_id($creator_user_id)
    {
        $this -> creator_user_id = $creator_user_id;
    }
    
    public function setDate_created($date_created){
        $this -> date_created = $date_created;
    }        
    
    public function getHistory_id()
    {
        return $this -> history_id;    
    }
    
    public function getTicket_id(){
        return $this -> ticket_id;
    }

    public function getCommentaire(){
        return $this -> commentaire;
    }
    
    public function getPriority_id_change()
    {
        return $this -> priority_id_change;
    }

    public function getPriority_id_change_label()
    {
        $manager = new ManagerTicketPriority();
        return $manager->getById(array("priority_id" => $this -> priority_id_change))->getLabel();
    }

    public function getState_id_change()
    {
        return $this -> state_id_change;
    }   
    
    public function getState_id_change_label()
    {
        $manager = new ManagerTicketState();
        return $manager->getById(array("state_id" => $this -> state_id_change))->getLabel();
    }   
            
    public function getTicket_assign_change()
    {
        return $this -> ticket_assign_change;
    } 

    public function getTicket_assign_change_user()
    {
        $manager = new ManagerAppUser();
        
        return $manager -> getById(array("user_id" => $this -> ticket_assign_change ));
    } 
        
    public function getPriority_id_old()
    {
        return $this -> priority_id_old;
    }

    public function getState_id_old()
    {
        return $this -> state_id_old;
    }   
    
        
    public function getTicket_assign_old()
    {
        return $this -> ticket_assign_old;
    }     


    public function getCreator_client_entity_id()
    {
        return $this -> creator_client_entity_id;
    }    
    
    
    public function getTicket_history_visibility()
    {
        return $this -> ticket_history_visibility;
    }
    
    
    public function getUser_name()
    {
        return $this -> user_name;
    }
    
    public function getPriority_id_old_label()
    {
        $manager = new ManagerTicketPriority();
        return $manager->getById(array("priority_id" => $this -> priority_id_old))->getLabel();
    }

    public function getTicket_assign_old_user()
    {
        $manager = new ManagerAppUser();
        
        return $manager -> getById(array("user_id" => $this -> ticket_assign_old ));
    } 
    
    public function getState_id_old_label()
    {
        $manager = new ManagerTicketState();
        return $manager->getById(array("state_id" => $this -> state_id_old))->getLabel();
    }   
    
  
    public function getCreator_user_id()
    {
        return $this -> creator_user_id;
    }
     
    public function getDate_created()
    {
        return $this -> date_created;
    }
    
    public function getChanges_html()
    {
        $html = "";
        if($this -> getState_id_change() || $this -> getPriority_id_change()|| $this -> getTicket_assign_change()){ 
            $html .= "<ul>";       
            
            if($this -> getState_id_change()){
                $html .= "<li>Statut changé de <strong>".$this -> getState_id_old_label()."</strong> à <strong>".$this -> getState_id_change_label()."</strong></li>";
            }
            if($this -> getPriority_id_change()){
                $html .= "<li>Priorité changée de <strong>".$this -> getPriority_id_old_label()."</strong> à <strong>".$this -> getPriority_id_change_label()."</strong></li>";   
            }
            if($this -> getTicket_assign_change()){
                $html .= "<li> Assigné ";
                if($this -> getTicket_assign_old()) 
                    $html .=" de <strong>".$this -> getTicket_assign_old_user() -> getName()."</strong>";
                
                $html .= " à <strong>".$this -> getTicket_assign_change_user() -> getName(). "</strong></li>";
            } 
            $html .= "</ul>";      
        }
        
        if($this -> getCommentaire())
            $html .= $this -> getCommentaire(); 
        
        return $html ;
    }
      	
	public function isNew()
	{
		if ($this -> history_id){
			return false;
		}else{
			return true;
		}
	}
    

    
	public function save()
	{

	}
	
}