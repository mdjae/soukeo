<?php

class BusinessAnnonceReception extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $annonce_reception_id;
	protected $annonce_reception_code;
	protected $commentaires; 
	protected $status;
	protected $date_created;
	protected $date_updated;
	protected $date_reception;

	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		
		if (!empty($data)) {
			$this->hydrate($data);
		}	
	}
	
	
	public function setAnnonce_reception_id($annonce_reception_id){
		$this -> annonce_reception_id = $annonce_reception_id ;
	}

	public function setAnnonce_reception_code($annonce_reception_code)
	{
		$this -> annonce_reception_code = $annonce_reception_code;
	}
	
	public function setCommentaires($commentaires)
	{
		$this -> commentaires = $commentaires;
	}
	
	public function setStatus($status)
	{
		$this -> status = strtolower($status);
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}

	public function setDate_reception($date_reception)
	{
		$this -> date_reception = $date_reception;
	}
	
	public function getAnnonce_reception_id()
	{
		return $this -> annonce_reception_id;
	}
	
	public function getAnnonce_reception_code()
	{
		return $this -> annonce_reception_code;
	}
	
	public function getCommentaires()
	{
		return $this -> commentaires;
	}
	
	public function getStatus()
	{
		return $this -> status;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}

	public function getDate_reception()
	{
		return $this -> date_reception;
	}
			
	public function isNew()
	{
		if ($this -> getAnnonce_reception_id()){
			return false;
		}else{
			return true;
		}
	}
				
	public function save()
	{
		
	}

	
	public function getAllItems()
	{
		return self::$db -> getAllItemsByAnnonceReception($this -> getAnnonce_reception_id());
	}
}