<?php 

class BusinessExportOptiLog {
	
	protected $path 		= "/opt/soukeo/export";
	protected $fileName 	= "";
	
	protected $sheet1 ;
	protected $sheet2 ;
	
	protected $refGrossiste = array();
	
	function __construct() {
		//$this -> path = $path;
	}
	
	/**
	 * Abstract method à rédéfinir pour créer contenu du CSV 
	 */
	public function makeContent($itemList, $order_grosssiste){

		$objPHPExcel = new PHPExcel();
		$objPHPExcel -> getProperties()->setCreator("Soukeo")
		                ->setLastModifiedBy("Philippe")
		                ->setTitle("ExportOptilog")
		                ->setSubject("ExportOpti")
		                ->setDescription("Description a definir")
		                ->setKeywords("office 2007 openxml php")
		                ->setCategory("Test result file");
								
		//1ère feuille
		$this -> sheet1 = $objPHPExcel->getActiveSheet();
		$this -> sheet1 -> setTitle('Optilog');
				
		//2eme feuille 
		$this -> sheet2 = $objPHPExcel->createSheet();
		$this -> sheet2 -> setTitle('Optilog 2');

		$this -> setAutoReSize();	
		$this -> makeHeader();							 
		$this -> setDefaultStyle();
		
		
		//TRAITEMENT CONTENT
		$column = "k";
		$row 	= 5; 	 // Sheet 1
		$row2 	= 2; 	 // Sheet 2
		$odd    = false; // Alternance couleurs pair impair
		$start  = true;  
		$objDateTime = new DateTime('NOW');
		
		foreach ($itemList as $item) {
			
			$order   = new BusinessOrder($item->getOrder_id());
			$address = new BusinessAddress($order -> getShipping_address_id());
			$prod_gr = $item -> getProductGrossiste();
			
			//GROUPE TOUTE COMMANDE SUR UNE SEULE LIGNE
			if($start){
				$start = false;
				$currentOrder = $order->getIncrement_id();
			}
			if($currentOrder != $order->getIncrement_id()){
				$currentOrder = $order->getIncrement_id();
				$row++;	//UNIQUEMENT SI COMMANDE SUIVANTE
			}	
			
			//GESTION COLONNE PRODUIT FEUILLET 1
			if(array_key_exists($prod_gr->getRef_grossiste(), $this -> refGrossiste)){
				
				//Si une colone a déja été crée pour ce produit on récupère le numéro de colonne associé
				$itemColumn = $this -> refGrossiste[$prod_gr->getRef_grossiste()]['col'];
				$this -> refGrossiste [$prod_gr->getRef_grossiste()]['qty'] += $item->getQty_ordered();
				
				$this->sheet1
				->setCellValue($itemColumn.'2', $item->getName())
				->setCellValue($itemColumn.'3', $prod_gr->getRef_grossiste())
				->setCellValue($itemColumn.'4', "QTE")
				->setCellValue($itemColumn.$row, $item->getQty_ordered());
					
			}else{
				$this -> refGrossiste [$prod_gr->getRef_grossiste()]['col'] = $column;
				$this -> refGrossiste [$prod_gr->getRef_grossiste()]['qty'] += $item->getQty_ordered();
				
				if($odd){
					$this->applyStyleColonnePair($column);
					$odd=false;
				}else{
					$this->applyStyleColonneImpair($column);
					$odd=true;
				}

				$this->sheet1
				->setCellValue($column.'2', $item->getName())
				->setCellValue($column.'3', $prod_gr->getRef_grossiste())
				->setCellValue($column.'4', "QTE")
				->setCellValue($column.$row, $item->getQty_ordered());
				
				$this->sheet1->getStyle($column.'2')->applyFromArray($this -> getStyleNameProduct());
				$this->sheet1->getStyle($column.'3')->applyFromArray($this -> getStyleNameProduct());	
				$this->sheet1->getStyle($column.'4')->applyFromArray($this -> getStyleNameProduct());		
				
				$column++;
			}

			//GESTION LIGNE CLIENT FEUILLET 1
			$this->sheet1
				->setCellValue('A'.$row, $order->getIncrement_id())
				->setCellValue('B'.$row, "SKO")
				->setCellValue('C'.$row, $order->getRecipient_name())
				->setCellValue('D'.$row, $address-> getStreet1())
				->setCellValue('E'.$row, $address-> getStreet2())
				->setCellValue('F'.$row, $address-> getPostcode())
				->setCellValue('G'.$row, $address-> getCity())
				->setCellValue('H'.$row, $order -> getRecipient_phone())
				->setCellValue('I'.$row, $order -> getRecipient_email());
				
			$this->sheet1->getStyle('A'.$row)->applyFromArray($this->getIncrementIdStyle());				
			
			//GESTION LIGNE PRODUIT FEUILLET 2
			$this->sheet2
				->setCellValue('A'.$row2, $prod_gr->getRef_grossiste())
				->setCellValue('B'.$row2, $item->getName())
				->setCellValue('C'.$row2, $prod_gr->getGrossiste_nom())
				->setCellValue('D'.$row2, $item->getCateg())
				->setCellValue('E'.$row2, "SOUKEO")
				->setCellValue('F'.$row2, $prod_gr->getWeight())
				->setCellValue('G'.$row2, $item->getQty_ordered())
				->setCellValue('H'.$row2, $order_grosssiste->getOrder_external_code())
				->setCellValue('I'.$row2, $order->getIncrement_id())
				->setCellValue('j'.$row2, $objDateTime->format('d/m/Y'));
			$row2 ++ ;
		}
		
		$this -> fileName = 'opti_'.$order_grosssiste -> getOrder_external_code().'_'.date("d-m-Y").".xlsx";
			
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		
		$this -> phpOutputStreaming($objWriter);

		$objWriter->save('/opt/soukeo/export/'.$this -> fileName);

	} 
	
	
	public function setAutoReSize()
	{
		PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
		foreach(range('A','I') as $columnID) {
    		$this -> sheet1 -> getColumnDimension($columnID)->setAutoSize(true);
		}
		foreach(range('A','J') as $columnID) {
    		$this -> sheet2 -> getColumnDimension($columnID)->setAutoSize(true);
		}
	}
	
	
	
	public function setDefaultStyle()
	{
		$this->sheet1->getDefaultStyle()
			->applyFromArray(array(
				'font'=>array(
				    'name'      =>  'Arial',
				    'size'      =>  12,
				    'bold'      => false),
				'borders' => array(
				    'allborders'=>array(
				    	'style' => PHPExcel_Style_Border::BORDER_THIN ,
						'color' => array(
    									'rgb' => '808080'
    						 		)
    				)
				)
			));
				
		$this->sheet1->getRowDimension('2')->setRowHeight(80);
		
		//BLUE FIRST COLUMN
		$this->sheet1->getStyle('A')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID); 
		$this->sheet1->getStyle('A')->getFill()->getStartColor()->setRGB('5067DA');
		   
		 //SOUS HEADER GRAY
		$this->sheet1->getStyle('B4:J4')->applyFromArray(array('fill'=>array(
                'type'=>PHPExcel_Style_Fill::FILL_SOLID,
                'color'=>array(
                    'argb'=>'A19999'))));
					
 		//HEADER GREEN                   
        $this->sheet1->getStyle('B3:J3')->applyFromArray(array(
            'fill'=>array(
                'type'=>PHPExcel_Style_Fill::FILL_SOLID,
                'color'=>array(
                    'argb'=>'0AD12C')),
			'alignment'=>array(
                            'horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));

		//FEUILLET 2 HEADER		
		$this->sheet2 ->getStyle('A1:J1')->applyFromArray(array(
            'fill'=>array(
                'type'=>PHPExcel_Style_Fill::FILL_SOLID,
                'color'=>array(
                    'argb'=>'0AD12C')),
			'borders' => array(
					    		'allborders'=>array(
					    			'style' => PHPExcel_Style_Border::BORDER_THIN ,
    								'color' => array(
    									'rgb' => '808080'
    						 		)
								)
							)
			));
	}


	public function getStyleNameProduct()
	{
		return 	$styleNameProd = array(
				    'font'  => array(
				        'bold'  => true,'size'  => 10,
				        'name'  => 'Arial'
				    ),
					'alignment'=> array(
                            'horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
							'vertical'=>PHPExcel_Style_Alignment::VERTICAL_JUSTIFY),
					'borders' => array(
					    'allborders'=>array(
					    	'style' => PHPExcel_Style_Border::BORDER_THIN ,
							'color' => array(
    									'rgb' => '000000'
    						 		)))
						);
	}
	
	public function makeHeader()
	{
							
		$this->sheet1
			
            ->setCellValue('A3', 'Num commande client')
			->mergeCells('B3:J3')
            ->setCellValue('B3', 'Tableau répartition Avahis')
			->setCellValue('B4', 'CODE')
			->setCellValue('C4', 'Nom Prénom')
			->setCellValue('D4', 'Adresse 1')
			->setCellValue('E4', 'Adresse 2')
			->setCellValue('F4', 'Code Postal')
			->setCellValue('G4', 'Ville')
			->setCellValue('H4', 'Téléphone')
			->setCellValue('I4', 'Mail');

		$this->sheet2		
            ->setCellValue('A1', 'ART_ID')
            ->setCellValue('B1', 'ART_LIBELLE')
			->setCellValue('C1', 'ART_STOCK')
			->setCellValue('D1', 'ART_GAMME')
			->setCellValue('E1', 'ART_ID_UC')
			->setCellValue('F1', 'ART_POIDS')
			->setCellValue('G1', 'QUANTITE')
			->setCellValue('H1', 'Numéro de commande Grossiste')
			->setCellValue('I1', 'Numero de commande Avahis')
			->setCellValue('J1', 'paniérisé le');
	}
	
	public function applyStyleColonnePair($column)
	{
		$this->sheet1 ->getStyle($column)->applyFromArray(array(
		            'fill'=>array(
		                'type'=>PHPExcel_Style_Fill::FILL_SOLID,
		                'color'=>array(
		                    'argb'=>'D87474'))));
	}
	
	public function applyStyleColonneImpair($column)
	{
		$this->sheet1 ->getStyle($column)->applyFromArray(array(
		            'fill'=>array(
		                'type'=>PHPExcel_Style_Fill::FILL_SOLID,
		                'color'=>array(
		                    'argb'=>'76BF36'))));
	}
	
	public function getIncrementIdStyle()
	{
		return 	$styleArray = array(
		    'font'  => array(
		        'bold'  => true,
		        'color' => array('rgb' => 'FF850A'),
		        'size'  => 15,
		        'name'  => 'Arial'
		    ),
			'alignment'=> array(
                            'horizontal'=>PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
			'borders' => array(
					    'allborders'=>array(
					    	'style' => PHPExcel_Style_Border::BORDER_THIN ,
							'color' => array(
    									'rgb' => '000000'
    						 		))));
	}

	public function phpOutputStreaming($objWriter)
	{
		// Redirect output to a client’s web browser (Excel2007)
		ob_clean();	
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$this -> fileName.'"');
		header('Cache-Control: max-age=0');

		$objWriter->save('php://output');
	}
	
	public function getPath()
	{
		return $this -> path ;
	}
	
	public function getFileName()
	{
		return $this -> fileName;
	}
	
} ?>