<?php 

/**
 * Script pour insérer les produits d'un e-commerçant CSV
 * vers la base de donnée intermédiaire Soukflux. Il y a tout d'abord
 * un parse du CSV obtenu depuis l'URL de l'ecommercant, permettant d'obtenir
 * les produits et leurs attributs sous forme de tableaux, puis il y a toutes
 * les insertions de ces produits, voir aussi la classe
 * BusinessSoukeCSVModel
 * 
 * @version 0.1.0
 * @author 	Philippe_LA
 * 
 */
class BusinessImportCSV {
	
	protected static $nbRun 		= 0; 		//Compte le nombre de fois que ce batch a été executé
	
	protected $csv_to_parse 		= "";		//URL de l'ecommerçant à parser
	protected $vendorID 			= ""; 		//VENDOR_ID de l'ecommercant
	protected $productListAttribute = null; 	//Liste produits avec ses attributs finale array()
	protected $attributeList 		= null; 	//Liste des différents attributs (entete) array()
	
	protected $nbInsert 			= 0; 		//log
	protected $nbInsertAttr 		= 0;		//log
	protected $nbUpdate 			= 0;		//log  A IMPLEMENTER
	protected $nbUpdateAttr 		= 0;		//log  A IMPLEMENTER
	protected $nbErrors 			= 0;
	protected $delimiter 			= ";";
	protected $nameCateg			=""; 		// nom générique pour la catégorie qui prendra le nom du fichier CSV
	protected $rep = "/opt/soukeo/import/commercants/";

	protected $path = "/var/www/media.avahis/media/catalog/product/";
	protected $local_url = "http://media.avahis.com/media/catalog/product/";
		
	private static $db;
	
	/**
	 * Constructeur, cette classe prend en paramètre le CSV et l'ID du vendeur
	 * @param 	string 	$csv 		Le nom du fichier
	 * @param 	string 	$vendorID 	L'id du vendeur dur la marketPlace
	 */
	function __construct($csv, $vendorID) 
	{
		$this->nameCateg 		= ucfirst($csv) ;	
		$this->csv_to_parse 	= $this->rep . $csv . ".csv";
		$this->vendorID 		= $vendorID;
	}
	
	
	/**
	 * Fonction qui parse le CSV ,produit des tableaux associatifs 
	 * et appelle le fonctions d'insertion en BDD
	 */
	public function run()
	{
	    
		
		self::$db 				= new BusinessSoukeoCSVModel ();
		
        self::$db -> prepareInsertSfkProdCSV($this->vendorID); // mutiCsv mm remarque pour la quantité
        self::$db -> prepareStmtProdEcommercant($this->vendorID); // mutiCsv mm remarque pour les produits mis en inactif
        self::$db -> prepareInsertSfkProdAttrCSV();
		
		//On parse le CSV de l'e-commercant dispo à l'URL fourni
		$this->parseCSV2($this->csv_to_parse, 15, ";");
		
		foreach ($this->productListAttribute as $product) {
			
            $product = $this -> formatFields($product);
            
            $state = self::$db->addRowSfkProdCSV($product, $this->vendorID) ;
            
            if( $state == 1){ //Si la requete et bien passée
                $this->nbInsert++;
            }elseif($state == 2 ){
                $this->nbUpdate++;
            }elseif($state == 0){
                $this->nbErrors++;
            }
            
            
            foreach ($this->attributeList as $attribute) {
                
                //Produit possède cet attribut?
                if ($product[$attribute] != "") {

                    //On vérifie si l'attribut existe déja 
                    $id_attribute = self::$db->checkIfAttributeExistsCSV($attribute);
                    //Relation produit_Attribut
                    if(self::$db->addRowSfkProdAttrCSV($product['ID_PRODUCT'], $this->vendorID, $id_attribute, $product[$attribute]) == true) {
                        $this->nbInsertAttr++;
                    }   
                }
            }
            
            
            // si aproduit ssocié mise a jour de la table produit stocke prix et Images
            if ($prodAv = self::$db->getProductAvAssoc($this->vendorID,$product['ID_PRODUCT'])){// a rajouté aprés pour verif la date &&self::$db->verifDate_upBddLocalImport($product)){   
                //self::$pictureManager -> savePictures($product, $prodAv);
                $test = self::$db->addRowSfkProdEcomSP($product, $this->vendorID);  
                    
            }
		}

		
		self::$nbRun++;	
		
		return true;
	}

    protected function parseCSV2($file, $nbChampsFixes, $delimiter, $bom = false)
    {
        
        if (($handle = fopen($file, "r")) !== FALSE) {
            $headerList = array();
            $productList = array();
            $row = 0;
            while (($data = fgetcsv($handle, 0, $this -> delimiter)) !== FALSE) {       
                $num = count($data);
                $product = array();
                //pour chaque colonnes
                for ($c = 0; $c < $num; $c++) {
                    
                    if($row == 0){
                        $headerList[] = trim(BusinessEncoding::toUTF8(html_entity_decode($data[$c])));
                        
                    }else{
                        $product[] = trim(BusinessEncoding::toUTF8(html_entity_decode($data[$c])));
                    }   
                }
                
                //Si on est pas sur l'entete
                if($row != 0) {
                    //Création du produit avec ses colonnes associatives
                    $productList [] = $product;
                }
                $row++;
            }
            
            //Création du tableau associatif
            foreach ($productList as $p) {
                $this->productListAttribute[] = array_combine($headerList, $p);
            }
            
            //Les attributs commencent dans l'entete après les premiers champs fixes
            $this->attributeList = array_slice($headerList, $nbChampsFixes);            
                
            return true;     
        }
        else{
            return false;
        }
    }

	
	public function cleanDescription($product)
	{
		return $product;
	}
	
	
	protected function formatFields($product){
		
		$product['ID_PRODUCT'] = $this->vendorID=="61" ? str_replace(' ', '_', $product['CODE'])."_61" : str_replace(' ', '_', $product['CODE']);
		$product['NAME_PRODUCT'] = $product['NOM'];
		$product['QUANTITY'] = $product['STOCK'];
		$product['PRICE_PRODUCT'] = $product['PRIX_PRODUIT']!=null? trim(str_replace(',', '.',$product['PRIX_PRODUIT'])) : trim(str_replace(',', '.',$product['PRIX_TTC'])) ;
		$product['PRICE_TTC'] = $product['PRIX_TTC'] !=null? trim(str_replace(',', '.',$product['PRIX_TTC'])) : trim(str_replace(',', '.',$product['PRIX_PRODUIT']))  ;
		$product['WEIGHT'] = trim(str_replace(',', '.',$product['POIDS']));
		$product['IMAGE_PRODUCT'] = $product['IMAGE_URL'];
		$product['IMAGE_PRODUCT_2'] = $product['IMAGE_URL_2'] !== null ? $product['IMAGE_URL_2'] : $product['IMAGE_URL'];
		$product['IMAGE_PRODUCT_3'] = $product['IMAGE_URL_3'] !== null ? $product['IMAGE_URL_3'] : $product['IMAGE_URL'];
		$product['DESCRIPTION'] = $product['DESCRIPTION_LONGUE'];
		$product['DESCRIPTION_SHORT'] = $product['DESCRIPTION_COURTE'] !== null ? $product['DESCRIPTION_COURTE'] : $product['FABRIQUANT'] . " - ". $product['NOM'] ;
		$product['META_DESCRIPTION'] = $product['META_DESCRIPTION'];
		$product['META_KEYWORDS'] = $product['META_KEYWORDS'];
		$product['MANUFACTURER'] = $product['FABRIQUANT'];
		$product['CATEGORY'] = $product['CATEGORY'] === null ? $this->nameCateg : $product['CATEGORY'] ;
		$product['REFERENCE_PRODUCT'] = $product['REFERENCE_PRODUIT'] === null ? str_replace(' ', '_', $product['CODE']) :str_replace(' ', '_', $product['REFERENCE_PRODUIT']);
		$product['EAN'] = $product['EAN'];
		 	
		return $product;
	}
	
	
	
	/**
	 * Pour le log, retourne le nombre d'insert effectués pour les
	 * produits
	 * @return nbInsert Int nb de Produits insert/update
	 */
	public function getNbInsert()
	{
		return $this->nbInsert;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'insert effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbInsertAttr()
	{
		return $this->nbInsertAttr;
	}
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * produits
	 * @return nbInsert Int nb de Produits insert/update
	 */
	public function getNbUpdate()
	{
		return $this->nbUpdate;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbUpdateAttr()
	{
		return $this->nbUpdateAttr;
	}
	
	/**
	 * @return url_to_parse String l'url de l'e-commerçant
	 */
	public function getCSV_to_parse()
	{
		return $this->csv_to_parse;	
	}
	
	
	/**
	 * @return vendorID Int l'id du vendeur
	 */
	public function getVendorID()
	{
		return $this->vendorID;
	}
	
	
	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return nbInsertAttr Int nb de relations Prod/attributs 
	 */
	public function getNbErrors()
	{
		return $this->nbErrors;
	}
		
	/**
	 * @return nbRun le nombre de fois que ce Script a été effectué
	 */
	public static function getNbRun()
	{
		return self::$nbRun;
	}
}
?>