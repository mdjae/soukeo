<?php

class BusinessFactureCreditLine extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $line_id;
	protected $facture_credits_id;
	protected $pack_id;
	protected $qty;
	protected $designation;
    protected $price_ht;
	protected $price_ttc; 
	protected $date_created;
	protected $date_updated;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	
	public function setLine_id($line_id ){
		$this -> line_id = $line_id ;
	}

	public function setFacture_credits_id($facture_credits_id)
	{
		$this -> facture_credits_id = $facture_credits_id;
	}
	
	public function setPack_id($pack_id)
	{
		$this -> pack_id = $pack_id;
	}
	
	public function setQty($qty)
	{
		$this -> qty = $qty;
	}

	public function setDesignation($designation)
	{
		$this -> designation = $designation;
	}
    
    public function setPrice_ht($price_ht)
    {
        $this -> price_ht = $price_ht;
    }
		
	public function setPrice_ttc($price_ttc)
	{
		$this -> price_ttc = $price_ttc;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}

	public function getLine_id(){
		return $this -> line_id;
	}

	public function getFacture_credits_id()
	{
		return $this -> facture_credits_id;
	}
	
	public function getPack_id()
	{
		return $this -> pack_id;
	}
	
	public function getQty()
	{
		return $this -> qty ;
	}
		
	public function getDesignation()
	{
		return $this -> designation;
	}
	
    public function getPrice_ht()
    {
        return $this -> price_ht;
    }
    
	public function getPrice_ttc()
	{
		return $this -> price_ttc;
	}

	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function isNew()
	{
		if (self::$db -> getLineFactuCredits($this -> getLine_id(), $this -> getFacture_credits_id())){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}