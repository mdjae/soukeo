<?php
/**
 * Objet View
 * Rassembles les fonctions de construction du formulaire
 * @author tpo
 *
 */
class BusinessView {
	protected $template;
	protected $path = "view";
	protected $tmp = array();
 
	public function __construct() {

	}

	public function assign($key, $value) {
		$this -> _[$key] = $value;
	}

	/**
	 * Affiche le code HTML généré par le fichier view demandé
	 * @param string $view : nom de la vue à afficher
	 */
	public function render($view) {
	    
		$this -> template = $view . ".view";
		$tpl = $this -> template;

		$file = $this -> path . DIRECTORY_SEPARATOR . $tpl . '.php';

		//echo $file;
		$exists = file_exists($file);
		if ($exists) {
			ob_start();
			include $file;
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		} else {
			return 'could not find template';
		}

	}
	
	/**
	 * Fonction permettant de générer un arbre de catégories sous forme de <ul> <li> à partir de donnée contenant
	 * toute les sous catégories d'une catégories (un niveau entier de catégorie). Chaque clic sur une catégorie
	 * lance un chargement ajax permettant de rappeller cette fonction pour les sous catégories.
	 * @param 	array 		$data 	tableau contenant les catégories label et id
	 * @param 	integer 	$level  le niveau d'imbrication de la catégorie
	 * @return 	string 		$out 	l'Output HTML
	 */
	public function showcat($data, $level = 0) {
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
		
		//La vue en mode grille ou liste doit être gardée et passée aux fonctions javascript
		!isset($_SESSION[$typev]['viewAv']) ? $_SESSION[$typev]['viewAv'] = "grid" : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
		
		$out = '';
		if (!empty($data)) {
			$out .= "<ul> ";
			foreach ($data as $v) {
				$count = dbQueryOne("SELECT COUNT(*) as c FROM sfk_catalog_product WHERE categories = ".$v['cat_id']." ");
				$out .= "<li>";
				$out .=		"<span class='tree-expander' onclick='expandnextcat(this);'></span>"; 
				$out .= 	"<span class='lev_$level' onclick=
													'loadnextcat(this);
													loadEnteteAvahis(this); 
													loadProductListAv(\"".$_SESSION[$typev]['viewAv']."\"); 
													loadFilterAv(this);
													loadBrandFilterAv(this); 
													' id='" . $v['cat_id'] . "' >";
				//$out .=								$v['cat_label'] ."(".$v['count_prod'].")";
				$out .=								$v['cat_label'] ."(".$count['c'].")";
				$out .=		"</span>";			
				$out .=	"</li>";
			}
			$out .= "</ul>";
		}
		
		return $out;
	}


	/**
	 * Fonction permettant de générer un arbre de catégories sous forme de <ul> <li> à partir de donnée contenant
	 * toute les sous catégories d'une catégories (un niveau entier de catégorie). Chaque clic sur une catégorie
	 * lance un chargement ajax permettant de rappeller cette fonction pour les sous catégories.
	 * @param 	array 		$data 	tableau contenant les catégories label et id
	 * @param 	integer 	$level  le niveau d'imbrication de la catégorie
	 * @return 	string 		$out 	l'Output HTML
	 */
	public function showcatGP($data, $level = 0) {
		$typev = "avaGP";
		
		//La vue en mode grille ou liste doit être gardée et passée aux fonctions javascript
		!isset($_SESSION[$typev]['viewAv']) ? $_SESSION[$typev]['viewAv'] = "grid" : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
		
		$out = '';
		// differenciation de la parti gestion/produit et qualif/assoc
		if (!empty($data)) {
		//var_dump( $GLOBALS['typeVendor']);
		$out .= "<ul> ";
			foreach ($data as $v) {
				$count = dbQueryOne("SELECT COUNT(*) as c FROM sfk_catalog_product cp
				                                          LEFT JOIN sfk_product_ecommercant_stock_prix sp ON sp.id_produit = cp.sku 
				                                          WHERE categories = ".$v['cat_id']." AND sp.QUANTITY > 0 ");
				$out .= "<li>";
				$out .=		"<span class='tree-expander' onclick='expandnextcatGP(this);'></span>"; 
				$out .= 	"<span class='lev_$level' onclick=
													'loadnextcatGP(this);
													loadEnteteAvahisGP(this); 
													loadProductListAvGP(\"".$_SESSION[$typev]['viewAv']."\"); 
													loadFilterAv(this);
													loadBrandFilterAvGP(this); 
													' id='" . $v['cat_id'] . "' ><a href='#' ";
                if(strlen($v['cat_label']) > 31){
                    $out .= "data-toggle='tooltip' title ='".$v['cat_label']."'>" . truncateStr($v['cat_label'], 32) ."(".$count['c'].") </a>";
                }else{
                    $out .= " >".$v['cat_label'] ."(".$count['c'].") </a>";
                }
				//$out .=								$v['cat_label'] ."(".$v['count_prod'].")";
				//$out .=								truncateStr($v['cat_label'], 32) ."(".$count['c'].") </a>";
				$out .=		"</span>";			
				$out .=	"</li>";
				
			}
			$out .= "</ul>";	
		}
		return $out;
	}

	
	/**
	 * Même principe que la function showcat mais affichage de checkboxes pour plier et déplier l'arbre de catégories
	 * les sous-catégories sont chargées en AJAX 
	 */
	public function showTreeCheckbox($data, $first = false, $level) {
		
		$out = '';
		if (!empty($data)) {
			if($first) $out .= "<ul id='checkboxtree'> ";
			else $out .= "<ul> ";
			foreach ($data as $v) {
				
				$out .= "<li>
							<input type='checkbox' id='p".$v['cat_id']."' name='__".$v['cat_id']."' onclick='loadnextcatcheckbox(this)'  class='lev_$level' value='".$v['cat_id']."'/>
							<label for='p".$v['cat_id']."'>" . $v['cat_label'] ."(".$v['count_prod'].")"."</label> 
						</li>";

			}
			$out .= "</ul>";
		}
		return $out;
	}
	
	

	
	/**
	 * Fonction d'affichage permettant d'afficher l'entete de la partie ecommercant. Il y a affichage de "chemin" des 
	 * catégories ainsi que multiples information sur le nombre de produits dans cette catégorie, ceux associées
	 * et ceux qui ne sont pas encore associées. Pour le moment seuls ceux qui SONT associés sont affichés
	 * @param 	string 	$catCli 			la chaine contenant l'arborescence de catégorie en cours
	 * @param 	string 	$tech 				la tech de l'ecommerçant ex : "Prestashop"
	 * @param 	integer $idVendor 			l'id du vendeur en cours dont on va afficher l'entête
	 * @param 	boolean $correspondance 	true si la catégorie est bien associée sinon false
	 * @param 	integer $id_cat_assoc 		l'id de la catégorie associée si elle l'est
	 */
	public function showEnteteEcommercant($catCli, $tech, $idVendor, $correspondance = false, $id_cat_assoc = false)
	{
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
		elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
			$typev = "avaGP";
		}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
		
		//Mise en session des variables utiles tout le long de la navigation pour cet ecommercant
		$_SESSION[$typev]['tech']   = $tech ;
		$_SESSION[$typev]['vendor'] = $idVendor ;
		$_SESSION[$typev]['cat'] 	= $catCli ;
			
		$dbClass = "BusinessSoukeo".$tech."Model";
		if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
		$db = new $dbClass();
		
		//Affichage du chemin
		$out = '<ul class="ariane">';
		$tree = explode('>', $catCli);
		$tree = array_map('trim', $tree);
		
		foreach ($tree as $categ) {
			if ($categ == end($tree)){
				$out .= "<li><a href='#' onclick='linkBreadcrumps(\"".htmlentities($catCli, ENT_QUOTES, 'UTF-8')."\");' class='active'>".htmlentities($categ, ENT_QUOTES, 'UTF-8')."</a></li>";
			}else{
				$link .= " > ".$categ;
				$out .= "<li><a href='#' onclick='linkBreadcrumps(\"".substr($link, 3)."\");'>".htmlentities($categ, ENT_QUOTES, 'UTF-8')."</a></li>";	
			}
			
		}
		$out .= '</ul>';
		$out .= '<div class="information">
                    <ul>';
		//Récupération des produits et compte des produits associés

		$listeProduits = $db -> getAllProdByCategVendor($catCli, $idVendor);
		$countAssoc = $db -> getCountAssocProdByCatVendor($catCli, $idVendor);

		$out .= '		<li><strong>'.count($listeProduits).' produits</strong> - <span class="red">'.(count($listeProduits) - $countAssoc['count']) .' non validés</span></li>';
		
		//Préparations données JSON pour popup AJAX permettant d'associer une catégorie ecommercant
		//A une catégorie avahis
		$typev == "gr" ? $typeVendor = "grossiste" : $typeVendor = "ecommercant" ;
        $typev == "gr" ? $path = "grossistes" : $path = "commercants" ;
		$arr = array('url' 			=> '/'.$path.'/'.$typeVendor.'/popup_assoc_categ/index.php', 
							 'vendor_id' 	=> $_SESSION[$typev]['vendor'],
							 'cat_ecom' 		    => str_replace(array(' ','"'), array('+', '\\"'),str_replace(' > ','|',$_SESSION[$typev]['cat'] )),
							 'id_cat_assoc' => $id_cat_assoc);
        
		$data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP);
		$action = "showDialogAssocCateg('".$data."')";
			
		//Si cette catégorie ecommercant est déja associé on a déja la catégorie avahis correspondante à celle ci
		//Le texte va changer mais l'actions pour lancer le popup de modification d'association est toujours diponnible
		if($correspondance){
			
			$out .= "<li id='assoc_cat'>Associé à <b>".substr($correspondance,0,strlen($correspondance)-2)."</b> <a href='#' onclick='loadcat(\"".$db -> getCatIdTree($id_cat_assoc)."\")'>(Voir)</a></li>";
			$out .=	' <a onclick='.$action.' href="#">modifier</a>
					 <span id="id_cat_assoc" style="display:none">'.$id_cat_assoc.'</span>
					</ul>
                </div>
                <div class="action">
                	<ul>
                    	<li></li>
                   	 </ul>
                </div>';
		//Pas de correspondance
		}else{
			$out .= '		<li id="assoc_cat">cette catégorie n\'a pas pu être associée</li>
							<a onclick='.$action.' href="#">modifier</a>
							<span id="id_cat_assoc" style="display:none"></span>
						</ul>
	                </div>
	                <div class="action">
	                	<ul>
	                    	<li></li>
	                   	 </ul>
	                </div>';	
		}
		$out.='<p id = "cat_ecom"   style="display:none">'.$_SESSION[$typev]['cat'].'</p>';		
		return $out;
	}
	
	
	
	public function showNavigation($nbAssoc, $nbRefused, $nbSelected)
	{
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
		
		$out .= "<li id='ongAll' onclick='changeContext(\"all\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>Tous</a></li>
                    <li id='ongSelected' onclick='changeContext(\"selected\", this);loadSelectedProductList(\"".$_SESSION[$typev]['view']."\");return false;'><a id='prod_selected' href='#'>Sélection";
   
    	$out.= "(".$nbSelected.") </a></li>
                <li id='ongAssoc' onclick='changeContext(\"assoc\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>Associés (".$nbAssoc.")</a></li>
                <li id='ongRefused' onclick='changeContext(\"refused\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>Refusés (".$nbRefused.")</a></li>
                <li id='ongTodo' onclick='changeContext(\"todo\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>A traiter</a></li>";
        
		return $out ;
	}
	
	
	public function showNavigationGrossiste($nbAssoc, $nbRefused, $nbSelected)
	{
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
		
		$out .= "<li id='ongAll' onclick='changeContext(\"all\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>Tous</a></li>
                    <li id='ongSelected' onclick='changeContext(\"selected\", this);loadSelectedProductList(\"".$_SESSION[$typev]['view']."\");return false;'><a id='prod_selected' href='#'>Sélection";
   
    	$out.= "(".$nbSelected.") </a></li>
                <li id='ongAssoc' onclick='changeContext(\"assoc\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>Ajoutés (".$nbAssoc.")</a></li>
                <li id='ongRefused' onclick='changeContext(\"refused\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>Refusés (".$nbRefused.")</a></li>
                <li id='ongTodo' onclick='changeContext(\"todo\", this);loadProductList (\"".$_SESSION[$typev]['view']."\");loadCatEcom();return false;'><a href='#'>A traiter</a></li>";
        
		return $out ;
	}

		
	/**
	 * Affichage des onglets de navigation pour la gestion des produits avahis
	 * @param 
	 *
	 */
	public function showNavigationAvahis($nbDeleted, $nbSelected)
	{
	 
		$typev = "ecom";

		$out .= "<li id='ongAll' onclick='changeContext(\"all\", this);loadProductListAvGP (\"".$_SESSION[$typev]['viewAv']."\");loadCat();return false;'>
					<a href='#'>Tous</a>
				</li>
                <li id='ongSelected' onclick='changeContext(\"selected\", this);loadSelectedProductListAv(\"".$_SESSION[$typev]['viewAv']."\");return false;'>
                	<a id='prod_selected' href='#'>Sélection(".$nbSelected.") </a>
                </li>
                <li id='ongDeleted' onclick='changeContext(\"deleted\", this);loadProductListAvGP (\"".$_SESSION[$typev]['viewAv']."\");loadCat();return false;'>
                	<a href='#'>Supprimés (".$nbDeleted.")</a>
                </li>
				";        

		return $out ;
	}
	
		
	/**
	 * Affichage des onglets de navigation pour la gestion des produits avahis
	 * @param 
	 *
	 */
	public function showNavigationAvahisGP($nbDeleted, $nbSelected)
	{

		$typev = "avaGP";

		$out .= "<li id='ongAll' onclick='changeContextGP(\"all\", this);loadProdListFilteredGP (\"".$_SESSION[$typev]['viewAv']."\");return false;'>
					<a href='#'>Tous</a>
				</li>
                <li id='ongSelected' onclick='changeContextGP(\"selected\", this);loadSelectedProductListAvGP(\"".$_SESSION[$typev]['viewAv']."\");return false;'>
                	<a id='prod_selected' href='#'>Sélection(".$nbSelected.") </a>
                </li>";
                //<li id='ongDeleted' onclick='changeContextGP(\"deleted\", this);loadProductListAvGP (\"".$_SESSION[$typev]['viewAv']."\");loadCatGP();return false;'>
                	//<a href='#'>Supprimés (".$nbDeleted.")</a>
                //</li>
				//";        

		return $out ; 
	}	
	/**
	 * Affichage de l'entete pour la partie gauche de l'écran Coté avahis, il y a affichage d'un chemin des catégories 
	 * explorées. Moins d'options sont disponnibless de ce coté
	 */
	public function showEnteteAvahis($idCat)
	{
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
		//Avahis GP
		elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
			$typev = "avaGP";
		}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
		
		//Mise en session de la catégorie avahis
		$_SESSION[$typev]['catAv'] 	= $idCat ;
		
		
		$db = new BusinessSoukeoModel();
		$tree = $db -> getCatIdTree($idCat, "");
		$_SESSION[$typev]['catAvIdTree'] = $tree;
		
		$out = '<ul class="ariane">';
		$tree = explode('|', $tree);
		$tree = array_map('trim', $tree);
		$tree = array_reverse($tree);
		
		
		foreach ($tree as $categ) {
			$infoCat = $db -> getCatbyId($categ);
			if ($categ == end($tree) & !empty($categ)){
				$out .= "<li><a href='#' onclick='loadcat(\"".$db -> getCatIdTree($categ)."\");' class='active'>".$infoCat['cat_label']."</a></li>";
			}elseif(!empty($categ)){
				$out .= "<li><a href='#' onclick='loadcat(\"".$db -> getCatIdTree($categ)."\");' >".$infoCat['cat_label']."</a></li>";	
			}			
		}
		$out .= '</ul>';		
		/*
		 * De plus si c'est pour la gestion des produits Avahis GESTION/Management Avahis
		 */ 
		if($_SESSION['gestion']=="produit"){

		}			
		
			
		return $out;
	}
	/**
	 * Affichage de l'entete pour la partie gauche de l'écran Coté avahis, il y a affichage d'un chemin des catégories 
	 * explorées. Moins d'options sont disponnibless de ce coté
	 */
	public function showEnteteAvahisGP($idCat)
	{
		$typev = "avaGP";
		//Mise en session de la catégorie avahis
		$_SESSION[$typev]['catAv'] 	= $idCat ;
		
		$db = new BusinessSoukeoModel();
		$tree = $db -> getCatIdTree($idCat, "");
		$_SESSION[$typev]['catAvIdTree'] = $tree;
		
		$out = '<ul class="ariane">';
		$tree = explode('|', $tree);
		$tree = array_map('trim', $tree);
		$tree = array_reverse($tree);
		
		foreach ($tree as $categ) {
			$infoCat = $db -> getCatbyId($categ);
			if ($categ == end($tree) & !empty($categ)){
				$out .= "<li><a href='#' onclick='loadcatGP(\"".$db -> getCatIdTree($categ)."\");' class='active'>".$infoCat['cat_label']."</a></li>";
			}elseif(!empty($categ)){
				$out .= "<li><a href='#' onclick='loadcatGP(\"".$db -> getCatIdTree($categ)."\");' >".$infoCat['cat_label']."</a></li>";	
			}			
		}
		$out .= '</ul>';		
				
		return $out;
	}
	
	
	/**
	 * Affiche une liste de produit sous deux formats grid et liste pour les catalogue de l'ecommercant
	 * et le catalogue avahis. Cette fonction se divise en deux affichages distincts si la vue voulue
	 * est le mode grille ou bien le mode liste. L'autre condition d'affichage de la fonction est le type de produit
	 * si c'est un affichage coté produit ecommercant ou bien affichage coté avahis.
	 */
	public function showProdList($listeProduits, $view, $type='', $id_cat_assoc=false, $totalProd = 0, $currPage = 1)
	{
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
		//Avahis GP
		elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
			$typev = "avaGP";
		}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
		//CHOIX VUE GRID / VIEW
		if($view == "") $view = "grid";

		//PREPARATION PAGINATION
		$nbProdsByPage = SystemParams::getParam($_SESSION['gestion']=="produit"?'system*nb_prods_page_avahis':'system*nb_prods_page');
		
		$nbPages = (int)($totalProd / $nbProdsByPage) ;
		if(($totalProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ; 
		if(($totalProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;
		//Différenciation coté gauche et coté droit
		if($type =='ecom') $idNbProd = "nbProd";
		if($type =='av')   $idNbProd = "nbProdAv";
		
		
		////////////////////////////////////////////PAGINATION/////////////////////////////////////////////////////////////////
		$out.= "<div class='pager'>";
		$out.= "<a href='#' onclick='firstPageProd(this);return false;' class='pager_first'><img src = '/skins/common/images/pager_first.png' alt='First page'></a><span> | </span>";
		$out.= "<a href='#' onclick='prevPageProd(this); return false;' class='pager_prev'><img src = '/skins/common/images/pager_prev.png' alt='Previous page'></a><span> [ ".$currPage." / ".$nbPages." ] </span>";
		$out.= "<a href='#' onclick='nextPageProd(this); return false;' class='pager_next'><img src = '/skins/common/images/pager_next.png' alt='Next page'></a><span> | </span>";
		$out.= "<a href='#' onclick='lastPageProd(this); return false;' class='pager_last'><img src = '/skins/common/images/pager_last.png' alt='Last page'></a>";
		$out.= "</div>";
		//////////////////////////////////////// FILTRE PRIX CROISSANT DECROISSANT //////////////////////////////////////////////
				
		$out.= " <span class='priceOrder'>|";
		$out.= "<a href='#' onclick='loadProdListFiltered".ucfirst($type)."(\"$view\", \"asc\"); return false;'>Prix croissant</a>|";
		$out.= "<a href='#' onclick='loadProdListFiltered".ucfirst($type)."(\"$view\", \"desc\"); return false;'>Prix décroisant</a>|</span>";
		
		//////////////////////////////////////// SPAN AFFICHAGE NOMBRE RESULTAT ////////////////////////////////////////////////
		$out.= "<p><span id='".$idNbProd."' class='results'>".$totalProd."</span><span> résultat(s) trouvé(s)</span></p>";	
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////// BOUCLE PRODUITS /////////////////////////////////////////////////////////
		$out .= "<ul class='thumbnails'>";
		foreach ($listeProduits as $prod) {
		
			//Traitement chaine nom et EAN pur pas de noms trop grand	
			mb_strlen($prod['NAME_PRODUCT']) > 35 ? $nameProd = mb_substr($prod['NAME_PRODUCT'], 0,35) : $nameProd = $prod['NAME_PRODUCT'];
			$prod['EAN'] == "" ? $ean = "--" : $ean = $prod['EAN'];
			
			//Add class selectionné, refusé etc
			//CLASS CSS COSMETIQUE D'ETAT DU PRODUIT
			$add_class = (isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds']) ? 'selected' :"" );
			if($_SESSION['gestion']=="produit")$add_class .= ' produitGP ';
			$prod['refus'] == 1 ? $add_class .= " refused": $add_class = $add_class;
			$prod['delete'] == 1 ? $add_class .= " refused": $add_class = $add_class;
			$prod['assoc'] == 1 ? $add_class .= " associated": $add_class = $add_class;
			$prod['AJOUTE'] == 1 ? $add_class .= " associated": $add_class = $add_class;
			$prod['WEIGHT'] == '0' | $prod['WEIGHT'] == '' ? $add_class .= " noweight" : $add_class = $add_class;
			
			///////////////////////////////////////// PREPARATION DU JSON ///////////////////////////////////////////////////////////
			//Utilisé en AJAX ensuite pour avoir les bonne donnée dans le popup
			$typev == "gr" ? $typeVendor = "grossiste" : $typeVendor = "ecommercant";
			$typev == "gr" ? $path = "grossistes" : $path = "commercants" ; 
			$arr = array('url' 			=> '/'.$path.'/'.$typeVendor.'/popup_assoc/index.php', 
						 'vendor_id' 	=> $_SESSION[$typev]['vendor'],
						 'tech' 		=> $_SESSION[$typev]['tech'],
						 'prod_id' 		=> $prod['ID_PRODUCT'],
						 'cat_ecom'     => str_replace(' ', '+',str_replace(' > ','|',$_SESSION[$typev]['cat'] )));
			
			//Pour l'affichage du popup il y a différenciation, il y a besoin de la tech ecommercant pour l'affichage
			//du pop-up ecommercant mais pas pour l'autre. (les classes sont consstruites dynamiquement en évaluant une chaine)			 
			$data = json_encode($arr, JSON_HEX_APOS);
			if($type == 'ecom') {
				$action = "showDialogEcom('".$data."')";
				
			}
			elseif($type == 'av'){
				$arr['tech'] ="";

				$arr['url'] ='/catalogue/produit/index.php' ; // si c'est pour la gestion/produit
				$data = json_encode($arr, JSON_HEX_APOS);
				$action = "showDialogAv('".$data."')";
			}
			
			////////////////////////////////////////////// HTML ////////////////////////////////////////////////////////////////////
			//HTML pour la div du produit, l'id est l'ID produit //////////////////////////////////////////////////////////////////
			//$out.= '<ul class="thumbnails">';
			$out.='<li  id="'.$prod['ID_PRODUCT'].'" class="produit '.$add_class.' span3" >';
			$out.= '<div class="thumbnail">';
			
			if($view == "list"){
				$out.= '<div class="imgProd" onclick='.$action.'><img src="'.$prod['IMAGE_PRODUCT'].'"></div>';
				$out.= "<h5>".htmlentities($nameProd, ENT_QUOTES, 'UTF-8')."</h5>";			
				$out.= "<p class='ref text-info'> Ref : ".$prod['REFERENCE_PRODUCT']."</p>";
			}
			elseif($view == "grid"){
				
				$out.= '<div class="imgProd" onclick='.$action.'><img  src="'.$prod['IMAGE_PRODUCT'].'"></div>';
				$out.= "<h5>".$nameProd."</h5>";
				$out.= '<span class="price label label-inverse">'.numberByLang($prod['PRICE_PRODUCT']).'€</span> 
				        <span class="attribut">'.$prod['MANUFACTURER'].'</span>
				        <span class="attribut">Ref: '.$prod['REFERENCE_PRODUCT'].'</span>';
				        //<span class="attribut">EAN :'.$prod['EAN'].'</span>';
				$desc = $prod['DESCRIPTION_SHORT'];
				$desc == "NULL" ? $desc = "Pas de description" : $desc = $desc;
				$out.= '<p class="description">'.htmlentities($desc, ENT_QUOTES, 'UTF-8').'</p>';
			}
			
			$out.= '<ul class="actions">';
			
			//////////////////////////////////////// SPECIFICTES PROD ECOM ///////////////////////////////////////////////////////
			//Un produit type ecommercant doit etre selectionnable et on doit pouvoir l'ajouter au traverss d'un
			//popup nouss guidant dans l'ajout d'un produit. Il faut également un lien pour associer le produit
			if($type =='ecom'){
				$typev == "gr" ? $typeVendor = "grossiste" : $typeVendor = "ecommercant";
                $typev == "gr" ? $path = "grossistes" : $path = "commercants" ;
				$arr['url'] = '/'.$path.'/'.$typeVendor.'/popup_add/index.php' ;
				if($id_cat_assoc) $arr['cat_id_assoc'] = $id_cat_assoc;
				$data = json_encode($arr, JSON_HEX_APOS);
				
				//SI PRODUIT NON REFUSE
				if($prod['refus']!=1){
					$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >';
					$action = "showDialogAddProduct('".$data."')";
					
					//SI PRODUIT ASSOCIE
					if($prod['assoc'] == 1){
						
						//PRODUIT ASSOCIE ET SELECTIONNE
						if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
							$out.= 'Désélect.</a></li><li><a style="display:none" href="#" onclick='.$action.'>Ajouter</a></li>';
						}
						else{
							$out.= 'Sélection</a></li><li><a style="display:none" href="#" onclick='.$action.'>Ajouter</a></li>';	
						}
						$out .= '<li><a href="#" onclick="associateProduct(this);return false;" class="assocButtonEcom">Désassocier</a></li>';
						$out .= '<li><a style="display:none" href="#" onclick="refuseProduct(this); return false;">Refuser</a></li></p>';
					}
					elseif($prod['AJOUTE'] == 1){
						//PRODUIT ASSOCIE ET SELECTIONNE
						if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
							$out.= 'Désélect.</a></li><li><a style="display:none" href="#" onclick='.$action.'>Ajouter</a></li>';
						}
						else{
							$out.= 'Sélection</a></li><li><a style="display:none" href="#" onclick='.$action.'>Ajouter</a></li>';	
						}
					}
					else{
						//SI PRODUIT EN SELECTION
						if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
							$out.= 'Désélect.</a></li><li><a href="#" onclick='.$action.'>Ajouter</a></li>';
						}else{
							$out.= 'Sélection</a></li><li><a href="#" onclick='.$action.'>Ajouter</a></li>';	
						}
						
						$out .= '<li><a href="#" onclick="associateProduct(this); return false;" class="assocButtonEcom">Associer</a></li><li><a href="#" class="refused" onclick="refuseProduct(this); return false;">Refuser</a></li></ul>';							
					}
					
				}else{
					//SI PRODUIT EN SELECTION
					if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
						$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >Désélect.</a><a style="display:none;" href="#" onclick='.$action.'>Ajouter</a></li>';
					}else{
						$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >Sélection</a><a style="display:none;" href="#" onclick='.$action.'>Ajouter</a></li>';	
					}
						$out .='<li><a style="display:none" href="#" onclick="associateProduct(this);return false;" class="assocButtonEcom">Associer</a></li>
							<li><a class="refused" onclick="refuseProduct(this); return false;" href="#">Annuler refus</a></li>';
				}
				
			}
			//////////////////////////////////////// SPECIFICTES PROD AVAHIS ///////////////////////////////////////////////////////////////
			//Un produit type avahis a juste besoin d'un lien associer pour associer le produit à un prod ecommercant
			elseif($type =='av'&!$_SESSION['gestion']=="produit"){
				$out .= '<a href="#" onclick="associateProductAv(this);return false;" class="assocButtonAv">Associer</a></p>';
			}
			//////////////////////////////////////// SPECIFICITES PROD GESTION AVAHIS ///////////////////////////////////////////////////////////////
			//Affichage spécial pour la gestion des produits Avahis
			elseif($_SESSION['gestion']=="produit"){
			$arr['url'] = '/commercants/ecommercant/popup_add/index.php' ;
				if($id_cat_assoc) $arr['cat_id_assoc'] = $id_cat_assoc;
				$data = json_encode($arr, JSON_HEX_APOS);
				
				//SI PRODUIT NON supprimé
				if($prod['delete']!=1){
					$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >';
					$action = "showDialogAv('".$data."')";
						
						if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
							$out.= 'Désélect.</a></li><li><a style="display:none" href="#" onclick='.$action.'>Ajouter</a></li>';
						}
						else{
							$out.= 'Sélection</a></li><li><a href="#" onclick="modifCat(this);">Modif Cat</a></li>';	
						}
						$out .= '<li><a style="deleted" href="#" onclick="supProduct(this); return false;">Supprimer</a></li>';
					
					}
	
					
				else{
					//SI PRODUIT EN SELECTION
					if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
						$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >Désélect.</a></li>
								<li><a href="#" style="display:none" onclick"modifCat(this);">Modif Cat</a></li>';
					}else{
						$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >Sélection</a></li>
								<li><a href="#"  onclick="modifCat(this);">Modif Cat</a></li>';	
					}
						$out .='<li><a class="deleted" onclick="supProduct(this); return false;" href="#">Annuler suppression</a></li>';
				}

			}
			//Fermeture div produit
			$out .= "</div></li>";
		}
		$out .= "</ul>";
		
		////////////////////////////////////////////PAGINATION/////////////////////////////////////////////////////////////////
		$out.= "<div class='pager'>";
		$out.= "<a href='#' onclick='firstPageProd(this);return false;' class='pager_first'><img src = '/skins/common/images/pager_first.png' alt='First page'></a><span> | </span>";
		$out.= "<a href='#' onclick='prevPageProd(this); return false;' class='pager_prev'><img src = '/skins/common/images/pager_prev.png' alt='Previous page'></a><span> [ ".$currPage." / ".$nbPages." ] </span>";
		$out.= "<a href='#' onclick='nextPageProd(this); return false;' class='pager_next'><img src = '/skins/common/images/pager_next.png' alt='Next page'></a><span> | </span>";
		$out.= "<a href='#' onclick='lastPageProd(this); return false;' class='pager_last'><img src = '/skins/common/images/pager_last.png' alt='Last page'></a>";
		$out.= "</div>";
		//Renvoi HTML
		return $out;
	}
	/**
	 * Affiche une liste de produit sous deux formats grid et liste pour le catalogues Avahis GP
	 */
	public function showProdListGP($listeProduits, $view, $type='', $id_cat_assoc = false, $totalProd = 0, $currPage = 1)
	{
		$typev = "avaGP";

		//CHOIX VUE GRID / VIEW
		if($view == "") $view = "grid";

		//PREPARATION PAGINATION
		$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis');
		
		//var_dump($nbProdsByPage);
		$nbPages = (int)($totalProd / $nbProdsByPage) ;
		if(($totalProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ; 
		if(($totalProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;
		//Différenciation coté gauche et coté droit
		if($type =='ecom') $idNbProd = "nbProd";
		if($type =='av')   $idNbProd = "nbProdAv";
		if($type =='avaGP')   $idNbProd = "nbProdAv";
		
		////////////////////////////////////////////PAGINATION/////////////////////////////////////////////////////////////////
		$out.= "<div class='pager'>";
		$out.= "<a href='#' onclick='firstPageProd(this);return false;' class='pager_first'><img src = '/skins/common/images/pager_first.png' alt='First page'></a><span> | </span>";
		$out.= "<a href='#' onclick='prevPageProd(this); return false;' class='pager_prev'><img src = '/skins/common/images/pager_prev.png' alt='Previous page'></a><span> [ ".$currPage." / ".$nbPages." ] </span>";
		$out.= "<a href='#' onclick='nextPageProd(this); return false;' class='pager_next'><img src = '/skins/common/images/pager_next.png' alt='Next page'></a><span> | </span>";
		$out.= "<a href='#' onclick='lastPageProd(this); return false;' class='pager_last'><img src = '/skins/common/images/pager_last.png' alt='Last page'></a>";
		$out.= "</div>";
		
		//////////////////////////////////////// FILTRE PRIX CROISSANT DECROISSANT //////////////////////////////////////////////
		$out.= " <span class='priceOrder'>|";
		$out.= "<a href='#' onclick='loadProdListFilteredGP(\"$view\", \"asc\"); return false;'>Prix croissant</a>|";
		$out.= "<a href='#' onclick='loadProdListFilteredGP(\"$view\", \"desc\"); return false;'>Prix décroisant</a>|</span>";
		
		//////////////////////////////////////// SPAN AFFICHAGE NOMBRE RESULTAT ////////////////////////////////////////////////
		$out.= "<h4><span id='".$idNbProd."' class='results'>".$totalProd."</span><span> résultat(s) trouvé(s)</span></h4>";	
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////// BOUCLE PRODUITS /////////////////////////////////////////////////////////
		$out .= "<ul class='thumbnails'>";
		
		foreach ($listeProduits as $prod) {
		
			//Traitement chaine nom et EAN pur pas de noms trop grand	
			strlen($prod['NAME_PRODUCT']) > 35 ? $nameProd = substr($prod['NAME_PRODUCT'], 0,35) : $nameProd = $prod['NAME_PRODUCT'];
			$prod['EAN'] == "" ? $ean = "--" : $ean = $prod['EAN'];
			
			//Add class selectionné, refusé etc
			//CLASS CSS COSMETIQUE D'ETAT DU PRODUIT
			$add_class = (isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds']) ? 'selected' :"" );
			//si écran gestion/produit
			$add_class .= ' produitGP ';
			$prod['refus'] 	== 1 ? $add_class .= " refused": $add_class = $add_class;
			$prod['delete'] == 1 ? $add_class .= " refused": $add_class = $add_class;
			$prod['assoc'] 	== 1 ? $add_class .= " associated": $add_class = $add_class;
			$prod['AJOUTE'] == 1 ? $add_class .= " associated": $add_class = $add_class;
			$prod['WEIGHT'] == '0' | $prod['WEIGHT'] == '' ? $add_class .= " noweight" : $add_class = $add_class;
			
			///////////////////////////////////////// PREPARATION DU JSON ///////////////////////////////////////////////////////////
			//Utilisé en AJAX ensuite pour avoir les bonne donnée dans le popup
			$typev == "gr" ? $path = "grossistes" : $path = "commercants" ;
             
			$arr = array('url' 			=> '/'.$path.'/'.$typeVendor.'/popup_assoc/index.php', 
						 'vendor_id' 	=> $_SESSION[$typev]['vendor'],
						 'tech' 		=> $_SESSION[$typev]['tech'],
						 'prod_id' 		=> $prod['ID_PRODUCT'],
						 'cat_ecom'     => str_replace(' ', '+',str_replace(' > ','|',$_SESSION[$typev]['cat'] )));
			
			//Pour l'affichage du popup il y a différenciation, il y a besoin de la tech ecommercant pour l'affichage
			//du pop-up ecommercant mais pas pour l'autre. (les classes sont consstruites dynamiquement en évaluant une chaine)			 
			$data = json_encode($arr);
		
			$arr['tech'] ="";

			$arr['url'] ='/catalogue/produit/index.php' ; // si c'est pour la gestion/produit
			$data = json_encode($arr);
			$action = "showDialogAvGP('".$data."')";
            $action = "openFicheProduit('".$prod['ID_PRODUCT']."'); return false";
		
			
			////////////////////////////////////////////// HTML ////////////////////////////////////////////////////////////////////
			//HTML pour la div du produit, l'id est l'ID produit //////////////////////////////////////////////////////////////////
			$out.='<li id="'.$prod['ID_PRODUCT'].'" class="produit '.$add_class.' span2" >';
			$out.= '<div class="thumbnail">';
			
            $prod['LOCAL_IMAGE'] ? $img = $prod['LOCAL_IMAGE'] : $img = $prod['IMAGE_PRODUCT'];
             
			if($view == "list"){
				$out.= '<a href="#"><div class="imgProd" onclick='.$action.'><img src="'.$img.'"></div></a>';
				$out.= "<div><h5>".htmlentities($nameProd, ENT_QUOTES, 'UTF-8')."</h5>";			
				$out.= "<span class='ref text-info'><i class='fa fa-certificate'></i>Ref : ".$prod['REFERENCE_PRODUCT']."</span></div>";
			}
			elseif($view == "grid"){
				
				$out.= '<a href="#"><div class="imgProd" onclick='.$action.'><img  src="'.$img.'"></div></a>';
				$out.= "<h5>".htmlentities($nameProd, ENT_QUOTES, 'UTF-8')."</h5>";
				$out.= '<span class="price label label-inverse">'.numberByLang($prod['PRICE_PRODUCT']).'€</span> <span class="attribut text-info"><i class="fa fa-bookmark"></i>'.$prod['MANUFACTURER'].'</span> <span class="attribut text-info "><i class="fa fa-certificate"></i>'.$prod['REFERENCE_PRODUCT'].'</span>';
				$desc = $prod['DESCRIPTION_SHORT'];
				$desc == "NULL" ? $desc = "Pas de description" : $desc = $desc;
				$out.= '<p class="description">'.htmlentities($desc, ENT_QUOTES, 'UTF-8').'</p>';
			}
			
			$out.= '<ul class="actions">';
			
			//////////////////////////////////////// SPECIFICITES PROD GESTION AVAHIS ///////////////////////////////////////////////////////////////
			//Affichage spécial pour la gestion des produits Avahis
			if($_SESSION['typeVendor'] == "avahisGP"){
			$arr['url'] = '/commercants/ecommercant/popup_add/index.php' ;
				if($id_cat_assoc) $arr['cat_id_assoc'] = $id_cat_assoc;
				$data = json_encode($arr);
				
				//SI PRODUIT NON supprimé
				if($prod['delete']!=1){
					$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;">';
					$action = "showDialogAv('".$data."')";
					
						if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
							$out.= 'Désélect.</a></li> <li><a style="display:none" href="#" onclick='.$action.'>Ajouter</a>';
						}
						else{
							$out.= 'Sélection</a></li> <li><a href="#" onclick="modifCat(this);">Modif Cat</a></li>';	
						}
						$out .= '<li><a style="deleted" href="#" onclick="supProduct(this); return false;">Supprimer</a></ul>';
					}
				else{
					//SI PRODUIT EN SELECTION
					if(isset($_SESSION[$typev]['selectedProds']) && in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
						$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >Désélect.</a></li>
								<li><a href="#" style="display:none" onclick"modifCat(this);">Modif Cat</a></li>';
					}else{
						$out.= '<li><a class="btnSelect" href="#" onclick="selectProduct(this); return false;" >Sélection</a></li>
								<li><a href="#"  onclick="modifCat(this);">Modif Cat</a></li>';	
					}
						$out .='<li><a class="deleted" onclick="supProduct(this); return false;" href="#">Annuler suppression</a></li>';
				}
			}
			//Fermeture div produit
			$out.= '</div>';
			$out .= "</li>";
		}
		$out .= "</ul>";
		$out.= "<div class='pager'>";
		$out.= "<a href='#' onclick='firstPageProd(this);return false;' class='pager_first'><img src = '/skins/common/images/pager_first.png' alt='First page'></a><span> | </span>";
		$out.= "<a href='#' onclick='prevPageProd(this); return false;' class='pager_prev'><img src = '/skins/common/images/pager_prev.png' alt='Previous page'></a><span> [ ".$currPage." / ".$nbPages." ] </span>";
		$out.= "<a href='#' onclick='nextPageProd(this); return false;' class='pager_next'><img src = '/skins/common/images/pager_next.png' alt='Next page'></a><span> | </span>";
		$out.= "<a href='#' onclick='lastPageProd(this); return false;' class='pager_last'><img src = '/skins/common/images/pager_last.png' alt='Last page'></a>";
		$out.= "</div>";
		//Renvoi HTML
		return $out;
	}
	
	
	/**
	 * Affiche les filtres de recherche dynamiques à partir d'un tableau contenant le couple
	 * Attribut et ses valeurs. Le but étant d'afficher les 3 attributs les plus utilisés dans la catégorie
	 * de produits actuelle. Et y référencer sous forme de liste pour chaque attributs les différentes valeurs
	 * existantes.
	 */
	public function showDynaFilter($attrVal, $type = "")
	{
		if(($type == "Grossiste")){
			$i=1;
			foreach ($attrVal as $key => $value) {
				$out.="<div class='iblock vtop'>";
				$out.="<label class='control-label'>".$key."</label>";
				$out.= "<select class='attr".$i."' id='".$key."'>";
				$out.= "<option selected='selected'>--</option>";
				
				//Affichage de la liste des options de valeurs
				
				foreach ($value as $val) {
					$out.="<option>".$val[$key]."</option>";
				}
				$out.= "</select><br>";
				$out.="</div>";
				$i++;
			}
		}
		else{
			$i=1;
			//Pour chaque couple attributs et ses valeurs
			foreach($attrVal as $couple){
				
				//Affichage label attribut
				$out.="<div class='iblock vtop'>";
				$out.= "<label class='control-label'>".$couple['attr']['LABEL_ATTR']."</label>";
				$out.= "<select class='attr".$i."' id='".$couple['attr']['ID_ATTR']."'>";
				$out.= "<option selected='selected'>--</option>";
				
				//Affichage de la lite des options de valeurs présentes
				foreach ($couple['val'] as $val) {
					if(trim($val['VALUE']) != "")
						$out.= "<option>".$val['VALUE']."</option>";
				}
				
				$out.= "</select>";
				$out.="</div>";
				$i++;
			}
		
		}	
			
		return $out;	
	}
	
	
	/**
	 * Affiche les filtres de recherche dynamiques à partir d'un tableau contenant le couple
	 * Attribut et ses valeurs. Le but étant d'afficher les 3 attributs les plus utilisés dans la catégorie
	 * de produits actuelle. Et y référencer sous forme de liste pour chaque attributs les différentes valeurs
	 * existantes.
	 */
	public function showGrossisteFilter($attrVal)
	{
		$i=1;
		//Pour chaque couple attributs et ses valeurs
		foreach($attrVal as $couple){
			
			//Affichage label attribut
			$out.="<div class='iblock vtop'>";
			$out.= "<label>".$couple['attr']['LABEL_ATTR']."</label>";
			$out.= "<select class='attr".$i."' id='".$couple['attr']['ID_ATTR']."'>";
			$out.= "<option selected='selected'>--</option>";
			
			//Affichage de la lite des options de valeurs présentes
			foreach ($couple['val'] as $val) {
				
				$out.= "<option>".$val['VALUE']."</option>";
			}
			
			$out.= "</select>";
			$out.= "</div>";
			
			$i++;
		}
		
		return $out;
	}
	
	
	/**
	 * Affiche le filtre de marques pour la catégorie du produit. Prend en paramètre un tableau de marques
	 * appartenant à la catégorie. Il y a également affichage des deux filtres de prix, prix minimum et prix
	 * maximum
	 */
		public function showBrandFilter($brands, $type="")
	{
		//GROSSISTE
		if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
		//ECOMMERCANT	
		else{ $typev = "ecom";}
        
        if($type == "av")
            $outGP4.="<input id ='cB_all_Prods' name ='cB_all_Prods' type = 'checkbox' /><label for='cB_all_Prods'>Toute catégories</label>";
		
		!empty($_SESSION[$typev]['view']) ? $view = $_SESSION[$typev]['view'] : $view = "grid";
		$out = "<table>";
		//Si les marques existent pour cette catégorie
		if(!empty($brands) && $brands != null){
			
			$out.= "
					<tr>
					<td><select id='selectManufac'>
						<option selected= 'selected' >Choix de marque :</option>";
			
			foreach ($brands as $brand) {
				$out.= "<option value='".$brand['MANUFACTURER']."'>".$brand['MANUFACTURER']."</option>";	
			}
			$out.= " 	</select></td>
						<td>".$outGP1."</td><td>".$outGP4."</td>
					</tr>
					<tr>
						<td><span>de </span>  	
						<input type='text' class= 'mini min_price' onKeyPress='if (event.keyCode == 13) loadProdListFiltered".ucfirst($type)."(\"".$view."\")'>
						<span>€ à</span>
						<input type='text' class='mini max_price' onKeyPress='if (event.keyCode == 13) loadProdListFiltered".ucfirst($type)."(\"".$view."\")'></td>
						<td>".$outGP2."</td><td>".$outGP5."</td>
					</tr>
					<tr>
                    	<td><input type='text' class= 'search' placeholder='Recherche...' onKeyPress='if (event.keyCode == 13) loadProdListFiltered".ucfirst($type)."(\"".$view."\")' ></td>
                    	<td>".$outGP3."</td><td></td>
                    </tr>";

		}
		else{
			$out= "	<tr>
						<td><span><b>Pas de choix de marques</b></span></td>
						<td>".$outGP1."</td><td>".$outGP4."</td>
					</tr>
					<tr>
						<td><span>de </span>
                    	<input type='text' class= 'mini min_price' onKeyPress='if (event.keyCode == 13) loadProdListFiltered".ucfirst($type)."(\"".$view."\")'>
                    	<span>€ à</span>
                    	<input type='text' class='mini max_price' onKeyPress='if (event.keyCode == 13) loadProdListFiltered".ucfirst($type)."(\"".$view."\")'></td>
                    	<td>".$outGP2."</td><td>".$outGP5."</td>
                    <tr>
                    	<td><input type='text' class= 'search' placeholder='Recherche...' onKeyPress='if (event.keyCode == 13) loadProdListFiltered".ucfirst($type)."(\"".$view."\")'></td>
                    	<td>".$outGP3."</td>
                    </tr>	
                    	";	
		
		}
			$out.= "</table>";
        return $out;               
	}
	/**
	 * Affiche le filtre de marques pour la catégorie du produit. Prend en paramètre un tableau de marques
	 * appartenant à la catégorie. Il y a également affichage des deux filtres de prix, prix minimum et prix
	 * maximum GP
	 */
		public function showBrandFilterGP($brands, $type="")
	{
		$typev = "avaGP";
		
		!empty($_SESSION[$typev]['view']) ? $view = $_SESSION[$typev]['view'] : $view = "grid";
		//$out = "<table>";
		
		$vendorAssoc = $_SESSION[$typev]['listVendorAss'] ;
        
        $_SESSION['avaGP']['activeProducts'] == "true" ? $state = "checked" : $state = "";
        $outGP0.="<input id ='cB_all_Actif' type = 'checkbox' $state onChange='setActifSession(this); loadProdListFilteredGP(\"".$view."\"); return false;'/><label for='cB_all_Actif'> Actifs </label> ";
		$outGP1.="<input id ='cB_all_Assoc' type = 'checkbox' onChange='loadProdListFilteredGP(\"".$view."\"); return false;' /><label for='cB_all_Assoc'> Associés </label> ";
		$outGP2.="<input id ='cB_all_vendu' type = 'checkbox' /><label for='cB_all_vendu'> Vendus  par </label>";
		$outGP3.= 	"<select id = 'selectVendor'>
					<option selected = 'selectedIdVendor' >Choix de l'e-commerçant :</option>";
		foreach ($vendorAssoc as $vendor) {
			$outGP3.= "<option value ='".$vendor['vendor_id']."'>".$vendor['vendor_nom']."</option>";	
		}
		$outGP3.= "<option value ='24'>Soukeo</option>";
		$outGP3.= "<option value ='allvendor'>Tous les e-coms</option>";
		$outGP3.= "</select>";
		$outGP4.="<input id ='cB_all_Prods' type = 'checkbox' /><label for='cB_all_Prods'>Toute catégories</label>";
		$outGP5.="<input id ='cB_all_zero' type = 'checkbox' onChange='loadProdListFilteredGP(\"".$view."\"); return false;' /><label for='cB_all_zero' >Tous les poids à 0 </label>";
		//Si les marques existent pour cette catégorie
		if(!empty($brands) && $brands != null){
			
			$out.= "
					<div class='iblock vtop'><select id='selectManufac' onChange='loadProdListFilteredGP(\"".$view."\"); return false;' >
						<option selected= 'selected' >Choix de marque :</option>";
			
			foreach ($brands as $brand) {
				$out.= "<option value='".$brand['MANUFACTURER']."'>".$brand['MANUFACTURER']."</option>";	
			}
			$out.= " 	</select></div>
			
			
						<div class='iblock vtop'>
							<input type='text' class= 'search' placeholder='Recherche...' onKeyPress='if (event.keyCode == 13) loadProdListFilteredGP(\"".$view."\")' > 
						</div>
                    	
                    	<div class='iblock vtop'>
                    	<div class='style_checkbox'>
                    		<div class='iblock vtop'>".$outGP4 ."</div>
                    		<div class='iblock vtop'>".$outGP5 ."</div>
                    	</div>
                  		</div>
	
						<div class='iblock vtop'><span>de </span>  	
						<input type='text' class= 'mini min_price' onKeyPress='if (event.keyCode == 13) loadProdListFilteredGP(\"".$view."\")'>
						<span>€ à</span>
						<input type='text' class='mini max_price' onKeyPress='if (event.keyCode == 13) loadProdListFilteredGP(\"".$view."\")'>
						</div>
						
						<div class='style_checkbox'>
						<div class='iblock vtop'>".$outGP0 ."</div>
						<div class='iblock vtop'>".$outGP1 ."</div>
						<div class='iblock vtop'>". $outGP2 ."</div>
						<div class='iblock vtop'>". $outGP3 ."</div>
										
						</div>
					
                    	";

		}
		else{
			$out= "	<div class='iblock vtop'><b>Pas de choix de marques</b></div>
					<div class='iblock vtop'>
						<input type='text' class= 'search' placeholder='Recherche...' onKeyPress='if (event.keyCode == 13) loadProdListFilteredGP(\"".$view."\")'>
                    </div>
			
					<div class='iblock vtop'>
                    	<div class='style_checkbox'>
                    		<div class='iblock vtop'>".$outGP4 ."</div>
                    		<div class='iblock vtop'>".$outGP5 ."</div>
                    	</div>
                  		</div>
					
					<div class='iblock vtop'>
						<span>de </span>
                    	<input type='text' class= 'mini min_price' onKeyPress='if (event.keyCode == 13) loadProdListFilteredGP(\"".$view."\")'>
                    	<span>€ à</span>
                    	<input type='text' class='mini max_price' onKeyPress='if (event.keyCode == 13) loadProdListFilteredGP(\"".$view."\")'>
                    </div>
                    	
                    	
                    	<div class='style_checkbox'>
                    	<div class='iblock vtop'>".$outGP0 ."</div>
						<div class='iblock vtop'>".$outGP1 ."</div>
						<div class='iblock vtop'>". $outGP2 ."</div>
						<div class='iblock vtop'>". $outGP3 ."</div>
										
						</div>
                    	
                    	";	
		
		}
			//$out.= "</table>";
        return $out;               
	}	
	
	/**
	 * Cette fonction est lié aux pop-up d'association de catégories et au pop up d'ajout d'une fiche produit
	 * à partir d'un produit ecommercant. Cette fonction va permettre d'afficher ssou forme de tableau
	 * la liste des attributs que l'ecommercant possède pour ce produit(ou cette catégorie) et les mettre en parallèle
	 * avec des listes déroulantes des attributs Avahis connus pour la catégorie dans laquelle le produit est
	 * associé. Si un mapping a déja été effectué les liste de selection devront s'afficher avec les préférences
	 * préselectionnées.
	 */
	public function showAttributsEcomAvPopup($attrListProd, $attrListAvCat, $vendor_id = "", $cat_ecom = "", $cat_av = "")
	{
		$db = new BusinessSoukeoModel();
		$out ="";
		
		//Pour chaque attributs de l'ecommercant, on va mettre en parallèle les attr Avahis
		foreach ($attrListProd as $ap) {
			
			//On va chercher si un mapping a déja été effectué pour cet attribut e commercant
			//Si aucun mapping n'a été enregisstré id_attr_av vaudra false
			$id_attr_av = $db -> getMappingForAttrEcom($ap['ID_ATTR'], $vendor_id, $cat_ecom, $cat_av);
			
			$out .= '<tr>
						<td>'.$ap['LABEL_ATTR'].' : </td>';
			
			
			$out .=    '<td>
							<select id="ecom_'.$ap['ID_ATTR'].'" class="av_attr '.$ap['LABEL_ATTR'].'">
								<option value="" selected disabled >Attribut correspondant...</option>';
			
			//Pour chaque attributs avahis pour la catégorie
			foreach ($attrListAvCat as $a) {
				
				//Si le mapping correpond on préselectionne l'option
				if($a['id_attribut'] == $id_attr_av){
					$out.="			<option value='".$a['id_attribut']."' selected>".$a['label_attr']."</option>";
					
				}else{
					$out.="			<option value='".$a['id_attribut']."'>".$a['label_attr']."</option>";	
				}				
			}
			
			//Fin HTML avec ajout de liens pour supprimer l'attribut et un lien pour ajouter un attribut
			//A la liste d'option au cas où l'utilisateur ne trouverait pas de correpondance
			$out .='		</select>
							<a id="add_'.$ap['ID_ATTR'].'" href="#" onclick="addAttrPopUp(this);return false;">+ Ajouter</a>
						</td>';
			
			$out .= '<td>'.$ap['VALUE'].'</td>';
			
			$out.='			<td><a class="supprAttr" href="#" onclick="supprLigne(this); return false;">Supprimer</a></td>
					</tr>';		
		}

		return $out;			
	}

    public function showCreditsPackTable($packList)
    {
        $out = '<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° </th>
                                <th>Libellé</th>
                                <td>Valeur Crédits</td>
                                <td>Prix </td>
                                <td>Action </td>
                            </tr>
                        </thead>
                        
                        <tbody>';
        foreach ($packList as $pack) {
            $out .= '       <tr>
                                <td>'.$pack -> getPack_id().'</td>
                                <td>'.$pack -> getLabel().'</td>
                                <td>'.$pack -> getValue().'</td>
                                <td>'.$pack -> getPrice().' €</td>
                                <td>
                                    <button class="btn" onClick="showDialogEditPackCredit('.$pack -> getPack_id().'); return false;"> Modifier </button>
                                    <button class="btn btn-danger" onClick="showDialogSupprPackCredit('.$pack -> getPack_id().'); return false;"> Suppr </button>
                                </td>
                            </tr>';
        }
        
        $out .= '       </tbody>
                </table>';
        return $out;
    }

    public function showLivraisonTypesTable($livraisonTypeList)
    {
        $out = '<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° </th>
                                <th>Code</th>
                                <td>Temps de livraison</td>
                                <td>Remboursé</td>
                                <td>Action </td>
                            </tr>
                        </thead>
                        
                        <tbody>';
        foreach ($livraisonTypeList as $livraisonType) {
            
            $livraisonType -> getRefund() == 1 ? $refund = "<span class='label label-important'>Oui</span>" : $refund = "Non";
            
            $out .= '       <tr>
                                <td>'.$livraisonType -> getShipping_id().'</td>
                                <td>'.$livraisonType -> getShipping_code().'</td>
                                <td>'.$livraisonType -> getDays_in_transit().' Jour(s)</td>
                                <td>'.$refund.'</td>
                                <td>
                                    <button class="btn" onClick="showDialogEditLivraisonType('.$livraisonType -> getShipping_id().'); return false;"> Modifier </button>
                                </td>
                            </tr>';
        }
        
        $out .= '       </tbody>
                </table>';
        return $out;        
    }

    public function showPaymentTypesTable($paymentList){ 
        
        $out = '<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° </th>
                                <th>Libellé</th>
                                <td>Action </td>
                            </tr>
                        </thead>
                        
                        <tbody>';
        foreach ($paymentList as $payment_type) {
            $out .= '       <tr>
                                <td>'.$payment_type -> getPayment_type_id().'</td>
                                <td>'.$payment_type -> getLabel().'</td> 
                                <td>
                                    <button class="btn" onClick="showDialogEditPaymentType('.$payment_type -> getPayment_type_id().'); return false;"> Modifier </button>
                                    <button class="btn btn-danger" onClick="showDialogSupprPaymentType('.$payment_type -> getPayment_type_id().'); return false;"> Suppr </button>
                                </td>
                            </tr>';
        }
        
        $out .= '       </tbody>
                </table>';
        return $out;        
    }

    public function showCustStatusTable($custStatusList){ 
        
        $out = '<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° </th>
                                <th>Libellé</th>
                                <td>Action </td>
                            </tr>
                        </thead>
                        
                        <tbody>';
        foreach ($custStatusList as $cust_status) {
            $out .= '       <tr>
                                <td>'.$cust_status -> getStatut_id().'</td>
                                <td>'.$cust_status -> getLabel().'</td> 
                                <td>
                                    <button class="btn" onClick="showDialogEditCustStatus('.$cust_status -> getStatut_id().'); return false;"> Modifier </button>
                                    <button class="btn btn-danger" onClick="showDialogSupprCustStatus('.$cust_status -> getStatut_id().'); return false;"> Suppr </button>
                                </td>
                            </tr>';
        }
        
        $out .= '       </tbody>
                </table>';
        return $out;        
    }

    public function showVendorStatusTable($vendorStatusList){ 
        
        $out = '<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>N° </th>
                                <th>Libellé</th>
                                <td>Action </td>
                            </tr>
                        </thead>
                        
                        <tbody>';
        foreach ($vendorStatusList as $vendor_status) {
            $out .= '       <tr>
                                <td>'.$vendor_status -> getStatut_id().'</td>
                                <td>'.$vendor_status -> getLabel().'</td> 
                                <td>
                                    <button class="btn" onClick="showDialogEditVendorStatus('.$vendor_status -> getStatut_id().'); return false;"> Modifier </button>
                                    <button class="btn btn-danger" onClick="showDialogSupprVendorStatus('.$vendor_status -> getStatut_id().'); return false;"> Suppr </button>
                                </td>
                            </tr>';
        }
        
        $out .= '       </tbody>
                </table>';
        return $out;        
    }


	public function showCustomerInfo($customer, $orderList)
	{
	    $ticketManager = new ManagerTicket();
        
		$out ="";
		$grid = new OrdersGrid($customer->getCustomer_id(), "", "", "", "", "all", true);
        
        $out.= '
                    <button class="btn" onclick="javascript:window.history.go(-1);"><i class="fa fa-arrow-left"></i></button>
                    <button class="btn pull-right" onclick="self.location.href=&quot;/app.php/dashboard &quot;"><i class="fa fa-dashboard"> Tableau de bord</i></button>
                    <span id="vendor_messagebox"></span>
                    <span id="ticket_messagebox"></span>
                  ';

        //INFOS
		$out .= "
		          <div class='row-fluid'>";
        
		$out.= "	<div class='span3 well' id='infoClient'>"
		              .$this -> ficheInfoClient($customer, $orderList).
		            "</div>";
        
        //DEMANDES
        $out.="     <div class='span9 well'>
                        <h4>Demandes client : <button class='pull-right btn btn-primary' value='Créer' onClick='showDialogAddTicket(".$customer->getEntity_id().", \"customer\");';><i class='fa fa-ticket icon-black' style='padding-right: 0px'></i> Créer ticket</button></h4>
                        <div id ='ticket_list_client'>";
                        
        $ticketList = $ticketManager -> getAll(array("ticket_entity_id" => $customer->getEntity_id()) );
        
        $out .= $this -> getTicketsListTable($ticketList, $customer->getEntity_id());
        $out.= "        </div>
                    </div>
                ";
                
        //COMMANDES        
 		$out.=" 
 		     	<div class='span9 well'>
  						<h4>Commandes passées : </h4>
  						<table class='table table-bordered paginated'>
							<caption></caption>
						  	<thead>
						    	<tr>
						    		<th></th>
						      		<th>N°Commande</th>
						      		<th>Date</th>
						      		<th>Total</th>
						      		<th>Etat (State)</th>
						      		<th>Addresse livrée:</th>
						    	</tr>
						  	</thead>
						  	<tbody>";

  		foreach ($orderList as $order) {
  			
            if($order->getOrder_state() == "canceled" || $order->getOrder_state() == "closed" ){
                $buttonOpen = "item";
            }else{
                $buttonOpen = "com";
            }
  			$address = new BusinessAddress($order->getShipping_address_id());
					  $out .= "<tr>
					  				<td><input type='button' class='closed btn' value='+' id='".$buttonOpen."_".$order->getOrder_id()."'  onclick='showChild(this)'  /></td>
					  				<td> ".$order->getIncrement_id()."</td>
					  				<td> ".smartDate($order->getDate_created())."</td>
					  				<td> ".$order->getBase_grand_total()."</td>
					  				<td> ".getLabelStatus($order->getOrder_state())."</td>
					  				<td><b>".htmlentities($address->getStreet1());
			if($address->getStreet2()) 
				$out.=						" ".htmlentities($address->getStreet2());
			$out.= 							"</br>".htmlentities($address->getPostcode())." ".htmlentities($address->getCity()).
					  						"</br>".htmlentities($address->getCountry())."</b></td>
					  		   </tr>	";
		}
				
  		$out .= "		     </tbody>
  		                </table>
  					</div>";
		$out .= "</div>";
        
		$out .= "<div class='row-fluid'>
		              <div class='span12'>
		                  <h4>Litiges Client : </h4>
                      </div>
		         </div>
		         <script type='text/javascript'>
		              $(document).keyup(function(e) {
                        if (e.keyCode == 27) { javascript:window.history.go(-1); }
                      });
		         </script>";
		
		return $out;
	}

	
    public function ficheInfoClient($customer, $orderList)
    {
        $custStatusManager = new ManagerCustomerStatut();
        $statusList = $custStatusManager -> getAll();
         
        $customer -> getCustomer_id() ? $idAvahis = $customer -> getCustomer_id() : $idAvahis = "<em>Non inscrit</em>";
        
        if($customer -> getCustomer_id() > 400000) $idAvahis.= " <span class='label label-important'>GUEST</span>";
        
        $out = "<h4>N°".$customer->getEntity_id()." | ".htmlentities(strtoupper($customer -> getFirstname()))." ".htmlentities(strtoupper($customer -> getLastname()))."<br/>ACHETEUR</h4>
                        <table class='table'>
                        <tr><td>Id Avahis : </td><td>".$idAvahis."</td></tr>";
                        
        $out .= "       <tr><td>Statut : </td><td>
                            <select class='input input-mediumsmall' id='select_statut_customer' onChange='editStatutCustomer(".$customer->getEntity_id()."); return false;'>";
                            
        foreach ($statusList as $status) {
            $out.= "<option value='".$status->getStatut_id()."' ";
            if($status->getStatut_id() == $customer -> getStatut() ){
                $out.= " selected ";    
            }
            $out .= " >".$status->getLabel()."</option>";
        }
        $countOrder = 0;
        
        
        foreach ($orderList as $order) {
            if(strtolower(trim($order->getOrder_state())) == "complete"){
                $countOrder ++;
            }    
        }

        $out .=        "<tr><td>Email : </td><td>".$this->getEditableAjaxInput("email", "text", $customer->getEntity_id(), "customer", htmlentities($customer->getEmail()) )."</td></tr>
                        <tr><td>Tel 1 : </td><td>".$this->getEditableAjaxInput("phone", "text", $customer->getEntity_id(), "customer", htmlentities($customer->getPhone()) )."</td></tr>
                        <tr><td>Tel 2 : </td><td>".$this->getEditableAjaxInput("phone2", "text", $customer->getEntity_id(), "customer", htmlentities($customer->getPhone2()) )."</td></tr>
                        <tr><td>Date de naissance: </td><td><span id='customer_birthdate'>".smartDateNoTime($customer->getBirthd())."</span></td></tr>
                        <tr><td>Nbr de commande : </td><td><strong id='customer_countorder'>".$countOrder."</strong></td></tr>
                        <tr><td>CA : </td><td><strong id='customer_ca'>".$customer->getCA()." €</strong></td></tr>
                        <tr><td>Nbr de litiges : </td><td><span id='customer_statut'>0</span></td></tr>
                        <tr><td>Date d'inscription : </td><td><span id='customer_date_created'>".smartDate($customer -> getDate_created())."</span></td></tr>
                        <tr><td>Autres informations : </td><td>".$this->getEditableAjaxInput("more_info", "text", $customer->getEntity_id(), "customer", htmlentities($customer->getMore_info()) )."</td></tr>
                        </table>
                        <button class='btn btn-primary' onClick='showFormEditClient(".$customer->getEntity_id()."); return false;'><i class='fa fa-pencil icon-black' style='padding-right: 0px'></i> Tout Editer</button>";
        return $out;
    }


    public function ficheInfoClientVendor($vendor, $orderList)
    {
        $categorieManager = new ManagerCategorie();
        $vendorCategManager = new ManagerVendorCategorie();
        //var_dump($categorieManager -> getAllUnivers());
        $vendorTypeManager = new ManagerVendorType();
        $vendorStatutManager = new ManagerVendorStatut();
        $typeList = $vendorTypeManager -> getAll();
        $statusList = $vendorStatutManager -> getAll(); 
        
        
        $vendor_univers_list = $vendorCategManager -> getAll(array("entity_id" => $vendor -> getEntity_id()));
        $str_univerlist = "";
        foreach ($vendor_univers_list as $vend_cat) {
            $cat = $categorieManager -> getById(array("cat_id" => $vend_cat -> getCat_id()));
            $str_univerlist .= " - ". $cat -> getCat_label();
        }
        
        $univers = "<span id='vendorcateg_".$vendor -> getEntity_id()."'>$str_univerlist<a class='pull-right' href='#' onClick='setVendorCateg(this); return false;' style='margin-right:5px'>
                                    <img class='iconAction' border='0' align='absmiddle' src='/skins/common/images/picto/edit.gif' title='Editer' alt='Editer'>
                                </a></span>";
        $vendor -> getVendor_id() ? $idAvahis = $vendor -> getVendor_id() : $idAvahis = "<em>Non inscrit</em>";
        
        if($vendor_type = $vendorTypeManager -> getById(array("type_id" => $vendor -> getVendor_type() ))){
            $type = $vendor_type -> getLabel();
        }else{
            $type = "<em>N/C</em>";
        }
        $out = "<h4>N°".$vendor->getEntity_id()." | ".htmlentities(strtoupper($vendor -> getVendor_attn()))." ".htmlentities(strtoupper($vendor -> getVendor_attn2()))."<br/>VENDEUR</h4>
                        <table class='table table-hover'>
                         <tr><td>Entreprise : </td><td>".$this->getEditableAjaxInput("vendor_nom", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getVendor_nom()) )."</td></tr>
                        <tr><td>Id Avahis : </td><td>".$idAvahis."</td></tr>";
        if($vendor -> getVendor_id()){
            $out .= "   <tr><td>Crédits : </td><td>".$vendor->getSolde_credit()
                        .' <a href="#" class="pull-right btn" onclick="showDialogEditCredits('.$vendor -> getVendor_id().'); return false;"><img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Ajouter crédits" alt="Ajouter crédits"></a>'
                        ." </td></tr>";
            $manager = new ManagerAbonnement();
                        
            $out .= "   <tr><td>Abonnement : </td><td>".getBtnAbonnement($vendor -> getAbonnement(), $vendor -> getVendor_id())."</td></tr>";            
        }
        $out .= "       <tr><td>Type : </td><td>
                            <select id='select_type_vendor' class='input input-mediumsmall' onChange='editTypeVendor(".$vendor->getEntity_id()."); return false;'>";
        foreach ($typeList as $type) {
            $out.= "<option value='".$type->getType_id()."' ";
            if($type->getType_id() == $vendor -> getVendor_type() ){
                $out.= " selected ";    
            }
            $out .= " >".$type->getLabel()."</option>";
        }
        $out .=         "   </select></td></tr>";
        
        $out .= "       <tr><td>Statut : </td><td>
                            <select id='select_statut_vendor' class='input input-mediumsmall' onChange='editStatutVendor(".$vendor->getEntity_id()."); return false;'>";
        foreach ($statusList as $status) {
            $out.= "<option value='".$status->getStatut_id()."' ";
            if($status->getStatut_id() == $vendor -> getStatut() ){
                $out.= " selected ";    
            }
            $out .= " >".$status->getLabel()."</option>";
        }
        $out .=         "   </select></td></tr>";
        
        foreach ($orderList as $po) {
            $order = new BusinessOrder($po->getOrder_id());
            if(strtolower(trim($order->getOrder_state())) == "complete"){
                $countOrder ++;
            }    
        }
        
        $out .= "       <tr>
                            <td>Modes de livraison :</td>
                            <td>".$vendor->getAllShippingMethod()."</td>
                        </tr>";     
        
        $out .= "       <tr><td>SIRET : </td><td>".$this->getEditableAjaxInput("siret", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getSiret()) )."</td></tr>";

        $out .= "       <tr><td>Nom : </td><td>".$this->getEditableAjaxInput("vendor_attn", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getVendor_attn()) )."</td></tr>
                        <tr><td>Prénom : </td><td>".$this->getEditableAjaxInput("vendor_attn2", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getVendor_attn2()) )."</td></tr>
                        <tr><td>Email : </td><td>".$this->getEditableAjaxInput("email", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getEmail_str()) )."</td></tr>
                        <tr><td>Tel 1 : </td><td>".$this->getEditableAjaxInput("telephone", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getTelephone()) )."</td></tr>
                        <tr><td>Tel 2 : </td><td>".$this->getEditableAjaxInput("fax", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getTelephone2()) )."</td></tr>
                        <tr><td>Rue : </td><td>".$this->getEditableAjaxInput("street", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getStreet()) )."</td></tr>
                        <tr><td>Ville : </td><td>".$this->getEditableAjaxInput("city", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getCity()) )."</td></tr>
                        <tr><td>Zip : </td><td>".$this->getEditableAjaxInput("zip", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getZip()) )."</td></tr>
                        <tr><td>Pays : </td><td>".$this->getEditableAjaxInput("country_id", "text", $vendor->getEntity_id(), "vendor", htmlentities($vendor->getCountry_id()) )."</td></tr>
                        <tr><td>Univers : </td><td>$univers</td></tr>
                        <tr><td>Date de naissance : </td><td>".$this->getEditableAjaxInput("birthd", "text", $vendor->getEntity_id(), "vendor", htmlentities(smartDateNoTime($vendor->getBirthd())) )."</td></tr>
                        <tr><td>Nbr de commande (complètes) : </td><td><span id='customer_countorder'>".$countOrder."</span></td></tr>
                        <tr><td>CA : </td><td><span id='customer_ca'>".$vendor->getCA()." €</span></td></tr>
                        <tr><td>Nbr de litiges : </td><td><span id='customer_statut'>0</span></td></tr>
                        <tr><td>Date d'inscription : </td><td><span id='customer_date_created'>".smartDateNoTime($vendor -> getCreated_at())."</span></td></tr>
                        </table>
                        <button class='btn btn-primary' onClick='showFormEditVendor(".$vendor->getEntity_id()."); return false;'><i class='fa fa-pencil icon-black' style='padding-right: 0px'></i> Tout Editer</button>";
        return $out;
    }


    public function getFormEditCustomer($customer, $statutList)
    {
        $out = "<h4>N°".$customer->getEntity_id()." | ".strtoupper(htmlentities($customer -> getFirstname()))." ".strtoupper(htmlentities($customer -> getLastname()))."</h4><hr/>";
        
        $out .= '<form class="form-horizontal" id="form_add_ticket">
      
                    <div class="control-group">
                        <label class="control-label" for="edit_cust_statut">Statut :</label>
                        <div class="controls">
                            <select id="edit_cust_statut">';
         foreach($statutList as $statut){ 
            $out .= '<option value="'.$statut -> getStatut_id().'">'.$statut -> getLabel().'</option>';
         }
                            
        $out.= '            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="edit_cust_mail">Email :</label>
                        <div class="controls">
                            <input type="text" id="edit_cust_mail" placeholder="Mail..." name="edit_cust_mail" value="'.htmlentities($customer -> getEmail()).'">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="edit_cust_tel1">Tel 1 :</label>
                        <div class="controls">
                            <input type="text" id="edit_cust_tel1" placeholder="Tel..." name="edit_cust_tel1" value="'.htmlentities($customer -> getPhone()).'">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="edit_cust_tel2">Tel 2 :</label>
                        <div class="controls">
                            <input type="text" id="edit_cust_tel2" placeholder="Tel 2..." name="edit_cust_tel2" value="'.htmlentities($customer -> getPhone2()).'">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="edit_cust_birthd">Date de Naissance :</label>
                        <div class="controls">
                            <input type="text" id="edit_cust_birthd" placeholder="Date..." name="edit_cust_birthd" value="'.htmlentities($customer -> getBirthd()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_cust_more_info">Autres informations :</label>
                        <div class="controls">
                            <textarea id="edit_cust_more_info" placeholder="Informations complémentaires..." name="edit_cust_more_info" >'.htmlentities($customer -> getMore_info()).'</textarea>
                        </div>
                    </div>  
                    
                     <button class="btn btn-primary" onClick="validFormEditCustomer('.$customer->getEntity_id().');return false;" >Enregistrer</button>
                     <button class="btn btn-inverse" onClick="exitFormEditCustomer('.$customer->getEntity_id().');return false;">Annuler</button>
                </form>
                <script>
                  $(function() {
                    $( "#edit_cust_birthd" ).datepicker({
                      changeMonth: true,
                      changeYear: true,
                      defaultDate: "-20y",
                      dateFormat : "yy-mm-dd",
                      showOn: "button",
                      buttonImage: "../../skins/common/images/picto/grid_ecom.png",
                      
                    }); 
                  });
               </script>';   
       return $out;   
    }


    public function getFormEditVendor($vendor, $statutList)
    {
        $out = "<h4>N°".$vendor->getEntity_id()." | ".htmlentities(strtoupper($vendor -> getVendor_attn()))." ".htmlentities(strtoupper($vendor -> getVendor_attn2()))."</h4><hr/>";

        $out .= '<form class="form-horizontal" id="form_add_ticket">

                    
                    <div class="control-group">
                        <label class="control-label" for="edit_vendor_nom">Entreprise :</label>
                        <div class="controls">
                            <input type="text" id="edit_vendor_nom" placeholder="Siret..." name="edit_vendor_nom" value="'.htmlentities($vendor -> getVendor_nom()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="VendorType">Type :</label>
                        <div class="controls">
                            '.BusinessReferentielUtils::getSelectList("VendorType", "Type de vendeur...", $vendor->getVendor_type()).'
                        </div>
                    </div>  
                    
                    <div class="control-group">
                        <label class="control-label" for="VendorStatut">Statut :</label>
                        <div class="controls">
                            '.BusinessReferentielUtils::getSelectList("VendorStatut", "Statut...", $vendor->getStatut()).'
                        </div>
                    </div>
                                     
                    <div class="control-group">
                        <label class="control-label" for="edit_siret">Siret :</label>
                        <div class="controls">
                            <input type="text" id="edit_siret" placeholder="Siret..." name="edit_siret" value="'.htmlentities($vendor -> getSiret()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_vendor_attn">Nom :</label>
                        <div class="controls">
                            <input type="text" id="edit_vendor_attn" placeholder="Nom..." name="edit_vendor_attn" value="'.htmlentities($vendor -> getVendor_attn()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_vendor_attn2">Prénom :</label>
                        <div class="controls">
                            <input type="text" id="edit_vendor_attn2" placeholder="Prénom..." name="edit_vendor_attn2" value="'.htmlentities($vendor -> getVendor_attn2()).'">
                        </div>
                    </div>
                        
                    <div class="control-group">
                        <label class="control-label" for="edit_email">Email :</label>
                        <div class="controls">
                            <input type="text" id="edit_email" placeholder="Mail..." name="edit_email" value="'.htmlentities($vendor -> getEmail_str()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_telephone">Tel 1 :</label>
                        <div class="controls">
                            <input type="text" id="edit_telephone" placeholder="Tel..." name="edit_telephone" value="'.htmlentities($vendor -> getTelephone()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_telephone2">Tel 2 :</label>
                        <div class="controls">
                            <input type="text" id="edit_telephone2" placeholder="Tel 2..." name="edit_telephone2" value="'.htmlentities($vendor -> getTelephone2()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_street">Rue :</label>
                        <div class="controls">
                            <input type="text" id="edit_street" placeholder="Rue..." name="edit_street" value="'.htmlentities($vendor -> getStreet()).'">
                        </div>
                    </div>
                        
                    <div class="control-group">
                        <label class="control-label" for="edit_city">Ville :</label>
                        <div class="controls">
                            <input type="text" id="edit_city" placeholder="Ville..." name="edit_city" value="'.htmlentities($vendor -> getCity()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_zip">Code postal :</label>
                        <div class="controls">
                            <input type="text" id="edit_zip" placeholder="Code postal..." name="edit_zip" value="'.htmlentities($vendor -> getZip()).'">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="edit_country_id">Pays :</label>
                        <div class="controls">
                            <input type="text" id="edit_country_id" placeholder="Pays..." name="edit_country_id" value="'.htmlentities($vendor -> getCountry_id()).'">
                        </div>
                    </div>
                                        
                    <div class="control-group">
                        <label class="control-label" for="edit_birthd">Date de naissance :</label>
                        <div class="controls">
                            <input type="text" id="edit_birthd" placeholder="Date de naissance..." name="edit_birthd" value="'.htmlentities($vendor -> getBirthd()).'">
                        </div>
                    </div>                     
                                        
                     <button class="btn btn-primary" onClick="validFormEditVendor('.$vendor->getEntity_id().');return false;" >Enregistrer</button>
                     <button class="btn btn-inverse" onClick="exitFormEditVendor('.$vendor->getEntity_id().');return false;">Annuler</button>
                </form>
                <script>
                  $(function() {
                    $( "#edit_birthd" ).datepicker({
                      changeMonth: true,
                      changeYear: true,
                      defaultDate: "-20y",
                      dateFormat : "yy-mm-dd",
                      showOn: "button",
                      buttonImage: "../../skins/common/images/picto/grid_ecom.png",
                      
                    }); 
                  });
               </script>';   
       return $out;   
    }

	
	public function getMiniCustomersListTable($customerList, $filter_id=null, $filter_name=null)
	{
		$customerManager        = new ManagerCustomer();
        
        $out ="<form id='searchCustomer'>
                 <input type='text' id='search_customer_id'   placeholder='Id Interne...' name='search_customer_id' value='$filter_id'>
                 <input type='text' id='search_customer_name' placeholder='Nom ou prénom...' name='search_customer_name' value='$filter_name'>
                 <button class='btn' onClick='searchCustomer(); return false;'><i class='fa fa-search icon-black' style='padding-right: 0px' ></i></button>
                 <button class='btn btn-primary pull-right' onClick='showDialogAddCustomer(); return false;'><i class='fa fa-add icon-black' style='padding-right: 0px' ></i>&nbsp;&nbsp;&nbsp;&nbsp;Créer acheteur</button>
               </form>
                  <table class='table table-bordered ' id='table-minicustomerlist'>
                    <caption></caption>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Email</th>
                            <th>Id Avahis</th>
                        </tr>
                    </thead>
                    <tbody>";
        
        foreach ($customerList as $customer) {
            
            $customer->getCustomer_id() ? $idAvahis = $customer->getCustomer_id() : $idAvahis = "<em>Non inscrit</em>";
            $out.= "<tr>";
                
            $out.= "<td><input type='button' class='closed btn' value='+' id='".$customer->getEntity_id()."'  onclick='self.location.href=\"clients/client?type=customer&id=".$customer->getEntity_id()."\"'  /></td>";
            $out.= "<td>".htmlentities($customer->getFirstname())."</td>";
            $out.= "<td>".htmlentities($customer->getLastname())."</td>";
            $out.= "<td>".htmlentities($customer->getEmail())."</td>";
            $out.= "<td>".$idAvahis."</td>";
            
            $out.= "</tr>";
        }
        
        $out .=    "</tbody>
                  </table>";
        
        return $out;
	}
	

    public function getMiniVendorsListTable($vendorList, $filter_id=null, $filter_name=null)
    {
        $vendorManager        = new ManagerVendor();
        
        $out ="<form id='searchCustomer'>
                 <input type='text' id='search_vendor_id'   placeholder='Id Interne...' name='search_vendor_id' value='$filter_id'>
                 <input type='text' id='search_vendor_name' placeholder='Nom ou entreprise' name='search_vendor_name' value='$filter_name'>
                 <button class='btn' onClick='searchVendor(); return false;'><i class='fa fa-search icon-black' style='padding-right: 0px' ></i></button>
                 <button class='btn btn-primary pull-right' onClick='showDialogAddVendor(); return false;'><i class='fa fa-add icon-black' style='padding-right: 0px' ></i>&nbsp;&nbsp;&nbsp;&nbsp;Créer vendeur</button>
               </form> 
                  <table class='table table-bordered' id='table-minivendorlist'>
                    <caption></caption>
                    <thead>
                        <tr>
                            <th style='width:5%'></th>
                            <th style='width:10%'>Entreprise</th>
                            <th style='width:10%'>Nom</th>
                            <th style='width:10%'>Prénom</th>
                            <th style='width:10%'>Email</th>
                            <th style='width:5%'>Id Avahis</th>
                        </tr>
                    </thead>
                    <tbody>";
        
        foreach ($vendorList as $vendor) {
            
            $vendor->getVendor_id() ? $idAvahis = $vendor->getVendor_id() : $idAvahis = "<em>Non inscrit</em>";
            $out.= "<tr>";
                
            $out.= "<td><input type='button' class='closed btn' value='+' id='".$vendor->getEntity_id()."'  onclick='self.location.href=\"clients/client?type=vendor&id=".$vendor->getEntity_id()."\"' /></td>";
            $out.= "<td>".htmlentities($vendor->getVendor_nom())."</td>";
            $out.= "<td>".htmlentities($vendor->getVendor_attn())."</td>";
            $out.= "<td>".htmlentities($vendor->getVendor_attn2())."</td>";
            $out.= "<td>".htmlentities($vendor->getEmail_str())."</td>";
            $out.= "<td>".$idAvahis."</td>";
            $out.= "</tr>";
        }
        
        $out .=    "</tbody>
                  </table>";
        
        return $out;
    }
    
    
    public function getNewTicketListTable($ticketList)
    {
        $ticketPriorityManager  = new ManagerTicketPriority();
        $ticketStateManager     = new ManagerTicketState();
        $entityManager          = new ManagerEntityCrm();
        $entityTypeManager      = new ManagerEntityType();
        $customerManager        = new ManagerCustomer();
        $vendorManager          = new ManagerVendor();
        $userManager            = new ManagerAppUser();
        
        $stateList = $ticketStateManager -> getAll();
        $priorityList = $ticketPriorityManager -> getAll();
        
        $out .= "<table class='table table-bordered'>
                    <caption></caption>
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Statut</th>
                            <th>Priorité</th>
                            <th>Date</th>";
        $out.=             "<th>Client</th>";
        $out.=             "<th>Titre</th>
                            <th style='width:350px'>Texte</th>
                            <th>Assigné à</th>
                            <th>Historique</th>
                            <th>Crée par</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>";
        foreach ($ticketList as $ticket) {
            
            if($creator = $userManager -> getById(array("user_id" => $ticket -> getTicket_creator_id()))){
                $creator_name = htmlentities($creator -> getName());
            }else{
                $creator_name = "Client";
            }
            
                
            if($ticket -> getTicket_entity_id()){
                $entity = $entityManager -> getById(array("entity_id" => $ticket -> getTicket_entity_id()));
                $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
                
                if($type -> getLabel() == "customer"){
                    $customer = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                    $ticketClient = "<a href='clients/client?type=customer&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($customer->getName())."</a> (A)";
                }elseif($type -> getLabel() == "vendor"){
                    $vendor = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                    $ticketClient = "<a href='clients/client?type=vendor&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($vendor->getVendor_nom())."</a> (V)"; 
                }
                
            }else{
                $ticketClient = "<em> Global </em>";
            } 
            
            $comment = "";
            if(count($ticket -> getHistory())>0) $style_red= "style='color:red'";
            else $style_red= "";
            $comment .= "<a class='closed btn' href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' id='comment_ticket_".$ticket -> getTicket_id()."' >
                     <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i><small $style_red >".count($ticket -> getHistory())."</small></a>";
            
            //assignation et relance         
            $action = "<a class='btn' href='#' id='http://".$_SERVER['HTTP_HOST']."/app.php/ticket_".$ticket -> getTicket_id()."' onclick='deleteIt(\"ticket_".$ticket -> getTicket_id()."\"); return false;'>
                     <i class='fa fa-trash ' style='padding-right: 0px' ></i></a>";
            $state = $ticketStateManager -> getById(array("state_id" => $ticket -> getTicket_state_id()));
            $priority = $ticketPriorityManager -> getById(array("priority_id" => $ticket -> getTicket_priority_id()));
            
            $date_create = date_create($ticket -> getDate_created());
            
            $ticket->getAssigned_user() ? $assigned_user = $ticket->getAssigned_user()->getName() : $assigned_user = " - ";
            
            $out .= "<tr>
                        <td><a href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' >".$ticket -> getTicket_code()."</a></td>
                        <td>".getLabelStateTicket($state -> getLabel())."</td>
                        <td>".getLabelPriorityTicket($priority -> getLabel())."</td>
                        <td>".date_format($date_create, 'd/m/Y')."</td>";
            $out .=    "<td>".$ticketClient."</td>";
            $out .=    "<td><a href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' >".htmlentities($ticket -> getTicket_title())."</a></td>
                        <td>".$this -> truncateTextHide(strip_tags($ticket -> getTicket_text()), 50)."</td>
                        <td>".$assigned_user."</td>
                        <td>$comment</td>
                        <td>".$creator_name."</td>
                        <td>$action</td>";
            $out .= "</tr>";
        }
        $out .= "   </tbody>
                </table>";
        
        return $out;
    }	
    public function getTicketsListTable($ticketList, $entity_id="", $user_id="", $filter_name=null, $filter_priority = null, $filter_state = null)
    {
        global $root_path;
        
        $ticketPriorityManager  = new ManagerTicketPriority();
        $ticketStateManager     = new ManagerTicketState();
        $entityManager          = new ManagerEntityCrm();
        $entityTypeManager      = new ManagerEntityType();
        $customerManager        = new ManagerCustomer();
        $vendorManager          = new ManagerVendor();
        $userManager            = new ManagerAppUser();
        $relanceManager         = new ManagerRelance();
        
        $stateList = $ticketStateManager -> getAll();
        $priorityList = $ticketPriorityManager -> getAll();
        
        if($entity_id)
            $add_id = "_cli";
        //FILTERS
        //FILTRE N°
        //$out = "<form><input type='text' id='search_ticket_code' placeholder='N°' name='search_ticket_code' value='$filter_ticket_code'>";
        $out = "<form><input type='text' id='search_ticket_client_name$add_id' placeholder='Nom client..' name='search_ticket_client_name$add_id' value='$filter_name'>";
        
        //FILTRE Statut
        $out.= "<select id='search_ticket_statut$add_id'>
                            <option value='0'>Statut...</option>";
        foreach ($stateList as $state) {
            $out.= "<option value='".$state -> getState_id()."'"; 
            
            if($state -> getState_id() == $filter_state)
                $out.= " selected ";
            
            $out.= ">".ucfirst($state -> getLabel())."</option>";
        }
        $out .= "        </select>";
        
        //FILTRE Priorité
        $out.= "<select id='search_ticket_priority$add_id'>
                            <option value='0'>Prioritée...</option>";
        foreach ($priorityList as $priority) {
            $out.= "<option value='".$priority -> getPriority_id()."' ";
            
            if($priority -> getPriority_id() == $filter_priority)
                $out .= " selected ";
                
            $out.=">".ucfirst($priority -> getLabel())."</option>";
        }
        $out .= "        </select>";

        $out .= "                 <button class='btn' onClick='filterTickets(\"".$entity_id."\", \"".$user_id."\"); return false;'><i class='fa fa-search icon-black' style='padding-right: 0px' ></i></button></form>";
        
        
        //DATA
        $out .= "<table class='table table-bordered paginated'>
                    <caption></caption>
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Statut</th>
                            <th>Priorité</th>
                            <th>Date</th>
                            ";
        if(!$entity_id)
            $out.=             "<th>Client</th>";
        $out.=             "<th>Titre</th>
                            <th style='width:350px'>Texte</th>
                            <th>Assigné à</th>
                            <th>Historique</th>
                            <th>Crée par</th>
                            <th>Relance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>";
        foreach ($ticketList as $ticket) {
            
            if($creator = $userManager -> getById(array("user_id" => $ticket -> getTicket_creator_id()))){
                $creator_name = htmlentities($creator -> getName());
            }else{
                $creator_name = "Client";
            }
                
            if($ticket -> getTicket_entity_id()){
                $entity = $entityManager -> getById(array("entity_id" => $ticket -> getTicket_entity_id()));
                $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
                
                if($type -> getLabel() == "customer"){
                    $customer = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                    $ticketClient = "<a href='http://".$_SERVER['HTTP_HOST']."/app.php/clients/client?type=customer&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($customer->getName())."</a> (A)";
                }elseif($type -> getLabel() == "vendor"){
                    $vendor = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                    $ticketClient = "<a href='http://".$_SERVER['HTTP_HOST']."/app.php/clients/client?type=vendor&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($vendor->getVendor_nom())."</a> (V)"; 
                }
                
            }else{
                $ticketClient = "<em> Global </em>";
            } 
            
            $comment = "";
            if(count($ticket -> getHistory())>0) $style_red= "style='color:red'";
            else $style_red= "";
            $comment .= "<a class='closed btn' href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' id='comment_ticket_".$ticket -> getTicket_id()."' >
                     <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i><small $style_red >".count($ticket -> getHistory())."</small></a>";
            
            //assignation et relance         
            $action = "<a class='btn' href='#' id='ticket_".$ticket -> getTicket_id().$add_id."' onclick='deleteIt(\"ticket_".$ticket -> getTicket_id()."\"); return false;'>
                     <i class='fa fa-trash ' style='padding-right: 0px' ></i></a>";
            
            $date_relance = "";
            if($ticket -> getDate_relance()){
                $date_relance = new DateTime($ticket -> getDate_relance());
                $date_relance = $date_relance -> format("d-m-Y");
            }
            $relance_list = $relanceManager -> getAll(array("ticket_id" => $ticket -> getTicket_id()));
            
            $relance_list ? $relance_count = count($relance_list) : $relance_count = "0";
            
            $relance = "<button class='btn' onClick='showDialogAddRelance(".$ticket -> getTicket_id()."); return false;' >+ <strong>$relance_count</strong></button>";
                     
            $state = $ticketStateManager -> getById(array("state_id" => $ticket -> getTicket_state_id()));
            $priority = $ticketPriorityManager -> getById(array("priority_id" => $ticket -> getTicket_priority_id()));
            
            $date_create = date_create($ticket -> getDate_created());
            
            $ticket->getAssigned_user() ? $assigned_user = $ticket->getAssigned_user()->getName() : $assigned_user = " - ";
            
            $out .= "<tr>
                        <td><a href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' >".$ticket -> getTicket_code()."</a></td>
                        <td>".getLabelStateTicket($state -> getLabel())."</td>
                        <td>".getLabelPriorityTicket($priority -> getLabel())."</td>
                        <td>".date_format($date_create, 'd/m/Y')."</td>";
            if(!$entity_id)
                $out .=    "<td>".$ticketClient."</td>";
            $out .=    "<td><a href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' >".htmlentities($ticket -> getTicket_title())."</a></td>
                        <td>".$this -> truncateTextHide(strip_tags($ticket -> getTicket_text()), 50)."</td>
                        <td>".$assigned_user."</td>
                        <td>$comment</td>
                        <td>".$creator_name."</td>
                        <td>$relance</td>
                        <td>$action</td>";
            $out .= "</tr>";
        }
        $out .= "   </tbody>
                </table>";
  
        
        return $out;
    }

    public function getPopUpDateRelance($ticket_id, $relance=null)
    {
        $relance ? $type_relance_id = $relance->getType_relance_id() : $type_relance_id = "";
        
        $out = '<div id="modalAddRelance" class="modal hide fade"  role="dialog" aria-labelledby="addRelanceLabel" aria-hidden="true">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 id="addProductModalLabel">Ajouter une relance</h4>
                  </div>
                  <div class="modal-body">
                    <div class="control-group">
                        <label for="select_RelanceType" class="control-label">Type de relance :</label>
                        <div class="controls">
                            '.BusinessReferentielUtils::getSelectList("RelanceType", "Type de relance...", $type_relance_id).'
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="datepickerRelance_'.$ticket_id.'" class="control-label">Date :</label>
                        <div class="controls">';
        if($relance){
            
            $date_relance = new DateTime($relance -> getDate_relance());
            
            $out .= '        <input type="text" name="datepickerRelance_'.$ticket_id.'" id="datepickerRelance_'.$ticket_id.'" value="'.$date_relance -> format("d-m-Y").'"/>';
        }else{
            $out .= '        <input type="text" name="datepickerRelance_'.$ticket_id.'" id="datepickerRelance_'.$ticket_id.'" />';
        }
                    
        $out .=        '</div>
                    </div>  
                      
                  </div>
                  <div class="modal-footer">';
        if($relance){
            $out .= '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onClick="addDateRelance('.$ticket_id.', '.$relance->getRelance_id().'); return false;">Sauvegarder</button>';
        }else{
            $out .= '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onClick="addDateRelance('.$ticket_id.'); return false;">Ajouter</button>';
        }
                     
        $out .=     '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>';
                
        $out .= '<script>
                  $(function() {
                    $( "#datepickerRelance_'.$ticket_id.'").datepicker({
                      defaultDate: "+1d",
                      dateFormat : "dd-mm-yy"
                    }); 
                  });
              </script>';    
              
       return $out;            
    }


    public function getRelanceTicketsListTable($relance_list, $filter_name, $filter_statut)
    {
        
        $ticketPriorityManager  = new ManagerTicketPriority();
        $ticketStateManager     = new ManagerTicketState();
        $entityManager          = new ManagerEntityCrm();
        $entityTypeManager      = new ManagerEntityType();
        $customerManager        = new ManagerCustomer();
        $vendorManager          = new ManagerVendor();
        $userManager            = new ManagerAppUser();
        $relanceManager         = new ManagerRelance();
        $relanceTypeManager     = new ManagerRelanceType();
        
        $out = "<form>
                    <input type='text' id='search_ticketrelance_client_name' placeholder='Nom client..' name='search_ticketrelance_client_name' value='$filter_name'>";
        
        //FILTRE Statut
        $out.= "<select id='search_ticketrelance_statut'>
                    ";
        $filter_statut == "0" ? $out.= "<option value='0' selected >Tous</option>": $out.= "<option value='0'  >Tous</option>";
        $filter_statut == "1" ? $out.= "<option value='1' selected >Dépassé</option>": $out.= "<option value='1'  >Dépassé</option>";
        $filter_statut == "2" ? $out.= "<option value='2' selected >Proche</option>": $out.= "<option value='2'  >Proche</option>";
        $filter_statut == "3" ? $out.= "<option value='3' selected >Lointain</option>": $out.= "<option value='3'  >Lointain</option>"; 
 
        $out .= "</select>";
        $out .= "<button class='btn' onClick='filterTicketsRelance(); return false;'><i class='fa fa-search icon-black' style='padding-right: 0px' ></i></button></form>";
        
        $out .= "<table class='table '>

                    <thead>
                        <tr>
                            <th>N° Ticket</th>
                            <th>Statut Ticket</th>
                            <th>Priorité Ticket</th>
                            <th>Date de relance</th>
                            <th>Type de relance</th>
                            <th>Etat relance</th>
                            <th>Client</th>
                            <th>Titre</th>
                            <th>Assigné à</th>
                            <th>Historique</th>
                            <th>Crée par</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>";

        foreach ($relance_list as $relance) {
                    
            $ticket = $relance->getTicket();
            
            if($creator = $userManager -> getById(array("user_id" => $ticket -> getTicket_creator_id()))){
                $creator_name = htmlentities($creator -> getName());
            }else{
                $creator_name = "Client";
            }        
            
            if($ticket -> getTicket_entity_id()){
                $entity = $entityManager -> getById(array("entity_id" => $ticket -> getTicket_entity_id()));
                $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
                
                if($type -> getLabel() == "customer"){
                    $customer = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                    $ticketClient = "<a href='http://".$_SERVER['HTTP_HOST']."/app.php/clients/client?type=customer&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($customer->getName())."</a> (A)";
                }elseif($type -> getLabel() == "vendor"){
                    $vendor = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                    $ticketClient = "<a href='http://".$_SERVER['HTTP_HOST']."/app.php/clients/client?type=vendor&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($vendor->getVendor_nom())."</a> (V)"; 
                }
                
            }else{
                $ticketClient = "<em> Global </em>";
            } 
            
            $comment = "";
            if(count($ticket -> getHistory())>0) $style_red= "style='color:red'";
            else $style_red= "";
            $comment .= "<a class='closed btn' href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' id='comment_ticket_".$ticket -> getTicket_id()."' >
                     <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i><small $style_red >".count($ticket -> getHistory())."</small></a>";
                     
            $ticket->getAssigned_user() ? $assigned_user = $ticket->getAssigned_user()->getName() : $assigned_user = " - ";
            
            $state = $ticketStateManager -> getById(array("state_id" => $ticket -> getTicket_state_id()));
            $priority = $ticketPriorityManager -> getById(array("priority_id" => $ticket -> getTicket_priority_id()));
                     
            $action = "<a class='btn' href='#' id='edit_ticket_".$ticket -> getTicket_id()."' 
                          onclick='showDialogAddRelance(\"".$ticket -> getTicket_id()."\", \"".$relance -> getRelance_id()."\"); return false;'>
                 <i class='fa fa-edit ' style='padding-right: 0px' ></i> Edit relance</a>";
            
            $action .= "<a class='btn btn-danger' href='#' id='edit_ticket_".$ticket -> getTicket_id()."' 
                          onclick='deleteRelance(\"".$relance -> getRelance_id()."\"); return false;'>
                 <i class='fa fa-trash ' style='padding-right: 0px' ></i> Suppr.</a>";     
                 
            $date_relance = "";
            $date_relance = new DateTime($relance -> getDate_relance());
            $now  = new DateTime();
            $now2 = new DateTime();
            $now2 -> add(new DateInterval('P3D'));
            
            $html_date = smartDateNoTime($date_relance -> format("Y-m-d"));
            
            $classAlert = "";
            
            if($date_relance > ($now2)) {
                $classAlert = "success";
            }elseif( ($date_relance > $now) && ($date_relance < $now2) ) {
                $classAlert = "warning";
            }
            elseif($date_relance < $now) {
                $classAlert = "error";
                $html_date = "<strong style='color:red'>".$html_date."</strong>";
            }
            
            if($relance_type = $relanceTypeManager->getBy(array("type_id" => $relance->getType_relance_id()))){
                $relance_type_html = "<strong>".$relance_type->getLabel()."</strong>";
            }else{
                $relance_type_html = " - ";
            }
            
            $action_close_relance = getActionRelanceDropdown( $relance->getRelance_id(), $relance->getDate_termine());
            
            $out .= "<tr class='$classAlert'>
                        <td><a href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' >".$ticket -> getTicket_code()."</a></td>
                        <td>".getLabelStateTicket($state -> getLabel())."</td>
                        <td>".getLabelPriorityTicket($priority -> getLabel())."</td>
                        <td>".$html_date."</td>
                        <td>".$relance_type_html."</td>
                        <td>".$action_close_relance."</td>
                        <td>".$ticketClient."</td>
                        <td><a href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' >".htmlentities($ticket -> getTicket_title())."</a></td>
                        <td>".$assigned_user."</td>
                        <td>$comment</td>
                        <td>".$creator_name."</td>
                        <td>$action</td>";
            $out .= "</tr>";                
        }
        
        $out .= "   </tbody>
                </table>";
                
        return $out;
    }

    public function getMiniRelanceListTable($relanceList, $ticket)
    {
        $now  = new DateTime();
        $now2 = new DateTime();
        $now2 -> add(new DateInterval('P3D'));
        $relanceTypeManager     = new ManagerRelanceType();
        $out .= "<table class='table table-bordered'>
                    <thead>
                        <tr>
                            <th>Date de relance</th>
                            <th>Type de relance</th>
                            <th>Etat relance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>";
                    
        foreach ($relanceList as $relance) {
            
            $action = "<a class='btn' href='#' id='edit_ticket_".$ticket -> getTicket_id()."' 
                              onclick='showDialogAddRelance(\"".$ticket -> getTicket_id()."\", \"".$relance -> getRelance_id()."\"); return false;'>
                     <i class='fa fa-edit ' style='padding-right: 0px' ></i> Edit relance</a>";
                
            $action .= "<a class='btn btn-danger' href='#' id='edit_ticket_".$ticket -> getTicket_id()."' 
                              onclick='deleteRelance(\"".$relance -> getRelance_id()."\"); return false;'>
                     <i class='fa fa-trash ' style='padding-right: 0px' ></i> Suppr.</a>";    
                     
            $date_relance = new DateTime($relance -> getDate_relance());
            
            $html_date = smartDateNoTime($date_relance -> format("Y-m-d"));
            
            $classAlert = "";
            
            if($date_relance > ($now2)) {
                $classAlert = "success";
            }elseif( ($date_relance > $now) && ($date_relance < $now2) ) {
                $classAlert = "warning";
            }
            elseif($date_relance < $now) {
                $classAlert = "error";
                $html_date = "<strong style='color:red'>".$html_date."</strong>";
            }
            
            if($relance_type = $relanceTypeManager->getBy(array("type_id" => $relance->getType_relance_id()))){
                $relance_type_html = "<strong>".$relance_type->getLabel()."</strong>";
            }else{
                $relance_type_html = " - ";
            }
            
            $action_close_relance = getActionRelanceDropdown( $relance->getRelance_id(), $relance->getDate_termine());
            
            $out .= "<tr class='$classAlert'>
                        <td>".$html_date."</td>
                        <td>".$relance_type_html."</td>
                        <td>".$action_close_relance."</td>
                        <td>$action</td>";
            $out .= "</tr>";
        }
        $out .= "   </tbody>
                </table>";

        return $out;
    }    
    
    
    public function showVendorInfo($vendor , $poList=null)
    {
        $ticketManager = new ManagerTicket();
        $customerManager = new ManagerCustomer();
        $out ="";
        
        $out.= '
                    <button class="btn" onclick="javascript:window.history.go(-1);"><i class="fa fa-arrow-left"></i></button>
                    <button class="btn pull-right" onclick="self.location.href=&quot;/app.php/dashboard &quot;"><i class="fa fa-dashboard"> Tableau de bord</i></button>
                    <span id="vendor_messagebox"></span>
                    <span id="ticket_messagebox"></span>
                  ';
        
        //INFOS
        $out .= "
                  <div class='row-fluid'>";
                    
        $out.= "    
                    <div class='span3 well' id='infoClient'>"
                      .$this -> ficheInfoClientVendor($vendor, $poList).
                    "</div>";
        
        //DEMANDES
        $out.="     <div class='span9 well'>
                        <div class='tabbable tabs'> 
                            <ul class='nav nav-tabs'>
                                <li class='active'><a href='#tickets_client' data-toggle='tab'>Demandes client</a></li>
                                <li><a href='#credits_client' data-toggle='tab'>Historique crédits</a></li>
                            </ul>
                            <div class='tab-content'>

                            <div class='tab-pane active' id='tickets_client'>
                                <h4>Demandes client : <button class='pull-right btn btn-primary' value='Créer' onClick='showDialogAddTicket(".$vendor->getEntity_id().", \"vendor\");';><i class='fa fa-ticket icon-black' style='padding-right: 0px'></i> Créer ticket</button></h4>
                                <div id ='ticket_list_client'>";
                        
        $ticketList = $ticketManager -> getAll(array("ticket_entity_id" => $vendor->getEntity_id()) );
        
        $creditsHistory = $vendor -> getCreditsHistory();
        
        $out .= $this -> getTicketsListTable($ticketList, $vendor->getEntity_id());
        $out.= "                </div>
                            </div>
                            
                            <div class='tab-pane' id='credits_client'>
                                <div id='histo_credit_vendor'>
                                ".$this->getHistoCreditsVendor($vendor, $creditsHistory)."
                                </div>   
                            </div>
                       </div>
                    </div>
                </div>";
             
        //COMMANDES        
        $out.="
                <div class='span9 well'>
                        <h4>Commandes : </h4>
                        <table class='table table-bordered paginated'>
                            <caption></caption>
                            <thead>
                                <tr>
                                    <th style='width:5%'></th>
                                    <th style='width:10%'>N°Commande</th>
                                    <th style='width:10%'>Date</th>
                                    <th style='width:15%'>Client</th>
                                    <th style='width:10%'>Total</th>
                                    <th style='width:10%'>Etat (State)</th>
                                    <th style='width:5%'>Livraison</th>
                                    <th style='width:10%'>Addresse livrée:</th>
                                    <th style='width:10%'>Litiges</th>
                                    <th style='width:10%'>Payment</th>
                                    <th style='width:10%'>Facture</th>
                                </tr>
                            </thead>
                            <tbody>";

        foreach ($poList as $po) {
            
            $order = new BusinessOrder($po->getOrder_id());
            
            $address = new BusinessAddress($order->getShipping_address_id());
          
            //Vérification si le po a été facturé entierement ou pas permet de donner une indication
            //Utilise la fonction interne de l'objet BusinessPo -> checkFacture() permettant de vérifier si chaque item est facturé
            if($po -> checkFacture()){
                $info_facture = '<span id="po_'.$po->getPo_id().'_facture" class="etat_order label label-inverse">'.$po -> checkFacture().'</span>';
            }else{
                $info_facture = '<span id="po_'.$po->getPo_id().'_facture" class="etat_order label label-inverse"></span>';
            }
            
            //Vérification de l'état de paiement des différents items contenu dans la commande
            if($po->getPayment() == "complete"){
                $info_payment = '<span id="po_'.$po->getPo_id().'_payment" class="etat_order label label-success">Payé</span>';
            }
            elseif($po->getPayment() == "partiel"){
                $info_payment = '<span id="po_'.$po->getPo_id().'_payment" class="etat_order label label-warning">Partiel</span>';
            }
            else{
                $info_payment = '<span id="po_'.$po->getPo_id().'_payment" class="etat_order label label-success"></span>';    
            }
            
            //Litiges
            if($po -> getLitiges()){
                $info_litiges = '<span id="po_'.$po->getPo_id().'_etat" class="etat_order label label-warning">litige</span>';
            }else{
                $info_litiges = '<span id="po_'.$po->getPo_id().'_etat" class="etat_order label label-warning"></span>';
            }
            
            //Affichage du nom avec bon lien vers fiche client en fonction des renseignements sur la commande
            if($order->getOrder_customer_id() == "0"){
                if($order->getRecipient_email() != ""){
                    if($cust = $customerManager -> getBy(array("email" => $order->getRecipient_email())))
                        $nomCli = '<a href="http://'.$_SERVER['HTTP_HOST'].'/app.php/clients/listing_acheteurs?id='.$cust -> getEntity_id().'" target="_blank">'.
                                        htmlentities($cust -> getName()).'
                                   </a>';
                    else 
                        $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order->getRecipient_name()) .'</em>';  
                }else{
                    $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order->getRecipient_name()) .'</em>';    
                }
            }else{
                $cust = $customerManager -> getBy(array("customer_id" => $order->getOrder_customer_id()));
                $nomCli = '<a href="http://'.$_SERVER['HTTP_HOST'].'/app.php/clients/listing_acheteurs?id='.$cust -> getEntity_id().'" target="_blank">'.
                                htmlentities($cust -> getName()).'
                           </a>';
            }
            
                      $out .= "<tr>
                                    <td><input type='button' class='closed btn' value='+' id='item_".$po->getOrder_id()."_".$po->getVendor_id()."' onclick='showChild(this)' /></td>
                                    <td>".$po->getIncrement_id()."</td>
                                    <td>".smartDateNoTime($po->getDate_created())."</td>
                                    <td>".$nomCli."</td>
                                    <td>".numberByLang($po->getBase_total_value() + $po->getBase_tax_amount())." €</td>
                                    <td>".getLabelStatus($order->getOrder_state())."</td>
                                    <td>".$po->getShipping_info()."</td>
                                    <td><b>".htmlentities($address->getStreet1());
            if($address->getStreet2()) 
                $out.=                      "</br>".htmlentities($address->getStreet2());
            
            $out.=                          "</br>".htmlentities($address->getPostcode())." ".htmlentities($address->getCity()).
                                            "</br>".htmlentities($address->getCountry())."</b></td>";
            $out.=                 "<td>$info_litiges</td>
                                    <td>$info_payment</td>
                                    <td>$info_facture</td>";                                            

            $out.=            "</tr>    ";
        }
                
        $out .= "            </tbody>
                        </table>
                    </div>";
        $out .= "</div>";
        
        $out .= "<div class='row-fluid'>
                      <div class='span12'>
                          <h4>Litiges Client : </h4>
                      </div>
                 </div>
                 <script type='text/javascript'>
                      $(document).keyup(function(e) {
                        if (e.keyCode == 27) { javascript:window.history.go(-1); }
                      });
                 </script>";
        
        return $out;        
    }
    
    /**
     * Fonction permettant de couper un texte et y ajouter un lien sous forme de (...) clickable
     * permettant d'afficher le reste du texte en javascript.
     * @param   string $text        chaine de caractère d'origine qui va être tronquée
     * @param   int    $limit       la limite de nombre de caractère après celle ci la chaine est tronquée
     * @return  string $newstring   chaine de caractère tronquée retournée avec lien cliquable javascript
     */
    public function truncateTextHide($text, $limit)
    {
        if(strlen($text) > $limit ){
            $oldString = substr($text, 0, 50);
            $insert_string = "<a href='#' onClick='showFullText(this); return false;'>(plus...)</a><span class='hide'>".substr($text, $limit, strlen($text))."</span>";
            $newstring = $oldString . $insert_string ;        
        }
        else{
            $newstring = $text;
        }
        return $newstring;
    }
    
    /**
     * Fonction permettant de retourner un span contenant une valeur suivie d'un boutton d'édition.
     * Ce boutton d'édition permet en javascript de transformer la valeur en input modifiable et sauvegardable directement
     * en Ajax afin de changer la valeur en base de donnée directement
     * @param   string  $property   le nom de la propriété à changer, la meme qu'un base de donnée elle sera aussi l'id du span HTML
     * @param   string  $type       le type de champ pour le moment limité à vide ou bien "num" pour spécifier différentes vérifications sur l'input
     * @param   int     $entity_id  l'entity_id de l'entité dont on est en train de modifier la propriété que ce soit un Customer ou un Vendor
     * @param   string  $value      La valeur de base que possède cette propriétée
     * @return  string  chaine HTML contenant la valeur et le lien clickable en javascript pour modifier l'input à la volée
     */
	public function getEditableAjaxInput($property, $type, $entity_id, $entity, $value )
	{
	   return "<span id='".$property."_".$entity_id."'>$value<a class='pull-right' href='#' onClick='setProperty(this, \"$entity\", \"$property\", \"$type\")' style='margin-right:5px'>
                                    <img class='iconAction' border='0' align='absmiddle' src='/skins/common/images/picto/edit.gif' title='Editer' alt='Editer'>
                                </a></span>"; 	
	}
    
	public function showOrderAvahisInfo($po, $order, $itemList)
	{
		$out ="";
		$address = new BusinessAddress($order->getShipping_address_id());
		
		$addr = htmlentities($address->getStreet1());
		
		if($address->getStreet2()) 
			$addr .="</br>".htmlentities($address->getStreet2());
			
		$addr .="</br>".htmlentities($address->getPostcode())." ".htmlentities($address->getCity()).
				"</br>".htmlentities($address->getCountry());

		if($order -> getOrder_customer_id() != "0"){
			
			$customer = new BusinessCustomer($order -> getOrder_customer_id());
			$name = strtoupper(htmlentities($customer -> getFirstname()))." ".strtoupper(htmlentities($customer -> getLastname()));
			$mail = htmlentities($customer -> getEmail());
			$phone = htmlentities($customer -> getPhone());
			
		}else{
			$name = htmlentities($order -> getRecipient_name());
			$mail = htmlentities($order -> getRecipient_email());
			$phone = htmlentities($order -> getRecipient_phone());
		}
		
		
		
		$out .= "<div class='row-fluid'>";
		$out.= "	<div class='span3 well'>
						<h4>".$name."</h4><hr/>
						<p><span><b>".$addr."</b></span>
						<p><span>Email : </span><span><b>".$mail."</b></span>
						<p><span>Tel : </span><span><b>".$phone."</b></span>
					</div>";

 		$out.=" 	<div class='span9 well'>
  						<h4>Contenu : </h4>
  						<table class='table table-bordered'>
							<caption></caption>
						  	<thead>
						    	<tr>
						    		<th>SKU</th>
						      		<th>Nom</th>
						      		<th>Prix</th>
						      		<th>Poids</th>
						      		<th>Qté dem</th>
						      		<th>Grossiste</th>
						      		<th>REF</th>
						      		<th>Qté dispo</th>
						      		<th>Etat</th>
						      		<th>Actions</th>
						      		<th> - </th>
						    	</tr>
						  	</thead>
						  	<tbody>";

  		foreach ($itemList as $item) {
  			 $prod_gr = $item -> getProductGrossiste();
			 $out .= "<tr>
		  				<td>".$item -> getSku()."</td>
		  				<td>".truncateStr($item->getName(), 40)."</td>
		  				<td>".numberByLang($item->getBase_row_total_incl_tax())." €</td>
		  				<td>".numberByLang($item->getRow_weight())."</td>
		  				<td>".$item->getQty_ordered()."</td>
		  				<td><b>".$prod_gr->getGrossiste_nom()."</b></td>";
			if($prod_gr->getUrl()){
				$out.= "<td><a href='".$prod_gr->getUrl()."' target='_blank'><b>".$prod_gr->getRef_grossiste()."</b></a></td>";
			}else{
				$out.= "<td><b>".$prod_gr->getRef_grossiste()."</b></td>";
			}
		  	
		  				
			
			if($prod_gr->getQuantity() == "0"){
				$out .= "<td><span class='etat_order label label-warning'><b style='color:black';> Délai ! <b></span></td>";
			}elseif($prod_gr->getActive() == "0"){
				$out .= "<td><span class='etat_order label label-important'> Rupture ! </span></td>";
			}
			else{
				$out .= "<td>".$prod_gr->getQuantity()."</td>";
			}
			
			$out .= "<td>";
			if($item->getOrder_id_grossiste()){
				$orderGrossiste = new BusinessOrderGrossiste($item->getOrder_id_grossiste());
				$out.= '<span id="item_'.$item->getItem_id().'_cmd" class="etat_order label label-success"> commandé : 
							<a href="/app.php/grossistes/commande_grossiste?id='.$orderGrossiste->getOrder_external_code().'" target="_blank">'.
								$orderGrossiste->getOrder_external_code().'
							</a>  
						  </span>';
			}else{
				$out.= '<span id="item_'.$item->getItem_id().'_cmd" class="etat_order label label-success"></span>';
			}
			if($item->getLitiges()){
				$out.='<span id="item_'.$item->getItem_id().'_etat" class="etat_order label label-warning"> litige </span>';
			}else{
				$out.='<span id="item_'.$item->getItem_id().'_etat" class="etat_order label label-warning"></span>';
			}
			$out .= "</td>";
			
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$item -> getItem_id().' AND commented_object_table = "skf_order_items"'));   
            if($countComment > 0){
                $countComment = "<small> ".$countComment."</small>";
            }
            else{
                $countComment = "<small> </small>";
            }
			$out.='<td><span class="popover-markup"> 
								<a class="trigger" id="popover_'.$item->getItem_id().'">
									<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Assigner commande" alt="Assigner commande">
								</a> 
							    <div class="head hide">
							    	<button type="button" id="close" class="close" onclick="$(&quot;#popover_'.$item->getItem_id().'&quot;).popover(&quot;hide&quot;);">&times;</button>
							    	<br>
							    	<h5>Commande grossiste : <h5>
							    </div>
							    <div class="content hide">
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="N° Commande…" id="popoverinput_'.$item->getItem_id().'">
							        </div>
							        <button type="submit" class="btn btn-primary btn-block" onClick="assignItemComGr(&quot;'.$item->getItem_id().'&quot;);"><b>OK</b></button>
							    </div>
							</span>
							<a class="closed btn" href="#" id="comment_item_'.$item->getItem_id().'" onclick="showComments(this)"><i class="fa fa-comment icon-black" style="padding-right: 0px" ></i>'.$countComment.'</a></td>';
			$out.= '<td><input id="item_' . $item->getItem_id() . '" type="checkbox"/></td>';
						
		 }		
  		$out .= "		</tbody>
  					</div>";
		$out .= "</div>";
		
		return $out;	
	}
	
	public function showAnnonceReceptionInfo($annonce_reception, $itemList)
	{
		$out .= "<div class='row-fluid'>";
		$out.= "	<div class='span3 well'>
						<h4>".$annonce_reception -> getAnnonce_reception_code()."</h4><hr/>
						<p><span>Création : </span><span><b>".smartdate($annonce_reception -> getDate_created())."</b></span>
						<p><span>Réception : </span><span><b>".smartdate($annonce_reception -> getDate_reception())."</b></span>
					</div>";

 		$out.=" 	<div class='span9 well'>
  						<h4>Contenu : </h4>
  						<table class='table table-bordered'>
							<caption></caption>
						  	<thead>
						    	<tr>
						    		<th>ComGR</th>
						    		<th>N° Comm</th>
						      		<th>Nom</th>
						      		<th>Prix</th>
						      		<th>Poids</th>
						      		<th>Qté</th>
						      		<th>Etat</th>
						      		<th>Actions</th>
						      		<th> - </th>
						    	</tr>
						  	</thead>
						  	<tbody>";

  		foreach ($itemList as $item) {
  			$order = new BusinessOrder($item -> getOrder_id());
			$order_grossiste = new BusinessOrderGrossiste($item -> getOrder_id_grossiste());
			$out .= "<tr>
						<td>".'<a href="/app.php/grossistes/commande_grossiste?id='.$order_grossiste->getOrder_external_code().'" target="_blank">'.
								 $order_grossiste ->getOrder_external_code().'
							</a>'. "</td>
			 			<td>".$order -> getIncrement_id()."</td>
		  				<td>".truncateStr(htmlentities($item->getName()), 40)."</td>
		  				<td>".numberByLang($item->getBase_row_total_incl_tax())." €</td>
		  				<td>".numberByLang($item->getRow_weight())."</td>
		  				<td>".$item->getQty_ordered()."</td>";
					  				
			
			$out .= "<td>";
			
			$out .= "</td>";
			
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$item -> getItem_id().' AND commented_object_table = "skf_order_items"'));	
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			$out.='<td><a class="closed btn" href="#" id="comment_item_'.$item->getItem_id().'" onclick="showComments(this)"><i class="fa fa-comment icon-black" style="padding-right: 0px" ></i>'.$countComment.'</a></td>';
			$out.= '<td><input id="item_' . $item->getItem_id() . '" type="checkbox"/></td>';
						
		 }		
  		$out .= "		</tbody>
  					</div>";
		$out .= "</div>";
		
		return $out;			
	}

	public function getCommentForm($type, $id)
	{

		$out .= "<div class='row-fluid'>";
		$out .= "	<div class='span6'>";
		$out .= '		<label class="control-label" for="inputContent_'.$id.'">Votre commentaire</label>
				      	<textarea rows="4" id="inputContent_'.$id.'" placeholder="Entrez votre commentaire"></textarea>
				      	<button class="btn" onClick="addComment('.$id.', \''.$type.'\'); return false">Valider</button>
					</div>
				</div>';
		return $out ;
	}
	
	public function showCommentList($commentList, $type, $id)
	{
		$user = BusinessUser::getClassUser($_SESSION['smartlogin']);
		
		$out .= "<div class ='span6 comment_list' id='comlist_".$id."'>";
		
		if(empty($commentList)){
			$out.= "Aucun commentaire...";
		}
		else{
			foreach ($commentList as $comment) {
				$out .= "<div class = 'well' id='comment_".$comment -> getId_comment()."'>";
				$out .= "<span class ='author'><strong>".htmlentities($comment -> getAuthor())."</span></strong> 
						le <span class ='date'>".smartdate($comment -> getDate_created())."</span>";
				if($comment -> getDate_updated() != $comment -> getDate_created()){
					$out .= "<em class='date_edit' style='color:red'> - Edité le ".smartdate($comment -> getDate_updated())."</em>";
				}
				$out.= "<br/>";
				$out .= "<br/><span class='comment_content'>".$comment -> getContent()."</span><br/>";
				$out .= "<br/>";
				if($user->getUserId() == $comment -> getUser_id()){
					$out .= "<a href='#' onclick='editComment (".$comment -> getId_comment()."); return false;'>Editer</a>  -  ";
					$out .= "<a href='#' onclick='supprComment (".$comment -> getId_comment()."); return false;'>Suppr.</a>";	
				}
				
				$out .= "</div>";
			}	
		}		
		
		$out .= "</div>";
		
		return $out;
	}
	
	public function showVendorPoList($po_list, $vendor_id, $filterdate = false)
	{
		$out .= "<div class='row-fluid'>";
 		$out.=" 	<div class='well'>
  						<h4>Commandes : </h4>
  						<table class='table table-bordered vendorpolist'>
							<caption></caption>
						  	<thead>
						    	<tr>
						    		<th></th>
						    		<th>N° Comm</th>
						    		<th>Date</th>
						      		<th>Prix TTC</th>
						      		<th>Total Comm</th>
						      		<th>Total Frais B.</th>
						      		<th>Status</th>
						      		<th>Qté Articles</th>
						      		<th>Payé</th>
						      		<th>Actions</th>
						    	</tr>
						  	</thead>
						  	<tbody>";

  		foreach ($po_list as $po) {
  			
  			$order = new BusinessOrder($po -> getOrder_id());
			
			$sql = "SELECT 	SUM(soi.base_row_total_incl_tax) as montant_total, SUM(soi.base_row_total_incl_tax * c.COMM) as montant_commission, SUM(soi.base_row_total_incl_tax * 0.005 ) as frais_banquaires 
								FROM skf_orders so
								INNER JOIN skf_order_items soi ON so.order_id = soi.order_id 
								INNER JOIN skf_po spo ON spo.order_id = so.order_id AND spo.vendor_id = soi.udropship_vendor
								INNER JOIN sfk_vendor v ON v.VENDOR_ID = soi.udropship_vendor 
								LEFT JOIN sfk_categorie c ON c.cat_id = soi.category_id
								WHERE soi.litiges <> 1 
								AND soi.udropship_vendor = '".$po->getVendor_id()."' 
								AND so.order_state = 'complete' AND spo.order_id = ".$po -> getOrder_id()." ";
								

			if($filterdate){
				$sql .= " AND spo.date_created < ('$filterdate' - INTERVAL 15 DAY) AND spo.payment <> 'complete' ";
			}
			
			
			$tmp = dbQueryAll($sql);
			$tmp = $tmp[0];
			$total = $tmp['montant_total'];
			$tmp['montant_commission'] ? $total_mc = numberByLang($tmp['montant_commission']) : $total_mc = "<strong style='color:red'>".numberByLang($tmp['montant_commission'])."</strong>";
			$total_frais_b = $tmp['frais_banquaires'];
			
			$show = false;
			
			if($complete && $order -> getOrder_state() == "complete"){
				$show = true;
			}elseif(!$complete){
				$show = true;
			}
			if($show){
				if($po -> getPayment() == "complete"){
					$info_payment = '<span id="po_'.$po -> getPo_id().'_payment" class="etat_order label label-success">Payé</span>';
				}elseif($po -> getPayment() == "partiel"){
					$info_payment = '<span id="po_'.$po -> getPo_id().'_payment" class="etat_order label label-warning">Partiel</span>';
				}else{
					$info_payment = '<span id="po_'.$po -> getPo_id().'_payment" class="etat_order label label-success"></span>';	
				}
				
				$out .= "<tr>
							<td><input type='button' class='closed btn' value='+' id='item_".$po -> getPo_id()."_".$po -> getVendor_id()."'  onclick='showPoVendorInfo(this)'  /></td>
				 			<td>".$po -> getIncrement_id()."</td>
				 			<td>".smartdate($po -> getDate_created())."</td>
			  				<td>".numberByLang($total)." €</td>
			  				<td>".$total_mc." €</td>
			  				<td>".numberByLang($total_frais_b)." €</td>
			  				<td>".getLabelStatus($order -> getOrder_state())."</td>
			  				<td>".$po -> getTotal_qty()."</td>
			  				<td>".$info_payment."</td>
			  				<td></td>";
						  				
				$out .= "</tr>";	
			}
  				
		}
		$out .= "			</tbody>
						</table>
					</div>
				</div>";
		
		return $out;
	}
	
	public function showVendorPoItemList($itemList, $po)
	{
		$out.=" 	<div class='well'>
  						<table class='table table'>
							<caption></caption>
						  	<thead>
						    	<tr>
						    		<th>Nom</th>
						    		<th>Categ</th>
						    		<th>Comm categ</th>
						    		<th>Prix TTC</th>
						      		<th>Montant comm</th>
						      		<th>Montant Frais B</th>
						      		<th>Status</th>
						      		<th>Payé</th>
						      		<th>Actions</th>
						    	</tr>
						  	</thead>
						  	<tbody>";
		foreach ($itemList as $item) {
			
			$query = dbQueryAll('SELECT COMM, cat_label FROM sfk_categorie WHERE cat_id = '.$item -> getCategory_id());
			$query = $query[0];
			$query['COMM'] ? $comm = $query['COMM'] : $comm = 0;
            $cat_label = $query['cat_label'];
			
			if($item -> getLitiges()){
				$info_etat = '<span id="item_'.$item -> getItem_id().'_litige" class="etat_order label label-warning">Litige</span>';
			}else{
				$info_etat = '<span id="item_'.$item -> getItem_id().'_litige" class="etat_order label label-warning"></span>';	
			}
			
			if($item -> getPayment()){
				$info_payment = '<span id="item_'.$item -> getItem_id().'_payment" class="etat_order label label-success">Payé</span>';
			}else{
				$info_payment = '<span id="item_'.$item -> getItem_id().'_payment" class="etat_order label label-warning"></span>';
			}
				
			$out .= "<tr>
				 			<td>".htmlentities($item -> getName())."</td>
				 			<td>".$cat_label."</td>
			  				<td>
			  				   <span style='font-weight:bold;'id='commcateg_".$item -> getCategory_id()."'>". htmlentities($comm) ."<a  href='#' onClick='setCommCateg(this); return false;' style='margin-right:5px'>
                                    <img class='iconAction' border='0' align='absmiddle' src='/skins/common/images/picto/edit.gif' title='Modifier Taux' alt='Modifier Taux'>
                                </a> </span></td>
				 			<td>".numberByLang($item -> getBase_row_total_incl_tax())." €</td>
			  				<td>".numberByLang($item -> getBase_row_total_incl_tax() * $comm)." €</td>
			  				<td>".numberByLang($item -> getBase_row_total_incl_tax() * 0.005)." €</td>
			  				<td>".$info_etat."</td>
			  				<td>".$info_payment."</td>
			  				<td></td>";
						  				
			$out .= "</tr>";	
		}			
							
		$out .=" 			</tbody>
						</table>
					</div>";
		
		return $out;
	}
	
    public function showAbonnementVendorInfo($vendor, $abonnement, $nbProductsSold)
    {
        $factCreditManager = new ManagerFactureCredit();
        
        global $root_path;
        $out .= "<div class='row-fluid'>";
        $out.= "    <div class='span3 well'>
                        <h4>".htmlentities($vendor -> getVendor_nom())."<button class='btn' id='".$vendor->getEntity_id()."' onclick='self.location.href=\"/app.php/clients/client?type=vendor&id=".$vendor->getEntity_id()."\"'><i class='fa fa-search'></i></button></h4>"
                        .getLabelAbonnement($abonnement->getLabel())."   <strong>(";
                        $abonnement->getAbonnement_limit_product() ? $out.= $abonnement->getAbonnement_limit_product() : $out.= "∞";
        $out.= ")</strong><hr/>
                        <p><span><b>".$vendor -> getCivilite()." " . htmlentities($vendor -> getVendor_attn()) ." " . htmlentities($vendor -> getVendor_attn2())."</b></span></p>
                        <p><span>Début abonnement : </span><span><b>".smartdateNotime(date('Y-m-d H:i:s'))."</b></span></p>
                        <p><span>Fin abonnement : </span><span><b>".smartdateNotime(date('Y-m-d', strtotime(date('Y-m-d H:i:s'). ' + 6 months')))."</b></span></p>
                    </div>";

        $out.="     <div class='span9 well'><h4>";
        
        if($vendor ->getProduct_limit()){
            
            if($nbProductsSold > $vendor ->getProduct_limit() ) {
                
                $out .= "<strong>".$vendor ->getProduct_limit() . "</strong> produit(s) en vente 
                         <span style='color:red'><strong>". ($nbProductsSold - (int)$vendor ->getProduct_limit()). "</strong> hors abonnement </span>"; 
            }
            else {
                $out .= "<strong>".$nbProductsSold . "</strong> produit(s) en vente ";
            }
        }
        elseif( ($abonnement -> getAbonnement_limit_product() != 0 ) && ($nbProductsSold > $abonnement -> getAbonnement_limit_product())) {
            
            $out .= "<strong>".$abonnement -> getAbonnement_limit_product() . "</strong> produit(s) en vente 
                     <span style='color:red'><strong>".($nbProductsSold - (int)$abonnement -> getAbonnement_limit_product()). "</strong> hors abonnement </span>";
        }
        else {
            $out .= "<strong>".$nbProductsSold . "</strong> produit(s) en vente ";
        }
        
        //Affichage du nombre de crédit et affichage de l'historique des transactions 
        //Transactions de crédits et achats d'abonnements
        $creditsHistory = $vendor -> getCreditsHistory();
        
        $out.="         </h4><h4>Crédit : ".$vendor -> getSolde_credit()." ø</h4>
                        <table class='table table-bordered paginated'>
                            <caption></caption>
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Date</th>
                                    <th>Lib</th>
                                    <th>Montant</th>
                                    <th>Type</th>
                                    <th>Facture</th>
                                    <th>Paiement</th>
                                    <th> - </th>
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($creditsHistory as $credit) {
            $out .= "<tr>";
            $out .= "   <td>".$credit -> getHistory_id()."</td>";
            $out .= "   <td>".smartDate($credit -> getDate_created())."</td>";
            $out .= "   <td>".$credit -> getLabel()."</td>";
            $out .= "   <td>".$credit -> getAmount()."</td>";
            $out .= "   <td>".$credit -> getType()."</td>";
            if($credit -> getFacture_id()){
                $fact = $factCreditManager -> getBy(array("facture_id" => $credit -> getFacture_id()));
                
                $out .= "   <td>"."<a href='". $root_path . "downloadfactCredit?id=".$credit -> getFacture_id()."' >Voir facture</a></td>";
                if($fact -> getPayment_type_id())
                    $out .= "   <td>".$fact -> getPayment_type_label()."</td>";
                else
                    $out .= "<td> - </td>"; 
            }
            else{
                $out .= "   <td> - </td>";
                $out .= "   <td> - </td>";
            }
            $out .= "   <td></td>";
            $out .= "</tr>";
        }

        $out .= "       </tbody>
                        </table>
                    </div>";
        $out .= "</div>";
        
        return $out;            
    }

    public function getHistoCreditsVendor($vendor, $creditsHistory)
    {
        $factCreditManager = new ManagerFactureCredit();
        
        $out="         </h4><h4>Crédit : ".$vendor -> getSolde_credit()." ø</h4>
                        <table class='table table-bordered paginated'>
                            <caption></caption>
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Date</th>
                                    <th>Lib</th>
                                    <th>Montant</th>
                                    <th>Type</th>
                                    <th>Facture</th>
                                    <th>Paiement</th>
                                    <th> - </th>
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($creditsHistory as $credit) {
            $out .= "<tr>";
            $out .= "   <td>".$credit -> getHistory_id()."</td>";
            $out .= "   <td>".smartDate($credit -> getDate_created())."</td>";
            $out .= "   <td>".$credit -> getLabel()."</td>";
            $out .= "   <td>".$credit -> getAmount()."</td>";
            $out .= "   <td>".$credit -> getType()."</td>";
            if($credit -> getFacture_id() && $fact = $factCreditManager -> getBy(array("facture_id" => $credit -> getFacture_id()))){
                
                
                $out .= "   <td>"."<a href='". $root_path . "/app.php/commercants/downloadfactCredit?id=".$credit -> getFacture_id()."' >Voir facture</a></td>";
                if($fact -> getPayment_type_id())
                    $out .= "   <td>".$fact -> getPayment_type_label()."</td>";
                else
                    $out .= "<td> - </td>"; 
            }
            else{
                $out .= "   <td> - </td>";
                $out .= "   <td> - </td>";
            }
            $out .= "   <td></td>";
            $out .= "</tr>";
        }

        $out .= "       </tbody>
                    </table>";
        
        return $out;
                    
    }

    public function getProductThumbnail($product)
    {
        $html = "";
        
        $html .= ' <div class="thumbnail span3"> 
                       <a href="/app.php/catalogue/produit/id?'.$product->getId_produit().'" > 
                        <img src="'.$product->getImage().'" alt="product 1" class="span6"></a>  
                        <div class="caption span6">  
                          <h5>'.truncateStr(htmlentities($product->getName()), 20).'</h5>  
                          <p>'.truncateStr(htmlentities($product->getShort_description()), 50).'</p>   
                        </div>  
                      </div><br /><br />';
            
        return $html;
        
    }
    
    public function showAttributsTable($data, $sort_nb_prods_class, $cat_id, $sort_code_attr_class, $cat_list = ""){
        
    $sort_nb_prods_icon ="";
    $sort_code_attr_icon ="";
            
    switch ($sort_nb_prods_class) {
        case 'cmpDesc':
            $sort_nb_prods_icon = "<i class='fa fa-sort-desc'></i>";
        break;
        case 'cmpAsc':
            $sort_nb_prods_icon = "<i class='fa fa-sort-asc'></i>";
        break;
    
        default:
            $sort_nb_prods_icon = "<i class='fa fa-sort'></i>";         
        break;
    }   
    
    switch ($sort_code_attr_class) {
        case 'asc':
            $sort_code_attr_icon = "<i class='fa fa-sort-alpha-asc'></i>";
        break;
        case 'desc':
            $sort_code_attr_icon = "<i class='fa fa-sort-alpha-desc'></i>";
        break;
    
        default:
            $sort_code_attr_icon = "<i class='fa fa-sort'></i>";         
        break;
    } 
        $out .= "<table class='table table-bordered table-data' id='attributs-table'>

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th><a onClick='sortCodeAttr(this);return false;' href='#' class='$sort_code_attr_class'>$sort_code_attr_icon CODE</a></th>
                            <th>LABEL</th>
                            <th><a onClick='sortNbProds(this);return false;' href='#' class='$sort_nb_prods_class'>$sort_nb_prods_icon Nb produits</a></th>
                            <th>Filtrable</th>
                            <th>Actions</th>
                            <th>Sélection</th>
                        </tr>
                    </thead>
                    <tbody>";
        if(!empty($data)){
            foreach ($data as $attr) {
                //$attr_category = $attr -> getAttribut_category($cat_id);
                if($attr -> getIs_filterable() === "1"){
                    $is_filter = "checked"; 
                }else{
                    $is_filter = "";
                }
                $out .= "<tr>";
                $out .= "<td onClick='openFicheAttribut(".$attr->getId_attribut().")'>".$attr->getId_attribut()."</td>";
                $out .= "<td onClick='openFicheAttribut(".$attr->getId_attribut().")'>".htmlentities($attr->getCode_attr())."</td>";
                $out .= "<td onClick='openFicheAttribut(".$attr->getId_attribut().")'>".htmlentities($attr->getLabel_attr())."</td>";
                //$out .= "<td onClick='openFicheAttribut(".$attr->getId_attribut().")'>".$attr->getCountProdsInCatWithThis($cat_id)."</td>"; //Sans recursivité
                $out .= "<td onClick='openFicheAttribut(".$attr->getId_attribut().")'>".$attr->getCountProdsInListCatWithThis($cat_list)."</td>"; //Avec récursivité
                $out .= "<td><input type='checkbox' id='check-isfilter-attr_".$attr->getId_attribut()."' $is_filter onChange='setIsFilterAttrCat(this, $cat_id); return false' /></td>";
                $out .= "<td><button onClick='deleteAttribut(".$attr->getId_attribut().", &quot;".str_replace(array('"', "'"), " ",htmlentities($attr->getLabel_attr()))."&quot;)' class='btn btn-danger'><i class='fa fa-trash'></i> Suppr.</button></td>";
                $out .= "<td><label for='check-attr_".$attr->getId_attribut()."'><input class='selection' type='checkbox' id='check-attr_".$attr->getId_attribut()."' onChange='highlightSelection(this); return false'/></label></td>";
                $out .= "</tr>";
            }            
        }else{
            $out .= "<tr>";
            $out .= "<td><em>Aucun attribut dans la catégorie...</em></td>";
            $out .= "<td></td>";
            $out .= "<td></td>";
            $out .= "<td></td>";
            $out .= "</tr>";
        }            

                    
        return $out;
        
    }

    public function showProdListAttributs($listeProduits, $id_attribut)
    {

        ///////////////////////////////////////////////// BOUCLE PRODUITS /////////////////////////////////////////////////////////
        $out.= '<table class="table table-bordered">';
        $out .= '<thead>
                    <tr>
                        <td style="width:10%" >Produit</td>
                        <td style="width:25%">Nom</td>
                        <td style="width:30%">Description</td>
                        <td style="width:25%">Valeur attribut</td>
                    </tr>
                 </thead>
                 <tbody>';
        foreach ($listeProduits as $prod) {
        
            $out.='<tr>';
            ////////////////////////////////////////////// HTML ////////////////////////////////////////////////////////////////////
            //HTML pour la div du produit, l'id est l'ID produit //////////////////////////////////////////////////////////////////
            $out.= '<td><a target="blank" href="/app.php/catalogue/produit?id='.$prod->getId_produit().'">'.$prod->getSku().'</a></td>';
            $out.= '<td>'.truncateStr(htmlentities($prod->getName()), 50).'</td>';
            $out.= '<td>'.truncateStr(htmlentities($prod->getShort_description()), 50).'</td>';
            $out.= '<td>'.$prod->getValueForAttribut($id_attribut).'</td>';
            //$out.= '<td><button class="btn">test</button></td>';
            
            $out.='</tr>';
        }
        $out .= "</tbody>
                </table>";


        return $out;
    }

    public function showValuesListAttribut($values_list, $id_attribut, $sort_nb_prods_class)
    {
        $sort_nb_prods_icon ="";
                
        switch ($sort_nb_prods_class) {
            case 'cmpDesc':
                $sort_nb_prods_icon = "<i class='fa fa-sort-desc'></i>";
            break;
            case 'cmpAsc':
                $sort_nb_prods_icon = "<i class='fa fa-sort-asc'></i>";
            break;
        
            default:
                $sort_nb_prods_icon = "<i class='fa fa-sort'></i>";         
            break;
        }   
    
        ///////////////////////////////////////////////// BOUCLE PRODUITS /////////////////////////////////////////////////////////
        $out.= '<table class="table table-bordered">';
        $out .= '<thead>
                    <tr>
                        <td style="width:45%">Valeur</td>
                        <td style="width:20%"><a onClick="sortNbProdsValues(this, '.$id_attribut.');return false;" href="#" class="'.$sort_nb_prods_class.'">'.$sort_nb_prods_icon.'</a>Nb produits concernés</td>
                        <td style="width:20%">Actions</td>
                        <td style="width:15%">Sélection</td>
                    </tr>
                 </thead>
                 <tbody>';
        $c = 0;                 
        foreach ($values_list as $value) {
            $c++;
            $out.='<tr>';
            ////////////////////////////////////////////// HTML ////////////////////////////////////////////////////////////////////
            //HTML pour la div du produit, l'id est l'ID produit //////////////////////////////////////////////////////////////////
            $out.= '<td class="attr_value">'.$value['value'].'</td>';
            $out.= '<td>'.$value['c'].'</td>';
            $out.= '<td><button class="btn btn-primary" onClick="getDialogValeurAttribut(this, '.$id_attribut.')">Modifier</button></td>';
            $out .= "<td><label for='check-attr-value_".$id_attribut."_".$c."'><input class='selection_value' type='checkbox' id='check-attr-value_".$id_attribut."_".$c."' onChange='highlightSelection(this); return false;' /></label></td>";
            
            $out.='</tr>';
        }
        $out .= "</tbody>
                </table>";


        return $out;
    }
	
	/******************************************TO REMOVE UNDER THIS LINE *************************************************/
	/**
	 * Renvoi l'URL
	 */
	public function getUrlEasycamp() {
		$url = "http://www.francecamping.com/" . SystemLang::getUrlCampLang() . "/acces_camping/by_identifiant?identifiant_camping=" . $this -> _["ID"];
		return $url;
	}

	public function getUrlCampingCar() {
		$url = "http://aires.camping-car.com/aires/aire," . $this -> _["ID"];
		return $url;
	}

	/**
	 * Renvoi le libelle traduit d'un onglet du formulaire en fonction de son code
	 * @param int $code : code de l'onglet
	 * @return string : Le nom traduit de l'onglet
	 * TODO : mettre les variables int sous forme de var de config
	 */
	public function getNameOnglet($code) {
		switch ($code ) {
			case 1 :
				return _('Mon terrain');
				break;

			case 2 :
				return _('Informations Générales');
				break;
			case 3 :
				return _('Services, Equipements et loisirs');
				break;

			case 4 :
				return _('Animations');

				break;
			case 5 :
				return _('Tarifs');

				break;

			case 6 :
				return _('Camping Car');
				break;

			case 'SITUATION' :
				return _("Situation");
				break;

			default :
				return strtoupper($code);
				break;
		}
	}

	/**
	 * Renvoi le nom traduit pour une rubrique
	 * @param string $code : code pour lar ubrique
	 * @return string : libelle traduit de la rubrique
	 */
	public function getNameSS($code) {

		switch ($code ) {
			case 'CLASSEMENT' :
				return _("Classement");
				break;

			case 'CHAINE' :
				return _("Chaîne Commerciales");
				break;
			case 'OUVERTURE' :
				return _("Dates d'ouverture");
				break;

			case 'ACCUEIL' :
				return _("Accueil");

				break;
			case 'AIRECAMP' :
				return _("Emplacements");

				break;

			case 'DIVERS' :
				return _("Divers");
				break;
			case 'SITUATION' :
				return _("Situation");
				break;

			case 'DIVERSSERV' :
				return _("Services divers");
				break;
			case 'LOCATION' :
				return _("Location");

				break;

			case 'ALIMENTAT' :
				return _("Alimentation");

				break;
			case 'PRESTA' :
				return _("Prestations");

				break;

			case 'AQUA' :
				return _("Activités aquatiques");

				break;
			case 'SPORTS' :
				return _("Sports");

				break;

			case 'MATERIEL' :
				return _("Matériel");

				break;
			case 'JEUX' :
				return _("Jeux");

				break;

			case 'SUPPLEMENT' :
				return _("Tarifs unitaires ou suppléments");

				break;
			case 'SEJOUR' :
				return _("Séjours");
				break;

			case 'PROMO' :
				return _("Promotions");
				break;
			case 'PAIEMENT' :
				return _("Paiement");
				break;

			case 'DEVISE' :
				return _("Devise");

				break;
			case 'AIRESERV' :
				return _("Aire de service");

				break;

			case 'FFTSERVICE' :
				return _("Tarifs");

				break;
			case 'LABELS' :
				return _("Labels");

				break;

			case "INFOTARIF" :
				return _("Informations tarifs");

				break;
			case "FFEMPLACE" :
				return _("Forfait emplacements");

				break;
			default :
				return strtoupper($code);
				break;
		}

	}

	/**
	 * Renvoi le libelle traduit de decription de la valeur de caracteristique
	 */
	public function getTableHeadLabelSS($code) {

		switch ($code ) {
			case 'REMARQUE' :
				return _("Remarque");
				break;

			case 'DESCRIPTION' :
				return _("Description");
				break;
			case 'COCHESIGNIFICATIVE' :
				return _("Oui");
				break;

			case 'VALEUR' :
				return _("Valeur");

				break;
			case 'ACCESDIRECT' :
				return _("Sur place");

				break;
			case 'ACCESDIRECT2' :
				return _("Accès direct");

				break;
			case 'DISTANCE' :
				return _("Distance");

				break;
			case 'SUPERFICIE' :
				return _("Superficie");

				break;
			case 'NOM' :
				return _("Nom");
				break;
			case 'NBRTOTAL' :
				return _("Nbr total");
				break;
			case 'QTE1' :
				return _("Nbre de places ") . "<br/>" . _("mini");
				break;
			case 'TARIF1' :
				return _("Tarif (mini)");
				break;
			case 'QTE2' :
				return _("Nbre de places ") . "<br/>" . _("maxi");
				break;
			case 'TARIF2' :
				return _("Tarif (maxi)");
				break;
			case 'DUREE' :
				return _("Durée");
				break;
			case 'TARIF' :
				return _("Tarif");
				break;
			case 'INCLUS' :
				return _("Inclus");
				break;
			case 'DATES' :
				return _("Dates");
				break;
			case "VALTOURISME" :
				return _("Tourisme");
				break;
			case "VALLOISIRS" :
				return _("Loisirs");
				break;
			case "VALCAMPINGCAR" :
				return _("CampingCar");
				break;
			case "VALTENTE" :
				return _("Tente");
				break;

			default :
				break;
		}
	}

	/**
	 * Construit l'url avec parametre langue
	 */
	public static function makeUrlLang() {
		return (strpos($_SERVER['REQUEST_URI'], 'lang') ? substr($_SERVER['REQUEST_URI'], 0, strlen($_SERVER['REQUEST_URI']) - 2) : $_SERVER['REQUEST_URI'] . (strpos($_SERVER['REQUEST_URI'], "?") ? '&' : '?') . "lang=");

	}

	public function getUrlCampFrance() {
		$baseurl = "http://www.campingfrance.com/";
		if ($_SESSION['lang'] == 'fr_FR') {
			$url = $baseurl . "acces_camping/by_identifiant?identifiant_camping=" . $this -> _['ID'];
		} else
			$url = $baseurl . SystemLang::getUrlCampLang() . "/acces_camping/by_identifiant?identifiant_camping=" . $this -> _['ID'];
		return $url;
	}

	/**
	 * Construit un bout de formulaire pour un onglet (RUBRIQUE)
	 * @param int $k : ID de la rubrique
	 * @param array $fs : tableau de caracteristiques de cette rubrique
	 * @param array $mpfModelGest : toutes les donnees du terrain concerne
	 * @return string $out : Chaine HTML contenant le div de l'accordeon (representant la rubrique)
	 */
	public function genRubForm($k, $fs, $mpfModelGest) {

		$out .= "<h3 class='active'>" . $this -> getNameSS($k) . "</h3>";
		$out .= "<div>";
		$out .= $this -> genLignesFieldsForm($fs, $k, $mpfModelGest);
		$out .= "</div>";
		return $out;
	}

	/**
	 * Renvoi le code HTML (une table de champs (label - element)) constituant la rubrique
	 * @param array $fs : tableau de caracteristiques
	 * @param int $k : ID de la rubrique
	 * @param array $mpfModelGest : toutes les donnees du terrain concerne
	 * @return string $out : code HTML (table de champs de la rubrique)
	 */
	private function genLignesFieldsForm($fs, $k, $mpfModelGest) {
		$out .= "<table class='tab2'>";
		$out .= "<tr>";
		//
		$out .= "<th >" . $this -> getTableHeadLabelSS("DESCRIPTION") . "</th>";
		$out .= $this -> getTableHead($k);
		$out .= "</tr>";

		foreach ($fs as $v) {
			$out .= $this -> genLineField($v, $k, false, $mpfModelGest);
			//$out .=$this -> genLineField($v,$k);
		}

		$out .= "</table>";
		return $out;
	}

	public function getFormError($arrDesc, $tarif, $code2) {
		$out = "<b>" . _("Veuillez saisir les informations suivantes : ") . "</b><br/>";
		foreach ($arrDesc as $V) {
			$out .= " - " . $this -> getNameOnglet($V['ONGLET']) . " > " . ($V["DESCRIPTION"] ? $V["DESCRIPTION"] : $V["DESC2"]) . "<br/>";
		}

		if ($tarif == "TARIFSERVICE") {
			$out .= " - " . _("Veuillez renseigner l'année du Tarif pour la catégorie : 'Tarifs' de l'onglet 'Camping Car'") . "<br/>";
		} else if ($tarif != "false") {
			$out .= " - " . _("Veuillez renseigner l'année du Tarif pour la catégorie : 'Tarifs unitaires ou suppléments' ") . "<br/>";
		}
		if (isset($code2)) {
			$errSite = explode('^', $code2);
			foreach ($errSite as $E) {
				switch ($E) {
					case 'CPGEO' :
						$out .= " - " . $this -> getNameOnglet(1) . " > " . _('Code postal') . "<br/>";

						break;
					case 'VILLEGEO' :
						$out .= " - " . $this -> getNameOnglet(1) . " > " . _('Ville') . "<br/>";
						break;
				}
			}
		}

		return $out;
	}

	/**
	 * Renvoi le TH de titre pour un detail de rubrique
	 * En dur : on retrouve les colonnes en fonction de la rubrique passee
	 * @param string $k : Code de la rubrique
	 * @return string $out : Code HTML de titres (th contenant des td)
	 */
	private function getTableHead($k) {
		$out .= "";
		$HeaderConfig = array("CLASSEMENT" => array("COCHESIGNIFICATIVE", "VALEUR", "REMARQUE"), "AIRESERV" => array("COCHESIGNIFICATIVE", "VALEUR", "REMARQUE"), "CHAINE" => array("COCHESIGNIFICATIVE", "REMARQUE"), "OUVERTURE" => array("VALEUR"), "ACCUEIL" => array("COCHESIGNIFICATIVE"), "LABELS" => array("COCHESIGNIFICATIVE", "REMARQUE"), "DIVERS" => array("COCHESIGNIFICATIVE", "REMARQUE"), "AIRECAMP" => array("COCHESIGNIFICATIVE", "VALEUR", "REMARQUE"), "SITUATION" => array("COCHESIGNIFICATIVE", "ACCESDIRECT2", "DISTANCE", "SUPERFICIE", "NOM", "REMARQUE"), "DIVERSSERV" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "REMARQUE"), "ALIMENTAT" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "REMARQUE"), "JEUX" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "DISTANCE", "REMARQUE"), "PRESTA" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "REMARQUE"), "AQUA" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "DISTANCE", "REMARQUE"), "SPORTS" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "DISTANCE", "REMARQUE"), "LOCATION" => array("COCHESIGNIFICATIVE", "NBRTOTAL", "QTE1", "TARIF1", "QTE2", "TARIF2", "DUREE", "REMARQUE"), "MATERIEL" => array("COCHESIGNIFICATIVE", "ACCESDIRECT", "NBRTOTAL", "DUREE", "TARIF", "REMARQUE"), "FFTSERVICE" => array("COCHESIGNIFICATIVE", "INCLUS", "TARIF", "DUREE", "REMARQUE"), "SUPPLEMENT" => array("COCHESIGNIFICATIVE", "TARIF", "DUREE", "REMARQUE"), "SEJOUR" => array("DATES", "DUREE", "TARIF", "REMARQUE"), "PROMO" => array("COCHESIGNIFICATIVE", "DATES", "REMARQUE"), "PAIEMENT" => array("COCHESIGNIFICATIVE", "REMARQUE"), "FFEMPLACE" => array("COCHESIGNIFICATIVE", "REMARQUE", "VALTOURISME", "VALLOISIRS", "VALCAMPINGCAR", "VALTENTE"), "INFOTARIF" => array("COCHESIGNIFICATIVE", "VALEUR", "REMARQUE"), );
		$out = "";
		if (array_key_exists($k, $HeaderConfig)) {
			foreach ($HeaderConfig[$k] as $value) {
				$out .= "<th class='$value' >" . $this -> getTableHeadLabelSS($value) . "</th>";
			}
		}
		return $out;
	}

	private function genlinegenLineFirstFieldSufix($f) {
		switch ($f['CODE'] ) {
			case 'TARANCLOC' :
			case 'TARANCFOR' :
			case 'TARANCSUP' :
			case 'TARANCEMP' :
				$today = SystemParams::getParam("system*year_label");
				return $today;
				break;
			case 'TARNOUVLOC' :
			case 'TARNOUVFOR' :
			case 'TARNOUVSUP' :
			case 'TARNOUVEMP' :
				$today = SystemParams::getParam("system*year_label");
				return $today + 1;
				break;
			default :
				// return $f['CODE'];
				break;
		}
	}

	/**
	 * renvoi une classe css
	 */
	private function genLineFirstFieldStyle($f, $k) {
		$CssCeluleLigne = " ";
		if ($f['DECALAGE'] == 1) {
			$CssCeluleLigne .= "AvecDecalage ";
		} else {
			$CssCeluleLigne .= "SansDecalage ";
		}
		return trim($CssCeluleLigne);
	}

	/**
	 * Genere une ligne dans le formulaire
	 * @param array $f : tableau de valeur d'une caracteristique
	 * @param string $k : ID de la rubrique
	 * @param bool $oldInfo : si c'est une valeur historisee, par defaut : false
	 * @param array $mpfModelGest : toutes les donnees du terrain concerne
	 */
	private function genLineField($f, $k, $oldInfo = false, $mpfModelGest = null) {

		$InformationHtml = "";

		if ($f['INFORMATION'] != null && SystemLang::getUrlCampLang() == 'fr') {
			$InformationHtml = "<img style='margin-left: 10px;' src='/skins/common/images/image001.png'> <span class='info'>" . $f['INFORMATION'] . "</span>";
		}

		if (stripos(strtoupper($f["INFORMATION"]), 'SUR PLACE') !== false) {
			$isPlace = true;
		}
		if ($f['OBLIGATOIRE'] == 1) {

			$ObmigatoireHtml = "*";
			$addhtml = "oblig";
			$addhtml2 = "oblig2";

		} elseif (($f['CODE'] == 'TARNOUVFOR' && $mpfModelGest['TYPEDESITECODE'] == 'SERVICE') || ($f['CODE'] == 'TARANCFOR' && $mpfModelGest['TYPEDESITECODE'] == 'SERVICE')) {

			/*
			 $ObmigatoireHtml="*";
			 $addhtml = "oblig";
			 $addhtml2 = "oblig2";
			 */
			//var_dump($mpfModelGest);
		} else {
			$ObmigatoireHtml = "";
			$addhtml = "";
			$addhtml2 = "";

		}

		if ($f['DECALAGE'] == 1) {
			$bdesc = "<ul class='uldesc'><li class='lidesc'>";
			$adecv = "</li></ul>";
		} else {
			$bdesc = '';
			$adecv = '';
		}

		$out .= "<tr  class='$addhtml2'  >";
		$out .= //"<td class=\"".$this->genLineFirstFieldStyle($f,$k)." \"></td>".
		"<td class=\" form_label " . $this -> genLineFirstFieldStyle($f, $k) . "  $addhtml \"> " . $bdesc . ($f['DESCRIPTION'] != "" ? $f['DESCRIPTION'] : $f['DESC2']) . " " . $this -> genlinegenLineFirstFieldSufix($f) . " " . $ObmigatoireHtml . " " . $InformationHtml . $adecv . " </td>";
		switch ($k) {
			case "CLASSEMENT" :
				//$out .=   $this->genLineFieldClassement($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				switch ($f['CODE']) {
					case 'CLASSEMNT' :
					case 'ARPREFET' :
					case 'PRL' :
						$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
						break;
					default :
						$out .= "<td></td>";
				}
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;

			case "OUVERTURE" :
				//$out .=   $this->genLineFieldOuverture($f,$k);
				$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
				break;
			case "ACCUEIL" :
				//$out .=   $this->genLineFieldAccueil($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				break;
			case "DIVERSSERV" :
			case "ALIMENTAT" :
			case "PRESTA" :
				if ($isPlace != false) {
					$out .= $this -> genEmpty();
					$out .= $this -> genACCESDIRECT($f, $oldInfo);
					//$out .= $this->genEmpty();
				} else {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genACCESDIRECT($f, $oldInfo);
					//$out .= $this->genIpuField($f,"DISTANCE", $oldInfo);
				}
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;
			case "AQUA" :
			case "JEUX" :
			case "SPORTS" :
				//$out .= $this->genLineFieldCochesignificativeAccesdirectDistanceRemarque($f,$k);
				// $out .= $this->genCOCHESIGNIFICATIVE($f, $oldInfo);
				if ($isPlace != false) {
					$out .= $this -> genEmpty();
					$out .= $this -> genACCESDIRECT($f, $oldInfo);
					$out .= $this -> genEmpty();
				} else {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genACCESDIRECT($f, $oldInfo);
					$out .= $this -> genIpuField($f, "DISTANCE", $oldInfo);
				}
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;

			case "CHAINE" :
			case "PAIEMENT" :
			case "LABELS" :
			case "DIVERS" :
				//$out .= $this->genLineFieldDivers($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;

			case "SITUATION" :
				//$out .=  $this->genLineFieldSituation($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				$out .= $this -> genACCESDIRECT($f, $oldInfo);
				$out .= $this -> genIpuField($f, "DISTANCE", $oldInfo);
				$out .= $this -> genIpuField($f, "SUPERFICIE", $oldInfo);
				$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;
			case "LOCATION" :
				//$out .=  $this->genLineFieldLocation($f,$k);
				if ($f['CODE'] == "TARANCLOC" || $f["CODE"] == "TARNOUVLOC") {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();

				} else {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genIpuField($f, "VALEUR", $oldInfo, "NBRTOTAL");
					$out .= $this -> genIpuField($f, "QTE1", $oldInfo);
					$out .= $this -> genIpuField($f, "TARIF1", $oldInfo);
					$out .= $this -> genIpuField($f, "QTE2", $oldInfo);
					$out .= $this -> genIpuField($f, "TARIF2", $oldInfo);
					$out .= $this -> genIpuField($f, "DUREE", $oldInfo);
					$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				}

				break;
			case "MATERIEL" :
				//$out .=  $this->genLineFieldCochesignificativeAccesdirectNbrtotalDureeTarifDistanceRemarque($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				$out .= $this -> genACCESDIRECT($f, $oldInfo);

				$out .= $this -> genIpuField($f, "VALEUR", $oldInfo, $k);
				//HERE !!!! NEED TO FORCE CLASS INPUT
				$out .= $this -> genIpuField($f, "DUREE", $oldInfo);
				$out .= $this -> genIpuField($f, "TARIF1", $oldInfo);
				//$out .= $this->genIpuField($f,"DISTANCE", $oldInfo);
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;
			case "FFTSERVICE" :
				//$out .=  $this->genLineFieldCochesignificativeAccesdirectDureeTarifRemarque($f,$k);
				if ($f['CODE'] == "TARANCFOR" || $f["CODE"] == "TARNOUVFOR") {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();

				} else {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genACCESDIRECT($f, $oldInfo);
					$out .= $this -> genIpuField($f, "TARIF1", $oldInfo);
					$out .= $this -> genIpuField($f, "DUREE", $oldInfo);
					$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				}
				break;
			case "SUPPLEMENT" :
				//$out .= $this->genLineFieldSupplement($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				if ($f['COCHESIGNIFICATIVE'] == "O") {
					$out .= "<td colspan='3'>&nbsp;</td>";
				} else {
					//$out .=  $this->genLineFieldDureeTarifRemarque($f,$k);
					$out .= $this -> genIpuField($f, "TARIF1", $oldInfo);
					$out .= $this -> genIpuField($f, "DUREE", $oldInfo);
					$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				}

				break;
			case "SEJOUR" :
				//$out .=$this->genLineFieldDatesDureeTarifRemarque($f,$k);
				$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
				$out .= $this -> genIpuField($f, "DUREE", $oldInfo);
				$out .= $this -> genIpuField($f, "TARIF1", $oldInfo);
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				//$out .= $this->genLineFieldDureeTarifRemarque($f,$k);
				break;
			case "PROMO" :
				//$out .=$this->genLineFieldCochesignificativeDatesRemarque($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;

			case "FFEMPLACE" :
				//$out .= $this->genLineFieldForfaitEmp($f,$k);
				if ($f['CODE'] == 'TARANCEMP' || $f['CODE'] == "TARNOUVEMP") {
					$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();
					$out .= $this -> genEmpty();

				} else {
					$out .= $this -> genEmpty();
					$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
					if ($f['COCHESIGNIFICATIVE'] == "O") {

						$out .= $this -> genCK($f, "VALTOURISME", $oldInfo);
						$out .= $this -> genCK($f, "VALLOISIRS", $oldInfo);
						$out .= $this -> genCK($f, "VALCAMPINGCAR", $oldInfo);
						$out .= $this -> genCK($f, "VALTENTE", $oldInfo);
					} else {

						$out .= $this -> genIpuField($f, "VALTOURISME", $oldInfo);
						$out .= $this -> genIpuField($f, "VALLOISIRS", $oldInfo);
						$out .= $this -> genIpuField($f, "VALCAMPINGCAR", $oldInfo);
						$out .= $this -> genIpuField($f, "VALTENTE", $oldInfo);
					}
				}
				break;

			case 'INTERNET' :
			case "TELEPHONE" :
				$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
				break;
			default :
				//$out .=  $this->genLineFieldDefault($f,$k);
				$out .= $this -> genCOCHESIGNIFICATIVE($f, $oldInfo);
				if ($f['COCHESIGNIFICATIVE'] == "O") {
					$out .= "<td></td>";
				} else {
					$out .= $this -> genIpuField($f, "VALEUR", $oldInfo);
				}
				$out .= $this -> genIpuField($f, "REMARQUE", $oldInfo);
		}

		if ($this -> _["admin"] && $oldInfo == false) {
			//    $data= $f['ACCESDIRECT'].$f['SUPERFICIE'].$f['DISTANCE'].$f['DUREE'].$f['VALEUR'].$f['VALTOURISME'].$f['VALLOISIRS'];
			//var_dump();  var_dump($this->_["ADATA"]['ACCUEILFR']);
			//if($f["CODE"])
			$temp = $f;
			//unset($temp["CODE"]);
			unset($temp["DESCRIPTION"]);
			unset($temp["DESC2"]);
			unset($temp["DECALAGE"]);
			unset($temp["OBLIGATOIRE"]);
			unset($temp["COCHESIGNIFICATIVE"]);
			unset($temp["INFORMATION"]);
			//unset($temp["CARACTERISTIQUECODE"]);

			//if($f["CODE"]=="ACCUEILFR") var_dump($this->_["ADATA"][$f["CODE"]]);

			// Si il ya modifications, alors on ajoute l'icone qui recharge les anciennes valeurs
			if ($this -> arrayRecursiveDiff($temp, $this -> _["ADATA"][$f['CODE']])) {
				foreach ($this->_["ADATA"][$f['CODE']] as $k2 => $v2) {
					$f[$k2] = $v2;
				}
				$out .= "<td><div class='info_label' href='#' id='" . $f['CODE'] . "' onclick='loadOldData(this)'>
                        <img class='imgad' style='margin-left: 10px;' src='/skins/common/images/fck_Undo.gif'> 
                        <div class='info2'>";
				$out .= "<table class='tab2'>";
				$out .= "<tr>";
				$out .= "<th>" . $this -> getTableHeadLabelSS("DESCRIPTION") . "</th>";
				$out .= $this -> getTableHead($k);
				$out .= "</tr><tr>" . $this -> genLineField($f, $k, true, $mpfModelGest) . "</tr></table>" . "</div></div></td>";
			}
		}
		$out .= "</tr>";

		return $out;
	}

	/**
	 * Revoi un tableau
	 */
	function arrayRecursiveDiff($aArray1, $aArray2) {
		$aReturn = array();

		foreach ($aArray1 as $mKey => $mValue) {
			if (array_key_exists($mKey, $aArray2)) {
				if (is_array($mValue)) {
					$aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]);
					if (count($aRecursiveDiff)) { $aReturn[$mKey] = $aRecursiveDiff;
					}
				} else {
					if ($mValue != $aArray2[$mKey]) {
						$aReturn[$mKey] = $mValue;
					}
				}
			} else {
				$aReturn[$mKey] = $mValue;
			}
		}

		return $aReturn;
	}

	//      "FFEMPLACE"=>array("COCHESIGNIFICATIVE", "VALEUR", "VALTOURISME", "VALLOISIRS", "VALCAMPING", "VALTENTE"),

	private function genLineFieldForfaitEmp($f, $l) {
		if ($f['CODE'] == 'TARANCEMP' || $f['CODE'] == "TARNOUVEMP") {
			$out .= $this -> genCOCHESIGNIFICATIVE($f);
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();

		} else {
			$out .= $this -> genEmpty();
			$out .= $this -> genIpuField($f, "REMARQUE");
			if ($f['COCHESIGNIFICATIVE'] == "O") {

				$out .= $this -> genCK($f, "VALTOURISME");
				$out .= $this -> genCK($f, "VALLOISIRS");
				$out .= $this -> genCK($f, "VALCAMPINGCAR");
				$out .= $this -> genCK($f, "VALTENTE");
			} else {

				$out .= $this -> genIpuField($f, "VALTOURISME");
				$out .= $this -> genIpuField($f, "VALLOISIRS");
				$out .= $this -> genIpuField($f, "VALCAMPINGCAR");
				$out .= $this -> genIpuField($f, "VALTENTE");
			}
		}
		return $out;
	}

	private function genLineFieldCochesignificativeRemarque($f, $k) {
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;

	}

	private function genLineFieldSupplement($f, $k) {

		$out = $this -> genCOCHESIGNIFICATIVE($f);
		if ($f['COCHESIGNIFICATIVE'] == "O") {
			$out .= "<td colspan='3'>&nbsp;</td>";
		} else {
			$out .= $this -> genLineFieldDureeTarifRemarque($f, $k);
		}

		return $out;
	}

	private function genLineFieldDureeTarifRemarque($f, $k) {

		$out = "";
		$out .= $this -> genIpuField($f, "DUREE");
		$out .= $this -> genIpuField($f, "TARIF1");
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldDatesDureeTarifRemarque($f, $k) {
		$out .= $this -> genIpuField($f, "VALEUR");
		$out .= $this -> genLineFieldDureeTarifRemarque($f, $k);
		return $out;

	}

	private function genLineFieldCochesignificativeDatesRemarque($f, $k) {
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genIpuField($f, "VALEUR");
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;

	}

	private function genLineFieldDefault($f, $k) {
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		if ($f['COCHESIGNIFICATIVE'] == "O") {
			$out .= "<td></td>";
		} else {
			$out .= $this -> genIpuField($f, "VALEUR");
		}
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldClassement($f, $k) {
		//		var_dump($f);
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		switch ($f['CODE']) {
			case 'CLASSEMNT' :
				$out .= $this -> genIpuField($f, "VALEUR");
				break;
			case 'PRL' :
				$out .= $this -> genIpuField($f, "VALEUR");
				break;
			case 'ARPREFET' :
				$out .= $this -> genIpuField($f, "VALEUR");
				break;
			default :
				$out .= "<td></td>";
		}
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldChaine($f, $k) {
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldOuverture($f, $k) {
		$out = $this -> genIpuField($f, "VALEUR");
		return $out;
	}

	/**
	 * Renvoi le code HTML pour un TD vide
	 */
	private function genEmpty() {
		return "<td></td>";
	}

	private function genLineFieldDivers($f, $k) {
		$out .= $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genIpuField($f, "REMARQUE");

		return $out;
	}

	private function genLineFieldAccueil($f, $k) {
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		return $out;

	}

	private function genLineFieldAirecamp($f, $k) {
		//var_dump($f);
		$out = "";
		$out .= $this -> genCOCHESIGNIFICATIVE($f);
		if ($f['COCHESIGNIFICATIVE'] == "O") {
			$out .= "<td></td>";
		} else {
			$out .= $this -> genIpuField($f, "VALEUR");
		}
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	/**
	 * Renvoi le code HTML du TD avec l'input
	 * @param array $f : caracteristique (tableau d'atributys)
	 * @param string $id : Code de l'attribut de caracteristique
	 * @param bool $oldInfo : si valeur historisee : si true: input disabled
	 * @param string $addcss : chaine a ajouter a l id css
	 * @return string $out : code HTML
	 */
	private function genIpuField($f, $id, $oldInfo, $addcss = "") {
		if ($oldInfo)
			$add = "_O";
		else
			$add = "";

		$out = "<td><input value=\"" . $f[$id] . "\"  
                    name='" . $f['CODE'] . "_" . $id . $add . "' 
                    id='" . $f['CODE'] . "_" . $id . $add . "' 
                    " . ($oldInfo ? "DISABLED" : "") . " 
                    type='text' class=\"inputTimesheetLine Feild" . ucfirst(strtolower($id)) . " " . $addcss . "\"></td>";
		return $out;
	}

	/**
	 * Renvoi le code HTML pour un TD avec checkbox
	 */
	private function genCK($f, $id, $oldInfo) {
		if ($oldInfo)
			$add = "_O";
		else
			$add = "";
		$out = "<td>";
		$out .= "<input name='" . $f['CODE'] . "_" . $id . $add . "' 
                    id='" . $f['CODE'] . "_" . $id . $add . "'  
                    type='checkbox' " . ($oldInfo ? "DISABLED" : "") . "
                    " . (($f[$id] == 'O' || $f[$id] == 'on') ? "CHECKED" : "") . " ></td>";
		return $out;
	}

	/**
	 * Renvoi le code HTML du TD avec checkbox pour acces direct
	 * @param array $f : caracteristique
	 * @param bool $oldInfo : si valeur historisee : disabled
	 * @return string $out : code HTML
	 */
	private function genACCESDIRECT($f, $oldInfo) {
		if ($oldInfo)
			$add = "_O";
		else
			$add = "";
		$out = "<td>";
		$out .= "<input name='" . $f['CODE'] . "_ACCESDIRECT" . $add . "' 
	               id='" . $f['CODE'] . "_ACCESDIRECT" . $add . "'  
	              type='checkbox' " . ($oldInfo ? "DISABLED" : "") . " 
	               " . ($f['ACCESDIRECT'] != null ? "CHECKED" : "") . " ></td>";
		return $out;
	}

	/**
	 * Renvoi le code HTML du TD avec checkbox OUI/NON
	 * si la caracteristique a l'attribut COCHESIGNIFICATIVE à O (oui)
	 * @param array $f : caracteristique (tableau d'attributs)
	 * @param bool $oldInfo : si c'est une valeur historisee : si true, alors checkbox disabled
	 */
	private function genCOCHESIGNIFICATIVE($f, $oldInfo) {
		if ($oldInfo)
			$add = "_O";
		else
			$add = "";
		$out = "<td>";
		if ($f['COCHESIGNIFICATIVE'] == "O") {
			// delit() vide les inputs associes
			$out .= "<input name='" . $f['CODE'] . "_CK" . $add . "' onclick = 'delit(this);' 
                    id='" . $f['CODE'] . "_CK" . $add . "' type='checkbox' " . ($oldInfo ? "DISABLED" : "") . " 
                    " . ($f['CARACTERISTIQUECODE'] != null ? "CHECKED" : "") . "></td>";
		} else {
			$out .= "</td>";
		}
		return $out;
	}

	private function genLineFieldSituation($f, $k) {
		$out = "";
		$out .= $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genACCESDIRECT($f);
		$out .= $this -> genIpuField($f, "DISTANCE");
		$out .= $this -> genIpuField($f, "SUPERFICIE");
		$out .= $this -> genIpuField($f, "VALEUR");
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldLocation($f, $k) {
		$out = "";
		// NBRTOTAL QTE1 TARIF1  QTE2 TARIF2 DUREE REMARQUE
		// Nbr total  mini Tarif maxi Tarif Durée Remarque
		$out .= $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genIpuField($f, "VALEUR");
		$out .= $this -> genIpuField($f, "QTE1");
		$out .= $this -> genIpuField($f, "TARIF1");
		$out .= $this -> genIpuField($f, "QTE2");
		$out .= $this -> genIpuField($f, "TARIF2");
		$out .= $this -> genIpuField($f, "DUREE");
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldCochesignificativeAccesdirectDistanceRemarque($f, $k) {
		$out = "";
		$out .= $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genACCESDIRECT($f);
		$out .= $this -> genIpuField($f, "DISTANCE");
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldCochesignificativeAccesdirectNbrtotalDureeTarifDistanceRemarque($f, $k) {
		$out = "";
		//    NBRTOTAL QTE1 TARIF1  QTE2 TARIF2 DUREE REMARQUE
		// Nbr total  mini Tarif maxi Tarif Durée Remarque
		$out .= $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genACCESDIRECT($f);
		$out .= $this -> genIpuField($f, "VALEUR");
		$out .= $this -> genIpuField($f, "TARIF1");
		$out .= $this -> genIpuField($f, "DUREE");
		$out .= $this -> genIpuField($f, "DISTANCE");
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

	private function genLineFieldCochesignificativeAccesdirectDureeTarifRemarque($f, $k) {
		$out = "";
		//    NBRTOTAL QTE1 TARIF1  QTE2 TARIF2 DUREE REMARQUE
		// Nbr total  mini Tarif maxi Tarif Durée Remarque

		if ($f['CODE'] == "TARANCFOR" || $f["CODE"] == "TARNOUVFOR") {
			$out .= $this -> genCOCHESIGNIFICATIVE($f);
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();
			$out .= $this -> genEmpty();

		} else {
			$out .= $this -> genCOCHESIGNIFICATIVE($f);
			$out .= $this -> genACCESDIRECT($f);
			$out .= $this -> genIpuField($f, "DUREE");
			$out .= $this -> genIpuField($f, "TARIF1");
			$out .= $this -> genIpuField($f, "REMARQUE");
		}
		return $out;
	}

	private function genLineFieldLabels($f, $k) {
		$out = $this -> genCOCHESIGNIFICATIVE($f);
		$out .= $this -> genIpuField($f, "REMARQUE");
		return $out;
	}

}
