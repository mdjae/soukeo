<?php

class BusinessCustomer extends BusinessEntity {
	
	protected static 	$db ;
	
    protected $entity_id;
	protected $customer_id;
	protected $name;
	protected $company; 
	protected $firstname;
	protected $lastname;
	protected $email;
	protected $phone;
    protected $phone2;
    protected $birthd;
    protected $type;
    protected $statut;
    protected $more_info;
	protected $date_created;
	protected $date_updated;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();    

        if (!empty($data)) {
            $this->hydrate($data);
        }   	
	}
	
    public function setEntity_id($entity_id)
    {
        $this -> entity_id = $entity_id;
        return $this;
    }
    	
	public function setCustomer_id($customer_id)
	{
		$this -> customer_id = $customer_id;
        return $this;
	}
    
	public function setName($name)
	{
		$this -> name = $name;
        return $this;
	}
	
	public function setCompany($company)
	{
		$this -> company = $company;
        return $this;
	}
	
	public function setFirstname($firstname)
	{
		$this -> firstname = $firstname;
        return $this;
	}
	
	public function setLastname($lastname)
	{
		$this -> lastname = $lastname ;
        return $this;
	}
	
	public function setEmail($email)
	{
		$this -> email = trim(strtolower($email));
        return $this;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
        return $this;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
        return $this;
	}
	
	public function setPhone($phone)
	{
		$this -> phone = $phone;	
        return $this;
	}
    
    public function setPhone2($phone2)
    {
        $this -> phone2 = $phone2;   
        return $this; 
    }
    
    public function setBirthd($birthd)
    {
        $this -> birthd = $birthd;
        return $this;
    }
    
    public function setType($type)
    {
       $this -> type = $type;
       return $this;
    }

    public function setStatut($statut)
    {
        $this -> statut = $statut;
        return $this;
    }
    
    public function setMore_info($more_info)
    {
        $this -> more_info = $more_info;
        return $this;
    }

    public function getEntity_id()
    {
        return $this -> entity_id ;
    }
    	
	public function getCustomer_id()
	{
		return $this -> customer_id;
	}
	
	public function getName()
	{
		return $this -> name;
	}
	
	public function getCompany()
	{
		return $this -> company;
	}
	
	public function getFirstname()
	{
		return $this -> firstname;
	}
	
	public function getLastname()
	{
		return $this -> lastname;
	}
	
	public function getEmail()
	{
		return $this -> email;
	}
	
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
	
	public function getPhone()
	{
		return $this -> phone;
	}

    public function getPhone2()
    {
        return $this -> phone2;
    }
    
    public function getBirthd()
    {
        return $this -> birthd;
    }
    
    public function getType()
    {
       return $this -> type ;
    }

    public function getStatut()
    {
        return $this -> statut;
    }
    
    public function getMore_info()
    {
        return $this -> more_info;
    }
    
    public function getStatut_label()
    {
        $statutManager = new ManagerCustomerStatut();
        $status = $statutManager -> getById(array("statut_id" => $this -> statut)) ;
        
        if($status) {
            return ucfirst( $status -> getLabel());
        }
        else {
            return "<em style='color:red'>Ref statut inconnue</em>";
        }
    }
    	
	public function getAllOrders()
	{
		return self::$db -> getAllOrdersByCustomer($this -> getCustomer_id());
	}
    
    public function getCA()
    {
        $all_orders = self::$db -> getAllOrdersPaidByCustomer($this -> getCustomer_id());
        
        $ca = 0;
        
        foreach ($all_orders as $order) {
            
            $ca += (float)$order->getBase_grand_total();
        }
        
        return $ca;
    }
    
    public function getAllOrdersByMail()
    {
        return self::$db -> getAllOrdersByCustomerMail($this -> getEmail());
    }
    
    public function isNew()
    {
        $custManager = new ManagerCustomer();
        
        if ($mail = $custManager -> getBy(array("email" => $this -> email))){
            
            return false;
        }else{
            return true;
        }
    }
    
	public function save()
	{
		self::$db -> updateCustomer($this);
	}
	
}