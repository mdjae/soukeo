<?php 
/**
 * 
 */
class BusinessTicketState extends BusinessEntity {
	
    
	protected $state_id;
    protected $label;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setState_id($state_id)
    {
        $this -> state_id = $state_id;
    }
    
    public function getState_id()
    {
        return $this -> state_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = $label;
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function isNew()
    {
        $manager = new ManagerTicketState();
        
        if ($this -> state_id || ($state = $manager -> getBy(array("label" => $this->label) )) ){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>