<?php 
/**
 * 
 */
class BusinessRelanceType extends BusinessEntity {
	
    
	protected $type_id;
    protected $label;
    
    public function __construct ($data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
        }
    }
    
    public function setType_id($type_id)
    {
        $this -> type_id = $type_id;
    }
    
    public function getType_id()
    {
        return $this -> type_id ;
    }
    
    public function setLabel($label)
    {
        $this -> label = trim($label);
    }
    
    public function getLabel()
    {
        return $this -> label;
    }
    
    public function isNew()
    {
        $manager = new ManagerRelanceType();
        
        if ($this -> type_id || ($state = $manager -> getBy(array("label" => $this->label)))){
            return false;
        }else{
            return true;
        }
    }

    public function save()
    {

    }
    
}

 ?>