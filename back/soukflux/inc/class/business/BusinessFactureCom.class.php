<?php

class BusinessFactureCom extends BusinessEntity {
	
	protected static 	$db ;
	
	protected $facture_com_id;
	protected $facture_id;
	protected $vendor_id;
	protected $date_created;
	protected $date_updated;
	protected $total_prix;
	protected $total_comm;
	protected $total_frais_banq;
	protected $grand_total;
	protected $mt_a_transferer;
	protected $date_limit;
	protected $email;
	protected $tel;
    protected $email_send;
	

	public function __construct ($data = array()) {
		
		self::$db = new BusinessSoukeoModel();	
		if (!empty($data)) {
			$this->hydrate($data);
		}
	}
	
	public function setFacture_com_id($facture_com_id)
	{
		$this -> facture_com_id = $facture_com_id;
	}
		
	public function setFacture_id($facture_id ){
		$this -> facture_id = $facture_id ;
	}
	
	public function setVendor_id($vendor_id)
	{
		$this -> vendor_id = $vendor_id;
	}
	
	public function setTotal_prix($total_prix)
	{
		$this -> total_prix = $total_prix;
	}
	
	public function setTotal_comm($total_comm)
	{
		$this -> total_comm = $total_comm;
	}
	
	public function setTotal_frais_banq($total_frais_banq)
	{
		$this -> total_frais_banq = $total_frais_banq;
	}
	
	public function setGrand_total($grand_total)
	{
		$this -> grand_total = $grand_total;
	}
	
	public function setMt_a_transferer($mt_a_transferer)
	{
		$this -> mt_a_transferer = $mt_a_transferer;
	}
	
	public function setDate_limit($date_limit)
	{
		$this -> date_limit = $date_limit;
	}
	
	public function setEmail($email)
	{
		$this -> email = $email;
	}
	
	public function setTel($tel)
	{
		$this -> tel = $tel;
	}
	
	public function setDate_created($date_created)
	{
		$this -> date_created = $date_created;
	}
	
	public function setDate_updated($date_updated)
	{
		$this -> date_updated = $date_updated;
	}
    
    public function setEmail_send($email_send)
    {
        $this -> email_send = $email_send;
    }

	public function getFacture_com_id()
	{
		return $this -> facture_com_id ;
	}
	
	public function getFacture_id(){
		return $this -> facture_id;
	}
	
	public function getVendor_id()
	{
		return $this -> vendor_id;
	}

	public function getTotal_prix()
	{
		return $this -> total_prix;
	}
	
	public function getTotal_comm()
	{
		return $this -> total_comm;
	}
	
	public function getTotal_frais_banq()
	{
		return $this -> total_frais_banq;
	}
	
	public function getGrand_total()
	{
		return $this -> grand_total;
	}
	
	public function getMt_a_transferer()
	{
		return $this -> mt_a_transferer;
	}
	
	public function getDate_limit()
	{
		return $this -> date_limit;
	}

	public function getEmail()
	{
		return $this -> email ;
	}
	
	public function getTel()
	{
		return $this -> tel;
	}
		
	public function getDate_created()
	{
		return $this -> date_created;
	}
	
	public function getDate_updated()
	{
		return $this -> date_updated;
	}
    
    public function getEmail_send()
    {
        return $this -> email_send;
    }
    
    public function getAllLines()
    {
        $factureComLineManager = new ManagerFactureComLine();
        
        return $factureComLineManager -> getAllFactureComLineForFactureCom ($this -> facture_com_id);
    }
	
	public function isNew()
	{
		if ($this -> getFacture_com_id()){
			return false;
		}else{
			return true;
		}
	}

	public function save()
	{

	}
	
}