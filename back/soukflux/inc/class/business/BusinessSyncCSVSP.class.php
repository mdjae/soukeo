<?php 

/**
 * Class type de script qui fait la mise a jour de la table stock/prix suivant la table association
 */
class BusinessSyncCSVSP {
	
	//protected static $nbRun 		= 0; 		//Compte le nombre de fois que ce batch a été executé
	
	protected $csv_to_parse 		= "";		//URL de l'ecommerçant à parser
	protected $vendorID 			= ""; 		//VENDOR_ID de l'ecommercant
	protected $productListAttribute = null; 	//Liste produits 
	protected static $nbSyncSP 		= 0; 		//log
	
	protected $delimiter 			= ";";
	protected static $db;
	protected $nameCateg			=""; 		// nom générique pour la catégory qui prendra le nom du fichier CSV
	protected $rep = "/opt/soukeo/import/commercants/";
	
	/**
	 * Constructeur, cette classe prend en paramètre le CSV et l'ID du vendeur
	 */
	function __construct($csv, $vendorID) 
	{
		$this->nameCateg 		= $csv ;	
		$this->csv_to_parse 	= $this->rep . $csv . ".csv";
		$this->vendorID 		= $vendorID;
		self::$nbSyncSP = 0;
	}
	
	/**
	 * Appelle les fonctions d'insertion en BDD
	 */
	public function run()
	{
		self::$db = new BusinessSoukeoCSVModel ();
		//On parse le CSV de l'e-commercant dispo à l'URL fourni
		$this->parseCSV($this->csv_to_parse);
		self::$nbSyncSP++;		
		return true;

	}
	/**
	 * @author Philippe_LA/FHKC
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param url 				String 		L'url qui va contenir le CSV
	 * @param nbChampsFixes 	Int 		Le nombre de champs fixe qui sont communs à tous produits
	 * @param delimiter 		String 		Le délimiteur choisi dans le CSV
	 */
	protected function parseCSV($file)
	{
		self::$db->prepareStmtProdEcommercant($this->vendorID);
		
		
		if (($handle = fopen($file, "r")) !== FALSE) {
			$headerList = array();
			$product = array();
			$row = 0;
		    while (($data = fgetcsv($handle, 0, $this -> delimiter)) !== FALSE) {
		    	
		        $num = count($data);

		        
		       	//pour chaque colonnes
		        for ($c=0; $c < $num; $c++) {
		        	
					if($row == 0){
						$headerList[] = trim(BusinessEncoding::toUTF8(html_entity_decode($data[$c])));
						
					}else{
						$product[] = trim(BusinessEncoding::toUTF8(html_entity_decode($data[$c])));
					}	
		        }
				
				
				//Si on est pas sur l'entete
				if($row != 0) {
					//Création du produit avec ses colonnes associatives
					$product = array_combine($headerList, $product);

						
					//$product = $this -> cleanDescription($product);
					$product = $this -> formatFields($product);
					//var_dump($product);break;
					
					
					if (self::$db->productIsAssoc($this->vendorID,$product['ID_PRODUCT'])){// a rajouté aprés pour verif la date &&self::$db->verifDate_upBddLocalImport($product)){

				        $test = self::$db->addRowSfkProdEcomSP($product, $this->vendorID); 					 	
			    		if ($test) self::$nbSyncSP++;
					    	
					}
					
				}	
	        
				unset($product);	
				$row++;
		    }
		fclose($handle);	
		}
		
	}

	protected function formatFields($product){
		
		$product['ID_PRODUCT'] = $this->vendorID=="61" ? str_replace(' ', '_', $product['CODE'])."_61" : str_replace(' ', '_', $product['CODE']);
		$product['NAME_PRODUCT'] = $product['NOM'];
		$product['QUANTITY'] = $product['STOCK'];
		$product['PRICE_PRODUCT'] = $product['PRIX_PRODUIT']!=null? trim(str_replace(',', '.',$product['PRIX_PRODUIT'])) : trim(str_replace(',', '.',$product['PRIX_TTC'])) ;
		$product['PRICE_TTC'] = $product['PRIX_TTC'] !=null? trim(str_replace(',', '.',$product['PRIX_TTC'])) : trim(str_replace(',', '.',$product['PRIX_PRODUIT']))  ;
		$product['WEIGHT'] = trim(str_replace(',', '.',$product['POIDS']));
		$product['IMAGE_PRODUCT'] = $product['IMAGE_URL'];
		$product['IMAGE_PRODUCT_2'] = $product['IMAGE_URL_2']!=null?$product['IMAGE_URL_2'] : $product['IMAGE_URL'];
		$product['IMAGE_PRODUCT_3'] = $product['IMAGE_URL_3']!=null?$product['IMAGE_URL_3'] : $product['IMAGE_URL'];
		$product['DESCRIPTION'] = $product['DESCRIPTION_LONGUE'];
		$product['DESCRIPTION_SHORT'] = $product['DESCRIPTION_COURTE'];
		$product['META_DESCRIPTION'] = $product['META_DESCRIPTION'];
		$product['META_KEYWORDS'] = $product['META_KEYWORDS'];
		$product['MANUFACTURER'] = $product['FABRIQUANT'];
		$product['CATEGORY'] = $product['CATEGORY'] == null ? $this->nameCateg : $product['CATEGORY'] ;
		$product['REFERENCE_PRODUCT'] = $product['REFERENCE_PRODUIT']==null?$product['CODE']:$product['CODE'];
		$product['EAN'] = $product['EAN'];
 	
		return $product;
	}
			
	/**
	 * @return $nbSyncSP le nombre de fois que la syncho SPP a été effectué
	 */
	public static function getNbSyncSP()
	{
		return self::$nbSyncSP;
	}
	
}
?>