<?php

class BusinessStatistiqueModel {
    
    protected static $db;
    protected $month_begin      = null;
    protected $month_end        = null;
    protected $sql_month        = null;
    protected $count_po         = null;
    protected $count_po_av      = null;
    protected $count_po_v       = null; 
    protected $count_orders     = null;
    protected $ca               = null;
    protected $ca_v             = null;
    protected $total_qty_items  = null;
    protected $total_value_po_v = null;
    protected $total_value_po_av= null;
    protected $count_new_vendor = null;
    protected $count_panier_abdn= null;
    protected $total_comm       = null;
    protected $payline_encours  = null;
    
    public function __construct($begin, $end){
        
        self::$db = new BusinessSoukeoModel();
        
        $this->month_begin = $begin;
        $this->month_end = $end;
        $this->sql_month =  " BETWEEN '".$begin."' AND '".$end."' ";

    }
	
    public function getCount_po(){
        
        if($this->count_po !== null){
            return $this->count_po;
        }else{
            return $this->count_po = self::$db->getTotalPo($this->sql_month);
        }
    }
    
    public function getCount_po_av()
    {
        if($this->count_po_av !== null){
            return $this->count_po_av;
        }else{
            $this->count_po_av = self::$db->getTotalPoAvahis($this->sql_month);
            return $this->count_po_av;
        }        
    }
    
    public function getCount_po_v()
    {
        if($this->count_po_v !== null){
            return $this->count_po_v;
        }else{
            return $this->count_po_v = self::$db->getTotalPoVendeur($this->sql_month);
        }        
    }
    
    public function getCount_orders()
    {
        if($this->count_orders !== null){
            return $this->count_orders;
        }else{
            return $this->count_orders = self::$db->getCountOrdersMonth($this->sql_month);
        }        
    }  
    
    public function getCa()
    {
        if($this->ca !== null){
            return $this->ca;
        }else{
            return $this->ca = self::$db->getCaMonth($this->sql_month);
        }         
    }      

    public function getCa_v()
    {
        if($this->ca_v !== null){
            return $this->ca_v;
        }else{
            return $this->ca_v = self::$db->getCaVendeurMonth($this->sql_month);
        }         
    } 
    
    public function getTotal_qty_items()
    {
        if($this->total_qty_items !== null){
            return $this->total_qty_items;
        }else{
            return $this->total_qty_items = self::$db->getTotalPanierMonth($this->sql_month);
        }          
    }
    
    public function getTotal_value_po_v()
    {
        if($this->total_value_po_v !== null){
            return $this->total_value_po_v;
        }else{
            return $this->total_value_po_v = self::$db->getTotalPanierVendeurMonth($this->sql_month);
        }          
    }    
    	    
    public function getTotal_value_po_av()
    {
        if($this->total_value_po_av !== null){
            return $this->total_value_po_av;
        }else{
            return $this->total_value_po_av = self::$db->getTotalPanierAvahisMonth($this->sql_month);
        }          
    }

    public function getCount_new_vendor()
    {
        if($this->count_new_vendor !== null){
            return $this->count_new_vendor;
        }else{
            return $this->count_new_vendor = self::$db->getNbNewVendeur($this->sql_month);
        }          
    }
    
    public function getCount_panier_abdn()
    {
        if($this->count_panier_abdn !== null){
            return $this->count_panier_abdn;
        }else{
            return $this->count_panier_abdn = self::$db->getPanierAbandonneMonth($this->sql_month);
        }          
    }   
    
    public function getTotal_comm()
    {
        if($this->total_comm !== null){
            return $this->total_comm;
        }else{
            return $this->total_comm = self::$db->getTotalCommMonth($this->sql_month);
        }
    }
    
    public function getPayline_encours($payline_encours)
    {
        if($this->payline_encours !== null){
            return $this->payline_encours;
        }else{
            return $this->payline_encours = self::$db->getTotalPaylineNXEncours($this->sql_month);
        }
    }
}


