<?php 

/**
 * Script pour insérer les produits d'un e-commerçant Prestashop
 * vers la base de donnée intermédiaire Soukflux. Il y a tout d'abord
 * un parse du CSV obtenu depuis l'URL de l'ecommercant, permettant d'obtenir
 * les produits et leurs attributs sous forme de tableaux, puis il y a toutes
 * les insertions de ces produits, voir aussi la classe
 * BusinessSoukePrestashopModel
 * 
 * @version 0.1.0
 * @author 	Philippe_LA
 * 
 */
class BusinessImportPrestashop extends BusinessImportPlugin {
	
	protected static $nbRun 		= 0; 		//Compte le nombre de fois que ce batch a été executé
	
	private static $db;
	private static $pictureManager ;

	
	/**
	 * Fonction qui parse le CSV ,produit des tableaux associatifs 
	 * et appelle le fonctions d'insertion en BDD
	 */
	public function run()
	{
		//On parse le CSV de l'e-commercant dispo à l'URL fourni
		if(!$this->parseCSV($this->url_to_parse, 30, "|")){
		    
		    return false;
		}
        else{
    		self::$db = new BusinessSoukeoPrestashopModel ();
    		self::$pictureManager = new BusinessPictureManager ($this->vendorID);
    		
    		//Préparation des requetes
    		self::$db -> prepareStmtProdEcommercant($this->vendorID);
    		self::$db -> prepareInsertSfkProdPS($this->vendorID);
    		self::$db -> prepareInsertSfkProdAttrPS();
    		
    		foreach ($this->productListAttribute as $product) {
                
                if( $this -> vendorID == 46){
                    $product = $this -> correctPitcures($product, $this -> vendorID);    
                }
    
                if( $this -> vendorID == 46){
                    $product = $this -> correctPoids($product);    
                }
                
                if( $this -> vendorID == 46){
                    $product = $this -> correctReferenceProduct($product);    
                }
                  if( $this -> vendorID == 117){
                    $product = $this -> correctReferenceProduct($product);    
                }
                            
    			//Insertion produit
    			$state = self::$db->addRowSfkProdPS($product, $this->vendorID) ;
    			
    			if( $state == 1){ //Si la requete et bien passée
    				$this->nbInsert++;
    			}elseif($state == 2 ){
    				$this->nbUpdate++;
    			}elseif($state == 0){
    				$this->nbErrors++;
    			}
    			
    			
    			foreach ($this->attributeList as $attribute) {
    				
    				//Produit possède cet attribut?
    				if ($product[$attribute] != "") {
    
    					//On vérifie si l'attribut existe déja 
    					$id_attribute = self::$db->checkIfAttributeExistsPS($attribute);
    					//Relation produit_Attribut
    					if(self::$db->addRowSfkProdAttrPS($product['ID_PRODUCT'], $this->vendorID, $id_attribute, $product[$attribute]) == true) {
    						$this->nbInsertAttr++;
    					}	
    				}
    			}
    			
    			// si associé mise a jour de la table produit stocke prix et Images
    			if ($prodAv = self::$db->getProductAvAssoc($this->vendorID,$product['ID_PRODUCT'])){// a rajouté aprés pour verif la date &&self::$db->verifDate_upBddLocalImport($product)){
    				    
    				if($prodAv["dropship_vendor"] != 24 && $prodAv["dropship_vendor"] != 57 && !$prodAv['local_image']){
    				    self::$pictureManager -> savePictures($product, $prodAv);    
    				}	
    				
    		        $test = self::$db->addRowSfkProdEcomSP($product, $this->vendorID); 	
    			}
    		}
    
    		self::$nbRun++;		
    		return true;
		}
	}

	
	public function correctPitcures($prod, $vendor_id)
	{
        if($vendor_id == 29){
            
            $newUrl = "http://www.kdopays.re/img/p/".$prod['ID_PRODUCT'];
            
            $img_1 = $prod['IMAGE_PRODUCT'];
            $img_1 = explode('_', $img_1);
            $img_1 = explode('/', $img_1[0]);
            $img_1 = end($img_1);
            
            $img_2 = $prod['IMAGE_PRODUCT_2'];
            $img_2 = explode('_', $img_2);
            $img_2 = explode('/', $img_2[0]);
            $img_2 = end($img_2);
            
            $img_3 = $prod['IMAGE_PRODUCT_3'];
            $img_3 = explode('_', $img_3);
            $img_3 = explode('/', $img_3[0]);
            $img_3 = end($img_3);
            
            $prod['IMAGE_PRODUCT'] = $newUrl . "-" . $img_1 . ".jpg";
            $prod['IMAGE_PRODUCT_2'] = $newUrl . "-" . $img_2 . ".jpg";
            $prod['IMAGE_PRODUCT_3'] = $newUrl . "-" . $img_3 . ".jpg";
            
            return $prod;
        }
        elseif($vendor_id == 46){
            
            $newUrl = "http://ckomca.com/img/p/".$prod['ID_PRODUCT'];
            
            $img_1 = $prod['IMAGE_PRODUCT'];
            $img_1 = explode('_', $img_1);
            $img_1 = explode('/', $img_1[0]);
            $img_1 = end($img_1);
            $img_1 = explode('-', $img_1);
            $img_1 = $img_1[0];
            
            $img_2 = $prod['IMAGE_PRODUCT_2'];
            $img_2 = explode('_', $img_2);
            $img_2 = explode('/', $img_2[0]);
            $img_2 = end($img_2);
            $img_2 = explode('-', $img_2);
            $img_2 = $img_2[0];
            
            $img_3 = $prod['IMAGE_PRODUCT_3'];
            $img_3 = explode('_', $img_3);
            $img_3 = explode('/', $img_3[0]);
            $img_3 = end($img_3);
            $img_3 = explode('-', $img_3);
            $img_3 = $img_3[0];
            
            $prod['IMAGE_PRODUCT'] = $newUrl . "-" . $img_1 . ".jpg";
            $prod['IMAGE_PRODUCT_2'] = $newUrl . "-" . $img_2 . ".jpg";
            $prod['IMAGE_PRODUCT_3'] = $newUrl . "-" . $img_3 . ".jpg";
            
            return $prod;
        }
	}

    public function correctPoids($product)
    {
        if( $this -> vendorID == 46){
            $product['WEIGHT'] = ( (float)$product['WEIGHT'] ) / 1000 ;
        }
        
        return $product;	
    }
	
    public function correctReferenceProduct($product)
    {
        if( $this -> vendorID == 46){
            $product['REFERENCE_PRODUCT'] = $product['REFERENCE_PRODUCT']."-".$product['ID_PRODUCT'] ;
        }
        if( $this -> vendorID == 117){
            $product['REFERENCE_PRODUCT'] = "e117-".$product['ID_PRODUCT'] ;
        }
        return $product;    
    }	
	
	/**
	 * @return nbRun le nombre de fois que ce Script a été effectué
	 */
	public static function getNbRun()
	{
		return self::$nbRun;
	}
}
?>