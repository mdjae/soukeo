<?php 
/**
 * Affichage des boites
 *
 */
class UIBox
{
	private $_box_id = "";
	private $_i_boxtitle = "";
	private $_i_picto = "";
	private $_i_iconclass = "";
	private $_height = "";
	private $_overflow = "";
	private $_width = "";

	/*
	* constructeur
	*
	* @param string $box_id ID javascript de la boite
	* @param array $headers Tableau contenant la description des entetes
	* @param array $content Contenu du tableau
	*/
	public function __construct($box_id, $i_boxtitle = "Box title",$i_picto="window",$i_iconclass = "")
	{
		$this->_box_id = $box_id;
		$this->_i_boxtitle = $i_boxtitle;
		$this->_i_picto = $i_picto;
		$this->_i_iconclass = $i_iconclass;
		$this->_height = "";
	}

	/**
	 * Param�trage de la hauteur de la boite
	 * exemple : "300px"
	 */
	function setHeight($height)
	{
		//echo "<h1>toto</h1>";
		$this->_height = $height;
		//$this->_height = "300px";
	}

	function setWidth($width)
	{
	    $this->_width = $width;
		//$this->_width = "300px";
	}

	/**
	 * Param�trage de la m�thode de gestion du contenu de la boite
	 * "auto" : mise en place de scrollbars en cas de d�bordement
	 * "hidden" : le contenu qui d�borde est cach�
	 */
	function setOverflow($overflow)
	{
		$this->_overflow = $overflow;
	}


	public function drawBoxHeader()
	{
		global $root_path;

		$out = '
		<div class="related" width="'.$this->_width.'" >
		<div class="t"><div class="tl"><div class="tr">

  		<table cellpadding="0" cellspacing="0" border="0" width="97%" id="related_title">
  		<tr><td align="left" valign="top">

    		<img class="icon'.$this->_i_iconclass.'" src="/skins/common/images/picto/'.$this->_i_picto.'.gif" align="absmiddle" />
    		&nbsp;'.$this->_i_boxtitle.'

  		</td><td align="right" valign="top">

  		<a href="#" id="reload_'.$this->_box_id.'"><img src="/skins/common/images/picto/reload.gif" align="absmiddle" border="0"/></a>
  		<a href="#" ><img
  				id="arr_'.$this->_box_id.'"
  				src="/skins/common/images/picto/arrow3_n.gif"
  				align="absmiddle" border="0"
  			</a>
  		<a href="#"><img src="/skins/common/images/picto/close.gif" align="absmiddle" border="0"/></a>

  		</td></tr></table>

    <div class="related_in">

		<div class="content" id="con_'.$this->_box_id .'"
			style="';
			if ($this->_overflow != "") $out .="overflow: " . $this->_overflow . ";"; 
			if ($this->_height != "") $out .="height: " . $this->_height; 
		$out.=	'">';
return $out;	
}

	public function drawBoxHeaderStatic()
	{
		global $root_path;

$out ='
<div class="related" style="width:'.$this->_width.'">
<div class="t"><div class="tl"><div class="tr" =>

<div class="top-bar"><h3><img class="icon'.$this->_i_iconclass.'" src="/skins/common/images/picto/'.$this->_i_picto.'.gif" align="absmiddle" />
&nbsp;'.$this->_i_boxtitle.'</h3> </div>


<div class="related_in well no-padding"">

<div  id="con_'.$this->_box_id.'">';

return $out;
}

function drawBoxFooter()
{
global $root_path;
$out .='
</div></div></div><br clear="both"/>
</div></div></div>
';
return $out;
/*
<script>
	var myRequest = new Request({
method: 'get',
url: '<?php  echo $root_path ?>
	/common/box / box.service.php',
	onSuccess: function(txt){
	//alert(txt);
	},
	onFailure: function(txt){
	//alert(txt);
	}
	});

	var myFx_
<?php  echo $this->_box_id ?>
= new Fx.Slide('con_<?php  echo $this->_box_id ?>
	', {
	duration: 1000,
	transition: Fx.Transitions.Pow.easeOut,
	onComplete: function(){
	if (!myFx_
<?php  echo $this->_box_id ?>.
open)
{
$('arr_<?php  echo $this->_box_id ?>').src = '<?php  echo $root_path ?>
	skins / common / images / picto / arrow3_s.gif';
	myRequest.send('function=saveStatus&box_id=
<?php  echo $this->_box_id ?>
	&
	status = closed');
	//alert('ouvrir');
	}
	else
	{
	$('arr_
<?php  echo $this->_box_id ?>').src = '<?php  echo $root_path ?>
	skins / common / images / picto / arrow3_n.gif';
	myRequest.send('function=saveStatus&box_id=
<?php  echo $this->_box_id ?>
	&
	status = open');
	//alert('fermer');
	}
	}
	});

	//
<?php
// Si la boite est ferm�e dans la config en base, on la ferme
if ($this->getStatus() == "closed")
{
?>
$('arr_<?php  echo $this->_box_id ?>').src = '<?php  echo $root_path ?>
	skins / common / images / picto / arrow3_s.gif';
	myFx_
<?php  echo $this->_box_id ?>
	.
	hide();

<?php
}
?></script>
<?php*/
}

/**
* Sauvegarde en base le status d'une boite pour un utilisateur
*
*/
function saveStatus($status)
{
$query = "SELECT count(1) as nb\n
FROM c_box\n
WHERE box_id = '" . $this->_box_id . "'\n
AND usercode = '" . $_SESSION['smarttri'] . "'";
$res = dbQueryOne($query);
if ($res['nb'] > 0)
{
// Mise � jour
$query = "UPDATE c_box\n
SET status = '" . $status . "'\n
WHERE box_id = '" . $this->_box_id . "'";
echo $query;
}
else
{
// Ajout
$query = "INSERT INTO c_box\n
(
box_id,
usercode,
status
)
VALUES
(
'" . $this->_box_id . "',
'" . $_SESSION['smarttri'] . "',
'" . $status . "'
)";
}
return dbQuery($query);
}

/**
* R�cup�re le statut de la boite : open ou closed
*/
function getStatus()
{
// Est-ce qu'un session est ouverte ?
if (isset($_SESSION))
{
$query = "SELECT status
FROM c_box
WHERE box_id = '" . $this->_box_id . "'
AND usercode = '" . (isset($_SESSION['smarttri'])?$_SESSION['smarttri']:'') . "'";
//echo "toto " . $query;
$res = dbQueryAll($query);
if (count($res) == 0 || $res[0]['status'] == 'open')
{
$status = "open";
}
else
{
$status = "closed";
}
}
else $status = "open";

return $status;
}

}
?>