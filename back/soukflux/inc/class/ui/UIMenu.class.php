<?php
class UIMenu {
	public function __construct() {
		$this -> menu_common = ("Accueil");
		$this -> link_home = ("Tableau de bord");
		$this -> link_pwd = ("Mot de passe");

		//$this -> menu_gest = ("Gest");
		//$this -> link_listsite = ("Liste des terrains");
		//$this -> link_logxml = ("Log XML");
		$this -> link_acceuil = ("acceuil");
		//$this -> link_maj = ("Mise à jours Information");
		//$this -> link_amaj = ("Mise à jours terrain");

		//	$this->link_download					= ("Téléchargements des fichiers");
		//	$this->link_suivifact					= ("Suivi de la facturation");

		$this -> menu_admin = ("Administration");

		$this -> link_users = ("Utilisateurs");

		$this -> link_groups = ("Groupes de sécurité");
		$this -> link_system = ("Paramètres système");
		$this -> link_config = ("Paramètres de configuration");

		$this -> link_connect = ("Suivi des connexions");
		$this -> link_batch = ("Tâches périodiques");
		$this -> link_filesexport = ("Téléchargement de fichiers");
        $this -> link_gestionreferentiels = ("Gestion des réferentiels");
		$this -> link_dbadmin = ("Maintenance de la base de données");
        $this -> link_statistiques = ("Statistiques");
		$this -> link_showeditmenu = ("Edition du menu Avahis");
        
		$this -> menu_catalogue = ("Catalogue");
		$this -> link_categorie = ("Management Catégorie");
		$this -> link_produit = ("Management Produit");
        $this -> link_attributs = ("Management Attributs");
		$this -> link_rechercheproduitgrossiste = ("Recherche Produit grossiste");
		$this -> link_ficheproduit = ("Voir une fiche produit");
		$this -> link_createproduit = ("Créer une fiche produit");
		
        $this -> menu_grossistes = ("Grossistes");
        $this -> link_grossiste = ("Qualification Grossistes");
        $this -> link_commande_grossiste = ("Commandes Grossiste");
        $this -> link_annonce_reception = ("Annonces Réception");
        
		//$this -> menu_qualification = ("Qualification");
		//$this -> link_ecommercant = ("Qualification e-commerçants");
		//$this -> link_grossiste = ("Qualification Grossistes");
		//$this -> link_tracking = ("Qualification Tracking");
		//$this -> link_venteavahis = ("Ventes Avahis");
		
		$this -> menu_clients = ("Clients");
		$this -> link_listing_acheteurs = ("Listing Acheteurs");
        $this -> link_listing_vendeurs = ("Listing Vendeurs");
        $this -> link_listing_tickets = ("Listing Tickets");
        $this -> link_livraisons = ("Modes de livraison");
		
        $this -> menu_avahis = ("Avahis");
        $this -> link_commande_avahis = ("Commandes Avahis");
        $this -> link_item_grossiste_to_order = ("A commander");
        
		//$this -> menu_facturation = ("Facturation");
		//$this -> link_annonce_reception = ("Annonces Réception");
		//$this -> link_commande = ("Commandes");
		//$this -> link_commande_avahis = ("Commandes Avahis");
		//
		//$this -> link_commande_commercant = ("Commandes Commercants");
		//$this -> link_item_grossiste_to_order = ("A commander");
		
		//$this -> link_commercants = ("Commercants");
		//$this -> link_historique_factures_com = ("Historique factures commerçants");
		
		$this -> menu_commercants = ("Commerçants");
		$this -> link_ecommercant = ("Qualification e-commerçants");
		$this -> link_tracking = ("Tracking Plugin");
        $this -> link_commande_commercant = ("Commandes Commercants");
        $this -> link_facturation = ("Facturation Commercants");
		$this -> link_abonnements = ("Etat des abonnements");
		$this -> link_historique_factures_com = ("Historique factures commerçants");
		
		
		
	}

	public function displayMenu() {
		global $root_path;

		// Menu
		$MENU_ITEMS_str = "<ul id=\"jsddm-no\" class=\"mainmenu nav pull-right\"> \n";

		// Liste des domaines et catégories accessibles pour l'utilisateur
		$query = "SELECT distinct s.seq, s.domain, s.catagory, s.page_path
					FROM c_group_security s
					WHERE s.catagory NOT IN ('login','logout')
					
					AND s.function = '' AND s.special = '' AND s.domain <> 'services'
					AND s.group_id IN " . SystemAccess::userGroupToSql() . "
				ORDER BY s.seq";

		/* Domains order :
		 - common
		 - services
		 - admin
		 */
		//var_dump( $query);
		// Boucle sur les domaines
		$res_domain = dbQueryAll($query);
		$prev_domain = "";
		for ($i = 0; $i < count($res_domain); $i++) {
			// Texte
			$text = "menu_" . $res_domain[$i]['domain'];
			$text2 = "link_" . $res_domain[$i]['catagory'];

			if ($res_domain[$i]['domain'] != $prev_domain  && $res_domain[$i]['domain'] != 'common' ) {
				if ($prev_domain != "") {
					$MENU_ITEMS_str .= "</ul></li> \n";
				}

				eval("\$t = \$this->$text;");
				$MENU_ITEMS_str .= "<li class=\"dropdown\">
										<a href=\"/app.php/" . SystemAccess::getPagePathDomCat($res_domain[$i]['domain'], $res_domain[$i]['catagory']) . "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"  ><i class=\"fa fa-align-justify fa-white\"></i> " . $t . "</a> \n
		                         		<ul class=\"dropdown-menu\"> \n";

				$prev_domain = $res_domain[$i]['domain'];
			} else {
				if ($prev_domain != "") {
					$MENU_ITEMS_str .= " ";
				}
			}
			
			if( $res_domain[$i]['domain'] == 'common' ){
				eval("\$t = \$this->$text2;");
				$MENU_ITEMS_users .= "<li><a href=\"/app.php/" . SystemAccess::getPagePathDomCat($res_domain[$i]['domain'], $res_domain[$i]['catagory']) . "\">  <i class='fa fa-cog'></i>   " . $t . "</a></li> \n";
			}else{
				eval("\$t = \$this->$text2;");
				$MENU_ITEMS_str .= "<li><a href=\"/app.php/" . SystemAccess::getPagePathDomCat($res_domain[$i]['domain'], $res_domain[$i]['catagory']) . "\"> " . $t . "</a></li> \n";
			}
		}
        
        $user = BusinessUser::getClassUser($_SESSION['smartlogin']);
        
        $unseenTickets = $user -> checkUnseenTickets();
        
        if($unseenTickets > 0) {
            $labelHtml = "<span class='badge badge-important'>$unseenTickets</span>";
            $countTickets = "($unseenTickets)";
        }
        
		// fin de la boucle, on terme la cha�ne
		$MENU_ITEMS_str .= "</ul></li>\n
		                            </ul> \n";
		$addjs = "<script>
				
				</script>";
				
				
				 $addmenu .= '  <ul class="nav pull-right">
          	
          	       	
          	<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-user icon-white"></i> 
					<span class="hidden-phone">'.$_SESSION['smartfirst']." ".$_SESSION['smartlast'] .' '.$labelHtml.'</span>
				</a>
				
				
			<ul class="dropdown-menu">
					<li><a href="/app.php/dashboard"><i class="fa fa-cog"></i>'._("Tableau de bord")." ".$countTickets.'</a></li>
					'.$MENU_ITEMS_users.'
					<li class="divider"></li>
					<li><a href="/common/logout/"><i class="fa fa-power-off"></i>'._("Se déconnecter").'</a></li>
				</ul>
				
				
			</li>
          </ul> '; 
				
				
		return $addmenu.$MENU_ITEMS_str  . $addjs;
	}

	public function getPageTitle($security_domain, $domain_categ) {
		eval("\$text = \$this->menu_$security_domain;");
		eval("\$text2 = \$this->link_$domain_categ;");
		return $text . " > <b>" . $text2 . "</b>";
	}

}
?>