﻿<?php 
class UIWindow
{
	private $_width;
	private $_height;
	private $_url;
	private $_title;
	private $_id;
	private $_load_method;
	private $picto;
	
	
	/**
	 * Type de la fenetre : window, modal, modal2, notification 
	 * @var string
	 */
	private $_type;
	
	public function __construct($height, $width, $url)
	{
		$this->_height = $height;
		$this->_width = $width;
		$this->_url = $url;
		
		$this->_load_method = "iframe";
	}
	
	public function setLoadMethod($method)
	{
		$this->_load_method = $method;
	}
	
	public function setPicto($picto)
	{
		$this->_picto = $picto;
	}
	
	public function display($title, $content, $get = null)
	{
		$url = $this->_url . $get;
		$output = '<a href="#" onClick="openMocha("' . $title . '", ' . $this->_height . ', ' . $this->_width . ', "' . $url . '", "' . $this->_type . '", "' . $this->_load_method . '"); ">' . $content . '</a>';
		return $output;
	}
}
?>