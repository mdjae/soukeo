<?php 
/**
 * offipsation de l'objet pChart
 * @author nsa
 *
 */
class Chart extends pChart
{
	/**
	 * Nom du graph
	 * @var unknown_type
	 */
	protected $name = "";
	
	/**
	 * Variables pour les classes qui �tendent la grid
	 * @var unknown_type
	 */
	private $_sql_query = "";
	
	/**
	 * R�sultat � afficher
	 * @var unknown_type
	 */
	protected $_result = array();
	
	protected $_hash = "";
	
	/**
	 * Contructeur
	 * @param $name
	 * @return unknown_type
	 */
	public function __construct($name)
	{
		$this->_name = $name;
	}
	
	/**
	 * Sauvegarde le rapport en base
	 * @return unknown_type
	 */
	private function _save()
	{
		$query = "SELECT count(1) as nb
					FROM c_obj_persistence
					WHERE session = '" . session_id() . "'
							AND name = '" . $this->_name . "'";
		$rs = dbQueryOne($query);
		if ($rs['nb'] > 0)
		{
			$query = "UPDATE c_obj_persistence
						SET obj = '" . addslashes(base64_encode(gzcompress(serialize($this)))) . "',
							expire = " . time() . "
						WHERE session = '" . session_id() . "'
							AND name = '" . $this->_name . "'";
		}
		else
		{
			$query = "INSERT INTO c_obj_persistence
						(
							session,
							name,
							expire,
							obj
						)
						VALUES
						(
							'" . session_id() . "',
							'" . dbEscape($this->_name) . "',
							" . time() . ",
							'" . addslashes(base64_encode(gzcompress(serialize($this)))) . "'
						)";
		}
		dbQuery($query);
	}
	
	/**
	 * Retourne une string contenant de quoi afficher le graph
	 * @return unknown_type
	 */
	public function display()
	{
		global $root_path;
		$pChart_path = $root_path . "/inc/libs/pChart/";
		
		// Sauvegarde avant affichage en cas de clique par l'utilisateur
		$this->_save();
		
		//echo "toto";
		$output .= '<img src="' . $root_path.'/tmp/' . $this->_hash . '.png?id=' . time() . '" border="0"/>' . "\n";
		return $output;
	}
	
	public function update($post)
	{
		
	}
	
	public static function getGraph($name)
	{
		$query = "SELECT obj
					FROM c_obj_persistence
					WHERE session = '" . session_id() . "'
					AND name = '" . $name . "'";
		$rs = dbQuery($query);

		$obj = dbQueryNext($rs);

		//echo gzuncompress(base64_decode(stripslashes($obj['obj'])));
		return unserialize(gzuncompress(base64_decode(stripslashes($obj['obj']))));
		//return unserialize(gzuncompress(stripslashes($obj['obj'])));
	}
	
	/**
	 * Retourne true si l'image est d�j� g�n�r�e pour le m�me graph
	 * @return boolean
	 */
	public function isInCache()
	{
		global $root_path;
		if (file_exists($this->getChartPath())) return true;
		else return false;
	}
	
	/**
	 * Retourne le chemin du graph
	 * @return string
	 */
	public function getChartPath()
	{
		global $root_path;
		return $root_path.'/tmp/' . $this->_hash . '.png';
	}
}
?>