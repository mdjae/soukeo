<?php 
/**
 * Objet g�n�rique de pr�sentation 
 *
 */
class FormInputComment extends FormInputText
{
	/**
	 * @param $text Texte � afficher
	 */
	public function __construct($text)
	{
		$this->_required = false;
		$this->setText($text);
		
		// N'est pas une donn�es du formulaire, juste pr�sentation
		$this->_input = false;
	}
	
	public function checkValue()
	{
		return true;
	}
	
	public function display()
	{
		
		$output = "<tr id=\"tr_" . $this->_name . "\">
						<td colspan=\"3\">
							<h4>" . $this->_text . "</h4>
						</td>
					</tr>";
		
		return $output;
	}
}
?>