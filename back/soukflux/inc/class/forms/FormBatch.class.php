<?php 
/**
 * Formulaire des programmations
 */
class FormBatch extends Form
{
	private $_prog_id = 0;
	
	public function __construct($prog_id)
	{
		global $root_path;
		
		$this->_prog_id = $prog_id;
		$this->_name = "progform";
		
		// Phrases d'entete
		$this->_txt_CreateInit = ("Création d'une nouvelle programmation");
		$this->_txt_CreateOk = ("La programmation a été créée");
		$this->_txt_UpdateInit = ("Modification d'une programmation");
		$this->_txt_UpdateOk = ("Mise à jour de la programmation enregistrée");
		
		// Est-ce une creation ou une mise � jour ?
		if ($prog_id == 0)
		{
			$this->_initCreate();
		}
		else
		{
			$this->_initUpdate();
		}
		
		//
		$con1 = new FormContainer("main", false	);
		
		$con1->addItem(new FormInputSubmitButton());
		$con1->addItem(new FormInputSeparator(''));
		$con1->addItem(new FormInputHidden("prog_id", $this->_prog_id));
		
		// Liste des classes disponibles
		$class = new FormInputList("class",("Programme"), $this->_class,false);
		// Boucle sur les fichiers du r�pertoires des batchs
		if ($handle = opendir($root_path.'/inc/class/batchs'))
		{	
    		while (false !== ($file = readdir($handle)))
    		{
    			if ($file != "." && $file != ".." && $file != ".svn" && $file != "archive")
    			{
    				$batch = substr($file,0,strpos($file,"."));
    				
    				//echo '$desc = ' . $batch . '::getDescription();';
    				eval('$desc = ' . $batch . '::getDescription();');
        			$class->addElement($batch,$desc);
    			}
    		}
		}
		$con1->addItem($class);
		
		$con1->addItem(new FormInputText("comment", ("Commentaire"), $this->_comment, true, 40));
		$con1->addItem(new FormInputText("email", ("Email"), $this->_email, true, 40));
		
		$con1->addItem(new FormInputSeparator(("Programmation")));
		$con1->addItem(new FormInputText("hour", ("Heure"), $this->_hour, true, 40));
		$con1->addItem(new FormInputText("day", ("Jour"), $this->_day, true, 40));
		$con1->addItem(new FormInputText("month", ("Mois"), $this->_month, true, 40));
		$con1->addItem(new FormInputText("year", ("Année"), $this->_year, true, 40));
		
		$con1->addItem(new FormInputSeparator(("Configuration")));
		$con1->addItem(new FormInputText("params", ("Paramètre"), $this->_hour, true, 40));
		
		$con1->addItem(new FormInputSeparator(""));
		$con1->addItem(new FormInputCheckbox("active",("Actif"),$this->_active));
		
		$con1->addItem(new FormInputSeparator(''));
		$con1->addItem(new FormInputSubmitButton());
		
		$this->addContainer($con1);
		
		// On indique la grid � reloader
		$this->setGridParams('batch','../../');
	}
	
	/**
	 * Initialise les propri�t�s du formulaire pour une cr�ation
	 */
	private function _initCreate()
	{
		$this->_status = "insert";
		$this->_class =				"";	
		$this->_comment =			"";	
		$this->_hour =				"";	
		$this->_day =				"";	
		$this->_month =				"";	
		$this->_year =				"";	
		$this->_email =				"";	
		$this->_params =			"";	
		$this->_company =			"";	
		$this->_active =			"checked";	

		return true;
	}
	
	/**
	 * Initialise les propri�t�s du formulaire pour une mise � jour
	 */
	private function _initUpdate()
	{
		// Mise � jour
		$this->_status = "update";
			
		$res2edit = dbQueryOne("SELECT class,
							comment,
							hour,
							day,
							month,
							year,
							email,
							params,
							active
						FROM c_batch 
						WHERE prog_id = '". $_GET['prog_id'] . "'");

		$this->_class 			=			stripslashes($res2edit['class']);
		$this->_comment 		=			stripslashes($res2edit['comment']);
		$this->_hour 			=			stripslashes($res2edit['hour']);
		$this->_day 			=			stripslashes($res2edit['day']);
		$this->_month 			=			stripslashes($res2edit['month']);
		$this->_year 			=			stripslashes($res2edit['year']);
		$this->_email 			=			stripslashes($res2edit['email']);
		$this->_params 			=			stripslashes($res2edit['params']);
		$this->_active 			=			stripslashes($res2edit['active']);
		
		if ($res2edit['active'] == "TRUE") $this->_active = "checked";
		else $this->_active = "";
		
		return true;
	}
	
	/**
	 * Insert en base en cas de cr�ation
	 */
	public function _create()
	{
		global $inc_path;
		
		// Champs sp�ciaux
		if (array_key_exists('active',$_POST) && $_POST['active'] == "checked") $active = "TRUE";
		else $active = "FALSE";
			
		// insert
		$query = "INSERT INTO c_batch
						(
							class,
							comment,
							hour,
							day,
							month,
							year,
							email,
							params,
							active
						)
					VALUES
						(
							'" . addslashes($_POST['class']) ."',
							'" . addslashes($_POST['comment']) . "',
							'" . addslashes($_POST['hour']) . "',
							'" . addslashes($_POST['day']) ."',
							'" . addslashes($_POST['month']) ."',
							'" . addslashes($_POST['year']) ."',
							'" . addslashes($_POST['email']) ."',
							'" . addslashes($_POST['params']) ."',
							'" . $active ."'
						)";
		$db = new sdb();
        $rs = $db->exec($query);
		//dbQuery($query);
			
		// get id of the new user
		$this->_prog_id = $db->lastInsertId($rs);
		
		return true;
	}
	
	/**
	 * Update en base en cas de mise � jour
	 */
	public function _update()
	{
		// Champs sp�ciaux
		if (array_key_exists('active',$_POST) && $_POST['active'] == "checked") $active = "TRUE";
		else $active = "FALSE";

		// update
		$query = "UPDATE c_batch set
				class =			'" . addslashes($_POST['class']) . "',
				comment =		'" . addslashes($_POST['comment']) . "',
				hour =			'" . addslashes($_POST['hour']) . "',
				day =			'" . addslashes($_POST['day']) . "',
				month =			'" . addslashes($_POST['month']) . "',
				year =			'" . addslashes($_POST['year']) . "',
				params =		'" . addslashes($_POST['params']) . "',
				email =			'" . addslashes($_POST['email']) . "',
				active =		'" . $active . "'
				WHERE prog_id = '" . $this->_prog_id . "'";
		dbQuery($query);
			
		return true;
	}

	
	/*
	 * Controle si l'on peut ajouter
	 */
	protected function _canInsert()
	{		
		return true;
	}
	
	/*
	 * Controle si l'on peut mettre � jour
	 */
	protected function _canUpdate()
	{
		return true;
	}
}
?>