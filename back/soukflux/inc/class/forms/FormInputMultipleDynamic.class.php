<?php 
/**
 * Objet g�n�rique de liste double pour formulaire
 *
 */
class FormInputMultipleDynamic extends FormInputText
{
	// Contenu
	private $_lines = array();
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $maxlines nb de lignes maximum (0 pas de limite)
	 */
	public function __construct($name, $text, $required = false, $maxlines = 0)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_maxlines = $maxlines;
	}
	
	public function addLine($input)
	{
		$this->_lines[] = $input;
	}
	
	public function checkValue()
	{
		$back = true;
		// TODO
		return $back;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<tr id=\"tr_" . $this->_name . "\" ";
		if ($this->_hidden) $output .= " style=\"display: none;\" ";
		$output .= ">";
		
		$output .= "
        <td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>";
		
     $output .= "<td>
     <p>
        <table border='0' class=\"contentformmulti\"><tr><td valign=\"bottom\" bgcolor=\"#EEEEEE\">
					<script>
					  var lcount_".$this->_name." = 1;
					  
						function addRow_" . $this->_name . "()
						{
						
              				var root = $('dyn_" . $this->_name . "').parentNode; //tbody
							var clone = $('dyn_" . $this->_name . "').cloneNode(true); //the clone of the first row
							clone.setAttribute('id','no_id');
							root.appendChild(clone); //appends the clone
						  
							lcount_".$this->_name."++;
            
             				if (". $this->_maxlines ." > 0)
             				{
              					if (lcount_".$this->_name." >= ". $this->_maxlines .")
              					{
                					tag_addrow_" . $this->_name . ".style.display = 'none';
              					}
              				}
						}
						
						function deleteRow_" . $this->_name . "(elm)
						{
							//alert('id ' + elm.parentNode.id);
							//if (elm.parentNode.id != 'suppr_" . $this->_name . "')
							//{
								var root = $('dyn_" . $this->_name . "').parentNode; //tbody
								root.removeChild(elm.parentNode.parentNode);
								
								lcount_".$this->_name."--;
            
                				if (". $this->_maxlines ." > 0)
                				{
                					if (lcount_".$this->_name." < ". $this->_maxlines .")
                					{
                  						tag_addrow_" . $this->_name . ".style.display = '';
               	 					}
                				}
							//}
						}
					</script>
					<div id='tag_addrow_" . $this->_name . "'><a href='#' onClick='javascript: addRow_" . $this->_name . "(); return false;'><img src='" . $skin_path . "images/picto/add.gif' border='0' class='iconAction'/></a></div>
					</td><td><table border='0'><tbody>";
		//foreach($this->_lines as $l)
		//echo "<pre>"; print_r($this->_lines); echo "</pre>";
		for($k = 0; $k < count($this->_lines); $k++)
		{
			$l = $this->_lines[$k];
			if ($k == 0) $output .= "<tr id='dyn_" . $this->_name . "'>";
			else $output .= "<tr>";
			foreach($l as $i)
			{
				$output .= $i->display();
			}
			if ($k == 0) $output .= "<td id='suppr_" . $this->_name . "'>";
			else $output .= "<td>";
			$output .= "<a href='#' onClick='javascript: deleteRow_" . $this->_name . "(this); return false;'><img src='" . $skin_path . "images/picto/trash.gif' border='0' class='iconAction'/></a>";
			$output .= "</td></tr>";
		}
		$output .= "
					</tbody></table></td>
					</tr></table>
				</p></td>
				<td></td>";
		
		$output .= "</tr>";
		
		return $output;
	}
}
?>