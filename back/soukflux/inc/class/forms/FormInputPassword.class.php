<?php 
/**
 * Objet g�n�rique d'input d'un mot de passe d'un formulaire
 *
 */
class FormInputPassword extends FormInputText
{
	private $_password1 = "";
	private $_password2 = "";
	private $_double_check = false;
	//public $_value = Array();
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $double_check Bool�en : indique si le mot de passe doit �tre saisi 2 fois pour contr�ler
	 */
	public function __construct($name, $text, $double_check = false, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_double_check = $double_check;
	}
	
	public function checkValue()
	{
		$back = false;
		
		if ($this->_required)
		{
			//print_r($this->_value);
			if (!isset($this->_value[0]))
			{
				//echo "isset";
				$back = false; 
			}
			else if (strlen($this->_value[0]) < 8)
			{
				//echo "longueur|" . strlen($this->_value[0]) . "|";
				$back = false;
			}
			else
			{
				//echo "egal";
				if ($this->_double_check && $this->_value[0] != $this->_value[1]) $back = false;
				else $back = true;
			}
		}
		else
		{
			$back = true;
		}
		
		return $back;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<tr id=\"tr_" . $this->_name . "\">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<p><input type=\"password\" size=\"25\" name=\"" . $this->_name . "[]\" id=\"id_" . $this->_name . "\"/>\n";
		if ($this->_double_check)
		{
			$output .= "<br/><input type=\"password\" size=\"25\" name=\"" . $this->_name . "[]\" id=\"id_" . $this->_name . "2\"/>\n";
		}
		$output .= "<p></td>
			<td></td>
			</tr>";
		
		return $output;
	}
}
?>