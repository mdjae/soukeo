﻿<?php 
require_once ($root_path . '/thirdparty/phpactiveresource-0.13-beta/ActiveResource.php');
/**
 * Formulaire de gestion des commandes
 */
class FormBugReportRedmine extends Form
{
	private $_category;
	private $_tracker;
	private $_description = "";
	private $_project_id;
	private $_url;
	private $_redmine_url;
	private $_ruser;
	private $_password;
	private $_version;
	
	public function __construct()
	{
		global $root_path, $version, $CORPORATE_PORTAL;
		$this->_name = "bugform";
		$this->_version = $version;
		
		$this->_redmine_url = SystemParams::getParam("redmine*url");
		$this->_user = "nsa";
		$this->_password = "t4t0u87;";
		$this->_project_id = SystemParams::getParam("redmine*id_project");
		
		// Phrases d'entete
		$this->_txt_CreateInit = ("Création d'un rapport de bug");
		$this->_txt_CreateOk = ("Le rapport de bug a été sauvé");
		$this->_txt_UpdateInit = "Modification";
		$this->_txt_UpdateOk = ("Mise à jour");
		
		// Forcément une création
		$this->_initCreate();

		//
		$con1 = new FormContainer("main");
		$con1->addItem(new FormInputSubmitButton("subbug", ("Soumettre le rapport")));
    	$con1->addItem(new FormInputSeparator(("Saisir les détails du rapport")));
    	
    	// Type de ticket
    	$tracker = new FormInputList("tracker",("Type"), $this->_type);
    	$tracker->addElement(1,"Anomalie");
    	$tracker->addElement(2,"Evolution");
    	$tracker->addElement(3,"Assistance");
    	$con1->addItem($tracker);
		
    	// Sujet
    	$subject = new FormInputText("subject",("Sujet"), "", true, 80);
		$con1->addItem($subject);
		
		// Description
		$description = new FormInputTextArea("description",("Description"), "", true, 10, 60);
		$con1->addItem($description);
    	
		// Catégorie
		$category = new FormInputList("category",("Categorie"), $this->_category);
		$category->addElement(1,"Accueil");
		$category->addElement(2,"Activités");
		$category->addElement(3,"Administration");
		$category->addElement(4,"Commercial");
		$category->addElement(11,"Cosmétique");
		$category->addElement(5,"Facturation");
		$category->addElement(6,"Global");
		$category->addElement(7,"Interfaces");
		$category->addElement(8,"Reporting");
		$category->addElement(9,"Ressources humaines");
		$category->addElement(10,"Technique");
		$con1->addItem($category);
		
		// URL
    	$url = new FormInputText("url",("URL"), $_SERVER['HTTP_REFERER'], true, 80);
		$con1->addItem($url);
		
		// Bouton
		$con1->addItem(new FormInputSubmitButton("subbug2", ("Soumettre le rapport")));
		
    	$this->addContainer($con1);
	}
	
	
	/**
	 * Initialise les propriétés du formulaire pour une création
	 */
	private function _initCreate()
	{
		$this->_status = "insert";
		return true;
	}
		
	/**
	 * Insert dans mantis
	 */
	public function _create()
	{
		$this->_subject = $_POST['subject'];
		$this->_description = $_POST['description'];
		$this->_tracker = $_POST['tracker'];
		$this->_url = $_POST['url'];
		$this->_category = $_POST['category'];
		
		$data = array
		(
			'subject' => $this->_subject,
			'project_id' => $this->_project_id,
			'description' => $this->_description,
			'category_id' => $this->_category,
		);
		
		$issue = new Issue($data);
		//$issue->site = $this->_redmine_url;
		$issue->site =  'http://' . $_SESSION['smartlogin'] . ':' . $_SESSION['smartpwd'] . '@' . $this->_redmine_url . "/";
		//$issue->user = $this->_user;
		//$issue->password = $this->_password;
		$issue->save();
		
		// Est-ce qu'on a une erreur ?
		if ($issue->id == "")
		{
			$this->backinfo['info_messages'][] = sprintf(("Erreur à la création du ticket : %s"), $issue->error);
		}
		else
		{
			$this->backinfo['info_messages'][] = sprintf(("Le ticket a été sauvegardé dans Redmine sous le numéro %s"), "<a target='_blank' href='http://" . $this->_redmine_url . "/issues/" . $issue->id . "'>#" . $issue->id . "</a>");
			$this->backinfo['disabled'][] = "category";
			$this->backinfo['disabled'][] = "subject";
			$this->backinfo['disabled'][] = "description";
			$this->backinfo['disabled'][] = "tracker";
			$this->backinfo['disabled'][] = "url";
		}
		
		return true;
	}
		
	/*
	 * Controle si l'on peut ajouter
	 */
	protected function _canInsert()
	{
		return true;
	}
}

class Issue extends ActiveResource
{
    //var $site = 'http://nsa:t4t0u87;@redmine.stratners.com/';
    var $request_format = 'xml'; // REQUIRED!
}
?>
