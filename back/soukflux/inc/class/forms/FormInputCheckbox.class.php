<?php 
/**
 * Objet g�n�rique d'input d'une checkbox d'un formulaire
 *
 */
class FormInputCheckbox extends FormInputText
{
	private $_checked = false;
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $checked Bool�en indiquant si la case est coch�e
	 */
	public function __construct($name, $text, $checked)
	{
		$this->_name = $name;
		$this->setText($text);
		$this->_checked = $checked;
	}
	
	public function checkValue()
	{
		return true;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<tr id=\"tr_" . $this->_name . "\">
				<td>
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<p>
						<input type=\"checkbox\" name=\"" . $this->_name . "\" id=\"id_' . $this->_name . '\" value=\"checked\"/";
		if ($this->_checked) $output .= " checked";
		$output .= "></p>\n";
		$output .= "</td>
				<td></td>
			</tr>";
		
		return $output;
	}
}
?>