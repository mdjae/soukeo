<?php 
/**
 * Objet g�n�rique d'input cach� d'un formulaire
 *
 */
class FormInputHidden extends FormInputText
{	
	/**
	 * @param $name Nom
	 * @param $value Valeur
	 */
	public function __construct($name, $value)
	{
		$this->_name = $name;
		$this->_value = $value;
		$this->_input = false;
	}
	
	public function display()
	{
		$output = "<tr id=\"tr_" . $this->_name . "\">
				<td colspan=\"3\">
					<div id=\"it_" . $this->_name . "\">
					<input id=\"id_" . $this->_name . "\" type=\"hidden\" name=\"" . $this->_name . "\" value=\"" . $this->_value . "\"/></div>
				</td>
			</tr>";
		
		return $output;
	}
	
	public function getJavascript()
	{
		return "";
	}
	
	/**
	 * @param $name Nom de l'input dont on d�pend
	 * @param $value Valeur qu'il doit prendre
	 */
	public function addDep($name, $value)
	{
		$this->_dep[] = array($name, $value);
	}
}
?>