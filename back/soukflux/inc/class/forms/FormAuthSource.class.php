﻿<?php 
/**
 * Formulaire des programmations
 */
class FormAuthSource extends Form
{
	private $_auth_source_id = 0;
	
	public function __construct($auth_source_id)
	{
		global $root_path;
		
		$this->_auth_source_id = $auth_source_id;
		$this->_name = "authsource";
		
		// Phrases d'entete
		$this->_txt_CreateInit = ("Création d'une nouvelle source d'authentification");
		$this->_txt_CreateOk = ("La source d'authentification a été créée");
		$this->_txt_UpdateInit = ("Modification d'une source d'authentification");
		$this->_txt_UpdateOk = ("Mise à jour de la source d'authentification enregistrée");
		
		// Est-ce une creation ou une mise à jour ?
		if ($auth_source_id == 0)
		{
			$this->_initCreate();
		}
		else
		{
			$this->_initUpdate();
		}
		
		//
		$con1 = new FormContainer("main", false	);
		
		$con1->addItem(new FormInputSubmitButton());
		$con1->addItem(new FormInputSeparator(''));
		$con1->addItem(new FormInputHidden("auth_source_id", $this->_auth_source_id));
		
		$con1->addItem(new FormInputText("auth_name", ("Nom"), $this->_auth_name, true, 40));
		$host = new FormInputText("host", ("Serveur LDAP"), $this->_host, true, 40);
		$host->setRightText(("Exemple: ldap.example.com"));
		$con1->addItem($host);
		$port = new FormInputText("port", ("Port"), $this->_port, true, 40);
		$port->setRightText(("Valeur par défaut : 389"));
		$con1->addItem($port);
		$base_dn = new FormInputText("base_dn", ("Base DN"), $this->_base_dn, true, 40);
		$base_dn->setRightText(("Exemple: DC=offipse,DC=com"));
		$con1->addItem($base_dn);
		$attr_login = new FormInputText("attr_login", ("Attribut de login"), $this->_attr_login, true, 40);
		$attr_login->setRightText(("Exemples de recherche d'un utilisateur LDAP:[AD: samAccountName] [openLDAP: dn] [Mac OS X: dn]"));
		$con1->addItem($attr_login);
		$attr_connexion = new FormInputText("attr_connexion", ("Attribut de connexion"), $this->_attr_connexion, true, 40);
		$attr_connexion->setRightText(("Exemples d'attribut utilisateur LDAP:[AD: userPrincipalName] [openLDAP: userPrincipalName] [Mac OS X: uid]"));
		$con1->addItem($attr_connexion);
		$con1->addItem(new FormInputText("account", ("Accompte de connexion"), $this->_account, true, 40));
		$account_password = new FormInputText("account_password", ("Mot de passe"), $this->_account_password, true, 40);
		$account_password->setAsPassword(true);
		$con1->addItem($account_password);
		
		$con1->addItem(new FormInputSeparator(''));
		$con1->addItem(new FormInputSubmitButton());
		
		$this->addContainer($con1);
		
		// On indique la grid à reloader
		$this->setGridParams('auth_sources','../../');
	}
	
	/**
	 * Initialise les propriétés du formulaire pour une création
	 */
	private function _initCreate()
	{
		$this->_status = "insert";
		
		$this->_auth_name =			"";
		$this->_host =				"";
		$this->_port =				"389";
		$this->_base_dn =			"";
		$this->_attr_connexion =	"";
		$this->_attr_login =		"";
		$this->_account =			"";
		$this->_account_password =	"";	

		return true;
	}
	
	/**
	 * Initialise les propriétés du formulaire pour une mise à jour
	 */
	private function _initUpdate()
	{
		// Mise à jour
		$this->_status = "update";
			
		$res2edit = dbQueryOne("SELECT name,
							host,
							port,
							base_dn,
							attr_connexion,
							attr_login,
							account,
							account_password
						FROM c_user_auth_sources 
						WHERE auth_source_id = '". $_GET['auth_source_id'] . "'");
		
		$this->_auth_name =			stripslashes($res2edit['name']);
		$this->_host =				stripslashes($res2edit['host']);
		$this->_port =				stripslashes($res2edit['port']);
		$this->_base_dn =			stripslashes($res2edit['base_dn']);
		$this->_attr_connexion =	stripslashes($res2edit['attr_connexion']);
		$this->_attr_login =		stripslashes($res2edit['attr_login']);
		$this->_account =			stripslashes($res2edit['account']);
		$this->_account_password =	stripslashes($res2edit['account_password']);	
		
		return true;
	}
	
	/**
	 * Insert en base en cas de création
	 */
	public function _create()
	{
		global $inc_path;

		// insert
		$query = "INSERT INTO c_user_auth_sources
						(
							name,
							host,
							port,
							base_dn,
							attr_connexion,
							attr_login,
							account,
							account_password
						)
					VALUES
						(
							'" . addslashes($_POST['auth_name']) ."',
							'" . addslashes($_POST['host']) . "',
							'" . addslashes($_POST['port']) . "',
							'" . addslashes($_POST['base_dn']) ."',
							'" . addslashes($_POST['attr_connexion']) ."',
							'" . addslashes($_POST['attr_login']) ."',
							'" . addslashes($_POST['account']) ."',
							'" . addslashes($_POST['account_password']) . "'
						)";
		
		dbQuery($query);
			
		// get id of the new user
		$this->_auth_source_id = dbInsertId();
		
		return true;
	}
	
	/**
	 * Update en base en cas de mise à jour
	 */
	public function _update()
	{
		// update
		$query = "UPDATE c_user_auth_sources set
				name =					'" . addslashes($_POST['auth_name']) . "',
				host =					'" . addslashes($_POST['host']) . "',
				port =					'" . addslashes($_POST['port']) . "',
				base_dn =				'" . addslashes($_POST['base_dn']) . "',
				attr_connexion =		'" . addslashes($_POST['attr_connexion']) . "',
				attr_login =			'" . addslashes($_POST['attr_login']) . "',
				account =				'" . addslashes($_POST['account']) . "',
				account_password =		'" . addslashes($_POST['account_password']) . "'
				WHERE auth_source_id = '" . $this->_auth_source_id . "'";
		dbQuery($query);
			
		return true;
	}

	
	/*
	 * Controle si l'on peut ajouter
	 */
	protected function _canInsert()
	{		
		return true;
	}
	
	/*
	 * Controle si l'on peut mettre à jour
	 */
	protected function _canUpdate()
	{
		return true;
	}
}
?>