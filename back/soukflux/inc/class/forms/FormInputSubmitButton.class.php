<?php 
/**
 * Objet g�n�rique de pr�sentation 
 *
 */
class FormInputSubmitButton extends FormInputText
{
	/**
	 * @param $text Texte � afficher
	 */
	public function __construct($name = "save", $text = "Enregistrer")
	{
		$this->_name = $name;
		$this->_required = false;
		$this->setText($text);

		// N'est pas une donn�es du formulaire, juste pr�sentation
		$this->_input = false;
	}
	
	public function checkValue()
	{
		return true;
	}
	
	public function display()
	{
		
		$output = "<tr id=\"tr_" . $this->_name . "\" ";
		if ($this->_hidden) $output .= " style=\"display: none;\" ";
		$output .= ">
						<td colspan=\"3\">
							<input type=\"submit\" id=\"id_" . $this->_name . "\" name=\"" . $this->_name . "\" id=\"submitter\" value=\"" . $this->_text ."\" ";
		if ($this->_readonly) $output .= " readonly=\"readonly\" ";
		if ($this->_disabled) $output .= " disabled ";
		$output .= ">
						</td>
					</tr>";
		
		return $output;
	}
}
?>