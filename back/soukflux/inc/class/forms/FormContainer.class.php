<?php 
/**
 * Bloque de pr�sentation dans le formulaire
 *
 */
class FormContainer
{
	private $_name = "";
	private $_collapse = false;
	
	public $_dep = array();
	
	// Tableau contenant les �l�ments du formulaire
	public $_items = Array();
	
	/**
	 * @param $name Nom
	 * @param $collapse Pli� � l'affichage
	 */
	public function __construct($name, $collapse = false)
	{
		$this->_name = $name;
		$this->_collapse = $collapse;
	}
	
	public function getName()
	{
		return $this->_name;
		$this->_items = Array();
	}
	
	public function addItem($item)
	{
		$this->_items[] = $item;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "";
		
		// Bouton pour d�plier
		if ($this->_collapse)
		{
			$output .= "<a href=\"#\" id=\"add_" . $this->_name . "\"><img id=\"img_" . $this->_name . "\" src=\"". $skin_path . "images/picto/add.gif\" border=\"0\" class=\"iconCircleWarn\" /></a>";
		}
		
		$output .= "<table width=\"100%\" id=\"tab_" . $this->_name . "\" cellpadding=\"2\" cellspacing=\"2\" border=\"0\"";
		
		$output .= ">";
		
		foreach($this->_items as $item)
		{
			$output .= $item->display();
		}
		
		$output .= "</table>";
		
		// Javascript pour controller le d�pli�
		if ($this->_collapse)
		{
			$output .= "<script type=\"text/javascript\" charset=\"utf-8\">
							
						  	var myFx_" . $this->_name . " = new Fx.Slide('tab_" . $this->_name . "', {
									duration: 1000,
									transition: Fx.Transitions.Pow.easeOut,
									onComplete : function(){
										if (myFx_" . $this->_name . ".open)
										{
											$('img_" . $this->_name . "').src = '". $skin_path . "images/picto/subtract.gif';
										}
										else
										{
											$('img_" . $this->_name . "').src = '". $skin_path . "images/picto/add.gif';
										}
									}
		  					}).hide();
		  					
			
							$('add_" . $this->_name . "').addEvent('click', function(e){
									e.stop();
									myFx_" . $this->_name . ".toggle();
							});
						</script>";
		}
		
		return $output;
	}
	
	/**
	 * @param $name Nom de l'input dont on d�pend
	 * @param $value Valeur qu'il doit prendre
	 */
	public function addDep($name, $value)
	{
		$this->_dep[] = array($name, $value);
	}

}
?>