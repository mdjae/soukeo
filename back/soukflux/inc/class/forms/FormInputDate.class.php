<?php 
/**
 * Objet g�n�rique d'input d'une date d'un formulaire
 *
 */
class FormInputDate extends FormInputText
{
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $text, $value, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		
		if ($value == "0000-00-00") $this->_value = "";
		else $this->_value = writeNormalDate($value);
		
	}
	
	public function checkValue()
	{
		if ($this->_needCheckValue())
		{
			if ($this->_required && $this->_value != "") return true;
			else if (!$this->_required) return true;
			else return false;
		}
		else return true;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<tr id=\"tr_" . $this->_name . "\">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<p>
						" . $this->_left_text . "
						<input type=\"text\" style=\"float: left;\" name=\"" . $this->_name . "\" id=\"id_" . $this->_name . "\" value=\"" . $this->_value . "\"/>
						" . $this->_right_text . "
						</p>\n";
		$output .= "</td>
				<td></td>
			</tr>";
		
		return $output;
	}
}
?>