﻿<?php 
/**
 * Formulaire de gestion des commandes
 */
class FormBugReportMantis extends Form
{
	private $_category = "";
	private $_reproducibilities = "";
	private $_severities = "";
	private $_priorities = "";
	private $_version = "";
	private $_description = "";
	private $_information = "";
	
	public function __construct()
	{
		global $root_path, $version, $CORPORATE_PORTAL;
		$this->_name = "bugform";
		$this->_version = $version;
		
		$user = SystemParams::getParam("mantis*user");
		$password = SystemParams::getParam("mantis*password");
		$project_id = SystemParams::getParam("mantis*id_project");
		
		// Phrases d'entete
		$this->_txt_CreateInit = ("Création d'un rapport de bug");
		$this->_txt_CreateOk = ("Le rapport de bug a été sauvé");
		$this->_txt_UpdateInit = "Modification";
		$this->_txt_UpdateOk = ("Mise à jour");
		
		// Forcément une création
		$this->_initCreate();

		//
		$con1 = new FormContainer("main");
		$con1->addItem(new FormInputSubmitButton("subbug", ("Soumettre le rapport")));
    	$con1->addItem(new FormInputSeparator(("Saisir les détails du rapport")));
		
		// Catégorie
		$category = new FormInputList("category",("Categorie"), $this->_category);

		// Récupération des catégorie
		$client = new SoapClient(SystemParams::getParam("mantis*wsdl_url"));
		$cat = $client->mc_project_get_categories($user, $password, $project_id);
		foreach($cat as $c)
		{
			$c = utf8_decode($c);
			$category->addElement($c,$c);
		}
		$con1->addItem($category);
		
		// Reproductibilité 
		$reproducibilities = new FormInputList("reproducibilities", ("Reproductibilité"), $this->_reproducibilities);
		$rep = $client->mc_enum_reproducibilities($user, $password);
		//echo "<pre>"; print_r($rep); echo "</pre>";
		foreach($rep as $r)
		{
			$r->name = utf8_decode($r->name);
			$reproducibilities->addElement($r->id,$r->name);
		}
		$con1->addItem($reproducibilities);
		
		// Sévérité 
		$severities = new FormInputList("severities", ("Sévérité"), $this->_severities);
		$sev = $client->mc_enum_severities($user, $password);
		//echo "<pre>"; print_r($rep); echo "</pre>";
		foreach($sev as $s)
		{
			$s->name = utf8_decode($s->name);
			$severities->addElement($s->id,$s->name);
		}
		$con1->addItem($severities);
		
		// Priorité 
		$priorities = new FormInputList("priorities", ("Priorité"), $this->_priorities);
		$pri = $client->mc_enum_priorities($user, $password);
		//echo "<pre>"; print_r($rep); echo "</pre>";
		foreach($pri as $p)
		{
			$p->name = utf8_decode($p->name);
			$priorities->addElement($p->id,$p->name);
		}
		$con1->addItem($priorities);

		// Version
		$version = new FormInputList("version", ("Version"), $this->_version);
		$ver = $client->mc_project_get_versions($user, $password, $project_id);
		//echo "<pre>"; print_r($ver); echo "</pre>";
		foreach($ver as $v)
		{
			$v->name = utf8_decode($v->name);
			$version->addElement($v->name,$v->name);
			// Si la version est identique à $this->_version, on force la valeur selectionnée
			if ($v->name == $this->_version) $version->setValue($v->name);
		}
		$con1->addItem($version);
		
		// Resolution
		/*
		$resolution = new FormInputList("resolution", _("resolution"), $this->_resolution);
		$res = $client->mc_enum_resolutions($user, $password);
		//echo "<pre>"; print_r($ver); echo "</pre>";
		foreach($res as $r)
		{
			$r->name = utf8_decode($r->name);
			$resolution->addElement($r->id,$r->id . " " . $r->name);
		}
		$con1->addItem($resolution);
		*/
		
		// Projection
		/*
		$projection = new FormInputList("projection", _("projection"), $this->_projection);
		$pro = $client->mc_enum_projections($user, $password);
		//echo "<pre>"; print_r($ver); echo "</pre>";
		foreach($pro as $p)
		{
			$p->name = utf8_decode($p->name);
			$projection->addElement($p->id,$p->id . ' ' . $p->name);
		}
		$con1->addItem($projection);
		*/
		
		// Eta
		/*
		$eta = new FormInputList("eta", _("Eta"), $this->_eta);
		$et = $client->mc_enum_etas($user, $password);
		//echo "<pre>"; print_r($ver); echo "</pre>";
		foreach($et as $e)
		{
			$e->name = utf8_decode($e->name);
			$eta->addElement($e->id,$e->id . ' ' . $e->name);
		}
		$con1->addItem($eta);
		*/
		
		// View States
		/*
		$view_state = new FormInputList("view_state", _("view_state"), $this->_view_state);
		$vie = $client->mc_enum_view_states($user, $password);
		//echo "<pre>"; print_r($ver); echo "</pre>";
		foreach($vie as $v)
		{
			$v->name = utf8_decode($v->name);
			$view_state->addElement($v->id,$v->id . ' ' . $v->name);
		}
		$con1->addItem($view_state);
		*/
		
		// Résumé
		$summary = new FormInputText("summary",("Résumé"), "", true, 80);
		$con1->addItem($summary);
		
		// Description
		$description = new FormInputTextArea("description",("Description"), "", true, 10, 60);
		$con1->addItem($description);
		
		// Informations complémentaires
		$additional_information = new FormInputTextArea("additional_information",("Informations complémentaires"), "", false, 10, 60);
		$con1->addItem($additional_information);
		
		// Champs spécifiques
		$cus = $client->mc_project_get_custom_fields($user, $password, $project_id);
		//echo "<pre>"; print_r($cus); echo "</pre>";
		// Boucle sur les champs spéciaux
		foreach($cus as $c)
		{
			if ($c->display_report)
			{
				if ($c->require_report == 1) $required = true;
				else $required = false;
				$field = $c->field;
				//echo "<p>" . $field->name . "</p>";
				if ($c->possible_values != "")
				{
					// Liste
					$list = new FormInputList("custom_" . $field->id, ($field->name), $c->default_value);
					//echo "<p>". utf8_decode($c->possible_values) . "</p>";
					$pos = explode("|", utf8_decode($c->possible_values));
					foreach($pos as $p)
					{
						$list->addElement($p,$p);
						// Si la version est identique à $this->_version, on force la valeur selectionnée
					}
					$con1->addItem($list);
				}
				else
				{
					$input = new FormInputText("custom_" . $field->id,($field->name), $c->default_value, $required, 25);
					if ($field->name == "URL")
					{
						$input->setValue($_SERVER['HTTP_REFERER']);
						$input->setSize(70);
					}
					$con1->addItem($input);
				}
			}
		}
		
		// Le site de l'application
		$con1->addItem(new FormInputHidden("Site", $CORPORATE_PORTAL));
		
		// Bouton
		$con1->addItem(new FormInputSubmitButton("subbug2", ("Soumettre le rapport")));
		
    	$this->addContainer($con1);
	}
	
	
	/**
	 * Initialise les propriétés du formulaire pour une création
	 */
	private function _initCreate()
	{
		$this->_status = "insert";
		return true;
	}
		
	/**
	 * Insert dans mantis
	 */
	public function _create()
	{
		$client = new SoapClient("http://bugs.offipse.com/api/soap/mantisconnect.php?wsdl");
		$issue = new IssueData();
		//$issue->id = null;
		$issue->project = new ObjectRef(1,"");
		$issue->category = utf8_encode($_POST['category']);
		$issue->projection = new ObjectRef($_POST['projection'],"");
		$issue->priority = new ObjectRef($_POST['priority'],"");
		$issue->severity = new ObjectRef($_POST['severity'],"");
		$issue->status = new ObjectRef($_POST['status'],"");
		$issue->summary = utf8_encode($_POST['summary']);
		$issue->version = utf8_encode($_POST['version']);
		$issue->reproducibility = new ObjectRef($_POST['reproducibility'],"");
		$issue->description = utf8_encode($_POST['description']);
		$issue->additional_information = utf8_encode($_POST['additional_information']);
		
		// Les champs custom
		$custom = array();
		foreach($_POST as $k => $p)
		{
			if (is_string($p) && strpos($k, "custom_") !== FALSE)
			{
				$t = explode("_",$k);
				$id = $t[1];
				$custom[] = new CustomFieldValueForIssueData($id, "", $p);
			}
		}
		$issue->custom_fields = $custom;
		
		$id = $client->mc_issue_add(SystemParams::getParam("mantis*user"), SystemParams::getParam("mantis*password"),$issue);
		
		$this->backinfo['info_messages'][] = ("La rapport de bug a été sauvegardé dans Mantis sous l'ID n°" . $id);
		$this->backinfo['disabled'][] = "subbug";
		$this->backinfo['disabled'][] = "category";
		$this->backinfo['disabled'][] = "reproducibilities";
		$this->backinfo['disabled'][] = "severities";
		$this->backinfo['disabled'][] = "priorities";
		$this->backinfo['disabled'][] = "version";
		$this->backinfo['disabled'][] = "summary";
		$this->backinfo['disabled'][] = "description";
		$this->backinfo['disabled'][] = "additional_information";
		$this->backinfo['disabled'][] = "subbug2";
		
		return true;
	}
		
	/*
	 * Controle si l'on peut ajouter
	 */
	protected function _canInsert()
	{
		return true;
	}
}

class IssueData
{
	public $id = 0;
	public $resolution;
	public $projection;
	public $eta;
	public $view_state;
	public $project;
	public $category;
	public $priority;
	public $severity;
	public $status;
	public $summary;
	public $version;
	public $reproducibility;
	public $description;
	public $additional_information;
	
	public function __construct()
	{
		$this->resolution = new ObjectRef(10,"");
		$this->projection = new ObjectRef(10,"");
		$this->eta = new ObjectRef(10,"");
		$this->view_state = new ObjectRef(10,"");
	}
}

class ObjectRef
{
	public $id = "";
	public $name = "";
	public function __construct($id, $name)
	{
		$this->id = $id;
		$this->name = $name; 
	}
}

class CustomFieldValueForIssueData
{
	public $field = null;
	public $value = "";
	
	public function __construct($id, $name, $value)
	{
		$this->field = new ObjectRef($id, $name);
		$this->value = $value;
	}
}
?>
