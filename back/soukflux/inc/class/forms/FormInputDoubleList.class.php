<?php 
/**
 * Objet g�n�rique de liste double pour formulaire
 *
 */
class FormInputDoubleList extends FormInputText
{
	// Contenu de la liste
	private $_right_elements = array();
	private $_left_elements = array();
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 */
	public function __construct($name, $text, $value, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_value = $value;
		
		// Ajout de la fonction de s�lection dans le formulaire
		$this->onsubmit[] = "selectAllElement_" . $this->_name . "()";
	}
	
	public function addLeftElement($value, $display)
	{
		$this->_left_elements[] = Array("value" => $value, "display" => $display);
	}
	
	public function addRightElement($value, $display)
	{
		$this->_right_elements[] = Array("value" => $value, "display" => $display);
	}
	
	public function checkValue()
	{
		$back = true;
		
		if ($this->_required)
		{
			if (!$this->_multiple)
			{
				if (!isset($this->_value) || $this->_value == "") $back = false;
			}
			else
			{
				if (!is_array($this->_value) || !isset($this->_value) || count($this->_value) == 0) $back = false;
			}
		}
		return $back;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<tr id=\"tr_" . $this->_name . "\" ";
		if ($this->_hidden) $output .= " style=\"display: none;\" ";
		$output .= ">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<script>
						function addElement_" . $this->_name . "()
						{
							var leftBox = document.getElementById('id_left_" . $this->_name . "');
							var rightBox = document.getElementById('id_right_" . $this->_name . "');
							for (var i = leftBox.options.length - 1;  i >= 0; i--)
							{
								if (leftBox.options[i].selected)
								{
									// Ajout dans l'autre liste
									//alert(i);
									//alert(leftBox.options[i].text);
									//leftBox.options[i].selected = false;
									//rightBox.options[rightBox.length] = leftBox.options[i];
									rightBox.options[rightBox.length] = new Option(leftBox.options[i].text,leftBox.options[i].value);
									leftBox.options[i] = null;
								}
							}
						}
		
						function removeElement_" . $this->_name . "()
						{
							var leftBox = document.getElementById('id_left_" . $this->_name . "');
							var rightBox = document.getElementById('id_right_" . $this->_name . "');
							for (var i = rightBox.options.length - 1;  i >= 0; i--)
							{
								if (rightBox.options[i].selected)
								{
									// Ajout dans l'autre liste
									//rightBox.options[i].selected = false;
									//leftBox.options[leftBox.length] = rightBox.options[i];
									leftBox.options[leftBox.length] = new Option(rightBox.options[i].text,rightBox.options[i].value);
									rightBox.options[i] = null;
								}
							}
						}
		
						function selectAllElement_" . $this->_name . "()
						{
							var rightBox = document.getElementById('id_right_" . $this->_name . "');
							for (var i = 0;  i < rightBox.options.length; i++)
							{
								// Selection
								rightBox.options[i].selected = true;
							}
						}
					</script>
					<table border='0'><tr><td><p>
						<select size=\"10\" multiple id=\"id_left_" . $this->_name . "\" ";
		if ($this->_readonly) $output .= " readonly=\"readonly\" ";
		if ($this->_disabled) $output .= " disabled ";
		
		$output .= " name=\"left_" . $this->_name . "[]\"";
		$output .= ">";
		
		foreach($this->_left_elements as $element)
		{
			$output .= "<option value=\"" . $element['value'] . "\"";
			$output .= ">" . $element['display'] . "</option>\n";
		}
		
		$output .= "</select>
					</p>
				</td>
				<td align=\"center\">
					<a href=\"javascript: addElement_" . $this->_name . "();\"><img src=\"" . $skin_path . "images/arrow_next.gif\" border=\"0\"/></a>
					<h2><a href=\"javascript: removeElement_" . $this->_name . "();\"><img src=\"" . $skin_path . "images/arrow_previous.gif\" border=\"0\"/></a></h2>
				</td>
				<td>
					<p>
						<select size=\"10\" multiple id=\"id_right_" . $this->_name . "\" ";
		if ($this->_readonly) $output .= " readonly=\"readonly\" ";
		if ($this->_disabled) $output .= " disabled ";
		
		$output .= " name=\"" . $this->_name . "[]\" onSubmit=\"selectAllUsers_" . $this->_name . "();\" ";
		$output .= ">";
		
		foreach($this->_right_elements as $element)
		{
			$output .= "<option value=\"" . $element['value'] . "\"";
			$output .= ">" . $element['display'] . "</option>\n";
		}
		
		$output .= "</select>
					</p>
				</td></tr></table></td>
				<td></td>
			</tr>";
		
		return $output;
	}
}
?>