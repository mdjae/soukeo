<?php 
/**
 * Objet qui permet de cr�er des formulaires
 *
 */
class Form extends SystemObjectPersistence
{
	// Tableau contenant les bloques du formulaire
	public $_containers = Array();

	// Nom
	protected $_name = "";

	// "insert" ou "update"
	protected $_status = "";

	// "ajax", "post" ou "get"
	private $_mode = "";

	// Boolean indiquant si upload de fichier
	private $_file_upload = false;

	// Phrase d'entete du formulaire
	protected $_txt_CreateInit = "";
	protected $_txt_CreateOk = "";
	protected $_txt_UpdateInit = "";
	protected $_txt_UpdateOk = "";

	/**
	 * Tableau contenant la liste des fonctions � appeler
	 * lors de la soumission du formulaire
	 * @var unknown_type
	 */
	protected $_onSubmit = array();

	// Tableau avec les infos de retour
	// cl� 'error_field' : liste des champs en erreur
	// cl� 'error_messages' : liste des messages d'erreur
	// cl� 'info_messages' : message principal de retour
	public $backinfo = array
	(
			'error_fields' => array(),
			'error_messages' => array(),
			'info_messages' => array(),
			'readonly' => array(),
			'disabled' => array(),
			'hidden' => array()
	);

	// Param�tres de la grid � recharger au besoin
	private $_grid_name = "";
	private $_grid_path = "";

	public function __construct($name)
	{
		$this->_containers = Array();
		$this->_name = $name;

		$this->_txt_CreateInit = ("Création");
		$this->_txt_CreateOk = ("Création ok");
		$this->_txt_UpdateInit = ("Mise à jour");
		$this->_txt_UpdateOk = ("Mise à jour ok");
	}

	public function addContainer($container)
	{
		$this->_containers[] = $container;
	}

	public function display()
	{
		global $root_path, $skin_path;

		// Sauvegarde du formulaire en base
		$this->_save();

		//echo $this->_status;

		$output = "<form method=\"post\" class=\"contentform\" action=\"" . $root_path . "/common/forms/forms.service.php\" id=\"" . $this->_name . "\">";

		$output .= "
					<input type=\"hidden\" name=\"form_name\" value=\"" . $this->_name . "\"/>
					<table border=\"0\" width=\"100%\">
					<tr><td colspan=\"3\"><div id=\"form_waiter\"></div></td></tr>
					<tr><td colspan=\"3\">
					<h3>";

		if ($this->_status == "insert") $output .= $this->_txt_CreateInit;
		else $output .= $this->_txt_UpdateInit;

		$output .= "</h3>
		<p id=\"head_" . $this->_name . "\">&nbsp;</p>
					</td></tr><tr><td>";

		foreach($this->_containers as $container)
		{
			$output .= $container->display();
		}

		$output .= "</td></tr></table>";
		$output .= "</form>";

		$output .= "<script language=\"JavaScript1.2\">
					$('" . $this->_name . "').addEvent('submit', function(e)
						{
							// Waiter
							var wait = $('form_waiter').empty().addClass('waiter');

							// Fonctions appelée avant l'envoi
							";

		// Ajout des fonctions onSubmit
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				foreach($item->onsubmit as $s)
				{
					$output .= $s . "\n";
				}
			}
		}

		$output .= "
							//Empeche la soumission
							e.stop();
							this.set('send', {encoding: 'utf-8',
								onComplete: function(response)
								{";

		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				if ($item->_input)
				{
					$output .= "$('it_" . $item->getName() . "').style.color = 'black';\n";
					$output .= "$('it_" . $item->getName() . "').style.fontWeight = 'normal';\n";
				}
			}
		}

		$output .= "				//alert(response);
									try
									{
										var myBack = JSON.decode(response);
									}
									catch(e)
									{
										alert('erreur : ' + response);
									}

									if (myBack.error_fields.length == 0)
									{\n";

		// Est-ce qu'il y a une grid � reloader ?
		// On ne reload pas � ce niveau s'il y a un upload de fichier, on va recharger au niveau de l'upload de fichiers
		// car sinon, la grid derri�re est reload�e avant que l'upload soit termin�, r�sultat on ne voit pas la modif en live
		// Redmine #111
		if ($this->_grid_name != "" && $this->_grid_path != "" && !$this->_file_upload)
		{
			$output2 = 'parent.$("#dialog").dialog("close");';
			$output .= "	if (typeof parent.gridReload == 'function') { parent.gridReload('" . $this->_grid_name . "','" . $this->_grid_path . "'); ".$output2 ." }\n";
			
		}

		/*
		 $output .= "
		 // Ok
		 //alert('ok');
		 $('head_" . $this->_name . "').innerHTML = \"<b>";

		 if ($this->_status == "insert") $output .= $this->_txt_CreateOk;
		 else $output .= $this->_txt_UpdateOk;

		 $output .= "</b>\";
		 */

		$output .= "					var e = '';
										for (var i = 0; i < myBack.info_messages.length; i++)
										{
											e += '<p>' + myBack.info_messages[i] + '</p>';
										}
										$('head_" . $this->_name . "').innerHTML = e;

										for (var i = 0; i < myBack.readonly.length; i++)
										{
											//alert('readonly');
											$('id_' + myBack.readonly[i]).readOnly = true;
										}

										for (var i = 0; i < myBack.disabled.length; i++)
										{
											//alert('disabled : ' + myBack.disabled[i]);
											$('id_' + myBack.disabled[i]).disabled = true;
										}

										for (var i = 0; i < myBack.hidden.length; i++)
										{
											//alert('hidden');
											$('tr_' + myBack.hidden[i]).style.display = 'none';
										}
									}
									else
									{
										//alert('ko');
										for (var i = 0; i < myBack.error_fields.length; i++)
										{
											//alert(myBack.error_fields[i]);
											$('it_' + myBack.error_fields[i]).style.color = 'red';
											$('it_' + myBack.error_fields[i]).style.fontWeight = 'bold';
										}

										var e = '';
										for (var i = 0; i < myBack.error_messages.length; i++)
										{
											e += '<p>' + myBack.error_messages[i] + '</p>';
											//alert(myBack.error_messages[i]);
										}

										$('head_" . $this->_name . "').innerHTML = \"<b>Un ou plusieurs champs ne sont pas correctement renseignés</b>\" + e;
									}

									//
									wait.removeClass('waiter');
									";

		// Si il y a de l'upload il faut soumettre le formulaire
		// de l'iframe
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				if (get_class($item) == "FormInputFileUpload")
				{
					//$output .= "alert(window." . $item->getName() . ".document.getElementById('" . $item->getName() . "'));\n";
					$output .= "window." . $item->getName() . ".document.getElementById('" . $item->getName() . "').submit();\n";
				}
			}
		}


		$output .= "		}});
							//Send the form.
							this.send();
						});\n";

		// D�pendances
		$mydeps = array();
		$mydeps2 = array();
		foreach($this->_containers as $container)
		{
			foreach($container->_dep as $dep)
			{
				$mydeps[$dep[0]][$container->getName()][] = array($container->getName(), $dep[1], "container");
				$mydeps2[$container->getName()][0] = "container";
				$mydeps2[$container->getName()][1][$dep[0]][] = array($container->getName(), $dep[1], "container");
			}

			foreach($container->_items as $item)
			{
				foreach($item->_dep as $dep)
				{
					// Constitution de la liste des d�pendances
					$mydeps[$dep[0]][$item->getName()][] = array($item->getName(), $dep[1], "item");
					$mydeps2[$item->getName()][0] = "item";
					$mydeps2[$item->getName()][1][$dep[0]][] = array($item->getName(), $dep[1], "item");
				}
			}
		}

		//echo "<pre>"; print_r($mydeps); echo "</pre>";
		//echo "<pre>"; print_r($mydeps2); echo "</pre>";

		// On constitue un tableau avec les valeurs des �l�ments
		$myvalues = array();
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				$myvalues[$item->_name] = $item->_value;
			}
		}


		//print_r($mydeps);

		// Boucle sur les �l�ments dont d'autres d�pendent
		foreach ($mydeps2 as $key => $dep)
		{
			/*******************************************
			 * Affichage initial
			 */
			$output .= "\nif (";
			$first = true;

			// Boucle sur les elements d�pendants
			foreach($dep[1] as $name => $item)
			{
				if (!$first) $output .= " || ";
				else $first = false;

				// Boucle sur les d�pendances
				$first2 = true;
				$output .= "(";
				foreach($item as $reta)
				{
					if (!$first2) $output .= " && ";
					else $first2 = false;

					$output .= "($('id_" . $name . "').value != \"" . $reta[1] . "\")";
				}
				$output .= ")";
			}

			$output .= ")\n{\n";
			if ($dep[0] == "container")
			{
				$output .= "\t$('tab_" . $item[0][0] . "').style.display = 'none';\n";
			}
			else $output .= "\t$('tr_" . $item[0][0] . "').style.display = 'none';\n";
			$output .= "}\n";
		}

		foreach ($mydeps as $key => $dep)
		{
			$output .= "\n$('id_" . $key . "').addEvent('change', function(e){\n";

			//$output .= "\t\talert('display ' + this.value + ' ' );\n";

			// Boucle sur les elements d�pendants
			foreach($dep as $name => $item)
			{
				$output .= "\tif (";
				$first = true;
				foreach($item as $valos)
				{
					if (!$first)
					{
						$output .= " || ";
					}
					else $first = false;
					$output .= "(this.value == '" . $valos[1] . "')";
				}
				//$output .= ")";
				foreach ($mydeps2[$name][1] as $zaza => $vals)
				{
					if ($zaza != $key) $output .= " && ($('id_" . $zaza . "').value == '" . $vals[0][1] . "')";
				}
				$output .= ")\n";
				$output .= "\t{\n";

				if ($item[0][2] == "container")
				{
					$output .= "\t\t$('tab_" . $name . "').style.display = '';\n";
				}
				else $output .= "\t\t$('tr_" . $name . "').style.display = '';\n";

				$output .= "\t}\n\telse\n\t{\n";

				if ($item[0][2] == "container")
				{
					$output .= "\t\t$('tab_" . $name . "').style.display = 'none';\n";
				}
				else $output .= "\t\t$('tr_" . $name . "').style.display = 'none';\n";

				$output .= "\t}\n";
			}

			$output .=	"});\n";

		}

		// Ecriture du javascript de chaque input
		$output .= "window.addEvent('domready',function(){\n";
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				$output .= $item->getJavascript() . "";
			}
		}
		$output .= "});\n";
		$output .= "</script>";

		return $output;
	}

	/*
	 * Controle si l'on peut ajouter
	 * Le controle se fait entre $_POST qui contient les nouvelles valeurs et les valeurs
	 * des input de l'objet actuelle
	 */
	protected function _canInsert()
	{
		return true;
	}

	/*
	 * Controle si l'on peut mettre � jour
	 * Le controle se fait entre $_POST qui contient les nouvelles valeurs et les valeurs
	 * des input de l'objet actuelle
	 */
	protected function _canUpdate()
	{
		return true;
	}

	/**
	 * Controle de formulaire et construit le message de retour
	 * 1/ Appel � _canInsert ou _canUpdate : les valeurs de l'objet ne sont pas encore mises � jour
	 * 2/ Appel � checkForm -> � ce moment les valeurs sont mises � jour par rapport � _POST
	 */
	public function checkForm()
	{
		$valid = true;
		//echo count($this->_items);
		//print_r($_POST);
		// Boucle sur les �l�ments du formulaire
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				//echo $item->getName() . " ";
				if (isset($_POST[$item->getName()]))
				{
					$item->setValue($_POST[$item->getName()]);
				}
				else
				{
					//echo "t";
					$item->setValue("");
				}

				if (!$item->checkValue())
				{
					$valid = false;
					$this->backinfo['error_fields'][] = $item->getName();
				}
			}
		}

		return $valid;
	}

	/**
	 * Initialise les propri�t�s du formulaire pour une cr�ation
	 */
	private function _initCreate()
	{
		return true;
	}

	/**
	 * Initialise les propri�t�s du formulaire pour une mise � jour
	 */
	private function _initUpdate()
	{
		return true;
	}

	/**
	 * Insert en base en cas de cr�ation
	 */
	protected function _create()
	{
		return true;
	}

	/**
	 * Update en base en cas de mise � jour
	 */
	protected function _update()
	{
		return true;
	}

	/**
	 * Sauvegare le formulaire suite � l'action "Enregistrer"
	 */
	public function saveForm()
	{
		// R�init de backinfo
		$this->backinfo['error_fields'] = array();
		$this->backinfo['error_messages'] = array();
		$this->backinfo['info_messages'] = array();
		$this->backinfo['readonly'] = array();
		$this->backinfo['disabled'] = array();
		$this->backinfo['hidden'] = array();


		if ($this->_status == "insert")
		{
			// Insertion
			if ($this->checkForm() && $this->_canInsert())
			{
				// Ok on peut ins�rer
				//$this->_utf8DecodePOST();
				$this->_feedPost();
				$this->_create();
				$this->_status = "update";
			}
		}
		else if ($this->_status == "update")
		{
			// Insertion
			if ($this->_canUpdate() && $this->checkForm())
			{
				// Ok on peut ins�rer
			//	$this->_utf8DecodePOST();
				$this->_feedPost();
				$this->_update();
			}
		}

		// Rechargement de l'objet et sauvegarde
		//$this->_initUpdate();
		$this->_save();

		// Ajout des message d'erreur
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				if ($item->getErrorMessage() != "")
				$this->backinfo['error_messages'][] = $item->getErrorMessage();

				if ($item->getInfoMessage() != "")
				$this->backinfo['info_messages'][] = $item->getInfoMessage();
			}
		}

		//$this->backinfo['debug'][] = $_FILES;
		//$this->backinfo['debug'][] = $_POST;

		foreach($this->backinfo as $key => $b)
		{
			foreach($b as $k => $m)
			{
				$this->backinfo[$key][$k] = SystemCharacterSet::dealWithAjaxString($m);
			}
		}
		return json_encode($this->backinfo);
	}

	/**
	 * Remet en iso le contenu de la variable _POST
	 * suite � un POST via ajax...
	 */
	private function _utf8DecodePOST()
	{
		foreach($_POST as $key => $val)
		{
			if (is_array($val))
			{
				foreach($val as $k => $v)
				{
					$_POST[$key][$k] = utf8_decode($v);
				}
			}
			else
			{
				$_POST[$key] = utf8_decode($val);
			}
		}
	}

	/**
	 * Permet d'indiquer si le formulaire permet d'uploader des fichiers
	 *
	 */
	public function setUpload($upload)
	{
		$this->_file_upload = $upload;
	}

	public function displayInputFileUpload($name)
	{
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				//echo $item->getName() . "<br/>";
				if ($item->getName() == $name)
				{
					//echo "toto";
					echo $item->displayIframe();
					echo "\n<script>\n";
					echo "if (typeof parent.parent.gridReload == 'function') { parent.parent.gridReload('" . $this->_grid_name . "','" . $this->_grid_path . "'); }\n";
					echo "</script>\n";
				}
			}
		}
	}

	public function dealWithInputFileUpload($name)
	{
		return true;
	}

	/**
	 * Complete la variable _POST pour cl�s qui ne sont pas renseign�es
	 * � cause du post via ajax mootools
	 */
	private function _feedPost()
	{
		foreach($this->_containers as $container)
		{
			foreach($container->_items as $item)
			{
				if (!isset($_POST[$item->getName()])) $_POST[$item->getName()] = "";
			}
		}
	}

	/**
	 * Param�trage de la grid � recharger apr�s sauvegarde
	 */
	protected function setGridParams($name, $path)
	{
		$this->_grid_name = $name;
		$this->_grid_path = $path;
	}
}
?>