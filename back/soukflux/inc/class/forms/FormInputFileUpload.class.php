﻿<?php 
/**
 * Objet générique d'input d'upload d'un fichier d'un formulaire
 *
 */
class FormInputFileUpload extends FormInputText
{
    private $_upload_type = "";
    private $_upload_path = "";
    private $_form_name = "";
    private $_hidden = "";

    /**
     * @param $name Nom
     * @param $text Label
     * @param $form_id Id du formulaire
     * @param $upload_type Type de fichier chargé : "file" pour un fichier ou "img" pour image
     * @param $upload_path Chemin vers le fichier
     */
    public function __construct($name, $text, $form_name, $upload_type = "", $upload_path = "", $hiddensup="")
    {
        $this->_name = $name;
        $this->setText($text);
        $this->_upload_type = $upload_type;
        $this->_upload_path = $upload_path;
        $this->_form_name = $form_name;
        $this->_input = false;
        $this->_hidden = $hiddensup;
    }

    static public function checkUpload()
    {
        $back = false;
        if (array_key_exists('photo',$_FILES) && $_FILES['photo']['name'] != "")
        {
            /////////////////////////////////////////////////
            // Ajout ou mise à jour de la photo de l'utilisateur

            // On check l'attachement
            // si il n'y a pas eu d'erreur lors de l'upload
            if ($_FILES['photo']['error'] != UPLOAD_ERR_OK)
            {
                $this->_error_message = ("Une erreur est survenue lors du chargement - ");
                switch ($_FILES['photo']['error'])
                {
                    case UPLOAD_ERR_INI_SIZE:
                        $this->_error_message .= ("Le fichier dépasse la taille maximum autorisée");
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        $this->_error_message .= ("Le fichier dépasse la taille maximum autorisée");
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        $this->_error_message .= ("Le fichier a été partiellement chargé");
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $this->_error_message .= ("Aucun fichier chargé");
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        $this->_error_message .= ("Le répertoire temporaire est introuvable");
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $this->_error_message .= ("Impossible d'écrire sur le disque");
                        break;
                }
            }
            else $back = true;
        }
        else $back = true;

        return $back;
    }
    
    public function generateCSS()
    {
      return "<style type='text/css'>
    					   h3 {
    					   		background-color: #EFF8FF;
    					   		color: #333333;
    					   		font-family: Arial;
    					   		font-size: 11px;
    					   		font-weight: normal;
    					   		padding: 5px 4px;
    					   	}
    					   	p {
    					   		font-family: Arial;
    					   		font-size: 11px;
    	    					font-weight: normal;
    	    					margin: 4px 5px;
    						}
    			  		</style>";
    }
    

    public function displayIframe()
    {
        global $skin_path;

        
        $output = "<style type='text/css'>
				   h3 {
				   		background-color: #EFF8FF;
				   		color: #333333;
				   		font-family: Arial;
				   		font-size: 11px;
				   		font-weight: normal;
				   		padding: 5px 4px;
				   	}
				   	p {
				   		font-family: Arial;
				   		font-size: 11px;
    					font-weight: normal;
    					margin: 4px 5px;
					}
		  		</style>

		<form method=\"POST\" enctype=\"multipart/form-data\" id=\"" . $this->getName() . "\">
				<!-- <input type=\"submit\" value=\"go\"/> -->
				<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"10000000\" />".
				( isset($this->_hidden)?  $this->_hidden  : "" ).
				"<table border=\"0\"><tr id=\"tr_" . $this->_name . "\">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<p><input type=\"file\" name=\"" . $this->_name . "\" id=\"id_' . $this->_name . '\" value=\"checked\"/></p>\n
				</td>";


        if ($this->_upload_type == "img")
        {
            $output .= "<td><img src=\"" . $this->_upload_path . "\" border=\"0\"/></td>";
        }
        else
        {
            $output .= "<td></td>";
        }

        $output .= "</tr></table></form>";

        return $output;
    }

    public function display()
    {
        global $skin_path, $root_path;

        $output = "<tr id=\"tr_" . $this->_name . "\">
				<td colspan=\"3\">
					<iframe frameborder=\"0\" name=\"" . $this->getName() . "\" width=\"50%\" height=\"100\" src=\"" . $root_path . "/common/forms/forms.upload.php?name=" . $this->_form_name . "&item=" . $this->_name. "\">
					</iframe>
				</td></tr>";

        return $output;
    }
}
?>