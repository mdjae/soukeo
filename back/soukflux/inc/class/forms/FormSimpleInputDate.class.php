<?php 
/**
 * Objet g�n�rique d'input d'une date d'un formulaire
 *
 */
class FormSimpleInputDate extends FormSimpleInputText
{
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $text, $value, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		
		if ($value == "0000-00-00") $this->_value = "";
		else $this->_value = writeNormalDate($value);
		
	}
	
	public function checkValue()
	{
		if ($this->_needCheckValue())
		{
			if ($this->_required && $this->_value != "") return true;
			else if (!$this->_required) return true;
			else return false;
		}
		else return true;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<td id=\"contentformtdtext\"><p>" . $this->_text . "</p></td>
				<td><p><input size=\"10\" type=\"text\" style=\"float: left;\" name=\"" . $this->_name . "[]\" id=\"id_" . $this->_name . "\" value=\"" . $this->_value . "\"/>
						</p></td>";
		
		return $output;
	}
	
	public function getJavascript()
	{
		/**
		 * @todo Faire fonctionner le datepicker dans les formulaires
		 */
		$output = "myCal = new Calendar({ id_" . $this->_name . ": 'd/m/Y' },
					{
						months: ['Janvier','F�vrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','D�cembre'],
						days: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
						navigation: 2,
						pad: 2
				});\n";
		return $output;
		//return "";
	}
}
?>