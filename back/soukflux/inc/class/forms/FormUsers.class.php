<?php 
/**
 * Formulaire de gestion des utilisateurs
 */
class FormUsers extends Form
{
	private $_user_id = 0;
	private $_erm_code = "";
	
	protected $_txt_CreateInit;
	protected $_txt_CreateOk;
	protected $_txt_UpdateInit;
	protected $_txt_UpdateOk;
	public function __construct($user_id)
	{
		global $root_path;
		
		$this->_user_id = $user_id;
		$this->_name = "userform";
		
		// Phrases d'entete
		$this->_txt_CreateInit = ("Création d'un nouvel utilisateur");
		$this->_txt_CreateOk = ("L'utilisateur a été créé");
		$this->_txt_UpdateInit = ("Modification d'un utilisateur");
		$this->_txt_UpdateOk = ("Mise à jour de l'utilisateur enregistrée");
		
		// Est-ce une creation ou une mise � jour ?
		if ($user_id == 0)
		{
			$this->_initCreate();
		}
		else
		{
			$this->_initUpdate();
		}
		
		//
		$con1 = new FormContainer("main", false	);
		
		$con1->addItem(new FormInputSubmitButton());
		$con1->addItem(new FormInputSeparator(''));
		$con1->addItem(new FormInputHidden("user_id", $this->_user_id));
		
		$con1->addItem(new FormInputText("firstname", ("Prénom"), $this->_firstname, true, 25, false, 32));
		$con1->addItem(new FormInputText("lastname", ("Nom"), $this->_lastname, true, 25, false, 32));
		
		$con1->addItem(new FormInputText("email",("Email"),$this->_email, true, 40, false, 64));
		
		// Code utilisateur
		// Si update, code en lecture seule
		//if ($user_id != 0) $readonly = true;
		//else $readonly = false;
		//$con1->addItem(new FormInputText("erm_code", ("Code franchis� Euromaster"), $this->_erm_code, true, 16, false, 16));
		//$con1->addItem($user_input);
		//$con1->addItem(new FormInputSeparator(("Suivi de la facturation")));
		
		//Cr�ation du menu d�roulant
		//$emailformat = new FormInputList("email_format", ("Format d'envoie : "), $this->_email_format, false);
    	//$emailformat->addElement(0, ("Format d'envoie"));
    	//$query = "SELECT ID_FORMAT, LABEL_FORMAT
    	//			FROM SUIVI_FACT_EXP_FORM
    	//			ORDER by ID_FORMAT";
    	//$res = dbQueryAll($query);
    	//foreach($res as $format)
    	//{
    	//	$emailformat->addElement($format['ID_FORMAT'], stripslashes($format['LABEL_FORMAT']));
    	//}
    	//$con1->addItem($emailformat);
				
		$con1->addItem(new FormInputSeparator(("Accés et sécurité")));
		$grp = new FormInputDoubleList("group", ("Groupe de sécurité"), "", true);
		// R�cup�ration des groupes
		$query = "SELECT g.group_id, g.name
					FROM c_group g
						LEFT JOIN c_group_users gu
							ON gu.group_id = g.group_id
							AND gu.user_id = " . $this->_user_id . "
					WHERE gu.group_id IS NULL
					ORDER BY g.name";
		$res = dbQueryAll($query);
		foreach($res as $g)
		{
			$grp->addLeftElement($g['group_id'],stripslashes($g['name']));
		}
		// R�cup�ration des groupes de l'utilisateur
		$query = "SELECT g.group_id, g.name
					FROM c_group g
						LEFT JOIN c_group_users gu
							ON gu.group_id = g.group_id
							AND gu.user_id = " . $this->_user_id . "
					WHERE gu.group_id IS NOT NULL
					ORDER BY g.name";
		$res = dbQueryAll($query);
		foreach($res as $g)
		{
			$grp->addRightElement($g['group_id'],stripslashes($g['name']));
		}
		$con1->addItem($grp);
		
		if ($user_id != 0) $readonly = true;
		else $readonly = false;
    	$con1->addItem(new FormInputText("login", ("Login"), $this->_login, true, 25, $readonly, 32));
    	
    	// Mode d'authenfication
    	$auth = new FormInputList("auth_source_id", ("Mode d'authentification"), $this->_auth_source_id, false);
    	$auth->addElement(0, ("Authentification Interne"));
    	$query = "SELECT auth_source_id, name
    				FROM c_user_auth_sources
    				ORDER by auth_source_id";
    	$res = dbQueryAll($query);
    	foreach($res as $auth_source)
    	{
    		$auth->addElement($auth_source['auth_source_id'], stripslashes($auth_source['name']));
    	}
    	$con1->addItem($auth);
    	
    	$con2 = new FormContainer("password", false	);
    	$con2->addDep("auth_source_id", 0);
		$con2->addItem(new FormInputPassword("password", ("Mot de passe<br/>(2 saisies pour vérification)"), true, false));
		
		$con3 = new FormContainer("main_bis", false	);
		$con3->addItem(new FormInputCheckbox("lockauth",("Utilisateur autorisé"),$this->_lockauth));
		$con3->addItem(new FormInputCheckbox("locked",("Utilisateur fermé"),$this->_locked));
        
        $con3->addItem(new FormInputCheckbox("skf_prd_access",("Accès au serveur de prod Soukflux"),$this->_skf_prd_access));
        $con3->addItem(new FormInputCheckbox("av_rec_access",("Accès au serveur de rec Avahis"),$this->_av_rec_access));
		
		//$con3->addItem(new FormInputSeparator(("Informations personnelles")));
		//$con3->addItem(new FormInputText("email",("Email"),$this->_email, true, 40, false, 64));
		//$con3->addItem(new FormInputText("address",("Adresse"),$this->_address, false, 40, false, 128));
		//$con3->addItem(new FormInputText("zipcode",("Code postal"),$this->_zipcode, false, 10, false, 10));
		//$con3->addItem(new FormInputText("city",("Ville"),$this->_city, false, 25, false, 32));

		$con3->addItem(new FormInputSeparator(''));
		$con3->addItem(new FormInputSubmitButton());
		
		$this->addContainer($con1);
		$this->addContainer($con2);
		$this->addContainer($con3);
		
		// On indique la grid � reloader
		$this->setGridParams('user','../../');
	}
	
	
	/**
	 * Initialise les propri�t�s du formulaire pour une cr�ation
	 */
	private function _initCreate()
	{
		$this->_status = "insert";
		$this->_gender =			"";	
		$this->_firstname =			"";	
		$this->_lastname =			"";	
		$this->_erm_code =			"";	
		$this->_email =				"";	
		$this->_address =			"";	
		//$this->_path =				"";
		$this->_zipcode =			"";	
		$this->_city =				"";	
		$this->_login =				"";
		$this->_auth_source_id =	0;	
		$this->_password =			"";
    	$this->_group =				"";		
		$this->_locked =			"";
    	$this->_lockauth =			" checked ";
    	$this->_email_format = 		"";
		return true;
	}
	
	/**
	 * Initialise les propri�t�s du formulaire pour une mise � jour
	 */
	private function _initUpdate()
	{
		// Mise � jour
		$this->_status = "update";
			
		$res2edit = dbQueryOne("SELECT c_user.gender,
              				c_user.firstname,
							c_user.lastname,
							c_user.erm_code,
							c_user.email,
							c_user.address,
							c_user.zipcode,
							c_user.city,
							c_user.login,
							c_user.password,
							c_group.group_id,
							c_group.name,
							c_user.locked,
							c_user.lockauth,
							c_user.auth_source_id,
							c_user.ID_FORMAT,
							c_user.skf_prd_access,
							c_user.av_rec_access 
						FROM c_user 
						  LEFT JOIN c_group_users 
        					ON c_group_users.user_id = c_user.user_id 
        				LEFT join c_group 
        					ON c_group.group_id = c_group_users.group_id
						WHERE c_user.user_id = '". $_GET['user_id'] . "'");

		$this->_gender 			=			stripslashes($res2edit['gender']);
   		$this->_firstname 	 	=			stripslashes($res2edit['firstname']);
		$this->_lastname 		=			stripslashes($res2edit['lastname']);
		$this->_erm_code 		=			stripslashes($res2edit['erm_code']);
		$this->_email 			=			stripslashes($res2edit['email']);
		$this->_address 		=			stripslashes($res2edit['address']);
		$this->_zipcode 		=			stripslashes($res2edit['zipcode']);
		$this->_city 			=			stripslashes($res2edit['city']);
		$this->_login 	 		=			stripslashes($res2edit['login']);
		$this->_password		=			stripslashes($res2edit['password']);
		$this->_auth_source_id	=			$res2edit['auth_source_id'];
		$this->_email_format	=			stripslashes($res2edit['ID_FORMAT']);
		
		if ($res2edit['locked'] == 1) $this->_locked = "checked";
		else $this->_locked = "";
		
		if ($res2edit['lockauth'] < SystemParams::getParam('security*pwd_error')) $this->_lockauth = "checked";
		else $this->_lockauth = "";
        
        if ($res2edit['skf_prd_access'] == 1) $this->_skf_prd_access = "checked";
        else $this->_skf_prd_access = "";

        if ($res2edit['av_rec_access'] == 1) $this->_av_rec_access = "checked";
        else $this->_av_rec_access = "";
        
		return true;
	}
	
	/**
	 * Insert en base en cas de cr�ation
	 */
	public function _create()
	{
		global $inc_path;
		
		// Champs sp�ciaux
		if (array_key_exists('locked',$_POST) && $_POST['locked'] == "checked") $locked = 1;
		else $locked = 0;

        if (array_key_exists('skf_prd_access',$_POST) && $_POST['skf_prd_access'] == "checked") $skf_prd_access = 1;
        else $skf_prd_access = 0;
        
        if (array_key_exists('av_rec_access',$_POST) && $_POST['av_rec_access'] == "checked") $av_rec_access = 1;
        else $av_rec_access = 0;
        		
    	if (array_key_exists('lockauth',$_POST) && $_POST['lockauth'] == "checked") $lockauth = 0;
		else $lockauth = SystemParams::getParam('security*pwd_error');
		
	/*	if (array_key_exists('business_card',$_POST) && $_POST['business_card'] == "checked") $business_card = 'TRUE';
		else $business_card = 'FALSE';
	*/	
			
		// insert
		$query = "INSERT INTO c_user
						(
							gender,
              				firstname,
							lastname,
							usercode,
							erm_code,
							email,
							address,
							zipcode,
							city,
							login,
							auth_source_id,\n";
		
		if (is_array($_POST['password']) && $_POST['password'][0] != '')
			$query .= " password,\n";
			
		$query .= "			locked,
		          			lockauth,
		          			ID_FORMAT,
		          			skf_prd_access,
		          			av_rec_access
						
						)
					VALUES
						(
							'" . addslashes($_POST['gender']) ."',
              				'" . addslashes($_POST['firstname']) ."',
							'" . addslashes($_POST['lastname']) . "',
							'" . addslashes($_POST['login']) . "',
							'" . addslashes($_POST['erm_code']) . "',
							'" . addslashes($_POST['email']) ."',
							'" . addslashes($_POST['address']) ."',
							'" . addslashes($_POST['zipcode']) ."',
							'" . addslashes($_POST['city']) ."',
							'" . addslashes($_POST['login']) ."',
							'" . $_POST['auth_source_id'] ."',";
		if (is_array($_POST['password']) && $_POST['password'][0] != '')
				$query .= "'" . md5($_POST['password'][0]) ."',";
		
				$query .= "'" . $locked ."',
							" . $lockauth .",
							'" .addslashes($_POST['email_format'])."', 
							" . $skf_prd_access .",
                            " . $av_rec_access ."
						)";
		    $db = new sdb();
		$rs = $db->exec($query);
		//dbQuery($query);
			
		// get id of the new user
		$this->_user_id = $db->lastInsertId($rs);
		$this->_login = $_POST['login'];
		
		// Insertion des groupes de s�curit�
		foreach($_POST['group'] as $g)
		{
			$query = "INSERT INTO c_group_users (user_id, group_id) VALUES (" . $this->_user_id . ", " . $g . ")";
			dbQuery($query);
		} 
		
		// Cr�ation du r�pertoire FTP
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/'.$_POST['login']."/", 0777);
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/'.$_POST['login']."/OUT/", 0777);
		
		// Cr�ation du r�pertoire FTP d'archivage
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/archives/'.$_POST['login']."/", 0777);
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/archives/'.$_POST['login']."/OUT/", 0777);
		
		// Cr�ation du r�pertoire de synchro
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/synchro/'.$_POST['erm_code']."/", 0777);
		
		// Cr�ation du r�pertoire de rejet
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/rejects/'.$_POST['login']."/", 0777);
		//
		// Cr�ation du r�pertoire de rejet dans le dossier d'archive
		//mkdir(SystemParams::getParam("ftp*ftp_local_path") . '/archives/rejects/'.$_POST['login']."/", 0777);		
		
		// Message de retour
		$this->backinfo['info_messages'][] = ("L'utilisateur a été créé");
		// On indique qu'il faut d�sactiver le code utilisateur apr�s l'ajout
		//$this->backinfo['readonly'][] = "erm_code";
		
		return true;
	}
	
	/**
	 * Update en base en cas de mise � jour
	 */
	public function _update()
	{
		// Champs sp�ciaux
		// DEBUG
		//echo serialize($_POST);
		if (array_key_exists('locked',$_POST) && $_POST['locked'] == "checked") $locked = 1;
		else $locked = 0;
		
		if (array_key_exists('lockauth',$_POST) && $_POST['lockauth'] == "checked") $lockauth = 0;
		else $lockauth = SystemParams::getParam('security*pwd_error');

        if (array_key_exists('skf_prd_access',$_POST) && $_POST['skf_prd_access'] == "checked") $skf_prd_access = 1;
        else $skf_prd_access = 0;
        
        if (array_key_exists('av_rec_access',$_POST) && $_POST['av_rec_access'] == "checked") $av_rec_access = 1;
        else $av_rec_access = 0;
        		
		if (array_key_exists('business_card',$_POST) && $_POST['business_card'] == "checked") $business_card = 'TRUE';
		else $business_card = 'FALSE';


		// update
		$query = "UPDATE c_user set
				gender =			'" . addslashes($_POST['gender'])."',
        		firstname =			'" . addslashes($_POST['firstname'])."',
				lastname =			'" . addslashes($_POST['lastname'])."',
				usercode =			'" . addslashes($_POST['login'])."',
				erm_code =			'" . addslashes($_POST['erm_code'])."',
				email =				'" . addslashes($_POST['email'])."',
				address =			'" . addslashes($_POST['address'])."',
				zipcode =			'" . addslashes($_POST['zipcode'])."',
				city =				'" . addslashes($_POST['city'])."',
				login =				'" . addslashes($_POST['login'])."',
				ID_FORMAT =				'" . addslashes($_POST['email_format'])."',
				auth_source_id =	'" . $_POST['auth_source_id'] . "',";

		// Si les mots de passes sont fournis, on les maj
		if (is_array($_POST['password']) && $_POST['password'][0] != '')
			$query .= " password =			'" . md5($_POST['password'][0])."',";

	
		$query .= "	locked =			'" . $locked ."',
		        skf_prd_access =          '" . $skf_prd_access ."',
		        av_rec_access =          '" . $av_rec_access ."',
		    	lockauth =			" . $lockauth ." 
				WHERE user_id = '" . $this->_user_id . "'";
		dbQuery($query);
		
		// Suppression et mise � jour des groupes de s�curit�
		$query = "DELETE FROM c_group_users WHERE user_id = " . $this->_user_id;
		dbQuery($query);
		foreach($_POST['group'] as $g)
		{
			$query = "INSERT INTO c_group_users (user_id, group_id) VALUES (" . $this->_user_id . ", " . $g . ")";
			dbQuery($query);
		} 

		$this->backinfo['info_messages'][] = utf8_decode(("L'utilisateur a été mis à jour"));
		return true;
	}
	
	/**
	 * G�re l'ajout ou la mise � jour de la photo
	 */
	private function _updatePhoto()
	{
		global $TMP_dir;
	
		// Delete de l'ancienne photo
		$query = "DELETE FROM c_user_photo
					WHERE user_id = " . $this->_user_id;
		dbQuery($query);

		$name		= $_FILES['photo']['name'];
		$filetype	= $_FILES['photo']['type'];
		$size		= $_FILES['photo']['size'];
		$tmp_name	= $_FILES['photo']['tmp_name'];
	
		$content	= base64_encode(file_get_contents($tmp_name));
		list($img_width, $img_height, $type, $attr) = getimagesize($tmp_name);
	
		// G�n�ration du thumbnail et de l'image � afficher dans l'annuaire
		// Thumbnail
		$thumb_name = $TMP_dir . "/thumb_" . $this->_user_id;
		$thumb_width = 0;
		$thumb_height = 0;
		list($thumb_width, $thumb_height) = createThumb($tmp_name,$thumb_name,36,40,true);
		$thumb_content = @base64_encode(file_get_contents($thumb_name));

		// Image � afficher dans l'annuaire
		$disp_name = $TMP_dir . "/disp_" . $this->_user_id;
		$disp_width = 0;
		$disp_height = 0;
		list($disp_width,$disp_height) = createThumb($tmp_name,$disp_name,90,100,true);
		$disp_content = @base64_encode(file_get_contents($disp_name));

		// Insertion de l'attachement
		$query = "INSERT INTO c_user_photo
					(
						user_id,
						img_content,
						img_filetype,
						img_size,
						img_name,
						img_width,
						img_height,
						thumb_content,
						thumb_width,
						thumb_height,
						disp_content,
						disp_width,
						disp_height
					)
					VALUES
					(
						" . $this->_user_id . ",
						'" . dbEscape($content) . "',
						'" . $filetype . "',
						'" . $size . "',
						'" . dbEscape($name) . "',
						" . $img_width . ",
						" . $img_height . ",
						'" . $thumb_content . "',
						'" . $thumb_width . "',
						'" . $thumb_height . "',
						'" . $disp_content . "',
						'" . $disp_width . "',
						'" . $disp_height . "'
					)";
		//echo $query;
		dbQuery($query);
	}
	
	public function dealWithInputFileUpload($name)
	{
		if (array_key_exists('photo',$_FILES) && $_FILES['photo']['name'] != "" && FormInputFileUpload::checkUpload())
		{
			$this->_updatePhoto();
		}
		return true;
	}
	
	/*
	 * Controle si l'on peut ajouter
	 */
	protected function _canInsert()
	{
		$back = false;
		// On doit controller que le login n'existe pas d�j�
		$query = "SELECT count(1) as nb
					FROM c_user
					WHERE login = '" . $_POST['login'] . "'";
		$res = dbQueryOne($query);
		
		if ($res['nb'] > 0)
		{
			// Le login existe d�j�
			$this->backinfo['error_messages'][] = ("Un utilisateur existe déjà avec ce login");
			$this->backinfo['error_fields'][] = "login";
			$back = false;
		}
		else
		{
			$back = true;
		}
		
		return $back;
	}
	
	/*
	 * Controle si l'on peut mettre � jour
	 */
	protected function _canUpdate()
	{
		$back = false;
		// Est-ce que le login change ?
		if ($_POST['login'] != $this->_login)
		{
			//print_r($_POST);
			// Le changement n'est pas autoris�
			$this->backinfo['error_messages'][] = ("Impossible de changer le login");
			$this->backinfo['error_fields'][] = "login";
			$back = false;
		}
		else $back = true;
		
		return $back;
	}
}
?>