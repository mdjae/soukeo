<?php 
/**
 * Objet g�n�rique d'input d'un text area dans un formulaire
 *
 */
class FormInputTextArea extends FormInputText
{
	private $_rows = 10;
	private $_cols = 20;
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $text, $value, $required = false, $rows = 10, $cols = 20)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->_rows = $rows;
		$this->_cols = $cols;
		$this->setText($text);
		$this->_value = $value;
		
	}
	
	public function checkValue()
	{
		if ($this->_needCheckValue())
		{
			if ($this->_required && $this->_value != "") return true;
			else if (!$this->_required) return true;
			else return false;
		}
		else return true;
	}
	
	public function display()
	{
		global $skin_path;
		
		$output = "<tr id=\"tr_" . $this->_name . "\" ";
		if ($this->_hidden) $output .= " style=\"display: none;\" ";
		$output .= ">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<p>
						" . $this->_left_text . "
						<textarea cols=\"" . $this->_cols . "\" rows=\"" . $this->_rows . "\" name=\"" . $this->_name . "\" id=\"id_" . $this->_name . "\" ";
		if ($this->_readonly) $output .= " readonly=\"readonly\" ";
		if ($this->_disabled) $output .= " disabled ";
		$output .= ">" . $this->_value . "</textarea>
						" . $this->_right_text . "
						</p>\n";
		$output .= "</td>
				<td></td>
			</tr>";
		
		return $output;
	}
}
?>