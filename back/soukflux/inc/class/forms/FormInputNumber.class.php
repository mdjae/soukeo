<?php 
/**
 * Objet g�n�rique d'input num�rique d'un formulaire
 *
 */
class FormInputNumber extends FormInputText
{
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $text, $value, $required = false, $size = 20)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_value = $value;
		$this->_size = $size;
	}
	
	public function checkValue()
	{
		if ($this->_needCheckValue())
		{
			if ($this->_required && $this->_value != "") return true;
			else if (!$this->_required) return true;
			else return false;
		}
		else return true;
	}
	
	public function display()
	{
		/**
		 * @todo Gestion du s�parateur num�rique dans la config
		 */
		$output = "<tr id=\"tr_" . $this->_name . "\">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text . "</h3>
				</td>
				<td>
					<p>" . $this->_left_text . "
					<input size=\"" . $this->_size . "\" type=\"text\" name=\"" . $this->_name . "\" id=\"id_" . $this->_name . "\" value=\"" . $this->_value . "\"/>
					" . $this->_right_text . "</p>
				</td>
				<td></td>
			</tr>";
		
		return $output;
	}
}
?>