<?php 
/**
 * Objet g�n�rique d'input texte d'un formulaire
 *
 */
class FormInputText
{
	public $_name = "";
	public $_value = "";
	public $_error_message = "";
	public $_info_message = "";
	public $_required = false;
	private $_size = 0;
	private $_readonly = false;
	private $_disabled = false;
	private $_hidden = false;
	private $_maxlength = 0;
	public $_input = true;
	public $onsubmit = array();
	
	public $_password = false;
	
	public $_text = "";
	public $_right_text = "";
	public $_left_text = "";
	
	public $_dep = array();
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 * @param $readonly En lecture seule, on peut le copier mais pas le modifier
	 * @param $maxlength Longueur maximale
	 */
	public function __construct($name, $text, $value, $required = false, $size = 20, $readonly = false, $maxlength = 0, $disabled=false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_value = $value;
		$this->_size = $size;
		$this->_readonly = $readonly;
		$this->_maxlength = $maxlength;
		$this->_disabled = $disabled;
	}
	
	public function getName()
	{
		return $this->_name;
	}
	
	public function setText($text)
	{
		$this->_text = $text;
		if ($this->_required) $this->_text .= "<b class=\"req\">*</b>";
	}
	
	public function getValue($value)
	{
		return $this->_value;
	}
	
	public function setValue($value)
	{
		$this->_value = $value;
	}
	
	public function setSize($size)
	{
		$this->_size = $size;
	}
	
	public function setRightText($text)
	{
		$this->_right_text = $text;
	}
	
	public function setLeftText($text)
	{
		$this->_left_text = $text;
	}
	
	public function setDisabled($disabled)
	{
		$this->_disabled = $disabled;
	}
	
	public function setAsPassword($pass)
	{
		$this->_password = $pass;
	}
	
	public function setHidden($hidden)
	{
		$this->_hidden = $hidden;
	}
	
	public function checkValue()
	{
		if ($this->_needCheckValue())
		{
			if ($this->_required && $this->_value != "") return true;
			else if (!$this->_required) return true;
			else return false;
		}
		else return true;
	}
	
	public function display()
	{
		// Est-ce que c'est un mot de passe ?
		if ($this->_password) $type = "password";
		else $type = "text";
		
		$output = "<tr id=\"tr_" . $this->_name . "\" ";
		if ($this->_hidden) $output .= " style=\"display: none;\" ";
		$output .= ">
				<td id=\"contentformtdtext\">
					<h3 id=\"it_" . $this->_name . "\">" . $this->_text;
		$output .= "
					</h3>
				</td>
				<td>
					<p>" . $this->_left_text . "
					<input size=\"" . $this->_size . "\" type=\"" . $type . "\" name=\"" . $this->_name . "\"
						id=\"id_" . $this->_name . "\" value=\"" . $this->_value . "\"";
		if ($this->_readonly) $output .= " readonly=\"readonly\" ";
		if ($this->_disabled) $output .= " disabled ";
		if ($this->_maxlength != 0) $output .= " maxlength=\"" . $this->_maxlength . "\" ";
		$output .= "/></p>
				</td>
				<td><p>" . $this->_right_text . "</p></td>
			</tr>";
		
		return $output;
	}
	
	public function getJavascript()
	{
		return "";
	}
	
	/**
	 * @param $name Nom de l'input dont on d�pend
	 * @param $value Valeur qu'il doit prendre
	 */
	public function addDep($name, $value)
	{
		$this->_dep[] = array($name, $value);
	}
	
	/**
	 * Recup�re le message d'erreur apr�s le check
	 */
	public function getErrorMessage()
	{
		return $this->_error_message;
	}
	
	/**
	 * Recup�re le message d'info apr�s le check
	 */
	public function getInfoMessage()
	{
		return $this->_info_message;
	}
	
	/**
	 * Indique s'il est n�cessaire de controler la valeur
	 * de cet �l�ment du formulaire
	 * En cas de d�pendance non remplie, pas de controle
	 */
	protected function _needCheckValue()
	{
		// Par d�faut on doit controller
		$need = true;
		
		// Est-ce qu'il y a des d�pendance ?
		if (count($this->_dep) > 0)
		{
			// Il y a une ou plusieurs d�pendances
			// Par met par d�faut aucun controle
			$need = false;
			
			// On boucle sur les d�pendances
			foreach($this->_dep as $dep)
			{
				// Si une est remplie, on doit faire un controle
				if (isset($_POST[$dep[0]]) && $_POST[$dep[0]] == $dep[1]) $need = true;
			}
		}
		return $need;
	}
}
?>