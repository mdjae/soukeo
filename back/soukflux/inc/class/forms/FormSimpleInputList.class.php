<?php 
/**
 * Objet g�n�rique de liste pour formulaire
 *
 */
class FormSimpleInputList extends FormSimpleInputText
{
	// Contenu de la liste
	private $_elements = array();
	
	// Indique si la s�lection est multiple
	private $_multiple = false;
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $text, $value, $multiple = false, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_value = $value;
		$this->_multiple = $multiple;
	}
	
	public function addElement($value, $display)
	{
		$this->_elements[] = Array("value" => $value, "display" => $display);
	}
	
	public function checkValue()
	{
		$back = true;
		
		if ($this->_needCheckValue())
		{
  		if ($this->_required)
  		{
  			if (!$this->_multiple)
  			{
  				if (!isset($this->_value) || $this->_value == "") $back = false;
  			}
  			else
  			{
  				if (!is_array($this->_value) || !isset($this->_value) || count($this->_value) == 0) $back = false;
  			}
  		}
  	}
		return $back;
	}
	
	public function display()
	{
		$output = "<td><p>" . $this->_text . "</p></td><td><p>";
		$output .= "<select id=\"id_" . $this->_name . "\" ";
		if ($this->_readonly) $output .= " readonly=\"readonly\" ";
		if ($this->_disabled) $output .= " disabled ";
		
		if ($this->_multiple)
		{
			$output .= " name=\"" . $this->_name . "[][]\"";
			$output .= " multiple size=\"7\"";
		}
		else $output .= " name=\"" . $this->_name . "[]\"";
		$output .= ">";
		
		foreach($this->_elements as $element)
		{
			$output .= "<option value=\"" . $element['value'] . "\"";
			
			// Si la valeur est un tableau, multi-valeurs
			if (is_array($this->_value))
			{
				if (in_array($element['value'],$this->_value)) $output .= " selected";
			}
			else
			{
				if ($element['value'] == $this->_value) $output .= " selected";
			}
			$output .= ">" . $element['display'] . "</option>\n";
		}
		
		$output .= "</select></p></td>";

		return $output;
	}
	
}
?>