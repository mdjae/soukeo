<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

/**
 * Batch qui va parcourir tout les e-commercant de la base de donnée afin de connaitre leur technologie.
 * A partir de leur technologie le Batch pourra lancer le batch d'import dédié.
 * @author Philippe_LA
 */
class BatchImportGrossiste extends Batch {

	static protected $_description = "[EXT] -- Import des catalogues grossistes";
	protected $_name = "BatchImportGrossiste";
	protected static $db;
	private static $regleCalcul;

	protected $grilleChronopost = array();

	protected $rep = "/opt/soukeo/import/grossistes/";
	protected $repArch = "/opt/soukeo/archives/import/grossistes/";
	protected $csv_to_parse = "";
	//URL de l'ecommerçant à parser
	protected $delimiter = "";
	protected $grossisteID = "";
	//VENDOR_ID de l'ecommercant
	protected $tech = "";
	protected $productListAttribute = array();
	//Liste produits avec ses attributs finale array()
	protected $attributeList = null;
	//Liste des différents attributs (entete) array()

	protected $specGrossiste = "";
	protected $grossisteNom = "";
	protected $nbInsert = 0;
	//log
	protected $nbUpdate = 0;
	//log
	protected $nbErrors = 0;
	//log
	protected $nbInactive = 0;
	protected $nbModifSellingStock = 0;
	protected $nbModifSellingCoef = 0;
	protected $nbModifSellingMissing = 0;
	protected $nbAlreadySold = 0;

	protected $nbInactiveMissingTot = 0;
	protected $nbInsertTot = 0;
	protected $nbUpdateTot = 0;
	protected $nbErrorsTot = 0;
	protected $nbInactiveTot = 0;
	protected $nbModifSellingStockTot = 0;
	protected $nbModifSellingCoefTot = 0;
	protected $nbModifSellingMissingTot = 0;
	protected $nbAlreadySoldTot = 0;

	protected $errors = array();
	protected $eanList = array();
	protected $refList = array();

	protected $coefmin;
	protected $coefmax;
	protected $weightmin;
	protected $weightmax;
	protected $stockmin;
	protected $autoAssoc;

	protected $listCat;
	//protected $rep = "/opt/soukeo/import/";

	/**
	 * Fonction de routine du batch
	 */
	public function work() {
		global $root_path;
		global $applicationMode;

		$this -> coefmin = SystemParams::getParam('grossiste*coef');
		$this -> coefmax = SystemParams::getParam('grossiste*coefmax');
		$this -> weightmin = SystemParams::getParam('grossiste*weightmin');
		$this -> weightmax = SystemParams::getParam('grossiste*weightmax');
		$this -> stockmin = SystemParams::getParam('grossiste*stock');
		$this -> autoAssoc = SystemParams::getParam('grossiste*autoassoc');

		var_dump($this -> autoAssoc);

		$temps_debut = microtime(true);

		$this -> _appendLog("Batch " . $this -> _name);
		$this -> _appendLog("");
		$this -> _appendLog("---------------------------------------------------------------------------------------------");
		$this -> _appendLog("---------------------------------------------------------------------------------------------");

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		//self::$regleCalcul  = new BusinessCalcul();
		self::$regleCalcul = new BusinessCalculNEW();
		self::$db -> prepareInsertSfkPrixGrossiste();
		self::$db -> prepInsertSfkCp();
		$this -> grilleChronopost = self::$db -> getGrilleChronopost();

		$list_grosssiste = self::$db -> getAllGrossiste();

		foreach ($list_grosssiste as $dealer) {

			$this -> setGrossisteID($dealer['GROSSISTE_ID']);

			$this -> tech = $dealer['TECH'];

			$this -> grossisteNom = $dealer['GROSSISTE_NOM'];

			$class = "BusinessGrossiste" . $dealer['CLASS_NAME'];
			$this -> specGrossiste = new $class();

			$this -> setDelimiter($this -> specGrossiste -> getDelimiter());

			if ($this -> tech == "XML") {
				$this -> csv_to_parse = $this -> rep . $dealer['GROSSISTE_CSV'] . ".xml";
			} else {
				$this -> csv_to_parse = $this -> rep . $dealer['GROSSISTE_CSV'] . ".csv";
			}

			if (($dealer['ACTIVE'])) {

				$this -> _appendLog("GROSSISTE_ID : " . $dealer['GROSSISTE_ID'] . " : " . $dealer['CSV_SPEC'] . " : " . $dealer['GROSSISTE_CSV']);

				if ($this -> grossisteID == "2") {
					$this -> listCat = self::$db -> getListWeightByCat($this -> grossisteID);

				}

				self::$db -> prepareInsertSfkProdGR($this -> grossisteID);

				$parsing_ok = null;

				if ($this -> tech == "XML") {
					$parsing_ok = $this -> parseXML($this -> csv_to_parse);
				} else {
					$parsing_ok = $this -> parseCSV($this -> csv_to_parse);
				}

				//SI LE PARSING S'EST BIEN DEROULE ET FICHIER PAS ABSENT
				if ($parsing_ok) {
					$this -> _appendLog("Verif produits absents du CSV ...");
					$this -> checkMissingProducts();
				}

				$this -> _appendLog("");

				$errors = $this -> specGrossiste -> getErrors();

				if (!empty($errors)) {
					$this -> _appendLog(count($errors) . " ERREURS RENCOTNREES SUR LES PRODUITS : ");
					$i = 1;
					foreach ($errors as $error) {
						$this -> _appendLog("ERR. " . $i . " : " . $error);
						$i++;
					}
				}

				///////////////////////////////////////////////////////////////////// LOG ///////////////////////////////////////////////////////////////////////////////////
				$this -> _appendLog($dealer['GROSSISTE_NOM'] . " : ");
				$this -> _appendLog($this -> getNbInsert() . " PRODUCTS INSERT : " . $this -> getNbUpdate() . " UPDATE " . $this -> getNbErrors() . " ERRORS " . $this -> getcsv_to_parse());
				$this -> _appendLog("");

				if ($this -> getNbInactiveMissing() != 0)
					$this -> _appendLog($this -> getNbInactiveMissing() . " PRODUITS ABSENTS PASSES INACTIFS");
				$this -> _appendLog("");

				if ($this -> getNbModifSellingStock() != 0)
					$this -> _appendLog($this -> getNbModifSellingStock() . " RETIRES DE VENTE CAR STOCK TROP BAS");

				if ($this -> getNbModifSellingCoef() != 0)
					$this -> _appendLog($this -> getNbModifSellingCoef() . " RETIRES DE VENTE CAR COEF HORS LIMITE ( < A " . $this -> coefmin . " ou > A " . $this -> coefmax . ") ");

				if ($this -> getNbModifSellingMissing() != 0)
					$this -> _appendLog($this -> getNbModifSellingMissing() . " RETIRES DE VENTE CAR ABSENT");

				if ($this -> nbAlreadySold != 0)
					$this -> _appendLog($this -> nbAlreadySold . " DEJA VENDUS PAR ECOMMERCANTS ");

				$this -> _appendLog("---------------------------------------------------------------------------------------------");
				$this -> _appendLog("---------------------------------------------------------------------------------------------");
			}

			self::$db -> setUpdateGrossiste($this -> grossisteID);

			$this -> nbInsertTot += $this -> nbInsert;
			$this -> nbUpdateTot += $this -> nbUpdate;
			$this -> nbErrorsTot += $this -> nbErrors;
			$this -> nbInactiveTot += $this -> nbInactive;
			$this -> nbInactiveMissingTot += $this -> nbInactiveMissing;
			$this -> nbModifSellingStockTot += $this -> nbModifSellingStock;
			$this -> nbModifSellingCoefTot += $this -> nbModifSellingCoef;
			$this -> nbModifSellingMissingTot += $this -> nbModifSellingMissing;
			$this -> nbAlreadySoldTot += $this -> nbAlreadySold;

			$this -> nbInsert = 0;
			$this -> nbUpdate = 0;
			$this -> nbErrors = 0;
			$this -> nbInactive = 0;
			$this -> nbInactiveMissing = 0;
			$this -> nbModifSellingStock = 0;
			$this -> nbModifSellingCoef = 0;
			$this -> nbModifSellingMissing = 0;
			$this -> nbAlreadySold = 0;
		}

		$temps_fin = microtime(true);
		$temps_exec = number_format($temps_fin - $temps_debut, 3);

		// LOG & MAIL RECAPITULATIF
		if ($this -> sendMailRecap($temps_exec))
			$this -> _appendLog('MAIL BIEN ENVOYE !');
		else
			$this -> _appendLog('ECHEC DE L ENVOI !');

		$this -> showLogRecap();

		$this -> _appendLog("---------------------------------------------------------------------------------------------");

		$this -> _appendLog('Temps d\'execution : ' . $temps_exec);
	}

	/**
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param string 	$file 			Le fichier CSV
	 */
	protected function parseCSV($file) {

		$row = 0;
		if (($handle = fopen($file, "r")) !== FALSE) {
			$headerList = array();
			$product = array();

			while (($data = fgetcsv($handle, 0, $this -> delimiter)) !== FALSE) {

				$num = count($data);

				//pour chaque colonnes
				for ($c = 0; $c < $num; $c++) {

					if ($row == 0) {
						$headerList[] = BusinessEncoding::toUTF8($data[$c]);
					} else {
						$product[] = BusinessEncoding::toUTF8($data[$c]);
					}
				}

				//Si on est pas sur l'entete
				if ($row != 0) {
					//Création du produit avec ses colonnes associatives
					$product = array_combine($headerList, $product);

					//Si aucune erreur dans le fichier CSV
					if (!$this -> specGrossiste -> errorsExist($product)) {

						//Log
						$message = "";

						$product['EAN'] ? $product['ean'] = $product['EAN'] : $product['ean'] = $product['ean'];

						$product = $this -> specGrossiste -> cleanDescription($product);

						//On récupère l'ancien produit s'il existe déja
						$prodGrOld = self::$db -> getProductByREF("", $product['ref']);
						$prodGrOld = $prodGrOld[0];

						//Format array field pour insertion generique en bdd
						$product = $this -> specGrossiste -> formatFields($product, $prodGrOld);

						//On garde les différents ean dans un tableau pour comparaison
						$this -> refList[] = $product['ref'];
						$product['PRICE_PRODUCT'] = $product['price'];
						$product['VOLUMETRIC_WEIGHT'] = $product['weight_volum'];
						$product['WEIGHT'] = $product['weight'];

						//GESTION DES POIDS PAR DEFAUT POUR LDLC grace à la table sfk_cat_poids
						if ($this -> grossisteID == 2 && ($product['weight'] == "0" | $product['weight'] == "NULL" | $product['weight'] == null)) {
							$key = $this -> recursive_array_search($prodGrOld['CATEGORY'], $this -> listCat, true);
							if ($key !== false) {
								if ($this -> listCat[$key]['POIDS_DEFAULT']) {
									$product['WEIGHT'] = $this -> listCat[$key]['POIDS_DEFAULT'];
									$product['weight'] = $this -> listCat[$key]['POIDS_DEFAULT'];
								}
							}
						}

						//Valeurs calculées
						$calcprix = self::$regleCalcul -> calculPrixGrossiste($product, null, $this -> grossisteID);

						//Insertion produit
						$state = self::$db -> addRowSfkProdGR($product, $this -> grossisteID);

						if ($state == 1) {//Si la requete et bien passée
							$message = $this -> grossisteNom . " - line " . $row . " - Prod Ref : " . $product['ref'] . " NOUVEAU AJOUTE - ";
							$this -> nbInsert++;
						} elseif ($state == 2) {
							$message = $this -> grossisteNom . " - line " . $row . " - Prod Ref : " . $product['ref'] . " DEJA PRESENT M.A.J. -";
							$this -> nbUpdate++;
						} elseif ($state == 0) {
							$message = $this -> grossisteNom . " - line " . $row . " - Prod Ref : " . $product['ref'] . " ERREUR SQL -";
							$this -> nbErrors++;
						}

						//On est obligé de récupérer une nouvelle fois le produit à partir de sa référence car
						//Si c'est un nouveau produit c'est seulement maintenant qu'il a un ID_PRODUCT
						if (!isset($prodGrOld) | $prodGrOld == null) {
							$prodGrOld = self::$db -> getProductByREF($dealerID, $product['ref']);
							$prodGrOld = $prodGrOld[0];
							$id_curr_prod = $prodGrOld['ID_PRODUCT'];
						} else {
							$id_curr_prod = $prodGrOld['ID_PRODUCT'];
						}

						//Insert prix produit
						if ($product['weight'] != "0" && $product['weight'] != "") {
							$calcprix['ID_PRODUCT'] = $id_curr_prod;
							self::$db -> addRowSfkPrixGrossiste($calcprix);
							$calcprix['QUANTITY'] = $product['stock'];
						}

						//Si produit associé
						if ($sku_av = self::$db -> getProdAvAssocGr($this -> grossisteID, $id_curr_prod)) {

							//MAJ de la fiche sfk_catalog_product
							if ($this -> grossisteID === "1") {
								self::$db -> updateWeightSfkCp($sku_av['ID_PRODUCT'], $calcprix['POIDS']);
							}

							//Si produit associé avahis vendu
							if (self::$db -> getStockPriceEcom($sku_av['EAN'], 24)) {

								$message .= " EN VENTE MISE A JOUR - ";
								//Si le stock est inférieur à 5
								if ((int)$product['stock'] < (int)$this -> stockmin) {

									$calcprix['QUANTITY'] = 0;
									//Produit désactivé de la vente

									$this -> nbModifSellingStock++;
									$message .= " MODIF VENTE STOCK INDISPONIBLE < " . $this -> stockmin . " -";
								}

								//Coeff de marge trop bas ou trop haut
								if ($calcprix['COEF'] < $this -> coefmin | $calcprix['COEF'] > $this -> coefmax) {

									$calcprix['QUANTITY'] = 0;
									//Produit désactivé de la vente

									$this -> nbModifSellingCoef++;
									$message .= " MODIF VENTE COEF HORS LIMITE  -";
								}

								//POIDS trop bas ou trop haut
								if ($calcprix['POIDS'] < $this -> weightmin | $calcprix['POIDS'] > $this -> weightmax) {

									$calcprix['QUANTITY'] = 0;
									//Produit désactivé de la vente
									$message .= " MODIF VENTE POIDS HORS LIMITE  -";
								}

								//MAJ de la ligne vente du produit pour stock à 0
								$log = self::$db -> addRowStockPrixProdVenteAv($calcprix);

								if ($log == 4) {
									$this -> nbAlreadySold++;
								}
							}
						}

						//GESTION AJOUT AUTO PRODUITS LDLC
						if ($this -> autoAssoc == "Y") {

							$key = $this -> recursive_array_search($prodGrOld['CATEGORY'], $this -> listCat, true);

							if ($this -> grossisteID == 2 && $key !== false) {

								if ($prodGrOld['WEIGHT'] == "0" | $prodGrOld['WEIGHT'] == "NULL" | $prodGrOld['WEIGHT'] == null) {
									if ($this -> listCat[$key]['POIDS_DEFAULT'])
										$prodGrOld['WEIGHT'] = $this -> listCat[$key]['POIDS_DEFAULT'];
								}

								if ($this -> listCat[$key]['AUTO_ASSOC']) {

									if (!self::$db -> getProdBySku($prodGrOld['EAN'])) {

										if ($catAv = self::$db -> getIdCatAssocGrossiste($prodGrOld['CATEGORY'], $this -> grossisteID)) {
											$id_avahis = self::$db -> addRowCatProduct($prodGrOld, $catAv);
											$this -> _appendLog('Produit avahis id ' . $id_avahis . ' crée !');
										}
									}
								}
							}
						}
						//$this -> _appendLog($this -> grossisteNom. " - line ".$row." - PRODUIT NON VALIDE ");
					}
				}
				unset($product);
				if ($row % 1000 == 0)
					$this -> _appendLog($this -> grossisteNom . " - " . $row . " - LIGNES PARSEES ");
				$row++;
			}
			fclose($handle);
			return true;
		}
		//MISSING FILE
		else {
			$this -> _appendLog(" !!!! -- ATTENTION -- !!!! " . $this -> grossisteNom . " - FICHIER MANQUANT ! ");
			$this -> sendMailAlertMissingFile();
			return false;
		}
	}

	/**
	 * Version XML du paring de produits
	 */
	public function parseXML($file) {
		libxml_use_internal_errors(true);
		if ($xml = simplexml_load_file($file)) {

			$this -> _appendLog('Fichier XML ' . $file . ' en cours de parsing... !');

			foreach ($xml as $item) {
				$row = 0;
				foreach ($item ->item as $product) {
					//Si aucune erreur dans le fichier CSV
					if (!$this -> specGrossiste -> errorsExist($product)) {

						//Log
						$message = "";

						//On récupère l'ancien produit s'il existe déja
						$prodGrOld = self::$db -> getProductByREF("", $product -> reference);
						$prodGrOld = $prodGrOld[0];

						//Format array field pour insertion generique en bdd
						$product = $this -> specGrossiste -> formatFields($product, $prodGrOld);

						//On garde les différents ean dans un tableau pour comparaison

						$this -> refList[] = $product['ref'];

						//Valeurs calculées
						$calcprix = self::$regleCalcul -> calculPrixGrossiste($product);

						//Insertion produit
						$state = self::$db -> addRowSfkProdGR($product, $this -> grossisteID);

						if ($state == 1) {//Si la requete et bien passée
							$message = $this -> grossisteNom . " - line " . $row . " - Prod Ref : " . $product['ref'] . " NOUVEAU AJOUTE - ";
							$this -> nbInsert++;
						} elseif ($state == 2) {
							$message = $this -> grossisteNom . " - line " . $row . " - Prod Ref : " . $product['ref'] . " DEJA PRESENT M.A.J. - ";
							$this -> nbUpdate++;
						} elseif ($state == 0) {
							$message = $this -> grossisteNom . " - line " . $row . " - Prod Ref : " . $product['ref'] . " ERREUR SQL - ";
							$this -> nbErrors++;
						}

						//On est obligé de récupérer une nouvelle fois le produit à partir de sa référence car
						//Si c'est un nouveau produit c'est seulement maintenant qu'il a un ID_PRODUCT
						if (!isset($prodGrOld) | $prodGrOld == null) {
							$prodGrOld = self::$db -> getProductByREF($dealerID, $product['ref']);
							$prodGrOld = $prodGrOld[0];
							$id_curr_prod = $prodGrOld['ID_PRODUCT'];
						} else {
							$id_curr_prod = $prodGrOld['ID_PRODUCT'];
						}

						//Insert prix produit
						if ($product['weight'] != "0" && $product['weight'] != "") {
							$calcprix['ID_PRODUCT'] = $id_curr_prod;
							self::$db -> addRowSfkPrixGrossiste($calcprix);
							$calcprix['QUANTITY'] = $product['stock'];
						}

						//Si produit associé
						if ($sku_av = self::$db -> getProdAvAssocGr($this -> grossisteID, $id_curr_prod)) {

							//Si produit associé avahis vendu
							if (self::$db -> getStockPriceEcom($sku_av['EAN'], 24)) {

								//$message .= " EN VENTE MISE A JOUR - ";

								//Si le stock est inférieur à 5 (ou autre valeur entré dans param systeme)
								if ((int)$product['stock'] < (int)$this -> stockmin) {

									$calcprix['QUANTITY'] = 0;
									//Produit désactivé de la vente

									$this -> nbModifSellingStock++;
									$message .= " MODIF VENTE STOCK INDISPONIBLE < " . $this -> stockmin . " -";
								}

								//Coeff de marge trop bas ou trop haut
								if ($calcprix['COEF'] < $this -> coefmin | $calcprix['COEF'] > $this -> coefmax) {

									$calcprix['QUANTITY'] = 0;
									//Produit désactivé de la vente

									$this -> nbModifSellingCoef++;
									$message .= " MODIF VENTE COEF HORS LIMITE  -";
								}

								//POIDS incorrect
								if ($calcprix['POIDS'] < $this -> weightmin | $calcprix['POIDS'] > $this -> weightmax) {

									$calcprix['QUANTITY'] = 0;
									//Produit désactivé de la vente
									$message .= " MODIF VENTE POIDS HORS LIMITE  -";
								}

								//MAJ de la ligne vente du produit pour stock à 0
								$log = self::$db -> addRowStockPrixProdVenteAv($calcprix);

								if ($log == 4) {
									$this -> nbAlreadySold++;
								}
							}
						}
					} else {
						//$this -> _appendLog($this -> grossisteNom. " - line ".$row." - PRODUIT NON VALIDE ");
					}
					if ($row % 1000 == 0)
						$this -> _appendLog($this -> grossisteNom . " - " . $row . " - LIGNES PARSEES ");
					$row++;
				}
			}
			return true;
		} else {
			$xml = simplexml_load_file($file);
			$errors = libxml_get_errors();

			foreach ($errors as $error) {
				echo $this -> display_xml_error($error, $xml);
			}

			libxml_clear_errors();
			$this -> _appendLog('ERREUR : Le fichier XML ' . $file . ' n\'a pas été chargé !');

			return false;
		}
	}

	protected function display_xml_error($error, $xml) {
		$return = $xml[$error -> line - 1] . "\n";
		$return .= str_repeat('-', $error -> column) . "^\n";

		switch ($error->level) {
			case LIBXML_ERR_WARNING :
				$return .= "Warning $error->code: ";
				break;
			case LIBXML_ERR_ERROR :
				$return .= "Error $error->code: ";
				break;
			case LIBXML_ERR_FATAL :
				$return .= "Fatal Error $error->code: ";
				break;
		}

		$return .= trim($error -> message) . "\n  Line: $error->line" . "\n  Column: $error->column";

		if ($error -> file) {
			$return .= "\n  File: $error->file";
		}

		return "$return\n\n--------------------------------------------\n\n";
	}

	/**
	 * Compare les produits existant en base de donnée grossiste avec les produits présents
	 * dans le CSV afin d'avoir un différentiel par rapport aux réferences produit manquantes
	 * du CSV venant d'être parsé.
	 */
	public function checkMissingProducts() {
		////////////////////// PRODUIT N'EST PLUS VENDU PAR LE GROSSISTE /////////////////////
		$listProdOld = self::$db -> getAllProdByVendor($this -> grossisteID);

		foreach ($listProdOld as $prodOld) {

			if (!in_array($prodOld['REF_GROSSISTE'], $this -> refList)) {
				//Produit passé en inactif
				self::$db -> setProductInactive($prodOld['ID_PRODUCT']);
				$this -> nbInactiveMissing++;

				//Si ce produit était en vente
				$sku_av = self::$db -> getProdAvAssocGr($this -> grossisteID, $prodOld['ID_PRODUCT']);
				if (self::$db -> getStockPriceEcom($sku_av['EAN'], 24)) {

					$this -> nbModifSellingMissing++;

					//On récupère les infos de ce produit (les prix surtout)
					$prodGrOld = self::$db -> getPrixCalcul($prodOld['ID_PRODUCT']);
					$prodGrOld = $prodGrOld[0];

					//On retire de la vente en passant le stock à 0
					$prodGrOld['QUANTITY'] = 0;
					self::$db -> addRowStockPrixProdVenteAv($prodGrOld);
				}
			}
		}
	}

	/**
	 * Fonction permetant d'envoyer le mail récapitulatif des imports des grossistes
	 * @param  float 	$temps_exec 	Le temps total d'éxecution
	 * @return boolean 	true si mail bien envoyé, sinon false
	 */
	public function sendMailRecap($temps_exec) {
		global $applicationMode;

		// Le transporteur
		$transport = Swift_MailTransport::newInstance();

		//Le mail
		$message = Swift_Message::newInstance();
		$message -> setSubject('Récapitulatif import quotidien produits grossistes');

		if ($applicationMode == "DEV") {
			$message -> setFrom('philippe.lattention@silicon-village.fr');
			$message -> setTo(array('philippe.lattention@silicon-village.fr'));
		} else {
			$message -> setFrom(SystemParams::getParam("system*email_from"));
			$message -> setTo(array('philippe.lattention@silicon-village.fr', 'thomas@silicon-village.fr'));
		}

		$message -> setBody('<h2>Récapitulatif</h2>
  							 <p>Produits insérés : <b>' . $this -> nbInsertTot . '</b><br/>
  							 Produits mis à jour : <b>' . $this -> nbUpdateTot . '</b><br/>
  							 Erreurs : <b>' . $this -> nbErrorsTot . '</b></p>
  							 <p>Produits absents passés inactifs : <b>' . $this -> nbInactiveMissingTot . '</b></p>
  							 <p>Produits non vendus car stock trop bas : <b>' . $this -> nbModifSellingStockTot . '</b></p>
  							 <p>Produits non vendus car COEF hors limite : ( < A ' . $this -> coefmin . ' ou > A ' . $this -> coefmax . ') <b>' . $this -> nbModifSellingCoefTot . '</b></p>
  							 <p>Produits non vendus car absents : <b>' . $this -> nbModifSellingMissingTot . '</b></p>
  							 <p>Produits déjà vendus par e-commerçants : <b>' . $this -> nbAlreadySoldTot . '</b></p>
  							 Temps d\'execution : ' . $temps_exec, 'text/html');

		$mailer = Swift_Mailer::newInstance($transport);

		if ($mailer -> send($message))
			return true;
		else
			return false;
	}

	public function sendMailAlertMissingFile() {
		global $applicationMode;

		// Le transporteur
		$transport = Swift_MailTransport::newInstance();

		//Le mail
		$message = Swift_Message::newInstance();
		$message -> setSubject('ATTENTION ! Fichier ' . $this -> grossisteNom . ' manquant !');

		if ($applicationMode == "DEV") {
			$message -> setFrom('philippe.lattention@silicon-village.fr');
			$message -> setTo(array('philippe.lattention@silicon-village.fr'));
		} else {
			$message -> setFrom(SystemParams::getParam("system*email_from"));
			$message -> setTo(array('philippe.lattention@silicon-village.fr', 'thomas@silicon-village.fr'));
		}

		$message -> setBody('<strong>ATTENTION le fichier de ' . $this -> grossisteNom . ' n a pas pu être parsé !!</strong>', 'text/html');

		$mailer = Swift_Mailer::newInstance($transport);

		if ($mailer -> send($message))
			return true;
		else
			return false;
	}

	/**
	 * Ecris le Log récapitulatif des insertions update et autres erreurs pour tous les grossiste au total
	 */
	public function showLogRecap() {

		$this -> _appendLog("RECAPITULATIF");
		$this -> _appendLog($this -> nbInsertTot . " PRODUCTS INSERT : " . $this -> nbUpdateTot . " UPDATE " . $this -> nbErrorsTot . " ERRORS ");
		$this -> _appendLog("");

		if ($this -> nbInactiveMissingTot != 0)
			$this -> _appendLog($this -> nbInactiveMissingTot . " PRODUITS ABSENTS PASSES INACTIFS");
		$this -> _appendLog("");

		if ($this -> nbModifSellingStockTot != 0)
			$this -> _appendLog($this -> nbModifSellingStockTot . " RETIRES DE VENTE CAR STOCK < " . $this -> stockmin);

		if ($this -> nbModifSellingCoefTot != 0)
			$this -> _appendLog($this -> nbModifSellingCoefTot . " RETIRES DE VENTE CAR COEF HORS LIMITE ( < A " . $this -> coefmin . " ou > A " . $this -> coefmax . ") ");

		if ($this -> nbModifSellingMissingTot != 0)
			$this -> _appendLog($this -> nbModifSellingMissingTot . " RETIRES DE VENTE CAR ABSENT");

		if ($this -> nbAlreadySoldTot != 0)
			$this -> _appendLog($this -> nbAlreadySoldTot . " DEJA VENDUS PAR ECOMMERCANTS ");
	}

	/**
	 * Pour le log, retourne le nombre d'insert effectués pour les
	 * produits
	 * @return integer 	$nbInsert  nb de Produits insert/update
	 */
	private function getNbInsert() {
		return $this -> nbInsert;
	}

	/**
	 * Pour le log, retourne le nombre d'insert effectués pour les
	 * relations attributs_produits
	 * @return integer 	$nbInsertAttr nb de relations Prod/attributs
	 */
	private function getNbInsertAttr() {
		return $this -> nbInsertAttr;
	}

	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * produits
	 * @return integer 	$nbUpdate nb de Produits update
	 */
	private function getNbUpdate() {
		return $this -> nbUpdate;
	}

	/**
	 * Pour le log, retourne le nombre de produits manquants du CSV
	 * @return integer nbInactiveMissing nb de Produits manquants
	 */
	private function getNbInactiveMissing() {
		return $this -> nbInactiveMissing;
	}

	/**
	 * Pour le log, retourne le nombre d'erreurs d'insertions de données
	 * rencontrées
	 * @return integer $nbErrors nb d'erreurs
	 */
	private function getNbErrors() {
		return $this -> nbErrors;
	}

	/**
	 * Pour le log, retourne le nombre d'update effectués pour les
	 * relations attributs_produits
	 * @return integer $nbUpdateAttr nb d'update relations Prod/attributs
	 */
	private function getNbUpdateAttr() {
		return $this -> nbUpdateAttr;
	}

	/**
	 * Pour le log, retourne le nombre de produits plus en vente
	 * à cause d'un stock incorrect
	 * @return integer $nbModifSellingStock
	 */
	private function getNbModifSellingStock() {
		return $this -> nbModifSellingStock;
	}

	/**
	 * Pour le log, retourne le nombre de produits plus en vente
	 * à cause d'un coef incorrect
	 * @return integer $nbModifSellingCoef
	 */
	private function getNbModifSellingCoef() {
		return $this -> nbModifSellingCoef;
	}

	/**
	 * Pour le log, retourne le nombre de produits plus en vente
	 * car abent du CV
	 * @return integer $nbModifSellingMissing
	 */
	private function getNbModifSellingMissing() {
		return $this -> nbModifSellingMissing;
	}

	/**
	 * @return string $csv_to_parse  l'url de l'e-commerçant
	 */
	private function getCsv_to_parse() {
		return $this -> csv_to_parse;
	}

	/**
	 * @return integer $grossisteID l'id du vendeur
	 */
	private function getGrossisteID() {
		return $this -> grossisteID;
	}

	/**
	 * @return nbRun le nombre de fois que ce Script a été effectué
	 */
	private static function getNbRun() {
		return self::$nbRun;
	}

	private function getVarDump() {
		return var_dump($this -> productListAttribute);
	}

	private function getErrors() {
		return $this -> errors;
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

	protected function setGrossisteID($grossisteID) {
		$this -> grossisteID = $grossisteID;
	}

	protected function setDelimiter($delimiter) {
		$this -> delimiter = $delimiter;
	}

	protected function setTech($tech) {
		$this -> tech = $tech;
	}

	protected function recursive_array_search($needle, $haystack) {
		foreach ($haystack as $key => $value) {
			$current_key = $key;
			if ($needle === $value OR (is_array($value) && $this -> recursive_array_search($needle, $value) !== false)) {
				return $current_key;
			}
		}
		return false;
	}

}
?>