<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchCalculPrixGrossiste extends Batch{

	static protected 	$_description 	= "[LOCAL] -- Calcul des prix grossistes";
	private 			$_name 			= "BatchCalculPrixGrossiste";
	
	private static 		$db;
	private static      $regleCalcul;

	protected $grilleChronopost = array(); 
	protected static 	$nbProd = 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide
	
	protected $coefmin ;
	protected $coefmax ;
	protected $weightmin ;
	protected $weightmax ;
	protected $stockmin ;
	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		$this -> coefmin 	= SystemParams::getParam('grossiste*coef');
		$this -> coefmax 	= SystemParams::getParam('grossiste*coefmax');
		$this -> weightmin 	= SystemParams::getParam('grossiste*weightmin');
		$this -> weightmax 	= SystemParams::getParam('grossiste*weightmax');
		$this -> stockmin 	= SystemParams::getParam('grossiste*stock');		
		
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux
		self::$db          = new BusinessSoukeoGrossisteModel();
		//self::$regleCalcul = new BusinessCalcul();
        self::$regleCalcul = new BusinessCalculNEW();
		
		$this -> grilleChronopost = self::$db -> getGrilleChronopost();
		
		self::$db -> prepareInsertSfkPrixGrossiste();
		////////////////////////// CONTENU // PRODUITS////////////////////////////////////////
		$productList = self::$db->getAllActiveProdGrossisteToCalcul();
				
		foreach ($productList as $product) {
			
			$message="";
			
			$product = self::$regleCalcul -> calculPrixGrossiste($product, null, $product['GROSSISTE_ID']);
			$product['QUANTITY'] = self::$db->getQuantityProdGr($product['ID_PRODUCT'])	;
							
			//Nouveau Prix
			if($product['POIDS'] != "0"){
				self::$db -> addRowSfkPrixGrossiste($product);
				$message = self::$nbProd . " - Prod ID : ". $product['ID_PRODUCT'] . " - CALCUL PRIX MIS A JOUR ";
				
				//MAJ SI PRODUIT EN VENTE
				if($sku_av = self::$db -> getProdAvahisAssocieGrossiste($product['ID_PRODUCT'])) {
					if(self::$db -> getStockPriceEcom($sku_av['EAN'], 24)){
						//Condition de vente
						if(   (float)$product['QUANTITY'] < (float)$this -> stockmin 
							| (float)$product['POIDS'] > (float)$this -> weightmax 
							| (float)$product['POIDS'] < (float)$this -> weightmin
							| (float)$product['COEF']  < (float)$this -> coefmin 
							| (float)$product['COEF']  > (float)$this -> coefmax) {
							
								
							$product['QUANTITY'] = 0;
						} 
							
						self::$db -> addRowStockPrixProdVenteAv ($product);
						$message.= "- STOCK PRIX VENTE MIS A JOUR !";
					}	
				}
			}

			if($message)$this -> _appendLog($message);
			
		 	self::$nbProd ++;
		}
		$this -> _appendLog('---------------------------------------------------------------');
		$this -> _appendLog(('Nombre de produits mis à jours : ' . self::$nbProd ) );

		self::$nbProd = 0 ;
		self::$nbErreurs = 0;

		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	
	
	/*private function getValeurCalcul($product)
	{
		if($calcul_values = self::$db -> getPrixCalcul($product['ID_PRODUCT'])){
			$calcul_values = $calcul_values[0];
			///////////////////////////////////////////////////////////////////////////////////////////////
			//RECUP ANCIENS PARAMETRES/////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////
			
			
			$aerien = (float)SystemParams::getParam('grossiste*aerienchrono') * $calcul_values['POIDS'];
			
			//On retrouve alors le taux TVA param entré
			if(!is_null($calcul_values['TVA_PROD'])){
				$paramTauxTVA = $calcul_values['TVA_PROD'];
			}
			 
			//Retrouver le 20 % ou les changements
			if(!is_null($calcul_values['MARGE_PROD'])){
				$percentMarge = $calcul_values['MARGE_PROD'];
			}
			

			if(!is_null($calcul_values['OM_PROD'])){
				$percentOctroi = $calcul_values['OM_PROD'];
			}

		}
		

		///////////////////////////////////////////////////////////////////////////////////////////////
		//RECALCUL AVEC LE NOUVEAU PRIX ///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////
		$prixAchatHT = (float)$product['PRICE_PRODUCT'];
		
		//pas de hauteur largeur longueur
		$product['VOLUMETRIC_WEIGHT'] && $product['VOLUMETRIC_WEIGHT'] != "0" ? $poidsVolum = $product['VOLUMETRIC_WEIGHT'] : $poidsVolum = $product['WEIGHT'];
		
		$prixKilo = $prixAchatHT / $poidsVolum;
		
		if((float)$prixKilo > 50){
			$frais_livr = 0.10 * $prixAchatHT;
		}else{
			$frais_livr = 0;
		}
		
		if(!isset($percentOctroi))$percentOctroi = "";
		
		//On va chercher le produit Avahis correspondant pour Octroi de mer 
		if($prod_av = self::$db -> getProdAvAssocGr($product['GROSSISTE_ID'], $product['ID_PRODUCT'])){
		
			//On vérifie si sa catégorie a un octroi de mer
			$infoCat = self::$db -> getCatbyId($prod_av['CATEGORY']);
			

			if(!is_null($infoCat['OM'])  ){
				$percentOctroi != null ? $percentOctroi = $percentOctroi : $percentOctroi = $infoCat['OM'];
				
			}else{
				$percentOctroi != null ? $percentOctroi = $percentOctroi : $percentOctroi = (float)SystemParams::getParam('grossiste*percentOM');
			}
			

			if(!is_null($infoCat['MARGE'])  ){
				$percentMarge != null ? $percentMarge = $percentMarge : $percentMarge = $infoCat['MARGE'];
				
			}else{
				$percentMarge != null ? $percentMarge = $percentMarge : $percentMarge = (float)SystemParams::getParam('grossiste*percentMarge');
			}
			
			if(!is_null($infoCat['TVA'])  ){
				$paramTauxTVA != null ? $paramTauxTVA = $paramTauxTVA : $paramTauxTVA = $infoCat['TVA'];
				
			}else{
				$paramTauxTVA != null ? $paramTauxTVA = $paramTauxTVA : $paramTauxTVA = (float)SystemParams::getParam('grossiste*percentTVA');
			}
		}
		
		$aerien = (float)SystemParams::getParam('grossiste*aerienchrono') * (float)$poidsVolum;
		
		
		if($percentOctroi == null) $percentOctroi = (float)SystemParams::getParam('grossiste*percentOM');
		if($percentMarge  == null) $percentMarge  = (float)SystemParams::getParam('grossiste*percentMarge');
		if($paramTauxTVA  == null) $paramTauxTVA  = (float)SystemParams::getParam('grossiste*percentTVA');
		
		
		$octroiMer = $percentOctroi * ($prixAchatHT + $aerien);

					 
		$prixRevient = $prixAchatHT 
					 + $aerien
					 + $frais_livr
					 + $octroiMer;
		
		$percentMarge != null ? $percentMarge = $percentMarge : $percentMarge = (float)SystemParams::getParam('grossiste*percentMarge');			 
		$margeNette = $prixRevient * $percentMarge ; // 25% marge nette à paramétrer, ici valeur par défaut ou ancienne val
		
		$paramTauxTVA != null ? $paramTauxTVA = $paramTauxTVA : $paramTauxTVA = (float)SystemParams::getParam('grossiste*percentTVA');
		$TVA = ( $prixRevient + $margeNette ) * $paramTauxTVA; //TVA 8,50 % à paramétrer plus tard, ici valeur par défaut ou ancienne val
		
		
		$prixVenteLivre = $prixRevient + $margeNette + $TVA ;
		
		$coeff = $prixVenteLivre / $prixAchatHT ;  
		
		$product['PRIX_ACHAT'] 		= $prixAchatHT;
		$product['POIDS'] 			= $poidsVolum;
		$product['PRIX_KILO'] 		= $prixKilo;
		$product['PRIX_METRO'] 		= $prixAchatHT + ($prixAchatHT * 0.196);
		$product['FRAIS_LIVR'] 		= $frais_livr;
		$product['AERIEN']			= $aerien;
		$product['OM'] 				= $octroiMer;
		$product['PRIX_LIVRE'] 		= $prixRevient;
		$product['PERCENT_MARGE'] 	= $percentMarge;
		$product['PERCENT_TVA'] 	= $paramTauxTVA;
		$product['PRIX_FINAL'] 		= $prixVenteLivre ;
		$product['COEF'] 			= $coeff ;
		$product['RATIO_RUN_FR'] 	= $prixVenteLivre / $product['PRIX_METRO'];
		$product['PRIX_LIVRAISON'] 	= $aerien + $frais_livr;
		$product['RATIO_TRANSPORT'] = $product['PRIX_LIVRAISON'] / $prixAchatHT;
		

		return $product;
	}*/
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>