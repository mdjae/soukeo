<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}


/**
 * Batch permettant l'export de la configuration des attributs
 * @author Philippe_LA
 */
class BatchExportConfigAttributes extends Batch{

	static protected 	$_description 	= "[EXPORT] Génération du CSV config des attributs";
	private 			$_name 			= "BatchExportConfigAttributes";
	
	private static 		$db;
	protected 			$vendor_id;
	protected 			$cat_id;
	protected 			$cat_name;
	
	protected 			$ArrCSV 	= array();
	
	protected static 	$nbProd 	= 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs 	= 0; //Nombre d'erreurs d'export produit car poids vide
	protected 			$total_attr = 0;
	
	protected           $specif_cat = "";
		
	private 			$rep 		= "/opt/soukeo/export/configs/";
	
	
	/**
	 * Routine du batch
	 * @param 	String 	$vendor_id 	 Optionnel contenant l'id du vendeur permettant alors de lancer ce batch
	 * pour un vendeur en particulier.
	 */
	public function work($specif_cat = "")
	{
        $temps_debut = microtime(true);
        $this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("\n\n");
        $this -> _appendLog("\n\n---------------------------------------------------------------------------------------------");
        
		$this -> specif_cat = $specif_cat;
        
        //Connection base SoukFlux
        self::$db = new BusinessSoukeoModel();
        
        if($this -> specif_cat != ""){
            $this -> _appendLog("CATEGORIE Specifique  N° : ". $this -> specif_cat);
        }
        else{
            $attributes_list = self::$db -> getAllActiveAttributes();
        }
		
        $this->ArrCSV[] = $this -> getHeader();
        
        foreach ($attributes_list as $attribut) {
                
            $this->ArrCSV[] = $this -> getLineAttribut($attribut);   
            $this->total_attr ++ ;
            
        }

        $this->generateCsv();


        $this -> _appendLog('---------------------------------------------------------------------------------------------');
		$this -> _appendLog('Nombre d attributs total '.$this->total_attr);
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));	
		
	}	
	
	
	/**
	 * Retourne un array contenant les labels du header du CSV 
	 * @return array 	contenant le header du CSV.
	 */
	protected function getHeader()
	{
		return array(	"attribute_code",
						"is_filterable",
						"frontend_input",
						"frontend_label" );
	}
	
	public function getLineAttribut($attribut)
	{
	    $attr = strtolower($attribut->getId_attribut()."_".$attribut->getCode_attr());
        $attr = stripAccents($attr);
        $attr = str_replace(' ', '_', $attr);
        $attr = preg_replace('/[^A-Za-z0-9 _]/', '', $attr);
        $arr = array ("av_".$attr,
                      $attribut->getIs_filterable(), 
                      "multiselect",
                      $attribut->getLabel_attr() );
        
        return $arr;
	}
    

	/**
	 * Fonction permettant de générer un csv grace à l'array contenant chaque ligne pour la catégorie
	 * Il y a création du fichier qui portera le nom de la catégorie et le nom du vendeur si ce batch est appellé
	 * dans le contexte d'un unique vendeur.
	 */
	protected function generateCsv()
	{
		global $root_path;
		
        if($this->cat_id != ""){
            $name = $this->rep."attributes-".$this->cat_id."-".date("d-m-Y")."_".date("H-i-s").".csv";    
        }
        else{
            $name = $this->rep."attributes-".date("d-m-Y")."_".date("H-i-s").".csv";    
        }
		
		//Create File
		file_put_contents($name , "");
		$fp = fopen($name, "w");

		//Genere CSV
		foreach ($this->ArrCSV as $fields) {
		    fputcsv($fp, $fields, ';', '"');
		}
		
		fclose($fp);
	}
	
    public function help()
    {
        $help =  PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL;
        $help .= "Aucun argument a entrer".PHP_EOL;
        $help .= PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL.PHP_EOL;
        
        return $help;
    }
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>