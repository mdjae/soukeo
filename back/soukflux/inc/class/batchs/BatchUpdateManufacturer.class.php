<?php 
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	require_once $main_path . "/" . $root_path . 'thirdparty/swiftmail/lib/swift_required.php';
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	require_once $root_path . "inc/offipse.inc.php";
	require_once $root_path . 'thirdparty/swiftmail/lib/swift_required.php';
}
/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchUpdateManufacturer extends Batch{

	static protected 	$_description 	= "[MAJ] [Manufacturer] Batch manufacturer";
	private 			$_name 			= "BatchUpdateManufacturer";
	
	private static 		$db;


	protected $grilleChronopost = array(); 
	protected static 	$nbProd = 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide

	protected $coefmin ;
	protected $coefmax ;
	protected $weightmin ;
	protected $weightmax ;
	protected $stockmin ;	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		
		//R.A.Z. des quantités des produits grossiste
		self::$db -> prepUpdateManufacturer();
		self::$db -> prepUpdateMetaKeyword();
		self::$db -> prepUpdateMetaDescrip();
		self::$db -> prepUpdateMetaTitle();
		
		$productList = self::$db->getAllProductsManufacturer();
		
				
		//Boucle sur liste produits grossistes		
		foreach ($productList as $product) {
			
			
			$manuf = $product['manufacturer'];
			$manuf = ucfirst(strtolower(str_replace("Ú", "e", $manuf)));
            $manuf = str_replace("®", "", $manuf);
			
			$metakeyword = $product['name'].", ".$manuf. ", ";
			
			$metadescrip = $product['name'];
			
			$name = str_replace(array("-", ",", ".", "?", "!"), "", $product['name']);
			$name = explode(" ", $name);
			
			$c = 0; 
			$metatitle = "" ;
			
			while($c < count($name) && $c < 5){
				$metatitle .= html_entity_decode($name[$c]) . " ";
				$c++;
			}
			
			$catDernierNiveau = self::$db -> getCatbyId($product['categories']);
			$catDernierNiveau = $catDernierNiveau['cat_label'];
			
			$metatitle .= "- " . $catDernierNiveau ;
			
			if($product['manufacturer'] != "" && $product['manufacturer'] !== null && $product['manufacturer'] != "NULL"){
				$metatitle .= " - " . $product['manufacturer'];
			}

			$metatitle .=  " - acheter La Réunion";
			
			
			
			if($product['short_description'] != "" && $product['short_description'] != "NULL" && $product['short_description'] !== null){
				$metadescrip .= ", " . $product['short_description'];
			}
			if($product['meta_title'] != "" && $product['meta_title'] != "NULL" && $product['meta_title'] !== null){
				$metadescrip .= ", " . $product['meta_title'];
			}
			
			$metadescrip = strip_tags($metadescrip);
			
			
			$cat = self::$db -> getCatIdTreeForMeta($product['categories']);

			
			$cat = explode(",", $cat);
			$cat = array_reverse($cat);
			foreach ($cat as $c) {
				
				if( $c != end($cat) ) {
					$metakeyword .= $c . ", ";
				}else {
					$metakeyword .= $c;
				} 
			}

			$metakeyword .= ", ".$product['sku'];	
			$this -> _appendLog($metatitle);
			self::$db -> updateManufacturer($product['id_produit'], $manuf);
			 
			self::$db -> updateMetaKeyword($product['id_produit'], $metakeyword.", ".stripAccents($metakeyword));
			self::$db -> updateMetaDescrip($product['id_produit'], $metadescrip);
			self::$db -> updateMetaTitle($product['id_produit'], $metatitle);
			
			self::$nbProd++;
		}

		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	
	


	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>