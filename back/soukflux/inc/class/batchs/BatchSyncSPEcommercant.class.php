<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

/**
 * Batch qui va parcourir tout les e-commercant de la base de donnée afin de connaitre leur technologie.
 * A partir de leur technologie le Batch pourra lancer le batch processus de synchronisaiton SP(stock prix) dédié.
 * @author Philippe_LA/FHKC
 */
class BatchSyncSPECommercant extends Batch {
	
	static protected 	$_description 	= "[EXT] -- Import des stock&prix Commercant";
	private 			$_name 			= "BatchSyncSPECommercant";
	private static 		$db;
	
	
	/**
	 * Fonction de routine du batch,
     * 
	 */
	function work($vendor_id=""){
	      
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");

    	//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
        $vendorManager = new ManagerVendor();
        $vendorCsvManager = new ManagerVendorCsv();
		
        //Param spécif pour 1 seul vendeur
        if($vendor_id != ""){
            
            if($vendor = $vendorManager -> getBy(array("vendor_id" => $vendor_id))){
                
                $this->_appendLog("Vendeur ID : ".$vendor_id." ".$vendor->getVendor_nom());
                
                if(  ($infoTracking = self::$db->getInfoTracking($vendor->getVendor_id()) )  &&  ($vendor->getActive())) {
                    
                    $this->_appendLog($infoTracking['SOFTWARE']." : ".$infoTracking['URL']);
                    $script = $this->getTechnoEcommercant($infoTracking);
                    
                    if ($script!="Attente"){
                        $script->run();
                    }
                    if ($script!="Attente")
                    $this -> _appendLog('Nombre de synchronisation Stock Prix : ' .$script->getNbSyncSP().' .');
                    $this -> _appendLog("---------------------------------------------------------------------------------------------");
                    
                    $this->_appendLog("---------------------------------------------------------------------------------------------");
                    
                    $temps_fin = microtime(true);
                    $this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
                }
                elseif( $vendor->getVendor_id() != null && ($infoCsv = $vendorCsvManager -> getAll(array("vendor_id" => $vendor->getVendor_id())) ) &&  ($vendor->getActive())){

                    self::$db -> razStockPrixCSV($vendor->getVendor_id());
                    
                    foreach ($infoCsv as $csv) {
                        
                        $this->_appendLog("CSV : ".$csv->getVendor_csv_name().".csv");
                        
                        $script = new BusinessSyncCSVSP($csv->getVendor_csv_name(), $csv->getVendor_id());
                        $script->run();
                        $this -> _appendLog('Nombre de synchronisation Stock Prix : ' .$script->getNbSyncSP().' .');
                        $this -> _appendLog("---------------------------------------------------------------------------------------------");
                    }                
                }
                else{
                    $this->_appendLog("Erreur, pas d'info de tracking sur ce vendeur ou bien il est inactif (ou CSV)!");
                }
            }

            else{
                $this->_appendLog("Erreur, ce vendeur ID n existe pas !");
                exit;
            }    
            
        }
        //Routine pour tous les commerçants
        else{
    		$vendorList = self::$db->getAllVendor();
    		
    		foreach ($vendorList as $vendor) {
    			
    			if(($infoTracking = self::$db->getInfoTracking($vendor['VENDOR_ID'])) && ($vendor['ACTIVE'])) { // si le tracking vendeur a été faii et que ce vendeur est actif
    				
    				//Choix techno
    				$this->_appendLog("VENDOR_ID : ".$vendor['VENDOR_ID']." : ".$infoTracking['SOFTWARE']." : ".$infoTracking['URL']);
    				$script = $this->getTechnoEcommercant($infoTracking);
    				if ($script!="Attente"){
    					$script->run();
    				}
    				if ($script!="Attente")
    				$this -> _appendLog('Nombre de synchronisation Stock Prix : ' .$script->getNbSyncSP().' .');
    				$this -> _appendLog("---------------------------------------------------------------------------------------------");
    				
    			}
                elseif( $vendor['VENDOR_ID'] != null && ($infoCsv = $vendorCsvManager -> getAll(array("vendor_id" => $vendor['VENDOR_ID'])) ) &&  ($vendor['ACTIVE'])){

                    //Si parmi tous les CSV vendeurs au moins 1 est auto alors on repasse sur tout
                    if($this -> atleastOneIsAuto($infoCsv)){
                        
                        self::$db -> razStockPrixCSV($vendor['VENDOR_ID']);
                        
                        foreach ($infoCsv as $csv) {
                            
                            $this->_appendLog("CSV : ".$csv->getVendor_csv_name().".csv");
                            
                            $script = new BusinessSyncCSVSP($csv->getVendor_csv_name(), $csv->getVendor_id());
                            $script->run();
                            $this -> _appendLog('Nombre de synchronisation Stock Prix : ' .$script->getNbSyncSP().' .');
                            $this -> _appendLog("---------------------------------------------------------------------------------------------");
                            
                        }
                    }
                }
    		}
    
    		$temps_fin = microtime(true);
            $this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
        }
	}

    protected function atleastOneIsAuto($infoCsv)
    {
        foreach ($infoCsv as $csv) {
            
            if($csv->getAuto()){
                return true;
            }
        }
        
        return false;
    }		
	
	/**
	 * Fonction permettant de retourner le script d'execution spécifique à la technologie de l'e-commerçant
	 * @param infoTracking Array contient l'idVendeur l'url et la techno de l'ecommercant
	 * @return object
	 */
	public function getTechnoEcommercant($infoTracking){
		switch ($infoTracking['SOFTWARE']) {
					
			case "Prestashop":
				
               	return new BusinessSyncPrestashopSP($infoTracking['URL'], $infoTracking['VENDOR_ID']);
                break;

			case "Thelia":
				return new BusinessSyncTheliaSP($infoTracking['URL'], $infoTracking['VENDOR_ID']);
				break;
				
			case "Oscommerce":
				$this->_appendLog('vendeur oscommerce a implanter'); //a implanter
                return 'Attente';
				break;
			
			case "Virtuemart":
				return new BusinessSyncVirtuemartSP($infoTracking['URL'], $infoTracking['VENDOR_ID']);
				break;
				
			case "Magento":
				$this->_appendLog('vendeur magento a implanter'); //a implanter
                return 'Attente';
				break;
			
			case 'CSV':
					return new BusinessSyncCSVSP($infoTracking['URL'], $infoTracking['VENDOR_ID']);
			break;
		}	
	}
	/**
 	 * Permet de récupérer la description du programme
 	 * @author tpo
 	 *
 	 */
	public static function getDescription() {
		return self::$_description;
	}
        
}

?>