<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA  
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */


class BatchPicturesPrestashop {
	
	//Variables pour la clase batch
	private $_name = "BatchPitcuresPrestashop";
	static protected $_description = "[TMP] [Prestashop] Modifie les urls pour ecom Prestashop avec mauvais url images";
	
	private static $db;


	public function work() {
		$temps_debut = microtime(true);
        
        /*
		$this -> cat_id = '';
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoPrestashopModel();
		
		
		////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////// TONER.RE /////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////
		$listeProd = self::$db -> getAllProductsPSByVendor(33);
		
		foreach ($listeProd as $prod) {
			$newUrl = "http://toner.re/img/p/".$prod['ID_PRODUCT'];
			$tmp = $prod['IMAGE_PRODUCT'];
			$tmp = explode('_', $tmp);
			$tmp = explode('/', $tmp[0]);
			$tmp = end($tmp);
			$newUrl = $newUrl . "-" . $tmp . ".jpg";
			echo $newUrl . "\n";
			
			self::$db -> updatePicProd($prod['ID_PRODUCT'], $prod['VENDOR_ID'], $newUrl);

		}
		
		
		////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////// KDOPAYS /////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////
		
		$listeProd = self::$db -> getAllProductsPSByVendor(29);
		
		foreach ($listeProd as $prod) {
			$newUrl = "http://www.kdopays.re/img/p/".$prod['ID_PRODUCT'];
			$tmp = $prod['IMAGE_PRODUCT'];
			$tmp = explode('_', $tmp);
			$tmp = explode('/', $tmp[0]);
			$tmp = end($tmp);
			$newUrl = $newUrl . "-" . $tmp . ".jpg";
			echo $newUrl . "\n";

			self::$db -> updatePicProd($prod['ID_PRODUCT'], $prod['VENDOR_ID'], $newUrl);
			
		}
		
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////// CKOMCA  /////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        
        $listeProd = self::$db -> getAllProductsPSByVendor(46);
        
        foreach ($listeProd as $prod) {
            $newUrl = "http://ckomca.com/img/p/".$prod['ID_PRODUCT'];
            $tmp = $prod['IMAGE_PRODUCT'];
            $tmp = explode('_', $tmp);
            $tmp = explode('/', $tmp[0]);
            $tmp = end($tmp);
            
            $tmp = explode('-', $tmp);
            $tmp = $tmp[0];
            
            $newUrl = $newUrl . "-" . $tmp . ".jpg";
            echo $newUrl . "\n";

            self::$db -> updatePicProd($prod['ID_PRODUCT'], $prod['VENDOR_ID'], $newUrl);
            
        }
        */
        		
		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

	}
	
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
