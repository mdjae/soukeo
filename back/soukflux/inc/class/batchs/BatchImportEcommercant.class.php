<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

/**
 * Batch qui va parcourir tout les e-commercant de la base de donnée afin de connaitre leur technologie.
 * A partir de leur technologie le Batch pourra lancer le batch d'import dédié.
 * @author Philippe_LA
 */
class BatchImportEcommercant extends Batch {
	static protected $_description = "[EXT] -- Import des produits Commercant + Stock et prix ";
	private static $db;
	private $_name = "BatchImportEcommercant";
	protected $totalInsert;
	protected $totalErrors;
	protected $totalUpdate;
	protected $recap_mail;

	/**
	 * Fonction de routine du batch
	 */
	public function work($vendor_id = "") {
		$temps_debut = microtime(true);

		$this -> _appendLog("Batch " . $this -> _name);
		$this -> _appendLog("");
		$this -> _appendLog("---------------------------------------------------------------------------------------------");
		$this -> _appendLog("---------------------------------------------------------------------------------------------");

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		$vendorManager = new ManagerVendor();

		//Param spécif pour 1 seul vendeur
		if ($vendor_id != "") {

			if ($vendor = $vendorManager -> getBy(array("vendor_id" => $vendor_id))) {

				$this -> uniqueVendorWork($vendor);
			} else {
				$this -> _appendLog("Erreur, ce vendeur ID n existe pas !");
				exit ;
			}
		}
		//Routine pour tous les commerçants
		else {
			$this -> allVendorWork();
			$this -> sendMailRecap();
		}
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

	}

	public function uniqueVendorWork(BusinessVendor $vendor) {
		$vendorCsvManager = new ManagerVendorCsv();

		$this -> _appendLog("Vendeur ID : " . $vendor -> getVendor_id() . " " . $vendor -> getVendor_nom());

		if (($infoTracking = self::$db -> getInfoTracking($vendor -> getVendor_id())) && ($vendor -> getActive())) {

			$this -> _appendLog($infoTracking['SOFTWARE'] . " : " . $infoTracking['URL']);
			$script = $this -> getTechnoEcommercant($infoTracking);
			$script -> run();
			$this -> _appendLog($script -> getNbInsert() . " PRODUCT(S) INSERT ");
			$this -> _appendLog($script -> getNbUpdate() . " PRODUCT(S) UPDATE ");
			$this -> _appendLog($script -> getNbErrors() . " ERROR(S) ");
			$this -> _appendLog("ATTRIBUTES INSERT : " . $script -> getNbInsertAttr());

			$this -> totalInsert += $script -> getNbInsert();
			$this -> totalUpdate += $script -> getNbUpdate();
			$this -> totalErrors += $script -> getNbErrors();

			$this -> _appendLog("---------------------------------------------------------------------------------------------");

		} elseif (($infoCsv = $vendorCsvManager -> getAll(array("vendor_id" => $vendor -> getVendor_id())))) {

			//Remise à Active = 0 des produits du vendeur (s'il a plusieurs CSV ce n'est pas grave l'opération arrive après)
			self::$db -> razActiveProdCSV($vendor -> getVendor_id());

			foreach ($infoCsv as $csv) {

				$this -> _appendLog("CSV : " . $csv -> getVendor_csv_name() . ".csv");

				$script = new BusinessImportCSV($csv -> getVendor_csv_name(), $csv -> getVendor_id());
				$script -> run();
				$this -> _appendLog($script -> getNbInsert() . " PRODUCT(S) INSERT ");
				$this -> _appendLog($script -> getNbUpdate() . " PRODUCT(S) UPDATE ");
				$this -> _appendLog($script -> getNbErrors() . " ERROR(S) ");
				$this -> _appendLog("ATTRIBUTES INSERT : " . $script -> getNbInsertAttr());

				$this -> totalInsert += $script -> getNbInsert();
				$this -> totalUpdate += $script -> getNbUpdate();
				$this -> totalErrors += $script -> getNbErrors();

				$this -> _appendLog("---------------------------------------------------------------------------------------------");

			}
		} else {
			$this -> _appendLog("Erreur, pas d'info de tracking ou csv sur ce vendeur ou bien il est inactif !");
		}
	}

	public function allVendorWork() {

		$vendorList = self::$db -> getAllVendor();
		$vendorCsvManager = new ManagerVendorCsv();

		foreach ($vendorList as $vendor) {
			// si le tracking vendeur a été faii et que ce vendeur est actif
			if (($infoTracking = self::$db -> getInfoTracking($vendor['VENDOR_ID'])) && ($vendor['ACTIVE'])) {
				//Choix techno
				$this -> _appendLog("VENDOR_ID : " . $vendor['VENDOR_ID'] . " : " . $infoTracking['SOFTWARE'] . " : " . $infoTracking['URL']);
				$script = $this -> getTechnoEcommercant($infoTracking);

				if ($script -> run()) {
					$this -> _appendLog($script -> getNbInsert() . " PRODUCT(S) INSERT ");
					$this -> _appendLog($script -> getNbUpdate() . " PRODUCT(S) UPDATE ");
					$this -> _appendLog($script -> getNbErrors() . " ERROR(S) ");
					$this -> _appendLog("ATTRIBUTES INSERT : " . $script -> getNbInsertAttr());

					$this -> totalInsert += $script -> getNbInsert();
					$this -> totalUpdate += $script -> getNbUpdate();
					$this -> totalErrors += $script -> getNbErrors();
				} else {
					$this -> _appendLog("LE SCRIPT N'A PAS PU ETRE LANCE CAR URL INACCESSIBLE !");
				}

				$this -> _appendLog("---------------------------------------------------------------------------------------------");
			} elseif ($vendor['VENDOR_ID'] != null && ($infoCsv = $vendorCsvManager -> getAll(array("vendor_id" => $vendor['VENDOR_ID']))) && ($vendor['ACTIVE'])) {

				//Si parmi tous les CSV vendeurs au moins 1 est auto alors on repasse sur tout
				if ($this -> atleastOneIsAuto($infoCsv)) {

					//Remise à Active = 0 des produits du vendeur (s'il a plusieurs CSV ce n'est pas grave l'opération arrive après)
					self::$db -> razActiveProdCSV($vendor['VENDOR_ID']);

					foreach ($infoCsv as $csv) {

						$this -> _appendLog("CSV : " . $csv -> getVendor_csv_name() . ".csv");

						$script = new BusinessImportCSV($csv -> getVendor_csv_name(), $csv -> getVendor_id());
						$script -> run();
						$this -> _appendLog($script -> getNbInsert() . " PRODUCT(S) INSERT ");
						$this -> _appendLog($script -> getNbUpdate() . " PRODUCT(S) UPDATE ");
						$this -> _appendLog($script -> getNbErrors() . " ERROR(S) ");
						$this -> _appendLog("ATTRIBUTES INSERT : " . $script -> getNbInsertAttr());

						$this -> totalInsert += $script -> getNbInsert();
						$this -> totalUpdate += $script -> getNbUpdate();
						$this -> totalErrors += $script -> getNbErrors();

						$this -> _appendLog("---------------------------------------------------------------------------------------------");

					}
				}
			}
		}

		$this -> _appendLog('NB Scripts Prestashops : ' . BusinessImportPrestashop::getNbRun());
		$this -> _appendLog('NB Scripts Thelia : ' . BusinessImportThelia::getNbRun());
		$this -> _appendLog('NB Scripts Virtuemart : ' . BusinessImportVirtuemart::getNbRun());
		$this -> _appendLog('NB Scripts CSV : ' . BusinessImportCSV::getNbRun());

	}

	protected function sendMailRecap() {

		global $applicationMode;
		// Le transporteur
		$transport = Swift_MailTransport::newInstance();

		//Le mail
		$message = Swift_Message::newInstance();
		$message -> setSubject('Récapitulatif Import ecommercants');

		if ($applicationMode == "DEV") {
			$message -> setFrom('philippe.lattention@silicon-village.fr');
			$message -> setTo(array('philippe.lattention@silicon-village.fr'));
		} else {
			$message -> setFrom(SystemParams::getParam("system*email_from"));
			$message -> setTo(array('philippe.lattention@silicon-village.fr', 'frederic.hewkianchong@silicon-village.fr', 'thomas@silicon-village.fr', 'edwige@silicon-village.fr'));
		}

		$message -> setBody('<h2>Récapitulatif</h2>
                             ' . $this -> recap_mail . '<br/>
                             <p>Total INSERT : ' . $this -> totalInsert . '<br/>
                             Total UPDATE : ' . $this -> totalUpdate . '<br/> 
                             Total ERRORS : ' . $this -> totalErrors . '</p>', 'text/html');

		$mailer = Swift_Mailer::newInstance($transport);

		if ($mailer -> send($message))
			$this -> _appendLog('MAIL BIEN ENVOYE !');
		else
			$this -> _appendLog('ECHEC DE L ENVOI !');
	}

	/**
	 * Fonction permettant de retourner le script d'execution spécifique à la technologie de l'e-commerçant
	 * @param infoTracking Array contient l'idVendeur l'url et la techno de l'ecommercant
	 * @return object
	 */
	public function getTechnoEcommercant($infoTracking) {
		switch ($infoTracking['SOFTWARE']) {

			case "Prestashop" :
				return new BusinessImportPrestashop($infoTracking['URL'], $infoTracking['VENDOR_ID'], $this -> _logfile);
				break;

			case "Thelia" :
				return new BusinessImportThelia($infoTracking['URL'], $infoTracking['VENDOR_ID'], $this -> _logfile);
				break;

			case "Oscommerce" :
				return new BusinessImportOscommerce($infoTracking['URL'], $infoTracking['VENDOR_ID']);
				break;

			case "Virtuemart" :
				return new BusinessImportVirtuemart($infoTracking['URL'], $infoTracking['VENDOR_ID'], $infoTracking['VERSION'], $this -> _logfile);
				break;

			case "Magento" :
				return new BusinessImportMagento($infoTracking['URL'], $infoTracking['VENDOR_ID']);
				break;

			case "CSV_AUTO" :
				//Cas particulier Grande Récrée
				//if($infoTracking['VENDOR_ID'] != '61' )
				return new BusinessImportCSV($infoTracking['URL'], $infoTracking['VENDOR_ID']);
				break;
		}
	}

	protected function atleastOneIsAuto($infoCsv) {
		foreach ($infoCsv as $csv) {

			if ($csv -> getAuto()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

	protected function _appendLog($txt) {

		//$this->_log .= $txt;
		file_put_contents($this -> _logfile, $txt . "\r\n", FILE_APPEND);
		echo $txt . "\r\n";

		$this -> recap_mail .= $txt . "<br/>";

	}

}
?>