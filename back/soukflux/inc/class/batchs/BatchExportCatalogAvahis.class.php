<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}


/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchExportCatalogAvahis extends Batch{

	static protected 	$_description 	= "[EXPORT] Génération des CSV fiches produits";
	private 			$_name 			= "BatchExportCatalogAvahis";
	
	private static 		$db;
	protected 			$vendor_id;
	protected 			$cat_id;
	protected 			$cat_name;
	
	protected 			$ArrCSV 	= array();
	
	protected static 	$nbProd 	= 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs 	= 0; //Nombre d'erreurs d'export produit car poids vide
	protected 			$total_prod = 0;
	
	protected 			$diff 		= false;
	protected 			$limit 		= true; 	//Limit l'export au produit associé.
	protected 			$mode 		= "create"; // "update" OU "create"
	protected           $specif_cat = "";
		
	private 			$rep 		= "/opt/soukeo/export/categories/";
	
	
	/**
	 * Routine du batch
	 * @param 	String 	$vendor_id 	 Optionnel contenant l'id du vendeur permettant alors de lancer ce batch
	 * pour un vendeur en particulier.
	 */
	public function work($vendor_id="", $diff=false, $mode="update", $specif_cat = "")
	{
		$this -> diff  = $diff;
		$this -> mode  = $mode;
		$this -> specif_cat = $specif_cat;
		
		if($this -> diff){
			$this -> _appendLog("Differentiel ACTIF -  Mode : ". $this -> mode);
		}else {
			$this -> _appendLog("Routine classique sans differentiel");
		}
        
        if($this -> specif_cat){
            $this -> _appendLog("CATEGORIE Specifique  N° : ". $this -> specif_cat);
        }
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("\n\n");
        $this -> _appendLog("\n\n---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		self::$db -> prepUpdateMetaKeyword();
		self::$db -> prepUpdateMetaDescrip();
		self::$db -> prepUpdateMetaTitle();
		
		if($vendor_id){
			$this -> vendor_id = $vendor_id;
			$catList = self::$db -> getAllCatSoldByEcom($this->vendor_id, $this->diff, $this->mode, $this -> specif_cat);
			$this -> _appendLog('Vendor id : '. $this->vendor_id . " - ". self::$db -> getNomVendor($this -> vendor_id));
			$this -> _appendLog("\n\n");
			$this -> _appendLog("\n\n--------------------------------------------------------------------\n\n");
		}
		else{
			$catList = self::$db->getAllCatWithProducts($this->limit, $this->diff, $this->mode, $this -> specif_cat);			
		}
         
		
		//POUR CHAQUE CATEGORIES
		foreach($catList as $cat){

			if( $cat['categories'] != "null" 
				&& $cat['categories'] != "NULL"
				&& $cat['categories'] != "" 
				&& $cat['categories'] != null ){				
				
				$this->cat_id = $cat['categories'];
				
				//Obtenir categories sous forme de chaine grace à récursive
				$categories = self::$db->getStrCategB($this->cat_id, "");
				$categories = substr($categories, 0, strlen($categories)-1); //Enlever le dernier "/"
				
				$this->cat_name = end(explode("/", $categories));
				
				//Liste attributs pour tous produits de cette Categ
				$attributeList = self::$db->getAllAttrByProdFromCat($this->cat_id);
				
				////////////////////////// ENTETE /////////////////////////////////////////////////////
				$champsFixes = $this -> getHeaderChpsFixes();
				$champsAttr  = $this -> getHeaderAttr($attributeList);
				
				$this->ArrCSV['entete'] = array_merge($champsFixes,$champsAttr);
		
				////////////////////////// CONTENU // PRODUITS////////////////////////////////////////
				if($this->vendor_id != ""){
					$productList = self::$db->getAllProdByCatIdAndVendor($this->cat_id, $this->vendor_id, $this->diff, $this->mode);
				}
				else{
					$productList = self::$db->getAllProdByCatId($this->cat_id, $this->diff, $this->limit, $this->mode);	
				}
						
				foreach ($productList as $product) {
					if( $product['weight'] != ""  && $product['weight'] != 0 
					 && $product['weight'] != "NULL" && $product['weight'] != null){
					 	
                        //SI FICHE PRODUIT AVAHIS CONDITIONS....
                        if($product['dropship_vendor'] == 24){
                            
                            //SI VENDU UNIQUEMENT PAR AVAHIS CONDITIONS ...
                            if (!self::$db -> productIsAlreadySold($product['sku'])){
                                
                                $stockprice = self::$db -> getStockPriceEcomGP($product['sku'], 24);
                                $stockprice = $stockprice[0];
                                
                                //CREATION FICHE PRODUIT AVAHIS
                                if($stockprice['QUANTITY'] > 0 && $product['weight'] > 0.15 && $product['weight'] < 30){
                                    self::$nbProd ++;
                                    $champsFixes = $this->getProductChpsFixes($product, $categories) ;                              
                                    $champsAttr  = $this->getProductAttr($product, $attributeList) ;
                                    $this->ArrCSV['prod'.self::$nbProd] = array_merge($champsFixes,$champsAttr); 
                                } 
                            }
                            //SINON VENDU PAR COMMERCANT ALORS EXPORT NORMAL
                            else{
                                self::$nbProd ++;
                                $champsFixes = $this->getProductChpsFixes($product, $categories) ;                              
                                $champsAttr  = $this->getProductAttr($product, $attributeList) ;
                                $this->ArrCSV['prod'.self::$nbProd] = array_merge($champsFixes,$champsAttr); 
                            }
                        //SINON EXPORT NORMAL
                        }else{
                            self::$nbProd ++;
                    
                            $champsFixes = $this->getProductChpsFixes($product, $categories) ;                              
                            $champsAttr  = $this->getProductAttr($product, $attributeList) ;
            
                            $this->ArrCSV['prod'.self::$nbProd] = array_merge($champsFixes,$champsAttr);
                        }	
					 	
					 }
					else {
						self::$nbErreurs++;
					}
				}
				
                if(self::$nbProd > 0){
                    $this->generateCsv();
                    $this -> _appendLog(('Nombre de produits '. $this->cat_name.' Cat_id :  '. $this->cat_id .' nb Produits exportés : ' . self::$nbProd ) );
                    
                    if(self::$nbErreurs != 0){
                        $this -> _appendLog('Produits sans poids : '. self::$nbErreurs);
                    } 
                    $this -> _appendLog('---------------------------------------------------------------------------------------------');    
                }
				
				$this->total_prod += self::$nbProd;
				self::$nbProd 	 = 0 ;
				self::$nbErreurs = 0 ;
				unset($this->ArrCSV);
			}		
		}

		$this -> _appendLog('Nombre de produits total '.$this->total_prod);
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));	
		
	}	
	
	
	/**
	 * Retourne un array contenant les labels des champs fixes pour entête 
	 * @return array 	contenant les champs fixes du CSV.
	 */
	protected function getHeaderChpsFixes()
	{
		return array(	"sku",
						"categories",
						"name",
						"description",
						"short_description",
						"price",
						"weight",
						"country_of_manufacture",
						"manufacturer",
						"meta_description",
						"meta_keyword",
						"meta_title",
						"image",
						"small_image",
						"thumbnail",
						"visibility",
						"attribute_set",
						"tax_class_id",
						"udropship_vendor" );
	}
	
	
	/**
	 * Retourne un array contenant les labels des attributs pour la 
	 * catégorie
	 * @return 	array 	$result 	contenant les labels des attributs
	 */
	protected function getHeaderAttr($attrList)
	{
		$result = array();
		
		foreach($attrList as $attr){
		    $attr = strtolower($attr['id_attribut']."_".$attr['code_attr']);
            $attr = stripAccents($attr);
            $attr = str_replace(' ', '_', $attr);
            $attr = preg_replace('/[^A-Za-z0-9 _]/', '', $attr);
            $result[] =  "av_".$attr ;
		}
		
		return $result;
	}
	
	
	/**
	 * Fonctionnant retournant un array contenant les données pour les champs fixes
	 * du produit
	 * @param 	array 	$product 	 	contenant les informations du produit
	 * @param  	string 	$categories  	contenant l'arborescence de la catégorie sous forme cat/sscat/sssscat
	 * @return 	array 					contenant les différents champs fixes remplis pour aller dans le CSV
	 */
	protected function getProductChpsFixes($product, $categories)
	{
		
		if($product['dropship_vendor'] == '39' ){
			
			$product['image'] = "";
		}
		
		$description = $this -> getDescript($product);
		$shortDescr  = $this -> getShortDescript($product);
		
		$image 		 = $this -> getImage($product);
		$small_image = $this -> getSmallImage($product);
		$thumbnail   = $this -> getThumbnail($product);
		
		$metaKeyword = $this -> getMetaKeyword($product);
		$metaDescrip = $this -> getMetaDescript($product);
		$metaTitle   = $this -> getMetaTitle($product);
		
		return array(	(string)$product['sku'],
						$categories,
						$product['name'],
						$description,
						$shortDescr,
						$product['price'],
						$product['weight'],
						$product['country_of_manufacture'] ? $product['country_of_manufacture'] : "",
						$product['manufacturer'] ? ucfirst(strtolower($product['manufacturer'])) : "",
						$metaDescrip,
						$metaKeyword,
						$metaTitle,
						$image,
						$small_image,
						$thumbnail,
						"Catalogue, Recherche",
						str_replace(' ', '_', "avahis_".$this->cat_name),
						$product['tva'] ? $product['tva'] : SystemParams::getParam('product*tva'),
						($product['dropship_vendor'] ? $product['dropship_vendor']  : 24 ) 
						);
	}
	
	
	/**
	 * Check si une description HTML est présente, si c'est le cas elle est renvoyé sinon renvoie la description
	 * normale. Et Enfin si pas de description normale alors on utlise la short_description
	 * @param 	array 	$product 		infos du produit
	 * @return 	string 	$description 	la description qui sera utilisée
	 */
	protected function getDescript ($product)
	{
		
		if($product['description_html'] != "" && $product['description_html'] != "NULL" && $product['description_html'] != null ){
			
			$description = $product['description_html'];
			
		}elseif($product['description']!="NULL" && $product['description']!=""){
			
			$description = $product['description'];
			
		}else{
			
			$description = $product['short_description'];
		}
		
		return $description;
	}
	
	
	/**
	 * Check si la short_description n'est pas vide, si elle est vide renvoie alors une chaine contenant
	 * le Manufacturer + le nom du produit
	 * @param 	array 		$product 			 	du produit
	 * @return 	string  	$shortDescription 		la short_description qui sera utilisée
	 */
	protected function getShortDescript($product)
	{
		if( $product['short_description'] != "NULL" && $product['short_description'] != "" && $product['short_description'] != "" ){
			$shortDescription = $product['short_description'] ;
		}
		else{
			$shortDescription = $product['manufacturer']. " - " . $product['name'];
		} 
		
		return $shortDescription ;
	}
	
	
	/**
	 * Check si la local_image est présente, sinon dans l'ordre de priorité récupérera : la local_small_image, local_thumbnail ou
	 * encore l'image simple qui est une URL pointant vers sites extérieurs 
	 * @param 	array 	$product 	 	du produit
	 * @return 	string 	$image 		 	adresse locale ou externe de l'image choisie
	 */
	protected function getImage($product)
	{
		if($product['local_image'] != "" && $product['local_image'] != "NULL" && $product['local_image'] !== null){
			
			$image = $product['local_image'] ;
			
		}elseif($product['local_small_image'] != "" && $product['local_small_image'] != "NULL" && $product['local_small_image'] !== null){
			
			$image = $product['local_small_image'];
			
		}elseif($product['local_thumbnail'] != "" && $product['local_thumbnail'] != "NULL" && $product['local_thumbnail'] !== null){
			
			$image = $product['local_thumbnail'] ;
			
		}else{
			
			$image = $product['image'];
		}
			
		return $image;	
	}
	
	
	/**
	 * Check si la local_small_image est présente, sinon dans l'ordre de priorité récupérera : la local_thumbnail, local_image ou
	 * encore la small_image simple qui est une URL pointant vers sites extérieurs
	 * @param 	array 	$product 		 	du produit
	 * @return 	string 	$small_image 		adresse locale ou externe de l'image choisie
	 */
	protected function getSmallImage($product)
	{
		if($product['local_small_image'] != "" && $product['local_small_image'] != "NULL" && $product['local_small_image'] != null){
			
			$small_image = $product['local_small_image'] ;
			
		}elseif($product['local_thumbnail'] != "" && $product['local_thumbnail'] != "NULL" && $product['local_thumbnail'] != null){
			
			$small_image = $product['local_thumbnail'];
			
		}elseif($product['local_image']!= "" && $product['local_image'] != "NULL" && $product['local_image'] !== null){
			
			$small_image = $product['local_image'] ;
					
		}else{
			
			$small_image = $product['small_image'];
		}
		
		return $small_image;		
	}
	
	
	
	/**
	 * Check si la local_thumbnail est présente, sinon dans l'ordre de priorité récupérera : la local_small_image, local_image ou
	 * encore la thumbnail simple qui est une URL pointant vers sites extérieurs
	 * @param 	array 	$product 		 	du produit
	 * @return 	string  $thumbnail 			adresse locale ou externe de l'image choisie
	 */
	protected function getThumbnail($product)
	{
		if($product['local_thumbnail'] != "" && $product['local_thumbnail'] != "NULL" && $product['local_thumbnail'] != null){
			
			$thumbnail = $product['local_thumbnail'] ;
			
		}elseif($product['local_small_image'] != "" && $product['local_small_image'] != "NULL" && $product['local_small_image'] != null){
			
			$thumbnail = $product['local_small_image'];
			
		}elseif($product['local_image']!= "" && $product['local_image'] != "NULL" && $product['local_image'] !== null){
			
			$thumbnail = $product['local_image'] ;
			
		}else{
			
			$thumbnail = $product['thumbnail'];
		}
		
		return $thumbnail;		
	}
	
	
	/**
	 * Fonction retournant un array contenant les données pour les attributs
	 * du produit
	 * @param 	array 	$product 			infos du produit
	 * @param 	array 	$attributeListe 	liste des attributs
	 * @return 	array 	$result 			tableau contenant les labels d'attributs
	 */
	protected function getProductAttr($product, $attributeList)
	{
		$result = array();
		
		//On récupère le tableau associatif label attribut / valeur pour ce produit
		$prodAttrList = self::$db->getAllAttrByProd($product['id_produit']);
		
		foreach ($attributeList as $attr) {
			
			if(isset( $prodAttrList[$attr['label_attr']]) ){
				$result[] = $prodAttrList[$attr['label_attr']];
			}else{
				$result[] = "";
			}
		}
		
		return $result;
	}
	
	
	/**
	 * Check si les champs meta_keyword ne sont pas vide. S'ils le sont il y a création des metakeyword avec utilisation
	 * du manufacturer, du nom du produit, de l'arborescence de catégorie du produit ainsi que le sku.
	 * Chacune de ces informations sont séparées par des virgules. Une fois la construction terminée, sauvegarde en base de donnée
	 * du metakeyword pour ce produit.
	 * @param 	array 	$product 		 	du produit
	 * @param 	string 	$categories 		catégories sous la forme Cat/sousCat/SSCat
	 * @return 	string  $metakeyword 	 	metakeyword final 
	 */
	protected function getMetaKeyword($product, $categories)
	{
		if($product['meta_keyword'] != "" && $product['meta_keyword'] !== null && $product['meta_keyword'] != "NULL"){
			
			$metakeyword = $product['meta_keyword'] ;
		}
		else{
			$manuf = $product['manufacturer'];
			$manuf = ucfirst(strtolower(str_replace("Ú", "e", $manuf)));
				
			$metakeyword = $product['name'].", ".$manuf. ", ";
			$categories = str_replace("/", ", ", $categories);
			
			$metakeyword .= $categories . ", " . $product['sku'];
			
			self::$db -> updateMetaKeyword($product['id_produit'], $metakeyword.", ".stripAccents($metakeyword));	
		}
		
		return $metakeyword;
	}
	
	
	/**
	 * Check si les champs meta_description ne sont pas vide. S'ils le sont il y a création de metadescription avec utilisation
	 * du nom du produit, de la short_description si présente et du meta_title si présent..
	 * Chacune de ces informations sont séparées par des virgules et strip_tags appliqué pour enlever toutes balises. 
	 * Une fois la construction terminée, sauvegarde en base de donnée de la metadescription pour ce produit.
	 * @param 	array 	$product 		du produit
	 * @return 	string  $metadescrip 	metadescrip final 
	 */
	protected function getMetaDescript($product)
	{
		if($product['meta_description'] != "" && $product['meta_description'] !== null && $product['meta_description'] != "NULL"){
			
			$metadescrip = $product['meta_description'] ;
		}
		else{
			
			$metadescrip = $product['name'];
		
			if($product['short_description'] != "" && $product['short_description'] != "NULL" && $product['short_description'] !== null){
				$metadescrip .= ", " . $product['short_description'];
			}
			
			if($product['meta_title'] != "" && $product['meta_title'] != "NULL" && $product['meta_title'] !== null){
				$metadescrip .= ", " . $product['meta_title'];
			}
			
			self::$db -> updateMetaDescrip($product['id_produit'], $metadescrip);
		}
		
		return strip_tags($metadescrip);	
	}
	
	
	/**
	 * Check si les champs meta_title ne sont pas vide. S'ils le sont il y a création du meta_title avec utilisation
	 * du nom du produit. 
	 * Une fois la construction terminée, sauvegarde en base de donnée du meta_title pour ce produit.
	 * @param 	array 	$product 		 du produit
	 * @return 	string 	$metatitle 		 meta_title final 
	 */
	protected function getMetaTitle($product)
	{
		if($product['meta_title'] != "" && $product['meta_title'] !== null && $product['meta_title'] != "NULL"){
			
			$metatitle = $product['meta_title'] ;
		}
		else{
			
			$metatitle = $product['name'];
			self::$db -> updateMetaTitle($product['id_produit'], $metatitle);
		}
		
		return $metatitle;		
	}
	
	
	/**
	 * Fonction permettant de générer un csv grace à l'array contenant chaque ligne pour la catégorie
	 * Il y a création du fichier qui portera le nom de la catégorie et le nom du vendeur si ce batch est appellé
	 * dans le contexte d'un unique vendeur.
	 */
	protected function generateCsv()
	{
		global $root_path;
		
		if($this->vendor_id != ""){
			$name = $this->rep.$this->vendor_id."-".str_replace(array(' ', "'", "\\", "/", ","), '_', self::$db->stripAccents($this->cat_name))."-".$this->cat_id."-".date("d-m-Y")."_".date("H-i-s").".csv";		
		}else{
			$name = $this->rep.str_replace(array(' ', "'", "\\", "/", ","), '_', self::$db->stripAccents($this->cat_name))."-".$this->cat_id."-".date("d-m-Y")."_".date("H-i-s").".csv";	
		}
		
		//Create File
		file_put_contents($name , "");
		$fp = fopen($name, "w");

		//Genere CSV
		foreach ($this->ArrCSV as $fields) {
		    fputcsv($fp, $fields, ';', '"');
		}
		
		fclose($fp);
	}
	
    public function help()
    {
        $help =  PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL;
        $help .= "Ordre des arguments a entrer : ".PHP_EOL;
        $help .= "[id_vendeur OU \"\"] [differientiel = 0 ou 1] [mode = \"create\" ou \"update\"] [id_categorie = int ou \"\"] ".PHP_EOL.PHP_EOL;
        $help .= "Exemple : \n php /var/www/soukflux/common/batch/execBatch.php 11 24 1 \"update\" 2305".PHP_EOL;
        $help .= "export vendeur 24 en differentiel mode update pour categorie 2305".PHP_EOL;
        $help .= PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL.PHP_EOL;
        
        return $help;
    }
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>