<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchImportCategorie extends Batch {
	private $_name = "BatchImportCategorie";
	static protected $_description = "[IMPORT] [API] -- Import catégories AVAHIS ";
	private static $db;
	private $listeid;

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();

		//Connexion webservice marketplace
		$proxy = new SoapClient(SystemParams::getParam("marketplace*urlApi"));
		$sessionId = $proxy -> login(SystemParams::getParam("marketplace*loginApi"), SystemParams::getParam("marketplace*passwordApi"));
		$result = $proxy -> catalogCategoryTree($sessionId);
		self::$db -> prepInsertCat();

		$V['cateory_id'] = $result -> category_id;
		$V['name'] = $result -> name;
		$V['parent_id'] = $result -> parent_id;
		$V['position'] = $result -> position;
		$V['level'] = $result -> level;

		self::$db -> AddCategory($V);
		$this -> listeid = $result -> category_id;

		$this -> loadDataCat($result -> children);
		$this -> deleteOldCat($this -> listeid);

		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

	}

	function deleteOldCat($listeId) {
		$sql = "update sfk_categorie set cat_active = 0 where cat_id not in ( $listeId )";
		self::$db -> exec($sql);
	}

	function loadDataCat($data) {
		foreach ($data as $D) {
			$this -> insertcat($D);
			$this -> loadDataCat($D -> children);
		}
	}

	function insertcat($data) {
		$V['cateory_id'] = $data -> category_id;
		$V['name'] = $data -> name;
		$V['parent_id'] = $data -> parent_id;
		$V['position'] = $data -> position;
		$V['level'] = $data -> level;
		$this -> listeid .= " , " . $V['cateory_id'];
		self::$db -> AddCategory($V);
		return true;
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
