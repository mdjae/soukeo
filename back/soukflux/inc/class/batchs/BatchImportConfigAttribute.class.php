<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchImportConfigAttribute extends Batch 
{
	private $_name = "BatchImportConfigAttribute";
	static protected $_description = "[AVAHIS-PROD][EXPORT] La configuration des attributes -> Avahis";
	private static $db;
	private $_debut;
	private $_fin;
	private $_dir = '/opt/soukeo/export/configs/';
	
	 /**
		 * Permet de récupérer la description du programme
		 * @author tpo
		 *
		 */
		public static function getDescription() {
		return self::$_description;
		}
	/**
	 * Fonction executee au debut du batch
	 */
	public function begin()
	{
		$this->_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
	}
	
	/**
	 * Fonction executee a la fin du batch
	 */
	public function end()
	{
		$this->_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($this->_fin - $this->_debut, 3));
	}
	
	
	
	/**
	 * Fonction executee par le batch
	 */
	public function work() 
	{
		$this->begin();
		
		//Connection base Avahis
		self::$db = new BusinessAvahisModel();
		
		$dir= '/opt/soukeo/export_rec/configs';
		$archives= $dir.'/archives';
	
	
		if ($hdle = opendir($dir))
		{
			$this -> _appendLog('Lancement de l\'import des fichiers');
			
	        $time = date('d-m-Y').'_'.time();
	        echo exec("mkdir $archives/$time");
	                
			while (false !== ($f = readdir($hdle))) 
			{
				if ($f != "." && $f != ".." && $f != "archives") 
				{
					$this -> _appendLog("$f");
					
					// read CSV
					$configs = self::$db->readCsv($f,';');
					
						//Foreach Element in csv update data
						foreach ($configs as $c) {
								
							$this -> _appendLog('Attribut : '.var_dump($c['frontend_label']));
							$attrData = self::$db->getAttributeInfoByCode($c['attribute_code']);
							
							//var_dump($attrData);
							//var_dump($attrData);
							
							$eavAttribute =  array(
								":entity_type_id" => $attrData['entity_type_id'],
								":attribute_code" => $attrData['attribute_code'],
								":frontend_input" => $c['frontend_input'],
								":frontend_label" => $c['frontend_label']
							);
							
							$eavAttributeLabel = array( 
								":label" => $c['frontend_label'],
								":attribute_id" => $attrData['attribute_id']
							);
							
							$catalogEavAttribute = array( 
								":is_filterable" => $c['is_filterable'],
								":attribute_id" => $attrData['attribute_id']
							);
							
							self::$db->updateEavAttribute($eavAttribute);
							self::$db->updateEavAttributeLabel($eavAttributeLabel);
							self::$db->updateCatalogEavAttribute($catalogEavAttribute);
						}
					
					exec("mv $dir/$f $archives/$time");
			    }
			}
		}
		
		closedir($hdle);
		$this->end();
	}
	
}
