<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch de remplacer la virgules du champ weight des tables catalogue produit et produit grossiste.
 * @author hkcfdc
 */
class BatchChangecaractvirgule extends Batch{

	static protected 	$_description 	= "[CHANGE] [CARACT] [,]changement des caractére , dans les tables catalog product et produit grossiste";
	private 			$_name 			= "BatchChangecaractvirgule";
	
	private static 		$db;
	protected static 	$nbProd = 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide
	protected $total_prod =0;
	
	/*** Routine du batch*/
	public function work()
	{
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
		
		self::$db = new BusinessSoukeoModel();
		
		// ---------------- table sfk_catalog_product ------- //
		$this -> _appendLog("-------------------------------------- table sfk_catalog_product -----------------");
		
		// sku weight
		$eanLab = 'sku' ;
		$poidLab = 'weight' ;
		$prixLab = 'price' ;
		$tableLab = 'sfk_catalog_product' ;
		//Toutes les produits avec un caractére a changé
		$productList = self::$db->getAllprodAvecvirgule($tableLab,$prixLab);
		//var_dump($productList);
		
		foreach ($productList as $product) {

		 	$product[$prixLab] = str_replace(",", ".", $product[$prixLab]);
		 	$this->nbProd ++;
	 		self::$db->updatetWeightToCatProd($product[$eanLab],$product[$prixLab],$tableLab,$prixLab,$eanLab );
			$this->total_prod ++ ;
		}
		
		$this -> _appendLog('Nombre de produits '.$tableLab.'  : '.$this->nbProd );
		$this->nbProd = 0 ;
		/*
		// -------------------- table sfk_produit_grossiste
		$this -> _appendLog("-------------------------------------- table sfk_produit_grossiste -----------------");
		
		// EAN WEIGHT
		//paramétre :
		$eanLab = 'EAN' ;
		$poidLab = 'WEIGHT' ;
		$tableLab = 'sfk_produit_grossiste' ; 
		//Toutes les produits avec un caractére a changé
		$productList = self::$db->getAllprodAvecvirgule($tableLab,$poidLab);
		//var_dump($productList);
		
		foreach ($productList as $product) {

		 	$product[$poidLab] = str_replace(",", ".", $product[$poidLab]);
		 	$this->nbProd ++;
	 		self::$db->updatetWeightToCatProd($product[$eanLab],$product[$poidLab],$tableLab,$poidLab,$eanLab );
			$this->total_prod ++ ;
		}
		
		$this -> _appendLog('Nombre de produits '.$tableLab.' : '.$this->nbProd );
		*/
		/*
		// ---------------- table sfk_catalog_product ------- //
		$this -> _appendLog("-------------------------------------- table sfk_catalog_product -----------------");
		
		// sku weight
		$eanLab = 'sku' ;
		$poidLab = 'weight' ;
		$tableLab = 'sfk_catalog_product' ;
		//Toutes les produits avec un caractére a changé
		$productList = self::$db->getAllprodAvecvirgule($tableLab,$poidLab);
		//var_dump($productList);
		
		foreach ($productList as $product) {

		 	$product[$poidLab] = str_replace(",", ".", $product[$poidLab]);
		 	$this->nbProd ++;
	 		self::$db->updatetWeightToCatProd($product[$eanLab],$product[$poidLab],$tableLab,$poidLab,$eanLab );
			$this->total_prod ++ ;
		}
		
		$this -> _appendLog('Nombre de produits '.$tableLab.'  : '.$this->nbProd );
		$this->nbProd = 0 ;
		
		// -------------------- table sfk_produit_grossiste
		$this -> _appendLog("-------------------------------------- table sfk_produit_grossiste -----------------");
		
		// EAN WEIGHT
		//paramétre :
		$eanLab = 'EAN' ;
		$poidLab = 'WEIGHT' ;
		$tableLab = 'sfk_produit_grossiste' ; 
		//Toutes les produits avec un caractére a changé
		$productList = self::$db->getAllprodAvecvirgule($tableLab,$poidLab);
		//var_dump($productList);
		
		foreach ($productList as $product) {

		 	$product[$poidLab] = str_replace(",", ".", $product[$poidLab]);
		 	$this->nbProd ++;
	 		self::$db->updatetWeightToCatProd($product[$eanLab],$product[$poidLab],$tableLab,$poidLab,$eanLab );
			$this->total_prod ++ ;
		}
		
		$this -> _appendLog('Nombre de produits '.$tableLab.' : '.$this->nbProd );
		
		 */
		$this -> _appendLog("------------------------------------------------------------------------------------");
			
		$this -> _appendLog('Nombre de produits total '.$this->total_prod);
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	

	/**
	 * Permet de récupérer la description du programme
	 * @author fred
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>