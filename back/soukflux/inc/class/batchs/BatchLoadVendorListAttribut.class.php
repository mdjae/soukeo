<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch permettant la génération de l'attribut VENDOR_LIST sur chaque produit prêts
 * à être exportés sur Avahis.com. L'attribut doit être rempli avec les noms des vendeurs
 * séparé par espace virgule espace.
 * @author Philippe_LA
 */
class BatchLoadVendorListAttribut extends Batch{

	static protected 	$_description 	= "[LOCAL] [Attribut VENDOR_LIST] Batch load VENDOR_LIST ATTRIBUT";
	private 			$_name 			= "BatchLoadVendorListAttribut";
	
	protected           $db;
    protected           $catalogProductManager;
    protected           $attributManager;
    protected           $productAttributManager;
    protected           $categorieManager;
    
	protected 	        $nbProd = 0; //Nombre de produits exportés modifiés pour le log
	protected 	        $nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide
	

	/**
	 * Routine du batch
	 */
	public function work()
	{
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux
		$this -> db                         = new BusinessSoukeoGrossisteModel();
        $this -> catalogProductManager      = new ManagerCatalogProduct();
        $this -> attributManager            = new ManagerAttribut () ;
        $this -> productAttributManager     = new ManagerProductAttribut();
        $this -> categorieManager           = new ManagerCategorie();                
		
		
		$product_list = $this -> catalogProductManager -> getAll() ;
        
        if($attribut = $this->attributManager->getBy(array("code_attr" => "VENDOR_LIST", "cat_id" => 2 ))){
            $attribut = $attribut;
        }
        else{
            $attribut = new BusinessAttribut(array("code_attr" => "VENDOR_LIST",
                                                   "label_attr"=> "Vendor list",
                                                   "cat_id"    => 2));         
                                                   
            $this -> attributManager -> save($attribut);            
        }
        
        
        foreach ($product_list as $product) {
            
            if($current_cat = $this->categorieManager -> getBy(array("cat_id" => $product->getCategories(), "cat_active" => 1))) {
                
                $value = $this->getVendorList($product);
                
                if($product_attribut = $this->productAttributManager->getBy(array("id_produit" => $product->getId_produit(),
                                                                                  "id_attribut"  => $attribut->getId_attribut()))){
                                                                                      
                    $product_attribut -> setValue($value);
                    $this->productAttributManager->save($product_attribut);
                    $this -> nbProd ++;                                                                      
                }
                else{
                    $product_attribut = new BusinessProductAttribut(array(  "id_produit"   => $product->getId_produit(),
                                                                            "value"        => $value,
                                                                            "id_attribut"  => $attribut->getId_attribut(),
                                                                            "active"       => 1 ));
                    $this->productAttributManager->save($product_attribut);
                    $this -> nbProd ++;
                }
            }
            else{
                $this -> _appendLog("Catégorie produit invalide ! ".$product -> getCategories());
            }
        }

		$this -> _appendLog($this -> nbProd . " Produits mis a jour ! ");
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	
	
    public function getVendorList($product)
    {
        $value = "";
        
        if($vendor_list = $product -> getVendorList()){
            
            $c = 1;
            foreach ($vendor_list as $vendor ) {
                
                
                if($c === 1 ){
                    $value .= $vendor["VENDOR_NOM"] ;
                }
                else {
                    $value .= " , " . $vendor["VENDOR_NOM"]; 
                }
                $c++;
            }            
        }
        
        $this -> _appendLog($value);
        return $value;
    }

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>