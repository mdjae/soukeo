<?php 
/**
 * Exemple de classe ex�cut�e par le moteur de batch PHP d'offipse
 */
class BatchMySQLBackup extends Batch
{
	private $_name = "BatchMySQLBackup";
	static protected $_description = "Sauvegarde de la base MySQL Soukflux";
	
	/**
	 * Execute le backup de la base
	 * @author n
	 *
	 */
	public function work()
	{
		global $inc_path;
		require_once $inc_path . '/libs/db.mysqldump.inc.php';
		$res = dbdump::saveDump(false, $this->_params, '', true);
		if ($res)
		{
			$this->_return_code = 0;
			echo "FIchier sauvegardé dans " . $this->_params . "/" . $res;
		}
		else $this->_return_code = 1;
	}
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author nsa
	 *
	 */
	public static function getDescription()
	{
		return self::$_description;
	}
}
?>