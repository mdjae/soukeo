<?php 
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchMiseEnVenteProdsGross extends Batch{

	static protected 	$_description 	= "[LOCAL] -- Contrôle de la mise en vente produit";
	private 			$_name 			= "BatchMiseEnVenteProdsGross";
	
	private static 		$db;
	private static 		$db2;

	protected $grilleChronopost = array(); 
	protected static 	$nbProd = 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide

	protected $coefmin ;
	protected $coefmax ;
	protected $weightmin ;
	protected $weightmax ;
	protected $stockmin ;	
	protected $nbDoublons = 0;
	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		
		global $applicationMode;
		
		$this -> coefmin 	= SystemParams::getParam('grossiste*coef');
		$this -> coefmax 	= SystemParams::getParam('grossiste*coefmax');
		$this -> weightmin 	= SystemParams::getParam('grossiste*weightmin');
		$this -> weightmax 	= SystemParams::getParam('grossiste*weightmax');
		$this -> stockmin 	= SystemParams::getParam('grossiste*stock');	
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux et Avahis
		self::$db  = new BusinessSoukeoGrossisteModel();
		self::$db2 = new BusinessAvahisModel();
		
		//R.A.Z. des quantités des produits grossiste
		//self::$db -> razQuantityVentesAvahis();
		self::$db -> prepDoublonIncorrect();
		self::$db -> prepGetDoublonsEAN();
		self::$db -> prepAddRowStockPrixGrossiste();
		
		self::$db2 -> prepCheckProductIsSold ();
		
		$productList = self::$db->getAllActiveProdGrossisteToSell();
		
		//Variables de logs
		$add 			= 0 ;	
		$updt 			= 0 ;	
  		$ass 			= 0 ;	
  		$dejavendu 		= 0 ;	
		$errStock 		= 0 ;	
		$dejaVenduBOav 	= 0 ;
		
		 		
		//Boucle sur liste produits grossistes		
		foreach ($productList as $product) {
			
			$erreurVente = false;
			$max = 0;
			$doublon_final = null;
			$doublons = null;
			//Différenciation produit avec quantitée poitive
			if($product['QUANTITY'] >= $this -> stockmin ){
				
				$moyenne = 0;
				
				///////////////////////////////////// GESTION DES DOUBLONS ///////////////////////////////////
				if($product['EAN'] != "" && $product['EAN'] != null && $product['EAN'] !== null && strtoupper($product['EAN']) != "NULL" && !is_null($product['EAN'] ))
					$doublons = self::$db -> getDoublonsEAN($product['EAN']);
				
				if(count($doublons) > 1 ) {
					foreach ($doublons as $doublon) {
						$moyenne += $doublon['WEIGHT'];
					}
					
					$moyenne = $moyenne / count($doublons);
					
					//REGLE SI MOYENNE INFERIEURE A 1.3
					if($moyenne < 1.3){
						foreach ($doublons as $doubon) {
	
							//On prend le plus haut 
							if($doublon['WEIGHT'] > $max){
								$max = $doublon['WEIGHT'];
								$doublon_final = $doublon;
							}
						}	
					}//processus standard
					else{
						foreach ($doublons as $doubon) {
							
							//Calcul % écart
							$percentEcart = abs($moyenne - $doublon['WEIGHT']) / $moyenne * 100;
							//10% de la moyenne
							
							if($percentEcart <= 10){
								//On prend le plus haut parmi ceux corrects (écart inférieur à 10%)
								if($doublon['WEIGHT'] > $max){
									$max = $doublon['WEIGHT'];
									$doublon_final = $doublon;
								}
							}
						}
					}
					
					//Si aucun doublon n'est à moins de 10% ou bien le produit en cours n'est PAS le doublon final alors on passera
					//QUANTITY A ZERO
					if(! $doublon_final | $doublon_final['ID_PRODUCT'] != $product['ID_PRODUCT']){
						$erreurVente = true;
						
						self::$db -> setDoublonIncorrect($product['ID_PRODUCT']);
						
						$this -> nbDoublons ++ ;
					}
					
					$message = "PRODUIT EN DOUBLON POUR EAN : " . $product['EAN']. " - MOYENNE DU POIDS : ". $moyenne;
					
					if($doublon_final){
						$message .= " - PRODUIT CONSERVE FINALEMENT EST ID_PRODUCT : ".$doublon_final['ID_PRODUCT']. " POIDS : ".$doublon_final['WEIGHT'];
					}else{
						$message .= " - AUCUN PRODUIT N'A ETE CONSERVE ";
					}
					
					//$this -> _appendLog($message);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				
				$prixProd = self::$db ->getPrixCalcul($product['ID_PRODUCT']);
				$prixProd = $prixProd[0];
				$prixProd['QUANTITY'] = $product['QUANTITY'];
				
				if((float)$prixProd['PRIX_FINAL'] > 3000
				 | (float)$prixProd['PRIX_ACHAT'] < 1 
				 | (float)$prixProd['PRIX_KILO'] < 20  
				 | (float)$prixProd['POIDS'] > (float)$this -> weightmax  
				 | (float)$prixProd['POIDS'] < (float)$this -> weightmin 
				 | (float)$prixProd['COEF']  < (float)$this -> coefmin 
				 | (float)$prixProd['COEF']  > (float)$this -> coefmax | $erreurVente){
					$prixProd['QUANTITY'] = 0 ;
				}
				
				
				/////////////////////////// SPECIAL LDLC JEUX VIDEO /////////////////////////////
				/////////////////////////// SPECIAL LDLC JEUX VIDEO /////////////////////////////
				if($product['GROSSISTE_ID'] == 2  && $this -> prodIsJeuxVideo($product['CATEOGRY'])){
					 $prixProd['QUANTITY'] = 0 ;
				}
				/////////////////////////// SPECIAL LDLC JEUX VIDEO /////////////////////////////
				/////////////////////////// SPECIAL LDLC JEUX VIDEO /////////////////////////////
				
				//Verif produit pas vendu sur avahis
				if($sku_av = self::$db -> getSkuAssocProdGr($product['ID_PRODUCT'])){
					if(self::$db2 -> productIsAlreadySoldAvahis($sku_av)){
						$prixProd['QUANTITY'] = 0 ;
						$this -> _appendLog("PRODUIT ID_PRODUCT : ".$product['ID_PRODUCT']. " - sku : ".$sku_av." -  deja vendu par un ecommercant sur le back office Avahis");
						$dejaVenduBOav ++ ;
					}	
				}
				
				//Insertion ligne vente
				$log = self::$db ->addRowStockPrixGrossiste($prixProd, $product['id_produit']);
				
				//Log insert/update/error
				if($log == 1){
					$add++;
					//$this -> _appendLog (self::$nbProd.' - Prod ID : ' . $product['ID_PRODUCT'] .' ajoute aux ventes !');
				}elseif ($log == 2){
					$updt++;
					//$this -> _appendLog (self::$nbProd.' - Prod ID : ' . $product['ID_PRODUCT'] .' deja en vente mis à jour !');
				}elseif($log == 0){
					$err++;
					//$this -> _appendLog (self::$nbProd.' - Prod ID : ' . $product['ID_PRODUCT'] .' ERREUR SQL');
				}elseif($log == 3){
					$ass++;
					//$this -> _appendLog (self::$nbProd.' - Prod ID : ' . $product['ID_PRODUCT'] .' NON VENDU CAR - NON ASSOCIE');
				}elseif($log == 4){
					$dejavendu++;
					//$this -> _appendLog (self::$nbProd.' - Prod ID : ' . $product['ID_PRODUCT'] .' NON VENDU CAR - DEJA VENDU PAR ECOM');
				}
			}else{
				$errStock ++;
				//$this -> _appendLog (self::$nbProd.' - Prod ID : ' . $product['ID_PRODUCT'] .' NON VENDU CAR - STOCK INFERIEUR A '.$stockMin.'');
			}
			
			self::$nbProd++;
		}

		$this -> _appendLog ('-------------------------------------------------------------------------------------------------------');
		$this -> _appendLog ('-------------------------------------------------------------------------------------------------------');
		$this -> _appendLog ($add ? $add. " nouveaux produit ajouté(s) aux ventes !" : 0 ." nouveaux produit ajouté(s) aux ventes !");
		$this -> _appendLog ($updt ? $updt." produit(s) déjà en vente mis à jour !"  : 0 ." produit(s) déjà en vente mis à jour !");
		$this -> _appendLog ($ass ? $ass." erreur(s) car produits grossiste ne correspond à aucun produit Avahis (non associé !)." : 0 ." erreur(s) car produits grossiste ne correspond à aucun produit Avahis (non associé !).");
		$this -> _appendLog ($dejavendu ? $dejavendu." produit(s) déja vendus par e-commerçants." : "Aucun produit déja vendu par e-commerçant");
		$this -> _appendLog ($dejaVenduBOav ? $dejaVenduBOav." produit(s) déja vendus par e-commerçants sur B.O. Avahis (dans udropship_vendor_product)." : "Aucun produit déja vendu par e-commerçant sur B.O. Avahis ");
		$this -> _appendLog ($errStock ? $errStock." produits avec stock inférieur A ".$this -> stockmin." non vendu !" : 0 ." produits avec stock inférieur A ".$this -> stockmin." non vendu !");
		$this -> _appendLog ($err ? $err." erreur(s)." : 0 ." erreur(s) SQL.");
		$this -> _appendLog ($this -> nbDoublons ? $this -> nbDoublons . " produits non insérés car ils sont des DOUBLONS NON VALIDES" : "Aucun DOUBLON");
								
		$this -> _appendLog(('TOTAL produits grossiste en vente : ' . $add + $updt ) );
		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
		$count = self::$db -> getCountVenteAvahis();
		
		// Le transporteur
		$transport = Swift_MailTransport::newInstance();

		$message = Swift_Message::newInstance();
  		$message -> setSubject('Récapitulatif mise en vente produits grossistes');
		
		if($applicationMode == "DEV" ) {
			$message -> setFrom('philippe.lattention@silicon-village.fr');
			$message -> setTo(array('philippe.lattention@silicon-village.fr'));
		}else{
			$message -> setFrom(SystemParams::getParam("system*email_from"));
			$message -> setTo(array('philippe.lattention@silicon-village.fr', 'thomas@silicon-village.fr'));	
		}	
		
  		$message -> setBody('<h2>Récapitulatif</h2>
  							 Temps d\'éxécution : '.number_format($temps_fin - $temps_debut, 3).' s 
  							 <p>'.$add . ' nouveaux produit ajouté(s) aux ventes ! <br/>
  							 '.$updt .' produit(s) déjà en vente mis à jour !<br/>
  							 '.$ass .' erreur(s) car produits grossiste ne correspond à aucun produit Avahis (non associé !)</p>
  							 <p>'.$dejavendu .' produit(s) déja vendus par e-commerçants.</p>
  							 <p>'.$dejaVenduBOav.' produit(s) déja vendus par e-commerçants sur B.O. Avahis (dans udropship_vendor_product)
  							 <p>'.$errStock .' produits avec stock inférieur A '.$this -> stockmin.' non vendu !</p>
  							 <p>'.$this -> nbDoublons . ' produits non insérés car ils sont des DOUBLONS NON VALIDES </p>
  							 <p>AVAHIS vend maintenant <strong>'.$count.'</strong> produits sur la market place !</p>'
  							 , 'text/html');
		
		$mailer = Swift_Mailer::newInstance($transport);
		
		if($mailer -> send($message)) 
			$this->_appendLog('MAIL BIEN ENVOYE !'); 
		else
			$this->_appendLog('ECHEC DE L ENVOI !'); 

		self::$nbProd = 0 ;
		self::$nbErreurs = 0;
	
	}	
	
	
	public function prodIsJeuxVideo($category)
	{
		if(  $category == "Jeu PlayStation 4" 
		   | $category == "Jeu Wii U" 
		   | $category == "Jeux  Wii" 
		   | $category == "Jeux PC" 
		   | $category == "Jeux Xbox 360"  
		   | $category == "Jeux PlayStation 3"  
		   | $category == "Jeux PSP" 
		   | $category == "Jeux DS" 
		   | $category == "Jeux 3DS" 
		   | $category == "Jeux PS Vita" 
		   | $category == "Jeu Xbox One" 
		   | $category == "Jeux MAC" ) {
		   	
			return true;
		}
		else{
			return false;	
		}
	}
	
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>