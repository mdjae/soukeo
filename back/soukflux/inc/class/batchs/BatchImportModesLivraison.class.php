<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchImportModesLivraison extends Batch {
	private $_name = "BatchImportModesLivraison";
	static protected $_description = "[IMPORT] [MODES LIVRAISON] Import des modes de livraison depuis la market-place";
	private static $db;
	private static $db2;
	private $listeid;

	/**
	 * Fonction executee par le batch
	 */
	public function work() {

		$temps_debut = microtime(true);
		self::$db = new BusinessAvahisModel();
		self::$db2 = new BusinessSoukeoModel();
		self::$db2 -> prepInsertLivraisonTypes();
		$shipping_method_list = self::$db -> getAllShippingMethod();

		$nb = 0;
		foreach ($shipping_method_list as $shippingmethod) {
			self::$db2 -> insertLivraisonTypes($shippingmethod);
			$nb++;
		}

		$this -> _appendLog('Nombre de modes de livraison importe : ' . $nb);
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
