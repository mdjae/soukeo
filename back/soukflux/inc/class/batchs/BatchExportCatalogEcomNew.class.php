<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch permettant l'export des stock prix vers un fichier CSV
 * On parcourt tous les vendeurs qui possèdent au moins un produit (en vente dans table product_ecom_stock_prix)
 * et un CSV référence chaque ligne avec passage du stock à 0 si la limite de produit par abonnement vendeur
 * est dépassée
 * @author Philippe_LA
 * @version 2 (Prise en compte des abonnements vendeurs et limite produits)
 */
class BatchExportCatalogEcomNew extends Batch {

	static protected $_description = "[EXPORT NEW] [ABO] -- Genération du CSV stock&prix";
	private $_name = "BatchExportCatalogEcomNew";
	private static $db;
	protected $cat_id;
	protected $cat_name;
	protected $vendor_id;
	protected $ArrCSV = array();
	protected static $nbSell = 0;
	//Nombre de produits exportés pour la catégorie
	protected static $nbSellStock = 0;
	protected $total_prod = 0;
	protected $diff = false;
	private $rep = "/opt/soukeo/export/qty_price/";

	/**
	 * Routine du batch
	 */
	public function work($vendor_id = "") {
		global $root_path;

		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
		$this -> _appendLog("\n");
		$this -> _appendLog("--------------------------------------------------------------------\n");

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();

		////////////////////////// ENTETE /////////////////////////////////////////////////////
		$champsFixes = $this -> getHeaderChpsFixes();
		$this -> ArrCSV['entete'] = $champsFixes;

		if ($vendor_id) {
			$this -> vendor_id = $vendor_id;
			$vendor_id_list = array();
			$vendor_id_list[] = array("VENDOR_ID" => $vendor_id);
		} else {
			$vendor_id_list = self::$db -> getAllDistinctVendors();
		}

		$vendorManager = new ManagerVendor();
		$abonnementManager = new ManagerAbonnement();

		foreach ($vendor_id_list as $vendor_id) {
			$vendor = $vendorManager -> getVendorByVendorId($vendor_id['VENDOR_ID']);
			if ($vendor -> getVendor_special_limit()) {
				$limit_abo = $vendor -> getVendor_special_limit();
			} elseif ($abonnement = $abonnementManager -> getAbonnementById($vendor -> getAbonnement_id())) {
				$limit_abo = $abonnement -> getAbonnement_limit_product();
			}

			//Filtre récupération des produits ?? Les plus chers,
			$sellingList = self::$db -> getAllStockPriceEcom($vendor_id['VENDOR_ID']);
			$this -> _appendLog('Vendor : ' . $vendor -> getVendor_id() . " - " . $vendor -> getVendor_nom());
			$this -> _appendLog('Abonnement : ' . $abonnement -> getAbonnement_label() . ' ( ' . $limit_abo . ' produits)');

			$cptProdVendu = 0;
			$cptProdsHorsLimit = 0;

			foreach ($sellingList as $sell) {

				if (self::$db -> verifVendorCSV($sell)) {
					if (($sell['QUANTITY'] > 0) && (!$limit_abo)) {
						$cptProdVendu++;
						self::$nbSellStock++;
					} elseif (($sell['QUANTITY'] > 0) && ($cptProdVendu < $limit_abo)) {
						$cptProdVendu++;
						self::$nbSellStock++;
					} elseif (($sell['QUANTITY'] > 0) && ($cptProdVendu >= $limit_abo)) {
						$cptProdsHorsLimit++;
						$sell['QUANTITY'] = 0;
					}
					self::$nbSell++;
					$champsFixes = $this -> getSellChpsFixes($sell);
					$this -> ArrCSV['sell' . self::$nbSell] = $champsFixes;
				}
			}
			
			$this -> _appendLog("");
			$this -> _appendLog("Nb produits en stock vendus : " . $cptProdVendu);
			$cptProdsHorsLimit ? $this -> _appendLog("Nb produits non vendus car hors limite de vente : " . $cptProdsHorsLimit . "\n") : $this -> _appendLog("");
			$this -> _appendLog("--------------------------------------------------------------------\n");
		}

		$this -> generateCsv();
		$this -> _appendLog('Nombre de produits total avec stock exportés : ' . self::$nbSellStock . ' sur ' . self::$nbSell . ' lignes ');
		$this -> _appendLog('--------------------------------------------------------------------');
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

		self::$nbSell = 0;
		unset($this -> ArrCSV);
	}

	/**
	 * Retourne un array contenant les labels des champs fixes
	 */
	protected function getHeaderChpsFixes() {
		return array("sku", "price", "qty", "udropship_vendor");
	}

	/**
	 * Fonctionnant retournant un array contenant les données pour les champs fixes
	 * du produit
	 */
	protected function getSellChpsFixes($sell) {
		if ($sell['SPECIAL_PRICE'] && $sell['SPECIAL_PRICE'] != 0) {
			$sell['PRICE_PRODUCT'] = $sell['SPECIAL_PRICE'];
		}
		return array($sell['id_produit'], $sell['PRICE_PRODUCT'], $sell['QUANTITY'], $sell['VENDOR_ID'], );
	}

	protected function generateCsv() {
		global $root_path;

		if ($this -> vendor_id != "") {
			$name = $this -> rep . "vendor" . $this -> vendor_id . "-stock_prix-" . date("d-m-Y") . "_" . date("H-i-s") . ".csv";
		} else {
			$name = $this -> rep . "stock_prix-" . date("d-m-Y") . "_" . date("H-i-s") . ".csv";
		}

		file_put_contents($name, "");
		$fp = fopen($name, "w");

		//Genere CSV
		foreach ($this->ArrCSV as $fields) {fputcsv($fp, $fields, ";");}

		fclose($fp);
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
?>