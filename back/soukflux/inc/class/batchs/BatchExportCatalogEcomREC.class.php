<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchExportCatalogEcomREC extends Batch{

	static protected 	$_description 	= "[EXPORT-REC] -- Genération du CSV stock&prix";
	private 			$_name 			= "BatchExportCatalogEcomREC";
	private static 		$db;
	protected 			$cat_id;
	protected 			$cat_name;
	protected 			$vendor_id;
	protected 			$ArrCSV = array();
	protected static 	$nbSell = 0; //Nombre de produits exportés pour la catégorie
	protected 			$total_prod = 0;
	
	protected $diff = false;
	
	private $rep = "/opt/soukeo/export_rec/qty_price/";
	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		global $root_path;
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("\n");
        $this -> _appendLog("\n\n--------------------------------------------------------------------\n");
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		
		//Toutes les ventes
		if($vendor_id){ 
			$this->vendor_id = $vendor_id; 
			$sellingList = self::$db->getAllStockPriceEcom($this -> vendor_id);
			
			$this -> _appendLog("\n\nVendor : " . $this -> vendor_id . " - ". self::$db -> getNomVendor($this -> vendor_id));
			$this -> _appendLog("");
			$this -> _appendLog("\n\n--------------------------------------------------------------------\n".PHP_EOL);
		}
		else{
			$sellingList = self::$db->getAllStockPriceEcomREC();			
		}
		
		////////////////////////// ENTETE /////////////////////////////////////////////////////
		$champsFixes = $this->getHeaderChpsFixes();
		$this->ArrCSV['entete'] = $champsFixes;
        
        if($vendor_id != ""){ 
            foreach($sellingList as $sell){

                self::$nbSell ++;
                $champsFixes = $this->getSellChpsFixes($sell) ;                             
                $this->ArrCSV['sell'.self::$nbSell] = $champsFixes;  
            }            
        }
        else{
            //mode AUTO
            foreach($sellingList as $sell){
                
                self::$nbSell ++;
                $champsFixes = $this->getSellChpsFixes($sell) ;                             
                $this->ArrCSV['sell'.self::$nbSell] = $champsFixes;
            }
                
        }

		$this->generateCsv();
		
		
		$this -> _appendLog('Nombre de produits en vente exportés : ' . self::$nbSell ."\n".PHP_EOL);
		$this -> _appendLog('--------------------------------------------------------------------');
		$temps_fin = microtime(true);	
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));	
		
		
		self::$nbSell = 0 ;
		unset($this -> ArrCSV);
	}	
	
	/**
	 * Retourne un array contenant les labels des champs fixes 
	 */
	protected function getHeaderChpsFixes()
	{
		return array(	"sku","price","qty","udropship_vendor");
	}

	
	/**
	 * Fonctionnant retournant un array contenant les données pour les champs fixes
	 * du produit
	 */
	protected function getSellChpsFixes($sell)
	{
        
		return array(	$sell['sku'],$sell['price'],5,24);
	}

	protected function generateCsv()
	{
		global $root_path;
		
		if($this->vendor_id != ""){
			$name = $this->rep."vendor".$this->vendor_id."-stock_prix-".date("d-m-Y")."_".date("H-i-s").".csv";	
		}else{
			$name = $this->rep."stock_prix-".date("d-m-Y")."_".date("H-i-s").".csv";	
		}

		file_put_contents($name , "");
		$fp = fopen($name, "w");

		//Genere CSV
		foreach ($this->ArrCSV as $fields) {fputcsv($fp, $fields, ";");}
		
		fclose($fp);
	}
	
    
    public function help()
    {
        $help =  PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL;
        $help .= "Ordre des arguments a entrer : ".PHP_EOL;
        $help .= "[id_vendeur OU \"\"] ".PHP_EOL.PHP_EOL;
        $help .= "Exemple : \n php /var/www/soukflux/common/batch/execBatch.php 66 24 ".PHP_EOL;
        $help .= "export stock prix vendeur 24".PHP_EOL;
        $help .= PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL.PHP_EOL;
        
        return $help;
    }
    	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>