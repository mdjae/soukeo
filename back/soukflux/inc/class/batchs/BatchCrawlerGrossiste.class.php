<?php

if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";

} else {
	require_once $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $root_path . "inc/offipse.inc.php";
}

/**
 * Batch dédié au Crawl de tous les grossiste Actif
 * @version 0.1.0
 * @author Philippe_LA
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */
class BatchCrawlerGrossiste extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchCrawlerGrossiste";
	static protected $_description = "[CRAWLER] -- Get data LDLC & MAGINEA";

	private static $db;
    private static $pictureManager; //L'outil permettant la sauvegarde d'images    

	private $deschtml = true;
	protected $local_url = "http://media.avahis.com/media/content/";

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		//Prepare stmt
		self::$db -> prepareInsertSfkProdAttrGR();
        self::$pictureManager = new BusinessPictureManager();

		$this -> _appendLog('--------- LDLC Crawl ---------');

		$this -> crawlLDLC();

		$this -> _appendLog('--------- MAGINEA Crawl ---------');

		$this -> crawlMAGINEA();

		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}

	protected function crawlLDLC() {
		$listeProd = self::$db -> getAllProdByVendorToCrawl(2);
		$numRep = 'g002';

		$context = $this -> getContextUserAgent();

		$nbProds = 0;
		// pour chaque produit dans la table grossiste et pour le grossiste LDLC
		foreach ($listeProd as $prod) {
			$sku = null;
			$nbProds++;

			$this -> _appendLog($nbProds . " - Produit id : " . $prod['ID_PRODUCT'] . " -- url : " . $prod['URL']);

			//GET HTML//
			$html = file_get_html($prod['URL'], false, $context);

			//Description Normale
			$descript = $html -> find(".description", 0) -> plaintext;
			$descript = str_replace(array("\r", "\n", "\r\n", "\t", "  "), "", $descript);
			$descript = implode(" ", explode(" ", $descript));
			$descript = html_entity_decode(BusinessEncoding::toUTF8($descript));

			//Description HTML
			$descripthtml = $html -> find(".description", 0) -> innertext;

			if ($this -> deschtml) {
				self::$db -> updateDescriptProduct($prod['ID_PRODUCT'], $descript);
			}

			//Récupération infos produit Avahis
			$sku = self::$db -> getSkuAssocProdGr($prod['ID_PRODUCT']);
			$prod_avahis = self::$db -> getProdAvahisAssocProdGr($prod['ID_PRODUCT']);
			$prod_avahis = $prod_avahis[0];

			//SUPPRESSION des balise de div commençant par <div style="border-bottom
			$marqueurDebutLien = '<div style="border';
			$marqueurFinLien = '</div>';

			$debutLien = stripos($descripthtml, $marqueurDebutLien) + strlen($marqueurDebutLien);
			$testFF = stripos($descripthtml, $marqueurDebutLien);

			$finLien = stripos($descripthtml, $marqueurFinLien, $debutLien);
			$leLien = substr($descripthtml, $debutLien, $finLien - $debutLien);
			if ($testFF != null) {
				$descripthtml = str_replace($leLien, "", $descripthtml);
			}

			//---------------------------------- Crawl des images
			$marqueurDebutLien = 'src="';
			$marqueurFinLien = '"';
			$mystring1 = $descripthtml;
			// on calcule le nombre de caractére dans le string
			$nbCara = strlen($mystring1);
			$noEnd = 1;
			$num = 0;

			// tant qu'on n'at pas parcourue tous les caractéres
			if ($this -> deschtml){
				while ($noEnd) {

					$debutLien = stripos($mystring1, $marqueurDebutLien) + strlen($marqueurDebutLien);
					$testFF = stripos($mystring1, $marqueurDebutLien);
					$finLien = stripos($mystring1, $marqueurFinLien, $debutLien);
					$leLien = substr($mystring1, $debutLien, $finLien - $debutLien);

					if ($testFF != null) {
						$mystring1 = substr($mystring1, $finLien);
						$ean = $prod['EAN'] . '_' . $num;
						// on sauvegarde sur media.avahis l'image et on retourne le nom et le type xxx.jpg
						$nameImage = $this -> saveImg($ean, $leLien, $numRep);
						$num++;

						if ($nameImage != null) {
							//on remplace l'adresse du site d'image :
							$descripthtml = str_replace($leLien, $this -> local_url . $nameImage, $descripthtml);
						}
					} else {
						$noEnd = 0;
					}
				}
            }
			// on update la description html du produit dans le catalog product
			if ($this -> deschtml) {
				self::$db -> updateDescriptProductCatProd($sku, $descripthtml);
				self::$db -> updateDescriptProductCatProdNotHtml($sku, $descript);
			}

			if (($sku != null) && ($prod['URL'] != null))
				self::$db -> insertToEanUrl($sku, $prod['URL']);

			/////////////////////////////////////////////////// GESTION ATTRIBUTS //////////////////////////////////////////////////////////////
			$this -> getAttributsLDLC($html, $prod, $prod_avahis);
            
            /////////////////////////////////////////////////// TELECHARGEMENT IMAGES /////////////////////////////////////////////////////////
            if($prod_avahis){
                self::$pictureManager -> savePictures($prod, $prod_avahis);
            }
            
			/////////////////////////////////////////////////// END CRAWL PRODUCT //////////////////////////////////////////////////////////////
			self::$db -> setCrawledProduct(2, $prod['ID_PRODUCT'], 1);
			$this -> _appendLog("");
		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");
		$temps_fin = microtime(true);
		echo "LDLC Durée : " . number_format($temps_fin - $temps_debut, 3);
	}

	/**
	 * Récupère les attributs LDLC dans le DOM ainsi que le poids pour
	 * ensuite mettre a jour ces informations dans les tables Grossiste ET les tables
	 * sfk_catalog_product représentant les produits avahis
	 *
	 * @param 	string 	$html 			adresse du produit
	 * @param 	array 	$prod 			Le produit Grossiste
	 * @param 	array 	$prod_avahis 	Le produit avahis associé
	 */
	public function getAttributsLDLC($html, $prod, $prod_avahis) {

		$attr = array();

		//Reset active = 0 pour RELATION product_attribute pour CE PRODUIT
		$log = self::$db -> resetProductAttrActive($prod_avahis['id_produit']);
		$this -> _appendLog($log ? $log . " Attributs Reset" : "0 attribut Reset");

		foreach ($html->find("font.legende" ) as $label) {

			$label_attr = html_entity_decode(BusinessEncoding::toUTF8(trim($label -> innertext)));

			$code_attr = str_replace(array(" ", "'"), "_", $label_attr);
			$code_attr = strtoupper(self::$db -> stripAccents($code_attr));

			$value = $label -> parent() -> parent() -> parent() -> next_sibling() -> firstChild() -> plaintext;
			$value = html_entity_decode(BusinessEncoding::toUTF8($value));

			if ($label_attr != "Présence LDLC") {
				$id_attr = self::$db -> checkIfAttributeExistsGR($code_attr, $label_attr);
				self::$db -> addRowSfkProdAttrGR($prod['ID_PRODUCT'], 2, $id_attr, $value);

				if ($prod_avahis) {

					if ($id_attr_av = self::$db -> checkIfAttributeExistsActive($code_attr, $prod_avahis['categories'], $label_attr)) {

						self::$db -> addAttributProduct($id_attr_av, $prod_avahis['id_produit'], $value);

						$this -> _appendLog("Attribut : " . $label_attr . " - Valeur : " . $value);
					}

				}

			}

			//////////////////////////////////////////////  GESTION WEIGHT  //////////////////////////////////////////////////////////////
			if (strtolower($label_attr) == "poids") {
				//Traitement du poids
				if (strpos($value, 'kg') !== false) {
					$value = trim(str_replace("kg", "", $value));
				} elseif (strpos($value, 'g') !== false) {
					$value = trim(str_replace("g", "", $value));
					$value = (float)$value / 1000;
				}
				//Update poids table grossite
				self::$db -> updatePoidsProduct($prod['ID_PRODUCT'], $value);
				//si le poids dans catalog product est supérieur a 0 on ne fait rien sinon
				if (self::$db -> verifPoidCatProdSupZero($product['EAN'])) {

				} else {
					//Update sfk_catalog_product
					if ($value > 0)
						self::$db -> updatePoidsProductCatProd($prod['EAN'], $value);
				}
			}
		}
	}

	protected function crawlMAGINEA() {
		$numRep = 'g003';

		$listeProd = self::$db -> getAllProdByVendorToCrawl(3, " ");

		$context = $this -> getContextUserAgent();

		$nbProds = 0;

		foreach ($listeProd as $prod) {

			$prod_avahis = null;
			$sku = null;

			$this -> _appendLog($nbProds . " - Produit id : " . $prod['ID_PRODUCT'] . " -- url : " . $prod['URL']);

			//GET HTML//
			$html = file_get_html($prod['URL'], false, $context);

			//Description Normale
			$descript = $html -> find("div#productFiche", 0) -> plaintext;
			$descript = str_replace(array("\r", "\n", "\r\n", "\t", "  "), "", $descript);
			$descript = implode(" ", explode(" ", $descript));
			$descript = html_entity_decode(BusinessEncoding::toUTF8($descript), ENT_QUOTES);

			//Description HTML
			$descripthtml = $html -> find("div#productFiche", 0) -> innertext;

			if ($this -> deschtml) {
				self::$db -> updateDescriptProduct($prod['ID_PRODUCT'], $descript);
			}

			//INFOS PRODUIT AVAHIS
			$sku = self::$db -> getSkuAssocProdGr($prod['ID_PRODUCT']);
			$prod_avahis = self::$db -> getProdAvahisAssocProdGr($prod['ID_PRODUCT']);
			$prod_avahis = $prod_avahis[0];

			//SUPPRESSION des balise de div commençant par <div style="border-bottom
			$marqueurDebutLien = '<div style="border';
			$marqueurFinLien = '</div>';

			$debutLien = stripos($descripthtml, $marqueurDebutLien) + strlen($marqueurDebutLien);
			$testFF = stripos($descripthtml, $marqueurDebutLien);

			$finLien = stripos($descripthtml, $marqueurFinLien, $debutLien);
			$leLien = substr($descripthtml, $debutLien, $finLien - $debutLien);
			// si il n'y a pas de div on n'enléve rien
			if ($testFF != null) {
				$descripthtml = str_replace($leLien, "", $descripthtml);
			}

			//-------------------------------- crawl des images
			$marqueurDebutLien = 'src="';
			$marqueurFinLien = '"';
			$mystring1 = $descripthtml;
			// on calcule le nombre de caractére dans le string
			$nbCara = strlen($mystring1);
			$noEnd = 1;
			$num = 0;

			// tant qu'on n'at pas parcourue tous les caractéres
			if ($this -> deschtml)
				while ($noEnd) {

					$debutLien = stripos($mystring1, $marqueurDebutLien) + strlen($marqueurDebutLien);
					$testFF = stripos($mystring1, $marqueurDebutLien);
					$finLien = stripos($mystring1, $marqueurFinLien, $debutLien);
					$leLien = substr($mystring1, $debutLien, $finLien - $debutLien);

					if ($testFF != null) {
						$mystring1 = substr($mystring1, $finLien);
						$ean = $prod['EAN'] . '_' . $num;
						// on sauvegarde sur media.avahis l'image et on retourne le nom et le type xxx.jpg
						$nameImage = $this -> saveImg($ean, $leLien, $numRep);
						$num++;

						if ($nameImage != null) {
							//on remplace l'adresse du site d'image :
							$descripthtml = str_replace($leLien, $this -> local_url . $nameImage, $descripthtml);
						}
					} else {
						$noEnd = 0;
					}
				}

			// on enlève les référence a maginéa
			$descript = str_replace(array("Maginéa", "MAGINEA", "maginea", "maginéa", "Maginea"), "Avahis", $descript);
			$descripthtml = str_replace(array("Maginéa", "MAGINEA", "maginea", "maginéa", "Maginea"), "Avahis", $descripthtml);

			// update des description avec et sans html

			if ($this -> deschtml)
				self::$db -> updateDescriptProductCatProd($sku, $descripthtml);
			if ($this -> deschtml)
				self::$db -> updateDescriptProductCatProdNotHtml($sku, $descript);

			//si le poid dans catalog product est supérieur a 0 on ne fait rien
			if (self::$db -> verifPoidCatProdSupZero($prod['EAN'])) {
			} else {
				//on rajoute le poid de Maginiea dans le catalogue produit
				self::$db -> updatetWeightToCatProd($prod['EAN'], str_replace(",", ".", str_replace(",", ".", $prod['WEIGHT'])));
			}

			//si l'ean et l'url n'est pas nul :
			if (($sku != null) && ($prod['URL'] != null)) {
				self::$db -> insertToEanUrl($sku, $prod['URL']);
			}

			//Tableau associatif qui comprendra à chaque index : le label, code et valeur de l'attribut
			$arrLabel = array();
			$i = 0;
			//Récupération des label des attributs
			foreach ($html->find("dl.productParameters dt" ) as $label) {

				$arrLabel[$i]['label'] = trim(html_entity_decode(BusinessEncoding::fixUTF8($label -> innertext), ENT_QUOTES));

				//On en profite pour créer le code attribut en Majuscules
				$code_attr = str_replace(array(" ", "'"), "_", $arrLabel[$i]['label']);
				$arrLabel[$i]['code'] = strtoupper(self::$db -> stripAccents($code_attr));
				$i++;
			}

			$i = 0;
			//Récupération des valeurs de ces attributs
			foreach ($html -> find("dl.productParameters dd") as $value) {
				$arrLabel[$i]['value'] = trim(html_entity_decode(BusinessEncoding::fixUTF8($value -> innertext)));
				$i++;
			}

			self::$db -> resetProductAttrActive($prod_avahis['id_produit']);
			//Utilisation du tableau attribut label / code / valeur pour insertion en BDD
			foreach ($arrLabel as $coupleAttVal) {

				if ($coupleAttVal['code'] != "POIDS") {
					//Si attribut existe on récupère ID sinon la fonction insère ce nouvel attribut et renvoie l'ID
					$id_attr = self::$db -> checkIfAttributeExistsGR($coupleAttVal['code'], $coupleAttVal['label']);

					self::$db -> addRowSfkProdAttrGR($prod['ID_PRODUCT'], 3, $id_attr, $coupleAttVal['value']);

					if ($prod_avahis) {

						if ($id_attr_av = self::$db -> checkIfAttributeExistsActive($coupleAttVal['code'], $prod_avahis['categories'], $coupleAttVal['label'])) {

							self::$db -> addAttributProduct($id_attr_av, $prod_avahis['id_produit'], $coupleAttVal['value']);

							$this -> _appendLog("Attribut : " . $coupleAttVal['label'] . " - Valeur : " . $coupleAttVal['value']);
						}
					}
				}
			}

			//MAJ description du produit
			if ($this -> deschtml)
				self::$db -> updateDescriptProduct($prod['ID_PRODUCT'], $descript);
            
            /////////////////////////////////////////////////// TELECHARGEMENT IMAGES /////////////////////////////////////////////////////////
            if($prod_avahis){
                self::$pictureManager -> savePictures($prod, $prod_avahis);
            }
            
			self::$db -> setCrawledProduct(3, $prod['ID_PRODUCT'], 1);
			$nbProds++;

		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
	}

	/**
	 * To Do
	 */
	protected function crawlPIXMANIA() {

	}

	/**
	 * To Do
	 */
	protected function crawlECOPRESTO() {

	}

	/**
	 * To Do
	 */
	protected function crawlFK() {

	}

	/**
	 * Permet de sauvegarder les images localement pour les descriptions
	 */
	protected function saveImg($name, $leLien, $numRep) {

		$nbrcaract = strlen($leLien) - 3;
		$typeImg = substr($leLien, $nbrcaract);
		$data = $leLien;

		if ($typeImg == 'jpg' || $typeImg == 'png' || $typeImg == 'gif') {

			$this -> _appendLog('la destination : var/www/media.avahis/media/content/' . $numRep . '/' . $name . '.' . $typeImg);
			$this -> _appendLog('l\'url : ' . $data);

			file_put_contents('/var/www/media.avahis/media/content/' . $numRep . '/' . $name . '.' . $typeImg, file_get_contents($data));

			return $numRep . '/' . $name . '.' . $typeImg;
		}
	}

	public function getContextUserAgent() {
		$opts = array('http' => array('method' => "GET", 'header' => "Accept-language: en\r\n" . "User-Agent: 	Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6\r\n" . "Cookie: foo=bar\r\n"));
		return stream_context_create($opts);
	}

	/**
	 * Permet de récupérer la description du programme
	 * @author
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
