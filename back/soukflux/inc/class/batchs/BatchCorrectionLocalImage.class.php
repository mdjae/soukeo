<?php 
class BatchCorrectionLocalImage extends Batch {
	
	
	static protected 	$_description 	= "[MAJ] [LOCAL_IMAGES] Batch Correction Local Images";
	protected 			$_name 			= "BatchCorrectionLocalImage";
	protected static 	$db;
	protected 			$path 			= "/var/www/media.avahis/media/catalog/product/";
	
	
	public function work()
	{
		self::$db = new BusinessSoukeoGrossisteModel();
		$listProdLocalImage = self::$db -> getAllProdLDLCLocalImage();
		self::$db -> prepUpdateLocalImage();
		
		foreach ($listProdLocalImage as $prod) {
			$namefile = explode("/", $prod['local_image']);
			$namefile = end($namefile);
			$namefile = $this -> path . $prod['categories'] . "/". $namefile ;
			
			if(filesize($namefile) < 20 * 1024 ){
				$this -> _appendLog("id_produit : ".$prod['id_produit']. " - ". filesize($namefile) . " bytes");
				self::$db -> updateLocalImage($prod['id_produit'], "");
			}
		}
	}
	
	
	
	/**
 	 * Permet de r�cup�rer la description du programme
 	 * @author tpo
 	 *
 	 */
	public static function getDescription() 
	{
		return self::$_description;
	}
	
}

 ?>