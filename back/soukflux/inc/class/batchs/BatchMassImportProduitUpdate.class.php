<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchMassImportProduitUpdate extends Batch {
	private $_name = 'BatchMassImportProduitUpdate';
	static protected $_description = '[AVAHIS-PROD] -- Chargement des fiches produits [UPDATE]';

	/*** Fonction executee par le batch */
	public function work() {
		$temps_debut = microtime(true);
		$this -> _appendLog('Batch ' . $this -> _name);
		$this -> _appendLog('');
		$this -> _appendLog('---------------------------------------------------------------------------------------------');

		$this -> _appendLog("$f" . PHP_EOL);
		echo exec('php /var/www/soukflux/thirdparty/magmi/cli/avahisMassImportProduitUpdate.php');

		//Log du temps d'execution
		$temps_fin = microtime(true);
		$this -> _appendLog(PHP_EOL . '================================================== ');
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		$this -> _appendLog(PHP_EOL . '================================================== ' . PHP_EOL);

	}

	/**
	 * Permet de recuperer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
