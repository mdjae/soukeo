<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

//require_once("/var/www/soukflux/inc/libs/log4php/Logger.php");

class BatchImportConfigAttributeRec extends Batch 
{
	 
	public $logger;
	
	private $_name = "BatchImportConfigAttributeRec";
	static protected $_description = "[AVAHIS-REC][EXPORT] La configuration des attributes -> Avahis";
	protected static $db;
	private $_debut;
	private $_fin;
	private $_dir = '/opt/soukeo/export/configs/';
	
	
	
	public function __construct() {
         //$this->logger = Logger::getLogger(__CLASS__);
		 //$this->logger = Logger::getLogger("main");
         //$this->logger->info('Logger principal ON <br/>');
	 	 //$this->logger->info("foo <br/>");
	 	 //$this->logger->warn("I'm not feeling so good...<br/>");
     }
	
	
	 /**
		 * Permet de récupérer la description du programme
		 * @author tpo
		 *
		 */
		public static function getDescription() {
		return self::$_description;
		}
	/**
	 * Fonction executee au debut du batch
	 */
	public function begin()
	{
		$this->_debut = microtime(true);
		$this -> _appendLog("Batch <br/>" . $this -> _name);
        $this -> _appendLog("--------------------------------------------------------------------------------------------- <br/>");
	}
	
	/**
	 * Fonction executee a la fin du batch
	 */
	public function end()
	{
		$this->_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($this->_fin - $this->_debut, 3));
	}
	
	
	
	/**
	 * Fonction executee par le batch
	 */
	public function work() 
	{
		$this->begin();
		
		//Connection base Avahis
		self::$db = new BusinessAvahisRecModel();
		
		$dir= '/opt/soukeo/export/configs/';
		$archives= $dir.'/archives';
	
	
		if ($hdle = opendir($dir))
		{
			$this -> _appendLog(' <br/> Lancement de l\'import des fichiers <br/>');
			
	        $time = date('d-m-Y').'_'.time();
	       // exec("mkdir $archives/$time");
	                
			while (false !== ($f = readdir($hdle))) 
			{
				if ($f != "." && $f != ".." && $f != "archives") 
				{
					
					$file = $dir.$f;
					$this -> _appendLog("$file");
					// read CSV
					$configs = self::$db->readCsv($file,';');
					
						//Foreach Element in csv update data
						foreach ($configs as $c) {
							//$this->logger->info('Configs attribut <br/>');
							
							$this -> _appendLog('Attribut : '.var_dump($c['frontend_label']));
							$attrData = self::$db->getAttributeInfoByCode($c['attribute_code']);
							
							//var_dump($attrData);
							//var_dump($attrData);
							
							$eavAttribute =  array(
								":entity_type_id" => $attrData['entity_type_id'],
								":attribute_code" => $attrData['attribute_code'],
								":frontend_input" => $c['frontend_input'],
								":frontend_label" => $c['frontend_label']
							);
							
							$eavAttributeLabel = array( 
								":label" => $c['frontend_label'],
								":attribute_id" => $attrData['attribute_id']
							);
							
							$catalogEavAttribute = array( 
								":is_filterable" => $c['is_filterable'],
								":attribute_id" => $attrData['attribute_id']
							);
							$this -> _appendLog("$eavAttribute");
							$this -> _appendLog("$eavAttributeLabel");
							$this -> _appendLog("$catalogEavAttribute");
							
							self::$db->updateEavAttribute($eavAttribute);
							self::$db->updateEavAttributeLabel($eavAttributeLabel);
							self::$db->updateCatalogEavAttribute($catalogEavAttribute);
						}
					
					exec("mv $dir/$f $archives/$time");
			    }
			}
		}
		
		closedir($hdle);
		$this->end();
	}
	
}
