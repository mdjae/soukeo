<?php 
/**
 * Exemple de classe ex�cut�e par le moteur de batch PHP d'offipse
 */
class Batch
{

	private $_name = "Batch";
	static protected $_description = "Batch Hello World";
	
	protected $_log_id = 0;
	protected $_prog_id = 0;
	protected $_return_code = 0;
	protected $_class = "";
	protected $_params = "";
	protected $_log = "";
	protected $_logfile = "";
	protected $_interactif = false;
	protected $_logname = "";
	
	/**
	 * Constructeur
	 * @author nsa
	 * @param int $prog_id Num�ro de la programmation dans le portail
	 * @param boolean $interactif Indique si le traitement est ex�cut� en int�ractif (depuis le portail) ou pas. Par d�faut, mode batch 
	 *
	 */
	public function __construct($prog_id, $interactif = false)
	{
		$this->_prog_id = $prog_id;
		$this->_interactif = $interactif;
		
        $db = new sdb();
        
		// R�cup�ration des infos sur le batch
		$query = "SELECT class, params
		FROM c_batch
		WHERE prog_id = " . $this->_prog_id;
		$res = dbQueryOne($query);
		//$this->_class = $res['class'];
		$this->_class = get_class($this);
		$this->_params = $res['params'];
		
		
		
		$query = "INSERT INTO c_batch_log (prog_id, class, start_date, status, log)
		VALUES (" . $prog_id . ",'" . $this->_class . "', now(), 'RUN', '' )";
		//dbQuery($query);
        $rs = $db->exec($query);
            
        // get id of the log
        $this->_log_id  = $db->lastInsertId($rs);
		//$this->_log_id = dbInsertId();

		// création du nom du fichier pour une r�utilisation ult�rieure
		$this->_logname = $this->_class . "_" . $this->_log_id . "_" . date('Ymd_hms') . ".log";
		
		//référencement du fichier en lui-m�me
		$this->_logfile = SystemParams::getParam("system*log_rep"). "/" . $this->_logname;
		
		// Si interactif, on a un affichage HTML
		if ($this->_interactif) echo "<pre>\n";
	}
	
	/**
	 * Execute le traitement
	 * @author nsa
	 *
	 */
	public function work()
	{
		$this->_appendLog("Hello World!");
		$this->_return_code = 0;
	}
	
	/**
	 * Destructeur
	 * @author nsa
	 *
	 */
	public function __destruct()
	{
		$query = "UPDATE c_batch_log
					SET end_date = now(),
					return_code = " . $this->_return_code . ",
					status = 'END',
					log = '" . $this->_logname . "'
					WHERE log_id = " . $this->_log_id;
		dbQuery($query);
		
		// Si interactif, on a un affichage HTML
		if ($this->_interactif) echo "</pre>\n";
	}
	
	/**
 	* Indique si il est n�cessaire d'ex�cuter le batch
	*/	
	public static function needBatchExecution($prog_id, $year, $month, $day, $hour)
	{
		//echo $hour . "\n";
		// On a besoin de pr�parer des �l�ments sur l'heure actuelle
		$curYear = date("Y");
		$curMonth = date("m");
		$curDay = date("d");
		$curHour = date("H");
	
		// Les infos du bach
		$batchYear = "";
		$batchMonth = "";
		$batchDay = "";
		$batchHour = "";
	
		// Constitution de l'heure d'ex�cution
	
		// Ann�e
		if ($year == "*") $batchYear = $curYear;
		else
		{
			// Les ann�es sont s�par�es par des virgules
			$tab = split(',',$year);
			if (in_array($curYear, $tab)) $batchYear = $curYear;
		}
	
		// Mois
		if ($month == "*") $batchMonth = $curMonth;
		else
		{
			// Les ann�es sont s�par�es par des virgules
			$tab = split(',',$month);
			if (in_array($curMonth, $tab)) $batchMonth = $curMonth;
		}
	
		// Jour
		if ($day == "*") $batchDay = $curDay;
		else
		{
			// Les ann�es sont s�par�es par des virgules
			$tab = split(',',$day);
			if (in_array($curDay, $tab)) $batchDay = $curDay;
		}
	
		// Heure
		if ($hour == "*") $batchHour = $curHour;
		else
		{
			// Les ann�es sont s�par�es par des virgules
			$tab = split(',',$hour);
			//print_r($curHour);
			if (in_array($curHour, $tab)) $batchHour = $curHour;
		}
		
		// Est-ce que ca vaut le coup d'aller plus loin ?
		if ($batchYear == "" || $batchMonth == "" || $batchDay == "" || $batchHour == "")
		{
			// C'est fini
			return false;
		}
		else
		{
			// Constitution d'une date
			$date1 = $batchYear . "-" . $batchMonth . "-" . $batchDay . " " . $batchHour . ":00:00";
			$date2 = $batchYear . "-" . $batchMonth . "-" . $batchDay . " " . $batchHour . ":59:59";
	
			// On regarde dans les logs si une execution a eu lieu
			$query = "SELECT count(1) as nb
			FROM c_batch_log
			WHERE prog_id = " . $prog_id . "
			AND start_date between '" . $date1 . "' and '" . $date2 . "'";
			//echo "\n" . $query . "\n";
			$res = dbQueryOne($query);
		
			if ($res['nb'] == 0) return true;
			else return false;
		}
	}
	
	/**
	  * Ajoute du texte � la log du batch
	 */
	protected function _appendLog($txt)
	{
		
		//$this->_log .= $txt;
		file_put_contents($this->_logfile, $txt. "\r\n", FILE_APPEND);
		echo $txt . "\r\n";
		
	}
	
	/**
	 * Fait partir un email avec la log du traitement
	 * @author nsa
	 *
	 */
	protected function _sendEmail()
	{
		// R�cup�ration des infos sur les traitement
	}
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author nsa
	 *
	 */
	public static function getDescription()
	{
		return self::$_description;
	}
}
?>
