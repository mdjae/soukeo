<?php 
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * 
 */
 
class BatchExportGrossiste extends Batch{

	static protected 	$_description 	= "[EXPORT] [CSV Grossiste] Batch d'export des produits grossistes en CSV";
	private 			$_name 			= "BatchExportGrossiste";
	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");

		$csv = new BusinessExportProdsNonVendus();
		
		//$csv -> makeContent();		
		//$csv -> generateCSV(";", true, false); // generateCSV($delimiter, $withBOM, $streaming)
		//$csv -> saveFileEntryInDB();
				
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	

	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>