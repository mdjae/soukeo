<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";

} else {

	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchCrawlerDWGrossiteToAvahis extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchCrawlerDWGrossiteToAvahis";
	static protected $_description = "[CRAWLER] [SYNCHRO DESCRHTML WEIGHT LDLC/MAGINEA]BatchCrawler LDLC et MAGINEA pour la description HTML et le Poid";

	private static $db;
	private static $pictureManager; //L'outil permettant la sauvegarde d'images    


	/**
	 * Fonction executee par le batch
	 */
	public function work() {
	    
		$temps_debut = microtime(true);
        

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		self::$db -> prepareInsertSfkProdAttrGR();
		self::$pictureManager     = new BusinessPictureManager();
		
		//*********************************************** LDLC ************************************************//
		//crawl en prod
		$listeProd = self::$db -> getAllProdByVendorToCrawl(2);
		//crawl de test a commenté par la suite
		$opts = array(
    		'http'=>array(
    			'method'=>"GET",
    			'header'=>"Accept-language: en\r\n" .
    			"User-Agent: 	Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6\r\n".
    			"Cookie: foo=bar\r\n"
    		)
    	);
    	
    	$context = stream_context_create($opts);
		$nbProds = 0;
		// pour chaque produit dans la table grossiste et pour le grossiste LDLC
		foreach ($listeProd as $prod) {
			$nbProds++;
			$sku = null;
			$this -> _appendLog($nbProds." - Produit id : ".$prod['ID_PRODUCT']." -- url : ".$prod['URL']);
			
			$html = file_get_html($prod['URL'], false , $context);
			
			$descript = $html -> find (".description", 0) -> plaintext ;
			$descripthtml =  $html -> find (".description", 0) -> innertext ;
			$descript = str_replace(array("\r", "\n", "\r\n", "\t", "  "), "", $descript);
			$descript = implode(" ", explode(" ", $descript));
			$descript = html_entity_decode(BusinessEncoding::toUTF8($descript));
			
			self::$db -> updateDescriptProduct($prod['ID_PRODUCT'] , $descript);
			$sku = $prod['EAN'];
			$this -> _appendLog($sku . " ean "); // pour affiché l'ean
			// on update la description html du produit dans le catalog product
			self::$db -> updateDescriptProductCatProd($sku , $descripthtml);
			self::$db -> updateDescriptProductCatProdNotHtml($sku , $descript);
			
			//on implémente la table ean_url
			//si l'ean et l'url n'est pas nul :
			if (($sku !=null)&&($prod['URL']!=null) )self::$db -> insertToEanUrl($sku , $prod['URL']);
			
			
			$attr = array();
			
			foreach ($html->find("font.legende" ) as $label) {
				
				
				$label_attr = html_entity_decode(BusinessEncoding::toUTF8(trim($label -> innertext)));
				
				$code_attr = str_replace(array(" ", "'"), "_", $label_attr);
				$code_attr = strtoupper(self::$db ->stripAccents($code_attr)); 
				
				$value = $label -> parent () -> parent () -> parent () -> next_sibling() -> firstChild() -> plaintext ;
				
				$value = html_entity_decode(BusinessEncoding::toUTF8($value));
				
			
				
				if(strtolower($label_attr) == "poids"){
					//Traitement du poids
					if (strpos($value,'kg') !== false) {
					    $value = trim(str_replace("kg", "", $value));
					}elseif(strpos($value,'g') !== false){
						$value = trim(str_replace("g", "", $value));
						$value = (float)$value / 1000;
					}
					
					// si le poid est supérieur a zero on update dans la table catalog product
					if ($value>0) self::$db -> updatePoidsProductCatProd($prod['EAN'], $value);
					//$this -> _appendLog($value . " :poid "); // pour affiché le poid 
				
				}				
			}
			
            if($prodAv = getProdAvahisAssocProdGr($prod['ID_PRODUCT'])){
                self::$pictureManager -> savePictures($prod, $prodAv);
            }
		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
		//************************************** Maginea ***********************************************//
		
		// crawl en prod
		$listeProd = self::$db -> getAllProdByVendorToCrawl(3, " ");

		$opts = array(
    		'http'=>array(
    			'method'=>"GET",
    			'header'=>"Accept-language: en\r\n" .
    			"User-Agent: 	Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6\r\n".
    			"Cookie: foo=bar\r\n"
    		)
    	);
    	$context = stream_context_create($opts);
		
		$nbProds = 0;
		foreach ($listeProd as $prod) {
			$this -> _appendLog($nbProds." - Produit id : ".$prod['ID_PRODUCT']." -- url : ".$prod['URL']);
			
			$html = file_get_html($prod['URL'], false , $context);
			
			//récupération de la description
			$descript = $html -> find ("div#productFiche", 0) -> plaintext ;
			// récupération de la description en html
			$descripthtml =  $html -> find ("div#productFiche", 0) -> innertext ;
			
			$descript = str_replace(array("\r", "\n", "\r\n", "\t", "  "), "", $descript);
			$descript = implode(" ", explode(" ", $descript));
			$descript = html_entity_decode(BusinessEncoding::toUTF8($descript), ENT_QUOTES);
			
			// update de la description dans la table catalog prod
			self::$db -> updateDescriptProduct($prod['ID_PRODUCT'] , $descript);
			$sku = $prod['EAN'];
            
			// on update la description html du produit dans le catalog product
			$descripthtml = str_replace(array("Maginéa","MAGINEA","maginea","maginéa", "Maginea"), "Avahis", $descripthtml);
			self::$db -> updateDescriptProductCatProd($sku , $descripthtml);
			$descript = str_replace(array("Maginéa","MAGINEA","maginea","maginéa", "Maginea"), "Avahis", $descript);
			self::$db -> updateDescriptProductCatProdNotHtml($sku , $descript);
            
			//si le poid dans catalog product est supérieur a 0 on ne fait rien 
 			if(self::$db->verifPoidCatProdSupZero($prod['EAN'])){
 			    
 			}
			else{
 			//sinon on rajoute le poid de Maginiea dans le catalogue produit
	 			self::$db->updatetWeightToCatProd($prod['EAN'],$prod['WEIGHT']);
	 			$this -> _appendLog(('EAN : ' . $prod['EAN'] ) );
	 			$this -> _appendLog(('WEIGHT : ' . $prod['WEIGHT'] ) );
			}
			
			
			//on implémente la table ean_url
			//si l'ean et l'url n'est pas nul :
			if (($sku !=null)&&($prod['URL']!=null) )self::$db -> insertToEanUrl($sku , $prod['URL']);
		
			$nbProds++;
		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		
	}



	/**
	 * Permet de récupérer la description du programme
	 * @author 
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
