<?php 
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	require_once $root_path . "inc/offipse.inc.php";
}


/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchExportAttrCategories extends Batch{

	static protected 	$_description 	= "[EXPORT] [ATTRIBUTS] Batch d'export des attributs par catégories";
	private 			$_name 			= "BatchExportAttrCategories";
	
	private static 		$db;
	protected 			$current_univers;
	protected 			$cat_id;
	protected 			$cat_name;
	
	protected 			$ArrCSV 	= array();
	
	protected static 	$nbProd 	= 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs 	= 0; //Nombre d'erreurs d'export produit car poids vide
	protected 			$total_prod = 0;
		
	private 			$rep 		= "/opt/soukeo/export/attributs/";
	
	
	/**
	 * Routine du batch
	 * @param 	String 	$vendor_id 	 Optionnel contenant l'id du vendeur permettant alors de lancer ce batch
	 * pour un vendeur en particulier.
	 */
	public function work($univers_id="")
	{
	    $categorieManager = new ManagerCategorie();
        
        //Univers individuel avec cat_id passé en argument batch
        if($univers_id != ""){
            
            if($cat = $categorieManager -> getBy(array("cat_id" => $univers_id))){
                    
                
                $this->_appendLog("Export categorie unique ".$cat->getCat_label());
                    
                $export = new BusinessExportAttributsFromUnivers($cat); //Attent Objet catégorie

                $export -> makeContent();
                $export -> generateCSV(";", true);
                
                $this->_appendLog("FICHIER UNIVERS : ".$cat->getCat_label(). "  ----  ".$export->getFileName());
                    
            }else{
                $this -> _appendLog("Numero de categorie inexistant ! ");
                exit;
            }
                
        }
        //Tous les univers dans des fichiers séparés
        else{
            $cat_list = $categorieManager -> getAll(array("parent_id" => 2, "cat_active" => 1));
            
            $this->_appendLog("Export de tous les univers");
            foreach ($cat_list as $cat) {
                
                $export = new BusinessExportAttributsFromUnivers($cat); //Attent Objet catégorie
                $export -> makeContent();
                $export -> generateCSV(";", true);
                
                $this->_appendLog("FICHIER UNIVERS : ".$cat->getCat_label(). "  ----  ".$export->getFileName());
            }
        }

		$this -> _appendLog('Nombre de produits total '.$this->total_prod);
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));	
		
	}	

	
    public function help()
    {
        $help = PHP_EOL."------------------------------------- HELP ------------------------------------- ".PHP_EOL;
        $help .= "Ordre des arguments a entrer : ".PHP_EOL;
        $help .= "[id_vendeur OU \"\"] [differientiel = 0 ou 1] [mode = \"create\" ou \"update\"] [id_categorie = int ou \"\"] ".PHP_EOL.PHP_EOL;
        $help .= "Exemple : \n php /var/www/soukflux/common/batch/execBatch.php 11 24 1 \"update\" 2305".PHP_EOL;
        $help .= "export vendeur 24 en differentiel mode update pour categorie 2305".PHP_EOL;
        $help .= "------------------------------------- HELP ------------------------------------- ".PHP_EOL.PHP_EOL;
        
        return $help;
    }
	
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>