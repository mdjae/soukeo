<?php 
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch permettant la synchronisation des poids du catalogue Pixmania vers le catalogue Avahis
 * Pour chaque produit Pixmania dont le poid est différent de 0
 * Si le poid d'un produit Avahis est a 0 on la remplaçe avec celle de pixmania
 * @author hkcfdc
 */
class BatchSyncWeightPixmania extends Batch{

	static protected 	$_description 	= "[SYNCHRO WEIGHT PIMANIA] Batch de synchronisation des poids Pixmania vers catalogue product";
	private 			$_name 			= "BatchSyncWeightPixmania";
	
	private static 		$db;

	protected static 	$nbProd = 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide

	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		
		////////////////////////// CONTENU // PRODUITS////////////////////////////////////////
		// sélection des produits 
		
		//Phase de prod
		$productList = self::$db->getAllProdByVendorComplete(4,'',1);
		
		//Phase de test
		//$productList = self::$db->getAllProdByVendorComplete(4,'limit 0,10',1);
				
		foreach ($productList as $product) {
			//$product = $this -> getValeurCalcul($product);
			//self::$db -> updateValCalculProductGrossiste($product);
 			
 			//si le poid dans catalog product est supérieur a 0 on ne fait rien sinon
 			if(self::$db->verifPoidCatProdSupZero($product['EAN'])){}
			else{
 			//on rajoute le poid de pixmania dans le catalogue produit
	 			self::$db->updatetWeightToCatProd($product['EAN'],$product['WEIGHT']);
	 			$this -> _appendLog(('EAN : ' . $product['EAN'] ) );
	 			$this -> _appendLog(('WEIGHT : ' . $product['WEIGHT'] ) );
				
			 	self::$nbProd ++;
			}
		}

		$this -> _appendLog(('Nombre de produits mis à jours : ' . self::$nbProd ) );

		self::$nbProd = 0 ;
		self::$nbErreurs = 0;

		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	
	

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>