<?php 

/**
 * 
 */
class BatchVindirectCleanHtml extends Batch {

    static protected    $_description   = "[MAJ][VINDIRECT][HTML] Batch de mise à jour des produits Vindirect nettoyage HTML ";
    private             $_name          = "BatchVindirectCleanHtml";    
    
    static protected    $db;
    static protected    $productManager;
    
    public function work () {
        
        $temps_debut = microtime(true);
        $this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
        
        self::$productManager = new ManagerCatalogProduct();
        
        $product_list = self::$productManager->getAll(array("dropship_vendor" => 26));
        
        foreach ($product_list as $product) {
            
            $product -> setDescription($this->cleanHtml($product->getDescription()));
            $product -> setShort_description($this->cleanHtml($product->getShort_description()));
            $product -> setDescription_html($this->cleanHtml($product->getDescription_html()));
            
            self::$productManager -> save($product);
        }

        
        $temps_fin = microtime(true);
        $this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3)); 
    }


    /**
     * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
     * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
     * les entêtes (les noms) de tous les attributs exportés.
     * @param string    $file           Le fichier CSV
     */
    protected function cleanHtml($str)
    {
        $in = array("&egrave,",
                    "&eacute,",
                    "&icirc,",
                    "&ocirc,",
                    "&acirc,",
                    "&ecirc,",
                    "&#39,",
                    "&rsquo,",
                    "&agrave,");
                    
        $out = array("è",
                     "é",
                     "î",
                     "î",
                     "â",
                     "ê",
                     "'",
                     "'",
                     "à");
        
        $str = str_replace($in, $out, $str);
        
        return $str;
    }

        /**
     * Permet de r�cup�rer la description du programme
     * @author tpo
     *
     */
    public static function getDescription() {
        return self::$_description;
    }
    
}


?>