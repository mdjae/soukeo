<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";

} else {

	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchCrawlerMAGINEA extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchCrawlerMAGINEA";
	static protected $_description = "[CRAWLER] [LDLC] pour infos completes Maginea";

	private static $db;


	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);


		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		//Prepare stmt pour insertion des attributs
		self::$db -> prepareInsertSfkProdAttrGR();
		
		//On récupère tous les produits de Maginea
		//$listeProd = self::$db -> getAllProdByVendorComplete(3, " LIMIT 0, 2");
		// crawl en prod
		$listeProd = self::$db -> getAllProdByVendorToCrawl(3, " ");
		//crawl de test a commenté par la suite
		//$listeProd = self::$db -> getAllProdByVendorToCrawlTest(3);

		$opts = array(
    		'http'=>array(
    			'method'=>"GET",
    			'header'=>"Accept-language: en\r\n" .
    			"User-Agent: 	Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6\r\n".
    			"Cookie: foo=bar\r\n"
    		)
    	);
    	$context = stream_context_create($opts);
		
		$nbProds = 0;
		foreach ($listeProd as $prod) {
			$this -> _appendLog($nbProds." - Produit id : ".$prod['ID_PRODUCT']." -- url : ".$prod['URL']);
			
			$html = file_get_html($prod['URL'], false , $context);
			
			//récupération de la description
			$descript = $html -> find ("div#productFiche", 0) -> plaintext ;
			// récupération de la description en html
			$descripthtml =  $html -> find ("div#productFiche", 0) -> innertext ;
			
			$descript = str_replace(array("\r", "\n", "\r\n", "\t", "  "), "", $descript);
			$descript = implode(" ", explode(" ", $descript));
			$descript = html_entity_decode(BusinessEncoding::toUTF8($descript), ENT_QUOTES);
			
			// update de la description dans la table catalog prod
			self::$db -> updateDescriptProduct($prod['ID_PRODUCT'] , $descript);
			$sku = $prod['EAN'];
			//$this -> _appendLog($sku . " ean "); // pour affiché l'ean
			// on update la description html du produit dans le catalog product
			$descripthtml = str_replace(array("Maginéa","MAGINEA","maginea","maginéa", "Maginea"), "Avahis", $descripthtml);
			self::$db -> updateDescriptProductCatProd($sku , $descripthtml);
			$descript = str_replace(array("Maginéa","MAGINEA","maginea","maginéa", "Maginea"), "Avahis", $descript);
			self::$db -> updateDescriptProductCatProdNotHtml($sku , $descript);
			//si le poid dans catalog product est supérieur a 0 on ne fait rien
 			if(self::$db->verifPoidCatProdSupZero($prod['EAN'])){}
			else{
 			//on rajoute le poid de Maginiea dans le catalogue produit
	 			self::$db->updatetWeightToCatProd($prod['EAN'],$prod['WEIGHT']);
	 			$this -> _appendLog(('EAN : ' . $prod['EAN'] ) );
	 			$this -> _appendLog(('WEIGHT : ' . $prod['WEIGHT'] ) );
			}
			
			
			//on implémente la table ean_url
			//si l'ean et l'url n'est pas nul :
			if (($sku !=null)&&($prod['URL']!=null) )self::$db -> insertToEanUrl($sku , $prod['URL']);
			
			
			//Tableau associatif qui comprendra à chaque index le label, code et valeur de l'attribut
			$arrLabel = array();
			
			$i = 0;		
			//récupération des label des attributs
			foreach ($html->find("dl.productParameters dt" ) as $label) {
				
				$arrLabel [$i]['label'] = trim(html_entity_decode(BusinessEncoding::fixUTF8($label -> innertext), ENT_QUOTES));
				//on en profite pour créer le code attribut en Majuscules
				$code_attr = str_replace(array(" ", "'"), "_", $arrLabel [$i]['label']);
				
				$arrLabel [$i]['code'] = strtoupper(self::$db ->stripAccents($code_attr)) ;
				
				$i ++; 
			} 	
			
			$i = 0;
			//récupération des valeurs de ces attributs
			foreach ($html -> find("dl.productParameters dd") as $value) {
				
				$arrLabel[$i]['value'] = trim(html_entity_decode(BusinessEncoding::fixUTF8($value -> innertext)));
				
				$i++;
			} 
			
			//utilisation du tableau attribut label / code / valeur pour insertion en BDD
			foreach ($arrLabel as $coupleAttVal) {
				
				//Si attribut existe on récupère ID sinon la fonction insère ce nouvel attribut et renvoie l'ID
				$id_attr = self::$db -> checkIfAttributeExistsGR($coupleAttVal['code'], $coupleAttVal ['label']);
				
				self::$db -> addRowSfkProdAttrGR($prod['ID_PRODUCT'], 3, $id_attr, $coupleAttVal['value']);
				
			}
			
			//MAJ description du produit 
			self::$db -> updateDescriptProduct($prod['ID_PRODUCT'] , $descript);
			
			self::$db -> setCrawledProduct(3, $prod['ID_PRODUCT'], 1);
			$nbProds++;
			
		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}



	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
