<?php
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $main_path . "/" . $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	

} else {

	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	//require_once $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchCrawler extends Batch {

	private $_name = "BatchCrawler";
	static protected $_description = "[CRAWLER] BatchCrawler GEN";

	private static $db;

	protected $cat_id;

	protected $rooturl = "http://www.education.gouv.fr";
	
	protected $Aattr = array();
	protected $arrCSV = array();
	//Tableau de fiches produit

	protected $curProd;
	protected $curCat;

	//ID DE LA CATEGORIE EN COURS DE TRAITEMENT

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);

		$this -> cat_id = '';
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoCrawlerModel();
		//Prepare stmt
		//self::$db -> prepareInsertSfkCatProduct();
		//self::$db -> prepareInsertSfkAttribute();
		//self::$db -> prepareInsertSfkProductAttribute();

		/*

		 * OUT DB SKFLUX
		 *
		 * Table : sfk_catalog_product
		 * Chps sfkcp		-->		champs CRAWLER
		 id_produit			--> 	AUTO GEN
		 sku	 			--> 	EAN -->
		 categories			-->		FROM SCRATCH
		 name				--> 	NOM
		 description 		--> 	CONCAT NOM ?
		 short_description 	--> 	CONCAT NOM ?
		 price	 			--> 	price
		 weight				--> 	Poids
		 country_of_manufacture --> 	null
		 meta_description	--> 	null
		 meta_keyword	 	--> 	null
		 meta_title			--> 	meta_title
		 image				--> 	image
		 small_image			--> 	null
		 thumbnail			--> 	null
		 attribute_set		--> 	null
		 date_create			--> 	now()
		 date_update			--> 	now()
		 *
		 */

		$nbtocheck = 999;
		$nbSousCatToCheck = 999;

		//$aurl = array("http://www.achetezle.fr/electromenager/");
		
		$this->assocCatCrawler();
		file_put_contents("/var/www/test.tmp", "");
			
		$fp = fopen("/var/www/test.tmp", "w+");

 $aurl  = array("http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=0",
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=1",
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=2",
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=3",
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=4",
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=5",
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=6", 
 "http://www.education.gouv.fr/pid24302/annuaire-resultat-recherche.html?college=2&lycee=3&erea=5&cio=5&lycee_name=&localisation=3&ville_name=&acad_select[]=28&public=1&prive=2&page=7"
 );
		$listetab = array();
		foreach ($aurl as $u) {
			$html2 = file_get_html($u);
			$i = 1;
			foreach ($html2->find("div.annuaire-etablissement-label" ) as $e) {
				$nom = $e -> find('a', 0) -> innertext;
				$test = $e -> find('a', 0) -> href;
				
				$urleta = $this->rooturl . $test ;
				$html3 = file_get_html($urleta);
				
				
				$nom = $html3 -> find('h2.titre-fiche-nom-etab', 0) -> innertext;
				$type  = $html3 -> find('div.titre-fiche-type-etab', 0) -> innertext;
				$type  = html_entity_decode($type  );
				
				$infos = $html3 -> find("div.annuaire-etablissement-infos", 0) -> innertext;
				
				
				$commune = $html3 -> find("span.nom-commune", 0) -> innertext;
				var_dump($html3 -> find("span.nom-commune", 0) ->prev_sibling ()  -> innertext  );	
				$test = split('<br/>', $infos);
				
				
				if (is_array($test)){
					foreach($test as $k=>$v ){
						if( strpos($v,'Code &eacute;tablissement') !== false   ){
								//var_dump($v);
							$temp = split(':', $v);
							
							$code_etab = trim($temp[1]);
							$mail = 'ce.'.$code_etab."@ac-reunion.fr";
							var_dump($nom);
							var_dump($type);
							var_dump($code_etab);
							var_dump($commune);
							var_dump($mail);
							
							
							
							$line =  array($nom,
										$type,
										$code_etab,
										$commune,
										$mail
									
										);
							
						//	$this -> arrCSV[] = $line;
						
						 fputcsv($fp, $line, ";");
							}

					}
	
				}
				$t = array_search("Code &eacute;tablissement", $test);

			}
		}

		//foreach ($this -> arrCSV as $line) {
		 //   fputcsv($fp, $line, ";");
		//}

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}
		
		
		
		
		
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
					
					
					
					
				foreach ($ul->children() as $e) {
					$cat = $e -> find('a', 0) -> innertext;
					
					//echo "\n==================================Begin cat  $cat {$i}==========================\n";
					$mycat = $cat . "/";

					//echo "\n==================================Begin Sous - cat ==========================\n\n\n";
					foreach ($e->find("ul li ") as $a) {
						$url = $a -> find('a', 0) -> href;
						$this -> curCat = $a -> find('a', 0) -> innertext;

						//	echo "********************* Sous cat " . ($this -> curCat) ;
						$catachetez = "$cat / $this->curCat";
						$sql = "select avahis_cat_id From mappingcrawler where cat_mp  = '$catachetez' and id_site = 1";
						if ($res = self::$db -> getone($sql)){
							echo "CAT UTILISER  : ". $res . "<br/>";
							$this -> cat_id = $res;
						}else{
							echo "<br/>ERROR  == -> NO CAT REJECT <br/>";
							break;
						}
						$url = explode(',', $url);

						//TRAITEMENT DES PAGES PRODUITS :
						 for ($i = 1; $i <= $nbtocheck; $i++) {
							 $uri = $url[0] . ',' . $url[1] . ',' . $i . ".htm";
						 	echo $uri . "\n";
						 	//Accès a la fiche produit
						 	if( $this -> getInfoProduit($uri) == "ALT" ){
						 		break;
						 	}

						 	if( ($i % 3)  == 1  ) {
						 		echo '-- sleep --\n';
						 		sleep(rand(1, 3));
						 	}
						 }
						//	echo "\n==================================End Sous - cat==========================\n\n";
					}
					//	echo "\n==================================END cat==========================\n\n";
					$i++;
					if ($i > $nbSousCatToCheck) {
						break;
					}
				}
			}
		 */
		
		

	function assocCatCrawler(){
		$sql = "update mappingcrawler
				set avahis_cat_id = (select cat_id from sfk_categorie where mappingcrawler.catavahis = sfk_categorie.cat_label group by cat_label)
				where  exists (select cat_id from sfk_categorie where mappingcrawler.catavahis = sfk_categorie.cat_label group by cat_label)";
				
		self::$db->exec($sql);
	}
	
	function getInfoProduit($u) {
		$url = $this -> rooturl . $u;
		$html = file_get_html($url);
		
		if( is_null($html->find('td.description', 0) ) ){
			return "ALT";
		}
		foreach ($html->find('td.description') as $desc) {
			$minidesc = $desc -> find('span', 0) -> innertext;
			if ($minidesc != '') {
				$this -> curProd = $desc -> find('a', 0) -> innertext;
				echo "\nNOM PRODUIT : " . $desc -> find('a', 0) -> innertext . "\n";

				//Nom Prod
				$this -> Aattr[$this -> curProd]['nom'] = $desc -> find('a', 0) -> innertext;

				//Desc Prod
				$this -> Aattr[$this -> curProd]['desc'] = $this -> curCat . ", " . $this -> Aattr[$this -> curProd]['nom'];

				$this -> getpartDescrition($this -> rooturl . $desc -> find('a', 0) -> href);

				$tmp = explode(',', $desc -> find('a', 0) -> href);
				$idprod = explode('.', end($tmp));

				$this -> getlargedescription($this -> constructurldescproduit($idprod[0]));

				$produit = $this -> Aattr[$this -> curProd];

				if (isset($produit['ean']) && $produit['ean'] != "") {

					//Insertion du produit avec tous ses champs "basique" 
					//var_dump($produit);			 
					self::$db -> addRowSfkCatProduct($produit, $this -> cat_id);

					//Besoin de l'id produit inseré ou mis à jour pour les relation attr/prod
					$idProd = self::$db -> getIdProduct($produit['ean']);

					//Récupération des attributs du produit
					$listeAttributsProduit = $this -> getAttributsProduct($produit);

					//boucle Attributs
					foreach ($listeAttributsProduit as $attr => $value) {

						//Si c'est un attribut déja connu on récupère son ID
						$idAttr = self::$db -> checkIfAttributeExistsPS($attr, $this -> cat_id);

						//Sinon on insère cet attribut et on récupère le lastInsertId
						if ($idAttr == false) {
							self::$db -> addRowSfkAttr($attr, $this -> cat_id);
							$idAttr = self::$db -> lastInsertId();
						}

						//Ajout de la ligne de relation entre ce produit et cet attribut
						self::$db -> addRowSfkProdAttr($idProd, $idAttr, $value, $this -> cat_id);
					}
				}
				unset($this -> Aattr[$this -> curProd]);
				echo "\n";
			}
		}
		return "OK";
	}

	function constructurldescproduit($idprod) {
		$uridesclarge = "http://www.achetezle.fr/electromenager/specs,";
		return $uridesclarge . $idprod . ".htm";

	}

	function getpartDescrition($url) {

		$html = file_get_html($url);
		if ($html) {
			foreach ($html->find('.pdtInfos') as $elm)
				foreach ($elm -> find ("li") as $e) {
					$temp = strip_tags($e -> innertext);
					$atemp = explode(':', $temp);
					if (trim($atemp[0]) != 'reference')
						$this -> Aattr[$this -> curProd][strtolower(trim($atemp[0]))] = trim($atemp[1]);

				}
				
			$this -> Aattr[$this -> curProd]['image'] = $html -> find(".pdtPhoto img", 0) -> src;
			$this -> Aattr[$this -> curProd]['meta_title'] = $html -> find("title", 0) -> innertext;

			//Price product
			$price = $html -> find(".price b", 0) -> innertext;
			$price = filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);
			$this -> Aattr[$this -> curProd]['price'] = $price;
		}

	}

	function getlargedescription($url) {
		$html = file_get_html($url);
		if ($html) {
			foreach ($html->find("td.name" ) as $elm) {
				$this -> Aattr[$this -> curProd][strtolower(trim($elm -> innertext))] = trim($elm -> nextsibling() -> innertext);
			}
		}
	}

	protected function ChecKAndUpdateAtt($arr) {
		$sqlT = "select code_attr as ID from sfk_attribut where cat_id= '$this->cat_id' ";
		$attrUsed = $this -> formatarr(self::$db -> getAll($sqlT));
		//Don t want titre in ATTR
		$attrUsed[] = 'titre';

		foreach ($arr as $k => $v) {
			if (!in_array($k, $attrUsed)) {
				if ($k == 'type')
					$k = "format";
				$A['cat_id'] = $this -> cat_id;
				$A['code_attr'] = $k;
				$A['label_attr'] = $k;
				$this -> addAttribut($A);
			}
		}
	}

	/**
	 * Fonction permettant de ne garder que les champs attributs d'un produit crawlé
	 */
	protected function getAttributsProduct($produit) {
		//On enleve les champs basiques du tableau pour n'avoir que les attributs dynamiques
		
		unset($produit['nom']);
		unset($produit['marque']);
		//unset($produit['référence']);
		//unset($produit['ean']);
		unset($produit['image']);
		unset($produit['meta_title']);
		unset($produit['price']);
		unset($produit['desc']);
		if (isset($produit['poids']))
			unset($produit['poids']);

		return $produit;
	}

	/**
	 * Permet de récupérer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
