<?php 
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	require_once $main_path . "/" . $root_path . 'thirdparty/swiftmail/lib/swift_required.php';
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	require_once $root_path . "inc/offipse.inc.php";
	require_once $root_path . 'thirdparty/swiftmail/lib/swift_required.php';
}
/**
 * Batch permettant l'export des fiches produits valides de soukflux vers un fichier CSV
 * On parcourt toutes les catégories qui posèdent au moins un produit (si possible aprè uniquement les catégories
 * qui n'ont pas d'enfants) et un .CSV est crée pour chaque catégories
 * @author Philippe_LA
 */
class BatchDeleteOldPictures extends Batch{

	static protected 	$_description 	= "[DEL] [PICTURE] Réference images obsolètes sur serveur";
	private 			$_name 			= "BatchDeleteOldPictures";
	
	private static 		$db;
 
	private  $nbProdObs 	= 0; //Nombre de produits obsolete
	private  $nbProdOk 	= 0;
	/**
	 * Routine du batch
	 */
	public function work()
	{
		$this -> nbProdObs = 0;
		$this -> nbProdOk  = 0;
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		
		$handle = fopen('/home/test.test', 'r');
		file_put_contents('/home/result.test' , "");
		$fp = fopen('/home/result.test', "w+");
		if ($handle)
		{
			/*Tant que l'on est pas à la fin du fichier*/
			while (!feof($handle))
			{
				$buffer = fgets($handle);
				
				$tmp = explode("/", $buffer);
				
				$pic_file = end($buffer);
				
				$id = explode("_", $pic_file);
				$id = $id[3]; 
			
				if($prod = self::$db -> getProductWithPicture($pic_file, $id)){
					echo "Images OK".PHP_EOL;
					$this -> nbProdOk++;
				}else{
					$this -> nbProdObs++;
					fputs($fp, $buffer);
					echo "Image Obsolete".PHP_EOL;;
				}
			}
			fclose($handle);
			fclose($fp);
		}
		
		$this -> _appendLog($this -> nbProdOk . " images OK");
		$this -> _appendLog($this -> nbProdObs . " images Obsoletes");
		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	
	


	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>