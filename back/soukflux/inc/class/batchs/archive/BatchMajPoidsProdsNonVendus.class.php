<?php 

/**
 * 
 */
class BatchMajPoidsProdsNonVendus extends Batch {

	static protected 	$_description 	= "[MAJ] [POIDS PRODUITS NON VENDUS] Batch de mise à jour des poids produits non vendus ";
	private 			$_name 			= "BatchMajPoidsProdsNonVendus";	
	
	static protected 	$db;
	protected 			$dirName = "/opt/soukeo/import/tmp";
	protected 			$delimiter = ";";
	
	public function work () {
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		self::$db = new BusinessSoukeoGrossisteModel();
		self::$db -> prepGetProdGrossisteAssoc();
		self::$db -> prepMajPoidsSfkCp();

		//Parcour du dossier
		if (is_dir($this -> dirName)) {
			
		    if ($dh = opendir($this -> dirName)) {
		    	
		        while (($file = readdir($dh)) !== false) {
		        	
		        	if(! is_dir($this -> dirName . "/" . $file)){
		        		$this -> _appendLog("Traitement fichier : $file : type : " . filetype($this -> dirName . "/" . $file) . "...............");
						$this -> parseCSV($this -> dirName . "/" . $file);
						rename($this -> dirName . "/" .$file, $this -> dirName . "/archives/" . $file );	
						//break;
		        	}
		            
		        }
		        closedir($dh);
		    }
		}
		
		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));	
	}


	/**
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param string 	$file 			Le fichier CSV
	 */
	protected function parseCSV($file)
	{
		
		$row = 0;
		if ( ($handle = fopen($file, "r")) !== FALSE) {
			$headerList = array();
			$product = array();
			
		    while (($data = fgetcsv($handle, 0, $this -> delimiter)) !== FALSE) {
		    	
		        $num = count($data);

		       	//pour chaque colonnes
		        for ($c=0; $c < $num; $c++) {
		        	
					if($row == 0){
						$headerList[] =  BusinessEncoding::toUTF8($data[$c]);
					}else{
						$product[] = BusinessEncoding::toUTF8($data[$c]);                    
					}	
		        }
			  	
				//Si on est pas sur l'entete
				if($row != 0) {
					//Création du produit avec ses colonnes associatives
					$product = array_combine($headerList, $product);
					$product['poids'] = str_replace(",", ".", $product['poids']);
					
					//MAJ poids sfk_catalog_product
					self::$db -> updateWeightSfkCp($product['id_produit'], $product['poids']);
					
					//MAJ poids grossiste
					$idProdGrossiste = self::$db -> getProdGrossisteAssoc($product['id_produit']) ;
					self::$db -> updatePoidsProduct($idProdGrossiste , $product['poids']);
			
				}
				unset($product);
					
				$row++;
		    }
			$this -> _appendLog("TERMINE - ".$row." - LIGNES PARSEES ");
			$this -> _appendLog("");
		fclose($handle);
			
		}
		else{
			$this -> _appendLog("Fichier $file non trouve !!");
		}
		
	}

	
	protected function recursive_array_search($needle,$haystack) {
	    foreach($haystack as $key=>$value) {
	        $current_key=$key;
	        if($needle===$value OR (is_array($value) && $this -> recursive_array_search($needle,$value) !== false)) {
	            return $current_key;
	        }
	    }
	    return false;
	}
		/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
	
}


?>