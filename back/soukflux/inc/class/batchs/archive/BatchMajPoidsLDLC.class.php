<?php 

/**
 * 
 */
class BatchMajPoidsLDLC extends Batch {

	static protected 	$_description 	= "[MAJ] [POIDS LDLC] Batch de mise à jour des poids LDLC ";
	private 			$_name 			= "BatchMajPoidsLDLC";	
	static protected 	$db;
	protected 			$grossiste_id;
	
	public function work ($grossiste_id = 2) {
		
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
		
		$this -> grossiste_id = $grossiste_id;
		self::$db = new BusinessSoukeoGrossisteModel();
		
		//Recup liste Categ 
		$listCat = self::$db -> getListWeightByCat ($this -> grossiste_id);
		self::$db -> prepMajPoidsGrossiste();
		self::$db -> prepMajPoidsSfkCp();
		
		/*foreach ($listCat as $cat) {
			
			self::$db -> majPoidsGrossiste($cat['CATEGORY'], $cat['POIDS_DEFAULT'], $this -> grossiste_id);
			self::$db -> majPoidsSfkCp($cat['CATEGORY'], $cat['POIDS_DEFAULT'], $this -> grossiste_id);
			
			$this -> _appendLog($cat['CATEGORY']. " - poids par défaut : ". $cat['POIDS_DEFAULT']);
		}*/
		$key = $this -> recursive_array_search("Souris", $listCat, true);
		var_dump($key);
		$this -> _appendLog('categorie');
		if($key !== false) var_dump($listCat[$key]['POIDS_DEFAULT']);
		$cat = self::$db -> getIdCatAssocGrossiste("Souris", 2);
		var_dump($cat);
		
		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));	
	}
	
	protected function recursive_array_search($needle,$haystack) {
	    foreach($haystack as $key=>$value) {
	        $current_key=$key;
	        if($needle===$value OR (is_array($value) && $this -> recursive_array_search($needle,$value) !== false)) {
	            return $current_key;
	        }
	    }
	    return false;
	}
		/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
	
}


?>