<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";

} else {

	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchCrawlerLDLC extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchCrawlerLDLC";
	static protected $_description = "[CRAWLER] [LDLC] pour infos completes LDLC pro";

	private static $db;


	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);


		//Connection base SoukFlux
		self::$db = new BusinessSoukeoGrossisteModel();
		//Prepare stmt
		self::$db -> prepareInsertSfkProdAttrGR();
		//$listeProd = self::$db -> getAllProdByVendorComplete(2);
		//crawl en prod
		$listeProd = self::$db -> getAllProdByVendorToCrawl(2);
		//crawl de test a commenté par la suite
		//$listeProd = self::$db -> getAllProdByVendorToCrawlTest(2);
		$opts = array(
    		'http'=>array(
    			'method'=>"GET",
    			'header'=>"Accept-language: en\r\n" .
    			"User-Agent: 	Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6\r\n".
    			"Cookie: foo=bar\r\n"
    		)
    	);
    	$context = stream_context_create($opts);
		$nbProds = 0;
		// pour chaque produit dans la table grossiste et pour le grossiste LDLC
		foreach ($listeProd as $prod) {
			$nbProds++;
			$sku = null;
			$this -> _appendLog($nbProds." - Produit id : ".$prod['ID_PRODUCT']." -- url : ".$prod['URL']);
			
			$html = file_get_html($prod['URL'], false , $context);
			
			$descript = $html -> find (".description", 0) -> plaintext ;
			$descripthtml =  $html -> find (".description", 0) -> innertext ;
			$descript = str_replace(array("\r", "\n", "\r\n", "\t", "  "), "", $descript);
			$descript = implode(" ", explode(" ", $descript));
			$descript = html_entity_decode(BusinessEncoding::toUTF8($descript));
			
			self::$db -> updateDescriptProduct($prod['ID_PRODUCT'] , $descript);
			$sku = $prod['EAN'];
			//$this -> _appendLog($sku . " ean "); // pour affiché l'ean
			// on update la description html du produit dans le catalog product
			self::$db -> updateDescriptProductCatProd($sku , $descripthtml);
			self::$db -> updateDescriptProductCatProdNotHtml($sku , $descript);
			
			//on implémente la table ean_url
			//si l'ean et l'url n'est pas nul :
			if (($sku !=null)&&($prod['URL']!=null) )self::$db -> insertToEanUrl($sku , $prod['URL']);
			
			
			$attr = array();
			
			foreach ($html->find("font.legende" ) as $label) {
				
				
				$label_attr = html_entity_decode(BusinessEncoding::toUTF8(trim($label -> innertext)));
				
				$code_attr = str_replace(array(" ", "'"), "_", $label_attr);
				$code_attr = strtoupper(self::$db ->stripAccents($code_attr)); 
				
				$value = $label -> parent () -> parent () -> parent () -> next_sibling() -> firstChild() -> plaintext ;
				
				$value = html_entity_decode(BusinessEncoding::toUTF8($value));
				
				if($label_attr != "Présence LDLC"){
					
					$id_attr = self::$db -> checkIfAttributeExistsGR($code_attr, $label_attr);
				
					self::$db -> addRowSfkProdAttrGR($prod['ID_PRODUCT'], 2, $id_attr, $value);
				}
				
				if(strtolower($label_attr) == "poids"){
					//Traitement du poids
					if (strpos($value,'kg') !== false) {
					    $value = trim(str_replace("kg", "", $value));
					}elseif(strpos($value,'g') !== false){
						$value = trim(str_replace("g", "", $value));
						$value = (float)$value / 1000;
					}
						self::$db -> updatePoidsProduct($prod['ID_PRODUCT'], $value);
					// si le poid est supérieur a zero on update dans la table catalog product
					if ($value>0) self::$db -> updatePoidsProductCatProd($prod['EAN'], $value);
					//$this -> _appendLog($value . " :poid "); // pour affiché le poid 
				
				}				
			}
			
			self::$db -> setCrawledProduct(2, $prod['ID_PRODUCT'], 1);

		}

		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}



	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
