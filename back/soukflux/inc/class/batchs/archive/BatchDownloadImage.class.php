<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}


/**
 * Batch dédié au download de toutes les images de sfk_catalog_product afin d'obtenir des url locales
 * sur media-avahis.com pour le front-office
 * @version 0.1.0
 * @author 	Philippe_LA
 * @see 	BusinessPictureManager.class.php pour la gestion du download d'images
 */
class BatchDownloadImage extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchDownloadImage";
	static protected $_description = "[TMP] [DownloadImage] Download des images";

	private static $db; 			//Model utilisé pour sauvegarde des données
	private static $pictureManager; //L'outil permettant la sauvegarde d'images	
	
	protected $path = "/var/www/media.avahis/media/catalog/product/";
	protected $local_url = "http://media.avahis.com/media/catalog/product/";
	
	/**
	 * Fonction executée par le batch
	 */
	public function work() {
		
		$temps_debut = microtime(true);

		//Connection base SoukFlux
		self::$db 				= new BusinessSoukeoModel();
		self::$pictureManager 	= new BusinessPictureManager();
		
		//Prepare stmt
		self::$db -> prepGetProdGrossisteAssoc();
		
		//Récupération de la liste des produits à mettre à jour
		$listeProd = self::$db -> getAllProducts();
		$nbProds = 0;
		
		// pour chaque produit dans la table grossiste et pour le grossiste LDLC
		foreach ($listeProd as $prod) {

			if ($prod["dropship_vendor"] != 39 ) {			
				//MAJ des fiches sfk_catalog_product
				if($idProdGrossiste = self::$db -> getProdGrossisteAssoc($prod['id_produit'])){
					$prodGr = self::$db -> getProduitGrossiste($idProdGrossiste);
					$prodGr = $prodGr[0];
				}
				
				self::$pictureManager -> savePictures($prodGr, $prod);
				
				$nbProds++;
			}
		}
		$this -> _appendLog($nbProds . " produits mis à jour ! ");

		$temps_fin = microtime(true);
		$this -> _appendLog ( "Duree du batch : " . number_format($temps_fin - $temps_debut, 3) );
	}



	/**
	 * Permet de récuperer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
