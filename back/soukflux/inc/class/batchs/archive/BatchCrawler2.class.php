<?php

/**
 * Batch dédié à rue-hardware.com pour du matériel informatique.
 * @version 0.1.0
 * @author Philippe_LA
 * @see simple_html_dom Utilisé pour le parsing des pages web
 */
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $main_path . "/" . $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $main_path . "/" . $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";

} else {

	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoModel.class.php";
	//require_once $root_path . "inc/class/business/BusinessSoukeoCrawlerModel.class.php";
	//require_once $root_path . "thirdparty/simplehtmldom_1_5/simple_html_dom.php";
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchCrawler2 extends Batch {

	//Variables pour la clase batch
	private $_name = "BatchCrawler2";
	static protected $_description = "[CRAWLER] [RUE HARDWARE] BatchCrawler2 pour rue-hardware.com";

	private static $db;

	// A COMPLEXIFIER
	protected $cat_id;
	//L'ID de la catégorie dans la BDD correspondante aux produits qu'on va crawler

	protected $rooturl = "http://www.rue-hardware.com";
	protected $Aattr = array();
	//Tableau de fiches produit

	protected $curProd;
	//Produit actuellement lu
	protected $curCat;
	//Catégorie dans laquelle on se trouve actuellement
	protected $curSCat;
	//Sous Catégorie dans laquelle on se trouve actuellement
	protected static $countCatChecked = 0;
	//Compteur du nombre de catégories déja vues

	//Entrez des nombres très grand (99999) pour faire tous les produits de toutes les sous catégories etc...
	private $nbCat = 999;
	private $nbSsCat = 999;
	private $nbProdBySsCat = 9000;

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);

		//Choix de l'id de la catégorie correspondante
		$this -> cat_id = 27;

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoCrawlerModel();
		//Prepare stmt
		self::$db -> prepareInsertSfkCatProduct();
		self::$db -> prepareInsertSfkAttribute();
		self::$db -> prepareInsertSfkProductAttribute();
		self::$db -> prepareInsertSfkProductCategorie();

		$aurl = array("http://www.rue-hardware.com/prix/");

		foreach ($aurl as $u) {
			$html2 = file_get_html($u);

			//Arbre principal de liens vers les catégorie
			foreach ($html2->find("ul.treeW" ) as $ul) {

				//Chaque element de l'arbre contient le lien a
				foreach ($ul->children() as $e) {

					//Récup nom catégorie dans $cat
					$this -> curCat = $e -> find('a', 0) -> innertext;
					self::$countCatChecked++;

				//	echo "\n==================================Begin cat  " . $this -> curCat . " {" . self::$countCatChecked . "}==========================\n";

					//On va chercher les sous cat de cette cat
					$this -> checkCat($e -> find('a', 0) -> href);

				//	echo "\n==================================END cat==========================\n\n";
				}
				//Si on est arrivé au nombre max de catégories on sort de la boucle
				if (self::$countCatChecked >= $this -> nbCat) {
					break;
				}
			}
		}

	

		$temps_fin = microtime(true);
		echo "Duree du batch : " . number_format($temps_fin - $temps_debut, 3);
		//$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}

	/**
	 * Permet de chercher toutes les sous catégories de la catégorie en cours
	 * On obtient un url en paramètre qu'on va concaténer avec le root url dispo en propriété de la classe
	 * Le parsing se fera sur cette nouvelle url et on va chercher les sous catégories présentes dans cette
	 * catégorie
	 * @param url String l'url pour la catégorie en cours
	 */
	protected function checkCat($url) {
		$url = $this -> rooturl . $url;
		$html = file_get_html($url);
		$i = 1;
		//count
		$sql = "select distinct categories as ID from sfk_catalog_product";
		//self::$db->getall($sql);
		$res = self::$db->formatarr(self::$db->getall($sql));
		//var_dump($res);
		foreach ($html->find("ul.subtree li ") as $a) {
			$url = $a -> find('a', 0) -> href;
			$this -> curSCat = $a -> find('a', 0) -> innertext;
			
				
			//ON VA CHERCHE LE CAT_ID grace à la dernière sous catégorie
			
			$cat = self::$db -> getCatId2($this -> curCat. " / ". $this -> curSCat);
			$this -> cat_id = $cat ;
			if (!in_array($this->cat_id, $res)){
				echo $this -> curCat. " / ". $this -> curSCat."  --->".$this->cat_id ;
				if ($url != "/prix/comparer,casques-micros,214,1,1,1")//catégorie casques micro fonctionne pas memory leak
					$this -> getInfoProduit($url);
			}else {
				echo $this -> curCat. " / ". $this -> curSCat."  --->".$this->cat_id . "Déja traité --> next ";
			}


			//echo "\n*********************End Sous - cat*********************\n\n";

			//Vérif sortie de boucle
			$i++;
			if ($i > $this -> nbSsCat)
				break;
		}
	}

	/**
	 * Parcourt tous les produits de la sous catégorie cherche l'url, le nom et le prix et grâce à cet url
	 * va chercher la description entière du produit
	 * @param url String l'url pour la sous catégorie en cours
	 */
	protected function getInfoProduit($url) {

		$url  = $this -> rooturl . $url;
		$html = file_get_html($url);
		$j = 1;
		//count

		//On est obligé de prendre les class "check" représentant les checkmark devant chaque produit car
		//Le début de description de chaque produit n'est marqué par aucune autre balise
		foreach ($html->find('td.check') as $desc) {

			//URL du produit
			$urlProd = $this -> rooturl . $desc -> next_sibling() -> find('a', 0) -> href;

			//NOM du produit
			$nomProd = $desc -> next_sibling() -> find('a', 0) -> innertext;
			$this -> curProd = $nomProd;
			$this -> Aattr[$this -> curProd]['nom'] = $nomProd;

			echo "\n\nProduit : " . $this -> curProd . "\n";

			//PRIX du produit
			$price = $desc -> next_sibling() -> next_sibling() -> next_sibling() -> innertext;
			$price = filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);
			$price = explode(",", $price);
			$price = $price[0] . "," . substr($price[1], 0,2);
			$this -> Aattr[$this -> curProd]['price'] = $price;

			echo "\nPrix = " . $price . "\n";

			//Récupération decription détaillée
			$this -> checkProd($urlProd);

			//Vérif sortie de boucle
			$j++;
			
			if ($j > $this -> nbProdBySsCat){
				break;
			}
		}
		$html -> __destruct();
	}

	/**
	 * Récupère la description détaillée pour 1 produit : récupération de tous
	 * les attributs ainsi que leur valeur qu'on met dans le tableau Aattr pour la
	 * clé associative correspondant au produit en cours.
	 * @param url String l'url du produit à crawler
	 */
	protected function checkProd($url) {

		$html = file_get_html($url);
		$rows = $html -> find("div.pdtInfos tr");

		foreach ($rows as $row) {

			//Fin de la fiche descriptive (souvent caractérisée par un tr avec un <b> a l'intérieur)
			if ($row -> find('b'))
				break;
			//////////////////////////////

			// Attribut :
			$attr = $this -> nettoyageStr(filter_var($row -> firstChild() -> innertext, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
			if ($attr == "code(s) ean")
				$attr = "ean";
			echo "\n" . $attr . " : ";

			//Valeur :
			$tagValue = $row -> firstChild() -> next_sibling();
			$value = "";

			if ($tagValue -> has_child()) {
				//Pour la marque on ne veux que le nom pas le lien vers le site de la marque etc...
				if ($attr == "marque") {
					$value = trim($tagValue -> firstChild() -> innertext);

				} else {
					//Plusieurs descriptions sous forme de <a>
					foreach ($tagValue->find('a') as $sTagValue) {
						$value .= trim($sTagValue -> innertext) . ", ";
					}
					$value = substr_replace($value, " ", -2);
				}
			} else {
				$value = trim($tagValue -> innertext);
			}

			$value = $this -> nettoyageStr(filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
			echo $value . "\n";
			$this -> Aattr[$this -> curProd][$attr] = $value;
		}
		//Description produit
		$desc = $this -> curSCat . " " . $this -> Aattr[$this -> curProd]['marque'] . ", " . $this -> Aattr[$this -> curProd]['nom'];
		$desc = $this -> nettoyageStr(filter_var($desc, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
		$this -> Aattr[$this -> curProd]['desc'] = $desc;

		//Image produit
		$this -> Aattr[$this -> curProd]['image'] = $html -> find(".pdtPhoto img", 0) -> src;
		echo "\nImage : " . $this -> Aattr[$this -> curProd]['image'] . "\n";

		//Categories produit pour permettre à Magmi de créer l'arborescence produit
		$this -> Aattr[$this -> curProd]['categories'] = $this -> cat_id;

		$produit = 	$this -> Aattr[$this -> curProd];
		
		if (isset($produit['ean']) && $produit['ean'] != "") {

			//Insertion du produit avec tous ses champs "basique"
			var_dump($produit);
			self::$db -> addRowSfkCatProduct($produit, $produit['categories']);

			//Besoin de l'id produit inseré ou mis à jour pour les relation attr/prod
			$idProd = self::$db -> getIdProduct($produit['ean']);
			self::$db -> addRowSfkProductCategorie($idProd, $produit['categories']);
			//Récupération des attributs du produit
			$listeAttributsProduit = $this -> getAttributsProduct($produit);

			//boucle Attributs
			foreach ($listeAttributsProduit as $attr => $value) {

				//Si c'est un attribut déja connu on récupère son ID
				$idAttr = self::$db -> checkIfAttributeExistsPS($attr, $produit['categories']);

				//Sinon on insère cet attribut et on récupère le lastInsertId
				if ($idAttr == false) {
					self::$db -> addRowSfkAttr($attr, $produit['categories']);
					$idAttr = self::$db -> lastInsertId();
				}

				//Ajout de la ligne de relation entre ce produit et cet attribut
				self::$db -> addRowSfkProdAttr($idProd, $idAttr, $value, $produit['categories']);
			}
		}

		//var_dump($this->Aattr);
		echo "********************************************************************** \n";
		$html -> __destruct();
		sleep(rand(0, 1));
		//sleep antiban
	}

	/**
	 * Nettoyage des caractères laissés par le filter_var FILTER_FLAG_STRIP_LOW
	 */
	public function nettoyageStr($str) {
		$str = str_replace('&#38;', '&', $str);
		$str = str_replace('&#34;', '"', $str);
		$str = str_replace('&#39;', "'", $str);

		return strtolower($str);
	}

	/**
	 * Fonction permettant de ne garder que les champs attributs d'un produit crawlé
	 * en enlevant les les champs fixes.
	 * @param 	produit Array avec champs attributs produit et champs fixe
	 * @return 	produit Array san champs fixes produit
	 */
	protected function getAttributsProduct($produit) {
		//On enleve les champs basiques du tableau pour n'avoir que les attributs dynamiques
		unset($produit['nom']);
		unset($produit['marque']);
		unset($produit['Référence constructeur']);
		//unset($produit['EAN']);
		unset($produit['image']);
		//unset($produit['meta_title']);
		unset($produit['price']);
		unset($produit['desc']);
		unset($produit['categories']);
		//if(isset($produit['poids'])) unset($produit['poids']);

		return $produit;
	}

	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
