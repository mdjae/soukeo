<?php 
if (isset($main_path)) {
	//require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	
	
} else {
	//require_once $root_path . "inc/class/system/SystemParams.class.php";
	require_once $root_path . "inc/offipse.inc.php";
}
/**
 * Batch de remplacer la virgules du champ weight des tables catalogue produit et produit grossiste.
 * @author hkcfdc
 */
class BatchUpdateEntityIdCustVend extends Batch{

	static protected 	$_description 	= "[UPDATE] [BDD] Batch update champ entity_crm_id de skf_customers et sfk_vendor";
	private 			$_name 			= "BatchUpdateEntityIdCustVend";
	
	private static 		$db;
	protected static 	$nbProd = 0; //Nombre de produits exportés pour la catégorie
	protected static 	$nbErreurs = 0; //Nombre d'erreurs d'export produit car poids vide
	protected $total_prod =0;
	
	/**
	 * Routine du batch
	 */
	public function work()
	{
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
		
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		
		$vendorManager = new ManagerVendor();
        $customerManager = new ManagerCustomer();
        $entityManager = new ManagerEntityCrm();
        $entityTypeManager = new ManagerEntityType();
        
        
        //VENDOR  
        
        $entityType = $entityTypeManager -> getBy(array("label" => "vendor"));
        
        $vendorList = $vendorManager -> getAll();
        
        foreach ($vendorList as $vendor) {
            if(!$vendor -> getEntity_id()){
                
                $entity = new BusinessEntityCrm(array("type_entity_id" => $entityType -> getType_entity_id()));
                
                $entityManager -> save($entity);
                
                $vendor -> setEntity_id($entity -> getEntity_id());
                
                $vendorManager -> updateEntityId($vendor);
            }
        }
        
        
        //CUSTOMERS  
        
        $entityType = $entityTypeManager -> getBy(array("label" => "customer"));
        
        $customerList = $customerManager -> getAll();
        
        foreach ($customerList as $customer) {
            if(!$customer -> getEntity_id()){
                
                $entity = new BusinessEntityCrm(array("type_entity_id" => $entityType -> getType_entity_id()));
                
                $entityManager -> save($entity);
                
                $customer -> setEntity_id($entity -> getEntity_id());
                $customer -> setStatut(2);
                
                $customerManager -> updateEntityId($customer);
            }
        }
                
		$this -> _appendLog("------------------------------------------------------------------------------------");
			
		$this -> _appendLog('Nombre de produits total '.$this->total_prod);
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
		
	}	

	/**
	 * Permet de récupérer la description du programme
	 * @author fred
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}
}
?>