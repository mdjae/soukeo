<?php 
if (isset($main_path)) {
//	require_once $main_path . "/" . $root_path . "inc/class/system/SystemParams.class.php";
//	require_once $main_path . "/" . $root_path . "inc/class/business/BusinessImportPrestashop.class.php";
//	require_once $main_path . "/" . $root_path . "inc/class/business/BusinessImportThelia.class.php";
//	require_once $main_path . "/" . $root_path . "inc/class/business/BusinessImportVirtuemart.class.php";
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
	
} else {
	require_once $root_path . "inc/offipse.inc.php";
//	require_once $root_path . "inc/class/system/SystemParams.class.php";
	//require_once $root_path . "inc/class/business/BusinessImportPrestashop.class.php";
	//require_once $root_path . "inc/class/business/BusinessImportThelia.class.php";
	//require_once $root_path . "inc/class/business/BusinessImportVirtuemart.class.php";
}

/**
 * Batch qui va parcourir tout les e-commercant de la base de donnée afin de connaitre leur technologie.
 * A partir de leur technologie le Batch pourra lancer le batch d'import dédié.
 * @author Philippe_LA
 */
class BatchGrandeRecre extends Batch 
{
	
	static protected 	$_description 	= "[TMP] [GRANDE RECRE] Import grande recre Avahis -> Soukflux";
	private 			$_name 			= "BatchImportGrandeRecre";
	private static 		$db;
	protected $csv_to_parse 		= "";		//URL de l'ecommerçant à parser
	protected $vendorID 			= ""; 		//VENDOR_ID de l'ecommercant	
	protected $rep = "/opt/soukeo/import/commercants/";
	
	/**
	 * Fonction de routine du batch
	 */
	public function work()
	{
		
		$temps_debut = microtime(true);
		
		$this -> _appendLog("Batch " . $this -> _name);
        $this -> _appendLog("");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");
        $this -> _appendLog("---------------------------------------------------------------------------------------------");

		//Connection base SoukFlux
		self::$db = new BusinessSoukeoCSVModel();
		$this->vendorID 		= 61;
		$this -> delimiter = ";";
		$this->csv_to_parse 	= $this->rep . "export_la_grande_recreGOOD2.csv";
		$this->parseCSV($this->csv_to_parse);
		
		
		
		//TEST
		//$string = "Jouets et jeux/Jouets d'éveil et 1er âge/Jouets musicaux  ";
		//$string = "Jouets et jeux/Loisirs créatifs/Pâte à modeler";
		//$string = "Jouets et jeux/Jeux de société/Jeux de rôle  ";
		
		//$this -> findIdCateg($string);
		
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}


	/**
	 * Cette fonction permet de parser un CSV en remplissant par la suite un tableau de produits ayant
	 * toutes leurs données fixes et leurs attributs. On rempli également un tableau d'attributs contenant
	 * les entêtes (les noms) de tous les attributs exportés.
	 * @param url 				String 		L'url qui va contenir le CSV
	 * @param nbChampsFixes 	Int 		Le nombre de champs fixe qui sont communs à tous produits
	 * @param delimiter 		String 		Le délimiteur choisi dans le CSV
	 */
	protected function parseCSV($file)
	{
		self::$db -> prepareInsertSfkProdCSV($vendorID);
		self::$db -> prepareStmtProdEcommercant($this->vendorID);
		self::$db -> prepInsertSfkCp();
	
		
		if (($handle = fopen($file, "r")) !== FALSE) {
			$headerList = array();
			$product = array();
			$row = 0;
		    while (($data = fgetcsv($handle, 0, $this -> delimiter)) !== FALSE) {
		    	
		        $num = count($data);

		        
		       	//pour chaque colonnes
		        for ($c = 0; $c < $num; $c++) {
		        	//var_dump($product);
					if($row == 0){
						$headerList[] = trim(BusinessEncoding::toUTF8(html_entity_decode($data[$c])));
						
					}else{
						$product[] = trim(BusinessEncoding::toUTF8(html_entity_decode($data[$c])));
					}	
		        }
				
				
				//Si on est pas sur l'entete
				if($row != 0) {
					//Création du produit avec ses colonnes associatives
					$product = array_combine($headerList, $product);

						
					
					$product = $this -> formatFields($product);

					
					//INSERT PRODUIT AVAHIS
					
					//Check sku exist
					if(! self::$db -> getProdBySku($product['sku'])){
						
						$this -> _appendLog('line -'.$row. ' - NOUVEAU produit '.$product['sku']);
						$id_avahis = self::$db -> addRowCatProduct($product, $this -> vendorID);
						
						//attributs
						$id_attribute = self::$db->checkIfAttributeExists("AGE");
						if($product['av_age'] != "" ){
							self::$db -> addAttributProduct($id_attribute, $id_avahis, $product['av_age']);
						}
						
						$id_attribute = self::$db->checkIfAttributeExists("PILES");
						if($product['av_piles'] != "" ){
							self::$db -> addAttributProduct($id_attribute, $id_avahis, $product['av_piles']);
						}
						
						$id_attribute = self::$db->checkIfAttributeExists("PILES_FOURNIES");
						if($product['av_piles_fournies'] != "" ){
							self::$db -> addAttributProduct($id_attribute, $id_avahis, $product['av_piles_fournies']);
						}
						
					}else{
						$id_avahis = self::$db -> getProdBySku($product['sku']);
						$this -> _appendLog('ERREUR line -'.$row. ' - SKU déja présent '.$product['sku']);
					}

					
					
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					//////////////////////////////////////////////// INSERT COTE ECOMMERCANTS ////////////////////////////////////////////					
					
					//Insertion produit
					$state = self::$db->addRowSfkProdCSV($product, $this->vendorID);
					
					
					self::$db->assocProduct($product['ID_PRODUCT'], $id_avahis, $this->vendorID);

				    self::$db->addRowSfkProdEcomSP($product, $this->vendorID) ;	

					$id_attribute = self::$db->checkIfAttributeExistsCSV("AGE");

					if($product['av_age'] != "" ){
						self::$db ->prepareInsertSfkProdAttrCSV();
						if(self::$db->addRowSfkProdAttrCSV($product['ID_PRODUCT'], $this->vendorID, $id_attribute, $product['av_age']) == true) {
							$this->nbInsertAttr++;
						}		
					}
					$id_attribute = self::$db->checkIfAttributeExistsCSV("PILES");
					if($product['av_piles'] != "" ){
						self::$db ->prepareInsertSfkProdAttrCSV();
						if(self::$db->addRowSfkProdAttrCSV($product['ID_PRODUCT'], $this->vendorID, $id_attribute, $product['av_piles']) == true) {
							$this->nbInsertAttr++;
						}		
					}
					$id_attribute = self::$db->checkIfAttributeExistsCSV("PILES_FOURNIES");
					if($product['av_piles_fournies'] != "" ){
						self::$db ->prepareInsertSfkProdAttrCSV();
						if(self::$db->addRowSfkProdAttrCSV($product['ID_PRODUCT'], $this->vendorID, $id_attribute, $product['av_piles_fournies']) == true) {
							$this->nbInsertAttr++;
						}		
					}
					
					self::$db -> addAssocCatVendor(61, $product['CATEGORY'], $product['categories']);
					
					if( $state == 1){ //Si la requete et bien passée
						$this->nbInsert++;
					}elseif($state == 2 ){
						$this->nbUpdate++;
					}elseif($state == 0){
						$this->nbErrors++;
					}
				}	
				unset($product);	
				$row++;
		    }
		fclose($handle);	
		}
	}
	
	
	public function cleanDescription($product)
	{
		return $product;
	}
	
	
	protected function formatFields($product){
		
		$product['ID_PRODUCT'] 			= $product['sku']."_61";
		$product['NAME_PRODUCT'] 		= $product['name'];
		$product['QUANTITY'] 			= $product['qty'];
		$product['PRICE_PRODUCT'] 		= $product['price'] ;
		$product['WEIGHT'] 				= $product['weight'];
		$product['IMAGE_PRODUCT'] 		= $product['image'];
		$product['IMAGE_PRODUCT_2'] 	= $product['small_image'];
		$product['IMAGE_PRODUCT_3'] 	= $product['thumbnail'];
		$product['DESCRIPTION'] 		= $product['description'];
		$product['DESCRIPTION_SHORT'] 	= $product['short_description'];
		$product['MANUFACTURER'] 		= $product['av_manufacturer'];
		$product['manufacturer'] 		= $product['av_manufacturer'];
		$product['CATEGORY'] 			= str_replace("/", " > ", $product['categories']) ;
		$product['categories'] 			= $this -> findIdCateg($product['categories']);
		$product['REFERENCE_PRODUCT'] 	= $product['sku'];
		$product['EAN'] 				= $product['sku'];
		 	
		return $product;
	}
	/**
 	 * Permet de r�cup�rer la description du programme
 	 * @author tpo
 	 *
 	 */
	public static function getDescription() 
	{
		return self::$_description;
	}
	
	public function findIdCateg($cat)
	{
		$cat_tree = explode("/", $cat);
		
		$end = trim(end($cat_tree)) ;
		
		if($end == ""){
			
			$cat = substr($cat, 0,-1);
			
			$cat_tree = explode("/", $cat);
		
			$end = trim(end($cat_tree)) ;
			
		}
		
		$res = self::$db -> getCatbyLabel($end);
		
		//var_dump($res);
		
		if(count($res) > 1){

			foreach ($res as $rescat) {
				
				$parentToFind = $rescat['parent_id'];	
				$parent = $cat_tree[count($cat_tree)-2];

				$res_parent = self::$db -> getCatbyLabel($parent);
				
				foreach ($res_parent as $r) {
					
					if($parentToFind == $r['cat_id']){

						$id_cat = $rescat['cat_id'];
					}
				}		
			}			
		}else{

			$id_cat = $res[0]['cat_id'];
		}
		
		if(!$id_cat)$this -> _appendLog("ERREUR ID FINAL = ".$id_cat);
		return $id_cat;
	}
}

?>