<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}

class BatchCountProduct extends Batch {

	private $_name = "BatchCountProduct";
	static protected $_description = "[LOCAL] -- Count produit par catégorie";

	private static $db;

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
		$temps_debut = microtime(true);
		$this -> _appendLog("Batch " . $this -> _name);
		$this -> _appendLog("");
		$this -> _appendLog("---------------------------------------------------------------------------------------------");
		//Connection base SoukFlux
		self::$db = new BusinessSoukeoModel();
		$listeCat = self::$db -> getAllCat();

		foreach ($listeCat as $cat) {
			$count = self::$db -> countAllProductInCatRecursive($cat['cat_id'], 0);
			self::$db -> insertCountProdForCat($cat['cat_id'], $count);
		}
		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));
	}

	/**
	 * Permet de récupérer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
