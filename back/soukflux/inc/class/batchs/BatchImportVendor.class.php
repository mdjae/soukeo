<?php
if (isset($main_path)) {
	require_once $main_path . "/" . $root_path . "inc/offipse.inc.php";
} else {
	require_once $root_path . "inc/offipse.inc.php";
}


class BatchImportVendor extends Batch {
	private $_name = "BatchImportVendor";
	static protected $_description = "[IMPORT] -- Import des vendeurs AVAHIS";
	private static $db;
	private static $db2;
	private $listeid ;

	/**
	 * Fonction executee par le batch
	 */
	public function work() {
	    global $applicationMode;
		$temps_debut = microtime(true);
		$listIDSvendor= array();
		$nbvendor = 0 ;

		//Connection base SoukFlux
		self::$db = new BusinessAvahisModel();
		self::$db2 = new BusinessSoukeoModel();
		self::$db2 -> prepInsertSfkVd() ;
        self::$db2 -> prepUpdateSfkVd() ;
		$listVendor = self::$db->getallVendor();
		
		$this -> _appendLog('-----------------------------------------------------------------------------------');	
		$this -> _appendLog(' 								IMPORT DES VENDEURS');
		$this -> _appendLog('-----------------------------------------------------------------------------------');
		foreach ($listVendor as $vendor){
			$vendor['street'] = self::$db2-> filtre_string($vendor['street']);	
			$vendor['street'] = $this -> stripAccents($vendor['street']);
            
            if($applicationMode == "REC" && $vendor["email"] == "test@test.fr"){
                $vendor["email"] = $vendor["email"].$nbvendor;    
            }
            			
            //update
            if(self::$db2 -> checkIfVendorExist($vendor['vendor_id']) > 0 ){    
                if(self::$db2->addRowValVendorUpdate($vendor)){   
                    $this -> _appendLog("Update du vendeur  : " .$vendor['vendor_name']." id_vendeur : ".$vendor['vendor_id']);  
                    $nbvendor++;
                    $listIDSvendor[] = $vendor['vendor_id'];
                } 
            }
            //insert
            else{
                if(self::$db2->addRowValVendor($vendor)){   
                    $this -> _appendLog("Insertion du vendeur  : " .$vendor['vendor_name']." id_vendeur : ".$vendor['vendor_id']);  
                    $nbvendor++;
                    $listIDSvendor[] = $vendor['vendor_id'];
                }    
            }
		}
		$this -> _appendLog('-----------------------------------------------------------------------------------');	
		$this -> _appendLog('-----------------------------------------------------------------------------------');	
		
		if ($nbvendor==0)$nbvendor= "pas de vendeur ajouté";
		else {
			$this -> _appendLog('Nombre de vendeur inséré : ' .$nbvendor);
			$this -> _appendLog("Nombre de vendeur inactive en base  : ".self::$db2->setInactiveVendor($listIDSvendor) );		
		}

		$temps_fin = microtime(true);
		$this -> _appendLog('Temps d\'execution : ' . number_format($temps_fin - $temps_debut, 3));

	}
		
	/**
	 * Enléve les accents
	 * @param string avant filtrage des accents
	 * @return string sans accents

	 */
	public function stripAccents($texte) {
		$texte = str_replace(
			array(
				'à', 'â', 'ä', 'á', 'ã', 'å',
				'î', 'ï', 'ì', 'í', 
				'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
				'ù', 'û', 'ü', 'ú', 
				'é', 'è', 'ê', 'ë', 
				'ç', 'ÿ', 'ñ',
				'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
				'Î', 'Ï', 'Ì', 'Í', 
				'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
				'Ù', 'Û', 'Ü', 'Ú', 
				'É', 'È', 'Ê', 'Ë', 
				'Ç', 'Ÿ', 'Ñ' 
			),
			array(
				'a', 'a', 'a', 'a', 'a', 'a', 
				'i', 'i', 'i', 'i', 
				'o', 'o', 'o', 'o', 'o', 'o', 
				'u', 'u', 'u', 'u', 
				'e', 'e', 'e', 'e', 
				'c', 'y', 'n', 
				'A', 'A', 'A', 'A', 'A', 'A', 
				'I', 'I', 'I', 'I', 
				'O', 'O', 'O', 'O', 'O', 'O', 
				'U', 'U', 'U', 'U', 
				'E', 'E', 'E', 'E', 
				'C', 'Y', 'N' 
			),$texte);
		return $texte;
	}
	/**
	 * Permet de r�cup�rer la description du programme
	 * @author tpo
	 *
	 */
	public static function getDescription() {
		return self::$_description;
	}

}
