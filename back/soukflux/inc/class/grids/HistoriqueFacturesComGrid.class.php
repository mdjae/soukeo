<?php 
/**
 * Class pour grid Grossiste
 */
class HistoriqueFacturesComGrid extends Grid
{
	protected $facturecode;
	protected $numvendor ;
	protected $commfilter ;
    protected $datelimitfilter;
	
	public function __construct($facturecode, $numvendor, $commfilter, $datelimitfilter)
	{	
		$this->_name = 'historique_factures_com';
		$this-> facturecode = $facturecode;
		$this-> numvendor = $numvendor;
		$this-> commfilter = $commfilter;
        $this-> datelimitfilter = $datelimitfilter;
		$this->_setSqlQuery();
		$this->_init('historique_factures_com',$this->_makeHeaders(),$this->_makeContent() );
		$this->setTitle("NoBg","grids_po",htmlentities(("Historique des factures commerçants")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("N°Facture")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Date création")),		"sortable" => true,  "excelable" => true, 	"align" => 'center', "format" => "date" ),
				array("name" => htmlentities("Vendeur"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Total avahis")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', 'total' => true, "format" => "money" ),
				array("name" => htmlentities(("Total à transférer")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', 'total' => true, "format" => "money" ),
				array("name" => htmlentities(("Date limite")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', 'format' => 'number', "format" => "date" ),
				array("name" => htmlentities("Payment"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities("Actions"),    "sortable" => false, "excelable" => false,  "align" => 'center' ),
				array("name" => htmlentities("Infos"),    "sortable" => false, "excelable" => false,  "align" => 'center', "width" => '5%'),
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '3%')
			);
		return $headers;
	}

	public function _makeContent()
	{
	    $factureManager = new ManagerFacture();
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		//var_dump($this->_sql_query);
		foreach($res as $facture)
		{
			$line =
				array(
					$facture['facture_code'],
					$facture['date_created'],
					$facture['vendor_nom'],
					$facture['grand_total'],
					$facture['mt_a_transferer'],
					$facture['date_limit'],
				);
                
            $factureObj = $factureManager -> getFactureById($facture["facture_id"]);
            $itemList = $factureObj -> getAllItems();
            
            $itemCount = count($itemList);
            $c = 0;    
            foreach ($itemList as $item ) {
                if( ! $item -> getLitiges() && $item -> getPayment()){
                    $c++;
                }       
            }
            if($c == 0){
                $info_payment = "unpaid";
            }elseif($c > 0 && $c != $itemCount){
                $info_payment = "partiel";
            }elseif($c == $itemCount){
                $info_payment = "complete";
            }
            
            
			$action = '';
            $infos = '';
			$num = "facture_".$facture['facture_id'];
			
			if($root_path){
				$action .= "<a class='btn' href='". $root_path . "app.php/commercants/downloadfact?id=".$facture['facture_id']."' ><i class='fa fa-file icon-black' style='padding-right: 0px' > Voir facture </i></a>";
			}else{
				$action .= "<a class='btn' href='". $root_path . "downloadfact?id=".$facture['facture_id']."' ><i class='fa fa-file icon-black' style='padding-right: 0px' ></i> Voir facture </a>";
			}
            
            $action .= "<a class='btn' href='#' onClick='payFacture(".$facture['facture_id'].")'><i class='fa fa-euro icon-black' style='padding-right: 0px'></i> Payer </a>";
            
            $action .= "<a class='btn' href='#' onClick='sendMailFact(".$facture['facture_id'].")'><i class='fa fa-envelope icon-black' style='padding-right: 0px'></i> Mail </a>";
            
            if($facture['email_send']){
                $infos .= "<em>Email envoyé</em>";
            }
             
            $line[] = getLabelPayment($info_payment, $facture["facture_id"]);
			$line[] = $action;
            $line[] = $infos;
			
			$line[] = '<input id="facture_' . $facture["facture_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
            
            
		}
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT *  
					FROM   
					skf_factures f  
					INNER JOIN skf_factures_com fc ON f.facture_id = fc.facture_id 
					INNER JOIN sfk_vendor v ON v.vendor_id = fc.vendor_id";
		
	
		$where = "";        
		
		if ( isset($this->facturecode ) && ($this->facturecode != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " f.facture_code = '$this->facturecode' ";
        }
        
		if ( isset($this->numvendor ) && ($this->numvendor  != "all") && ($this->numvendor != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " fc.vendor_id = '$this->numvendor' ";
        }
		
		if ( isset($this->commfilter ) && ($this->commfilter != "all") && ($this->commfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " fc.vendor_id = '$this->commfilter' ";
        }
        
        if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
            
            $date = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 3 DAY)');
            $date = $date['MIN(date_value)']; 
           
			$where .= " po.date_created < ('$date' - INTERVAL 15 DAY) and po.payment <> 'complete' ";
        }
        
        if( isset($this -> datelimitfilter) && ($this -> datelimitfilter != "all")){
                  
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            
            $where .= " fc.date_limit  = '".$this -> datelimitfilter."'";
        }
		
        
        $order =  "   ORDER BY date_created DESC"; 
		
        $this->_sql_query = $select . $where . $order;       
    }
    
    
	public function update($post)
    {

        $this->__construct($post['facturecode'], $post['numvendor'], $post['commfilter'], $post['datelimitfilter']);
    }
}
?>