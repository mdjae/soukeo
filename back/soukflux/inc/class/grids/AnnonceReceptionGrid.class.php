<?php 
/**
 * Class pour grid Grossiste
 */
class AnnonceReceptionGrid extends Grid
{
	protected $numfilter;
	protected $datefilter;
	
	public function __construct( $numfilter="", $datefilter ="all")
	{	
		$this->_name = 'annonce_reception';
		$this->numfilter 		= $numfilter ;
		$this->datefilter 		= $datefilter;

		$this->_setSqlQuery();

		$this->_init('annonce_reception',$this->_makeHeaders(),$this->_makeContent());
		

		$this->setTitle("NoBg","grid_annonce_reception",htmlentities(("Listing Des Annonces de Réception")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(""),				"sortable" => false, "excelable" => false , "width" => '6%'),
				array("name" => htmlentities("Num Annonce"),	"sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Date Création"),	"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'date'),
				array("name" => htmlentities("Date Reception"),	"sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Etat"),			"sortable" => true,  "excelable" => false, 	"align" => 'center'),		
				array("name" => htmlentities("Actions"),		"sortable" => false, "excelable" => false,	"align" => 'center'),
				array("name" => htmlentities(""), 				"sortable" => false, "excelable" => false,	"align" => 'center', 	"width" => '1%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
		
		$content = array();
		
		foreach($res as $ar)
		{
				
			if($ar['date_reception']){
				$date_recep = "<span id='".$ar['annonce_reception_id']."_daterecep'>".smartdate($ar['date_reception'])."</span>";
			}else{
				$date_recep = "<span id='".$ar['annonce_reception_id']."_daterecep'></span>";
			}
			
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='ar_".$ar['annonce_reception_id']."' onclick='showAnnonceRepInfo(this)'  />
					 ",
					$ar['annonce_reception_code'],
					$ar['date_created'],
					$date_recep,
					$this -> getDropDownButton($ar['status'], $ar['annonce_reception_id']),
				);
				
			$state 	= "";
			$action = "";
			
			$num = "ar_".$ar['annonce_reception_id'];
			
			$action .= '<a href="#" onclick="deleteIt(\''.$num.'\');return false;"> 
						<img border="0" alt="' . ("Supprimer de la commande") . '" 
						align="absmiddle" title="Supprimer de la commande"  class="iconAction" src="' . $skin_path . 'images/picto/trash.gif"></a> ';
						
			$line [] = $action;
			$line [] = '<input id="ar_' . $ar["annonce_reception_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT *
					FROM   
					skf_annonce_reception ";
		$where = "";
        $order = ""; 

		if ( isset($this->numfilter ) && ($this->numfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " annonce_reception_code = '$this->numfilter' ";
        }

        if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " date_created > (NOW() - INTERVAL ".$this->datefilter." DAY) ";
        }

		$order =  "";
		
        $this->_sql_query = $select . $where . $order; 

    }
    
    
    public function update($post)
    {
        $this->__construct($post['numfilter'], $post['datefilter'] );
    }
	
	protected function getDropDownButton($status, $annonce_reception_id)
	{
		
		switch ($status) {
			case 'new':
				$class = "btn-info";	
			break;
			case 'receptionne':
				$class = "btn-success";	
			break;
			case 'transit':
				$class = "btn-warning";	
			break;
			
			case 'canceled':
				$class = "btn-danger";	
			break;
			
			default:
				
			break;
		}
		
		$html = '<div id="status_'.$annonce_reception_id.'" class="btn-group">
					<a class="btn dropdown-toggle '.$class.'" style="color:white" data-toggle="dropdown" href="#">
						'.ucfirst($status).'
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#" onclick="changeStatusAR(\''.$annonce_reception_id.'\', \'New\')">New</a></li>
						<li><a href="#" onclick="changeStatusAR(\''.$annonce_reception_id.'\', \'Transit\')">Transit</a></li>
						<li><a href="#" onclick="changeStatusAR(\''.$annonce_reception_id.'\', \'Receptionne\')">Receptionné</a></li>
						<li><a href="#" onclick="changeStatusAR(\''.$annonce_reception_id.'\', \'Canceled\')">Canceled</a></li>
					</ul>
				 </div>';
		return $html;
	}
}
?>