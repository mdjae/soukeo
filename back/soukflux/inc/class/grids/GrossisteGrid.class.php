<?php 
/**
 * Class pour grid Grossiste
 */
class GrossisteGrid extends Grid
{
	public function __construct($nom='',$fSoft='',$fNbProd='')
	{	
		$this->_name = 'grossiste';
		$this->_fSoft= $fSoft ;
		$this->_nom = $nom ;
		$this->_fNbProd = $fNbProd ;
		//$this->_sql_query = $sql_query;
		$this->_setSqlQuery();
		$this->_init('grossiste',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_ecommercant",htmlentities(("Qualification grossistes")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("N°")),						"sortable" => true, "excelable" => false, "align" => 'center', "width" => '5%'),
				array("name" => 			("Nom"),						"sortable" => true, "excelable" => false),
				array("name" => htmlentities(("Dernière M.A.J.")),			"sortable" => true, "excelable" => false, "align" => 'center', "width" => '30%'),
				array("name" => htmlentities(("TECH")),						"sortable" => true, "excelable" => false, "align" => 'center'),
				array("name" => htmlentities(("Nb Prod Actifs")),			"sortable" => true, "excelable" => false, "align" => 'center'),
				array("name" => htmlentities(("Nb Prod Associés Actifs")),	"sortable" => true, "excelable" => false, "align" => 'center'),
				array("name" => htmlentities(("Nb Prod Total")),			"sortable" => true, "excelable" => false, "align" => 'center'),
				array("name" => htmlentities(("Nb Prod Associés Total")),	"sortable" => true, "excelable" => false, "align" => 'center'),
				array("name" => htmlentities(("Nb Prod Vendus")),	"sortable" => true, "excelable" => false, "align" => 'center'),
				array("name" => htmlentities(("Actif")),		"sortable" => true,  "excelable" => true,"align" => 'center',"width" => '6%'					),
				array("name" => 			("Actions"),					"sortable" => false, "excelable" => false,"align" => 'center'),
				array("name" => htmlentities(("")), 			"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $ecomm)
		{
			if (!$ecomm['ACTIVE'])
			{
				$locked_html = '<img id ="'.$ecomm['GROSSISTE_ID'].'" border="0" alt="'. ("Profil innactif")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/denied.gif">';
				$locked_txt = ("Oui");
			}
			else
			{
				$locked_html = '<img id ="'.$ecomm['GROSSISTE_ID'].'" border="0" alt="'. ("Profil actif")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/check.gif">';
				$locked_txt = ("Non");
			}
			
			$nbProds 			= $ecomm['NBPRODS'];
			$nbProds_tot 		= $ecomm['NBPRODS_TOT'];
			$nbProdsAssoc 		= $ecomm['NBPRODS_ASS'];
			$nbProdsAssoc_tot 	= $ecomm['NBPRODS_ASS_TOT'];
			$nbProdsSold 		= $ecomm['NBPRODS_SOLD'];
			
			$line =
				array(
					$ecomm['GROSSISTE_ID'],
					$ecomm['CLASS_NAME'],
					smartDate($ecomm['DATE_UPDATE_PRODUCT']),
					$ecomm['TECH'],
					$nbProds,
					$nbProdsAssoc,
					$nbProds_tot,
					$nbProdsAssoc_tot,
					$nbProdsSold,
					array('html' => $locked_html, 'txt' => $locked_txt),
				);

			$action = '';
			$action .= '<a href="/app.php/grossistes/grossiste/assocdealer?id='.$ecomm['GROSSISTE_ID'].'&tech='.$ecomm['CLASS_NAME'].'" onclick="testLoader();"> 
						<img border="0" alt="' . ("Associations catégories et produits") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
			
			$action .= '<a href="/app.php/grossistes/categoriesgrossiste?id='.$ecomm['GROSSISTE_ID'].'" onclick="testLoader();"> 
						<img border="0" alt="' . ("Etat des catégories grossiste") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/addressbook.gif"></a>';						

			$line[] = $action;
			$line[] = '<input id="vid_' . $ecomm["GROSSISTE_ID"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		
		return $content;
	}
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @param $id2
	 * @param $id3
	 * @return boolean
	 */
	public function delete($id1, $id2, $id3)
	{
		// Suppression des logs
		$query = "DELETE FROM sfk_vendor
					WHERE VENDOR_ID = " . $id1;
		dbQuery($query);
		
		return true;
	}
	protected function _setSqlQuery(){
		
		$select =  "SELECT g.GROSSISTE_ID, CLASS_NAME, TECH, GROSSISTE_CSV , DATE_UPDATE_PRODUCT, g.ACTIVE, 
					(SELECT count(*) FROM sfk_produit_grossiste as pg 
						WHERE pg.GROSSISTE_ID = g.GROSSISTE_ID AND pg.ACTIVE = 1 
 					) as NBPRODS , 
 					(SELECT count(*) FROM sfk_produit_grossiste as pg 
						WHERE pg.GROSSISTE_ID = g.GROSSISTE_ID
 					) as NBPRODS_TOT , 
 					(SELECT count(*) FROM  sfk_assoc_product_grossiste as ap 
 					INNER JOIN sfk_produit_grossiste as pg ON ap.id_produit_grossiste = pg.ID_PRODUCT 
							WHERE ap.GROSSISTE_ID = g.GROSSISTE_ID AND pg.ACTIVE = 1
					) as NBPRODS_ASS , 
 					(SELECT count(*) FROM  sfk_assoc_product_grossiste as ap 
 					INNER JOIN sfk_produit_grossiste as pg ON ap.id_produit_grossiste = pg.ID_PRODUCT 
							WHERE ap.GROSSISTE_ID = g.GROSSISTE_ID 
					) as NBPRODS_ASS_TOT ,
					(SELECT COUNT( * ) 
						FROM sfk_produit_grossiste AS pg
						INNER JOIN sfk_assoc_product_grossiste AS ap ON ap.id_produit_grossiste = pg.ID_PRODUCT
						INNER JOIN sfk_catalog_product AS cp ON cp.id_produit = ap.id_avahis
						INNER JOIN sfk_product_ecommercant_stock_prix sp ON cp.sku = sp.id_produit
						WHERE pg.GROSSISTE_ID = g.GROSSISTE_ID
						AND pg.ACTIVE =1
						AND sp.QUANTITY >0
						AND sp.VENDOR_ID =24)
					as NBPRODS_SOLD 
					FROM `sfk_grossiste` as g ";
 


        $order =  " ORDER BY g.GROSSISTE_ID "; 
        $this->_sql_query = $select . $where . $order;       
       //var_dump($this->_sql_query);
	   //echo $this->_sql_query ;
    }
    
    
        public function update($post)
    {

        $this->__construct($post['nom'],$post['fSoft'],$post['fNbProd']);
    }
}
?>