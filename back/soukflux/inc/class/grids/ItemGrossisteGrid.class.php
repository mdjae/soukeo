<?php 
/**
 * Class pour grid Grossiste
 */
class ItemGrossisteGrid extends Grid
{
	protected $vendor_id;
	protected $order_id_grossiste;
	
	
	public function __construct($order_id_grossiste, $vendor_id, $interne = false )
	{	
		$this->_name = 'item_grossiste';
		$this->order_id_grossiste = $order_id_grossiste;
		$this->vendor_id = $vendor_id;
		$this->_setSqlQuery();
		$this->_init('item_grossiste',$this->_makeHeaders(),$this->_makeContent(), $interne);
		$js = array("$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});");
		$this->setJavascript($js);
		$this->setTitle("NoBg","grids_item",htmlentities(("Listing Produit PO")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("N° Comm Avahis")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("name"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("SKU")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("prix client")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 , "format"=>'money' ),
				array("name" => htmlentities(("prix grossiste")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 , "format"=>'money' ),
				array("name" => htmlentities(("qty")),	"sortable" => true,  "excelable" => true, 	"align" => 'center',"total" => true	 , "format"=>'number'  ),
				array("name" => htmlentities(("Poids")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true , "format"=>'number' 	),
				
			);
			
		if($this->vendor_id == "24"){
			
			$headers[] = array("name" => htmlentities("Grossiste"),	"sortable" => false, "excelable" => false,	"align" => 'center' );
			$headers[] = array("name" => htmlentities("Ref Grossiste"),	"sortable" => false, "excelable" => false,	"align" => 'center' );
		}
			$headers[] = array("name" => htmlentities("Etat"),	"sortable" => false, "excelable" => false,	"align" => 'center' ) ;
		$headers[] = array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ) ;
		
		$headers[] = array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '2%');
		
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		//var_dump($this->_sql_query);
		foreach($res as $item)
		{
		 	if($this -> vendor_id == "24"){
				$sql = "SELECT * FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID WHERE cp.sku = '".$item['sku']."' ";
			
				$result = dbQueryAll($sql);

				$prod_gr = $result[0];	
				$nomGr = $prod_gr['GROSSISTE_NOM'];
				$refGR = $prod_gr['REF_GROSSISTE'];
				$prixGr = $prod_gr['PRICE_PRODUCT'];
			}	
			
			$noCommAv = '<a href="/app.php/avahis/commande_avahis?id='.$item['increment_id'].'" target="_blank">'.
									$item['increment_id'].'
								</a>';
			
			$line =
				array(
					$noCommAv,
					htmlentities($item['name']),
					$item['sku'],
					$item['base_row_total_incl_tax'],
					$prixGr * $item['qty_ordered'],
					$item['qty_ordered'],
					$item['row_weight'],
					$nomGr,
					$refGR
				);
			
			$state = "";
			
			if($item['litiges']){
				$state.= '<span id="item_'.$item['item_id'].'_etat" class="etat_order label label-warning"> litige </span>';
			}else{
				$state.= '<span id="item_'.$item['item_id'].'_etat" class="etat_order label label-warning"></span>';
			}
			
			if($item['annonce_reception_id'] != "" && $item['annonce_reception_id'] !== null && strtolower($item['annonce_reception_id']) != "null"){
				$info_recep = dbQueryAll("SELECT * FROM skf_annonce_reception WHERE annonce_reception_id = ".$item['annonce_reception_id']);
				$info_recep = $info_recep[0];
				$state.= '<span id="item_'.$item['item_id'].'_reception" class="etat_order label label-inverse">A.R. :  
								<a href="/app.php/grossistes/annonce_reception?id='.$info_recep['annonce_reception_code'].'" target="_blank">'.
									$info_recep['annonce_reception_code'].'
								</a>
							  </span>';
			}else{
				$state.= '<span id="item_'.$item['item_id'].'_reception" class="etat_order label label-inverse"></span>';
			}

			$line[] = $state;
			
			$action = '';
			$num = "itemcomgr_".$item['item_id'];

			$action .= '<a href="#" onclick="deleteIt(\''.$num.'\');return false;"> 
						<img border="0" alt="' . ("Supprimer de la commande") . '" 
						align="absmiddle" title="Supprimer de la commande"  class="iconAction" src="' . $skin_path . 'images/picto/trash.gif"></a>';
			$action .= '<span class="popover-markup"> 
								<a class="trigger" id="popover_'.$item['item_id'].'">
									<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Avis de réception" alt="Avis de réception">
								</a> 
							    <div class="head hide">
							    	<button type="button" id="close" class="close" onclick="$(&quot;#popover_'.$item['item_id'].'&quot;).popover(&quot;hide&quot;);">&times;</button>
							    	<br>
							    	<h5>Avis de Réception : <h5>
							    </div>
							    <div class="content hide">
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="Avis de réception…" id="popoverinput_'.$item['item_id'].'">
							        </div>
							        <button type="submit" class="btn btn-primary btn-block" onClick="assignItemAvisRecept(&quot;'.$item['item_id'].'&quot;);"><b>OK</b></button>
							    </div>
							</span>';
						
			$line[] = $action;
			
			$line[] = '<input id="itemcomgr_' . $item["item_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT s0.sku, s0.name, s0.base_row_total_incl_tax, s0.qty_ordered,  s0.row_weight, s0.litiges, s0.item_id , s0.order_id_grossiste, so.increment_id, s0.annonce_reception_id  
					FROM   
					skf_order_items s0 INNER JOIN skf_po s1  ON s0.order_id = s1.order_id  AND s0.udropship_vendor = s1.vendor_id
					INNER JOIN  sfk_vendor s2 on s0.udropship_vendor = s2.VENDOR_ID 
					INNER JOIN skf_orders so ON so.order_id = s0.order_id  
 					";

		
		$where = "WHERE s0.order_id_grossiste = '".$this->order_id_grossiste."'   
					AND s2.VENDOR_ID = '".$this->vendor_id."'
		
		";
        $order =  "  ORDER BY so.increment_id"; 
		
        $this->_sql_query = $select . $where . $order;       
        
        //var_dump($this->_sql_query);
	
    }
    
    
        public function update($itemst)
    {

        $this->__construct($post['nom'],$post['fSoft'],$post['fNbProd']);
    }
}
?>