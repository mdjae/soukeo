<?php 
/**
 * Class pour grid Grossiste
 */
class AttributsGrid extends Grid
{
	protected $cat_id;
	
	public function __construct($cat_id)
	{	
		$this->_name = 'attributs';
		$this->cat_id = $cat_id;

		$this->_setSqlQuery();
		$this->_init('attributs',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_attributs",htmlentities(("Attributs")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("Id")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Code"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Label")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 , "format"=>'money' ,"width" => '8%'),
				array("name" => htmlentities(("Nb présence")),	"sortable" => true,  "excelable" => true, 	"align" => 'center',"total" => true	 , "format"=>'number' ,"width" => '8%' ),
				
			);
			
		$headers[] = array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '2%');
		
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
		$factureManager = new ManagerFacture();
		$content = array();
		$javascript = array();
		//var_dump($this->_sql_query);
		foreach($res as $attr)
		{
		 
			$line =
				array(
					htmlentities($attr['id_attribut']),
					htmlentities($attr['code_attr']),
					htmlentities($attr['label_attr']),
					"test",
				);
				
			$state = "";

			
			$line[] = '<input id="attr_' . $attr["id_attribut"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT DISTINCT a.id_attribut, a.code_attr, a.label_attr    
					FROM   
					sfk_attribut a 
					INNER JOIN  sfk_product_attribut pa on pa.id_attribut = a.id_attribut 
					INNER JOIN  sfk_catalog_product  cp on cp.id_produit = pa.id_produit 
					WHERE cp.categories = ".$this-> cat_id." 
 					";

		
		$where = "";
        
        $order =  "  ORDER BY a.label_attr"; 
		
        $this->_sql_query = $select . $where . $order;       
	
    }
    
    
        public function update()
    {

        $this->__construct($post['cat_id']);
    }
}
?>