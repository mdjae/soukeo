<?php 
/**
 * Class pour grid Grossiste
 */
class PoGrid extends Grid
{
    protected static $db_av;
    
	public function __construct($order_id, $interne = false )
	{
	    self::$db_av = new BusinessAvahisModel();
        	
		$this->_name = 'po';
		$this->order_id = $order_id;
		$this->interne = $interne;
		$this->_setSqlQuery();
		$this->_init('po',$this->_makeHeaders(),$this->_makeContent(), $this->interne   );
		$this->setTitle("NoBg","grids_po",htmlentities(("Bon de commande Commercant")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("")),			"sortable" => false, "excelable" => false, 	"align" => 'center' ),
				array("name" => htmlentities(("N°")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Vendeur"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("total qty")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', 'format' => 'number' ),
				array("name" => htmlentities(("Status")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Livraison ")),   "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("total Poids")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', 'format' => 'number' ),
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities("Etat"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		//var_dump($this->_sql_query);
		foreach($res as $po)
		{
		    
    		$tmp = explode("_", $po["udropship_method"], 2);
            
            if($rs = self::$db_av->getUdropship_shipping($tmp[1])){
                $rs = $rs[0];
                $livraison = $rs['shipping_title']. " - ".$rs['days_in_transit']. " jours";    
            }
            else{
                $livraison = $tmp[1];
            }
            
         
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='item_".$po['order_id']."_".$po['vendor_id']."'  onclick='showChild(this)'  />",
					$po['increment_id'],
					htmlentities($po['vendor_nom']),
					$po['total_qty'],
					$po['udropship_status'],
					$livraison,
					$po['total_weight'],
				);

			$action = '';
			$num = "po_".$po['po_id'];
            
			if($po['litiges']){
				$action .= '<a id="litigeit_'.$po['po_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un litige ") . '" 
							align="absmiddle" title="Annuler litige"  class="iconAction" src="' . $skin_path . 'images/picto/check.gif"></a>';
			}else{			
				$action .= '<a id="litigeit_'.$po['po_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un litige ") . '" 
							align="absmiddle" title="Daclarer litige"  class="iconAction" src="' . $skin_path . 'images/picto/denied.gif"></a>';
			}

			$line[] = $action;
			
             
			
            $state = "";
			
			if($po['litiges']){
				$state = '<span id="po_'.$po['po_id'].'_etat" class="etat_order label label-warning"> litige </span>';
			}else{
				$state = '<span id="po_'.$po['po_id'].'_etat" class="etat_order label label-warning"></span>';
			}
			
            $state .= getPoStateShipping($po['udropship_status']);
            
            $line[] = $state;
            
			$line[] = '<input id="po_' . $po["po_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT *  
					FROM   
					skf_po s1  inner join sfk_vendor s2 on s1.vendor_id = s2.vendor_id ";
		
		$where = "WHERE order_id = '".$this->order_id."'   ";
        
        $order =  "   ORDER BY increment_id "; 
		
        $this->_sql_query = $select . $where . $order;       

    }
    
    
        public function update($post)
    {

        $this->__construct($post['nom'],$post['fSoft'],$post['fNbProd']);
    }
}
?>