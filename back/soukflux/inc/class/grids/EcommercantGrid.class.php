<?php 
/**
 * Class pour grid Ecommercant
 */
class EcommercantGrid extends Grid {
	public function __construct($nom='',$fSoft='',$fNbProd='',$fNbProdAss='',$fActive='1',$fDelete ='0')
	{	
		$this->_name = 'ecommercant';
		$this->_nom = $nom ;
		$this->_fSoft= $fSoft ;
		$this->_fNbProd = $fNbProd ;
		$this->_fNbProdAss = $fNbProdAss ;
		$this->_fActive = $fActive ;
		//$this->_fDelete = $fDelete ;
		$this->_fDelete = SystemParams::getParam('system*select_ecom_delete') ==N ? '0':$fDelete ;
		//$this->_sql_query = $sql_query;
		$this->_setSqlQuery();
		$this->_init('ecommercant',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_ecommercant",htmlentities(("Qualification e-commerçants")));
	}

	public function _makeHeaders() {
		$headers =
			array(
				array("name" => htmlentities(("N°")),			"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '5%'					),
				array("name" => 			( "Nom"),			"sortable" => true,  "excelable" => true														),
				array("name" => htmlentities(("SOFTWARE")),		"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '5%'					),
				array("name" => htmlentities(("DATE_INSERT")),	"sortable" => true,  "excelable" => true														),
				array("name" => htmlentities(("URL")),			"sortable" => true,  "excelable" => true														),
				array("name" => htmlentities(("Nb Prod")),		"sortable" => true,  "excelable" => true,"align" => 'center',"width" => '6%',"total" => true	),
				array("name" => htmlentities(("Nb ProdAss")),	"sortable" => true,  "excelable" => true,"align" => 'center',"width" => '8%',"total" => true	),
				array("name" => htmlentities(("Actif")),		"sortable" => true,  "excelable" => true,"align" => 'center',"width" => '3%'					),
				array("name" => 			( "Actions"),		"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%'),
				array("name" => 			( "Exports"),		"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%'),
				array("name" => 			( "Commentaires"),		"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%'),
				array("name" => htmlentities(("")), 			"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%')
				/*
				array("name" => '<img border="0" alt="'.htmlentities(("Vendeur activé ou innactivé"))  .'" align="absmiddle" class="icon" src="/skins/common/images/picto/denied.gif">',
				"excel_name" => ("Vendeur activé ou innactivé"),
				"sortable" => true, 
				"excelable" => true,
				 "align"=>'center'),
				*/
			);
		return $headers;
	}

	public function _makeContent() {
		global $root_path, $skin_path, $main_url;
        
        $vendorCsvManager = new ManagerVendorCsv();
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $ecomm)
		{
			$vendor_use_csv = false;
			
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$ecomm['entity_id'].' AND commented_object_table = "sfk_vendor"'));
			
			if($vendorcsv = $vendorCsvManager -> getBy(array("vendor_id" => $ecomm['VENDOR_ID']))){
			    $vendor_use_csv = true;
			}
            
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";;
			}
			
			//var_dump($ecomm);
			//eval('$class = ' . $prog['class'] . '::getDescription();');
			if (!$ecomm['ACTIVE'])
			{
				$locked_html = '<img border="0" alt="'. ("Profil innactif")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/denied.gif">';
				$locked_txt = ("Oui");
			}
			else
			{
				$locked_html = '<img border="0" alt="'. ("Profil actif")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/check.gif">';
				$locked_txt = ("Non");
			}
			//$resultNbProdsAssoc = $this->getNbrProdAssoc($ecomm['VENDOR_ID']);
			$nbProdsAssoc =$ecomm['NBPRODS_ASS'];
			$nbProds = $ecomm['NBPRODS_' . $ecomm['SOFTWARE']];
            if($vendor_use_csv){
                $nbProds = $ecomm['NBPRODS_CSV'];
            }
			
			$line =
				array(
					$ecomm['VENDOR_ID'],
					htmlentities($ecomm['VENDOR_NOM']),
					$ecomm['SOFTWARE'] ? $ecomm['SOFTWARE']: 'aucun',
					$ecomm['DATE_INSERT'] ? smartDate($ecomm['DATE_INSERT']):'',
					htmlentities($ecomm['VENDOR_URL']),
					$nbProds > 0 ? $nbProds : '',
					$nbProdsAssoc > 0 ? $nbProdsAssoc : '',
					array('html' => $locked_html, 'txt' => $locked_txt),
				);


			$action = '';
			$exports = '';
			
			$action .= '<a href="#" onclick = "openInfoVendor('.$ecomm['VENDOR_ID'].')"> 
						<img border="0" alt="' . ("accès au info vendeur") . '" title="Informations du vendeur"
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/search.gif"></a>';
			
			if ($ecomm['SOFTWARE']){
			     
				$action .= '<a href="/app.php/commercants/ecommercant/assoc?id='.$ecomm['VENDOR_ID'].'&tech='.$ecomm['SOFTWARE'].'"> 
							<img border="0" alt="' . ("Associations catégories et produits") . '" title="Associations catégories et produits"
							align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
				
				$arr = array ( 'url' 		 => '/commercants/ecommercant/import/index.php',
								'class' 	 => 'BusinessImport'.$ecomm['SOFTWARE'],
								'vendor_id'  => $ecomm['VENDOR_ID'], 
								'vendor_url' => $ecomm['VENDOR_URL'],
								'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
								
				$data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);		
				$tmp = "showDialog2('".$data."')";			
				$action .= '<a href="#" onclick ='.$tmp.'> 
				            <img border="0" align="absmiddle" alt="' . ("Exécuter le batch import") . '" title="Executer le batch d\'import" class="iconAction" src="' . $skin_path . 'images/picto/reload.gif"></a></p>';
					

                $arr = array ( 'url'         => '/commercants/ecommercant/importsp/index.php',
                                'class'      => 'BusinessSync'.ucfirst($ecomm['SOFTWARE'])."SP",
                                'vendor_id'  => $ecomm['VENDOR_ID'], 
                                'vendor_url' => $ecomm['VENDOR_URL'],
                                'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
                                
                $data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);          
                $tmp = "showDialog2('".$data."')";          
                $action .= '<a href="#" onclick ='.$tmp.'> 
                            <img border="0" align="absmiddle" alt="' . ("Exécuter la sync stock prix") . '" title="Executer le batch de synchronisation stock prix" class="iconAction" src="' . $skin_path . 'images/picto/dollar.gif"></a></p>';
                            
                            					
				$arr = array ( 'url' 		 => '/commercants/ecommercant/export/index.php',
								'class' 	 => 'BatchExportCatalogAvahis',
								'vendor_id'  => $ecomm['VENDOR_ID'],
								'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
				$data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);  
				$tmp = "showDialog2('".$data."')";			
				$exports .= '<a href="#" onclick ='.$tmp.'> 
					<img border="0" align="absmiddle" alt="' . ("Exporter produits CSV") . '" title="Exporter produits en CSV" class="iconAction" src="' . $skin_path . 'images/picto/grids_catalog.gif"></a>';
					
					
				$arr = array ( 'url' 		 => '/commercants/ecommercant/export/index.php',
								'class' 	 => 'BatchExportCatalogEcom',
								'vendor_id'  => $ecomm['VENDOR_ID'],
								'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
				$data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);  	
				$tmp = "showDialog2('".$data."')";			
				$exports .= '<a href="#" onclick ='.$tmp.'> 
					<img border="0" align="absmiddle" alt="' . ("Exporter Stock / Prix CSV") . '" title="Exporter stock/prix en CSV" class="iconAction" src="' . $skin_path . 'images/picto/dollar.gif"></a></p>';
					
			}
            elseif($vendor_use_csv){
                
                $action .= '<a href="/app.php/commercants/ecommercant/assoc?id='.$ecomm['VENDOR_ID'].'&tech=CSV"> 
                            <img border="0" alt="' . ("Associations catégories et produits") . '" title="Associations catégories et produits"
                            align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
                            
                $arr = array ( 'url'         => '/commercants/ecommercant/import/index.php',
                                'class'      => 'BusinessImportCSV',
                                'vendor_id'  => $ecomm['VENDOR_ID'], 
                                'vendor_url' => $vendorcsv->getVendor_csv_name(),
                                'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
                                
                $data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);            
                $tmp = "showDialog2('".$data."')";           
                $action .= '<a href="#" onclick ='.$tmp.'> 
                    <img border="0" align="absmiddle" alt="' . ("Exécuter le batch import") . '" title="Executer le batch d\'import" class="iconAction" src="' . $skin_path . 'images/picto/reload.gif"></a></p>';
                    
                $arr = array ( 'url'         => '/commercants/ecommercant/importsp/index.php',
                                'class'      => 'BusinessSyncCSVSP',
                                'vendor_id'  => $ecomm['VENDOR_ID'], 
                                'vendor_url' => $vendorcsv->getVendor_csv_name(),
                                'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
                                
                $data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);  
                $tmp = "showDialog2('".$data."')";          
                $action .= '<a href="#" onclick ='.$tmp.'> 
                            <img border="0" align="absmiddle" alt="' . ("Exécuter la sync stock prix") . '" title="Executer le batch de synchronisation stock prix" class="iconAction" src="' . $skin_path . 'images/picto/dollar.gif"></a></p>';
                                                
                $arr = array ( 'url'         => '/commercants/ecommercant/export/index.php',
                                'class'      => 'BatchExportCatalogAvahis',
                                'vendor_id'  => $ecomm['VENDOR_ID'],
                                'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
                $data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);  
                $tmp = "showDialog2('".$data."')";          
                $exports .= '<a href="#" onclick ='.$tmp.'> 
                    <img border="0" align="absmiddle" alt="' . ("Exporter produits CSV") . '" title="Exporter produits en CSV" class="iconAction" src="' . $skin_path . 'images/picto/grids_catalog.gif"></a>';
                    
                    
                $arr = array ( 'url'         => '/commercants/ecommercant/export/index.php',
                                'class'      => 'BatchExportCatalogEcom',
                                'vendor_id'  => $ecomm['VENDOR_ID'],
                                'vendor_nom' => str_replace(" ", "_", utf8_encode($ecomm['VENDOR_NOM'])));
                $data = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);  
                $tmp = "showDialog2('".$data."')";          
                $exports .= '<a href="#" onclick ='.$tmp.'> 
                    <img border="0" align="absmiddle" alt="' . ("Exporter Stock / Prix CSV") . '" title="Exporter stock/prix en CSV" class="iconAction" src="' . $skin_path . 'images/picto/dollar.gif"></a></p>';
            }
			
			$line[] = $action;
			$line[] = $exports;
			$line[] = "<a class='closed btn' href='#' id='comment_vendor_".$ecomm['entity_id']."' onclick='showComments(this)'><i class='fa fa-comment icon-black' style='padding-right: 0px'></i>$countComment</a>";
			$line[] = "<input id='vid_" . $ecomm["VENDOR_ID"] . "' type='checkbox'/>";
			
			$content[] = $line;
		}
		
		return $content;
	}
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @return boolean
	 */
	public function delete($id1 ) {
		// Suppression des logs
		$query = "DELETE FROM sfk_vendor
					WHERE VENDOR_ID = " . $id1;
		dbQuery($query);
		
		return true;
	}
	protected function _setSqlQuery(){
		
		$select =  "SELECT v.VENDOR_ID, v.VENDOR_NOM, tv.SOFTWARE, tv.DATE_INSERT, v.VENDOR_URL, v.ACTIVE, v.entity_id, 
						(SELECT count(*) FROM sfk_produit_ps as ps 
							WHERE ps.VENDOR_ID = v.VENDOR_ID AND ACTIVE = 1 
						) as NBPRODS_Prestashop, 
						(SELECT count(*) FROM sfk_produit_th as th 
							WHERE th.VENDOR_ID = v.VENDOR_ID AND ACTIVE = 1 
						) as NBPRODS_Thelia, 
						(SELECT count(*) FROM sfk_produit_vm as vm 
							WHERE vm.VENDOR_ID = v.VENDOR_ID AND ACTIVE = 1 
						) as NBPRODS_Virtuemart,
						(SELECT count(*) FROM sfk_produit_csv as csv 
							WHERE csv.VENDOR_ID = v.VENDOR_ID AND ACTIVE = 1 
						) as NBPRODS_CSV,
						(SELECT count(*) FROM  sfk_assoc_product as ap
							WHERE ap.VENDOR_ID = v.VENDOR_ID 
						) as NBPRODS_ASS
					FROM `sfk_vendor` as v 	LEFT JOIN `sfk_tracking_vendor` as tv 	ON v.VENDOR_ID = tv.VENDOR_ID ";
 					
        if ( isset($this->_nom ) && ($this->_nom != "all") && ($this->_nom != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " UPPER(v.VENDOR_NOM) like upper(('". $this->_nom ."%')) ";
        }

        if ( isset($this->_fNbProd) && ($this->_fNbProd != 'all') && ($this->_fNbProd != ''))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " (SELECT count(*) FROM sfk_produit_ps as ps WHERE ps.VENDOR_ID = v.VENDOR_ID ) >= '".$this->_fNbProd."' 
						OR 
						(SELECT count(*) FROM sfk_produit_th as th WHERE th.VENDOR_ID = v.VENDOR_ID ) >= '".$this->_fNbProd."' 
						OR 
						(SELECT count(*) FROM sfk_produit_vm as vm WHERE vm.VENDOR_ID = v.VENDOR_ID ) >= '".$this->_fNbProd."' 
						OR
						(SELECT count(*) FROM sfk_produit_csv as csv WHERE csv.VENDOR_ID = v.VENDOR_ID ) >= '".$this->_fNbProd."'";
           
        }
		if ( isset($this->_fNbProdAss) && ($this->_fNbProdAss != 'all') && ($this->_fNbProdAss != ''))
        {
			if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
			$where .= "(SELECT count(*) FROM  sfk_assoc_product as ap WHERE ap.VENDOR_ID = v.VENDOR_ID ) >= '".$this->_fNbProdAss."'" ;
		}  
        if ( isset($this->_fSoft ) && ($this->_fSoft != "all")&& ($this->_fSoft != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
			
			if ($this->_fSoft!='aucun'){
            	$where .= "   tv.SOFTWARE = '".$this->_fSoft."' ";
			}
			else{
				$where .= " v.VENDOR_ID NOT IN ( SELECT tv.VENDOR_ID FROM sfk_tracking_vendor as tv) ";
			}
        }
         if ( isset($this->_fActive ) && ($this->_fActive != "all")&& ($this->_fActive != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " v.ACTIVE = '".$this->_fActive."' ";
        }
		if ( isset($this->_fDelete ) && ($this->_fDelete != "all")&& ($this->_fDelete != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " v.DELETE = '".$this->_fDelete."' ";
        }

        $order =  " GROUP BY v.VENDOR_ID "; 
        $this->_sql_query = $select . $where . $order;       

    }
    
    
	public function update($post) {
		
		$this->__construct($post['nom'],$post['fSoft'],$post['fNbProd'],$post['fNbProdAss'],$post['fActive'],$post['fDelete']);
    }
  
}
?>