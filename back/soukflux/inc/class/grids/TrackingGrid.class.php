<?php 
/**
 * Class pour grid qualification tracking
 */
class TrackingGrid extends Grid {
	public function __construct($vendor_id='',$fSoft='',$date1='', $date2='')
	{	
		$this->_name = 'tracking';
		$this->_vendor_id = $vendor_id ;
		$this->_fSoft = $fSoft ;
        $this->_date1 = $date1;
        $this->_date2 = $date2;
		$this->_setSqlQuery();
		$this->_init('tracking',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","type",htmlentities(("Qualification tracking")));
	}

	public function _makeHeaders() {
		$headers =
			array(
				array("name" => htmlentities(("Vendor id")),	"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '7%'	),
				array("name" => htmlentities(("DATE_INSERT")),	"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '20%'	),
				array("name" => htmlentities(("URL")),			"sortable" => true,  "excelable" => true,"align" => 'center'					),
				array("name" => htmlentities(("SOFTWARE")),		"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '5%'	),
				array("name" => htmlentities(("VERSION")),		"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '5%'	),
				array("name" => htmlentities(("VERSION Plugin")),		"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '5%'	),
				array("name" => 			( "Actions"),		"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%'	),
				array("name" => htmlentities(("")), 			"sortable" => false, "excelable" => false,"align" => 'center', "width" => '3%')
			);
		return $headers;
	}

	public function _makeContent() {
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $tracking)
		{
			//var_dump($tracking);
			//eval('$class = ' . $prog['class'] . '::getDescription();');
			/*
			if (!$tracking['ACTIVE'])
			{
				$locked_html = '<img border="0" alt="'. ("Profil innactif")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/denied.gif">';
				$locked_txt = ("Oui");
			}
			else
			{
				$locked_html = '<img border="0" alt="'. ("Profil actif")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/check.gif">';
				$locked_txt = ("Non");
			}
			 */
			//$resultNbProdsAssoc = $this->getNbrProdAssoc($tracking['VENDOR_ID']);
			// $V['sku'] ? $V['sku'] : $null
			$out2 = "";
			$tmp = explode ('^', $tracking['DATE_INSERT'] );
			foreach ($tmp as $T ){
				$out2 .= smartDate($T)."<br/>";
			}
			
			$line =
				array(
					$tracking['VENDOR_ID'],
					$out2,
//					$tracking['DATE_INSERT'] ? smartDate($tracking['DATE_INSERT']):'',
					$tracking['URL'],
					$tracking['SOFTWARE'] ? $tracking['SOFTWARE']: 'aucun',
					$tracking['VERSION'],
					$tracking['VERSION_PLUGIN'],
					//array('html' => $locked_html, 'txt' => $locked_txt),
				);


			$action = '';
			if ($this->_verifVendorId($tracking['VENDOR_ID'])){ 
			$action .= '<a onclick = "openInfoVendor('.$tracking['VENDOR_ID'].')"> 
						<img border="0" alt="' . ("accès au info vendeur") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/search.gif"></a>';
			}
			/*
			
			$action .= '<a href="/app.php/qualification/tracking/listeProduits?id='.$tracking['VENDOR_ID'].'&tech='.$tracking['SOFTWARE'].'"> 
						<img border="0" alt="' . ("accès à la liste des produits pour ce commercant") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/grids_catalog.gif"></a>';
			
			$action .= '<a href="/app.php/qualification/tracking/assoc?id='.$tracking['VENDOR_ID'].'&tech='.$tracking['SOFTWARE'].'"> 
						<img border="0" alt="' . ("Associations catégories et produits") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
			*/
			//$action .= "<a href=\"#\" onClick=\"confirmDelete('" . ("Etes-vous certain de vouloir supprimer ce commerçant ?") . "', " . $tracking['VENDOR_ID'] . ", '', '', '" . $this->_name . "', '" . $root_path . "')\"> 
				//<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Suppression d'un commerçant") . "\" class=\"iconAction\" src=\"" . $skin_path . "images/picto/open.gif\"></a>";
			
			$line[] = $action;
			$line[] = '<input id="vid_'.$tracking["VENDOR_ID"].'~'.$tracking["DATE_INSERT"].'" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	/**
	 * Construit la requéte sql en fonction des filstres
	 * @return String la reaquéte sql
	 */

	protected function _setSqlQuery(){
		
		$select =  "SELECT VENDOR_ID, group_concat( DATE_INSERT SEPARATOR '^' ) as DATE_INSERT , group_concat( URL SEPARATOR '<br/>' ) as URL, 
					group_concat( SOFTWARE SEPARATOR '<br/>' ) AS SOFTWARE,group_concat(  VERSION SEPARATOR '<br/>' ) as VERSION, group_concat(  VERSION_PLUGIN SEPARATOR '<br/>' ) as VERSION_PLUGIN 
					FROM `sfk_tracking_vendor` ";
 					
        if ( isset($this->_vendor_id ) && ($this->_vendor_id != "all") && ($this->_vendor_id != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " UPPER(VENDOR_ID) like upper(('". $this->_vendor_id ."%')) ";
        }

        
        if ( isset($this->_fSoft ) && ($this->_fSoft != "all")&& ($this->_fSoft != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
           // var_dump($this->_fSoft);
            $where .= "   SOFTWARE = '".$this->_fSoft."' ";
        }
  		if ( isset($this->_date1) && ($this->_date1 != 'all') && ($this->_date1 != ''))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " CONCAT(YEAR(DATE_INSERT), '-', DATE_FORMAT(DATE_INSERT, '%m'), '-', DATE_FORMAT(DATE_INSERT, '%d'), ' 00:00:00') >= '" . dbdate($this->_date1) . "'\n";
        }

        if ( isset($this->_date2) && ($this->_date2 != 'all') && ($this->_date2 != ''))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " CONCAT(YEAR(DATE_INSERT), '-', DATE_FORMAT(DATE_INSERT, '%m'), '-', DATE_FORMAT(DATE_INSERT, '%d'), ' 00:00:00') <= '" . dbdate($this->_date2) . "'\n";
        }

        $order =  " GROUP BY VENDOR_ID ORDER BY VENDOR_ID"; 
        $this->_sql_query = $select . $where . $order;       
		//echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
		//var_dump($this->_sql_query);
		//=echo $this->_sql_query ;
    }
    
   /**
	 * lorsqu'une demande de rafraichissement est demandé les paramétre sont retourné
    * @param $post array les paramétres de filtrage passé en post 
 	*/
	public function update($post) {
		//var_dump($post);
		$this->__construct($post['vendor_id'],$post['fSoft'],$post['date1'],$post['date2']);
    }
	/**
	 * Vérifie si un vendeur existe dans la bdd sfk_vendor
	 * @param $vid string l'id vendeur a vérifier
	 */
	public function _verifVendorId($vid){
		$sqlUrlTracking = "SELECT `VENDOR_ID` FROM sfk_vendor WHERE VENDOR_ID = ".$vid." ";
		//echo $sqlUrlTracking ;
		return dbQueryAll($sqlUrlTracking);

	}
	
}
?>