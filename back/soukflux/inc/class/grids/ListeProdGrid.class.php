<?php 
class ListeProdGrid extends Grid
{
	protected static $db;
	protected $idVendor;
	protected $prefix;
	protected $filter; //le filtre de recherche, par catégorie etc
	
	public function __construct($id, $tech, $filter)
	{	
		$this->_name = 'produits';
		$this->idVendor = $id;
		$this->filter = $filter;
		
		switch ($tech) {
			case 'Prestashop':
				self::$db = new BusinessSoukeoPrestashopModel();
				$this -> prefix = 'PS';
				break;
			
			case 'Thelia' :
				self::$db = new BusinessSoukeoTheliaModel();
				$this -> prefix = 'TH';
				break;
			default:
				break;
		}
		
		$nameVendor = self::$db -> getValidVendor($id);
		$this->_init('ecommercant',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_produits",htmlentities(("Liste Produits e-commerçant : ". $nameVendor['VENDOR_NOM'])));


	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("ID")),					"sortable" => true, "excelable" => true),
				array("name" => htmlentities(("Référence")),			"sortable" => true, "excelable" => true),
				array("name" => htmlentities(("EAN")),					"sortable" => true, "excelable" => true),
				array("name" => htmlentities(("Nom Produit")),			"sortable" => true, "excelable" => true),
				array("name" => htmlentities(("Description")),			"sortable" => false, "excelable" => true),
				array("name" => htmlentities(("Catégorie")),			"sortable" => true, "excelable" => true),
				array("name" => htmlentities(("Prix")),					"sortable" => true, "excelable" => true, "format"=> 'money'),
				array("name" => htmlentities(("Stock")),				"sortable" => true, "excelable" => true),
				array("name" => htmlentities(("Date Création")),		"sortable" => true, "excelable" => true  , "format"=>  'date'),
				array("name" => htmlentities(("Associé")),				"sortable" => true, "excelable" => true),
				
				array("name" => ("Actions"),				"sortable" => false, "excelable" => false)
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;

		//CONTENU de la grid
		
		if($this->filter != ""){
			if(self::$db != null)
				$res = self::$db -> getAllProdByCategVendor($this->filter, $this->idVendor);	
			else //empecher l'affichage d'erreurs si mauvaise url
				$res= array();
		}
		else{
			$func = "getAllProducts".$this -> prefix ."ByVendor";
			if(self::$db != null)
				$res = self::$db->$func($this->idVendor);
			else //empecher l'affichage d'erreurs si mauvaise url
				$res= array();
		}
		
		
		
		$content = array();
		$javascript = array();
		foreach($res as $prod)
		{
			//eval('$class = ' . $prog['class'] . '::getDescription();');
			
			$line =
				array(
					$prod['ID_PRODUCT'],
					$prod['REFERENCE_PRODUCT'],
					isset($prod['EAN']) ? $prod['EAN'] : "--",
					htmlentities($prod['NAME_PRODUCT']),
					$prod['SHORT_DESCRIPTION'],
					htmlentities($prod['CATEGORY']),
					$prod['PRICE_PRODUCT'],
					$prod['QUANTITY'],
						($prod['DATE_CREATE'])
					
				);
			
			if (self::$db -> productIsAssoc($prod['ID_PRODUCT'], $this->idVendor))
			{
				$active_html = '<img border="0" align="absmiddle" alt="' . ("Programmation active") . '" class="iconCircleCheck" src="' . $skin_path . 'images/picto/check.gif">';
				$active_txt = ("Oui"); 
			}
			else
			{
				$active_html = '<img border="0" align="absmiddle" alt="' . ("Programmation active") . '" class="iconCircleWarn" src="' . $skin_path . 'images/picto/close.gif">';
				$active_txt = ("Non");
			}
			
			$line[] = array("txt" => $active_txt, "html" => $active_html);
			
			$action = '';
			$action .= '<a href="/app.php/qualification/ecommercant/listeProduits/prod?prod='.$prod['ID_PRODUCT'].'&vendor='.$this -> idVendor.'&t='.$this -> prefix.'"> 
						<img border="0" alt="' . ("Fiche du produit") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';

			//$action .= '' . popupLink('/qualification/ecommercant/popup_addmod/index.php?prog_id=' . $ecomm["VENDOR_ID"], ("Modification d'un commerçant"), 600, 600,"jdiag") . 
				//'<img border="0" align="absmiddle" alt="' . ("Modification d'un commerçant") . '" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
			
			///$action .= "<a href=\"#\" onClick=\"confirmDelete('" . ("Etes-vous certain de vouloir supprimer ce commerçant ?") . "', " . $ecomm['VENDOR_ID'] . ", '', '', '" . $this->_name . "', '" . $root_path . "')\"> 
				//<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Suppression d'un commerçant") . "\" class=\"iconAction\" src=\"" . $skin_path . "images/picto/trash.gif\"></a>";
			
			$line[] = $action;
			$content[] = $line;
		}
		
		return $content;
	}
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @param $id2
	 * @param $id3
	 * @return boolean
	 */
	public function delete($id1, $id2, $id3)
	{
		// Suppression des logs
		$query = "DELETE FROM sfk_vendor
					WHERE VENDOR_ID = " . $id1;
		dbQuery($query);
		
		return true;
	}

}
?>
