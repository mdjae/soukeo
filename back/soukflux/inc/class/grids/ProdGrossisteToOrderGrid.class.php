<?php 
/**
 * Class pour grid Grossiste
 */
class ProdGrossisteToOrderGrid extends Grid
{
	protected $grossiste_id;
	protected $order_id_grossiste;
	protected $nom_gr;
	protected $stockfilter;
	
	public function __construct($grossiste_id, $nom_gr, $interne = false, $stockfilter )
	{	
		$this->_name = 'item_grossiste_to_order'.$grossiste_id;
		$this->order_id_grossiste = $order_id_grossiste;
		$this->grossiste_id = $grossiste_id;
		$this->nom_gr = $nom_gr;
		$this->stockfilter = $stockfilter;
		$this->_setSqlQuery();
		$this->_init('item_grossiste_to_order'.$grossiste_id ,$this->_makeHeaders(),$this->_makeContent(), $interne);
		$this->setTitle("NoBg","grids_item",htmlentities(("Listing Produit $nom_gr a commander")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities("N° Com"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Date"),			"sortable" => true,  "excelable" => true, 	"align" => 'center' , "format"=>'date' ),
				array("name" => htmlentities("REF"),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Nom"),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Prix"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 , "format"=>'money' ),
				array("name" => htmlentities("Prix Gross"),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 , "format"=>'money' ),
				array("name" => htmlentities("Qté demandée"),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 ),
				array("name" => htmlentities("Qté Gross"),	"sortable" => true,  "excelable" => true, 	"align" => 'center'  ),
				array("name" => htmlentities("Poids"),			"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true , "format"=>'number' 	),
			);

		$headers[] = array("name" => htmlentities("Etat"),	"sortable" => false, "excelable" => false,	"align" => 'center' ) ;
		$headers[] = array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ) ;
		
		$headers[] = array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '2%');
		
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		

		

		foreach($res as $item)
		{
			if($item['URL'] !== null && $item['URL'] != "" && $item['URL'] != "NULL"){
				$refGrossiste = "<a href='".$item['URL']."' target='_blank'><b>".htmlentities($item['REF_GROSSISTE'])."</b></a>";
			}else{
				$refGrossiste = htmlentities($item['REF_GROSSISTE']) ;
			}
			
			$stock = "";
			
			if(!$item['ACTIVE'] ){
				$stock .= '<span id="item_'.$item['item_id'].'_stock" class="etat_order label label-important"> Rupture ! </span>';
			}
			elseif($item['QUANTITY'] == "0"){
				$stock .= '<span id="item_'.$item['item_id'].'_stock" class="etat_order label label-warning"><b style="color:black";> Délai ! <b></span></td>';
			}
			else{
				$stock.= '<span id="item_'.$item['item_id'].'_stock" class="etat_order label label-success">En stock</span>';
			}
						
			$line =
				array(
					'<a href="/app.php/avahis/commande_avahis?id='.$item['increment_id'].'" target="_blank">'.
									$item['increment_id'].'
								</a>',
					htmlentities($item['date_created']),
					$refGrossiste,
					truncateStr(htmlentities($item['name']), 40),
					$item['base_row_total_incl_tax'],
					$item['PRICE_PRODUCT'],
					$item['qty_ordered'],
					$stock,
					$item['WEIGHT'] * $item['qty_ordered'],
				);
			
			$state = "";
			
			
			if($item['litiges']){
				$state.= '<span id="item_'.$item['item_id'].'_etat" class="etat_order label label-warning"> litige </span>';
			}else{
				$state.= '<span id="item_'.$item['item_id'].'_etat" class="etat_order label label-warning"></span>';
			}
			
			if($item['shipping_address_id']){
			    $address = new BusinessAddress($item['shipping_address_id']);
                
                if($address -> getCountry() !=  "RE"){
                    $state .= '<span id="addr_'.$item['item_id'].'_etat" class="etat_order label label-important"> '.countryName($address -> getCountry()).' </span>';
                }else{
                    $state .= '<span id="addr_'.$item['item_id'].'_etat" class="etat_order label label-important"></span>';
                }
            }
			

			
			$line[] = $state;
			
			$action = '';
			$num = "item_".$item['item_id'];
			
			if($item['litiges']){
				$action .= '<a id="litigeit_'.$item['item_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un litige ") . '" 
							align="absmiddle" title="Annuler litige"  class="iconAction" src="' . $skin_path . 'images/picto/check.gif"></a>';
			}else{			
				$action .= '<a id="litigeit_'.$item['item_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un litige ") . '" 
							align="absmiddle" title="Daclarer litige"  class="iconAction" src="' . $skin_path . 'images/picto/denied.gif"></a>';
			}
			$action .= '<span class="popover-markup"> 
								<a class="trigger" id="popover_'.$item['item_id'].'">
									<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Assigner commande" alt="Assigner commande">
								</a> 
							    <div class="head hide">
							    	<button type="button" id="close" class="close" onclick="$(&quot;#popover_'.$item['item_id'].'&quot;).popover(&quot;hide&quot;);">&times;</button>
							    	<br>
							    	<h5>Commande grossiste : <h5>
							    </div>
							    <div class="content hide">
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="N° Commande…" id="popoverinput_'.$item['item_id'].'">
							        </div>
							        <button type="submit" class="btn btn-primary btn-block" onClick="assignItemComGr(&quot;'.$item['item_id'].'&quot;, true);"><b>OK</b></button>
							    </div>
							</span>';
			$line[] = $action;
			
			
	
			
			$line[] = '<input id="item_' . $item["item_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT so.increment_id , s0.item_id, s1.date_created , pg.REF_GROSSISTE, s0.name, s0.base_row_total_incl_tax, pg.PRICE_PRODUCT, pg.QUANTITY, pg.WEIGHT, s0.qty_ordered, pg.ACTIVE, pg.URL, s0.litiges, so.shipping_address_id 
					FROM skf_order_items s0 
					INNER JOIN skf_po s1  ON s0.order_id = s1.order_id  AND s0.udropship_vendor = s1.vendor_id
					INNER JOIN skf_orders so  ON s0.order_id = so.order_id 
					INNER JOIN sfk_catalog_product cp ON cp.sku = s0.sku
					INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
					INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste    
 					";

		
		$where = " WHERE s0.order_id_grossiste IS NULL
				  AND pg.GROSSISTE_ID = $this->grossiste_id 
				  AND so.order_state = 'processing' AND s0.udropship_vendor = 24
				  AND s0.litiges = 0    
		";
        $order =  "  ORDER BY s1.increment_id DESC"; 
		
		if ( isset($this->stockfilter) && ($this->stockfilter === "1"))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " pg.QUANTITY > 0 AND pg.ACTIVE = 1";
        }
        
        if ( isset($this->stockfilter) && ($this->stockfilter === "0"))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " ( pg.QUANTITY = 0 OR pg.ACTIVE = 0 )";
        }
		
		
        $this->_sql_query = $select . $where . $order;
               
        
    }
    
    
        public function update($post)
    {
		
        $this->__construct($this->grossiste_id, $this->nom_gr, false, $post['stockfilter']);
    }
}
?>