<?php 
/**
 * Class pour grid Grossiste
 */
class OrdersGrid extends Grid
{
	protected $customerfilter;
	protected $vendorfilter;
	protected $statusfilter;
	protected $numfilter;
	protected $litigefilter;
	protected $datefilter;
	
	public function __construct($customerfilter="", $vendorfilter ="", $statusfilter="", $numfilter="", $litigefilter="", $datefilter ="2", $interne = false)
	{	
		$this->_name = 'commande';
		$this->customerfilter = $customerfilter;
		$this->vendorfilter = $vendorfilter ;
		$this->statusfilter = $statusfilter ;
		$this->numfilter 	= $numfilter;
		$this->litigefilter = $litigefilter ;
		$this->datefilter 	= $datefilter;
		$this->interne = $interne;
		
		$this->_setSqlQuery();
		$this->_init('commande',$this->_makeHeaders(),$this->_makeContent(), $this->interne );
		$this->setTitle("NoBg","grids_order",htmlentities(("Listing Commandes Market")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("")),			"sortable" => false, "excelable" => false, 	"align" => 'center' ),
				array("name" => htmlentities(("N°")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Date")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' , "format"=>'date'),
				array("name" => htmlentities(("Client")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Total"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' , "format"=>'money'),
				array("name" => htmlentities(("Status")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Poids")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', "format"=>'number' ),
				array("name" => htmlentities(("Infos / Etat")),		"sortable" => true,  "excelable" => true, 	"align" => 'center'),	
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// Contenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();

		foreach($res as $order)
		{
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$order['order_id'].' AND commented_object_table = "skf_orders"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			
			if($order['order_customer_id'] == "0"){
				$nomCli = 	'<em>'.htmlentities($order['recipient_name']) .'</em>';
			}else{
				$cust = new BusinessCustomer($order['order_customer_id']);
				$nomCli = '<a href="/app.php/clients/listing?id='.$cust -> getCustomer_id().'" target="_blank">'.
									htmlentities($cust -> getName()).'</a>';
			} 
			
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='com_".$order['order_id']."'  onclick='showChild(this)'  />",
					$order['increment_id'],
					$order['date_created'],
					$nomCli,
					$order['grand_total'],
					$order['order_status'],
					$order['weight'],
				);
			
			
			

			$state = "";
			$address = new BusinessAddress($order['shipping_address_id']);
			
			
			if($order['litiges']){
				$state .= '<span id="com_'.$order['order_id'].'_etat" class="etat_order label label-warning"> litige </span>';
			}else{
				$state .= '<span id="com_'.$order['order_id'].'_etat" class="etat_order label label-warning"></span>';
			}
			
			if($address -> getCountry() !=  "RE"){
				$state .= '<span id="addr_'.$order['order_id'].'_etat" class="etat_order label label-important">'.$this -> countryName($address -> getCountry()).'</span>';
			}else{
				$state .= '<span id="addr_'.$order['order_id'].'_etat" class="etat_order label label-important"></span>';
			}
			$line[] = $state;
			
			$action = '';
			$num = "com_".$order['order_id'];
			
			if($order['litiges']){
				$action .= '<a id="litigeit_'.$order['order_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un litige ") . '" 
							align="absmiddle" title="Annuler litige"  class="iconAction" src="' . $skin_path . 'images/picto/check.gif"></a>';
			}else{			
				$action .= '<a id="litigeit_'.$order['order_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un litige ") . '" 
							align="absmiddle" title="Daclarer litige"  class="iconAction" src="' . $skin_path . 'images/picto/denied.gif"></a>';
			}
			$action .= "<a class='closed btn' href='#' id='comment_order_".$order['order_id']."' onclick='showComments(this); return false;'><i class='fa fa-comment icon-black' style='padding-right: 0px'></i>$countComment</a>";
			$line[] = $action;
			$line[] = '<input id="comma_' . $order["increment_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT DISTINCT so.order_id, so.increment_id, so.grand_total, 
									so.order_status, so.order_state, so.weight, 
									so.litiges, so.date_created, so.order_customer_id, 
									so.shipping_address_id, so.recipient_name   
					FROM   skf_orders as so
					INNER JOIN skf_po po ON po.order_id = so.order_id ";
		
		$where = "";
        
		if ( isset($this->customerfilter ) && ($this->customerfilter != "all") && ($this->customerfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.order_customer_id = '$this->customerfilter' ";
        }
		
		if ( isset($this->vendorfilter ) && ($this->vendorfilter != "all") && ($this->vendorfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " po.vendor_id = '$this->vendorfilter' ";
        }
        
		if ( isset($this->statusfilter ) && ($this->statusfilter  != "all") && ($this->statusfilter  != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.order_status = '$this->statusfilter' ";
        }
		
		if ( isset($this->numfilter ) && ($this->numfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.increment_id = '$this->numfilter' ";
        }
        
        if ( isset($this->litigefilter ) && ($this->litigefilter  != "")  && ($this->litigefilter  != "all"))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.litiges = $this->litigefilter ";
        }
		 
		if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " so.date_created > (NOW() - INTERVAL ".$this->datefilter." DAY) ";
        }
		
		$order =  "   ORDER BY so.increment_id DESC";
		 
       	$this->_sql_query = $select . $where . $order;       

    }
    
    
	public function update($post)
    {

        $this->__construct($post['customerfilter'], $post['vendorfilter'],$post['statusfilter'], $post['numfilter'], $post['litigefilter'], $post['datefilter']);
    }
    
	public function countryName($abr)
	{
		switch ($abr) {
			case 'FR':
				return 'FR';
			break;
			
			case 'MU':
				return 'MAURICE';
			break;
			
			case 'YT':
				return 'MAYOTTE';
			break;
			
			default:
				return $abr;
			break;
		}
	}
}
?>