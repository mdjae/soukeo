<?php 
/**
 * Class pour grid Grossiste
 */
class CustomersGrid extends Grid
{
	protected $namefilter;
	protected $numclient;
	protected $datefilter;
	
	public function __construct($namefilter, $numclient, $datefilter)
	{
		$this->namefilter = $namefilter;
		$this->numclient  = $numclient ;
		$this->datefilter = $datefilter;
			
		$this->_name = 'customers';
		
		$this->_setSqlQuery();
		$this->_init('customers',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","customers",htmlentities(("Clients acheteurs")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("")),			"sortable" => false, "excelable" => false, 	"align" => 'center' ),
				array("name" => htmlentities(("Id interne")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Id Magento")),       "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("Nom")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Prénom"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Email")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Phone")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Date creation")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', "format"=>'date' ),
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities(("Etat")),		"sortable" => true,  "excelable" => true, 	"align" => 'center'),	
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// Contenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		//var_dump($res);
		foreach($res as $cust)
		{
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$cust['entity_id'].' AND commented_object_table = "skf_customers"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			 
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='".$cust['entity_id']."'  onclick='self.location.href=\"client?type=customer&id=".$cust['entity_id']."\"'  />
					",
					$cust['entity_id'],
					$cust['customer_id'] ? $cust['customer_id'] : "Non inscrit",
					htmlentities($cust['firstname']),
					htmlentities($cust['lastname']),
					htmlentities($cust['email']),
					htmlentities($cust['phone']),
					$cust['date_created'],
				);

			$action = '';
			$num = "cust_".$order['entity_id'];
			
			$action .= "<a class='closed btn' href='#' id='comment_cust_".$cust['entity_id']."' onclick='showComments(this); return false;'><i class='fa fa-comment icon-black' style='padding-right: 0px'></i>$countComment</a>";
			$line[] = $action;
			
			
			$line[] = '<input id="cust_' . $cust["entity_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT * FROM skf_customers ";
		
		$where = "";
        
		
		if ( isset($this->namefilter ) && ($this->namefilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " firstname LIKE '%$this->namefilter%' OR lastname LIKE '%$this->namefilter%'";
        }

		if ( isset($this->numclient ) && ($this->numclient != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " entity_id = '$this->numclient'";
        }
		 
		if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " date_created > (NOW() - INTERVAL ".$this->datefilter." DAY) ";
        }
		
		$order =  "   ORDER BY date_created DESC";
		 
       	$this->_sql_query = $select . $where . $order;      

    }
    
    
        public function update($post)
    {

        $this->__construct($post['namefilter'], $post['numclient'], $post['datefilter']);
    }
}
?>