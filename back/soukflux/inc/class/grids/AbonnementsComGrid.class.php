<?php 
/**
 * Class pour grid Grossiste
 */
class AbonnementsComGrid extends Grid
{
	protected $numvendor ;
    protected $vendeurfilter;
	protected $typeabofilter ;
	protected $typevendorfilter;
    protected $statusfilter;
    protected $creditspendfilter;
	
	public function __construct($numvendor, $vendeurfilter, $typeabofilter, $typevendorfilter, $creditspendfilter, $statusfilter ="actif" )
	{	
		$this ->_name = 'abonnements';
		$this -> numvendor = $numvendor;
        $this -> vendeurfilter = $vendeurfilter;
		$this -> typeabofilter = $typeabofilter;
		$this -> typevendorfilter = $typevendorfilter;
        $this -> creditspendfilter = $creditspendfilter;
        
        $this -> statusfilter = $statusfilter;
		$this ->_setSqlQuery();
		$this ->_init('abonnements',$this->_makeHeaders(),$this->_makeContent() );
		$this -> setTitle("NoBg","grid_abonnements",htmlentities(("Abonnements des commerçants")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
			    array("name" => htmlentities(""),            "sortable" => false, "excelable" => false , "width" => '6%'),
				array("name" => htmlentities(("N° Vendeur")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' , "width" => '6%'),
				array("name" => htmlentities("Nom"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Type Vendeur"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Type abonnement")),	"sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities(("Nb produits")),  "sortable" => true,  "excelable" => true,   "align" => 'center'),
				array("name" => htmlentities(("Date début")),  "sortable" => true,  "excelable" => true,   "align" => 'center', 'format' => 'date' ),
				array("name" => htmlentities(("Solde crédits")),  "sortable" => true,  "excelable" => true, "align" => 'center'),
				array("name" => htmlentities(("Limite produit spéciale")),	"sortable" => true,  "excelable" => true, "align" => 'center'),
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
        
        $vendorManager = new ManagerVendor();
        $vendorTypeManager = new ManagerVendorType();
        
		foreach($res as $vendor)
		{
		    
            if($vendorObj = $vendorManager -> getBy(array("vendor_id" => $vendor['vendor_id']))){
                $vendorNom = "<a href='#' onclick='self.location.href=\"/app.php/clients/client?type=vendor&id=".$vendor['entity_id']."\"' id='".trim($vendorObj -> getEntity_id())."'>".htmlentities($vendorObj -> getVendor_nom())."</a>";
            }else{
                $vendorNom = $vendor['$vendor_nom'];
            }
			
            if($vendor_type = $vendorTypeManager -> getById(array("type_id" => $vendor['vendor_type']))){
                $type = $vendor_type -> getLabel();
            }else{
                $type = "<em>N/C</em>";
            }
			$line =
				array(
				    "<input type='button' class='closed btn' value='+' id='vendor_".$vendor['vendor_id']."' onclick='showAbonnementVendorInfo(this); return false;'  />",
					$vendor['vendor_id'],
					$vendorNom,
					$type,
					getBtnAbonnement($vendor['abonnement'], $vendor['vendor_id']),
					$vendor['count_sales'],
					$vendor['date_debut_abonnement'],
					$vendor['solde_credit'] ."<a  href='#' class='pull-right btn' onClick='showDialogEditCredits(".$vendor['vendor_id']."); return false;'>".
					                             '<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Assigner commande" alt="Modifier">'.
					                         "</a>",
					"<span style='font-weight:bold;'id='limitprod_".$vendor['vendor_id']."'>".$vendor['product_limit'].' <a class="pull-right btn" href="#" onClick="setProductLimit(this)" style="margin-right:5px">
                                    <img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Ajouter crédits" alt="Ajouter crédits">
                                </a> </span>'
				);

			$action = '<h4>∞</h4>';
			$num = "vendor_".$facture['vendor_id'];



			$line[] = $action;
			
			$line[] = '<input id="facture_' . $facture["vendor_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT v.vendor_id, v.entity_id, vendor_nom, vendor_type, label, abonnement, date_debut_abonnement, solde_credit, product_limit, cvs.count_sales    
					FROM   
					sfk_vendor v  
					INNER JOIN skf_abonnement a ON a.abonnement_id = v.abonnement 
					INNER JOIN skf_count_vendor_sales cvs ON cvs.vendor_id = v.vendor_id ";
		
	
		$where = " WHERE v.active = 1 AND v.vendor_id IS NOT NULL AND v.vendor_id <> '' ";        

        
		if ( isset($this->numvendor ) && ($this->numvendor != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " v.vendor_id = '$this->numvendor' ";
        }
        
        if ( isset($this->vendeurfilter ) && ($this->vendeurfilter != "") && ($this->vendeurfilter != "all") )
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";  
            $where .= " v.vendor_id = '".$this->vendeurfilter."' ";
        }
        
		
		if ( isset($this->typeabofilter ) && ($this->typeabofilter != "all") && ($this->typeabofilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " a.abonnement_id = '$this->typeabofilter' ";
        }
        
        if ( isset($this->typevendorfilter ) && ($this->typevendorfilter != "all") && ($this->typevendorfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " v.vendor_type = '$this->typevendorfilter' ";
        }
        
        if ( isset($this->creditspendfilter) && ($this->creditspendfilter != "all")){
                
            if ($where == "") $where = " WHERE ";
            else $where .= " AND "; 
            
            if($this->creditspendfilter == "yes"){
                $where .= " v.vendor_id IN (SELECT DISTINCT vendor_id FROM skf_credits_history)";
            }elseif($this->creditspendfilter == "no"){
                $where .= " v.vendor_id NOT IN (SELECT DISTINCT vendor_id FROM skf_credits_history)";
            }
        }
		
        $order =  " ORDER BY v.vendor_id ASC"; 
		
        $this->_sql_query = $select . $where . $order;  
        //var_dump($this->_sql_query);
    }
    

    
	public function update($post)
    {

        $this->__construct($post['numvendor'], $post['vendeurfilter'], $post['typeabofilter'], $post['typevendorfilter'], $post['creditspendfilter']);
    }
}
?>