<?php 
/**
 * Class pour grid Grossiste
 */
class OrdersAvahisGrid extends Grid
{
	protected $numfilter;
	protected $statusfilter;
	protected $litigefilter;
	protected $datefilter;
	protected $grossistefilter;
	
	public function __construct( $numfilter="", $statusfilter="", $litigefilter="", $datefilter ="all", $grossistefilter="")
	{	
		$this->_name = 'commande_avahis';
		$this->statusfilter 	= $statusfilter ;
		$this->numfilter 		= $numfilter ;
		$this->litigefilter 	= $litigefilter ;
		$this->datefilter 		= $datefilter;
		$this->grossistefilter 	= $grossistefilter;

		$this->_setSqlQuery();
		
		$js = array("$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});");
		$this->setJavascript($js);
		
		$this->_init('commande_avahis',$this->_makeHeaders(),$this->_makeContent());
		

		$this->setTitle("NoBg","grids_order",htmlentities(("Listing Commandes Market Avahis")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(""),			"sortable" => false, "excelable" => false , "width" => '6%'),
				array("name" => htmlentities("Num Cmd"),	"sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Date"),		"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'date'),
				array("name" => htmlentities("Client"),		"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Total"),		"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'money' ),
				array("name" => htmlentities("Poids"),		"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'number'),
				array("name" => htmlentities("Etat (State)"),		"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Statut (Status)"),       "sortable" => true,  "excelable" => false,  "align" => 'center'),
				array("name" => htmlentities("Satut PO"),       "sortable" => true,  "excelable" => false,  "align" => 'center'),
				array("name" => htmlentities("Commandé"),	"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Infos/Etat"),	"sortable" => true,  "excelable" => false, 	"align" => 'center'),		
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center'),
				array("name" => htmlentities(""), 			"sortable" => false, "excelable" => false,	"align" => 'center', 	"width" => '1%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
        $customerManager = new ManagerCustomer();
		$content = array();
		$javascript = array();
		foreach($res as $order)
		{
			$items = dbQueryAll('SELECT order_id_grossiste FROM  `skf_order_items`  WHERE order_id ="'.$order['order_id'].'" AND udropship_vendor =24');
			
			$count = count($items);
			$customerManager = new ManagerCustomer();
			$orderPartComplete = false;
			
			foreach ($items as $item) {
				if($item['order_id_grossiste'] == null | $item['order_id_grossiste'] == ''){
					$ordercomplete = false;
					break;
				}else{
					$orderPartComplete = true;
					$ordercomplete = true;
				}
			}
			
			foreach ($items as $item) {
				if($item['order_id_grossiste'] != null && $item['order_id_grossiste'] != ''){
					$orderPartComplete = true;
				}
			}
			
			if($order['order_customer_id'] == "0"){
			    if($order['recipient_email'] != ""){
			        if($cust = $customerManager -> getBy(array("email" => $order['recipient_email'])))
                        $nomCli = "<a href='#' onClick='showInfo(this, \"Customers\"); return false;' id='".trim($cust -> getEntity_id())."'>".htmlentities($cust -> getName())."</a>";
                    else 
                        $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order['recipient_name']) .'</em>';      
			    }else{
                    $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order['recipient_name']) .'</em>';    
			    }
				
			}else{
				if($cust = $customerManager -> getBy(array("customer_id" => $order['order_customer_id']))){
				    $nomCli = "<a href='#' onClick='showInfo(this, \"Customers\"); return false;' id='".trim($cust -> getEntity_id())."'>".htmlentities($cust -> getName())."</a>";
				}else{
				    $nomCli = "<strong style='color:red'>Erreur : customer_id ".$order['order_customer_id']." absent base Soukflux </strong><br/> ".htmlentities($order['recipient_name']);
				}
				/*$nomCli = '<a href="/app.php/clients/listing_acheteurs?id='.$cust -> getEntity_id().'" target="_blank">'.
									htmlentities($cust -> getName()).'
								</a>';*/
                    
			} 
			
			$addr = new BusinessAddress($order['shipping_address_id']);
			$htmladdr = $addr -> getStreet1()." ".$addr -> getStreet2()." ". $addr -> getPostcode()." ". $addr -> getCity()." ". $addr -> getRegion_code();
			
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$order['po_id'].' AND commented_object_table = "skf_po"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='po_".$order['po_id']."_".$order['order_id']."' onclick='showOrderAvahisInfo(this)'  />
					 ",
					$order['idcmd'],
					$order['date_created'],
					$nomCli,
					$order['grand_total'],
					$order['total_weight'],
					getLabelStatus($order['order_state']),
					getLabelStatus($order['order_status']),
					getPoStateShipping($order['udropship_status'])
				);
				
			$state = "";
			$info = "";
			
			$address = new BusinessAddress($order['shipping_address_id']);
				
			if($ordercomplete){
				$state .= '<span id="com_'.$order['po_id'].'_po" class="etat_order label label-success">commande complete</span>';
			}
			elseif($orderPartComplete && !$ordercomplete){
				$state .= '<span id="com_'.$order['po_id'].'_po" class="etat_order label label-info">commande partielle</span>';
			}
			else{
				$state .= '<span id="com_'.$order['po_id'].'_po" class="etat_order label label-success"></span>';
			}	
			
			if($address -> getCountry() !=  "RE"){
				$info .= '<span id="addr_'.$order['order_id'].'_etat" class="etat_order label label-important"> '.countryName($address -> getCountry()).' </span>';
			}else{
				$info .= '<span id="addr_'.$order['order_id'].'_etat" class="etat_order label label-important"></span>';
			}
	
			if($order['litiges']){
				$info .= '<span id="com_'.$order['order_id'].'_etat" class="etat_order label label-warning">litige</span>';
			}else{
				$info .= '<span id="com_'.$order['order_id'].'_etat" class="etat_order label label-warning"></span>';
			}
			
			$line [] = $state;
			$line [] = $info;
			
			$action = "";
			$num = "com_".$order['order_id'];
			
			if($order['litiges']){
				$action .= '<a id="litigeit_'.$order['order_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un litige ") . '" 
							align="absmiddle" title="Annuler litige"  class="iconAction" src="' . $skin_path . 'images/picto/check.gif"></a>';
			}else{			
				$action .= '<a id="litigeit_'.$order['order_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un litige ") . '" 
							align="absmiddle" title="Daclarer litige"  class="iconAction" src="' . $skin_path . 'images/picto/denied.gif"></a>';
			}
			
			$action .= '<span class="popover-markup"> 
								<a class="trigger" id="popover_'.$order['po_id'].'">
									<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Assigner commande" alt="Assigner commande">
								</a> 
							    <div class="head hide">
							    	<button type="button" id="close" class="close" onclick="$(&quot;#popover_'.$order['po_id'].'&quot;).popover(&quot;hide&quot;);">&times;</button>
							    	<br>
							    	<h5>Commande grossiste : <h5>
							    </div>
							    <div class="content hide">
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="N° Commande…" id="popoverinput_'.$order['po_id'].'">
							        </div>
							        <button type="submit" class="btn btn-primary btn-block" onClick="assignPoComGr(&quot;'.$order['po_id'].'&quot;);"><b>OK</b></button>
							    </div>
							</span>';
							
			$action .= "<a class='closed btn' href='#' id='comment_po_".$order['po_id']."' onclick='showComments(this)'>
					 <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i>$countComment</a>";
			$line[] = $action;
			
			$line[] = '<input id="po_' . $order["po_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT so.grand_total, po.increment_id, po.base_total_value, so.order_status, so.order_state, po.total_weight, po.vendor_id, po.order_id, po.litiges, so.increment_id as idcmd,
					so.date_created, po.po_id, so.base_shipping_amount, so.shipping_address_id, so.order_customer_id, so.recipient_name, so.shipping_address_id, so.recipient_phone, so.recipient_email,   
					po.udropship_status 
					FROM   
					skf_po  po INNER JOIN skf_orders so ON so.order_id = po.order_id   
					";
		$where = " WHERE po.vendor_id = '24' ";
        $order =  " "; 
		
		if ( isset($this->statusfilter ) && ($this->statusfilter  != "all") && ($this->statusfilter  != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.order_status = '$this->statusfilter' ";
        }
		
		if ( isset($this->numfilter ) && ($this->numfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.increment_id = '$this->numfilter' ";
        }
        
        if ( isset($this->litigefilter ) && ($this->litigefilter  != "")  && ($this->litigefilter  != "all"))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " po.litiges = $this->litigefilter ";
        }
        
        if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " so.date_created > (NOW() - INTERVAL ".$this->datefilter." DAY) ";
        }
        
		if (isset($this->grossistefilter) && ($this->grossistefilter != "") && ($this->grossistefilter != "all")){
			if ($where == "") $where = " WHERE ";
    		else $where .= " AND ";
			$where .= " po.order_id IN (SELECT DISTINCT oi.order_id FROM sfk_catalog_product cp 
										INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
										INNER JOIN sfk_grossiste g ON apg.GROSSISTE_ID = g.GROSSISTE_ID 
										INNER JOIN skf_order_items oi ON cp.sku = oi.sku
										WHERE apg.GROSSISTE_ID = ".$this->grossistefilter.") ";
		}

		$order =  "   ORDER BY po.increment_id desc";
		
        $this->_sql_query = $select . $where . $order; 

    }
    
    
    public function update($post)
    {
        $this->__construct($post['numfilter'], $post['statusfilter'], $post['litigefilter'], $post['datefilter'], $post['grossistefilter']);
    }
	
	public function countryName($abr)
	{
		switch ($abr) {
			case 'FR':
				return 'FR';
			break;
			
			case 'MU':
				return 'MAURICE';
			break;
			
			case 'YT':
				return 'MAYOTTE';
			break;
			
			default:
				return $abr;
			break;
		}
	}
}
?>