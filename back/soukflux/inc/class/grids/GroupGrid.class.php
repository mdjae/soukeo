<?php 
class GroupGrid extends Grid
{
	public function __construct($sql_query)
	{	
		$this->_name = "group";
		$this->_sql_query = $sql_query;
		$this->_init('group',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_group",("Liste des groupes"));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => ("Nom"),						"sortable" => true, "excelable" => true, "align"=>'center',"width" => '8%'),
				array("name" => ("Description"),				"sortable" => true, "excelable" => true, "align"=>'center'),
				array("name" => ("Email"),						"sortable" => true, "excelable" => true, "align"=>'center'),
				array("name" => ("Date d'ajout"),				"sortable" => true, "excelable" => true, "align"=>'center',"width" => '10%'),
				array("name" => ("Dernière modification"),		"sortable" => true, "excelable" => true, "align"=>'center',"width" => '20%'),
				array("name" => ("Actions"),					"sortable" => false,"excelable" => false,"align"=>'center',"width" => '5%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// Contenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		foreach($res as $group)
		{
			
			$line =
				array(
					$group['name'],
					$group['descr'],
					$group['email'],
					writeNormalDate($group['add_date']),
					(writeNormalDate($group['mod_date'])!=''?writeNormalDate($group['mod_date']).(" par ").$group['who_mod']:'')
				);

			$action = '';
			if (SystemAccess::canAccess("admin.groups..addmod"))
			{
				$action .= popupLink('/o_admin/groups/popup_add/index.php?g_id=' . $group["group_id"],("Mettre à jour le groupe"),650,600,"jdiag") . '<img border="0" alt="' . ("Mettre à jour le groupe") . '" align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
			
				// Si jamais utilis�, on peut supprimer
				if ($group['nb'] == 0)
				{
					$action .= "<a href=\"#\" onClick=\"confirmDelete('" . sprintf(("Etes-vous certain de vouloir supprimer le groupe %s ?"),$group['name']) . "', '" . $group['group_id'] . "', '', '', '" . $this->_name . "', '" . $root_path . "')\"> 
					<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Suppression d'un groupe") . "\" class=\"iconAction\" src=\"" . $skin_path . "images/picto/trash.gif\"></a></p>";	
				}
			}

			$line[] = $action;
			$content[] = $line;
		}

		return $content;
	}
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @param $id2
	 * @param $id3
	 * @return boolean
	 */
	public function delete($id1, $id2, $id3)
	{
		$query = "DELETE FROM c_group
					WHERE group_id = " . $id1;
		dbQuery($query);
		
		$query = "DELETE FROM c_group_users
					WHERE group_id = " . $id1;
		dbQuery($query);
		
		return true;
	}

}
?>
