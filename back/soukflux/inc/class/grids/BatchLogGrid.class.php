<?php 
class BatchLogGrid extends Grid
{
	public function __construct($sql_query)
	{	
		$this->_sql_query = $sql_query;
		$this->_init('batchlog',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","tools",("Suivi des logs"));
		$this->_width = "100%";
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => ("Date de départ"),		"sortable" => true, "excelable" => true,"align" => 'center',"width" => '30%'),
				array("name" => ("Date de fin"),		"sortable" => true, "excelable" => true,"align" => 'center',"width" => '30%'),
				array("name" => ("Code de retour"),		"sortable" => true, "excelable" => true,"align" => 'center',"width" => '10%'),
				array("name" => ("Statut"),				"sortable" => true, "excelable" => true,"align" => 'center',"width" => '3%'),
				//array("name" => ("Nom du log"),			"sortable" => true, "excelable" => true),
				array("name" => ("Action"),				"sortable" => true, "excelable" => false,"align" => 'center',"width" => '3%'),
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// Contenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $log)
		{	
			if ($log['log']) 			// v�rification de la pr�sence du nom en base
			{
				$path = SystemParams::getParam("system*log_rep") ;
				$file = $path . "/" . $log['log'];
				if (file_exists($file))
				{
					$action = "";
					$action .= "<button class='btn' style='font-size: 2em;line-height: 22px;padding: 1px;color: black;'><a href='". $root_path . "o_services/download/download.php?path=". urlencode($path) . "&file=" . urlencode($log['log']) . "'\"> 
							<i class='fa fa-download'></i></a></button></p>";
				}	
			}
			
			else 
			{
				$action = "";
			}
			
			$line =
				array(
					$log['start_date']?smartDate($log['start_date']):"",
					$log['end_date']?smartDate($log['end_date']):"",
					$log['return_code'],
					$log['status'],
					//$log['log'],
					$action
				);

			$content[] = $line;
		}
		return $content;
	}

}
?>
