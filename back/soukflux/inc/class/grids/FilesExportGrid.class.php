<?php 
class FilesExportGrid extends Grid
{
		public function __construct()
	{	
		$this->_name = 'filesexport';
		$this->_setSqlQuery();
		$this->_init('filesexport',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_ecommercant",htmlentities(("Fichier à télécharger")));
	}

	public function _makeHeaders() {
		$headers =
			array(
				array("name" => 			( "Description"),			"sortable" => true,  "excelable" => true														),
				array("name" => htmlentities(("Provenance")),		"sortable" => true,  "excelable" => true,"align" => 'center', "width" => '10%'					),
				array("name" => htmlentities(("Nom fichier")),	"sortable" => true,  "excelable" => true,"align" => 'center'														),
				array("name" => htmlentities(("Dernière MAJ")),	"sortable" => true,  "excelable" => true,"align" => 'center'														),
				array("name" => 			( "Actions"),		"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%'),
				array("name" => htmlentities(("")), 			"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent() {
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $file)
		{
			$sql = "SELECT * FROM `c_export_files` WHERE CLASS = '".$file['CLASS']."' ORDER BY DATE_CREATE DESC LIMIT 1";
			
			$result = dbQueryAll($sql);

			$info_file = $result[0];
			
			$line =
				array(
					htmlentities($info_file['DESCRIPTION']), 
					$info_file['CLASS'], 
					$info_file['FILE_NAME'], 
					$info_file['DATE_CREATE'] 
				);


			$action = '';


/*			$action .= "<p><a  href='#' onClick = \"getFile('".$info_file['CLASS']."');\"> 
							<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Téléchargement du Fichier") . 
							"\" class=\"iconAction\" title= \"" . ("Téléchargement du Fichier") . "\" src=\"" . $skin_path . "images/picto/download.gif\"></a></p>";
*/							
			$action .= "<p><a  href='". $root_path . "/../../app.php/getfile?class=".$info_file['CLASS'] ."'> 
							<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Téléchargement du Fichier") . 
							"\" class=\"iconAction\" title= \"" . ("Téléchargement du Fichier") . "\" src=\"" . $skin_path . "images/picto/download.gif\"></a></p>";
			//getFile
			
			$line[] = $action;
			$line[] = $exports;
			$line[] = '<input id="vid_' . $ecomm["VENDOR_ID"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT DISTINCT CLASS FROM c_export_files";
 					
       
        $this->_sql_query = $select . $where . $order;       

    }
    
    /**
	 * lorsqu'une demande de rafraichissement est demandé les paramétre sont retourné
	 */
	public function update($post) {
		//var_dump($post);
		$this->__construct();
    }

}
?>
