<?php 
/**
 * Class pour grid Grossiste
 */
class OrdersGrossisteGrid extends Grid
{
	
	protected $numfilter;
	protected $litigefilter;
	protected $statusfilter;
	
	public function __construct($numfilter="", $statusfilter="", $litigefilter="")
	{	
		$this->_name = 'commande_grossiste';
		
		$this->statusfilter = $statusfilter ;
		$this->numfilter = $numfilter ;
		$this->litigefilter = $litigefilter ;
		$this->_fNbProd = $fNbProd ;

		$this->_setSqlQuery();
		$this->_init('commande_grossiste',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_order",htmlentities(("Listing Commandes aux Grossistes")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("")),				"sortable" => false, "excelable" => true, "width" => '6%'),
				array("name" => htmlentities(("N° Cmd")),		"sortable" => true,  "excelable" => true, "align" => 'center' ),
				array("name" => htmlentities(("Date")),			"sortable" => true,  "excelable" => true, "align" => 'center' , "format"=>'date'),
				array("name" => htmlentities("Total estimé"),	"sortable" => true,  "excelable" => true, "align" => 'center' , "format"=>'money' ),
				array("name" => htmlentities(("Poids estimé")),	"sortable" => true,  "excelable" => true, "align" => 'center', "format"=>'number'),
				array("name" => htmlentities(("Nb produits")),	"sortable" => true,  "excelable" => true, "align" => 'center'),
				array("name" => htmlentities(("Status")),		"sortable" => true,  "excelable" => true, "align" => 'center' ),
				array("name" => htmlentities(("Etat")),			"sortable" => true,  "excelable" => true, "align" => 'center'),		
				array("name" => htmlentities("Actions"),		"sortable" => false, "excelable" => false,"align" => 'center'),
				array("name" => htmlentities(("")), 			"sortable" => false, "excelable" => false,"align" => 'center', "width" => '1%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $order)
		{
			$state = "";
			
			$items = dbQueryAll('SELECT annonce_reception_id FROM  `skf_order_items`  WHERE order_id_grossiste ="'.$order['order_id_grossiste'].'" AND udropship_vendor =24');
			
			$count = count($items);
			
			$orderPartRecieved = false;
			
			foreach ($items as $item) {
				if($item['annonce_reception_id'] == null | $item['annonce_reception_id'] == ''){
					$orderRecieved = false;
					break;
				}else{
					$orderPartRecieved = true;
					$orderRecieved = true;
				}
			}
			
			foreach ($items as $item) {
				if($item['annonce_reception_id'] != null && $item['annonce_reception_id'] != ''){
					$orderPartRecieved = true;
				}
			}
			
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$order['order_id_grossiste'].' AND commented_object_table = "skf_orders_grossiste"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";;
			}
			
			$count = dbQueryOne("SELECT SUM(qty_ordered) as sum FROM skf_order_items WHERE order_id_grossiste = ".$order['order_id_grossiste']);
			$count = $count['sum'];
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='itemgr_".$order['order_id_grossiste']."_24'  onclick='showChild(this)'  />",
					$order['order_external_code'],
					$order['date_created'],
					$order['estimated_price'],
					$order['estimated_weight'],
					$count,
					getDropDownButton($order['order_status'], $order['order_id_grossiste']),
				);
				
			if($orderRecieved){
				$state .= '<span id="comgr_'.$order['order_id_grossiste'].'_recept" class="etat_order label label-success">Réception complete</span>';
			}
			elseif($orderPartRecieved && !$orderRecieved){
				$state .= '<span id="comgr_'.$order['order_id_grossiste'].'_recept" class="etat_order label label-info">Réception partielle</span>';
			}
			else{
				$state .= '<span id="comgr_'.$order['order_id_grossiste'].'_recept" class="etat_order label label-success"></span>';
			}	
			
			$line[] = $state;
			
			$action = "";
			$num = "comgr_".$order['order_id_grossiste'];
			
			$action .= "<p><a  href='". $root_path . "/../../app.php/exportOpti?order_id_grossiste=".$order['order_id_grossiste'] ."'> 
							<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Téléchargement du Fichier") . 
							"\" class=\"iconAction\" title= \"" . ("Téléchargement du Fichier") . "\" src=\"" . $skin_path . "images/picto/download.gif\"></a>";
							
			$action .= '<a href="#" onclick="deleteIt(\''.$num.'\');return false;"> 
						<img border="0" alt="' . ("Supprimer de la commande") . '" 
						align="absmiddle" title="Supprimer de la commande"  class="iconAction" src="' . $skin_path . 'images/picto/trash.gif"></a> ';		
			
			$action .= "<a class='closed btn' href='#' id='comment_ordergr_".$order['order_id_grossiste']."' onclick='showComments(this); return false;'><i class='fa fa-comment icon-black' style='padding-right: 0px'></i>$countComment</a>";						
			$line[] = $action;
			
			$line[] = '<input id="comgr_' . $order["order_id_grossiste"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT * FROM skf_orders_grossiste  
					";
		$where = "";
        $order =  " ORDER BY date_created DESC"; 
		
		if ( isset($this->numfilter ) && ($this->numfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " order_external_code = '$this->numfilter' ";
        }
		
        $this->_sql_query = $select . $where . $order;       
		
    }
    
    
        public function update($post)
    {
        $this->__construct( $post['numfilter'], $post['statusfilter'], $post['litigefilter']);
    }
	
/*	protected function getDropDownButton($order_status, $order_id)
	{
		
		switch ($order_status) {
			case 'new':
				$class = "btn-info";	
			break;
			case 'completed':
				$class = "btn-success";	
			break;
			case 'canceled':
				$class = "btn-danger";	
			break;
			default:
				
			break;
		}
		
		$html = '<div id="status_'.$order_id.'" class="btn-group">
					<a class="btn dropdown-toggle '.$class.'" style="color:white" data-toggle="dropdown" href="#">
						'.ucfirst($order_status).'
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#" onclick="changeStatusOrder(\''.$order_id.'\', \'New\')">New</a></li>
						<li><a href="#" onclick="changeStatusOrder(\''.$order_id.'\', \'Completed\')">Completed</a></li>
						<li><a href="#" onclick="changeStatusOrder(\''.$order_id.'\', \'Canceled\')">Canceled</a></li>
					</ul>
				 </div>';
		return $html;
	}*/
}
?>