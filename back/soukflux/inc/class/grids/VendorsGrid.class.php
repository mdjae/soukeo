<?php 
/**
 * Class pour grid Grossiste
 */
class VendorsGrid extends Grid
{
	protected $namefilter;
	protected $numclient;
    protected $vendor_id;
	protected $datefilter;
    protected $inscriptionfilter;
    protected $mailfilter;
	
	public function __construct($namefilter, $numclient, $vendor_id, $datefilter, $inscriptionfilter, $mailfilter)
	{
		$this->namefilter = $namefilter;
		$this->numclient  = $numclient ;
        $this->vendor_id  = $vendor_id;
		$this->datefilter = $datefilter;
        $this->inscriptionfilter = $inscriptionfilter;
        $this->mailfilter = $mailfilter;
			
		$this->_name = 'vendors';
		
		$this->_setSqlQuery();
		$this->_init('vendors',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","vendors",htmlentities(("Clients vendeurs")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("")),			"sortable" => false, "excelable" => false, 	"align" => 'center' ),
				array("name" => htmlentities(("Id interne")),		"sortable" => true,  "excelable" => true, 	"align" => 'center', "width" => '6%' ),
				array("name" => htmlentities(("Id Vendeur Avahis")),       "sortable" => true,  "excelable" => true,   "align" => 'center', "width" => '6%' ),
				array("name" => htmlentities(("Entreprise")),		"sortable" => true,  "excelable" => true, 	"align" => 'center', "width" => '12%' ),
				array("name" => htmlentities("Nom"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Prénom"),     "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("Email")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Phone")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Date creation")),	"sortable" => true,  "excelable" => true, 	"align" => 'center', "format"=>'date' ),
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities(("Etat")),		"sortable" => true,  "excelable" => true, 	"align" => 'center'),	
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '3%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// Contenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		//var_dump($res);
		foreach($res as $vendor)
		{
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$vendor['entity_id'].' AND commented_object_table = "sfk_vendor"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			
            if($vendor['email'] == $vendor['entity_id']){
                $email = "";
            }else{
                $email = "<a href='mailto:".htmlentities($vendor['email'])."' >".htmlentities($vendor['email'])."</a>";
            } 
             
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='".$vendor['entity_id']."'  onclick='self.location.href=\"client?type=vendor&id=".$vendor['entity_id']."\"' />
					",
					$vendor['entity_id'],
					$vendor['vendor_id'] ? $vendor['vendor_id'] : "Non inscrit",
					htmlentities($vendor['vendor_nom']),
					htmlentities($vendor['vendor_attn']),
					htmlentities($vendor['vendor_attn2']),
				    $email,
					htmlentities($vendor['telephone']),
					$vendor['created_at'],
				);

			$action = '';
			$num = "vendor_".$vendor['entity_id'];
			
			$action .= "<a class='closed btn' href='#' id='comment_vendor_".$vendor['entity_id']."' onclick='showComments(this); return false;'><i class='fa fa-comment icon-black' style='padding-right: 0px'></i>$countComment</a>";
			$line[] = $action;
			
			
			$line[] = '<input id="vendor_' . $order["entity_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT * FROM sfk_vendor ";
		
		$where = "";
        
		
		if ( isset($this->namefilter ) && ($this->namefilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= "( vendor_nom LIKE '%$this->namefilter%' OR vendor_attn LIKE '%$this->namefilter%' OR vendor_attn2 LIKE '%$this->namefilter%' ) ";
        }

		if ( isset($this->numclient ) && ($this->numclient != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " entity_id = '$this->numclient'";
        }

        if ( isset($this->vendor_id ) && ($this->vendor_id != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " vendor_id = '$this->vendor_id'";
        }
		 
		if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " created_at > (NOW() - INTERVAL ".$this->datefilter." DAY) ";
        }

        if ( isset($this->inscriptionfilter) && ($this->inscriptionfilter != "all")){
                
            if ($where == "") $where = " WHERE ";
            else $where .= " AND "; 
            
            if($this->inscriptionfilter == "0"){
                $where .= " vendor_id IS NULL ";    
            }
            elseif($this->inscriptionfilter == "1"){
                $where .= " vendor_id IS NOT NULL ";
            }
        }

        if ( isset($this->mailfilter) && ($this->mailfilter != "all")){
                
            if ($where == "") $where = " WHERE ";
            else $where .= " AND "; 
            
            if($this->mailfilter == "0"){
                $where .= " (email IS NULL OR email = '' OR email = entity_id) ";    
            }
            elseif($this->mailfilter == "1"){
                $where .= " (email IS NOT NULL AND email <> '' AND email <> entity_id) ";
            }
        }
                		
		$order =  "   ORDER BY entity_id DESC";
		 
       	$this->_sql_query = $select . $where . $order;   

    }
    
    
        public function update($post)
    {

        $this->__construct($post['namefilter'], $post['numclient'], $post['vendor_id'], $post['datefilter'], $post['inscriptionfilter'], $post['mailfilter']);
    }
}
?>