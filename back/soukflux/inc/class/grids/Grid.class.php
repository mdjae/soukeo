<?php
class Grid extends SystemObjectPersistence {
	private $_headers = array();
	private $_content = array();
	protected $_javascript = array();

	protected $_excelable = false;
	protected $_total = false;
	protected $_break = false;

	private $_nlist = 0;
	private $_count = 0;
	private $_start = 0;
	private $_end = 0;

	protected $_total_tab = array();
	protected $_break_tab = array();
	protected $_break_tab_real = array();
	protected $_break_cpt = array();
	protected $_break_total_cpt = array();
	protected $_break_total_tab = array();

	private $_index = array();

	private $_sort = "";
	private $_colsort = "";

	private $_iconclass = "";
	private $_picto = "";
	private $_boxtitle = "";

	protected $_width = "";

	public $output = "";

	// Couleurs d'alternance des lignes
	private $_color = "";
	private $_color1 = "";
	private $_color2 = "";

	// Variables pour les classes qui �tendent la grid
	private $_sql_query = "";

	protected $_grid_path;
	protected $_skin_path;

	/*
	 * Index des colonnes
	 */

	/**
	 * constructeur
	 *
	 * @param string $name Nom du tableau
	 * @param array $headers Tableau contenant la description des entetes
	 * @param array $content Contenu du tableau
	 */
	public function __construct($name, $headers, $content) {
		global $root_path, $skin_path;

		if ($this -> _grid_path == "")
			$this -> _grid_path = $root_path;
		if ($this -> _skin_path == "")
			$this -> _skin_path = $skin_path;
		$this -> _init($name, $headers, $content);
	}

	public function _init($name, $headers, $content , $interne ) {
		$this -> _name = $name;
		$this -> _headers = $headers;
		$this -> interne = $interne ;
		$this -> setContent($content);

		$this -> _width = $width;

		$this -> setNlist(SystemParams::getParam('system*nlist_element'));
		$this -> setListColors(SystemParams::getParam('system*nlist_color1'), SystemParams::getParam('system*nlist_color2'));

		// Export excel ?
		$this -> _checkExcellable();
		// Total
		$this -> _checkTotal();
		// Ruptude
		$this -> _checkBreak();

		// Recalcul des totaux
		//if ($this->_total) $this->computeTotal();

		// Recalcul des ruptures
		//if ($this->_break) $this->computeBreak();
	}

	/**
	 * Controle si la grid est exportable vers excel
	 */
	private function _checkExcellable() {
		$i = 0;
		while (!$this -> _excelable && $i < count($this -> _headers)) {
			//if (array_key_exists('excelable',$this->_headers[$i]) && $this->_headers[$i]['excelable'])
			if (is_array($this -> _headers[$i]) && array_key_exists('excelable', $this -> _headers[$i]) && $this -> _headers[$i]['excelable']) {
				$this -> _excelable = true;
			}
			$i++;
		}
	}

	/**
	 * Controle si la grid doit afficher des totaux pour certains champs
	 */
	private function _checkTotal() {
		$i = 0;
		while (!$this -> _total && $i < count($this -> _headers)) {
			//if (array_key_exists('excelable',$this->_headers[$i]) && $this->_headers[$i]['excelable'])
			if (is_array($this -> _headers[$i]) && array_key_exists('total', $this -> _headers[$i])) {
				$this -> _total = true;
			}
			$i++;
		}
	}

	/**
	 * Controle si la grid doit r�aliser des ruptures pour certains champs
	 */
	private function _checkBreak() {
		$i = 0;
		while (!$this -> _break && $i < count($this -> _headers)) {
			//if (array_key_exists('excelable',$this->_headers[$i]) && $this->_headers[$i]['excelable'])
			if (is_array($this -> _headers[$i]) && array_key_exists('break', $this -> _headers[$i])) {
				$this -> _break = true;
			}
			$i++;
		}
	}

	/**
	 * Setter du contenu
	 * @param array $content
	 */
	public function setContent($content) {
		//echo "setContent";
		$this -> _content = $content;
	}

	/**
	 * Ajout du tableau contenant le javascript de la grid
	 */
	protected function setJavascript($javascript) {
		$this -> _javascript = $javascript;
	}

	/**
	 * Récupération du javascript en string
	 */
	protected function getJavascriptStr() {
		$output = "";
		foreach ($this->_javascript as $js) {
			$output .= "\n" . $js;
		}
		return "<script type=\"text/javascript\">\n".$output."</script>";
	}

	/**
	 * Fixe la lageur (html) du tableau
	 * @param string $width Largeur du tableau (html)
	 */
	public function setWidth($width) {
		$this -> _width = $width;
	}

	/**
	 * Fixe le nombre de ligne maximum par page
	 * Surcharge le param�tre offiPSe
	 * @param integer $nlist Nombre de ligne
	 */
	public function setNlist($nlist) {
		$this -> _nlist = $nlist;
		$this -> _count = count($this -> _content);
		$this -> _start = 0;
		if ($this -> _nlist >= $this -> _count)
			$this -> _end = $this -> _count;
		else
			$this -> _end = $this -> _nlist;

	}

	/**
	 * Fixe les 2 couleurs alternatives
	 * qui est � utiliser une ligne sur deux
	 * @param string $listcol1 Couleur 1
	 * @param string $listcol2 Couleur 2
	 */
	public function setListColors($listcol1, $listcol2) {
		$this -> _color1 = $listcol1;
		$this -> _color2 = $listcol2;
	}

	/**
	 * Paramétrage du title
	 * @param $iconclass
	 * @param $picto nom du pictogramme
	 * @param $boxtitle titre de la boite
	 */
	function setTitle($iconclass, $picto, $boxtitle) {
		$this -> _iconclass = $iconclass;
		$this -> _picto = $picto;
		$this -> _boxtitle = $boxtitle;
	}

	/**
	 * Alterne la couleur de chaque ligne dans l'affichage
	 */
	private function _switchColor() {
		if ($this -> _color == $this -> _color2)
			$this -> _color = $this -> _color1;
		else
			$this -> _color = $this -> _color2;
	}

	public function canBeDisplayed() {
		if ($this -> _count != 0)
			return true;
		else
			return false;
	}

	public function display() {
		global $skin_path, $root_path, $mt1, $real_path_pop;
		$skin_path = "/skins/common/";
		if (isset($real_path_pop))
			$root_path = $real_path_pop;

		// Recalcul des ruptures
		//echo "BREAK TOTAL<br/>";
		if ($this -> _break)
			$this -> computeBreak();

		// Recalcul des totaux
		//echo "DISPLAY TOTAL<br/>";
		if ($this -> _total)
			$this -> computeTotal();

		// Largeur de la grid
		if ($this -> _width == "")
			$this -> _width = "100%";

		//$this->_content = $this->colSort(array(array('key' => 2,'sort' => 'asc')));

		// Sauvegarde avant affichage en cas de clique par l'utilisateur
		// pour un tri ou un filtre en ajax
		$this -> _save();

		$output = '';

		//$output .= "<script>var local_root_path = '" . $root_path . "'</script>";
		//$output .= '<script src="' . $root_path . '/common/grid/grid.service.js"></script>';
		$output .= '<div id="grid_' . $this -> _name . '">';
		
		$navig_str = '<div class="pager"><a href="javascript: firstPage(\'' . $this -> _name . '\',\'' . $root_path . '\')"><img class="iconNoBg" src="' . $skin_path . 'images/picto/first.gif" border="0" align="absmiddle"></a>&nbsp;';
		$navig_str .= '<a href="javascript: previousPage(\'' . $this -> _name . '\',\'' . $root_path . '\')"><img class="iconNoBg" src="' . $skin_path . 'images/picto/previous.gif" border="0" align="absmiddle"></a>&nbsp;';
		$navig_str .= '[' . ($this -> _start + 1) . ' .. ' . $this -> _end . '] / ' . $this -> _count . '&nbsp;';
		$navig_str .= '<a href="javascript: nextPage(\'' . $this -> _name . '\',\'' . $root_path . '\')"><img class="iconNoBg" src="' . $skin_path . 'images/picto/next.gif" border="0" align="absmiddle"></a>&nbsp;';
		$navig_str .= '<a href="javascript: lastPage(\'' . $this -> _name . '\',\'' . $root_path . '\')"><img class="iconNoBg" src="' . $skin_path . 'images/picto/last.gif" border="0" align="absmiddle"></a>&nbsp;';        
		$navig_str .= '<a href= "#" "><img onclick="plierMenu(this);" id="plier" src="/skins/common/images/picto/add.gif"></a></div>';
		$output .= '<div style="width: ' . $this -> _width . '" class="related">
		         		<div class="t">
		         			<div class="tl">
		         				<div class="tr">';
        if( !$this->interne){
	        $output .= '							<table cellpadding="0" cellspacing="0" border="0" width="100%" id="related_title_grid" class="top-bar">
	        								<tr><td align="left" valign="top">
	  			   							 <h3><i class="fa fa-users"></i>' . $this -> _boxtitle . '</h3>
	    		    						</td><td align="right" valign="top">'; //<img class="icon' . $this -> _iconclass . '" src="/skins/common/images/picto/' . $this -> _picto . '.gif" align="absmiddle" /> &nbsp;
	
			$output .= $navig_str;
	
			$output .= '</td></tr></table>';
		}
		$output .= '     <div class="related_in dataTables_wrapper">';

		// Si l'objet contient des ruptures, on mets une bordure au tableau
		if ($this -> _break){
			if($this -> interne){
				$output .= '<table cellpadding="0" cellspacing="0" border="1" width="100%" class="data-table dataTable ssniveau1">' . "\n";	
			}else{
				$output .= '<table cellpadding="0" cellspacing="0" border="1" width="100%" class="data-table dataTable">' . "\n";	
			}	
			
		}
		else{
			if($this -> interne){
				$output .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="data-table dataTable ssniveau1">' . "\n";
			}else{
				$output .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="data-table dataTable">' . "\n";
			}
		}
		
		$output .= '<tr>';

		$count_colspan = 0;

		//foreach($this->_headers as $header)
		for ($k = 0; $k < count($this -> _headers); $k++) {
			$output .= '<td';
			$count_colspan++;
			if (array_key_exists('width', $this -> _headers[$k]) && $this -> _headers[$k]['width']) {
				$output .= ' width="' . $this -> _headers[$k]["width"] . '"';
			}
			$output .= '>';

			// Est-ce que la ligne est triable ?
			if (array_key_exists('sortable', $this -> _headers[$k]) && $this -> _headers[$k]['sortable']) {
				if ($this -> _colsort != "" && $this -> _colsort == $k) {
					if ($this -> _sort == "desc")
						$mysort = "asc";
					else
						$mysort = "desc";

					/*$output .= '<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>&nbsp;&nbsp;</td>
                    <td><a href="javascript: gridSort(\'' . $this -> _name . '\',\'' . $k . '\',\'' . $mysort . '\',\'' . $root_path . '\');"><img src="' . $skin_path . 'images/sort_' . $this -> _sort . '_selected.gif" border="0"/></a></td>
										<td>' . $this -> _headers[$k]["name"] . '</td>
									</tr>
								</table>';*/
					$output .= '<div class="sort-table">';					
					$output .= $this -> _headers[$k]["name"];
					$output .= '<span class="sort-table-icon">';																
			        $output .= '<a href="javascript: gridSort(\'' . $this -> _name . '\',\'' . $k . '\',\'' . $mysort . '\',\'' . $root_path . '\');"><img src="' . $skin_path . 'images/sort_' . $this -> _sort . '_selected.gif" border="0"/></a>';
					$output .='</span></div>'; 			
								
								
				} else {
					/*$output .= '<table border="0" cellspacing="0" cellpadding="0">
									<tr>
									  <td rowspan="2">&nbsp;&nbsp;</td>
										<td><a href="javascript: gridSort(\'' . $this -> _name . '\',\'' . $k . '\',\'asc\',\'' . $root_path . '\');"><img src="' . $skin_path . 'images/sort_asc.gif" border="0"/></a></td>
										<td rowspan="2" >' . $this -> _headers[$k]["name"] . '</td>
									</tr>
									<tr>
										<td><a href="javascript: gridSort(\'' . $this -> _name . '\',\'' . $k . '\',\'desc\',\'' . $root_path . '\');"><img src="' . $skin_path . 'images/sort_desc.gif" border="0"/></a></td>'; */
					
					$output .= '<div class="sort-table">';					
					$output .= $this -> _headers[$k]["name"];
					$output .= '<span class="sort-table-icon">';																
					$output .='<a href="javascript: gridSort(\'' . $this -> _name . '\',\'' . $k . '\',\'asc\',\'' . $root_path . '\');"><img src="' . $skin_path . 'images/sort_asc.gif" border="0"/></a>';
					$output .=' <a href="javascript: gridSort(\'' . $this -> _name . '\',\'' . $k . '\',\'desc\',\'' . $root_path . '\');"><img src="' . $skin_path . 'images/sort_desc.gif" border="0"/></a>';					
					$output .='</span></div>'; 


					if (array_key_exists('index', $this -> _headers[$k]) && $this -> _headers[$k]['index']) {
						$output .= '<td><p><select class="selectTimesheetLine">';

						foreach ($this->_index[$k] as $option) {
							$output .= '<option>' . $option . '</option>';
						}
						$output .= '</select></p></td>';
					}

					//$output .= '</tr>
					//			</table>';
				}
				
				
				
			} else {
				$output .= '<div class="sort-table">';
				$output .= $this -> _headers[$k]["name"];
				$output .='</div>';
			}

			$output .= '</td>';
		}

		// Est-ce qu'au moins une des colonnes peut s'exporter sur excel ?
		if ($this -> _excelable) {
			$count_colspan++;
			$output .= '<td align="center" width="5%">';
			$output .= '<div class="sort-table">';
			$output .= '<a href="' . $root_path . '/common/grid/grid.excel.php?name=' . $this -> _name . '"><img class="iconAction" src="' . $skin_path . '/images/picto/excel.gif" border="0"
							alt="' . ("Exporter le tableau en Excel") . '"/></a><input type="checkbox" id="checkall-'.$this -> _name.'" onchange="checkGrid(this);"/></td>' . "\n";
			$output .='</span></div>';
		}

		$output .= '</tr>';

		// -- header separator
		//$output .= '<tr><td colspan="' . $count_colspan . '"></td></tr>' . "\n";

		// -- détail

		if ($this -> canBeDisplayed()) {
			//for ($i = $this->_start; $i < $this->_end; $i++)
			for ($i = 0; $i < $this -> _count; $i++) {

				// Est-ce que l'on doit afficher ?
				if ($i < $this -> _start or $i >= $this -> _end) {
					///////////////////////////////
					// ON N'AFFICHE PAS !
					$line = $this -> _content[$i];

					// ON DOIT GERER LES RUPTURES ET LES TOTAUX COMME SI L'ON AFFICHAIT
					for ($j = 0; $j < count($this -> _headers); $j++) {
						// Est-ce qu'il y a une rupture sur cette colonne ?
						if (array_key_exists('break', $this -> _headers[$j])) {
							// On constuit la string de recherche (dans le cas d'une sous rupture
							// On boucle � l'envers sur l'entete
							$name = "";
							$data_break = "";
							for ($jk = $j - 1; $jk >= 0; $jk--) {
								if (array_key_exists('break', $this -> _headers[$jk])) {
									// Rupture
									//$this->_break_tab[$this->_headers[$jk]['name']][$line[$jk]]--;
									//echo "Inter this->_break_tab[$name][$data_break] " . $this->_break_tab[$name][$data_break] . "<br>";
									$data_break = $line[$jk] . "|" . $data_break;
									$name = $this -> _headers[$jk]['name'] . "|" . $name;
									//

								}
							}
							$data_break .= $line[$j];
							$name .= $this -> _headers[$j]['name'];

							// On enleve du compteur cette ligne
							$this -> _break_tab[$name][$data_break]--;
							$this -> _break_tab_real[$name][$data_break]--;
							//echo "this->_break_tab[$name][$data_break] " . $this->_break_tab[$name][$data_break] . "<br>";
						}
					}

					////////////////////////////////////
					// TOTAL EN CAS DE RUPTURE
					if ($this -> _total && $this -> _break) {
						//echo "toto";
						// Il y a des totaux et des ruptures
						// On boucle sur les colonnes pour d�tecter les ruptures
						for ($k = count($this -> _headers) - 1; $k >= 0; $k--) {
							// On doit d�terminer le nom et la donn�e pour la recherche du total
							$name = "";
							$data = "";
							for ($jk = $k - 1; $jk >= 0; $jk--) {
								if (array_key_exists('break', $this -> _headers[$jk])) {
									// Rupture
									$data = $this -> _getData($line[$jk]) . "|" . $data;
									$name = $this -> _headers[$jk]['name'] . "|" . $name;
								}
							}
							$data .= $this -> _getData($line[$k]);
							$name .= $this -> _headers[$k]['name'];

							//echo "recherche rupture " . $name . " " . $data . "<br/>";

							if (array_key_exists('break', $this -> _headers[$k]) && $this -> _break_tab_real[$name][$data] == $this -> _break_cpt[$name][$data]) {
								// On a un total de rupture � supprimer des compteurs
								// On doit d�composer et s�parer les "|"
								$n = explode("|", $name);
								$d = explode("|", $data);
								$nname = "";
								$ddata = "";
								for ($z = 0; $z < count($n) - 1; $z++) {
									if ($nname != "")
										$nname .= "|";
									if ($ddata != "")
										$ddata .= "|";
									$nname .= $n[$z];
									$ddata .= $d[$z];
									$this -> _break_tab[$nname][$ddata]--;
									//echo "Rupture this->_break_tab[$nname][$ddata] " . $this->_break_tab[$nname][$ddata] . "<br>";
								}
							}
						}
					}
				} else {
					///////////////////////////////
					// ON AFFICHE !
					$line = $this -> _content[$i];

					// Est-ce que la ligne force une couleur ?
					if (is_array($line) && array_key_exists('color', $line) && $line['color'] != "") {
						$output .= '<tr bgcolor="' . $line['color'] . '">';
					} else {
						// Couleur normale
						$this -> _switchColor();
						$output .= '<tr >'; // bgcolor="' . $this -> _color . '"
					}

					//for($j = 0; $j < count($line); $j++)
					for ($j = 0; $j < count($this -> _headers); $j++) {
						$data = $this -> _getData($line[$j]);

						// Est-ce qu'un formattage est param�tr� ?
						if (array_key_exists('format', $this -> _headers[$j])) {
							// Est-ce qu'il y a un param�tre de d�cimal ?
							if (array_key_exists('decimal', $this -> _headers[$j]))
								$decimal = $this -> _headers[$j]['decimal'];
							else
								$decimal = 2;

							$data = $this -> _getValue($data, $this -> _headers[$j]['format'], $decimal);
						}

						// Est-ce qu'il y a une rupture sur cette colonne ?
						if (array_key_exists('break', $this -> _headers[$j])) {
							// RUPTURE

							// On constuit la string de recherche (dans le cas d'une sous rupture
							// On boucle � l'envers sur l'entete
							$name = "";
							$data_break = "";
							for ($jk = $j - 1; $jk >= 0; $jk--) {
								if (array_key_exists('break', $this -> _headers[$jk])) {
									// Rupture
									$data_break = $line[$jk] . "|" . $data_break;
									$name = $this -> _headers[$jk]['name'] . "|" . $name;
								}
							}
							$data_break .= $line[$j];
							$name .= $this -> _headers[$j]['name'];

							//echo $name . " " . $data_break ."<br/>";

							// Est-ce qu'on doit afficher avec un rowspan ou ne pas afficher ?
							if ($this -> _break_cpt[$name][$data_break] == 0) {
								// On doit afficher le rowspan
								// Premier affichage

								// Est-ce qu'on met un maximum de ligne dans le rowspan ou est-ce que la fin de la page est proche ?
								if ($this -> _end - $i >= $this -> _break_tab[$name][$data_break])
									$rowspan = $this -> _break_tab[$name][$data_break];
								else
									$rowspan = ($this -> _end - $i);
								$rowspan = $this -> _break_tab[$name][$data_break];
								//echo '$this->_break_tab[' . $name . '][' . $data_break . '] = ' . $this->_break_tab[$name][$data_break] . '<br/>';

								$output .= '<td rowspan="' . $rowspan . '"';

								if (is_array($this -> _headers[$j]) && array_key_exists('align', $this -> _headers[$j]))
									$output .= ' align="' . $this -> _headers[$j]['align'] . '"';
								
								if (is_array($this -> _headers[$j]) && array_key_exists('valign', $this -> _headers[$j]))
								$output .= "style='vertical-align:". $this -> _headers[$j]['valign'] . " ; '";
								// Comptage des colonnes
								//if ($j + 1 == count($line)) $output .= ' colspan="2"';
								if ($j + 1 == count($this -> _headers))
									$output .= ' colspan="2"';
								$output .= '>' . $data . "</td>\n";
							}

							// On incr�mente le compteur
							$this -> _break_cpt[$name][$data_break]++;

							// On incr�mente un comteur total pour la colonne
							$this -> _break_total_cpt[$name][$data]++;

						} else {
							// Pas de rupture, affichage classique
							$output .= '<td '; //style="border-left-width: 1px ; border-left-style: solid; "
							if (is_array($this -> _headers[$j]) && array_key_exists('align', $this -> _headers[$j]))
								$output .= ' align="' . $this -> _headers[$j]['align'] . '"';
							// Comptage des colonnes
							//if ($j + 1 == count($line)) $output .= ' colspan="2"';
							if ($j + 1 == count($this -> _headers))
								$output .= ' colspan="2"';
							$output .= '>' . $data . "</td>\n";
						}
					}

					$output .= '</tr>';

					////////////////////////////////////
					// TOTAL EN CAS DE RUPTURE
					if ($this -> _total && $this -> _break) {
						//echo "toto";
						// Il y a des totaux et des ruptures
						// On boucle sur les colonnes pour d�tecter les ruptures
						for ($k = count($this -> _headers) - 1; $k >= 0; $k--) {
							// On doit d�terminer le nom et la donn�e pour la recherche du total
							$name = "";
							$data = "";
							for ($jk = $k - 1; $jk >= 0; $jk--) {
								if (array_key_exists('break', $this -> _headers[$jk])) {
									// Rupture
									$data = $this -> _getData($line[$jk]) . "|" . $data;
									$name = $this -> _headers[$jk]['name'] . "|" . $name;
								}
							}
							$data .= $this -> _getData($line[$k]);
							$name .= $this -> _headers[$k]['name'];

							//echo "recherche rupture " . $name . " " . $data . "<br/>";

							if (array_key_exists('break', $this -> _headers[$k]) && $this -> _break_tab_real[$name][$data] == $this -> _break_cpt[$name][$data]) {
								//echo "<p>Break " . $name . " " . $data . "</p>";
								// On est en fin de groupe pour la rupture
								// On doit afficher une ligne de total
								$output .= "<tr>";

								// Boucle sur l'entete � nouveau
								// Pour afficher le bon nombre de colonne, on part de la colonne qui a la rupture ($k)
								for ($h = $k; $h < count($this -> _headers); $h++)
								//for($h = 0; $h < count($this->_headers); $h++)
								{
									// Est-ce qu'on est � la derni�re colonne ?
									if ($h + 1 == count($this -> _headers))
										$last = true;
									else
										$last = false;

									if ($this -> _headers[$h]['name'] == $this -> _headers[$k]['name']) {
										$output .= "<td";
										if ($last)
											$output .= " colspan='2'";
										$output .= ">" . ("Total") . " " . $line[$k] . "</td>\n";
									} else if (array_key_exists('total', $this -> _headers[$h])) {
										//echo $this->_headers[$k]['name'] . " " . $data . " ";
										$total = $this -> _break_total_tab[$name][$data][$this -> _headers[$h]['name']];

										// Est-ce qu'un formattage est param�tr� ?
										if (array_key_exists('format', $this -> _headers[$h])) {
											// Est-ce qu'il y a un param�tre de d�cimal ?
											if (array_key_exists('decimal', $this -> _headers[$h]))
												$decimal = $this -> _headers[$h]['decimal'];
											else
												$decimal = 2;

											$total = $this -> _getValue($total, $this -> _headers[$h]['format'], $decimal);
										}

										$output .= "<td ";
										if ($last)
											$output .= " colspan='2' ";
										if (array_key_exists('align', $this -> _headers[$h]))
											$output .= " align='" . $this -> _headers[$h]['align'] . "'";
										$output .= ">" . $total . "</td>\n";
									} else {
										$output .= "<td";
										if ($last)
											$output .= " colspan='2'";
										$output .= "></td>\n";
									}
								}

								$output .= "</tr>\n";
							}
						}
					}
				}
			}

			///////////////////////////////////
			// TOTAUX

			// Si il y a des totaux � afficher
			// Et si l'on est � la derni�re page
			// On affiche les totaux
			if ($this -> _total && $this -> _end == $this -> _count) {
				$info = false;
				// On met une ligne de total
				for ($h = 0; $h < count($this -> _headers); $h++) {
					// Est-ce qu'on est � la derni�re colonne ?
					if ($h + 1 == count($this -> _headers))
						$last = true;
					else
						$last = false;

					if (array_key_exists('total', $this -> _headers[$h])) {
						$total = $this -> _total_tab[$this -> _headers[$h]['name']];

						// Est-ce qu'un formattage est param�tr� ?
						if (array_key_exists('format', $this -> _headers[$h])) {
							// Est-ce qu'il y a un param�tre de d�cimal ?
							if (array_key_exists('decimal', $this -> _headers[$h]))
								$decimal = $this -> _headers[$h]['decimal'];
							else
								$decimal = 2;

							$total = $this -> _getValue($total, $this -> _headers[$h]['format'], $decimal);
						}

						$output .= "<td style=\"border-left:1px solid #C2C2C2 \" class=\"center\"";
						if ($last)
							$output .= " colspan='2'";
						$output .= ">" . $total . "</td>";
					} else if (!$info) {
						$output .= "<td style=\"border-left:1px solid #C2C2C2 \"";
						if ($last)
							$output .= " colspan='2'";
						$output .= ">" . ("Total") . "</td>";
						$info = true;
					} else {
						$output .= "<td style=\"border-left:1px solid #C2C2C2 \" ></td>";
					}
				}
			}

		} else {

			$output .= '<tr bgcolor="#FFFFFF"><td colspan="' . $count_colspan . '"><p><br/>';
			$output .= '<img class="iconRed" src="' . $skin_path . '/images/picto/comment.gif" border="0" align="absmiddle"/>';
			$output .= '&nbsp;<i>' . htmlentities(("Aucun élément à afficher")) . '</i></p></td></tr>' . "\n";

		}

		$output .= '</table>';

		$output .= "</div></div>\n
		          \n
		              </div></div></div>";

		// bas de la grid : navigation
		if( !$this->interne){
		
			$output .= '<div style="width: ' . $this -> _width . '" class="related">
			        <div class="bt"><div class="btl"><div class="btr">
	        		<table cellpadding="0" cellspacing="0" border="0" width="100%" id="related_title_grid" class="top-bar bottom-bar">
	        		<tr><td align="right" valign="top">';
			$output .= $navig_str;
			$output .= '</td></tr></table>
			            </div></div></div></div>';
		}
		$output .= "</div>";

		
		// Javascript de la grid
		If (!empty($this -> _javascript) && $this -> _javascript != null){
			$output .= $this->getJavascriptStr();	
		}
		

		return $output;
	}

	public function nextPage() {
		if ($this -> _start + $this -> _nlist < $this -> _count) {
			$this -> _start += $this -> _nlist;
			if ($this -> _end + $this -> _nlist <= $this -> _count) {
				$this -> _end += $this -> _nlist;
			} else {
				$this -> _end = $this -> _count;
			}
		}
	}

	public function lastPage() {
		// Est-ce que le max est plus petit que le nombre de ligne par page ?
		if ($this -> _nlist > $this -> _count)
			$this -> _start = 0;
		else
			$this -> _start = intval($this -> _count / $this -> _nlist) * $this -> _nlist;
		$this -> _end = $this -> _count;
	}

	public function previousPage() {
		if ($this -> _start >= $this -> _nlist) {
			$this -> _start -= $this -> _nlist;
			$this -> _end = $this -> _start + $this -> _nlist;
		}
	}

	public function firstPage() {
		$this -> _start = 0;
		// Est-ce que le max est plus petit que le nombre de ligne par page ?
		if ($this -> _nlist > $this -> _count)
			$this -> _end = $this -> _count;
		else
			$this -> _end = $this -> _nlist;
	}

	public function colSort($keys) {
		// Si le tableau a 0 lignes, on ne fait rien
		if (count($this -> _content) > 0) {
			// List As Columns
			foreach ($this->_content as $key => $row) {
				foreach ($keys as $k) {
					$cols[$k['key']][$key] = $row[$k['key']];
				}
			}

			// List original keys
			$idkeys = array_keys($this -> _content);

			// Sort Expression
			$i = 0;
			$sort = '';
			foreach ($keys as $k) {
				if ($i > 0)
					$sort .= ',';
				$sort .= '$cols[' . $k['key'] . ']';
				if (array_key_exists('sort', $k))
					$sort .= ',SORT_' . strtoupper($k['sort']);
				if (array_key_exists('type', $k))
					$sort .= ',SORT_' . strtoupper($k['type']);
				$i++;
			}

			//$sort .= ',$idkeys';
			$sort .= ',$this->_content';

			// Sort Funct
			$sort = 'array_multisort(' . $sort . ');';
			//var_dump( $this->_content);
			//echo $sort;
			eval($sort);
			//	var_dump( $this->_content);
			// Rebuild Full Array
			foreach ($idkeys as $idkey) {
				$result[$idkey] = $this -> _content[$idkey];
			}

			return $result;
		} else {
			return $this -> _content;
		}

	}

	/**
	 *
	 * @col : int number of the column
	 * @sort : string asc ou desc
	 */
	public function gridSort($col, $sort) {
		// Si le tableau ne contient pas de rupture c'est simple
		if (!$this -> _break) {
			$this -> _colsort = $col;
			$this -> _sort = $sort;
			$this -> _content = $this -> colSort(array( array('key' => $this -> _colsort, 'sort' => $this -> _sort)));
		} else {
			// On trie par cette colonne
			$this -> _content = $this -> colSort(array( array('key' => $col, 'sort' => $sort)));

			// Il faut trier également les colonnes avec rupture qui sont plus � gauche
			// On recherche la position de l'entete � trier
			//echo "toto " . $cpt . " " . $col . " " . $sort . "<br/>";

			for ($k = $col - 1; $k >= 0; $k--) {
				// Est-ce qu'il y a une rupture ?
				if ($this -> _headers[$k]['break']) {
					// Oui rupture,on trie
					//echo "tri de " . $k . "<br/>";
					$this -> _content = $this -> colSort(array( array('key' => $k, 'sort' => 'asc')));
				}
			}
			$this -> _colsort = $col;
			$this -> _sort = $sort;
		}
	}

	public function getExcel() {
		$file = "Excel_" . $_GET['name'] . date("Ymd") . ".xls";
		$fname = tempnam($TMP_dir, $file);
		$workbook = new writeexcel_workbook($fname);

		# Some common formatsz
		$center = &$workbook -> addformat(array('align' => 'center'));
		$normal = &$workbook -> addformat(array('align' => 'left'));
		$normal -> set_text_wrap();
		$heading = &$workbook -> addformat(array('align' => 'center', 'bold' => 1));
		$heading -> set_text_wrap();

		$worksheet1 = &$workbook -> addworksheet($_GET['name']);

		// Nombre de colonne excel
		$nb_excel = 0;
		$col_excel = Array();
		for ($i = 0; $i < count($this -> _headers); $i++) {
			if (array_key_exists('excelable', $this -> _headers[$i]) && $this -> _headers[$i]['excelable']) {
				$nb_excel += 1;
				$col_excel[] = $i;
			}
		}

		$worksheet1 -> set_column(0, $nb_excel, 25);

		// Entete de la feuille excel
		for ($i = 0; $i < count($col_excel); $i++) {
			if (array_key_exists('excel_name', $this -> _headers[$col_excel[$i]]))
				$worksheet1 -> write(0, $i, $this -> _headers[$col_excel[$i]]['excel_name'], $heading);
			else
				$worksheet1 -> write(0, $i, $this -> _headers[$col_excel[$i]]['name'], $heading);
		}

		// Contenu
		//for($i = 0; $i < 26; $i++)
		for ($i = 0; $i < $this -> _count; $i++) {
			for ($j = 0; $j < count($col_excel); $j++) {
				$data = $this -> _content[$i][$col_excel[$j]];
				if (is_array($data)) {
					// Le contenu est un tableau
					if (array_key_exists('txt', $data))
						$data = $data['txt'];
					else if (array_key_exists('html', $data))
						$data = $data['html'];
					else
						$data = "";
				}
				$worksheet1 -> write($i + 1, $j, $data, $normal);
			}
		}

		$workbook -> close();

		//if(strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
		//	header('Content-Type: application/force-download');
		//else header('Content-Type: application/octet-stream');
		//header("Content-Type: application/x-msexcel; name=\"" . $file . "\"");
		header("Content-Type: application/vnd-msexcel;");
		header("Content-Disposition: attachment; filename=\"" . $file . "\"");
		$fh = fopen($fname, "rb");
		fpassthru($fh);
	}

	public function _makeHeaders() {
		return array();
	}

	public function _makeContent() {
		return array();
	}

	public function reload() {
		$this -> _headers = $this -> _makeHeaders();
		$this -> setContent($this -> _makeContent());

		// On trie � nouveau la grid si n�cessaire
		if ($this -> _sort != "" && $this -> _colsort != "") {
			$this -> _content = $this -> colSort(array( array('key' => $this -> _colsort, 'sort' => $this -> _sort)));
		}

		$this -> _count = count($this -> _content);
		if ($this -> _end > $this -> _count)
			$this -> _end = $this -> _count;
		if ($this -> _end == ($this -> _count - 1))
			$this -> _end = $this -> _count;
	}

	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @param $id2
	 * @param $id3
	 * @return boolean
	 */
	public function delete($id1, $id2, $id3) {
		return true;
	}

	public function setTotal($name) {
		for ($i = 0; $i < count($this -> _headers); $i++) {
			//echo $this->_headers[$i]['name'] . " = " . $name;
			if ($this -> _headers[$i]['name'] == $name) {
				$this -> _headers[$i]["total"] = true;
				$this -> _total = true;
			}
		}

		// Recalcul des totaux
		//$this->_computeBreak();
		//$this->_computeTotal();
	}

	public function setBreak($name) {
		for ($i = 0; $i < count($this -> _headers); $i++) {
			if ($this -> _headers[$i]['name'] == $name) {
				$this -> _headers[$i]['break'] = true;
				$this -> _break = true;
			}
		}

		// Recalcul des totaux
		//$this->_computeBreak();
		//$this->_computeTotal();
	}

	/**
	 * Param�tre le format d'une colonne
	 * @param $name
	 * @param $format
	 * @param $decimal
	 * @return unknown_type
	 */
	public function setFormat($name, $format, $decimal = 2) {
		for ($i = 0; $i < count($this -> _headers); $i++) {
			if ($this -> _headers[$i]['name'] == $name) {
				$this -> _headers[$i]['format'] = $format;
				$this -> _headers[$i]['decimal'] = $decimal;
			}
		}
	}

	/**
	 * Param�tre l'alignement d'une colonne
	 * @param $name
	 * @param $align
	 * @return unknown_type
	 */
	public function setAlign($name, $align) {
		for ($i = 0; $i < count($this -> _headers); $i++) {
			if ($this -> _headers[$i]['name'] == $name) {
				$this -> _headers[$i]['align'] = $align;
			}
		}
	}

	/**
	 * Calcul les diff�rents totaux
	 * @return unknown_type
	 */
	public function computeTotal() {
		$this -> _total_tab = array();
		$this -> _break_total_tab = array();

		// Est-ce qu'il y a des ruptures ?
		if ($this -> _break) {
			// Totaux avec ruptures
			foreach ($this->_content as $line) {
				for ($i = 0; $i < count($line); $i++) {
					//Est-ce que cette colonne demande un total ?
					if (array_key_exists("total", $this -> _headers[$i])) {
						// On doit ajouter la valeur sur les ent�tes qui sont en rupture
						for ($j = 0; $j < count($this -> _headers); $j++) {
							if (array_key_exists('break', $this -> _headers[$j])) {
								// R�cup�ration des colonnes pr�c�dentes qui sont peut-�tre en rupture
								$name = "";
								$data = "";
								for ($jk = $j - 1; $jk >= 0; $jk--) {
									if (array_key_exists('break', $this -> _headers[$jk])) {
										$name = $this -> _headers[$jk]['name'] . "|" . $name;
										$data = $this -> _getData($line[$jk]) . "|" . $data;
									}
								}
								$name .= $this -> _headers[$j]['name'];
								$data .= $this -> _getData($line[$j]);

								// Rupture sur cette colonne
								// Ajout
								$val = $this -> _getData($line[$i]);
								$this -> _break_total_tab[$name][$data][$this -> _headers[$i]['name']] += $val;
							}
						}
					}
				}
			}

			//echo "<pre>"; print_r($this->_break_total_tab); echo "</pre>";

			// Les lignes de totaux ont un impact sur le rowspan de la rupture d'un niveau au dessus
			// Il faut incr�menter les compteurs avec le nombre de variantes
			for ($i = 0; $i < count($this -> _headers); $i++) {
				if (array_key_exists('break', $this -> _headers[$i])) {
					// Rupture
					$name = "";
					for ($j = $i - 1; $j >= 0; $j--) {
						if (array_key_exists('break', $this -> _headers[$i])) {
							$name = $this -> _headers[$j]['name'] . "|" . $name;
						}
					}
					$name .= $this -> _headers[$i]['name'];
					//echo $name. "<br/>";

					// Boucle sur les valeurs
					if (array_key_exists($name, $this -> _break_tab)) {
						//echo $name;
						foreach ($this->_break_tab[$name] as $key => $val) {
							// Double boucle sur tout le tableau
							foreach ($this->_break_tab as $t) {
								foreach ($t as $k => $v) {
									//echo $key . " " . $k . " POPO ";
									if (strpos($k, $key . "|") !== FALSE) {
										// On en a 1
										// Incr�ment
										//echo $key . " " . $k . " " . $this->_break_tab[$name][$key] . "<br/>";
										$this -> _break_tab[$name][$key]++;
									}
								}
							}
						}
					}
				}
			}
			//echo "<pre>"; print_r($this->_break_tab); echo "</pre>";
		}

		//echo "<p>TOTAL</p>";
		//echo "<pre>"; print_r($this->_break_total_tab); echo "</pre>";
		//echo "<p>BREAK APRES TOTAL</p>";
		//echo "<pre>"; print_r($this->_break_tab); echo "</pre>";

		// Dans tous les cas, il faut calculer un total g�n�ral

		//echo "<pre>";
		//print_r($this->_headers);
		//echo "</pre>";
		// Pas de rupture
		foreach ($this->_content as $line) {
			for ($i = 0; $i < count($line); $i++) {
				// Est-ce que cette colonne demande un total ?
				if (array_key_exists("total", $this -> _headers[$i])) {
					//echo "toto";
					// On ajoute la valeur de la colonne au tableau des totaux
					$val = $this -> _getData($line[$i]);

					$this -> _total_tab[$this -> _headers[$i]['name']] += $val;
				}
			}
		}
	}

	/**
	 * Comptages pour les ruptures
	 * @return unknown_type
	 */
	public function computeBreak() {
		$this -> _break_tab = array();

		// Les lignes de total vont influencer les compteurs
		// que nous allons initialiser ici pour �tablir les rownspan du tableau
		$nb_total = 0;
		if ($this -> _total) {
			//echo "toto";
			// Il y a des totaux
			// Combien ?
			foreach ($this->_headers as $h) {
				if (array_key_exists('total', $h))
					$nb_total++;
			}
		}

		//echo "<p>nb total " . $nb_total . "</p>";

		// Boucle sur les entetes
		for ($i = 0; $i < count($this -> _headers); $i++) {
			// Est-ce que cette colonne supporte une rupture ?
			if (array_key_exists("break", $this -> _headers[$i])) {
				// OK rupture

				// R�cup�ration des colonnes pr�c�dentes qui sont peut-�tre en rupture
				$subbreak = array();
				for ($j = $i - 1; $j >= 0; $j--) {
					if (array_key_exists('break', $this -> _headers[$j])) {
						$subbreak[] = array($this -> _headers[$j]['name'], $j);
					}
				}

				// Boucle sur le contenu, d�tection des ruptures
				$content = "";
				$premier = true;
				foreach ($this->_content as $line) {
					// Le nom est soit le nom de la colonne, soit un compos� des ruptures de niveau sup�rieur
					// avec s�paration "|"
					$name = "";
					if (count($subbreak > 0)) {
						foreach ($subbreak as $b) {
							$name = $b[0] . "|" . $name;
						}
					}
					$name .= $this -> _headers[$i]['name'];

					$line_content = "";
					if (count($subbreak > 0)) {
						foreach ($subbreak as $b) {
							$line_content = $this -> _getData($line[$b[1]]) . "|" . $line_content;
						}
					}
					$line_content .= $this -> _getData($line[$i]);

					if ($premier || $line_content != $content) {
						// Rupture
						$content = "";
						if (count($subbreak > 0)) {
							foreach ($subbreak as $b) {
								$content = $this -> _getData($line[$b[1]]) . "|" . $content;
							}
						}
						$content .= $this -> _getData($line[$i]);
						$premier = false;

						$this -> _break_tab[$name][$content] = 1;
						$this -> _break_cpt[$name][$content] = 0;
					} else {
						// Incr�ment du compteur
						$this -> _break_tab[$name][$content]++;
					}
				}
			}
		}

		// On garde une copie des compteurs r�ellement initialis�s
		// Car avec les totaux, ces compteurs peuvent �voluer
		$this -> _break_tab_real = $this -> _break_tab;

		//echo "<p>BREAK</p>";
		//echo "<pre>"; print_r($this->_break_tab); echo "</pre>";
	}

	private function _getValue($val, $format, $decimal = 2) {
		// Si la valeur est vide, on laisse � vide
		$output = $val;

		if ($output != "") {
			switch ($format) {
				case "money" :
					$output = number_format($val, $decimal, ',', ' ') . " &euro;";
					break;
				case "number" :
					// Si la valeur n'est pas num�riques, on n'y touche pas
					if (is_numeric($val))
						$output = number_format($val, $decimal, ',', ' ');
					break;
				case "pourcent" :
					$output = number_format($val, 0, ',', ' ') . " %";
					break;
				case "byte" :
					$output = size($val);
					break;
				case "date" :
					$output = smartDate($val);
					break;
				case "normalDate" :
					$output = writeNormalDate($val);
					break;
			}
		}

		//echo $format;
		return $output;
	}

	private function _getData($val) {
		$data = "";

		if (is_array($val)) {
			//if (array_key_exists("total",$val)) $data = $val['total'];
			//else
			
			
			if (array_key_exists("html", $val))
				$data = $val['html'];
			else
				$data = $val['txt'];
		} else
			$data = $val;

		return $data;
	}

}
?>
