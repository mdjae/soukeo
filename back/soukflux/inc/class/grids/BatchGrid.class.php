<?php 
class BatchGrid extends Grid
{
	public function __construct($sql_query)
	{	
		$this->_name = 'batch';
		$this->_sql_query = $sql_query;
		$this->_init('batch',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_batch",htmlentities(("Tâches périodiques")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("groupe")),			"sortable" => true, "excelable" => true,"align" => 'center' , "break" => true , "valign" => "middle" ),
				array("name" => htmlentities(("N°")),					"sortable" => true, "excelable" => true,"align" => 'center', "width" => '5%'),
				array("name" => 			("Programme"),				"sortable" => true, "excelable" => true,"align" => 'center', "width" => '20%'),
				array("name" => htmlentities(("Commentaire")),			"sortable" => true, "excelable" => true,"align" => 'center'),
				array("name" => 			("Heure"),					"sortable" => true, "excelable" => true,"align" => 'center', "width" => '3%'),
				array("name" => 			("Jour"),					"sortable" => true, "excelable" => true,"align" => 'center', "width" => '3%'),
				array("name" => 			("Mois"),					"sortable" => true, "excelable" => true,"align" => 'center', "width" => '3%'),
				array("name" =>	htmlentities( ("Année")),				"sortable" => true, "excelable" => true,"align" => 'center', "width" => '5%'),
				array("name" => 			("Email"),					"sortable" => true, "excelable" => true,"align" => 'center'),
				array("name" =>	htmlentities( ("Paramètres")),			"sortable" => true, "excelable" => true,"align" => 'center'),
				array("name" => htmlentities(("Dernière exécution")),	"sortable" => true, "excelable" => true,"align" => 'center', "width" => '15%'),
				array("name" => 			("Actif"),					"sortable" => true, "excelable" => true,"align" => 'center', "width" => '4%'),
				array("name" =>				("Actions"),				"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;

		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $prog)
		{
			eval('$class = ' . $prog['class'] . '::getDescription();');
			
			$line =
				array(
				$prog['group'],
					$prog['prog_id'],
					htmlentities($class),
					$prog['comment'],
					$prog['hour'],
					$prog['day'],
					$prog['month'],
					$prog['year'],
					$prog['email'],
					$prog['params']
				);
			
			if ($prog['last_exec'] != "") $line[] = (smartDate($prog['last_exec']));
			else $line[] = ("Jamais");
					
			if ($prog['active'] == "TRUE")
			{
				$active_html = '<img border="0" align="absmiddle" alt="' . ("Programmation active") . '" class="iconCircleCheck" src="' . $skin_path . 'images/picto/check.gif">';
				$active_txt = ("Oui"); 
			}
			else
			{
				$active_html = '<img border="0" align="absmiddle" alt="' . ("Programmation active") . '" class="iconCircleWarn" src="' . $skin_path . 'images/picto/close.gif">';
				$active_txt = ("Non");
			}
			
			$line[] = array("txt" => $active_txt, "html" => $active_html);

			$action = '';
			$action .= '<p>' . popupLink('/o_admin/batch/popup_log/index.php?prog_id=' . $prog["prog_id"], ("Suivi des logs"), 1200, 480,"jdiag") . 
				'<img border="0" align="absmiddle" alt="' . ("Suivi des logs") . '" class="iconAction" src="' . $skin_path . 'images/picto/zoomin.gif"></a>';

			$action .= '' . popupLink('/o_admin/batch/popup_addmod/index.php?prog_id=' . $prog["prog_id"], ("Modification d'une programmation"), 800, 600,"jdiag") . 
				'<img border="0" align="absmiddle" alt="' . ("Modification d'une programmation") . '" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
			
			$action .= "<a href=\"#\" onClick=\"confirmDelete('" . ("Etes-vous certain de vouloir supprimer la programmation ?") . "', " . $prog['prog_id'] . ", '', '', '" . $this->_name . "', '" . $root_path . "')\"> 
				<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Suppression d'une programmation") . "\" class=\"iconAction\" src=\"" . $skin_path . "images/picto/trash.gif\"></a>";
			
			$action .= '' . popupLink('/o_admin/batch/popup_exec/index.php?class=' . $prog["class"] . '&prog_id=' . $prog['prog_id'] . '&interactif=' . true, ("Exécuter le batch"), 400, 400,"jdiag") . 
				'<img border="0" align="absmiddle" alt="' . ("Exécuter le batch") . '" class="iconAction" src="' . $skin_path . 'images/picto/reload.gif"></a></p>';
			
			$sql = "SELECT * FROM `c_export_files` WHERE BATCH = '".$prog['class']."' ORDER BY DATE_CREATE DESC LIMIT 1";
			
			$result = dbQueryAll($sql);
			
			if(!empty($result)){
				$info_file = $result[0];
				$action .= "<p><a href='". $root_path . "/../../o_services/download/download.php?path=". urlencode("/opt/soukeo/export/") . "&file=" . urlencode($info_file['FILE_NAME']) . "'\"> 
							<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Téléchargement du Fichier") . 
							"\" class=\"iconAction\" title= \"" . ("Téléchargement du log") . "\" src=\"" . $skin_path . "images/picto/download.gif\"></a></p>";
			}
			
			
			$line[] = $action;
			$content[] = $line;
		}
		
		return $content;
	}
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @param $id2
	 * @param $id3
	 * @return boolean
	 */
	public function delete($id1, $id2, $id3)
	{
		// Suppression des logs
		$query = "DELETE FROM c_batch_log
					WHERE prog_id = " . $id1;
		dbQuery($query);
					
		$query = "DELETE FROM c_batch
					WHERE prog_id = " . $id1;
		dbQuery($query);
		
		return true;
	}
}
?>
