<?php 

/**
 * Class pour grid Ecommercant
 */
class CategoriesGrossisteGrid extends Grid {
	
	protected $grossiste_id;
	protected static $db;
	protected $filterActive;
	
	public function __construct($vendor_id, $filterActive = "")
	{
		self::$db = new BusinessSoukeoModel();
			
		$this -> filterActive = $filterActive;
			
		$this->_name 		= 'categoriesgrossiste';
		
		$this -> grossiste_id = $vendor_id;
		
		//$this->_sql_query = $sql_query;
		$this->_setSqlQuery();

		$this->_init('categoriesgrossiste',$this->_makeHeaders(),$this->_makeContent());


		$this->setTitle("NoBg","grids_categoriesgrossiste",htmlentities(("Qualification Categories Grossiste ")));
		
	}

	public function _makeHeaders() {
		$headers =
			array(
				array("name" => htmlentities(("Catégorie")),					"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '25%'					),
				array("name" => htmlentities( "Poids par défaut"),					"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '18%'					),
				array("name" => htmlentities(("Auto association")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '15%', "total" => true	),
				array("name" => htmlentities(("Associé à ")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '30%', "total" => true	),
				array("name" => 			( "Save"),				"sortable" => false, "excelable" => false,"align" => 'center', "width" => '15%'),
				array("name" => htmlentities(("Séléction")), 					"sortable" => false, "excelable" => false,"align" => 'center', "width" => '6%')

			);
		return $headers;
	}

	public function _makeContent() {
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
		
		$content = array();
		$javascript = array();
		foreach($res as $cat)
		{
			$cat_id = dbQueryOne('SELECT c.cat_id FROM sfk_assoc_categ_grossiste acg 
									 INNER JOIN sfk_categorie c ON c.cat_id = acg.cat_id 
									 WHERE acg.GROSSISTE_ID = '.$this->grossiste_id.' AND acg.categ_grossiste = "' . $cat['CATEGORY']. '"');
									 
			$cat_label = self::$db -> getStrCateg($cat_id['cat_id']);						 
			if(!$cat_id){
				$cat_label = "N/A";
			}else{
				//$cat_label = $cat_label['cat_label'];
			}

			
			if (!$cat['AUTO_ASSOC'])
			{
				$locked_html = '<img border="0" alt="'. ("Pas auto associé")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/denied.gif">';
				$locked_txt = ("Oui");
			}
			else
			{
				$locked_html = '<img border="0" alt="'. ("Auto associée")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/check.gif">';
				$locked_txt = ("Non");
			}
			$line =
				array(
					htmlentities($cat['CATEGORY']),
					'<input class="poids" type="text" size="3" value="'.numberByLang($cat['POIDS_DEFAULT']).'" />',
					array('html' => $locked_html, 'txt' => $locked_txt),
					htmlentities($cat_label)
				);
			 

			$action = '';
			
			$action .= '<a href="#" onclick = "savePoidsCat(this, '.$this -> grossiste_id.'); return false;" id = "'.str_replace(array('"', "'", " "), "_", strtolower(stripAccents($cat['CATEGORY']) ) ).'_save"> 
						<img border="0" alt="' . ("Enregistrer modifications sur la ligne") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/save.gif"></a>';

			$line[] = $action;			
			$input = '<input id="pid_' . str_replace(array('"', "'", " "), "_", strtolower(stripAccents($cat['CATEGORY']) ) ) . '" type="checkbox" onchange="selectOneSession(this)"/>';
			$line[] = $input;
			$content[] = $line;
		}
		
		return $content;
	}
	
	
	
	protected function _setSqlQuery(){
		 
		$select =  "SELECT *
					FROM `sfk_cat_poids` ";
					
		$where = 	"WHERE GROSSISTE_ID = ".$this -> grossiste_id. " ";		
		
		////////////////////////////// SEARCH FILTER CATEGORY ////////////////////////////////////////////// 					
       	if ( isset($this -> filterActive ) && ($this -> filterActive != "all") && ($this -> filterActive != "")) {
        	if ($where == "")	$where = " WHERE ";
            else 				$where .= " AND ";
			
            $where .= " AUTO_ASSOC = '". $this -> filterActive ."' ";
        }
        
        $this->_sql_query = $select . $where . $order;      
    }
    
	
	public function _getSqlQuery()
	{
		return $this->_sql_query;
	}
	
    /**
	 * lorsqu'une demande de rafraichissement est demandée les paramétres sont retournés
	 */
	public function update($post) {
		$grossiste_id = $this->grossiste_id;
		$this->__construct( $grossiste_id , $post['fActive']);
    }
  
}
?>