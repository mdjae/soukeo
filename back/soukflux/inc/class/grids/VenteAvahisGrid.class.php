<?php 
/**
 * Class pour grid Ecommercant
 */
class VenteAvahisGrid extends Grid {
	public function __construct($filtercat='', $nom='',$pricemin='', $pricemax='', $weightmin=0.2, $weightmax='30', $quantitymin='4', $idgross='1', $added='1', $selling ='', $coefmax ='6', $margemin='')
	{	
		$this->_name 		= 'venteavahis';
		$this->filtercat    = $filtercat;
		$this->nom 			= $nom ;
		$this->pricemin 	= $pricemin ;
		$this->pricemax 	= $pricemax ;
		$this->weightmin 	= $weightmin;
		$this->weightmax 	= $weightmax;
		$this->quantitymin  = $quantitymin;
		$this->idgross 		= $idgross ;
		$this->added 		= $added;
		$this->selling 		= $selling;
		$this->coefmax 		= $coefmax;
		$this->margemin 	= $margemin;
		
		//$this->_sql_query = $sql_query;
		$this->_setSqlQuery();

		$this->_init('venteavahis',$this->_makeHeaders(),$this->_makeContent());
		
		$js = array("$( document ).ready(function() {
						 if($('#grid_venteavahis').length > 0) {
							 var theData = {};
							 var aInput = document.getElementsByTagName('input');
							 for (var i = 0; i < aInput.length; i++) {
								if (aInput[i].type == 'checkbox' && aInput[i].id != '') {
									aInput[i].checked = false;
								}
							 }
							 $.ajax({
							 type : 'POST',
							 url : '/app.php/getallselectedventesav',
							 data : theData,
							 success : function(data) {
								if(data != ''){
									var obj = jQuery.parseJSON(data);
					
									 $.each(obj, function(i, item){
									 	$('#grid_venteavahis #pid_'+obj[i]).attr('checked', true);
									 });	
								}
							 }
						 });
					 }})");
		$this->setJavascript($js);
		$nom_grossisste = dbQueryOne("SELECT GROSSISTE_NOM FROM sfk_grossiste WHERE GROSSISTE_ID = $idgross");
		$this->setTitle("NoBg","grids_venteavahis",htmlentities(("Qualification Ventes ".$nom_grossisste['GROSSISTE_NOM'])));
		
	}

	public function _makeHeaders() {
		$headers =
			array(
				array("name" => htmlentities(("ID")),					"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '1%'					),
				array("name" => 			( "Nom"),					"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '30%'					),
				array("name" => htmlentities(("Prix Achat")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%', "total" => true	),
				array("name" => htmlentities(("Qté")),					"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%', "total" => true	),
				array("name" => htmlentities(("Poids")),				"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%', "total" => true	),
				array("name" => htmlentities(("Poids Vol.")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%'					),
				array("name" => htmlentities(("Prix/Kg")),				"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%', "total" => true	),
				array("name" => htmlentities(("% Octr. mer")),			"sortable" => false,  "excelable" => true, "align" => 'center', "width" => '6%'	),
				array("name" => htmlentities(("% Marge")),				"sortable" => false,  "excelable" => false, "align" => 'center', "width" => '6%'	),
				array("name" => htmlentities(("Marge Net")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%'					),
				array("name" => htmlentities(("Prix revient")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%'					),
				array("name" => htmlentities(("% TVA")),				"sortable" => false,  "excelable" => true, "align" => 'center', "width" => '6%'					),
				array("name" => htmlentities(("Prix vente livré")),		"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%'					),
				array("name" => htmlentities(("Coef")),					"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%', 'format' => 'number'					),
				//array("name" => htmlentities(("ID Grossiste")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%',					),
				array("name" => htmlentities(("Vendu")),				"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '6%'					),
				array("name" => 			( "Save"),				"sortable" => false, "excelable" => false,"align" => 'center', "width" => '1%'),
				array("name" => htmlentities(("")), 					"sortable" => false, "excelable" => false,"align" => 'center')

			);
		return $headers;
	}

	public function _makeContent() {
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
		$grilleChronopost = dbQueryAll("SELECT * FROM sfk_chronopost");
		
		$content = array();
		$javascript = array();
		foreach($res as $prod)
		{
			//if (dbQueryAll('SELECT id_produit FROM sfk_product_ecommercant_stock_prix WHERE VENDOR_ID = 24 AND id_produit = "'.$prod['EAN'].'"'))
			if (dbQueryAll('SELECT id_produit FROM sfk_product_ecommercant_stock_prix WHERE VENDOR_ID = 24 AND ref_ecommercant = "'.$prod['ID_PRODUCT'].'" AND QUANTITY >= 5'))
			{
				$locked_html = '<img id="'.$prod["ID_PRODUCT"].'_icnvente" border="0" alt="'. ("Produit vendu")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/check.gif">';
				$locked_txt = ("Oui");
			}
			else
			{
				$locked_html = '<img id="'.$prod["ID_PRODUCT"].'_icnvente" border="0" alt="'. ("Produit non vendu")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/denied.gif">';
				$locked_txt = ("Non");
			}
			
			$prod['VOLUMETRIC_WEIGHT'] && $prod['VOLUMETRIC_WEIGHT'] != "0" ? $poids = $prod['VOLUMETRIC_WEIGHT'] : $poids = $prod['WEIGHT'] ;
			$aerien = (float)SystemParams::getParam('grossiste*aerienchrono') * (float)$poids;

			
			
			$frais_livr = $prod['FRAIS_LIVR'];
			
			//On retrouve alors le taux TVA param entré
			if(!is_null($prod['TVA_PROD'])){
				$paramTauxTVA = $prod['TVA_PROD'] * 100;
			}
			else{
				//On retrouve alors le taux TVA param entré
				$paramTauxTVA = $prod['PERCENT_TVA'] * 100;
			} 
			//Retrouver le 20 % ou les changements
			if(!is_null($prod['MARGE_PROD'])){
				$percentMarge = $prod['MARGE_PROD'] * 100;
			}
			else{
				//Retrouver le 25 % ou les changements
				$percentMarge = $prod['PERCENT_MARGE'] * 100 ;	
			}

			if(!is_null($prod['OM_PROD'])){
				$percentOctroi = $prod['OM_PROD'] * 100;
			}
			else{
				$percentOctroi = $prod['OM'] / ($prod['PRICE_PRODUCT'] + $aerien) * 100;	
			}

			
			$action = "majCalcul(this, $frais_livr, $aerien)";
			
			$line =
				array(
					$prod['ID_PRODUCT'],
					htmlentities($prod['NAME_PRODUCT']),
					array("sort" => (float)$prod['PRICE_PRODUCT'], "txt" => numberByLang($prod['PRICE_PRODUCT']), "html" => '<span id="'.$prod["ID_PRODUCT"].'_prxachat" >' . numberByLang($prod['PRICE_PRODUCT']). '</span>'),
					$prod['QUANTITY'],
					numberByLang($prod['WEIGHT']),
					numberByLang($prod['VOLUMETRIC_WEIGHT']) ,					
					numberByLang($prod['PRIX_KILO']),
					
					'<input id="'.$prod["ID_PRODUCT"].'_prctoctroimer" type="text" size="3" value="'.numberByLang($percentOctroi).'" onkeyup="'.$action.'" />',
					'<input id="'.$prod["ID_PRODUCT"].'_prctmarge" type="text" size="3" value="'.numberByLang($percentMarge).'" onkeyup="'.$action.'" />',
					
					array('sort' => (float)$prod['PRIX_LIVRE'] * $prod['PERCENT_MARGE'] , 'html' => '<span id="'.$prod["ID_PRODUCT"].'_margenet" >'.numberByLang($prod['PRIX_LIVRE'] * $prod['PERCENT_MARGE']).'</span>'),
					array('sort' => (float)$prod['PRIX_LIVRE'], 'html' => '<span id="'.$prod["ID_PRODUCT"].'_prxrevient" >'.numberByLang($prod['PRIX_LIVRE']).'</span>'),
					'<input id="'.$prod["ID_PRODUCT"].'_prctTVA" type="text" size="3" value="'.numberByLang($paramTauxTVA).'" onkeyup="'.$action.'" />',
					
					array('sort' => (float)$prod['PRIX_FINAL'], 'html' => '<span id="'.$prod["ID_PRODUCT"].'_prxlivre" >'.numberByLang($prod['PRIX_FINAL']).'</span>') ,
					array('sort' => (float)$prod["COEF"], 'html' => '<span id="'.$prod["ID_PRODUCT"].'_coef" >'.numberByLang($prod['COEF']).'</span>') ,
					//$prod['GROSSISTE_NOM'],
					array('html' => $locked_html, 'txt' => $locked_txt),
				);
			 

			$action = '';
			
			$action .= '<a href="#" onclick = "saveModifsVenteAv(this); return false;" id = "'.$prod["ID_PRODUCT"].'_save"> 
						<img border="0" alt="' . ("Enregistrer modifications sur la ligne") . '" 
						align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/save.gif"></a>';

			$line[] = $action;			
			$input = '<input id="pid_' . $prod["ID_PRODUCT"] . '" type="checkbox" onchange="selectOneSession(this)"/>';
			$line[] = $input;
			$content[] = $line;
		}
		
		return $content;
	}
	
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @return boolean
	 */
	public function delete($id1 ) {
		// Suppression des logs
		$query = "DELETE FROM sfk_vendor
					WHERE VENDOR_ID = " . $id1;
		dbQuery($query);
		
		return true;
	}
	
	
	protected function _setSqlQuery(){
		
		//REQUETE DE BASE 
		$select =  "SELECT *
					FROM `sfk_produit_grossiste` as pg INNER JOIN sfk_grossiste as g ON pg.GROSSISTE_ID = g.GROSSISTE_ID INNER JOIN sfk_prix_grossiste as prx ON prx.ID_PRODUCT = pg.ID_PRODUCT ";
		
		
		////////////////////////////// SEARCH FILTER CATEGORY ////////////////////////////////////////////// 					
       	if ( isset($this->filtercat ) && ($this->filtercat != "all") && ($this->filtercat != "")) {
        	if ($where == "")	$where = " WHERE ";
            else 				$where .= " AND ";
			
            $where .= " pg.CATEGORY = '". $this->filtercat ."' ";
        }
		
		
		////////////////////////////// SEARCH NAME PRODUCT ///////////////////////////////////////////////// 					
       	if ( isset($this->nom ) && ($this->nom != "all") && ($this->nom != "")) {
        	if ($where == "")	$where = " WHERE ";
            else 				$where .= " AND ";
			
            $where .= " MATCH(NAME_PRODUCT, DESCRIPTION, DESCRIPTION_SHORT) AGAINST ('". $this->nom ."')";
        }

		///////////////////////////// PRICE MIN AND MAX ////////////////////////////////////////////////////
		if ( isset($this->pricemin) && ($this->pricemin != '')) {
    		if ($where == "") 	$where = " WHERE ";
			else 				$where .= " AND ";
			
			$where .= " CAST(PRICE_PRODUCT AS DECIMAL(10,5)) >= '".str_replace(",", ".", $this->pricemin)."' ";
		}
		
		if ( isset($this->pricemax) && ($this->pricemax != '')) {
            if ($where == "") 	$where = " WHERE ";
            else 				$where .= " AND ";
            
            $where .= " CAST(PRICE_PRODUCT AS DECIMAL(10,5)) <= '".str_replace(",", ".", $this->pricemax)."' ";
           
        }
        
		///////////////////////////// WEIGHT MIN AND MAX ////////////////////////////////////////////////////
		if ( isset($this->weightmin) && ($this->weightmin != '')) {
    		if ($where == "") 	$where = " WHERE ";
			else 				$where .= " AND ";
			
			$where .= " CAST(WEIGHT AS DECIMAL(10,5)) >= '".$this->weightmin."' ";
		}
		
		if ( isset($this->weightmax) && ($this->weightmax != '')) {
            if ($where == "") 	$where = " WHERE ";
            else 				$where .= " AND ";
            
            $where .= " CAST(WEIGHT AS DECIMAL(10,5)) <= '".$this->weightmax."' ";
           
        }
        
		/////////////////////////////// QUANTITY MIN       ////////////////////////////////////////////////
		
		if ( isset($this->quantitymin) && ($this->quantitymin != '')) {
            if ($where == "") 	$where = " WHERE ";
            else 				$where .= " AND ";
            
            $where .= " pg.QUANTITY >= '".$this->quantitymin."' ";
           
        }

		
		
		/////////////////////////////// FILTER ID GROSISTE ///////////////////////////////////////////////
		
		if ( isset($this->idgross) && ($this->idgross != 'all') && ($this->idgross != '')) {
			if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
			$where .= " pg.GROSSISTE_ID = '". $this->idgross ."' ";
		} 
		
		
		//////////////////////////////// FILTER PRODUCT ADDED OR NOT /////////////////////////////////////
		if ( isset($this->added) && ($this->added != "all") && ($this->added != '')) {
			if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
			$where .= " pg.AJOUTE = '". $this->added ."' ";
		} 
		
		
		///////////////////////////// FILTER PRODUIT EN VENTE OU PAS /////////////////////////////////////
		if ( isset($this->selling) && ($this->selling != "all") && ($this->selling != '') ){
			if ($where == "")	$where = " WHERE ";
       	 	else 				$where .= " AND ";
       	 	
       	 	if($this->selling == "1"){
				       	 	
       	 		$where .= " pg.EAN IN (SELECT id_produit FROM  sfk_product_ecommercant_stock_prix WHERE VENDOR_ID = 24) ";
			}
			elseif($this->selling == "0"){
				$where .= " pg.EAN NOT IN (SELECT id_produit FROM  sfk_product_ecommercant_stock_prix WHERE VENDOR_ID = 24) ";
			}
		}
		
				
		//////////////////////////////////////// COEFF MIN ////////////////////////////////////////////////
		
		if ( isset($this->coefmax) && ($this->coefmax != '')) {
            if ($where == "") 	$where = " WHERE ";
            else 				$where .= " AND ";
            
            $where .= " prx.COEF <= ".(float)str_replace(",", ".", $this->coefmax)." ";
           
        }
		
		
		//////////////////////////////////////// MARGE_NET MIN ////////////////////////////////////////////////
		
		if ( isset($this->margemin) && ($this->margemin != '')) {
            if ($where == "") 	$where = " WHERE ";
            else 				$where .= " AND ";
            
            $where .= " pg.MARGE_NET >= ".(float)str_replace(",", ".", $this->margemin)."";
           
        }
		
		///////////////////////////////////////// DEFAULT ////////////////////////////////////////////////
		
		if ($where == "")	$where = " WHERE ";
        else 				$where .= " AND ";
			
        $where .= " pg.ACTIVE = 1 ";
		

        $order =  " ORDER BY CATEGORY  "; //LIMITE est de 18 114 ??
        $this->_sql_query = $select . $where . $order;       
		
		//var_dump($this -> _sql_query);

    }
    
	
	public function _getSqlQuery()
	{
		return $this->_sql_query;
	}
    /**
	 * lorsqu'une demande de rafraichissement est demandée les paramétres sont retournés
	 */
	public function update($post) {
		//var_dump($post);
		
		$this->__construct( $post['filtercat'], $post['nom'], $post['pricemin'], $post['pricemax'], $post['weightmin'], 
							$post['weightmax'], $post['quantitymin'], $post['idgross'], $post['added'], $post['selling'], 
							$post['coefmax'], $post['margemin']);
    }
  
}
?>