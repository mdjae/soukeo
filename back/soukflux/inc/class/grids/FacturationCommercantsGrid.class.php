<?php 
/**
 * Class pour grid Grossiste
 */
class FacturationCommercantsGrid extends Grid
{
	
	protected $numvendor;
	protected $commfilter;
	protected $litigefilter;
	protected $datefilter;
	
	
	public function __construct( $numvendor="", $commfilter="", $datefilter ="next")
	{	
		$this->_name = 'facturationcommercants';
		
		$this->numvendor 		= $numvendor ;
		$this->commfilter 		= $commfilter ;
		$this->datefilter 		= $datefilter;

		$this->_setSqlQuery();
		
		$js = array("$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});");
		$this->setJavascript($js);
		
		$this->_init('facturationcommercants',$this->_makeHeaders(),$this->_makeContent());
		

		$this->setTitle("NoBg","grids_facturationcommercants",htmlentities(("Facturation des Commerçants")));
        
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(""),			            "sortable" => false, "excelable" => false , "width" => '6%'),
				array("name" => htmlentities("Commercant"),	            "sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Nb Commandes"),           "sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Total prix articles TTC"),"sortable" => true,  "excelable" => true, 	"align" => 'center', "total" => true, "format" => "money"),
				array("name" => htmlentities("Total Commission HT"),	"sortable" => true,  "excelable" => true, 	"align" => 'center', "total" => false),
				array("name" => htmlentities("Total Frais Banq HT"),	"sortable" => true,  "excelable" => true, 	"align" => 'center', "total" => true, "format" => "money"),
				array("name" => htmlentities("Total Avahis.com TTC"),	"sortable" => true,  "excelable" => true, 	"align" => 'center', "total" => true, "format" => "money"),
				array("name" => htmlentities("Total à verser TTC"),	    "sortable" => true,  "excelable" => true, 	"align" => 'center', "total" => true, "format" => "money"),
				array("name" => htmlentities("Dernière facture"),	    "sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Actions"),	            "sortable" => false, "excelable" => false,	"align" => 'center'),
				array("name" => htmlentities("Infos"),                  "sortable" => false, "excelable" => false,  "align" => 'center'),
				array("name" => htmlentities("Livraison"),                  "sortable" => false, "excelable" => false,  "align" => 'center'),
				array("name" => htmlentities(""), 			            "sortable" => false, "excelable" => false,	"align" => 'center', 	"width" => '1%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
        
        $livraisonTypeManager = new ManagerLivraisonType();
        
        $alert_low_comm = SystemParams::getParam("marketplace*alertcommabo1");
        
		foreach($res as $com)
		{
		    $alert_taux_comm = false;
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$com['vendor_id'].' AND commented_object_table = "sfk_vendor"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			
		    if($livraison_type = $livraisonTypeManager -> getById(array("type_id" => $com['livraison_type_id'])) ){
		        
		        $livraison_type_out = $livraison_type -> getLabel() ;
		    }else{
		        
		        $livraison_type_out = "Non défini";
		    }	
			
			$sql = "SELECT 	ROUND(SUM(soi.base_row_total_incl_tax),2) as total, ROUND(SUM((soi.base_row_total_incl_tax * c.COMM)),2) as montant_commission, ROUND(SUM((soi.base_row_total_incl_tax * 0.005 )),2) as frais_banquaires 
								FROM skf_orders so
								INNER JOIN skf_order_items soi ON so.order_id = soi.order_id 
								INNER JOIN skf_po spo ON spo.order_id = so.order_id AND spo.vendor_id = soi.udropship_vendor
								INNER JOIN sfk_vendor v ON v.VENDOR_ID = soi.udropship_vendor 
								LEFT JOIN sfk_categorie c ON c.cat_id = soi.category_id
								WHERE soi.litiges <> 1
								AND soi.udropship_vendor = '".$com['vendor_id']."'
								AND so.order_state = 'complete'";
			
			if ( isset($this->datefilter) && ($this->datefilter == "next") ){
				$date = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
            	$date = $date['MIN(date_value)']; 
            	
				$sql .= " AND spo.date_created < ('$date' - INTERVAL 15 DAY) AND soi.payment = 0";
				$showChildButton = "<input type='button' class='closed btn' value='+' id='vendor_".$com['vendor_id']."_".$com['vendor_id']."'  onclick='showOrderVendorInfo(this, \"next\")'  />";
			}else{
				$showChildButton = "<input type='button' class='closed btn' value='+' id='vendor_".$com['vendor_id']."_".$com['vendor_id']."'  onclick='showOrderVendorInfo(this)'  />";
			}
			
			$tmp = dbQueryAll($sql);
			$tmp = $tmp[0];
			$total = $tmp['total'];
			$total_mc = $tmp['montant_commission'];
			$total_frais_b = $tmp['frais_banquaires'];
			
			$sql = "SELECT   c.COMM 
                                FROM skf_orders so
                                INNER JOIN skf_order_items soi ON so.order_id = soi.order_id 
                                INNER JOIN skf_po spo ON spo.order_id = so.order_id AND spo.vendor_id = soi.udropship_vendor
                                INNER JOIN sfk_vendor v ON v.VENDOR_ID = soi.udropship_vendor 
                                LEFT JOIN sfk_categorie c ON c.cat_id = soi.category_id
                                WHERE soi.litiges <> 1
                                AND soi.udropship_vendor = '".$com['vendor_id']."'
                                AND so.order_state = 'complete' AND c.COMM IS NULL ";
            if ( isset($this->datefilter) && ($this->datefilter == "next") ){
                $sql .= " AND spo.date_created < ('$date' - INTERVAL 15 DAY) AND soi.payment = 0";
            }
            $tmp2 = dbQueryAll($sql);
            $alertComm = "";
            if(!empty($tmp2)){
               $alertComm = "<strong style='color:red'>Taux de commissions à remplir </strong>";
            }                    
			$state = "";
			
			$vendorManager = new ManagerVendor();
			$vendor_obj = $vendorManager -> getBy(array("vendor_id" => $com['VENDOR_ID']));
			
			if($vendor_obj->getAbonnement() == "1" && (float) $total_mc < $alert_low_comm){
			    $alert_taux_comm = true;     
			    $total_mc_html =  "<strong style='color:red'>".$total_mc." € - Abonnement gratuit</strong> " ;
			}else{
			    $total_mc_html =  $total_mc . " €" ;
			}
			
			$line =
				array(
					$showChildButton,
					"<a href='/app.php/clients/client?type=vendor&id=".$com['entity_id']."' target='blank'>".htmlentities($com['VENDOR_NOM'])."</a>",
					$com['c'],
					$total,
					$total_mc_html,
					$total_frais_b,
					$total_mc * 1.085 + $total_frais_b * 1.085,
					$total - ($total_mc * 1.085 + $total_frais_b * 1.085), 
				);
			
			$sql = "SELECT fc.facture_id, facture_code, f.date_created, fc.date_limit
					FROM  `skf_factures` f
					INNER JOIN skf_factures_com fc ON fc.facture_id = f.facture_id
					WHERE fc.vendor_id = ".$com["VENDOR_ID"]."
					AND fc.date_limit = (SELECT (MIN( date_value ) - INTERVAL 15 DAY ) 
										 FROM skf_calendar
										 WHERE date_type_id =1
										 AND date_value > (NOW() - INTERVAL 5 DAY))
					AND f.date_created = (SELECT MAX( date_created ) FROM skf_factures sf 
											INNER JOIN skf_factures_com sfc ON sfc.facture_id = sf.facture_id WHERE sfc.vendor_id = ".$com["VENDOR_ID"].")";	
			$fact = dbQueryAll($sql);
			
			$factureGenerated = false;
			
			if(!empty($fact)){
				$fact = $fact[0];
				$facture_code = $fact['facture_code'];
				$facture_id   = $fact['facture_id'];
				$facture_date_limit = $fact['date_limit'];
				$state .= "Jusqu'au ".smartDateNoTime($fact['date_limit']). " - crée le ".smartDateNoTime($fact['date_created'])."<br />";
				if($root_path){
					$state .= "<a href='". $root_path . "app.php/commercants/showLastFacture?id=".$fact['facture_id']."' >".$fact['facture_code']."</a>";	
				}else{
					$state .= "<a href='". $root_path . "showLastFacture?id=".$fact['facture_id']."' >".$fact['facture_code']."</a>";
				}
                
                $factureGenerated = true;
				
			}else{
				$state .= " - ";
			}		
			
			$info = "";
            $infoLivraison = $livraison_type_out;
			$action = "";
			$line [] = $state;
			
			if ( isset($this->datefilter) && ($this->datefilter == "next")){
				
				if(!$factureGenerated){
				    $arr = array('url'          => '/commercants/facturation/popup_factu/index.php', 
                             'vendor_id'    => $com['vendor_id']);           
                    $data = json_encode($arr, JSON_HEX_APOS);
                    $popup = "showDialogFactureCommercant('".$data."')";
				    $action .= ' <a class="btn" onclick='.$popup.' href="#">Créer Facture <i class="fa fa-file icon-black" style="padding-right: 0px" ></i></a>';    
				}else{
				    $action .= ' <a class="btn" onclick="alert(&#39; Facture déja générée pour la période en cours! &#39;)" href="#" >Créer Facture <i class="fa fa-file icon-black" style="padding-right: 0px" disabled></i></a>';
				}
				
			}
            
            
            
			$action .= "<a class='closed btn' href='#' id='comment_vendor_".$com['VENDOR_ID']."' onclick='showComments(this); return false;'>
					 <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i>$countComment</a>";
					 
					 
			$line[] = $action;
			$line[] = $alertComm;
			$line[] = $infoLivraison;
			$line[] = '<input id="vendor_' . $com["VENDOR_ID"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT po.vendor_id, v.VENDOR_NOM, v.VENDOR_ID, v.entity_id, v.livraison_type_id, count(*) as c, po.date_created as date_created
					FROM   
					skf_po  po INNER JOIN skf_orders so ON so.order_id = po.order_id   
					INNER JOIN sfk_vendor v ON v.VENDOR_ID = po.vendor_id 
					";
		$where = " WHERE po.vendor_id <> '24' AND so.order_state = 'complete'";
        $order =  " "; 
		
		if ( isset($this->numvendor ) && ($this->numvendor  != "all") && ($this->numvendor != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " po.vendor_id = '$this->numvendor' ";
        }
		
		if ( isset($this->commfilter ) && ($this->commfilter != "all") && ($this->commfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " po.vendor_id = '$this->commfilter' ";
        }
        
        if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
            
            $date = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
            $date = $date['MIN(date_value)']; 
           
			$where .= " po.date_created < ('$date' - INTERVAL 15 DAY) and po.payment <> 'complete' ";
        }

		$order =  "GROUP BY po.vendor_id";
		
        $this->_sql_query = $select . $where . $order;
		
		//var_dump($this->_sql_query);

    }
    
    
    public function update($post)
    {
        $this->__construct($post['numvendor'], $post['commfilter'], $post['datefilter']);
    }
	
}
?>