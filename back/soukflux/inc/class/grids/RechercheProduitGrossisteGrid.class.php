<?php 
/**
 * Class pour grid Ecommercant
 */
class RechercheProduitGrossisteGrid extends Grid {
	public function __construct($sku='', $ref_grossiste='', $nom='')
	{	
		$this->_name 		 = 'rechercheproduitgrossiste';
		$this->sku 			 = $sku ;
		$this->ref_grossiste = $ref_grossiste ;
		$this->nom 			 = $nom ;
		
		$this->_setSqlQuery();

		$this->_init('rechercheproduitgrossiste',$this->_makeHeaders(),$this->_makeContent());
		
		
		$this->setTitle("NoBg","grids_rechercheproduitgrossiste",htmlentities(("Recherche produit grossiste ")));
		
	}

	public function _makeHeaders() {
		$headers =
			array(
				array("name" => htmlentities(("Grossiste")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '8%'					),
				array("name" => 			( "Nom produit"),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '15%'					),
				array("name" => 			( "Ref grossiste"),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '8%'					),
				array("name" => htmlentities(("Prix Achat")),			"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '4%'),
				array("name" => htmlentities(("Qté Grossiste")),		"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '4%'),
				array("name" => htmlentities(("Poids")),				"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '4%'),
				array("name" => htmlentities(("Prix vente Avahis")),	"sortable" => true,  "excelable" => true, "align" => 'center', "width" => '4%'					),
			);
		return $headers;
	}

	public function _makeContent() {
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);

		$content = array();
		$javascript = array();
		foreach($res as $prod)
		{

			
			$line =
				array(
					$prod['GROSSISTE_NOM'],
					htmlentities($prod['NAME_PRODUCT']),
					$prod['REF_GROSSISTE'],
					$prod['PRICE_PRODUCT'], 
					$prod['QUANTITY'],
					numberByLang($prod['WEIGHT']),
					(float)$prod['PRIX_FINAL'], 
					
				);
			 

			$content[] = $line;
		}
		
		return $content;
	}
	
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1
	 * @return boolean
	 */
	public function delete($id1 ) {
		// Suppression des logs
		$query = "DELETE FROM sfk_vendor
					WHERE VENDOR_ID = " . $id1;
		dbQuery($query);
		
		return true;
	}
	
	
	protected function _setSqlQuery(){
		
		//REQUETE DE BASE 
		$select =  "SELECT *
					FROM `sfk_produit_grossiste` as pg INNER JOIN sfk_grossiste as g ON pg.GROSSISTE_ID = g.GROSSISTE_ID INNER JOIN sfk_prix_grossiste as prx ON prx.ID_PRODUCT = pg.ID_PRODUCT ";
		
		$where .= "WHERE pg.ACTIVE = 1 ";
		//////////////////////////////////////// SEARCH SKU ///////////////////////////////////////////////// 					
       	if ( isset($this->sku ) && ($this->sku != "") ) {
        	if ($where == "")	$where = " WHERE ";
            else 				$where .= " AND ";
			
            $where .= " EAN  = '". $this->sku ."'";
        }

		////////////////////////////// SEARCH REF_GROSSISTE ///////////////////////////////////////////////// 					
       	if ( isset($this->ref_grossiste ) && ($this->ref_grossiste != "") ) {
        	if ($where == "")	$where = " WHERE ";
            else 				$where .= " AND ";
			
            $where .= " REF_GROSSISTE = '". $this->ref_grossiste ."'";
        }
		
		////////////////////////////// SEARCH NAME PRODUCT ///////////////////////////////////////////////// 					
       	if ( isset($this->nom ) && ($this->nom != "all") && ($this->nom != "")) {
        	if ($where == "")	$where = " WHERE ";
            else 				$where .= " AND ";
			
            $where .= " NAME_PRODUCT LIKE '%". $this->nom ."%'";
        }
		
		if($this->nom == "" && $this->ref_grossiste == "" && $this->sku == ""){
			$where .= " AND 0";
		}

        $order =  " LIMIT 20  "; //LIMITE est de 18 114 ??
        $this->_sql_query = $select . $where . $order;       
		
		//var_dump($this -> _sql_query);

    }
    
	
	public function _getSqlQuery()
	{
		return $this->_sql_query;
	}
    /**
	 * lorsqu'une demande de rafraichissement est demandée les paramétres sont retournés
	 */
	public function update($post) {
	
		$this->__construct( $post['sku'], $post['ref_grossiste'], $post['nom']  );
    }
  
}
?>