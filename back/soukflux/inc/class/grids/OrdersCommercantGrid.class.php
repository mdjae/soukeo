<?php 
/**
 * Class pour grid Grossiste
 */
class OrdersCommercantGrid extends Grid
{
	
	protected $numfilter;
	protected $statusfilter;
	protected $litigefilter;
	protected $datefilter;
	protected $commfilter;
	
	public function __construct( $numfilter="", $statusfilter="", $litigefilter="", $datefilter ="all", $commfilter)
	{
	    $customerManager = new ManagerCustomer();	
		$this->_name = 'commande_commercant';
		$this->statusfilter 	= $statusfilter ;
		$this->numfilter 		= $numfilter ;
		$this->litigefilter 	= $litigefilter ;
		$this->datefilter 		= $datefilter;
		$this->commfilter 	= $commfilter;

		$this->_setSqlQuery();
		
		$js = array("$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});");
		$this->setJavascript($js);
		
		$this->_init('commande_commercant',$this->_makeHeaders(),$this->_makeContent());
		

		$this->setTitle("NoBg","grids_order",htmlentities(("Listing Commandes des Commerçants")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(""),			"sortable" => false, "excelable" => false , "width" => '6%'),
				array("name" => htmlentities("Num Cmd"),	"sortable" => true,  "excelable" => true, 	"align" => 'center'),
				array("name" => htmlentities("Date"),		"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'date'),
				array("name" => htmlentities("Vendeur"),		"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Client"),		"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Total"),		"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'money' ),
				array("name" => htmlentities("Poids"),		"sortable" => true,  "excelable" => true, 	"align" => 'center', 	"format"=>'number'),
				array("name" => htmlentities("Etat (state) Commande"),     "sortable" => true,  "excelable" => false,  "align" => 'center'),
				array("name" => htmlentities("Statut (status) Commande"),     "sortable" => true,  "excelable" => false,  "align" => 'center'),
				array("name" => htmlentities("Statut PO"),		"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Infos/Etat"),	"sortable" => true,  "excelable" => false, 	"align" => 'center'),
				array("name" => htmlentities("Facturé"),	"sortable" => true,  "excelable" => false, 	"align" => 'center'),		
				array("name" => htmlentities("Payé par Avahis"),	"sortable" => true,  "excelable" => false, 	"align" => 'center'),		
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center'),
				array("name" => htmlentities(""), 			"sortable" => false, "excelable" => false,	"align" => 'center', 	"width" => '1%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
        
        $customerManager = new ManagerCustomer();
        $vendorManager = new ManagerVendor();
        
		$content = array();
		$javascript = array();
		foreach($res as $order)
		{
			$poObj = new BusinessPo($order['po_id']);
			
			$countComment = count(dbQueryAll('SELECT * FROM skf_comments WHERE commented_object_id = '.$order['po_id'].' AND commented_object_table = "skf_po"'));
			
			if($countComment > 0){
				$countComment = "<small> ".$countComment."</small>";
			}
			else{
				$countComment = "<small> </small>";
			}
			
			$items = dbQueryAll('SELECT order_id_grossiste FROM  `skf_order_items`  WHERE order_id ="'.$order['order_id'].'" AND udropship_vendor <> 24');
			
			$count = count($items);
			

			if($order['order_customer_id'] == "0"){
                if($order['recipient_email'] != ""){
                    if($cust = $customerManager -> getBy(array("email" => $order['recipient_email'])))
                        $nomCli = "<a href='/app.php/clients/client?type=customer&id=".trim($cust -> getEntity_id())."'>".htmlentities($cust -> getName())."</a>";
                    else 
                        $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order['recipient_name']) .'</em>';  
                }else{
                    $nomCli =   '<span class="glyphicon glyphicon-warning-sign"></span><em>'.htmlentities($order['recipient_name']) .'</em>';    
                }
                
            }else{
				$cust = $customerManager -> getBy(array("customer_id" => $order['order_customer_id']));
				/*$nomCli = '<a href="/app.php/clients/listing_acheteurs?id='.$cust -> getEntity_id().'" target="_blank">'.
                                    htmlentities($cust -> getName()).'
                                </a>';*/
                $nomCli = "<a href='/app.php/clients/client?type=customer&id=".trim($cust -> getEntity_id())."' >".htmlentities($cust -> getName())."</a>";
			} 
			
			$addr = new BusinessAddress($order['shipping_address_id']);
			$htmladdr = $addr -> getStreet1()." ".$addr -> getStreet2()." ". $addr -> getPostcode()." ". $addr -> getCity()." ". $addr -> getRegion_code();
			
			$vend = $vendorManager -> getBy(array("vendor_id" => $order['vendor_id']));
            
            $link_vendor = "<a href='/app.php/clients/client?type=vendor&id=".trim($vend -> getEntity_id())."' >".htmlentities($order['VENDOR_NOM'])."</a>";
            
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='item_".$order['order_id']."_".$order['vendor_id']."'  onclick='showChild(this)'  />",
					$order['increment_id'],
					$order['date_created'],
					$link_vendor,
					$nomCli,
					$order['base_total_value'] + $order['base_tax_amount'],
					$order['total_weight'],
					getLabelStatus($order['order_state']),
					getLabelStatus($order['order_status']),
					getPoStateShipping($order['udropship_status'])
				);
				
			$state = "";
			$info = "";
			$action = "";
			$info_payment = "";
			$info_facture = "";
			
			$address = new BusinessAddress($order['shipping_address_id']);
				
			
			if($address -> getCountry() !=  "RE"){
				$info .= '<span id="addr_'.$order['order_id'].'_etat" class="etat_order label label-important"> '.$this->countryName($address -> getCountry()).' </span>';
			}else{
				$info .= '<span id="addr_'.$order['order_id'].'_etat" class="etat_order label label-important"></span>';
			}
	
			if($order['litiges']){
				$info .= '<span id="po_'.$order['po_id'].'_etat" class="etat_order label label-warning">litige</span>';
			}else{
				$info .= '<span id="po_'.$order['po_id'].'_etat" class="etat_order label label-warning"></span>';
			}

			$line [] = $info;
			
			
			$num = "po_".$order['po_id'];
			
			if($order['litiges']){
				$action .= '<a id="litigeit_'.$order['po_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un litige ") . '" 
							align="absmiddle" title="Annuler litige"  class="iconAction" src="' . $skin_path . 'images/picto/check.gif"></a>';
			}else{			
				$action .= '<a id="litigeit_'.$order['po_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un litige ") . '" 
							align="absmiddle" title="Daclarer litige"  class="iconAction" src="' . $skin_path . 'images/picto/denied.gif"></a>';
			}
			
			if($order['payment'] =="complete" || $order['payment'] =="partiel"){
				$action .= '<a id="payit_'.$order['po_id'].'" href="#" onclick="payIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un payement") . '" 
							align="absmiddle" title="Annuler payment"  class="iconAction" src="' . $skin_path . 'images/picto/undo.gif"></a>';
			}else{			
				$action .= '<a id="payit_'.$order['po_id'].'" href="#" onclick="payIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un payement") . '" 
							align="absmiddle" title="Daclarer payment"  class="iconAction" src="' . $skin_path . 'images/picto/dollar.gif"></a>';
			}
			
			if($poObj -> checkFacture()){
				$info_facture .= '<span id="po_'.$order['po_id'].'_facture" class="etat_order label label-inverse">'.$poObj -> checkFacture().'</span>';
			}else{
				$info_facture .= '<span id="po_'.$order['po_id'].'_facture" class="etat_order label label-inverse"></span>';
			}
			
			if($order['payment'] == "complete"){
				$info_payment .= '<span id="po_'.$order['po_id'].'_payment" class="etat_order label label-success">Payé</span>';
			}
			elseif($order['payment'] == "partiel"){
				$info_payment .= '<span id="po_'.$order['po_id'].'_payment" class="etat_order label label-warning">Partiel</span>';
			}
			else{
				$info_payment .= '<span id="po_'.$order['po_id'].'_payment" class="etat_order label label-success"></span>';	
			}
			$line[] = $info_facture;
			$line[] = $info_payment;
			$action .= "<a class='closed btn' href='#' id='comment_po_".$order['po_id']."' onclick='showComments(this); return false;'>
					 <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i>$countComment</a>";
					 
			$line[] = $action;
			
			$line[] = '<input id="po_' . $order["po_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		return $content;
	} 
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT so.grand_total, po.vendor_id, v.VENDOR_NOM, po.increment_id, po.base_total_value, po.base_tax_amount, so.order_status, so.order_state, po.total_weight, po.vendor_id, po.order_id, po.litiges, so.increment_id as idcmd,
					po.date_created, po.po_id, so.base_shipping_amount, so.shipping_address_id, so.order_customer_id, so.recipient_name, so.shipping_address_id, so.recipient_phone, po.payment, so.recipient_email, po.udropship_status    
					
					FROM   
					skf_po  po INNER JOIN skf_orders so ON so.order_id = po.order_id   
					INNER JOIN sfk_vendor v ON v.VENDOR_ID = po.vendor_id 
					";
		$where = " WHERE po.vendor_id <> '24' ";
        $order =  " "; 
		
		if ( isset($this->statusfilter ) && ($this->statusfilter  != "all") && ($this->statusfilter  != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.order_state = '$this->statusfilter' ";
        }
		
		if ( isset($this->numfilter ) && ($this->numfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " so.increment_id = '$this->numfilter' ";
        }
        
        if ( isset($this->litigefilter ) && ($this->litigefilter  != "")  && ($this->litigefilter  != "all"))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " po.litiges = $this->litigefilter ";
        }
        
        if ( isset($this->datefilter) && ($this->datefilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " so.date_created > (NOW() - INTERVAL ".$this->datefilter." DAY) ";
        }
		
		if ( isset($this->commfilter ) && ($this->commfilter  != "")  && ($this->commfilter  != "all"))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " po.vendor_id = $this->commfilter ";
        }
		
		$order =  "   ORDER BY po.date_created desc";
		
        $this->_sql_query = $select . $where . $order; 

    }
    
    
    public function update($post)
    {
        $this->__construct($post['numfilter'], $post['statusfilter'], $post['litigefilter'], $post['datefilter'], $post['commfilter']);
    }
	
	
	public function countryName($abr)
	{
		switch ($abr) {
			case 'FR':
				return 'FR';
			break;
			
			case 'MU':
				return 'MAURICE';
			break;
			
			case 'YT':
				return 'MAYOTTE';
			break;
			
			default:
				return $abr;
			break;
		}
	}
}
?>