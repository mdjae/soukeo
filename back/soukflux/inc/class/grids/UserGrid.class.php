<?php 
class UserGrid extends Grid
{
	public function __construct($sql_query)
	{	
		$this->_name = 'user';
		$this->_sql_query = $sql_query;
		$this->_init('user',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_user",("Liste des utilisateurs"));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				//array("name" => ("Utilisateur"),			"sortable" => true, "excelable" => true),
				array("name" => 			("Nom"),				"sortable" 	=> true, "excelable" => true, "align" => "center"),
				array("name" =>htmlentities(("Prénom")),			"sortable" 	=> true, "excelable" => true, "align" => "center","width" => '15%'),
				array("name" => 			("Email"),				"sortable"	=> true, "excelable" => true, "align" => "center"),
				array("name" => 			("Login"),				"sortable"	=> true, "excelable" => true, "align" => "center","width" => '6%'),
				array("name" => 			("Groupe(s)"),			"sortable"	=> true, "excelable" => true, "align" => "center","width" => '6%'),
				array("name" => '<img border="0" alt="'.htmlentities(("Profil autorisé ou bloqué"))  .'"  class="icon" src="/skins/common/images/picto/unlock.gif">', "excel_name" => ("Profil autorisé ou bloqué"),"sortable" => true, "excelable" => true, "align"=>'center',"width" => '6%'),
				array("name" => '<img border="0" alt="'. htmlentities(("Profil fermé ?") ) .'" class="icon" src="/skins/common/images/picto/denied.gif">', "excel_name" => ("Profil fermé ?"),"sortable" => true, "excelable" => true, "align"=>'center',"width" => '6%'),
				array("name" => 			("Actions"),			"sortable" 	=> false, "excelable" => false, "align" => "center", "width" => '10%'),
				array("name" => "", 								"sortable"=> false, "excelable" => false, "align" => "center", "width" => '2%')
			);
		return $headers;
	}


	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;

		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query, MYSQL_ASSOC);

		$content = array();
		foreach($res as $user)
		{
			if ($user["locked"] == '1')
			{
				$locked_html = '<img border="0" alt="'. ("Profil fermé")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/denied.gif">';
				$locked_txt = ("Oui");
			}
			else
			{
				$locked_html = '<img border="0" alt="'. ("Profil actif")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/check.gif">';
				$locked_txt = ("Non");
			}

			if ($user["lockauth"] >= SystemParams::getParam('security*pwd_error') )
			{
				$lockauth_html = '<img border="0" alt="'. ("Profil bloqué")  .'" align="absmiddle" class="iconCircleWarn" src="'. $skin_path .'images/picto/lock.gif">';
				$lockauth_txt = ("Oui");
			}
			else
			{
				$lockauth_html = '<img border="0" alt="'. ("Profil autorisé")  .'" align="absmiddle" class="iconCircleCheck" src="'. $skin_path .'images/picto/unlock.gif">';
				$lockauth_txt = ("Non");
			}
			
			$hash = md5($user["usercode"] . time());
			$line =
				array(
					//$user['usercode'],
					$user['lastname'],
					$user['firstname'],
					$user['email'],
					//$user['erm_code'],
					$user['login'],
					$user['name'],
					/*array('sort' => $user["contract_date"], 'html' => smartdateNoTime($user["contract_date"]), 'txt' => writeNormalDate($user["contract_date"])),
					$year,
					array('html' => $extres_html, 'txt' => $extres_txt),*/
          array('html' => $lockauth_html, 'txt' => $lockauth_txt),
          array('html' => $locked_html, 'txt' => $locked_txt)
				);

			$action = '';
			if (SystemAccess::canAccess("admin.users..addmod"))
			{
				$action .= popupLink('/o_admin/users/popup_add/index.php?user_id=' . $user["user_id"],("Mettre à jour l'utilisateur (actif?, infos,..)"),650,600,"jdiag") . '<img border="0" alt="' . ("Mettre à jour l'utilisateur (actif?, infos,..)") . '" align="absmiddle" class="iconAction" src="' . $skin_path . 'images/picto/edit.gif"></a>';
			
				// Si jamais factur�, on peut supprimer
				if ($user['amount'] == 0)
				{
					$action .= "<a href=\"#\" onClick=\"confirmDelete('" . addslashes(sprintf(("Etes-vous certain de vouloir supprimer l'utilisateur %s ?"),$user['firstname'] . " " . $user['lastname'])) . "', '" . $user['user_id'] . "', '" . $user['usercode'] . "', '" . $user['login'] . "', '" . $this->_name . "', '" . $root_path . "')\"> 
					<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Suppression d'un utilisateur") . "\" class=\"iconAction\" src=\"" . $skin_path . "images/picto/trash.gif\"></a></p>";	
				}
			}

			$line[] = $action;
			$line[] = '<input id="uid_' . $user["user_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}

		return $content;
	}
	
	/**
	 * R�alise la suppression d'une ligne en base
	 * @param $id1 user_id
	 * @param $id2 usercode
	 * @param $id3 login
	 * @return boolean
	 */
	public function delete($id1, $id2, $id3)
	{
		$query = "DELETE FROM c_team_users
					WHERE user_id = " . $id1;
		dbQuery($query);
		
		$query = "DELETE FROM c_group_users
					WHERE user_id = " . $id1;
		dbQuery($query);
		
		$query = "DELETE FROM c_box
					WHERE usercode = '" . $id2 . "'";
		dbQuery($query);
		
		$query = "DELETE FROM c_user_logged_in
					WHERE login = '" . $id3 . "'";
		dbQuery($query);
		
		$query = "DELETE FROM c_user
					WHERE user_id = " . $id1;
		dbQuery($query);
		
		return true;
	}
}
?>
