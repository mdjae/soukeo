<?php 
/**
 * Class pour grid Grossiste
 */
class TicketsGrid extends Grid
{
	protected $codefilter;
	protected $clientfilter;
	protected $priorityfilter;
    protected $statutfilter;
    protected $userassignedfilter;
    protected $typefilter;
    protected static $ticketManager;
	
	public function __construct($codefilter, $clientfilter, $priorityfilter, $statutfilter, $userassignedfilter, $typefilter)
	{
		$this->codefilter = $codefilter;
		$this->clientfilter  = $clientfilter ;
		$this->priorityfilter = $priorityfilter;
        $this->statutfilter = $statutfilter;
        $this->userassignedfilter = $userassignedfilter;
        $this->typefilter = $typefilter;
        
        self::$ticketManager = new ManagerTicket();
			
		$this->_name = 'tickets';
		
		$this->_setSqlQuery();
		$this->_init('tickets',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","tickets",htmlentities(("Liste de tous les tickets")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("")),			"sortable" => false, "excelable" => false, 	"align" => 'center' ),
				array("name" => htmlentities(("Code")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Statut")),     "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("Priorité")),     "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("Date creation")),    "sortable" => true,  "excelable" => true,   "align" => 'center', "format"=>'date' ),
				array("name" => htmlentities(("Client")),       "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("Type")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Titre"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Assigné à")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("Réponses")),    "sortable" => true,  "excelable" => true,   "align" => 'center' ),
				array("name" => htmlentities(("Crée par")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ),
				array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '6%')
			);
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		

		// Contenu de la grid
		$res = dbQueryAll($this->_sql_query);
        
		$content = array();
		$javascript = array();
		//var_dump($res);
		foreach($res as $t)
		{
		    $assigned_user=""; 
		    $comment=""; 
		    $creator ="";
            
		    $ticket = self::$ticketManager->getById(array("ticket_id" => $t['ticket_id']));
		       
			
			
            $ticket ->getAssigned_user() ? $assigned_user = $ticket -> getAssigned_user() ->getName() : $assigned_user = " - ";
            
            $ticket->getTicket_creator_user() ? $creator = $ticket->getTicket_creator_user()->getName() : $creator = "Client";

			$comment = "";
            if(count($ticket -> getHistory())>0) $style_red= "style='color:red'";
            else $style_red= "";
            $comment .= "<a class='closed btn' href='http://".$_SERVER['HTTP_HOST']."/app.php/ticket?id=".$ticket -> getTicket_id()."' id='comment_ticket_".$ticket -> getTicket_id()."' >
                     <i class='fa fa-comment icon-black' style='padding-right: 0px' ></i><small $style_red >".count($ticket -> getHistory())."</small></a>";
                      
			$line =
				array(
					"<input type='button' class='closed btn' value='+' id='".$t['ticket_id']."'  onclick='self.location.href=\"/app.php/ticket?id=".$t['ticket_id']."\"'  />
					",
					$ticket->getTicket_code(),
					getLabelStateTicket($ticket->getTicket_state_label()),
					getLabelPriorityTicket($ticket->getTicket_priority_label()),
					smartDate($ticket->getDate_created()),
					$ticket->getClient(),
					$ticket->getTicket_type(),
					$ticket->getTicket_title(),
					$assigned_user,
					$comment,
					$creator
				);

			$action = '';
			$num = "ticket_".$t['ticket_id'];
			
			$action .= "";
			$line[] = $action;
			
			
			$line[] = '<input id="cust_' . $t["ticket_id"] . '" type="checkbox"/>';
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT * FROM skf_tickets ";
		
		$where = "";
        
		
		if ( isset($this->codefilter ) && ($this->codefilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " ticket_code = ".trim($this->codefilter)." ";
        }

		if ( isset($this->clientfilter ) && ($this->clientfilter != ""))
        {
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " client_name LIKE '%".trim($this->clientfilter)."%'";
        }
        
        if ( isset($this->priorityfilter) && ($this->priorityfilter != "all")){
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " ticket_priority_id = ".$this->priorityfilter." " ;
        }

        if ( isset($this->statutfilter) && ($this->statutfilter != "all")){
            if ($where == "") $where = " WHERE ";
            else $where .= " AND ";
            $where .= " ticket_state_id = ".$this->statutfilter." " ;
        }
        		 
		if ( isset($this->userassignedfilter) && ($this->userassignedfilter != "all")){
        		
        	if ($where == "") $where = " WHERE ";
            else $where .= " AND ";	
        	$where .= " assigned_user_id = ".$this->userassignedfilter." ";
        }

        if ( isset($this->typefilter) && ($this->typefilter != "all")){
                
            if ($where == "") $where = " WHERE ";
            else $where .= " AND "; 
            $where .= " ticket_type_id = ".$this->typefilter." ";
        }
        		
		$order =  "   ORDER BY date_created DESC ";
		 
       	$this->_sql_query = $select . $where . $order;      

    }
    
    
    public function update($post)
    {
        $this->__construct($post['codefilter'], $post['clientfilter'], $post['priorityfilter'], $post['statutfilter'], $post['userassignedfilter'], $post['typefilter']);
    }
}
?>