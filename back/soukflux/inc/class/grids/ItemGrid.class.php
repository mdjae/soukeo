<?php 
/**
 * Class pour grid Grossiste
 */
class ItemGrid extends Grid
{
	protected $vendor_id;
	protected $idItem;
	
	public function __construct($idItem, $vendor_id, $interne = false )
	{	
		$this->_name = 'item';
		$this->idItem = $idItem;
		$this->vendor_id = $vendor_id;
		$this->_setSqlQuery();
		$this->_init('item',$this->_makeHeaders(),$this->_makeContent(), $interne);
		$this->setTitle("NoBg","grids_item",htmlentities(("Listing Produit PO")));
	}

	public function _makeHeaders()
	{
		$headers =
			array(
				array("name" => htmlentities(("SKU")),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities("name"),		"sortable" => true,  "excelable" => true, 	"align" => 'center' ),
				array("name" => htmlentities(("prix")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true	 , "format"=>'money' ,"width" => '8%'),
				array("name" => htmlentities(("qty")),	"sortable" => true,  "excelable" => true, 	"align" => 'center',"total" => true	 , "format"=>'number' ,"width" => '8%' ),
				array("name" => htmlentities(("Poids")),	"sortable" => true,  "excelable" => true, 	"align" => 'center' ,"total" => true , "format"=>'number' 	,"width" => '8%'),
				
			);
			
		if($this->vendor_id == "24"){
			
			$headers[] = array("name" => htmlentities("Grossiste"),	"sortable" => false, "excelable" => false,	"align" => 'center' ,"width" => '6%');
			$headers[] = array("name" => htmlentities("Ref Grossiste"),	"sortable" => false, "excelable" => false,	"align" => 'center' ,"width" => '8%');
		}
			$headers[] = array("name" => htmlentities("Etat"),	"sortable" => false, "excelable" => false,	"align" => 'center' ) ;
		$headers[] = array("name" => htmlentities("Actions"),	"sortable" => false, "excelable" => false,	"align" => 'center' ,"width" => '8%') ;
		
		$headers[] = array("name" => htmlentities(("")), 		"sortable" => false, "excelable" => false,	"align" => 'center', "width" => '2%');
		
		return $headers;
	}

	public function _makeContent()
	{
		global $root_path, $skin_path, $main_url;
		
		// COntenu de la grid
		$res = dbQueryAll($this->_sql_query);
		$factureManager = new ManagerFacture();
		$content = array();
		$javascript = array();
		//var_dump($this->_sql_query);
		foreach($res as $item)
		{
			$commande = "";
			$commande = dbQueryAll('SELECT order_id_grossiste, order_external_code FROM skf_orders_grossiste 
									WHERE order_id_grossiste = "'.$item['order_id_grossiste'].'"');
			$commande = $commande[0];
									 
			$line =
				array(
					htmlentities($item['sku']),
					htmlentities($item['name']),
					$item['base_row_total_incl_tax'],
					$item['qty_ordered'],
					$item['row_weight'],
				);
				
			$state = "";
			
			if($item['litiges']){
				$state.= '<span id="item_'.$item['item_id'].'_etat" class="etat_order label label-warning"> litige </span>';
			}else{
				$state.= '<span id="item_'.$item['item_id'].'_etat" class="etat_order label label-warning"></span>';
			}
			
			if($this -> vendor_id == "24"){
				$sql = "SELECT * FROM sfk_catalog_product cp 
						INNER JOIN sfk_assoc_product_grossiste apg ON apg.id_avahis = cp.id_produit 
						INNER JOIN sfk_produit_grossiste pg ON pg.ID_PRODUCT = apg.id_produit_grossiste  
						INNER JOIN sfk_grossiste g ON pg.GROSSISTE_ID = g.GROSSISTE_ID 
						WHERE cp.sku = '".$item['sku']."' ";
			
				$result = dbQueryAll($sql);

				$prod_gr = $result[0];	
				$line[] = $prod_gr['GROSSISTE_NOM'];
				$line[] = $prod_gr['REF_GROSSISTE'];
				
				if($commande['order_id_grossiste']){
					$state.= '<span id="item_'.$item['item_id'].'_cmd" class="etat_order label label-success"> commandé : 
								<a href="/app.php/grossistes/commande_grossiste?id='.$commande['order_external_code'].'" target="_blank">'.
									$commande['order_external_code'].'
								</a>
							  </span>';
				}else{
					$state.= '<span id="item_'.$item['item_id'].'_cmd" class="etat_order label label-success"></span>';
				}
			}else{
				if($item['facture_id']){
					if($facture 	= $factureManager -> getFactureById($item['facture_id'])){
					   $state.= '<span id="facture_'.$item['facture_id'].'" class="etat_facture label label-inverse">Facture : 
                        <a href="/app.php/commercants/historique_factures_com?id='.$facture -> getFacture_code().'" target="_blank">
                                    '.$facture -> getFacture_code().'
                                </a>
                        </span>';    
					}else{
					    $state.= '<strong style="color:red">Erreur Facture id : '.$item['facture_id'].' inexistant (supprimé) !</strong>';
					}
					
				}
                if($item['payment']){
                    $state.= '<span id="payment_'.$item['facture_id'].'" class="etat_payment label label-success">Payé</span>';
                }
			}	
			

			$line[] = $state;
			
			$action = '';
			$num = "item_".$item['item_id'];
			
			if($item['litiges']){
				$action .= '<a id="litigeit_'.$item['item_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 0);return false;"> 
							<img border="0" alt="' . ("Annuler un litige ") . '" 
							align="absmiddle" title="Annuler litige"  class="iconAction" src="' . $skin_path . 'images/picto/check.gif"></a>';
			}else{			
				$action .= '<a id="litigeit_'.$item['item_id'].'" href="#" onclick="litigeIt(\''.$num.'\', 1);return false;"> 
							<img border="0" alt="' . ("Declarer un litige ") . '" 
							align="absmiddle" title="Daclarer litige"  class="iconAction" src="' . $skin_path . 'images/picto/denied.gif"></a>';
			}
			$action .= '<span class="popover-markup"> 
								<a class="trigger" id="popover_'.$item['item_id'].'">
									<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Assigner commande" alt="Assigner commande">
								</a> 
							    <div class="head hide">
							    	<button type="button" id="close" class="close" onclick="$(&quot;#popover_'.$item['item_id'].'&quot;).popover(&quot;hide&quot;);">&times;</button>
							    	<br>
							    	<h5>Commande grossiste : <h5>
							    </div>
							    <div class="content hide">
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="N° Commande…" id="popoverinput_'.$item['item_id'].'">
							        </div>
							        <button type="submit" class="btn btn-primary btn-block" onClick="assignItemComGr(&quot;'.$item['item_id'].'&quot;);"><b>OK</b></button>
							    </div>
						</span>';
			$line[] = $action;
			
			
	
			
			$line[] = '<input id="item_' . $item["item_id"] . '" type="checkbox"/>';
			
			$content[] = $line;
		}
		
		return $content;
	}
	

	protected function _setSqlQuery(){
		
		$select =  "SELECT s0.sku, s0.name, s0.base_row_total_incl_tax, s0.qty_ordered,  s0.row_weight, s0.litiges, s0.item_id , s0.order_id_grossiste , s0.facture_id, s0.payment   
					FROM   
					skf_order_items s0 
					INNER JOIN  sfk_vendor s2 on s0.udropship_vendor = s2.VENDOR_ID 
 					";

		
		$where = "WHERE s0.order_id = '".$this->idItem."' ";
        if($this->vendor_id)   
		  $where.= " AND s2.VENDOR_ID = '".$this->vendor_id."'
		
		";
        $order =  "  ORDER BY s0.name"; 
		
        $this->_sql_query = $select . $where . $order;       
	
    }
    
    
        public function update($itemst)
    {

        $this->__construct($post['nom'],$post['fSoft'],$post['fNbProd']);
    }
}
?>