<?php 
class FilterListYear extends FilterList
{

	public function __construct($name, $text, $value, $start,$interval)
	{
		$this->_name = $name;
		$this->setText($text);
		$this->_value = $value;
		for ($i=0; $i<$interval ; $i++ ) {
			$this->addElement($start-$i,$start-$i);
		}
	}
}