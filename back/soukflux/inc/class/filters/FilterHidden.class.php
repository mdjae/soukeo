<?php 
/**
 * Objet g�n�rique d'input texte d'un filtre
 *
 */
class FilterHidden extends  FilterText
{
	public $_name = "";
	public $_value = "";
	public $_error_message = "";
	public $_info_message = "";
	public $_required = false;
	private $_size = 0;
	public $_input = true;
	
	public $_text = "";
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $value, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		//$this->setText($text);
		$this->_value = $value;
		//$this->_size = $size;
	}
	
	public function getName()
	{
		return $this->_name;
	}
	
	public function setText($text)
	{
		$this->_text = $text;
		if ($this->_required) $this->_text .= "<b class=\"req\">*</b>";
	}
	
	public function getValue($value)
	{
		return $this->_value;
	}
	
	public function setValue($value)
	{
		$this->_value = $value;
	}
	
	public function setRightText($text)
	{
		$this->_right_text = $text;
	}
	
	public function setLeftText($text)
	{
		$this->_left_text = $text;
	}
	
	
	
	public function display()
	{
		
		$output .= "<p>
					<input size=\"" . $this->_size . "\" type=\"hidden\" name=\"" . $this->_name . "\" id=\"id_" . 
						$this->_name . "\" value=\"" . $this->_value . "\"/>
					</p>";
		
		return $output;
	}

	
	/**
	 * Recup�re le message d'erreur apr�s le check
	 */
	public function getErrorMessage()
	{
		return $this->_error_message;
	}
	
	/**
	 * Recup�re le message d'info apr�s le check
	 */
	public function getInfoMessage()
	{
		return $this->_info_message;
	}

}
?>