<?php 
/**
 * Objet g�n�rique de liste pour filtre
 *
 */
class FilterListDouble extends FilterText
{
	// Contenu de la liste
	private $_elements1 = array();
	private $_elements2 = array();
	
	/**
	 * @param $name Nom
	 * @param $text Label
	 * @param $value Valeur
	 * @param $required Obligatoire
	 * @param $size Taille de la zone
	 */
	public function __construct($name, $text, $value1, $value2, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_value1 = $value1;
		$this->_value2 = $value2;
	}
	
	public function addElement1($value, $display)
	{
		$this->_elements1[] = Array("value" => $value, "display" => $display);
	}
	
	public function addElement2($value, $display)
	{
		$this->_elements2[] = Array("value" => $value, "display" => $display);
	}
	
	public function checkValue()
	{
		$back = true;
		return $back;
	}
	
	public function display()
	{
		$output = "<tr id=\"tr_" . $this->_name . "\">
				<td>
					<p id=\"it_" . $this->_name . "\">" . $this->_text;
		
		$output .= "<select class=\"selectTimesheetLine\" id=\"id_" . $this->_name . "_1\" name=\"" . $this->_name . "_1\"";
		$output .= ">";
		
		// Premi�re liste
		foreach($this->_elements1 as $element)
		{
			$output .= "<option value=\"" . $element['value'] . "\"";
			
			// Si la valeur est un tableau, multi-valeurs
			if (is_array($this->_value1))
			{
				if (in_array($element['value'],$this->_value1)) $output .= " selected";
			}
			else
			{
				if ($element['value'] == $this->_value1) $output .= " selected";
			}
			$output .= ">" . $element['display'] . "</option>\n";
		}
		
		$output .= "</select>";
		
		$output .= " / ";
		
		$output .= "<select class=\"selectTimesheetLine\" id=\"id_" . $this->_name . "_2\" name=\"" . $this->_name . "_2\"";
		$output .= ">";
		
		// Seconde liste
		foreach($this->_elements2 as $element)
		{
			$output .= "<option value=\"" . $element['value'] . "\"";
			
			// Si la valeur est un tableau, multi-valeurs
			if (is_array($this->_value2))
			{
				if (in_array($element['value'],$this->_value2)) $output .= " selected";
			}
			else
			{
				if ($element['value'] == $this->_value2) $output .= " selected";
			}
			$output .= ">" . $element['display'] . "</option>\n";
		}
		
		$output .= "</select>";
		
		$output .= "</p>
				</td>
			</tr>";
		
		return $output;
	}
}
?>