<?php 
class FilterListMonth extends FilterList
{
	
	public function __construct($name, $text, $value, $multiple = false, $required = false)
	{
		$this->_name = $name;
		$this->_required = $required;
		$this->setText($text);
		$this->_value = $value;
		$this->_multiple = $multiple;
		
		$this->addElement(1,'Janvier');
		$this->addElement(2,'F�vrier');
		$this->addElement(3,'Mars');
		$this->addElement(4,'Avril');
		$this->addElement(5,'Mai');
		$this->addElement(6,'Juin');
		$this->addElement(7,'Juillet');
		$this->addElement(8,'Aout');
		$this->addElement(9,'Septembre');
		$this->addElement(10,'Octobre');
		$this->addElement(11,'Novembre');
		$this->addElement(12,'D�cembre');
	}
}