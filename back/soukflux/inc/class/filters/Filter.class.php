<?php
/**
 * Classe d'affichage des boites d'option de filtrage
 * pour les pages et les reports
 * capable de reloader une grid
 * @author nsa
 *
 */
class Filter
{
    // Nom du filtre
    private $name = "";

    // Tableau des options de filtrage
    private $_filters = array();

    // Grid � recharger en pressant le bouton !
    private $_grid_reload = "";

    /**
     * Rapport � recharger en cas de rafraichissement
     * @var string
     */
    private $_report_reload = "";

    /**
     * Constructeur
     */
    public function __construct($name)
    {
        $this -> _name = $name;
    }

    /**
     * Ajoute un object filtre
     * @param $filter
     * @return unknown_type
     */
    public function addFilter($filter)
    {
        $this -> _filters[] = $filter;
    }

    /**
     * Param�tre la grid � recharger en pressant le bouton
     * @param $grid
     * @return unknown_type
     */
    public function setGridToReload($grid)
    {
        $this -> _grid_reload = $grid;
    }

    /**
     * Param�tre le rapport � recharger en pressant le bouton
     * @param $report
     * @return unknown_type
     */
    public function setReportToReload($report)
    {
        $this -> _report_reload = $report;
    }

    /**
     * Retourne le code pour l'affichage du filtre
     * @return string
     */
    public function display()
    {
        global $root_path;

        $output = "";
        //$output .= drawRMenuHeader(("Options de filtrage"),"FILTER","80");

        if ($this -> _report_reload != "")
        {
            $output .= '<form action="' . $root_path . '/common/report/report.service.php" id="filter_' . $this -> _name . '" method="POST">' . "\n";
            $output .= '<input type="hidden" name="function" value="update"/>' . "\n";
            $output .= '<input type="hidden" name="name" value="' . $this -> _report_reload . '"/>' . "\n";

        }
        else
        {
            $output .= '<form action="/common/grid/grid.service.php" id="filter_' . $this -> _name . '" method="POST">' . "\n";
            $output .= '<input type="hidden" name="function" value="update"/>' . "\n";
            $output .= '<input type="hidden" name="name" value="' . $this -> _grid_reload . '"/>' . "\n";
        }

        $output .= '<table border="0" width="100%">' . "\n";

        // Boucle sur les filters
        foreach ($this->_filters as $filter)
        {
            $output .= $filter -> display();
        }

        $output .= '<tr><td colspan="2"><p>&nbsp;</p><p align="center">
					<input class="btn btn-primary" style="padding:4px;margin:8px;font-size:1.3em" type="submit" value="' . ("Rafraîchir l'affichage") . '"/>
					</p></td></tr>';

        $output .= '</table>' . "\n";
        $output .= '</form>' . "\n";

        $output .= "<script language=\"JavaScript1.2\">\n
					  $('#filter_" . $this -> _name . "').submit(function() {
                        var commonUrl ='/common/grid/grid.service.php';
                        jQuery.ajax({
                            type : 'post',
                            data : $(this).serialize(),
                            url  : commonUrl ,
							beforeSend: function() {
									loaddialog = $('<div id=\"prod_ecom_loader\"></div>')
							    		.html('')
							    		.dialog({
							    			autoOpen: false,
							    			modal : true,
							    			draggable: false,
							    			closeText: 'hide',
							    			title : 'Rechargement de la liste...',
											closeOnEscape: false,
					   						open: function(event, ui) { $('.ui-dialog-titlebar-close', ui.dialog || ui).hide(); },
											close: function(event, ui){
												$(this).dialog('destroy');
												$(this).remove();
											},
						    			});
							    	loaddialog.dialog('open');
							},
							complete: function(){
								loaddialog.dialog('close');
							},
                            success : function(data) {";
                            
        if ($this -> _report_reload != "")
        {
            $output .= "                $('#report_" . $this -> _report_reload . "').html(data)\n";
        }
        else
        {
            $output .= "                $('#grid_" . $this -> _grid_reload . "').html(data)\n";
        }
            $output .= "}
                        })
                      return false;

                      });
					   
   
				//	$('filter_"        . $this -> _name . "').addEvent('submit', function(e)
				//		{
							//Empeche la soumission
				//			e.stop();
							
				//			var myHTMLRequest = new Request.HTML({url:'" . $root_path . "/common/grid/grid.service.php?path=" . $root_path . "',
				//				evalScripts: true,
				//				evalResponse: true,
				//				onComplete: function(responseTree, responseElements, responseHTML, responseJavaScript)
				//				{
									//alert('reload fini' + responseHTML);\n";
       // if ($this -> _report_reload != "")
       // {
            //$output .= "				$('report_" . $this->_report_reload .
            // "').set('html',responseHTML);\n";
       //     $output .= "				$('report_" . $this -> _report_reload . "').innerHTML = responseHTML;\n";
       // }
       // else
       // {
            //$output .= "				$('grid_" . $this->_grid_reload .
            // "').set('html',responseHTML);\n";
        //    $output .= "				$('grid_" . $this -> _grid_reload . "').innerHTML = responseHTML;\n";
       // }
      //  $output .= "				eval(responseJavaScript);\n";
      //  $output .= "			},
		//						onError : function(response)
		//						{
		//							alert('Erreur : ' + response);
		//						}
		//					});\n";

       // if ($this -> _report_reload != "")
       // {
            //$output .= "	$('report_" . $this->_report_reload . "').set('html','<p
            // align=\'center\'><img src=\"" . $root_path .
            // "/skins/common/images/waiter.gif\" border=\"0\"/></p>');\n";
        //    $output .= "	$('report_" . $this -> _report_reload . "').innerHTML = '<p align=\'center\'><img src=\"" . $root_path . "/skins/common/images/waiter.gif\" border=\"0\"/></p>';\n";
       // }
       // else
       // {
            //$output .= "	$('grid_" . $this->_grid_reload . "').set('html','<p
            // align=\'center\'><img src=\"" . $root_path .
            // "/skins/common/images/waiter.gif\" border=\"0\"/></p>');\n";
         //   $output .= "	$('grid_" . $this -> _grid_reload . "').innerHTML = '<p align=\'center\'><img src=\"" . $root_path . "/skins/common/images/waiter.gif\" border=\"0\"/></p>';\n";
       // }

       // $output .= "		myHTMLRequest.post($('filter_" . $this -> _name . "'));
							
							//alert('fin reload');
		//				});\n</script>\n";

        //$output .= drawRMenuFooter();
        return $output."</script>\n";
    }

}
?>