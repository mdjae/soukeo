﻿<?php 
/**
 * PHP session handling with MySQL-DB
 */
class SystemSession
{
	/**
	 * Open the session
	 * @return bool
	 */
	public static function open()
	{
		return true;
	}

	/**
	 * Close the session
	 * @return bool
	 */
	public static function close()
	{
		return true;
	}

	/**
	 * Read the session
	 * @param int session id
	 * @return string string of the sessoin
	 */
	public static function read($id)
	{
		$id = dbEscape($id);
		$sql = sprintf("SELECT `session_data` FROM `c_user_sessions` " .
					   "WHERE `session` = '%s'", $id);
		$result = dbQuery($sql);

		if ($record = dbQueryNext($result))
		{
			return $record['session_data'];
		}
		else
		{
			return '';
		}
	}

	/**
	 * Write the session
	 * @param int session id
	 * @param string data of the session
	 */
	public static function write($id, $data)
	{
		// Est-ce que la session existe ?
		$query = "SELECT count(1) as nb FROM c_user_sessions
					WHERE session = '" . dbEscape($id) . "'";
		$res = dbQueryOne($query);

		if ($res['nb'] == 0)
		{
			// Insertion
			$query = "INSERT INTO c_user_sessions
						(
							session,
							session_expires,
							session_data,
							creation_date
						)
						VALUES
						(
							'" . dbEscape($id) . "',
							'" . dbEscape(time()) . "',
							'" . dbEscape($data) . "',
							now()
						)";
		}
		else
		{
			// Mise à jour
			$query = "UPDATE c_user_sessions SET
							session_expires = '" . dbEscape(time()) . "',
							session_data = '" . dbEscape($data) . "',
							update_date = now()
						WHERE session = '" . dbEscape($id) . "'";
		}
		return dbQuery($query);
	}

	/**
	 * Destroy the session
	 * @param int session id
	 * @return bool
	 */
	public static function destroy($id)
	{
		$sql = sprintf("DELETE FROM c_obj_persistence WHERE session = '%s'", $id);
		dbQuery($sql);
		$sql = sprintf("DELETE FROM `c_user_sessions` WHERE `session` = '%s'", $id);
		return dbQuery($sql);
	}

	/**
	 * Garbage Collector
	 * @param int life time (sec.)
	 * @return bool
	 * @see session.gc_divisor		100
	 * @see session.gc_maxlifetime 1440
	 * @see session.gc_probability	  1
	 * @usage execution rate 1/100
	 *		  (session.gc_probability/session.gc_divisor)
	 */
	public static function gc($max)
	{
		$sql = sprintf("DELETE FROM `c_user_sessions` WHERE `session_expires` < '%s'",
					   dbEscape(time() - $max));
		return dbQuery($sql);
	}
}

//session_regenerate_id(false); //also works fine
/*
if (isset($_SESSION['counter']))
{
	$_SESSION['counter']++;
}
else
{
	$_SESSION['counter'] = 1;
}
*/
//echo '<p>SessionID: '. session_id() .' - Counter: '. $_SESSION['counter'] . "</p>";
?>
