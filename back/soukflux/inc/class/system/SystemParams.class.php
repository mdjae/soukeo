<?php
/**
 * Objet de gestion des param�tres syst�mes offipses
 * @author nsa
 *
 */
class SystemParams
{
    private static $params = array(
        "system" => array("descr" => "Système"),
        "system*email_from" => array(
            "descr" => "Adresse de provenance des emails",
            "default" => "thomas@silicon-village.fr"
        ),
        "system*admin_email" => array("descr" => "Email de l'administrateur"),
        "system*log_rep" => array("descr" => "Répertoire de stockage des fichiers de log"),
        //"system*php_cli_path"		=> array("descr"		=> "Chemin vers php en ligne de
        // commande"),
        
        "system*year_label" => array(
            "descr" => "Année en cours",
            "default" => 2013
        ),
        
        
        "system*select_year_min" => array(
            "descr" => "Année minimum pour les listes déroulantes",
            "default" => 2009
        ),
        "system*select_year_max" => array(
            "descr" => "Année maximum pour les listes déroulantes",
            "default" => 2013
        ),
        "system*nlist_element" => array(
            "descr" => "Nombre de ligne par pages dans les listes",
            "default" => "25"
        ),
        "system*nlist_color1" => array(
            "descr" => "Couleur alternative 1 dans les listes",
            "default" => "#EFF8FF",
            "type" => "color"
        ),
        "system*nlist_color2" => array(
            "descr" => "Couleur alternative 2 dans les listes",
            "default" => "#DBF0FF",
            "type" => "color"
        ),
        "system*nb_prods_page" => array(
            "descr" => "Nombre de produits affichés par page",
            "default" => "9"
        ),
        "system*nb_prods_page_avahis" => array(
            "descr" => "Nombre de produits affichés par page ecran gestion produit",
            "default" => "14"
        ),
         "system*select_ecom_delete" => array(
            "descr" => "Afficher les e-commerçants \"delete\"",
            "type" => "boolean",
            "default" => "N"
        ),

         "system*plugin_max_trys" => array(
            "descr" => "Nombre d'essais pour accéder à l'URL CSV d'un e-commerçant plugin",
            "default" => "5"
        ),

         "system*plugin_curl_timeout" => array(
            "descr" => "Nombre de secondes avant que cURL ne passe en time-out",
            "default" => "5"
        ),
                        
        "marketplace" => array("descr" => "Paramètres Market Place"),
        "marketplace*url" => array("descr" => "Url de la Market Place" ),
		"marketplace*urlApi" => array("descr" => "Url de l'api webservice de la market place"  ),
		"marketplace*loginApi" => array("descr" => "Login de l'Api" ),
		"marketplace*passwordApi" => array("descr" => "Mot de passe de l'Api" ,   "type" => "password"),
		"marketplace*street" => array("descr" => "Rue" ,   "default" => "71 bis rue Pasteur"),
		"marketplace*zip" => array("descr" => "Code postal" ,   "default" => "97490"),
		"marketplace*city" => array("descr" => "Ville" ,   "default" => "Saint Denis"),
		"marketplace*country" => array("descr" => "Pays" ,   "default" => "Réunion"),
		"marketplace*mailVendeur" => array("descr" => "Email contact pour vendeurs" ,   "default" => "vendeur@avahis.zendesk.com"),
		"marketplace*tel" => array("descr" => "N° de telephone entreprise" ,   "default" => "0693-911-859"),
		"marketplace*siret" => array("descr" => "N° de SIRET" ,   "default" => "791331267"),
		"marketplace*directionmail" => array("descr" => "Mail direction" ,   "default" => "direction@avahis.com"),
		"marketplace*alertcommabo1" => array( "descr" => "Seuil déclenchant l'alerte de taux commission trop faible pour vendeur Abonn. Gratuit " ,   
		                                      "default" => 50),
		//"marketplace*blocNL" => array("descr" => "" ),
		

		"grossiste" => array("descr" => "Paramètres de vente des produits de grossiste"),
        "grossiste*coef" => array(  "descr" => "Coefficient de marge minimum pour mise en vente",
        							 "default" => "1.4"),
        							 
        "grossiste*coefmax" => array(  "descr" => "Coefficient de marge maximum pour mise en vente",
        							 "default" => "6"),
        							 
        "grossiste*weightmin" => array(  "descr" => "Poids minimum pour mise en vente",
        							 "default" => "0.2"),
        							 
        "grossiste*weightmax" => array(  "descr" => "Poids maximum pour mise en vente",
        							 "default" => "30"),
        "grossiste*stock" => array(
            "descr" => "Stock minimum grossiste pour vendre produit",
            "default" => "5"
        ),

        "grossiste*prixmin" => array(
            "descr" => "Prix final (après calcul) minimum grossiste pour vendre produit",
            "default" => "5"
        ),
        
		
        "grossiste*aerienchrono" => array(
            "descr" => "Aérien Chronopost",
            "default" => "5.9"
        ),
        
		"grossiste*percentOM" => array(
            "descr" => "Octroi de mer par défaut",
            "default" => "0.25"
        ),
        
        "grossiste*percentMarge" => array(
            "descr" => "Marge par défaut",
            "default" => "0.20"
        ),
        
        "grossiste*percentTVA" => array(
            "descr" => "TVA par défaut",
            "default" => "0.085"
        ),
        
		"grossiste*nbArticlMoyen" => array(
            "descr" => "Nombre d'articles moyens / commande",
            "default" => "3"
        ),
        
		"grossiste*stockdefaultgrossiste" => array(
            "descr" => "Stock par défaut attribué aux prods grossiste sans stock",
            "default" => "5"
        ),
        
        "grossiste*autoassoc" => array(
            "descr" => "Ajout automatique des produits grossistes LDLC si la catégorie est associée",
            "type" => "boolean",
            "default" => "N"
        ),
        
		
		
		"product" => array("descr" => "Paramètres sur les produits"),
        "product*tva" => array(  "descr"  => "Champ TVA export par défaut",
        						 "default"=> "8,50% Tva Réunion"),
		
        "security" => array("descr" => "Sécurité"),
        "security*pwd_error" => array(
            "descr" => "Nombre de tentatives de connexion autorisées avant de bloquer le compte utilisateur concerné",
            "default" => "100"
        ),
        "security*pwd_min" => array(
            "descr" => "Longueur minimum du mot de passe",
            "default" => "6"
        ),
        "security*pwd_max" => array(
            "descr" => "Longueur maximum du mot de passe",
            "default" => "12"
        ),

        "ftp" => array("descr" => "Paramètres FTP"),
        //	"ftp*ftp_local_new"			=> array("descr"		=> "Répertoire local stockant les
        // fichiers ERM � traiter"),
        "ftp*ftp_local_path" => array(
            "descr" => "Répertoire local du serveur FTP",
            "default" => ""
        ),
        //	"ftp*ftp_tmp_path"			=> array("descr"		=> "R�pertoire temporaire de r�cup�ration
        // des fichers FTP", "default" => ""),
        "ftp*ftp_log_path" => array(
            "descr" => "Chemin du fichier de log des transferts FTP",
            "default" => ""
        ),
        "ftp*ftp_erm_host" => array(
            "descr" => "Adresse du serveur FTP ",
            "default" => ""
        ),
        "ftp*ftp_type" => array(
            "descr" => "Type de serveur FTP",
            "type" => "list",
            "values" => array(
                "ftp" => "FTP",
                "ftps" => "FTPs",
                "sftp" => "sFTP"
            ),
            "default" => "FTPs"
        ),
        "ftp*ftp_erm_login" => array(
            "descr" => "Login du FTP ",
            "default" => ""
        ),
        "ftp*ftp_erm_password" => array(
            "descr" => "Mot de passe du FTP ",
            "type" => "password",
            "default" => ""
        ),
        "ftp*ftp_erm_path" => array(
            "descr" => "Chemin sur le serveur FTP",
            "default" => ""
        ),
        "ftp*ftp_lftp_path" => array(
            "descr" => "Chemin du binaire LFTP",
            "default" => "/usr/bin/lftp"
        ),
        "ftp*ftp_erm_passv" => array(
            "descr" => "Activer le mode FTP passif",
            "type" => "boolean",
            "default" => "N"
        ),

		

        //"transco"						=> array("descr"		=> "Gestion des conditions de sous-traitance"),
        //"transco*client_donneur_ordre"	=> array("descr"		=> "Code du client donneur
        // d'ordre", "default" => "B60429076"),
        //"transco*email_alert_recipient"	=> array("descr"		=> "Emails en cas de conditions
        // de sous-traitance manquante", "default" => "nsaillet@stratners.com"),
        //"transco*email_report_prod"	    => array("descr"		=> "Emails pour les rapports de
        // production", "default" => "nsaillet@stratners.com"),

    //    "bugreport" => array("descr" => "Remontée de bugs"),
    //    "bugreport*system" => array(
    //        "descr" => "Système de suivi des bugs",
    //        "type" => "list",
    //        "values" => array(
    //            "none" => "Aucun",
    //            "redmine" => "Redmine"
    //        ),
    //        "default" => "none"
    //    ),

    //    "redmine" => array("descr" => "Remontée de bugs dans Redmine"),
    //   "redmine*url" => array(
    //        "descr" => "URL Redmine (sans le http:// devant)",
    //        "default" => "supportparis1.alteca.fr"
    //    ),
    //    "redmine*id_project" => array(
    //        "descr" => "ID du projet Redmine",
    //        "default" => 1
    //    ),

    //    "information" => array("descr" => "Bloc d'information "),
    //    "information*blocfr" => array("descr" => "Code html du bloc en français <img src='/skins/mpf/flag_fr.gif' />" ,  "type" => "textarea"),
        
    //    "information*blocUK" => array("descr" => "Code html du bloc en anglais <img src='/skins/mpf/flag_en.gif' />" ,  "type" => "textarea"),
        
       // "information*blocDE" => array("descr" => "Code html du bloc en allemand <img src='/skins/mpf/flag_de.gif' />" ,  "type" => "textarea"),
        
    //    "information*blocES" => array("descr" => "Code html du bloc en espagnol <img src='/skins/mpf/flag_es.gif' />" ,  "type" => "textarea"),
        
      // "information*blocNL" => array("descr" => "Code html du bloc en néerlandais <img src='/skins/mpf/flag_nl.gif' />" ,  "type" => "textarea"),

        //"sms"					    => array("descr"		=> "Envoie de sms par lesms.com"),
        //"sms*account"				=> array("descr"		=> "Nom du compte lesms.com", "default" =>
        // ""),
        //"sms*pwd"		            => array("descr"		=> "Mot de passe du compte le sms.com",
        // "default" => ""),
        //"sms*phone"		            => array("descr"		=> "Num�ro de t�l�phone des
        // destinataire", "default" => ""),
   
    );

    static public function getParamArray()
    {
        return self::$params;
    }

    /**
     *
     * @param $code
     * @return unknown_type
     */
    static public function getParam($code)
    {
        // Est-ce que la param�tre existe dans la liste ?
        if (array_key_exists($code, self::$params))
        {
            // Get the value of the param($code)
            $query = "SELECT value
						FROM c_params p
						WHERE p.code = '$code'";

            $res = dbQueryOne($query);

            // Est-ce que la valeur est vide ?
            if ($res['value'] != "")
                $value = stripslashes($res['value']);
            else
            {
                if (array_key_exists('default', self::$params[$code]))
                    $value = self::$params[$code]['default'];
                else
                    $value = "";
            }
        }
        else
            $value = ("paramètre non défini");

        return $value;
    }

    static public function displaySetting()
    {
        global $skin_path;
        $out = "<form action='$copy_url' method='POST'>";

        // - - - - BOX - -
        $mybox = new UIBox("params_box", ("Gestion des paramètres du système"), "tools", "NoBg");
        $out .= $mybox -> drawBoxHeaderStatic();
        $out .= '

		<input type="hidden" value="1" name="submitted"/>
		<table cellpadding="2" cellspacing="2" border="0" align="center" width="100%">

		<tr>
				<td align="center" colspan="2" style="height: 80px;">
					<input type="submit" class="btn btn-large btn-primary" value="Enregistrer">
				</td>
		</tr>';

        // Boucle sur le tableau des param�tres
        $param = SystemParams::getParamArray();
        $keys = array_keys($param);
        foreach ($keys as $k)
        {
            // S'il y a pas de point dedans, c'est une partie
            if (strpos($k, '*') == 0)
            {
                // Entete de partie

                $out .= ' <tr>
				<td colspan="4" align="center" style="border-bottom: 1px solid #c2c2c2; background-color: #fff">
						<h5>' . $param[$k]['descr'] . '</h5>
				<td>
			</tr>';

            }
            else
            {
                // Param�tre
                // On va chercher sa valeur dans la base
                $query = "SELECT value
							FROM c_params
							WHERE code = '" . $k . "'";
                $res = dbQueryOne($query);
                $value = stripslashes($res['value']);
                $out .= '<tr>
                        <td width="35%">
                        <p>' . $param[$k]['descr'] . '</p>
                        </td>
                        <td width="19%">' . self::_displayParamValue($k) . '</td>
                        <td width="5%">';

                // Est-ce que la valeur est renseign�e ?
                if ($value == "")
                    $out .= "<img class=\"iconCircleWarn\" src='" . $skin_path . "/images/picto/close.gif' border='0'/>";
                else
                    $out .= "<img class=\"iconCircleCheck\" src='" . $skin_path . "/images/picto/check.gif' border='0'/>";
                $out .= '</td> <td width="35%">';

                // Est-ce qu'il y a une valeur par d�faut ?
                if (array_key_exists('default', $param[$k]))
                {
                    // Oui!
                    $outecho = '';

                    if ($param[$k]['type'] == "color")
                        $outecho .= '<table cellpadding="0" cellspacing="0" border="0"><tr><td>';

                    $outecho .= '<p>' . ("Valeur par défaut") . ' : ';

                    if ($param[$k]['type'] == "color")
                        $outecho .= '</p></td><td width="20" bgcolor="' . $param[$k]['default'] . '"><img src="' . $skin_path . '/images/picto/bg_blank.gif" align="absmiddle" border="0"/></td><td><p>';

                    $outecho .= $param[$k]['default'] . '</p>';

                    if ($param[$k]['type'] == "color")
                    {
                        $outecho .= '</td></tr></table>';
                    }
                    $out .= $outecho . "\n";
                }

                // Est-ce qu'il y a un commentaire ?
                if (array_key_exists('comment', $param[$k]))
                {
                    // Oui!
                    $out .= "<p><i>" . _($param[$k]['comment']) . "</i></p>\n";
                }
                $out .= '</td></tr>';
            }
        }
        $out .= '<tr>
                <td align="center" colspan="2" style="height: 80px;">
        
                <input type="submit" class="btn btn-large btn-primary" value="Enregistrer">
                </td>
                </tr>
                </table>';

        $out .= $mybox -> drawBoxFooter();

        $out .= '</form>';
        return $out;
    }

    static private function _displayParamValue($key)
    {
        global $skin_path;

        $output = "";

        // Param�tre
        // On va chercher sa valeur dans la base
        $query = "SELECT value
                  FROM c_params
                  WHERE code = '" . $key . "'";
        $res = dbQueryOne($query);
        $value = stripslashes($res['value']);

        // Si la valeur est vide, on prend la valeur par d�faut
        if ($value == "" && array_key_exists('default', self::$params[$key]))
            $value = self::$params[$key]['default'];

        //echo "<p>" . $key . " " . $value . "</p>";

        // Le type
        if (array_key_exists('type', self::$params[$key]))
            $type = self::$params[$key]['type'];
        else
            $type = "text";

        // On affiche suivant le type
        switch ($type)
        {
            case "boolean" :
            // Type booleen
                $output .= '<p><select name="' . $key . '"> <option value="Y"';
                if ($value == "Y")
                    $output .= ' selected';
                $output .= '>' . ("Oui") . '</option> <option value="N"';
                if ($value == "N") 
                    $output .= ' selected';
                $output .= '>' . ("Non") . '</option></select></p>';
                break;
            case "list" :
            // Type liste
                $output .= '<p><select name="' . $key . '">';

                // Boucle sur les valeurs
                foreach (self::$params[$key]['values'] as $k => $v)
                {
                    $output .= '<option value="' . $k . '"';
                    if ($value == $k)
                        $output .= " selected";
                    $output .= '>' . $v . '</option>';
                }
                $output .= "</select></p>";
                break;
            case "password" :
                $output .= '<p><input type="password" name="' . $key . '" value="' . $value . '" size="40"/></p>';
                break;
            case "color" :
                $output .= '<table cellpadding="0" cellspacing="0" border="0"><tr>';
                $output .= '<td width="20" bgcolor="' . $value . '"><img src="' . $skin_path . '/images/picto/bg_blank.gif" align="absmiddle" border="0"/></td>';
                $output .= '<td><p><input type="text" name="' . $key . '" value="' . $value . '" size="20"/></p></td>';
                $output .= '</tr></table>';
                break;
                
            case "textarea":
                    $output .= '<p><textarea id="' . str_replace('*', '_', $key) . '" name="' . $key . '" >' . $value . '</textarea> </p>';
                break;
            default :
                $output .= '<p><input type="text" name="' . $key . '" value="' . $value . '" size="40"/></p>';
        }

        return $output;
    }

}
?>