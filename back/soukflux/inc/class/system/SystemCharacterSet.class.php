﻿<?php 
/**
 * Classe de gestion des caractères set
 * @author nsa
 *
 */
class SystemCharacterSet
{
	/**
	 * Traite la chaine de retour d'une requete AJAX
	 * En fonction du paramètrage de PHP
	 * @param unknown_type $ajax_output
	 */
	public static function dealWithAjaxString($ajax_output)
	{
		// Si default_charset = "iso-8859-1" est positionné dans php.ini
		// Alors pas besoin de faire un utf8_encode car le navigateur aura l'info
		// que c'est bien de l'iso-8859-1
		//if (ini_get('default_charset') != "iso-8859-1") 
		//$ajax_output = utf8_encode($ajax_output);
		
		return $ajax_output;
	}
}
?>