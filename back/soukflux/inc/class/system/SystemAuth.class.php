﻿<?php
class SystemAuth
{
    static public function Auth($login, $password)
    {
        $back = false;
        if (($login != '') && ($password != ''))
        {
            $password = md5($password);
            $query = "SELECT c_user.user_id,
							c_user.locked,
							c_user.lockauth,
							c_user.firstname,
							c_user.lastname,
							c_user.usercode 
					FROM c_user
					WHERE login=?
					AND password=?";

           // $is_exist = dbQueryAll($query);
			$db = new sdb();
			$is_exist = $db -> getAllPrep($query, array($login, $password));
			
            if (count($is_exist) == 1 && $is_exist[0]["locked"] == '0' && ($is_exist[0]["lockauth"] < SystemParams::getParam('security*pwd_error')))
            {
                $back = true;
            }
        }
        //echo "<br>" . $query;
        return $back;
    }

    static public function isUserActive($login)
    {
        $query = "SELECT c_user.user_id,
						c_user.locked,
						c_user.lockauth
					FROM c_user
					WHERE login='" . dbEscape($login) . "'";

        $is_exist = dbQueryAll($query);
        if (count($is_exist) == 1 && $is_exist[0]["locked"] == '0' && ($is_exist[0]["lockauth"] < SystemParams::getParam('security*pwd_error')))
        {
            return true;
        }
        else
            return false;
    }

}
?>