<?php
/**
 * Suivi et tracking des utilisateurs
 * 
 * @author nsa
 *
 */
class SystemUserTracking
{
    /*
     * ajoute la page consultée par l'utilisateur
     * à la page de tracking par session
    */
    public static function trackUser()
    {
        if (session_id() != '')
        {
            $query = "INSERT INTO c_user_tracking
                        (
                            session,
                            url,
                            date,
                            get,
                            post
                        )
                        VALUES
                        (
                            '" . (session_id()) . "',
                            '" . ($_SERVER['PHP_SELF']) . "',
                            now(),
                            '" . (serialize($_GET)) . "',
                            '" . (serialize($_POST)) . "'
                        )";
        }
        dbQuery($query);
    }
}