﻿<?php 
/**
 * Object de controle de la configuration du serveur
 * 
 * @author nsa
 *
 */
class SystemServerConfig
{	
	/**
	 * Affiche le tableau de controle du serveur
	 * 
	 * @return none
	 */
	public static function displayServerConfig()
	{
		global $root_path, $skin_path, $total;
		global $DATABASE_host, $DATABASE_user, $DATABASE_user_passwd;

		// Nombre de controle qui doivent �tre OK pour passer � l'installation
		$nb_ok = 9;
		
		// - - - - BOX - - 
		$mybox = new UIBox("chksrv_box",("Configuration du serveur"),"tools","NoBg"); 
		$mybox->drawBoxHeaderStatic(); 
?>
				<script>
					function displayInstall()
					{
						if (total == <?php  echo $nb_ok ?>)
						{
							document.getElementById('installation').style.display = "";
						}
						else
						{
							//alert('no! ' + total);
						}
					}
		
					var srvtest = false;
		
					function checkSrvCon()
					{
						//alert(ndf_id + ' ' + fnum);
		
						// Firefox
						if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
						// IE
						else if(window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
						else alert('<?php  echo ("Votre navigateur ne supporte pas AJAX. Impossible de faire la mise à jour du numéro de fiche") ?>');
		
						var server = document.getElementById('server').value;
						var user = document.getElementById('user').value;
						var pass = document.getElementById('pass').value;
						var url = "service.php?function=checkSrvCon&server=" + server + "&user=" + user + "&pass=" + pass;
						//alert(url);
						xhr_object.open("GET", url, false);
						xhr_object.send(null);
						if(xhr_object.readyState == 4)
						{
							//alert(xhr_object.responseText);
							if (xhr_object.responseText == "false")
							{
								document.getElementById('result').innerHTML = '<p><?php  echo ("Erreur technique") ?></p>';
							 //alert(document.getElementById('ndf_' + ndf_id).style.display);
							 //alert(document.getElementById('ndf_' + ndf_id).style.display);
							}
							else
							{
								var reg = new RegExp("[.]", "g");
								var tableau = xhr_object.responseText.split(reg);
								if (tableau[0] >= 5)
								{
									// Ok
									try
									{
										document.getElementById('result').innerHTML = '<p><?php  echo ("Version du serveur") ?> : ' + xhr_object.responseText + '</p>';
									}
									catch (e) {}
									document.getElementById('imagesrc').src = '../../skins/common/images/picto_check.gif';
								
									if (!srvtest)
									{
										srvtest = true;
										total++;
										displayInstall();
									}
									
								}
								else if (tableau[0] < 5)
								{
									document.getElementById('result').innerHTML = '<p><?php  echo ("Version du serveur") ?> : ' + xhr_object.responseText + '</p>';
									document.getElementById('action').innerHTML = '<p><?php  echo ("Utiliser un serveur MySQL de version au moins �gale � 5") ?></p>';
								}
								else
								{
									// Erreur de connexion
									document.getElementById('result').innerHTML = '<p><?php  echo ("Erreur lors de la connexion") ?> : ' + xhr_object.responseText + '</p>';
								}
							}
						}
						else
						{
							alert('<?php  echo ("probleme") ?>');
						}
					}
				</script>
				<h2 align="center"><?php  echo ("Contr�le des pr�-requis � l'installation") ?></h2>
		
				<table bgcolor="#DDDDDD" cellpadding="1" cellspacing="0" border="0" align="center" width="70%">
				<tr><td>
				<table bgcolor="#EEEEEE" cellpadding="8" cellspacing="1" border="0" width="100%" align="center">
		
				<tr bgcolor="#DDDDDD">
					<td><p align="center"><?php  echo ("Contr�le") ?></p></td>
					<td><p align="center"><?php  echo ("Information") ?></p></td>
					<td><p align="center"><?php  echo ("Statut") ?></p></td>
					<td><p align="center"><?php  echo ("Action") ?></p></td>
				</tr>
		
<?php 	
			// Version de php
			$php_version = phpversion();
			list($major,$minor) = explode(".",$php_version);
			if ($major >= 5)
			{
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$action = "";
				$total++;
			}
			else
			{
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$action = ("Mettez � jour votre version de PHP, version minimum : 5.2.0");
			}
			
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Version de PHP") ?></p></td>
					<td><p><?php  echo ("PHP version") . " " . $php_version ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>
		
<?php 
		
			// Est-ce que le dossier config est accessible en �criture ?
			if (is_writable("../../config"))
			{
				$info = ("Ok");
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$action = "";
				$total++;
			}
			else
			{
				$info = ("Ko");
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$action = ("Mettre en �criture le dossier 'config' � la racine d'offiPSe pour �crire le fichier de configuration");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Dossier de configuration en �criture") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>
		
<?php 
		
			// Est-ce que le dossier tmp est accessible en �criture ?
			if (is_writable("../../tmp"))
			{
				$info = ("Ok");
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$action = "";
				$total++;
			}
			else
			{
				$info = ("Ko");
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$action = ("Mettre en écriture le dossier 'tmp' à la racine d'offiPSe pour écrire les fichiers tmp accessibles");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Dossier tmp en �criture") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>
		

<?php 
			// Gettext
			if (("Test"))
			{
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$info = "Ok";
				$action = "";
				$total++;
			}
			else
			{
				// Pas de support gettext
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$info = "Ko";
				$action = ("Installer et activer Gettext");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Support Gettext") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>
		
<?php 
			// JSON
			if (function_exists("json_encode"))
			{
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$info = "Ok";
				$action = "";
				$total++;
			}
			else
			{
				// Pas de support JSON
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$info = "Ko";
				$action = ("Activer le support JSON");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Support JSON") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>

<?php 
			// CTYPE
			if (function_exists("ctype_digit"))
			{
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$info = "Ok";
				$action = "";
				$total++;
			}
			else
			{
				// Pas de support CTYPE
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$info = "Ko";
				$action = ("Activer le support CTYPE");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Support CTYPE") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>

<?php 
			// CALENDAR
			if (function_exists("easter_days"))
			{
				$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
				$info = "Ok";
				$action = "";
				$total++;
			}
			else
			{
				// Pas de support CTYPE
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$info = "Ko";
				$action = ("Activer le support CALENDAR");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Support CALENDAR") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>




<?php 
			// Version du client MySQL
			if (function_exists('mysql_connect'))
			{
				$mysql_version = mysql_get_client_info();
				list($major,$minor) = explode(".",$mysql_version);
				if ($major >= 5)
				{
					$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
					$info = ("Version du client MySQL : ") . " " . $mysql_version;
					$action = "";
					$total++;
				}
				else
				{
					$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
					$info = ("Version du client MySQL : ") . " " . $mysql_version;
					$action = ("Activer le support MySQL 5 dans votre installation PHP");
				}
			}
			else
			{
				// Pas de support mysql
				$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
				$info = "";
				$action = ("Activer le support MySQL 5 dans votre installation PHP");
			}
		
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Support MySQL") ?></p></td>
					<td><p><?php  echo $info ?></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td><p><?php  echo $action ?></p></td>
				</tr>
		
<?php 
			// Si client OK, on permet de tester la version de la base
			if ($major >= 5)
			{
				
			/*	$mysql_srv_version = @mysql_get_server_info();
				if ($mysql_srv_version)
				{
					list($srv_major,$srv_minor) = explode(".",$mysql_srv_version);
					if ($srv_major >= 5)
					{
						$picto = "<img src='../../skins/common/images/picto_check.gif'/>";
						$info = ("Version du serveur MySQL : ") . " " . $mysql_srv_version;
						$action = "";
					}
					else
					{
						$picto = "<img src='../../skins/common/images/picto_stop.gif'/>";
						$info = ("Version du client MySQL : ") . " " . $mysql_srv_version;
						$action = ("Utilisez une base de donn�es MySQL version 5");
					}
				}
				else
				{*/
					if (file_exists("../../config/config.inc.php"))
					{
						$root_path = "../../";
						//require_once "../../config/config.inc.php";
						$server = $DATABASE_host;
						$user = $DATABASE_user;
						$pass = $DATABASE_user_passwd;
						$pass = "";
					}
					else
					{
						$server = "";
						$user = "";
						$pass = "";
					}
		
					// Connexion �chou�e
					$picto = "<img id='imagesrc' src='../../skins/common/images/picto_stop.gif'/>";
					$info = ("Pour tester la connexion vers votre serveur, indiquer son nom, un login et un mot de passe : ") .
						"<span id='context'><form onSubmit='checkSrvCon()'>
						<table border='0'>
							<tr>
								<td><p>" . ("Serveur :") . "</p></td>
								<td><p><input type='text' id='server' value='" . $server . "'/></p></td>
								<td><p>&nbsp;</p></td>
								<td><p>" . ("Login : ") . "</p></td>
								<td><input type='text' id='user' value='" . $user . "'/></td>
							</tr>
							<tr>
								<td><p align='right'><input type='button' onClick='javascript: checkSrvCon()' value='Tester'/></p></td>
								<td><p>&nbsp;</p></td>
								<td><p>&nbsp;</p></td>
								<td><p>" . ("Mot de passe : ") . "</p></td>
								<td><input type='text' id='pass' value='" . $pass . "'/></td>
							</tr>
						</table></form></span>";
					$action = "";
				//}
		
?>
				<tr bgcolor="#FFFFFF">
					<td><p><?php  echo ("Serveur MySQL") ?></p></td>
					<td><p><?php  echo $info ?><span id='result'></span></p></td>
					<td><p align="center"><?php  echo $picto ?></p></td>
					<td id='action'><p><?php  echo $action ?></p></td>
				</tr>
		
<?php 
			}
?>
		
		
				</table>
				</td></tr>
				</table>



<?php 
		$mybox->drawBoxFooter(); 
	}

}
?>