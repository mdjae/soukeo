﻿<?php 
class SystemAuthLDAP extends SystemAuth
{
	private static $ldapConfig = array
	(
		'users' => array
		(
        	'fields' => array
			(
                                                        "givenName"					=>'first_name',
                                                        "sn"						=>'last_name',
                                                        "mail"						=>'email1',
                                                        "telephoneNumber"			=>'phone_work',
                                                        "facsimileTelephoneNumber"	=>'phone_fax',
                                                        "mobile"					=>'phone_mobile',
                                                        "street"					=>'address_street',
                                                        "l"							=>'address_city',
                                                        "st"						=>'address_state',
                                                        "postalCode"				=>'address_postalcode',
                                                        "c"							=>'address_country'
			)
		),
		'system' => array('overwriteSugarUserInfo'=>true,),
	);
	
    static public function Auth($login, $password, $server, $port, $account, $account_password, $base_dn, $attr_login, $attr_connexion)
    {
		//$server = SystemParams::getParam("ldap*host");
        //$port = SystemParams::getParam("ldap*port");

        $ldapconn = ldap_connect($server, $port);
        $error = ldap_errno($ldapconn);
        
        if(self::loginError($error)) return false;

        @ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        @ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0); // required for AD

        $bind_user = self::ldap_rdn_lookup($login, $password, $server, $port, $account, $account_password, $base_dn, $attr_login, $attr_connexion);
        
        if (!$bind_user) return false;

        // MRF - Bug #18578 - punctuation was being passed as HTML entities, i.e. &amp;
        $bind_password = html_entity_decode($password,ENT_QUOTES);
        $bind = ldap_bind($ldapconn, $bind_user, $bind_password);
        $error = ldap_errno($ldapconn);
        
        if(self::loginError($error))
        {
        	$bind = ldap_bind($ldapconn, $attr_connexion . "=" . $bind_user . "," . $base_dn, $bind_password);
            $error = ldap_errno($ldapconn);
            if(self::loginError($error))
            {
            	return false;
            }
        }
        
		if ($bind)
		{
        	// Authentication succeeded, get info from LDAP directory
            $attrs = array_keys(self::$ldapConfig['users']['fields']);
            $base_dn = $base_dn;
            $name_filter = "(" . $attr_login . "=" . $login . ")";
            //echo "<p>DEBUG " . $name_filter . "</p>";

            //$GLOBALS['log']->debug("ldapauth: Fetching user info from Directory.");
            $result = @ldap_search($ldapconn, $base_dn, $name_filter, $attrs);
            if(self::loginError($error))
            {
            	return false;
            }

            $info = @ldap_get_entries($ldapconn, $result);
            $error = ldap_errno($ldapconn);

            if(self::loginError($error))
            {
            	return false;
            }
           
            // some of these don't seem to work
            $ldapUserInfo = array();
            foreach(self::$ldapConfig['users']['fields'] as $key=>$value)
            {
            	//MRF - BUG:19765
                $key = strtolower($key);
                if(isset($info[0]) && isset($info[0][$key]) && isset($info[0][$key][0]))
                {
                	$ldapUserInfo[$value] = $info[0][$key][0];
                }
             }

             ldap_close($ldapconn);
             
             // Est-ce que l'utilisateur est actif ?
             if (self::isUserActive($login))
             {
             	return true;
             }
             else
             {
             	return false;
             }
		}
		else
		{
        	//$GLOBALS['log']->fatal("SECURITY: failed LDAP bind (login) by $this->user_name using bind_user=$bind_user");
            //$GLOBALS['log']->fatal("ldapauth: failed LDAP bind (login) by $this->user_name using bind_user=$bind_user");
            ldap_close($ldapconn);
            return false;
		}
	}

    /*
    * @return string appropriate value for username when binding to directory server.
    * @param string $user_name the value provided in login form
    * @desc Take the login username and return either said username for AD or lookup
     * distinguished name using anonymous credentials for OpenLDAP.
     * Contributions by Erik Mitchell erikm@logicpd.com
    */
    function ldap_rdn_lookup($user_name, $password, $server, $base_dn, $account, $account_password, $base_dn, $attr_login, $attr_connexion)
    {

        // MFH BUG# 14547 - Added htmlspecialchars_decode()
        //$server = SystemParams::getParam("ldap*host");
        $base_dn = htmlspecialchars_decode($base_dn);
        $admin_user = htmlspecialchars_decode($account);
        $admin_password = htmlspecialchars_decode($account_password);
        $user_attr = $attr_login;
        $bind_attr = $attr_connexion;
        $port = $port;

        $ldapconn = ldap_connect($server, $port);
        $error = ldap_errno($ldapconn);
        if(self::loginError($error)) return false;
        
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0); // required for AD
        //if we are going to connect anonymously lets atleast try to connect with the user connecting
        if(empty($admin_user))
        {
            $bind = @ldap_bind($ldapconn, $user_name, $password);
        	$error = ldap_errno($ldapconn);
        }
        if(empty($bind))
        {
        	$bind = @ldap_bind($ldapconn, $admin_user, $admin_password);
            $error = ldap_errno($ldapconn);
        }

        if(self::loginError($error))
        {
        	return false;
        }
        
        if (!$bind)
        {
            //$GLOBALS['log']->warn("ldapauth.ldap_rdn_lookup: Could not bind with admin user, trying to bind anonymously");
        	$bind = @ldap_bind($ldapconn);
            $error = ldap_errno($ldapconn);

            if(self::loginError($error)) return false;

            if (!$bind)
            {
            	//$GLOBALS['log']->warn("ldapauth.ldap_rdn_lookup: Could not bind anonymously, returning username");
                return $user_name;
            }
        }
        

        // If we get here we were able to bind somehow
        $search_filter = "(" . $user_attr."=" . $user_name . ")";
        //$GLOBALS['log']->info("ldapauth.ldap_rdn_lookup: Bind succeeded, searching for $user_attr=$user_name");
        //$GLOBALS['log']->debug("ldapauth.ldap_rdn_lookup: base_dn:$base_dn , search_filter:$search_filter");

        $result = @ldap_search($ldapconn, $base_dn , $search_filter, array("dn", $bind_attr));
        $error = ldap_errno($ldapconn);
        if(self::loginError($error))
        {
                return false;
        }
        $info = ldap_get_entries($ldapconn, $result);
        //echo "<pre>"; print_r($info); echo "</pre>";
        if($info['count'] == 0) return false;

        ldap_unbind($ldapconn);

        //$GLOBALS['log']->info("ldapauth.ldap_rdn_lookup: Search result:\nldapauth.ldap_rdn_lookup: " . count($info));

        if ($bind_attr == "dn")
        {
        	$found_bind_user = $info[0]['dn'];
        }
        else
        {
        	$found_bind_user = $info[0][strtolower($bind_attr)][0];
        }

        //$GLOBALS['log']->info("ldapauth.ldap_rdn_lookup: found_bind_user=" . $found_bind_user);

        if (!empty($found_bind_user))
        {
            return $found_bind_user;
        }
        elseif ($user_attr == $bind_attr)
        {
            return $user_name;
        }
        else
        {
            return false;
        }
    }

         /**
         * Called with the error number of the last call if the error number is 0
         * there was no error otherwise it converts the error to a string and logs it as fatal
         *
         * @param INT $error
         * @return boolean
         */
        static public function loginError($error)
        {
                if(empty($error)) return false;
                $errorstr = ldap_err2str($error);
                return $errorstr;
        }
    
}
?>