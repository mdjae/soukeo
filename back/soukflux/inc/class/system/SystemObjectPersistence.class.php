<?php 
/**
 * Implémente des fonctions de sauvegarde en base d'un objet pour le récupérer à
 * n'importe quel moment
 * @author nsa
 *
 */
class SystemObjectPersistence
{
	/**
	 * Obligation d'implémenter un nom à l'objet, unique dans la session utilisateur
	 * sert d'identifiant de sauvegarde, de mise à jour et de récupération
	 * @var unknown_type
	 */
	protected $_name = "";
	
	/**
	 * Sauvegarde de l'objet en base
	 * @return unknown_type
	 */
	protected function _save()
	{
		$query = "SELECT count(1) as nb
					FROM c_obj_persistence
					WHERE session = '" . session_id() . "'
							AND name = '" . $this->_name . "'";
		$rs = dbQueryOne($query);
		if ($rs['nb'] > 0)
		{
			$query = "UPDATE c_obj_persistence
						SET obj = '" . addslashes(base64_encode(serialize($this))) . "',
							expire = " . time() . "
						WHERE session = '" . session_id() . "'
							AND name = '" . $this->_name . "'";
		}
		else
		{
			$query = "INSERT INTO c_obj_persistence
						(
							session,
							name,
							expire,
							obj
						)
						VALUES
						(
							'" . session_id() . "',
							'" . ($this->_name) . "',
							" . time() . ",
							'" . addslashes(base64_encode(serialize($this))) . "'
						)";
		}
		dbQuery($query);
	}
	
	/**
	 * Méthode statique de récupération de l'objet depuis la base
	 * @param $name
	 * @return unknown_type
	 */
	public static function getObj($name)
	{
		$query = "SELECT obj
					FROM c_obj_persistence
					WHERE session = '" . session_id() . "'
					AND name = '" . $name . "'";
//	echo $query;	
		$rs = dbQueryone($query);
//var_dump($rs);
		//$obj = dbQueryNext($rs);

		//echo gzuncompress(base64_decode(stripslashes($obj['obj'])));
		return unserialize(base64_decode(stripslashes($rs['obj'])));
		//return unserialize(gzuncompress(stripslashes($obj['obj'])));
	}
} 
?>