<?php 
/**
 * Object de gestion de la configuration offipse
 * 
 * @author nsa
 *
 */
class SystemConfig
{
	private static $_config = array
	(
		"CORPORATE_PORTAL"		=> array("default" => "skf", "descr" => "Nom du portail"),
		"license_file"			=> array("default" => "", "descr" => "Chemin complet du fichier de licence"),

		"DATABASE_host"			=> array("default" => "localhost", "descr" => "IP ou nom du serveur MySQL", "disabled" => true),
		"DATABASE_name"			=> array("default" => "skf", "descr" => "Nom de la base de données", "disabled" => true),
		"DATABASE_dba"			=> array("default" => "skf", "descr" => "Utilisateur DBA", "disabled" => true),
		"DATABASE_dba_passwd"	=> array("default" => "skf", "descr" => "Mot de passe de l'utilisateur DBA", "type" => "password", "disabled" => true),
		"DATABASE_user"			=> array("default" => "skf", "descr" => "Utilisateur de la base de donn�es", "disabled" => true),
		"DATABASE_user_passwd"	=> array("default" => "skf", "descr" => "Mot de passe de l'utilisateur de la base de donn�es", "type" => "password", "disabled" => true),

		"ERROR_technical"		=> array("default" => true, "descr" => "Afficher les erreurs techniques", "type" => "boolean"),
		"ERROR_show_php_strict" => array("default" => false, "descr" => "Afficher les erreurs \"PHP stricts\"", "type" => "boolean"),

		"TMP_dir"				=> array("default" => "/tmp", "descr" => "Chemin du répertoire temporaire"),
	
		"APACHE_VHOST_CONFIG"	=> array("default" => false, "descr" => "Multi-configuration via les virtual hosts Apache", "type" => "boolean")
	);
	
	/**
	 * Affiche le formulaire de configuration
	 * 
	 * @return none
	 */
	public static function displayConfigSetting()
	{
		global $skin_path;
?>
		<form action="<?php  echo $copy_url ?>" method="POST">
<?php 
		// - - - - BOX - - 
		$mybox = new UIBox("config_box",("Gestion des paramètres de configuration"),"tools","NoBg"); 
		$mybox->drawBoxHeaderStatic(); 
?>
		<input type="hidden" value="1" name="submitted"/>
		<table cellpadding="2" cellspacing="2" border="0" align="center" width="55%">
<?php 
		// Boucle sur le tableau des param�tres
		$keys = array_keys(self::$_config);
		foreach($keys as $k)
		{
?>
			<tr>
				<td>
						<p><?php  echo self::$_config[$k]['descr'] ?></p>
				</td>
				<td><?php  echo self::_displayConfigValue($k); ?></td>
			</tr>
<?php 
		}
?>
			<tr>
				<td align="center" colspan="2">
				  <p>&nbsp;</p>
					<p><input type="submit" value="Enregistrer"></p>
				</td>
			</tr>
		</table>
		
<?php 
		$mybox->drawBoxFooter(); 
?>		
		</form>
<?php 
	}
	
	/**
	 * Affiche un param�tre dans le formulaire
	 * 
	 * @param $key
	 * 
	 * @return string
	 */
	static private function _displayConfigValue($key)
	{
		$output = "";
		// Si la valeur est vide, on prend la valeur par d�faut
		if (isset($$key)) $value = $$key;
		else if (array_key_exists($key, $_POST)) $value = $_POST[$key];
		else if (array_key_exists('default', self::$_config[$key])) $value = self::$_config[$key]['default'];
		
		//echo "<p>" . $key . " " . $value . "</p>";
		
		// Le type
		if (array_key_exists('type', self::$_config[$key])) $type = self::$_config[$key]['type'];
		else $type = "text";
		
		// On affiche suivant le type
		switch ($type)
		{
			case "boolean":
				// Type booleen
				$output .= '<p><select name="' . $key . '">
									<option value="true"';
				if ($value) $output .= ' selected';
				$output .= '>' . ("Oui") . '</option>
									<option value="false"';
				if (!$value) $output .= ' selected';
				$output .= '>' . ("Non") . '</option>
								</select></p>';
				break;
			case "list":
				// Type liste
				$output .= '<p><select name="' . $key . '">';
				
				// Boucle sur les valeurs
				foreach(self::$params[$key]['values'] as $k => $v)
				{
					$output .= '<option value="' . $k . '"';
					if ($value == $k) $output .= " selected";
					$output .= '>' . $v . '</option>';
				}	
				$output .= "</select></p>";
				break;
			case "password":
				$output .= '<p><input type="password" name="' . $key . '" value="' . $value . '" size="40"/></p>';
				break;
			default:
				$output .= '<p><input type="text" name="' . $key . '" value="' . $value . '" size="40"/></p>';
		}
		
		return $output;
	}
	
	/**
	 * Met � jour le fichier de config
	 * 
	 * @return unknown_type
	 */
	public static function updateConfigFile()
	{
		global $root_path;
		$write_file = true;
		$error = array();
		
		// Controle des param�tres de MySQL
		$conn_dba = @mysql_connect($_POST['DATABASE_host'], $_POST['DATABASE_dba'], $_POST['DATABASE_dba_passwd']);
		$err_dba = mysql_error();
		$conn_user = @mysql_connect($_POST['DATABASE_host'], $_POST['DATABASE_user'], $_POST['DATABASE_user_passwd']);
		$err_user = mysql_error();
		
		if (!$conn_dba)
		{
			$write_file = false;
			$error[] = ("Erreur de connexion DBA : " . $err_dba);
		}
		
		if (!$conn_user)
		{
			$write_file = false;
			$error[] = ("Erreur de connexion utilisateur : " . $err_user);
		}
		
		if ($write_file && !mysql_select_db($_POST['DATABASE_name'], $conn_dba))
		{
			$write_file = false;
			$error[] = ("Erreur de connexion DBA : impossible d'acc�der � la base " . $_POST['DATABASE_name']);
		}
		
		if ($write_file && !mysql_select_db($_POST['DATABASE_name'], $conn_user))
		{
			$write_file = false;
			$error[] = ("Erreur de connexion utilisateur : impossible d'acc�der � la base " . $_POST['DATABASE_name']);
		}
	
		if ($write_file)
		{
			$content = "<?php \n";
			foreach($_POST as $k => $v)
			{
				if (self::$_config[$k]['type'] != "boolean") $content .= '$' . $k . ' = "' . $v . '"' . ";\n";
				else $content .= '$' . $k . ' = ' . $v . ";\n";
			}
			$content .= "?>\n";
			file_put_contents($root_path . "config/config.inc.php", $content);
			return true;
		}
		else
		{
			foreach($error as $e)
			echo "<h2 align='center'>" . $e . "</h2>";
			return false;
		}
	}
}
?>