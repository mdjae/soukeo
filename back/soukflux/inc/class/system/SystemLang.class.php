<?php
/**
 * Classe de gestion des langage dans le portail
 * @author nsa
 *
 */
class SystemLang
{
    /**
     * Détermine et stocke en session la langue de l'utilisateur
     * @return none
     */
    public static function setLang()
    {
        // LANGUAGE
        if (array_key_exists('lang',$_GET))
        {
            // change the navigation language
           self::getlang($_GET['lang']);
           
        }
        else
        {

            if (!array_key_exists('lang',$_SESSION) || $_SESSION['lang'] == 'null')
            {
                // Sets its value
                if ( array_key_exists('HTTP_ACCEPT_LANGUAGE', $_SERVER) )
                {
                    self::getlang( strtoupper( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) ) );
                }
                else
                { 
                    $_SESSION['lang'] = 'en_US';
                }
            }
        }

        
    }
    
    public static function getlang($lang){

         switch ($lang) {
                case 'FR':
                    $_SESSION['lang'] = "fr_FR";
                    break;
                case 'EN':
                    $_SESSION['lang'] = "en_US";
                    break;
                /*case 'DE':
                    $_SESSION['lang'] = "de_DE";
                    break;
                   
                case 'NL':
                    $_SESSION['lang'] = "nl_NL";
                    break;
                */
                case 'ES':
                    $_SESSION['lang'] = "es_ES";
                    break;

                
                default:
                    $_SESSION['lang'] = "en_US";
                    break;
            } 
    }
    
    public static function getUrllang(){
     
         switch ($_SESSION['lang']) {
                case 'fr_FR':
                    return "lang=FR";
                    break;
                case 'en_US':
                    return "lang=EN";
                    break;
                /*case 'de_DE':
                    return "lang=DE";
                    break;
                case 'nl_NL':
                    return "lang=NL";
                    break;
                */
                case 'es_ES':
                    return "lang=ES";
                    break;

                default:
                    return "lang=EN";
                    break;
            } 
    }
    
    public static function getUrlCampLang(){
     
         switch ($_SESSION['lang']) {
                case 'fr_FR':
                    return "fr";
                    break;
                case 'en_US':
                    return "UK";
                    break;
              /*  case 'de_DE':
                    return "DE";
                    break;

                 case 'nl_NL':
                    return "NL";
                    break;  
               */
                case 'es_ES':
                    return "ES";
                    break;

                default:
                    return "UK";
                    break;
            } 
    }
    public static function getLangGmap(){
                switch ($_SESSION['lang']) {
                case 'fr_FR':
                    return "fr";
                    break;
                case 'en_US':
                    return "en";
                    break;
              /*  case 'de_DE':
                    return "de";
                    break;

                 case 'nl_NL':
                    return "nl";
                    break;  
               */
                case 'es_ES':
                    return "es";
                    break;

                default:
                    return "en";
                    break;
            } 
        
    }
    
}