<?php
/**
 * Objet de gestion des param�tres systèmes offipses
 * @author nsa
 *
 */
class SystemAccess
{
    private static $lbl_secu = array(
        "common" => "Commun",
        "services" => "Services",
        "admin" => "Administration",
      //  "gest" => "Gestionnaire",
        "catalogue" => "Catalogue",
         "commercants" => "Commerçants",
         "grossistes"  => "Grossistes",
         "avahis"  => "Avahis",
         "clients"  => "Clients"
    );

    private static $access_key = array(
    
        "common.login.." => array(
            "seq" => "10000",
            "domain" => "common",
            "catagory" => "login",
            "function" => "",
            "special" => "",
            "security_key" => "common.login..",
            "page_path" => "common/login/",
            "description" => "Identification"
        ),
        "common.login.pwd.popup" => array(
            "seq" => "10100",
            "domain" => "common",
            "catagory" => "login",
            "function" => "pwd",
            "special" => "popup",
            "security_key" => "common.login.pwd.popup",
            "page_path" => "common/login/popup_pwd/",
            "description" => "Mot de passe oublié"
        ),
        "common.logout.." => array(
            "seq" => "10200",
            "domain" => "common",
            "catagory" => "logout",
            "function" => "",
            "special" => "",
            "security_key" => "common.logout..",
            "page_path" => "common/logout/",
            "description" => "Déconnexion"
        ),
        "common.pwd.." => array(
            "seq" => "15000",
            "domain" => "common",
            "catagory" => "pwd",
            "function" => "",
            "special" => "",
            "security_key" => "common.pwd..",
            "page_path" => "common/pwd/",
            "description" => "Changement de mot de passe"
        ),
        
        "admin.users.." => array(
            "seq" => "90000",
            "domain" => "admin",
            "catagory" => "users",
            "function" => "",
            "special" => "",
            "security_key" => "admin.users..",
            "page_path" => "o_admin/users/",
            "description" => "Gestion des utilisateurs"
        ),

        "admin.users..addmod" => array(
            "seq" => "90900",
            "domain" => "admin",
            "catagory" => "users",
            "function" => "",
            "special" => "addmod",
            "security_key" => "admin.users..addmod",
            "page_path" => "o_admin/users/",
            "description" => "Ajouter et modifier un utilisateur"
        ),

        "admin.groups.." => array(
            "seq" => "92000",
            "domain" => "admin",
            "catagory" => "groups",
            "function" => "",
            "special" => "",
            "security_key" => "admin.groups..",
            "page_path" => "o_admin/groups/",
            "description" => "Gestion des groupes de sécurité"
        ),
        "admin.groups..addmod" => array(
            "seq" => "92100",
            "domain" => "admin",
            "catagory" => "groups",
            "function" => "",
            "special" => "addmod",
            "security_key" => "admin.groups..addmod",
            "page_path" => "o_admin/groups/",
            "description" => "Ajouter et modifier un groupe de sécurité"
        ),

        "admin.system.." => array(
            "seq" => "95000",
            "domain" => "admin",
            "catagory" => "system",
            "function" => "",
            "special" => "",
            "security_key" => "admin.system..",
            "page_path" => "o_admin/system/",
            "description" => "Gestion des paramètres du système"
        ),

        "admin.batch.." => array(
            "seq" => "97000",
            "domain" => "admin",
            "catagory" => "batch",
            "function" => "",
            "special" => "",
            "security_key" => "admin.batch..",
            "page_path" => "o_admin/batch/",
            "description" => "Tâches périodiques"
        ),
       
        "admin.filesexport.." => array(
            "seq" => "97500",
            "domain" => "admin",
            "catagory" => "filesexport",
            "function" => "",
            "special" => "",
            "security_key" => "admin.filesexport..",
            "page_path" => "o_admin/filesexport/",
            "description" => "Téléchargement de fichiers"
        ),
       
        "admin.gestionreferentiels.." => array(
            "seq" => "97501",
            "domain" => "admin",
            "catagory" => "gestionreferentiels",
            "function" => "",
            "special" => "",
            "security_key" => "admin.gestionreferentiels..",
            "page_path" => "o_admin/gestionreferentiels/",
            "description" => "Gestion des réferentiels"
        ),
       
        "admin.statistiques.." => array(
            "seq" => "97502",
            "domain" => "admin",
            "catagory" => "statistiques",
            "function" => "",
            "special" => "",
            "security_key" => "admin.statistiques..",
            "page_path" => "o_admin/statistiques/",
            "description" => "Statistiques"
        ),
        
		"admin.showeditmenu.." => array(
            "seq" => "97503",
            "domain" => "admin",
            "catagory" => "showeditmenu",
            "function" => "",
            "special" => "",
            "security_key" => "admin.showeditmenu..",
            "page_path" => "o_admin/showeditmenu/",
            "description" => "Edition du menu Avahis"
        ),
                	   
        "services.download.." => array(
			"seq" => "16200", 
			"domain" => "services",
			"catagory" => "download",
			"function" => "",
			"special" => "",
			"security_key" => "services.download..", 
			"page_path" => "o_services/download/", 
			"description" => "Téléchargement des fichiers transformés"
		),

		"services.download..allusers" => array(
			"seq" => "16250",
			"domain" => "services", 
			"catagory" => "download", 
			"function" => "", 
			"special" => "allusers", 
			"security_key" => "services.download..allusers", 
			"page_path" => "o_services/download/", 
			"description" => "Téléchargement de tous les fichiers transformés"
		),
 
		  "catalogue.categorie.." => array(
            "seq" => "100000",
            "domain" => "catalogue",
            "catagory" => "categorie",
            "function" => "",
            "special" => "",
            "security_key" => "catalogue.categorie..",
            "page_path" => "catalogue/categorie",
            "description" => "Gestion des catégories"
        ),

        
       "catalogue.produit.." => array(
             "seq" => "100100",
            "domain" => "catalogue",
            "catagory" => "produit",
            "function" => "",
            "special" => "",
            "security_key" => "catalogue.produit..",
            "page_path" => "catalogue/produit",
            "description" => "Gestion des produits"
        ),
        
       "catalogue.attributs.." => array(
             "seq" => "100101",
            "domain" => "catalogue",
            "catagory" => "attributs",
            "function" => "",
            "special" => "",
            "security_key" => "catalogue.attributs..",
            "page_path" => "catalogue/attributs",
            "description" => "Gestion des attributs"
        ),
                        
       "catalogue.ficheproduit.." => array(
             "seq" => "100102",
            "domain" => "catalogue",
            "catagory" => "ficheproduit",
            "function" => "",
            "special" => "",
            "security_key" => "catalogue.ficheproduit..",
            "page_path" => "catalogue/produit/fiche",
            "description" => "Voir une fiche produit"
        ),
        
       "catalogue.createproduit.." => array(
             "seq" => "100104",
            "domain" => "catalogue",
            "catagory" => "createproduit",
            "function" => "",
            "special" => "",
            "security_key" => "catalogue.createproduit..",
            "page_path" => "catalogue/produit/fiche/create",
            "description" => "Créer une fiche produit"
        ),
                 
       "catalogue.rechercheproduitgrossiste.." => array(
        	 "seq" => "100150",
            "domain" => "catalogue",
            "catagory" => "rechercheproduitgrossiste",
            "function" => "",
            "special" => "",
            "security_key" => "catalogue.rechercheproduitgrossiste..",
            "page_path" => "catalogue/rechercheproduitgrossiste",
            "description" => "Recherche Produit grossiste"
        ),
        
		"commercants.ecommercant.." => array(
        	 "seq" => "100200",
            "domain" => "commercants",
            "catagory" => "ecommercant",
            "function" => "",
            "special" => "",
            "security_key" => "commercants.ecommercant..",
            "page_path" => "commercants/ecommercant",
            "description" => "Qualification E-Commerçants"
        ),
              "commercants.ecommercant.ficheproduit.addmod" => array(
        	 "seq" => "100210",
            "domain" => "commercants",
            "catagory" => "ecommercant",
            "function" => "ficheproduit",
            "special" => "addmod",
            "security_key" => "commercants.ecommercant.ficheproduit.addmod",
            "page_path" => "commercants/ecommercant/ficheProduit",
            "description" => "Fiche produit à associer"
        ),
        
		"commercants.ecommercant.assoc.addmod" => array(
        	 "seq" => "100220",
            "domain" => "commercants",
            "catagory" => "ecommercant",
            "function" => "assoc",
            "special" => "addmod",
            "security_key" => "commercants.ecommercant.assoc.addmod",
            "page_path" => "commercants/ecommercant/assoc",
            "description" => "Ecran asociation"
        ),
        
         "commercants.tracking.." => array(
             "seq" => "100221",
            "domain" => "commercants",
            "catagory" => "tracking",
            "function" => "",
            "special" => "",
            "security_key" => "commercants.tracking..",
            "page_path" => "commercants/tracking",
            "description" => "Tracking Plugin"
        ),
        "commercants.facturation.." => array(
             "seq" => "100222",
            "domain" => "commercants",
            "catagory" => "facturation",
            "function" => "",
            "special" => "",
            "security_key" => "commercants.facturation..",
            "page_path" => "commercants/facturation",
            "description" => "Facturation Commercants"
        ),
        
        "commercants.historique_factures_com.." => array(
             "seq" => "100223",
            "domain" => "commercants",
            "catagory" => "historique_factures_com",
            "function" => "",
            "special" => "",
            "security_key" => "commercants.historique_factures_com..",
            "page_path" => "commercants/historique_factures_com",
            "description" => "Historique factures commerçants"
        ),
        
        "commercants.commande_commercant.." => array(
             "seq" => "100224",
            "domain" => "commercants",
            "catagory" => "commande_commercant",
            "function" => "",
            "special" => "",
            "security_key" => "commercants.commande_commercant..",
            "page_path" => "commercants/commande_commercant",
            "description" => "Commandes Commercant"
        ),
        
        "commercants.abonnements.." => array(
             "seq" => "100225",
            "domain" => "commercants",
            "catagory" => "abonnements",
            "function" => "",
            "special" => "",
            "security_key" => "commercants.abonnements..",
            "page_path" => "commercants/abonnements",
            "description" => "Info abonnements commerçants"
        ),
		"grossistes.grossiste.." => array(
        	 "seq" => "100800",
            "domain" => "grossistes",
            "catagory" => "grossiste",
            "function" => "",
            "special" => "",
            "security_key" => "grossistes.grossiste..",
            "page_path" => "grossistes/grossiste",
            "description" => "Qualification Grossistes"
        ),
            "grossistes.grossiste.categoriesgrossiste.addmod" => array(
        	"seq" => "100810",
            "domain" => "grossistes",
            "catagory" => "grossiste",
            "function" => "categoriesgrossiste",
            "special" => "addmod",
            "security_key" => "grossistes.grossiste.categoriesgrossiste.addmod",
            "page_path" => "grossistes/categoriesgrossiste",
            "description" => "Catégories grossiste"
        ),
        
            "grossistes.grossiste.assocdealer.addmod" => array(
        	 "seq" => "100820",
            "domain" => "grossistes",
            "catagory" => "grossiste",
            "function" => "assocdealer",
            "special" => "addmod",
            "security_key" => "grossistes.grossiste.assocdealer.addmod",
            "page_path" => "grossistes/grossiste/assocdealer",
            "description" => "Ecran asociation grossiste"
        ),
          "grossistes.annonce_reception.." => array(
             "seq" => "100821",
            "domain" => "grossistes",
            "catagory" => "annonce_reception",
            "function" => "",
            "special" => "",
            "security_key" => "grossistes.annonce_reception..",
            "page_path" => "grossistes/annonce_reception",
            "description" => "Annonces de réception"
        ),
        
         "grossistes.commande_grossiste.." => array(
             "seq" => "100822",
            "domain" => "grossistes",
            "catagory" => "commande_grossiste",
            "function" => "",
            "special" => "",
            "security_key" => "grossistes.commande_grossiste..",
            "page_path" => "grossistes/commande_grossiste",
            "description" => "Commandes Grossiste"
        ),        
       
   /*     "qualification.venteavahis.." => array(
        	 "seq" => "101000",
            "domain" => "qualification",
            "catagory" => "venteavahis",
            "function" => "",
            "special" => "",
            "security_key" => "qualification.venteavahis..",
            "page_path" => "qualification/venteavahis",
            "description" => "Ventes Avahis"
        ),

        
        
		"qualification.listeproduits.." => array(
        	 "seq" => "100300",
            "domain" => "qualification",
            "catagory" => "listeproduits",
            "function" => "",
            "special" => "popup",
            "security_key" => "qualification.listeproduits..",
            "page_path" => "qualification/ecommercant/listeProduits",
            "description" => "Qualification Produits E-Commerçants"
        ),
        

    

		 "facturation.commande.." => array(
        	 "seq" => "102550",
            "domain" => "facturation",
            "catagory" => "commande",
            "function" => "",
            "special" => "",
            "security_key" => "facturation.commande..",
            "page_path" => "facturation/commande",
            "description" => "Commandes"
        ),*/

        "avahis.commande_avahis.." => array(
        	 "seq" => "102600",
            "domain" => "avahis",
            "catagory" => "commande_avahis",
            "function" => "",
            "special" => "",
            "security_key" => "avahis.commande_avahis..",
            "page_path" => "avahis/commande_avahis",
            "description" => "Commandes Avahis"
        ),
        
        "avahis.item_grossiste_to_order.." => array(
             "seq" => "102601",
            "domain" => "avahis",
            "catagory" => "item_grossiste_to_order",
            "function" => "",
            "special" => "",
            "security_key" => "avahis.item_grossiste_to_order..",
            "page_path" => "avahis/item_grossiste_to_order",
            "description" => "A commander"
        ),
        
		"clients.listing_acheteurs.." => array(
        	 "seq" => "102800",
            "domain" => "clients",
            "catagory" => "listing_acheteurs",
            "function" => "",
            "special" => "",
            "security_key" => "clients.listing_acheteurs..",
            "page_path" => "clients/listing_acheteurs",
            "description" => "Listing Acheteurs"
        ),
        "clients.listing_vendeurs.." => array(
             "seq" => "102801",
            "domain" => "clients",
            "catagory" => "listing_vendeurs",
            "function" => "",
            "special" => "",
            "security_key" => "clients.listing_vendeurs..",
            "page_path" => "clients/listing_vendeurs",
            "description" => "Listing Vendeurs"
        ),

        "clients.listing_tickets.." => array(
             "seq" => "102802",
            "domain" => "clients",
            "catagory" => "listing_tickets",
            "function" => "",
            "special" => "",
            "security_key" => "clients.listing_tickets..",
            "page_path" => "clients/listing_tickets",
            "description" => "Listing tickets"
        ),
                
        "clients.livraisons.." => array(
             "seq" => "102803",
            "domain" => "clients",
            "catagory" => "livraisons",
            "function" => "",
            "special" => "",
            "security_key" => "clients.livraisons..",
            "page_path" => "clients/livraisons",
            "description" => "Modes de livraison"
        ),

		
    );

    /**
     * Renvoie le tableau des droits d'acc�s
     * @return unknown_type
     */
    public static function getAccessList()
    {
        return self::$access_key;
    }

    /**
     * Renvoie la description
     * @param $security_key
     * @return unknown_type
     */
    public static function getDescription($security_key)
    {
        return self::$access_key[$security_key]['description'];
    }

    /**
     * Renvoie l'url
     * @param $security_key
     * @return unknown_type
     */
    public static function getPagePath($security_key)
    {
        return self::$access_key[$security_key]['page_path'];
    }

    /**
     * Renvoie l'url
     * @param $domain
     * @param $catagory
     * @return unknown_type
     */
    public static function getPagePathDomCat($domain, $catagory)
    {
        return self::$access_key[$domain . "." . $catagory . ".."]['page_path'];
    }

    public static function getLblSecu($key)
    {
        return self::$lbl_secu[$key];
    }

    /**
     * Fonction de contr�le d'acc�s
     * $key : string
     *  Cl� de fonctionnalit�, exemple services.timesheets..modadd
     *  Possibilit� de mettre un % comme caract�re joker
     */
    public static function canAccess($key)
    {
        $key_array = split('\.', $key);
        $canAccess = false;
		
		// Pour la page de login, on force à true
        if ($key == "common.login.."){
            $canAccess = true;
        }
        else
        if (array_key_exists('smartlog', $_SESSION) && array_key_exists('smartgroup', $_SESSION))
        {
            $query = "SELECT count(*) as '0'
                        FROM c_group_security
                        WHERE group_id IN " . self::userGroupToSql() . "
                            AND domain LIKE '" . $key_array[0] . "'
                            AND catagory LIKE '" . $key_array[1] . "'
                            AND function LIKE '" . $key_array[2] . "'
                            AND special LIKE '" . $key_array[3] . "'";

            $result = dbQueryOne($query);

            if ($result[0] > 0)
            {
                $canAccess = true;
            }
        }
        // return is not reached in case of "no access"
        return $canAccess;
    }

    /**
     * Check if key is not denied
     * if key not set
     */
    public static function canAccessPage2($key = "")
    {
        global $root_path, $ACCESS_key;

        // Overload $ACCESS_key is key passed
        if ($key != "")
            $ACCESS_key = $key;

        if (!isset($ACCESS_key))
        {
            trigger_error(("ATTENTION : l'accès à la page n'est pas sécurisée, déclarer la variable \$ACCESS_key"));
        }
        else
        if (!self::canAccess($ACCESS_key))
        {
            header("location: /common/logout/");
        }
    }

    /*
     * Retourne des groupes de l'utilisateur
     * pour etre utilisé dans IN SQL (grp1, grp2, grp3)
     */
    public static function userGroupToSql()
    {
        $first = true;
        $output = "";
        //print_r($_SESSION['smartgroup']);
        if (array_key_exists('smartgroup', $_SESSION) && is_array($_SESSION['smartgroup']))
        {
            foreach ($_SESSION['smartgroup'] as $grp)
            {
                if ($first)
                {
                    $output = "(" . $grp;
                    $first = false;
                }
                else
                    $output .= "," . $grp;
            }
            $output .= ")";
        }
        else
        {
            $output = "(null)";
        }

        return $output;
    }

    /*
     * Détermine si l'on peut voir un utilisateur
     * en fonction des équipes gérées
     */
    public static function isUserTeamAllowed($usercode)
    {
        $allowed = false;

        $query = "SELECT count(1)
                        FROM c_team t,
                            c_team_users tu,
                            c_team_managers tm,
                            c_user u
                        WHERE t.team_id = tu.team_id
                            AND tu.team_id = tm.team_id
                            AND u.user_id = tu.user_id
                            AND tm.user_id = " . $_SESSION['smartuserid'] . "
                            AND u.usercode = '" . $usercode . "'";

        $res = dbQueryAll($query);

        if (count($res) > 0)
        {
            $allowed = true;
        }

        return $allowed;
    }
}