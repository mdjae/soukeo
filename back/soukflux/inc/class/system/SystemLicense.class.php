﻿<?php 
/**
 * Objet de gestion des licences offiPSe
 * @author nsa
 *
 */
class SystemLicense
{
	private $loader_enabled = false;
	private $optimzer_enabled = false;
	private $host_id = 0;
	private $license_enabled = false;
	private $license_infos = array();
	private $license_file = "";
	private $file_encoded = false;
	private $nb_users = 0;
	private $max_users = 0;
	private $max_date = "1970/01/01";
	private $max_concurrent_users = 0;
	
	/**
	 * Constructeur
	 * @return unknown_type
	 */
	public function __construct($license_file = "")
	{
		//if ($license_file != "") zend_loader_install_license($license_file,true);
		$this->loaded_enabled = zend_loader_enabled();
		$this->optimzer_enabled = zend_loader_enabled();
		$this->host_id = zend_get_id();
		$lic = zend_loader_file_licensed();
		//echo "<pre>toto"; print_r($lic); echo "</pre>";
		if ($lic !== false)
		{
			$this->license_enabled = true;
			$this->license_infos = $lic;
			$this->max_users = $lic['max_users'];
			$this->max_concurrent_users = $lic['Max-Concurrent'];
		}
		$this->license_file = $license_file;
		$this->file_encoded = zend_loader_file_encoded();
		
		// Récupération du nombre d'utilisateurs
		$query = "SELECT count(usercode) as nb
					FROM c_user
					WHERE locked = 0";
		$res = dbQueryOne($query);
		$this->nb_users = $res['nb'];
		
		// Controle de license
		if ($this->file_encoded && $this->nb_users > $this->max_users)
		{
			// Problème de license
			trigger_error(sprintf(("Le nombre d'utilisateur dépasse les possibilités de la license installée : %s"),$this->max_users), E_USER_ERROR);
			//exit;
		}
	}
	
	public function isEncoded()
	{
		return $this->file_encoded;
	}
	
	public function isLicenseFileOk()
	{
		return $this->license_enabled;
	}
	
	public function getLicenseFile()
	{
		return $this->license_file;
	}
	
	/**
	 * Indique si l'on peut ajouter un utilisateur
	 * par rapport à la licence installée.
	 * La fonction tient compte de l'activation ou pas du
	 * système de license
	 * 
	 * @return boolean
	 */
	public function canCreateUser()
	{
		// Uniquement encodé
		if ($this->file_encoded)
		{
			$query = "SELECT count(usercode) as nb
					FROM c_user
					WHERE locked = 0";
			$res = dbQueryOne($query);
			$nb = $this->getNbUser();
			if ($nb < $this->max_users) return true;
			else return false;
		}
		return true;
	}
	
	/**
	 * Retourne le nombre d'utilisateur créés dans
	 * la base actuelle
	 * 
	 * @return int
	 */
	public function getNbUser()
	{
		return $this->nb_users;
	}
	
	/**
	 * Retourne le nombre maximum d'utlisateur que
	 * la license en place permet de gérer
	 * 
	 * @return int
	 */
	public function getMaxUser()
	{
		return $this->max_users;
	}
	
	/**
	 * Retourne une string html
	 * affichant des infos sur la license en place
	 * 
	 * @return string
	 */
	public function displayLicenseInfo()
	{
		if ($this->loader_encoded) $loader = ("oui");
		else $loader = ("non");
		if ($this->file_encoded) $encoded = ("oui");
		else $encoded = ("non");
		if ($this->license_enabled) $license_enabled = ("oui");
		else $license_enabled = ("non");
		
		$output .= "";
		$output .= "<p>" . ("Zend loader") . " : <b>" . $loader . "</b></p>";
		$output .= "<p>" . ("Fichier de licence") . " : <b>" . $this->license_file . "</b></p>";
		//$output .= "<p>" . ("Host ID") . " : <b>" . print_r($this->host_id,true) . "</b></p>";
		$output .= "<p>" . ("Encodage") . " : <b>" . $encoded . "</b></p>";
		$output .= "<p>" . ("Licence activée") . " : <b>" . $license_enabled . "</b></p>";
		if ($this->license_enabled)
		{
			//$output .= "<p>" . ("Fichier de license") . " : <b>" . $this->license_file . "</b></p>";
			foreach($this->license_infos as $k => $l)
			{
				$output .= "<p>" . $k . " : <b>" . $l . "</b></p>";
			}
		}
		return $output;
	}
}
?>