<?php 
class ReportGrid extends Grid
{	
	/**
	 * R�sultat � afficher
	 * @var unknown_type
	 */
	protected $_result = array();
	
	public function __construct($title)
	{	
		$this->_name = 'reportGrid';
		//$this->_result = $result;

		$this->_init('board_admin',$this->_makeHeaders(),$this->_makeContent());
		$this->setTitle("NoBg","grids_report", $title);
	}

	public function _makeHeaders()
	{
		$headers = array();
		
		// Boucle sur les cl�s
		if (count($this->_result) > 0)
		{
			foreach ($this->_result[0] as $key => $val)
			{
				$headers[] = array("name" => $key, "sortable" => true, "excelable" => true);
			}
		}

		return $headers;
	}

	public function _makeContent()
	{
		$content = array();
		
		foreach($this->_result as $l)
		{        		
			$line = array();
			
			foreach($l as $v)
			{
				$val = stripslashes($v);
				$line[] = $val;
			}

			$content[] = $line;
		}
		
		return $content;
	}
}
?>