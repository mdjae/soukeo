<?php 
/**
 * Classe pour connecter offipse � mantis
 * @author nsa
 *
 */
class BugReportMantis
{
	private $_client;
	private $_project_id;
	private $_user;
	
	public function __construct()
	{
		$this->_user = SystemParams::getParam("mantis*user");
		$this->_password = SystemParams::getParam("mantis*password");
		$this->_project_id = SystemParams::getParam("mantis*id_project");
		$soap_url = SystemParams::getParam("mantis*wsdl_url");
		
		$this->_client = new SoapClient($soap_url);
	}
	
	/**
	 * R�cup�re la liste des cat�gories de bugs
	 * @return unknown_type
	 */
	public function getCatagories()
	{
		return $this->_client->mc_project_get_categories($this->_user, $this->_password, $this->_project_id);
	}
	
	/**
	 * Retourne la liste des types de reproductibilit�s
	 * @return unknown_type
	 */
	public function getReproducibilities()
	{
		return $this->_client->mc_enum_reproducibilities($this->_user, $this->_password);
	}
	
	/**
	 * Retourne la liste des s�v�rit�s
	 * @return unknown_type
	 */
	public function getSeverities()
	{
		return $this->_client->mc_enum_severities($this->_user, $this->_password);
	}
	
	/**
	 * Retourne la liste des priorit�s
	 * @return unknown_type
	 */
	public function getPriorities()
	{
		return $this->_client->mc_enum_priorities($this->_user, $this->_password);
	}
	
	/**
	 * Return la liste des versions
	 * @return unknown_type
	 */
	public function getVersions()
	{
		return $this->_client->mc_project_get_versions($this->_user, $this->_password, $this->_project_id);
	}
}
?>