<?php
/**
  * Bib de gestion des erreurs offipse
  *
  * $errno : Niveau d'erreur
  * $errstr : contient le message d'erreur, sous forme de cha�ne
  * $errfile : contient le nom du fichier dans lequel l'erreur a �t� identifi�e
  * $errcontext : tableau contenant les variables et leurs valeurs au moment de l'erreur
  */
function offipse_error_handler($errno, $errstr, $errfile, $errline, $errcontext = Array())
{
    global $root_path, $ERROR_technical, $ERROR_show_php_strict;

    $errdesc = $errno;

    // Niveau d'erreur
    switch ($errno)
    {
        case E_ERROR:
            $errdesc = "E_ERROR";
            break;
        case E_WARNING:
            $errdesc = "E_WARNING";
            break;
        case E_PARSE:
            $errdesc = "E_PARSE";
            break;
        case E_NOTICE:
            $errdesc = "E_NOTICE";
            break;
        case E_CORE_ERROR:
            $errdesc = "E_CORE_ERROR";
            break;
        case E_CORE_WARNING:
            $errdesc = "E_CORE_WARNING";
            break;
        case E_COMPILE_ERROR:
            $errdesc = "E_COMPILE_ERROR";
            break;
        case E_COMPILE_WARNING:
            $errdesc = "E_COMPILE_WARNING";
            break;
        case E_USER_ERROR:
            $errdesc = "E_USER_ERROR";
            break;
        case E_USER_WARNING:
            $errdesc = "E_USER_WARNING";
            break;
        case E_USER_NOTICE:
            $errdesc = "E_USER_NOTICE";
            break;
        case E_ALL:
            $errdesc = "E_ALL";
            break;
        case E_STRICT:
            $errdesc = "E_STRICT";
            break;
        case E_RECOVERABLE_ERROR:
            $errdesc = "E_RECOVERABLE_ERROR";
            break;
        case E_DEPRECATED:
           $errdesc = "E_DEPRECATED";
           break;

    }

    // Est-ce que l'on passe l'erreur ?
    //Modification pour gestion des fonctions d�precier en php 5.3
    //if ($errno != E_NOTICE && ($ERROR_show_php_strict || $errno != E_STRICT))
    if ($errno != E_NOTICE && ($ERROR_show_php_strict || $errno != E_STRICT ) && $errno != E_DEPRECATED  && $errno != E_WARNING  )
    {
        if($ERROR_technical)
        {
?>
                <table bgcolor="#FFFFFF" cellpadding="1" cellspacing="0" border="0" width="600" align="center">
                <tr><td>
                <table border="0">
                    <tr>
                        <td colspan="2"><h3 style="color: red;"><?php  echo ("Erreur d'exécution") ?></h3></td>
                    </tr>
                    <tr>
                        <td><p><b><?php  echo ("Erreur de niveau") ?></b></p></td>
                        <td><p><?php  echo $errdesc ?></p></td>
                    </tr>
                    <tr>
                        <td><p><b><?php  echo ("Description") ?></b></p></td>
                        <td><p><?php  echo $errstr ?></p></td>
                    </tr>
                     <tr>
                        <td><p><b><?php  echo ("Fichier") ?></b></p></td>
                        <td><p><?php  echo $errfile ?></p></td>
                    </tr>
                    <tr>
                        <td><p><b><?php  echo ("Ligne") ?></b></p></td>
                        <td><p><?php  echo $errline ?></p></td>
                    </tr>
                    <tr>
                        <td><p><b><?php  echo ("Pile d'erreur") ?></b></p></td>
                        <td><pre><?php  debug_print_backtrace(); ?></pre></td>
                    </tr>
                </table>
                </td></tr>
                </table>
<?php 
        }
        else
        {
?>
                <h3 style="color: red;"><?php  echo ("Erreur d'exécution") ?></h3>
                <p><?php  echo ("Une erreur techniques s'est produite.") ?></p>
                <p><?php  echo ("Veuillez avertir l'adrministrateur de la plateforme") ?></p>
<?php 
        }
    }

    return true;
}

$old_error_handler = set_error_handler("offipse_error_handler");