﻿<?php 
/**
 * @class dbdump
 *
 */
class dbdump
{
	/**
	@function _getDCTablesList

	Création d'un tableau contenant la listes des tables c_* à sauvegarder.

	@return	array
	*/
	function _getTableList()
	{
		// Récupération auprès de MySQL de la liste des tables préfixées
		// par c_*
		$res = dbQueryAll("SHOW TABLES LIKE 'c_%'");

		// Maintenant on consolide le tout...
		$cpt = 0;
		foreach ($res as $table)
		{
			// On sort la table de log
			if (true)
			//if ($table[0] != "c_services_line")
			//if ($table[0] != "c_error_log")
			{
				$table_list[] = $table[0];
				//$cpt++;
			}
			//if ($cpt == 60) break;
		}

		return($table_list);
	}


	/**
	 @function getDump

	 Effectue un dump des tables offiPSe sous forme d'un fichier SQL standard.

	 L'export se compose pour chacune des tables :
		- d'une instruction conditionnelle DROP TABLE
		- de la clause complète CREATE TABLE (clés comprises)
		- des clauses INSERT nécessaires à la restauration des données.

	 NB : Les noms des colonnes sont "backquotés".

	 @return string La chaine représentant le dump.
	 */
	function getDump()
	{
		global $version, $DATABASE_host, $DATABASE_name;
		$c_tables = dbdump::_getTableList();

		// On précise à MySQL qu'il doit "quoter" les résultats des
		// requêtes SHOW CREATE TABLE qui vont suivre.
		dbQuery("SET SQL_QUOTE_SHOW_CREATE = 1");

		// Récupération de la version du serveur MySQL
		$rs = dbQueryOne("SELECT REPLACE(VERSION(),'-log','') as version");
		$mysql_version = 'unknown';
		if ($rs) $mysql_version = $rs['version'];

		// Une entête pour mettre quelques infos (in)utiles
		$dump_str =
			"#<" . $version .">\n".
			"#-------------------------------------------------------------------\n".
			"# Dump offiPSe MySQL\n".
			"#\n".
			"# Version	: ". $version ."\n".
			"# Serveur	: ". $DATABASE_host ." (MySQL v ". $mysql_version .")\n".
			"# Base		: ". $DATABASE_name ."\n".
			"# Date		: ". date("d/m/Y H:i") ."\n".
			"#-------------------------------------------------------------------\n".
			"\n";

		// C'est parti !
		foreach ($c_tables as $c_table)
		{
			$rs = dbQueryOne("SHOW CREATE TABLE $c_table");

			//echo "<p>" . $rs[1] . "</p>";

			// Dump de la clause CREATE TABLE
			$dump_str .=
				"#-------------------------------------------------------------------\n".
				"# Structure de la table $c_table\n".
				"#-------------------------------------------------------------------\n".
				"DROP TABLE IF EXISTS `$c_table`;\n".
				str_replace("\n","",$rs[1]).";".
				"\n\n";

			// Dump des clauses INSERT
			//if ($c_table != "c_services_line")
			$dump_str .=
					"#-------------------------------------------------------------------\n".
					"# Données de la table $c_table\n".
					"#-------------------------------------------------------------------\n";
			$res = dbQuery("SELECT * FROM $c_table");
			while ($line = mysql_fetch_array($res, MYSQL_NUM))
			{
				$values = array();
				foreach ($line as $col)
				{
					$values[] = dbEscape($col);
				}
				$dump_str .= "INSERT INTO `$c_table` VALUES ( '". implode("', '",$values) ."' );\n";
				//$dump_str .= "INSERT INTO `$c_table` VALUES ( '". implode("', '",$line) ."' );\n";
			}
			$dump_str .= "\n";
		}
		$dump_str .= "#-- EOF --\n";

		return($dump_str);
	}


	/**
	 @function saveDump

	 Enregistre un fichier de dump des tables
	 Avec les paramètres par défaut, un fichier gzippé dump-YYYYMMDD.sql.gz est
	 créé dans le répertoire share/mysql/ de l'installation.

	 @param	boolean	transmit	indicateur d'envoi du fichier de dump (false)
	 @param string	path		le chemin d'enregistrement ('')
	 @param string	name		le nom du fichier cible ('')
	 @param boolean	compressed	indicateur de compression ou non (true)

	 @return mixed Retourne une chaine représentant le nom du fichier dans lequel
				   le dump a été sauvegardé. Retourne un boolean à false en cas
				   de problème sinon.

	 @see dbdump::getDump
	 */
	function saveDump($transmit = false, $path = '', $name = '', $compressed = true)
	{
		global $DATABASE_host, $version;

		if ($compressed && !function_exists('gzencode'))
		{
			return(false);
		}

		if (!empty($path) && is_dir($path) && is_writable($path))
		{
			$trg_path = $path;
		}
		else
		{
			$trg_path = DC_SHARE_DIR.'/mysql';
		}

		if (!empty($name))
		{
			$trg_name = $name;
		}
		else
		{
			$trg_name = 'offipse-v' . $version . '-' . $DATABASE_host . '-'.date("Ymd").'.sql';
		}

		$dump = dbdump::getDump();
		//echo "taille : " . strlen($dump);

		$content_type = "text/plain";
		if ($compressed)
		{
			$dump = gzencode($dump);
			$trg_name .= '.gz';
			$content_type = "application/x-gzip";
		}

		if ($transmit)
		{
			header('Content-Type: ' . $content_type);
			header('Content-Disposition: attachment; filename='.$trg_name);
			echo $dump;
		}
		else
		{
			$trg_fullname = $trg_path.'/'.$trg_name;
			if ($fh = fopen($trg_fullname,"wb"))
			{
				fwrite($fh, $dump);
				fclose($fh);
			}
			else
			{
				return(false);
			}
			return($trg_name);
		}
	}


	/**
	 @function restoreDump

	 Restaure des tables depuis un fichier de dump précédemment créé
	 par l'opération de sauvegarde.

	 @param string	src_file	le chemin du fichier de dump à restaurer
	 @param boolean	compressed	indicateur de compression ou non (true)

	 @return mixed Retourne une chaine indiquant que l'opération s'est correctement
				   déroulée. Retourne un boolean à false en cas de problème sinon.

	 @see dbdump::saveDump
	 */
	function restoreDump($src_file, $compressed = true)
	{
		global $version, $TMP_dir;

		// Pour un fichier compressé, on crée un version décompressée
		// temporaire dans le répertoire temp
		$chrono_start = dbdump::_getChrono();
		if ($compressed)
		{
			if (!$fh = fopen($src_file, "rb"))
			{
				return(false);
			}

			$tmp_dump = fread($fh, filesize($src_file));
			fclose($fh);

			//if (!$dump = gzuncompress(substr($tmp_dump, 10)))
			if (!$dump = gzinflate(substr($tmp_dump, 10)))
			{
				return(false);
			}

			unset($tmp_dump);
			$tmp_file = $TMP_dir . '/_'.basename($src_file);

			if (!$fh = fopen($tmp_file, "wb"))
			{
				return(false);
			}

			fwrite($fh, $dump);
			fclose($fh);
			unlink($src_file);
			unset($dump);
			$src_file = $tmp_file;
		}


		// Parsing "simpliste" du fichier dump pour la remontée en base.
		// Seules les requêtes DROP TABLE, CREATE TABLE et INSERT seront
		// traitées.
		$scan = file($src_file);
		$token = $sql = '';
		$first = true;
		foreach ($scan as $line)
		{
			// Premiere, ligne on doit trouver le numéro de version
			// A comparer à celle de l'installation
			if ($first)
			{
				$first = false;
				echo "<p>$line</p>";
				// BOn format ?
				if (preg_match("/#<[1-9]\.[1-9]{1,3}.+>/",$line,$myver))
				{
					// Version
					//print_r($myver);
					$strver = $myver[0];
					$strver = str_replace("<","",$strver);
					$strver = str_replace(">","",$strver);
					$strver = str_replace("#","",$strver);
					
					// Est-ce que la version de la base est la même ?
					if ($strver != $version)
					{
						return sprintf(("offiPSe version %s, dump version %s, impossible de restaurer la base"),$version,$strver);
					}
				}
				else
				{
					// Dump au mauvais format
					return ("Impossible de détecter la version d'offiPSe du dump");
				}
			}

			$line = rtrim($line);
			if (empty($line) || strpos(ltrim($line), "#") === 0) continue;
			$token .= $line;

			if (preg_match("/^((DROP|CREATE) TABLE .*);$/", $token, $matches) ||
				preg_match("/^(INSERT INTO (.*)'\s\));$/", $token, $matches))
				{
				$sql = $matches[1];
				if (!empty($sql))
				{
					$res = dbQuery($sql);
					if ($res === false)
					{
						unlink($src_file);
						return(false);
					}
				}
				$token = $sql = '';
			}
		}

		unlink($src_file);
		$chrono_end = dbdump::_getChrono();
		//buffer::str(sprintf("(%.2f sec.)",$chrono_end - $chrono_start));
		return(true);
	}


	/**
	@function _getChrono

	@return	float
	*/
	function _getChrono()
	{
	   list($usec, $sec) = explode(" ", microtime());
	   return ((float)$usec + (float)$sec);
	}

	function getTableInfos()
	{
		# Les tables de la base de données
		$res = dbQueryAll("SHOW TABLE STATUS LIKE 'c_%'");
	
		$infos = Array();
		foreach($res as $table)
		{
			//$size = size($table['Data_length'] + $table['Index_length']);
			$size = $table['Data_length'] + $table['Index_length'];
			array_push($infos, Array("name" => $table['Name'], "rows" => $table['Rows'], "size" => $size,"sizesort" => ($table['Data_length'] + $table['Index_length'])));
		}
	
		return $infos;
	}
}

?>
