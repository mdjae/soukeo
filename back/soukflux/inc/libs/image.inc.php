﻿<?php 
/*
 * Produce
 *
 */
function createThumb($name,$thumbname, $new_width, $new_height = 0, $keep_aspect_ratio = false)
{
	// Type d'image
	list($old_x, $old_y, $type) = getimagesize($name);
	$extension = image_type_to_extension($type);

	if (preg_match('/jpg|jpeg/',$extension))
	{
		$src_img = imagecreatefromjpeg($name);
	}
	else if (preg_match('/png/',$extension))
	{
		$src_img = imagecreatefrompng($name);
	}
	else if (preg_match('/gif/',$extension))
	{
		$src_img = imagecreatefromgif($name);
	}
	else
	{
		return false;
	}


	// Non conservsation du ratio
	if ($keep_aspect_ratio)
	{
		$thumb_w = $new_width;
		$thumb_h = $new_height;
	}
	else
	{
		// Conservation du ratio

		// Le coté qui sera dépendant de l'autre est à 0
		if ($old_y > $old_x)
		{
			// Calcul de la largeur
			$thumb_w = $old_x * $new_height / $old_y;
			$thumb_h = $new_height;
		}
		else
		{
			// Calcul de la hauteur
			$thumb_w = $new_width;
			$thumb_h = $old_y * $new_width / $old_x;
		}
	}
	
	$dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
	
		
	if (preg_match("/png/",$extension))
	{
		imagepng($dst_img,$thumbname); 
	}
	else if (preg_match('/jpg|jpeg/',$extension))
	{
		imagejpeg($dst_img,$thumbname); 
	}
	else if (preg_match('/gif/',$extension))
	{
		imagegif($dst_img,$thumbname);
	}

	imagedestroy($dst_img); 
	imagedestroy($src_img); 

	return Array($thumb_w, $thumb_h);
}
?>
