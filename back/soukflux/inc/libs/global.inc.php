<?php 
/**************************/
/*** FONCTIONS GLOBALES ***/
/**************************/

// Output: the number of seconds between 2 dates ($d2-$d1)
function dateDifference($d1,$d2)
{
	$date_tab1 = split(" |-|:",$d1);
	$new_time1 = mktime($date_tab1[3],$date_tab1[4],$date_tab1[5],$date_tab1[1],$date_tab1[2],$date_tab1[0]);
	$date_tab2 = split(" |-|:",$d2);
	$new_time2 = mktime($date_tab2[3],$date_tab2[4],$date_tab2[5],$date_tab2[1],$date_tab2[2],$date_tab2[0]);
	//
	return ($new_time2-$new_time1);
}

// Output: the number of days between 2 dates ($d2-$d1)
function daysDifference($d1,$d2)
{
	$date_tab1 = split(" |-|:",$d1);
	$new_time1 = mktime($date_tab1[3],$date_tab1[4],$date_tab1[5],$date_tab1[1],$date_tab1[2],$date_tab1[0]);
	$date_tab2 = split(" |-|:",$d2);
	$new_time2 = mktime($date_tab2[3],$date_tab2[4],$date_tab2[5],$date_tab2[1],$date_tab2[2],$date_tab2[0]);
	//
	return (ceil(($new_time2-$new_time1)/3600/24));
}

// -- Months
$GLOBAL_time_tab = array
(
	("&agrave;"),
	("janvier"),
	("février"),
	("mars"),
	("avril"),
	("mai"),
	("juin"),
	("juillet"),
	("ao&ucirc;t"),
	("septembre"),
	("octobre"),
	("novembre"),
	("décembre")
);

// Output: a formatted date
function smartDate($db_date)
{
	global $GLOBAL_time_tab,$time_zone_txt;

	// yyyy-mm-dd hh:ii-ss
	//	0	1  2  3  4	5
	$date_tab = split(" |-|:",$db_date);

	if ($_SESSION['lang'] == "en_EN")
	{
		$output_date = $GLOBAL_time_tab[(int)$date_tab[1]].", ".$date_tab[2]." ".$date_tab[0];
		if ($date_tab[3]>12)
		{
			$time_zone_txt = "pm";
			$date_tab[3] = (int)$date_tab[3]-12;
		}
		else { $time_zone_txt = "am"; }
	}
	else
	{
		$output_date = $date_tab[2]." ".$GLOBAL_time_tab[(int)$date_tab[1]]." ".$date_tab[0];
	}
	$output_date .= " ".$GLOBAL_time_tab[0]." ".$date_tab[3].":".$date_tab[4].":".$date_tab[5]." ".$time_zone_txt;

	return $output_date;
}

// Output: a formatted date
function smartDateNoTime($db_date)
{
	global $GLOBAL_time_tab;

	if ($db_date != "")
	{
		// yyyy-mm-dd hh:ii-ss
		//	0	1  2  3  4	5
		$date_tab = split(" |-|:",$db_date);
	
		if ($_SESSION['lang'] == "en_EN")
		{
			$output_date = $GLOBAL_time_tab[(int)$date_tab[1]].", ".$date_tab[2]." ".$date_tab[0];
		}
		else
		{
			$output_date = $date_tab[2]." ".$GLOBAL_time_tab[(int)$date_tab[1]]." ".$date_tab[0];
		}
	}
	else $output_date = "";

	return $output_date;
}

//
function writeNormalDate($d1)
{
	if ($d1 != "")
	{
		// yyyy-mm-dd hh:ii-ss
		$date_tab = split(" |-|:",$d1);
		
		// dd/mm/yyyy
		return $date_tab[2].'/'.$date_tab[1].'/'.$date_tab[0];
	}
	else return "";
}

// Write yyyy-mm-dd hh:ii-ss from dd/mm/yyyy
function dbdate($d1)
{
	if (($d1) != "")
	{
		$date_tab = split("/",$d1);
		$str = $date_tab[2].'-'.$date_tab[1].'-'.$date_tab[0].' 00:00:00';
	}
	else
	{
		$str = "";
	}

	return $str;
}

// Checks if a variable is empty
// Output: BOOLEAN
function isEmpty($my_string)
{
	if ( trim($my_string)=="" )
	{
		return true;
	}
	return false;
}

// Checks if a string is alphanumeric
// Output: BOOLEAN
function isAlphanumeric($my_string) {
	if ( ereg("^[A-Za-z0-9]",trim($my_string)) ) {
		return true;
	}
	return false;
}

// Checks if a string is alphabetic
// Output: BOOLEAN
function isAlphabetic($my_string) {
	if ( ereg("^[A-Za-z]",trim($my_string)) ) {
		return true;
	}
	return false; 
}

// Checks if a string is numeric
// Output: BOOLEAN
function isNumeric($my_string) {
	if ( is_numeric($my_string) ) {
		return true;
	}
	return false; 
}

// Checks if a string is a french date (jj/mm/aaaa)
// Output: BOOLEAN
function isFrenchDate($my_string) {
	if (strlen($my_string)==10) {
		if ( isNumeric(substr($my_string,0,2)) && isNumeric(substr($my_string,3,2)) && isNumeric(substr($my_string,6,4)) ) {
			if ( (substr($my_string,2,1)=='/') && (substr($my_string,5,1)=='/') ) {
				if ( ( substr($my_string,0,2)<32 ) && ( substr($my_string,0,2)>0 ) ) {
					if ( ( substr($my_string,3,2)<13 ) && ( substr($my_string,3,2)>0 ) ) {
						if ( ( substr($my_string,6,4)<2025 ) && ( substr($my_string,6,4)>2007 ) ) {
							return true;
						}
					}
				}
			}
		}
	}
	return false; 
}

// Output: a decimal number with a separator '.' or ','
function numberByLang($number)
{
	if ($_SESSION['lang'] == "en_EN")
	{
		return $number;
	}
	else
	{
		return number_format($number, 2, ",", " ");
	}
}


// Fonction qui calcule la diff�rence entre 2 temps pr�cis au milli�me de seconde
function DiffTime($microtime1, $microtime2)
{
	// on s�pare les secondes et les millisecondes
	list($micro1, $time1) = explode(' ', $microtime1);
	list($micro2, $time2) = explode(' ', $microtime2);
	// on calule le nombre de secondes qui s�parent les 2
	$time = $time2 - $time1;
	// on calcule les fractions de secondes qui s�parent les 2
	if ($micro1 > $micro2)
	{
		// si le nombre de millisecondes du 2� temps est sup�rieur au 1�, on a compt� une seconde de trop
		$time--;
		$micro = 1 + $micro2 - $micro1;
	}
	else
	{
		// sinon, on fait juste la diff�rence
		$micro = $micro2 - $micro1;
	}
	// A la fin, on ajoute les secondes et les millisecondes
	$micro += $time;
	// Et on renvoie le tout
	return $micro;
} 

/*
 * Recup la liste des jours f�ri�s au format SQL prete � �tre utilis�s
 * dans un IN SQL
 */
function getSQLFeriesDays($year = 0)
{
	$output = '';
	$first = true;
	
	$ferieDays = getFeriesDaysArray($year);

	foreach($ferieDays as $day)
	{
		if ($first) $first = false;
		else $output .= ",";
		$output .= "'" . dbDate($day) . "'";
	}

	if (count($ferieDays) == 0)
	{
		$output = "''";
	}

	return $output;
}

function addDays2Date($idate, $idays) {
  $date_tab = split(" |-|:",$idate);
	$new_time = mktime($date_tab[3],$date_tab[4],$date_tab[5],$date_tab[1],$date_tab[2],$date_tab[0]);
	
	$new_time = $new_time+($idays*24*3600);
	
	return date("Y-m-d H:i:s",$new_time);
}

function size($size)
{
	$kb = 1024;
	$mb = 1024 * $kb;
	$gb = 1024 * $mb;
	$tb = 1024 * $gb;

	if($size < $kb)
	{
		return $size." B";
	}
	else if($size < $mb)
	{
		return round($size/$kb,2)." KB";
	}
	else if($size < $gb)
	{
		return round($size/$mb,2)." MB";
	}
	else if($size < $tb)
	{
		return round($size/$gb,2)." GB";
	}
	else
	{
		return round($size/$tb,2)." TB";
	}
}

/**
 * Affiche un popup
 *
 * @param $url Url de la page
 * @param $title Titre de la page
 * @param $width Largeur du popup
 * @param $height Hauteur du popup
 * @param $type smoothbox, window, oldschool ,jdiag
 */
function popupLink($url='', $title='', $width='', $height='', $type = "windoo" , $function = '')
{
	if ($type == "smoothbox")
	{
		if (strpos($url,"?") > 0) $sep = "&";
		else $sep = "?";
		return "<a href=\"" . $url . $sep . "keepThis=true&TB_iframe=true&height=" . $height . "&width=" . $width . "\" title=\"" . $title . "\" class=\"smoothbox\">";
	}
	/*
	else if ($type == "windoo")
	{
		return '<a href="#" onclick="openWindoo(\'' . $url . '\',\'' . addslashes($title) . '\',' . $width . ',' . $height . '); return false;">';
	}
	else if ($type == "mocha")
	*/
	else if ($type == "windoo")
	{
		return '<a href="#" onclick="openMocha(\'' . $url . '\',\'' . addslashes($title) . '\',' . $width . ',' . $height . '); return false;">';
	}
	else if ($type == "Jajax"){
		return '<a href="#" onclick="'.$function.'(); return false;">';
		
	}else if($type =="jdiag"){
		return '<a href="#" onclick="showDialog(\''.$url.'\',\''.$height.'\',\''.$width.'\'); return false;">';
	}	
	else
	{
		$name = "WIN";
		return '<a href="#" onclick="openWindow(\'' . $url . '\',\'' . $name . '\',\'scrollbars=yes\',\'' . $width . '\',\'' . $height . '\',\'true\'); return false;">';
	}
}
function stripAccents($texte) {
	$texte = str_replace(
		array(
			'à', 'â', 'ä', 'á', 'ã', 'å',
			'î', 'ï', 'ì', 'í', 
			'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
			'ù', 'û', 'ü', 'ú', 
			'é', 'è', 'ê', 'ë', 
			'ç', 'ÿ', 'ñ',
			'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
			'Î', 'Ï', 'Ì', 'Í', 
			'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
			'Ù', 'Û', 'Ü', 'Ú', 
			'É', 'È', 'Ê', 'Ë', 
			'Ç', 'Ÿ', 'Ñ', '"' 
		),
		array(
			'a', 'a', 'a', 'a', 'a', 'a', 
			'i', 'i', 'i', 'i', 
			'o', 'o', 'o', 'o', 'o', 'o', 
			'u', 'u', 'u', 'u', 
			'e', 'e', 'e', 'e', 
			'c', 'y', 'n', 
			'A', 'A', 'A', 'A', 'A', 'A', 
			'I', 'I', 'I', 'I', 
			'O', 'O', 'O', 'O', 'O', 'O', 
			'U', 'U', 'U', 'U', 
			'E', 'E', 'E', 'E', 
			'C', 'Y', 'N', '\"' 
		),$texte);
	return $texte;
}

function truncateStr($string, $length)
{
	$result = "";
	strlen($string) > $length ? $result = substr($string, 0, $length -3). "..." : $result = $string;
	return $result;
}

function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$zip->addFile($file,$file);
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}

function getLabelStatus($status)
{
	switch ($status) {
		case 'complete':
			$class = "success";
		break;
		case 'processing':
			$class = "info";
		break;
		case 'holded':
			$class = "warning";
		break;
		case 'closed':
			$class = "important";
		break;				
		default:
		break;
	}

	return "<span class='label label-$class'>$status</span>";	
}

function getLabelAbonnement($id)
{
    $managerAbo = new ManagerAbonnement();
    switch ($id) {
        case '1':
            $class = "brass";
            $name = $managerAbo -> getBy(array("abonnement_id" => $id));
        break;
        
        case '2':
            $class = "silver";
            $name = $managerAbo -> getBy(array("abonnement_id" => $id));
        break;
        
        case '3':
            $class = "gold";
            $name = $managerAbo -> getBy(array("abonnement_id" => $id));
        break;

        case '4':
            $class = "metalblue";
            $name = $managerAbo -> getBy(array("abonnement_id" => $id));
        break;
                        
        default:
        break;
    }
    
    return "<span class='label label-$class'><strong>$name</strong></span>";
}

function getLabelPayment($state, $id_facture)
{
    switch ($state) {
        case 'unpaid':
            $class = "danger";
        break;
        
        case 'partiel':
            $class = "info";
        break;
        
        case 'complete':
            $class = "success";
        break;
                
        default:
        break;
    }
    
    return "<span id='payment_".$id_facture."' class='label label-$class'><strong>$state</strong></span>";
}

function getBtnAbonnement($id, $vendor_id)
{
    $managerAbo = new ManagerAbonnement();
    $out = '<div id="abo_'.$vendor_id.'" class="btn-group pull-right" style="width:100%">';
    switch ($id) {
        case '1':
            $name = $managerAbo -> getBy(array("abonnement_id" => $id))->getLabel();
            $out .= '<a class="btn dropdown-toggle btn-brass" data-toggle="dropdown" href="#" >
                        '.ucfirst($name).'
                        <span class="caret"></span>
                    </a>';
        break;
        
        case '2':
            $name = $managerAbo -> getBy(array("abonnement_id" => $id))->getLabel();
            $out .= '<a class="btn dropdown-toggle btn-silver" data-toggle="dropdown" href="#">
                        '.ucfirst($name).'
                        <span class="caret"></span>
                    </a>';
        break;
        
        case '3':
            $name = $managerAbo -> getBy(array("abonnement_id" => $id))->getLabel();
            $out .= '<a class="btn dropdown-toggle btn-gold" data-toggle="dropdown" href="#">
                        '.ucfirst($name).'
                        <span class="caret"></span>
                    </a>';
        break;
        
        case '4':
            $name = $managerAbo -> getBy(array("abonnement_id" => $id))->getLabel();
            $out .= '<a class="btn dropdown-toggle btn-metalblue" data-toggle="dropdown" href="#">
                        '.ucfirst($name).'
                        <span class="caret"></span>
                    </a>';
        break;
                
        default:
        break;
    }
    
    $out .= '   <ul class="dropdown-menu">
                    <li><a href="#" onclick="changeTypeAbo(\''.$vendor_id.'\', 1); return false;">'.ucfirst($managerAbo -> getBy(array("abonnement_id" => 1))->getLabel()).'</a></li>
                    <li><a href="#" onclick="changeTypeAbo(\''.$vendor_id.'\', 2); return false;">'.ucfirst($managerAbo -> getBy(array("abonnement_id" => 2))->getLabel()).'</a></li>
                    <li><a href="#" onclick="changeTypeAbo(\''.$vendor_id.'\', 3); return false;">'.ucfirst($managerAbo -> getBy(array("abonnement_id" => 3))->getLabel()).'</a></li>
                    <li><a href="#" onclick="changeTypeAbo(\''.$vendor_id.'\', 4); return false;">'.ucfirst($managerAbo -> getBy(array("abonnement_id" => 4))->getLabel()).'</a></li>
                </ul>
            </div>';
    
    return $out;
}

function getBtnStateTicket($ticket_state, $ticket_id)
{
    switch ($ticket_state) {
        
        case 'nouveau':
            $class = "btn-inverse";    
        break;
        case 'en cours':
            $class = "btn-info";    
        break;
        case 'terminé':
            $class = "btn-success"; 
        break;
        case 'annulé':
            $class = "btn-danger";  
        break;
        default:
            
        break;
    }
    
    $html = '<div id="ticketstate_'.$ticket_id.'" class="btn-group">
                <a class="btn dropdown-toggle '.$class.'" style="color:white" data-toggle="dropdown" href="#">
                    '.ucfirst($ticket_state).'
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="changeStateTicket(\''.$ticket_id.'\', \'Nouveau\'); return false;">Nouveau</a></li>
                    <li><a href="#" onclick="changeStateTicket(\''.$ticket_id.'\', \'En cours\'); return false;">En cours</a></li>
                    <li><a href="#" onclick="changeStateTicket(\''.$ticket_id.'\', \'Terminé\'); return false;">Terminé</a></li>
                    <li><a href="#" onclick="changeStateTicket(\''.$ticket_id.'\', \'Annulé\'); return false;">Annulé</a></li>
                </ul>
             </div>';
        return $html;
}

function getBtnPriorityTicket($ticket_priority, $ticket_id)
{
    switch ($ticket_priority) {
        case 'basse':
            $class = "btn-inverse";    
        break;
        case 'moyenne':
            $class = "btn-warning";    
        break;
        case 'haute':
            $class = "btn-danger"; 
        break;
        default:
            
        break;
    }
    
    $html = '<div id="ticketpriority_'.$ticket_id.'" class="btn-group">
                <a class="btn dropdown-toggle '.$class.'" style="color:white" data-toggle="dropdown" href="#">
                    '.ucfirst($ticket_priority).'
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="changePriorityTicket(\''.$ticket_id.'\', \'Basse\'); return false;">Basse</a></li>
                    <li><a href="#" onclick="changePriorityTicket(\''.$ticket_id.'\', \'Moyenne\'); return false;">Moyenne</a></li>
                    <li><a href="#" onclick="changePriorityTicket(\''.$ticket_id.'\', \'Haute\'); return false;">Haute</a></li>
                </ul>
             </div>';
        return $html;
}

function getActionRelanceDropdown($relance_id, $date_termine='')
{
    if($date_termine){
        $class = "btn-success";
        $text = "Terminé";
    }else{
        $class = "btn-info";
        $text = "En cours";
    }

        
	$html = '<div id="relanceaction_'.$relance_id.'" class="btn-group">
                <a class="btn dropdown-toggle '.$class.'" style="color:white" data-toggle="dropdown" href="#">
                    '.$text.'
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="setRelanceTermine(\''.$relance_id.'\', \'Terminé\'); return false;">Terminé</a></li>
                    <li><a href="#" onclick="setRelanceTermine(\''.$relance_id.'\', \'En cours\'); return false;">En cours</a></li>
                </ul>
             </div>';
    
    return $html;         
}

function getLabelPriorityTicket($ticket_priority)
{
    switch ($ticket_priority) {
        case 'basse':
            $class = "label-inverse";    
        break;
        case 'moyenne':
            $class = "label-warning";    
        break;
        case 'haute':
            $class = "label-important"; 
        break;
        default:
            
        break;
    }
    
    $html = '<span class="label '.$class.'">'.ucfirst($ticket_priority).'</span>';
        return $html;
}

function getLabelStateTicket($ticket_state)
{
    switch (trim(strtolower($ticket_state))) {
        
        case 'nouveau':
            $class = "label-inverse";    
        break;
        case 'en cours':
            $class = "label-info";    
        break;
        case 'terminé':
            $class = "label-success"; 
        break;
        case 'annulé':
            $class = "label-important";  
        break;
        default:
            
        break;
    }
    
    $html = '<span class="label '.$class.'">'.ucfirst($ticket_state).'</span>';
    return $html;
}

function getLabelActiveProdGr($active)
{
    switch ($active) {
        
        case 1 :
            $class = "label-info"; 
            $text = "Actif";   
        break;

        case 0 :
            $class = "label-important";
            $text = "Inactif"; 
        break;

            
        break;
    }
    
    $html = '<span class="label '.$class.'">'.$text.'</span>';
    return $html;
}

function countryName($abr)
{
    switch ($abr) {
        case 'FR':
            return 'FR';
        break;
        
        case 'MU':
            return 'MAURICE';
        break;
        
        case 'YT':
            return 'MAYOTTE';
        break;
        
        default:
            return $abr;
        break;
    }
}

function zerofill ($num, $zerofill = 5)
{
	return str_pad($num, $zerofill, '0', STR_PAD_LEFT);

}

function getDropDownButton($order_status, $order_id)
{
    
    switch ($order_status) {
        case 'new':
            $class = "btn-info";    
        break;
        case 'completed':
            $class = "btn-success"; 
        break;
        case 'canceled':
            $class = "btn-danger";  
        break;
        default:
            
        break;
    }
    
    $html = '<div id="status_'.$order_id.'" class="btn-group">
                <a class="btn dropdown-toggle '.$class.'" style="color:white" data-toggle="dropdown" href="#">
                    '.ucfirst($order_status).'
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" onclick="changeStatusOrder(\''.$order_id.'\', \'New\'); return false">New</a></li>
                    <li><a href="#" onclick="changeStatusOrder(\''.$order_id.'\', \'Completed\'); return false">Completed</a></li>
                    <li><a href="#" onclick="changeStatusOrder(\''.$order_id.'\', \'Canceled\'); return false">Canceled</a></li>
                </ul>
             </div>';
    return $html;
}

function getPoStateShipping($state)
{

    switch ($state) {
        case 0:
            $msg = "PENDING";
            $text = "Attente";
            $class = "label-inverse"; 
        break;

        case 1:
            $msg = "SHIPPED";
            $text = "Expedié";
            $class = "label-info"; 
        break;

        case 2:
            $msg = "PARTIAL";
            $text = "Partiel";
            $class = "label-warning"; 
        break;
            
        case 3:
            $msg = "READY";
            $text = "Prêt à expedier";
            $class = "label-info";
        break;
            
        case 4:
            $msg = "ONHOLD";
            $text = "En attente";
            $class = "label-warning";
        break;
            
        case 5:
            $msg = "BACKORDER";
            $text = "Renvoyé";
            $class = "label-important"; 
        break;
            
        case 6:
            $msg = "CANCELED";
            $text = "Annulé";
            $class = "label-important";
        break;
            
        case 7:
            $msg = "DELIVERED";
            $text = "Livré";
            $class = "label-success";
        break;
            
        case 8:
            $msg = "PENDPICKUP";
            $text = "Attente de récupération";
            $class = "label-info";
        break;
            
        case 9:
            $msg = "ACK";
            $text = "ACK";
            $class = "label-inverse";
        break;
            
        case 10:
            $msg = "EXPORTED";
            $text = "Exporté";
            $class = "label-info";
        break;
        
        case 11:
            $msg = "RETURNED";
            $text = "Renvoyé";
            $class = "label-warning";
        break;                                                                                            
        default:
            
            break;
    }

    $html = '<span class="label '.$class.'">'.$text.'</span>';
    return $html;
    
}

    
function getSelectReferentiel($entity, $selected = "")
{
    $class= "Manager".$entity;
    $manager = new $class();
    
    $list = $manager->getAll();
    
    $id = $manager->getId();
    $id = array_keys($id);
    $id = $id[0];
    
    $func = "get".ucfirst($id);
    
    $html = "<select id='select_".strtolower($entity)."' >";
    foreach ($list as $item) {
        
        $html .= "<option value='".$item->$func()."' ";
        
        if($selected == $item->$func())
            $html .= " selected ";
        
        $html .= " >".$item->getLabel()."</option>";
        
    }
    $html .= "</select>";
    
    return $html;
}

function curl_file_get_contents($url)
{
    $curl = curl_init();
    $userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.13) Gecko/20101203 AlexaToolbar/alxf-1.54 Firefox/3.6.13 GTB7.1";
 
    curl_setopt($curl,CURLOPT_URL,$url); //L'URL à parser. Pourrait être mis au niveau de curl_init lors de l'ouverture de la session.
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE); //TRUE pour retourner en String la valeur de curl_exec() au lieu de l'afficher directement.
    curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,SystemParams::getParam('system*plugin_curl_timeout')); //Le nombre de secondes à attendre en essayant de se connecter.  
 
    curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //Le contenu de "User_Agent:" l'header qui sera utilisé dans la requete HTTP
    
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE); //Pour suivre toutes les "Location: " header que le serveur lance dans le header http
    curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //Pour automatiquement mettre le champ Referer dans les requetes de redirection
    
    //curl_setopt($curl, CURLOPT_FAILONERROR, TRUE); //Echec 'silencieux' si le code HTTP retourné est supérieur ou égal à 400
    //curl_setopt($curl, CURLOPT_TIMEOUT, 10); //Le nombre maximum de soncondes aloués à cURL pour éxecuter la fonction    
 
    $contents = curl_exec($curl);
    curl_close($curl);
    
    return $contents;
}
    
 function cmpAsc($a, $b)
{
    if ($a->getCountProdsInCurrentCatWithThis() == $b->getCountProdsInCurrentCatWithThis()) {
        return 0;
    }
    return ($a->getCountProdsInCurrentCatWithThis() < $b->getCountProdsInCurrentCatWithThis()) ? -1 : 1;
}

function cmpDesc($a, $b)
{
    if ($a->getCountProdsInCurrentCatWithThis() == $b->getCountProdsInCurrentCatWithThis()) {
        return 0;
    }
    return ($a->getCountProdsInCurrentCatWithThis() > $b->getCountProdsInCurrentCatWithThis()) ? -1 : 1;
}

 function cmpAscValues($a, $b)
{
    if ($a['c'] == $b['c']) {
        return 0;
    }
    return ($a['c'] < $b['c']) ? -1 : 1;
}

function cmpDescValues($a, $b)
{
    if ($a['c'] == $b['c']) {
        return 0;
    }
    return ($a['c'] > $b['c']) ? -1 : 1;
}

?>
