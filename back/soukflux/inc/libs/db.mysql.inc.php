<?php

/**
 * PDO SINGLETON CLASS
 *
 * @author tpo
 */

/**
 * Creates a PDO instance representing a connection to a database and makes the instance
 * available as a singleton
 *
 * @param string $dsn The full DSN, eg: mysql:host=localhost;dbname=testdb
 * @param string $username The user name for the DSN string. This parameter is optional for
 * some PDO drivers.
 * @param string $password The password for the DSN string. This parameter is optional for
 * some PDO drivers.
 * @param array $driver_options A key=>value array of driver-specific connection options
 *
 * @return PDO
 */

class DB extends PDO
{
    public static function &instance()
    {
        global $DATABASE_host, $DATABASE_user, $DATABASE_user_passwd, $DATABASE_name;
        static $instance = null;
		

	
        if ($instance === null)
        {
            try
            {
                $instance = new self("mysql:host=$DATABASE_host;dbname=$DATABASE_name", $DATABASE_user, $DATABASE_user_passwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        
			}
            catch(PDOException $e)
            {
             //   throw new DBException($e -> getMessage());
             echo $e -> getMessage();
            }
        }
        return $instance;
    }

}




class DB2 extends PDO
{
    public static function &instance()
    {
        global $DATABASE_host2, $DATABASE_user2, $DATABASE_user_passwd2, $DATABASE_name2;
        static $instance = null;

        if ($instance === null)
        {
            try
            {
                $instance = new self("mysql:host=$DATABASE_host2;dbname=$DATABASE_name2", $DATABASE_user2, $DATABASE_user_passwd2, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        
			}
            catch(PDOException $e)
            {
                throw new DBException($e -> getMessage());
            }
        }
        return $instance;
    }

}

class sdb2 extends sdb {
	    static private $instance;
		
	    public function __construct()
    {
        if (!self::$instance)
        {
            $this -> db = DB2::instance();
        }
        return self::$instance;
    }	
}


class DBREC extends PDO
{
    public static function &instance()
    {
        global $DATABASE_host_rec, $DATABASE_user_rec, $DATABASE_user_passwd_rec, $DATABASE_name_rec;
        static $instance = null;

        if ($instance === null)
        {
            try
            {
                $instance = new self("mysql:host=$DATABASE_host_rec;dbname=$DATABASE_name_rec", $DATABASE_user_rec, $DATABASE_user_passwd_rec, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				var_dump($instance);
			}
            catch(PDOException $e)
            {
                throw new DBException($e -> getMessage());

            }
        }
        return $instance;
    }

}

class DBAVAHIS extends PDO
{
    public static function &instance()
    {
        global $DATABASE_host_avahis, $DATABASE_user_avahis, $DATABASE_user_passwd_avahis, $DATABASE_name_avahis;
        static $instance = null;

        if ($instance === null)
        {
            try
            {
                $instance = new self("mysql:host=$DATABASE_host_avahis;dbname=$DATABASE_name_avahis", $DATABASE_user_avahis, $DATABASE_user_passwd_avahis, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        
			}
            catch(PDOException $e)
            {
                throw new DBException($e -> getMessage());
            }
        }
        return $instance;
    }

}

class sdb_avahis extends sdb {
	    static private $instance;
		
	    public function __construct()
    {
        if (!self::$instance)
        {
            $this -> db = DBAVAHIS::instance();
        }
        return self::$instance;
    }	
}




class sdb_rec_avahis extends sdb {
	    static private $instance;
		//public $db;
	    public function __construct()
    {
        if (!self::$instance)
        {
            $this -> db = DBREC::instance();
			var_dump($this -> db);
        }

        return self::$instance;
    }	
	

}






class sdb
{

    static private $instance;
    //singleton instance
    public $db;

    public function __construct()
    {
        if (!self::$instance)
        {
            $this -> db = DB::instance();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }




    /**
     * Execute query and return all rows in assoc array
     *
     * @param string $statement
     * @return array
     */
    public function getAll($statement)
    {
        return ($res = $this -> db -> query($statement)) ? $res -> fetchAll(PDO::FETCH_ASSOC) : array();
    }
	
	public function getAllPrepa($statement)
    {
        return ($statement->execute() ) ? $statement-> fetchAll(PDO::FETCH_ASSOC) : array();
    }

    /**
     * Execute query and return one row in assoc array
     *
     * @param string $statement
     * @return array
     */
    public function getRow($statement)
    {
        return ($res = $this -> db -> query($statement)) ? $res -> fetch(PDO::FETCH_ASSOC) : array();
    }

    /**
     * Execute query and return one value only
     *
     * @param string $statement
     * @return mixed
     */
    public function getOne($statement)
    {
        $row = ($res = $this -> db -> query($statement)) ? $res -> fetch() : null;
        return $row ? $row[0] : null;
    }


	public function getOnePrep($statement)
    {
        $row = ( $statement -> execute()) ? $statement -> fetch() : null;
        return $row ? $row[0] : null;
    }
	
    /**
     * Initiates a transaction
     *
     * @return bool
     */
    public function beginTransaction()
    {
        return $this -> db -> beginTransaction();
    }

    /**
     * Commits a transaction
     *
     * @return bool
     */
    public function commit()
    {
        return $this -> db > commit();
    }

    /**
     * Rolls back a transaction
     *
     * @return bool
     */
    public function rollBack()
    {
        return $this -> db -> rollBack();
    }

    /**
     * Execute an SQL statement and return the number of affected rows
     *
     * @param string $statement
     */
    public function exec($statement)
    {
        return $this -> db -> exec($statement);
    }

    /**
     * Retrieve a database connection attribute
     *
     * @param int $attribute
     * @return mixed
     */
    public function getAttribute($attribute)
    {
        return $this -> db -> getAttribute($attribute);
    }

    /**
     * Return an array of available PDO drivers
     *
     * @return array
     */
    public function getAvailableDrivers()
    {
        return $this -> db > getAvailableDrivers();
    }

    /**
     * Returns the ID of the last inserted row or sequence value
     *
     * @param string $name Name of the sequence object from which the ID should be
     * returned.
     * @return string
     */
    public function lastInsertId($name='')
    {
    	if(!$name)
        	return $this -> db -> lastInsertId();
		else 
			return $this -> db -> lastInsertId($name);
    }

    /**
     * Prepares a statement for execution and returns a statement object
     *
     * @param string $statement A valid SQL statement for the target database server
     * @param array $driver_options Array of one or more key=>value pairs to set attribute
     * values for the PDOStatement obj
     returned
     * @return PDOStatement
     */
    public function prepare($statement, $driver_options = false)
    {
        if (!$driver_options)
            $driver_options = array();
        return $this -> db -> prepare($statement, $driver_options);
    }
    
    //public function execute ($statement, $data){
        
    //    return $statement -> execute ($data);
        
   // }
    
    public function getAllPrep($statement, $data){
        $sth = $this->prepare($statement); 
        $sth -> execute($data);
        return ($rs = $sth -> fetchall(PDO::FETCH_ASSOC)) ? $rs : array();
    }
    
    
    public function getRowPrep($statement, $data){
        $sth = $this->prepare($statement); 
        $sth -> execute($data);
        return ($rs = $sth -> fetch(PDO::FETCH_ASSOC)) ? $rs : array();
    }

    /**
     * Execute query and return one row in assoc array
     *
     * @param string $statement
     * @return array
     */
    public function queryFetchRowAssoc($statement)
    {
        return $this -> db -> query($statement) -> fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Execute query and select one column only
     *
     * @param string $statement
     * @return mixed
     */
    public function queryFetchColAssoc($statement)
    {
        return $this -> db -> query($statement) -> fetchColumn();
    }

    /**
     * Quotes a string for use in a query
     *
     * @param string $input
     * @param int $parameter_type
     * @return string
     */
    public function quote($input, $parameter_type = 0)
    {
        return $this -> db -> quote($input, $parameter_type);
    }

    /**
     * Set an attribute
     *
     * @param int $attribute
     * @param mixed $value
     * @return bool
     */
    public function setAttribute($attribute, $value)
    {
        return $this -> db -> setAttribute($attribute, $value);
    }

    /**
     * Fetch the SQLSTATE associated with the last operation on the database handle
     *
     * @return string
     */
    public function errorCode()
    {
        return $this -> db -> errorCode();
    }

    /**
     * Fetch extended error information associated with the last operation on the database
     * handle
     *
     * @return array
     */
    public function errorInfo()
    {
        return $this -> db -> errorInfo();
    }
	
	
	public function stripAccents($texte) {
		$texte = str_replace(
			array(
				'à', 'â', 'ä', 'á', 'ã', 'å',
				'î', 'ï', 'ì', 'í', 
				'ô', 'ö', 'ò', 'ó', 'õ', 'ø', 
				'ù', 'û', 'ü', 'ú', 
				'é', 'è', 'ê', 'ë', 
				'ç', 'ÿ', 'ñ',
				'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
				'Î', 'Ï', 'Ì', 'Í', 
				'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø', 
				'Ù', 'Û', 'Ü', 'Ú', 
				'É', 'È', 'Ê', 'Ë', 
				'Ç', 'Ÿ', 'Ñ', '"' 
			),
			array(
				'a', 'a', 'a', 'a', 'a', 'a', 
				'i', 'i', 'i', 'i', 
				'o', 'o', 'o', 'o', 'o', 'o', 
				'u', 'u', 'u', 'u', 
				'e', 'e', 'e', 'e', 
				'c', 'y', 'n', 
				'A', 'A', 'A', 'A', 'A', 'A', 
				'I', 'I', 'I', 'I', 
				'O', 'O', 'O', 'O', 'O', 'O', 
				'U', 'U', 'U', 'U', 
				'E', 'E', 'E', 'E', 
				'C', 'Y', 'N', '\"' 
			),$texte);
		return $texte;
	}

}



function dbConnect()
{

 //   return $db;
}

// Special dbConnect for error handling
function dbConnectError()
{
    //global $DATABASE_host, $DATABASE_user, $DATABASE_user_passwd,$DATABASE_name;
    //$DATABASE_conn = mysql_connect($DATABASE_host, $DATABASE_user,
    // $DATABASE_user_passwd);

    // -- Select the correct DATABASE
    //mysql_select_db($DATABASE_name);

    //return $DATABASE_conn;
}

// -- DATABASE disconnection
function dbDisconnect($DATABASE_conn)
{
    //mysql_close($DATABASE_conn);
}

// -- return last inserted id
function dbInsertId()
{
    $db = new sdb();
   // return $db;
    return $db->lastInsertId();
}

// -- Execute a query
function dbQuery($DATABASE_query)
{
    $db = new sdb();
    $result = $db -> exec($DATABASE_query);
    //$DATABASE_result = mysql_query($DATABASE_query);
    /**
     * Modif pour ticket #1016
     * Mise en commantaire du DIE la fonction return false en cas d'erreur
     * @return ressouces / true / false en cas d'erreur
     */
    //	or die (trigger_error("<p>MySQL " . mysql_errno() . " : " . mysql_error() .
    // "</p><pre>$DATABASE_query</pre>"));
    //	or die ("<br> " . mysql_errno().": ".mysql_error());
    return $result;
}

// -- Stock the results in an array
function dbQueryAll($DATABASE_query, $result_type = MYSQL_BOTH)
{
    $db = new sdb();
    $result = $db -> getAll($DATABASE_query);
    return $result;
}

// -- Get the first result
function dbQueryOne($DATABASE_query)
{
    // Execute the query
    //$DATABASE_result = dbQuery($DATABASE_query);
    //return dbQueryNext($DATABASE_result);
    $db = new sdb();
    $result = $db -> getRow($DATABASE_query);
    return $result;
}

// -- Get the next result
function dbQueryNext($DATABASE_result, $result_type = MYSQL_BOTH)
{
    //return mysql_fetch_array($DATABASE_result, $result_type);
}

// Escape
function dbEscape($string)
{
    //return mysql_escape_string($string);
//    return mysql_real_escape_string($string);
    //return $string;
}

// -- call the sql function to get the year of a date
function dbYear($field)
{
    return "YEAR(" . $field . ")";
}

// -- call the sql function to get the month of a date
function dbMonth($field)
{
    return "MONTH(" . $field . ")";
}