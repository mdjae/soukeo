<?php 
/*
RIGHT MENU 
*/

// i_action possible :
// - FILTER
// - ACTION
// - EXPLANATION
function drawRMenuHeader($i_boxtitle = "Box title",$i_action="FILTER",$i_HEIGHT="100")
{
  global $root_path;
  
  $i_picto = "picto_filter";
  if ($i_action == 'ACTION') { $i_picto = "picto_action"; }
  if ($i_action == 'EXPLANATION') { $i_picto = "picto_explanation"; }
  if ($i_action == 'CALC') { $i_picto = "picto_stats"; }
  
  ?>
  <div class="menuRight well no-padding">
  	
  <div class="top-bar">
    <h3> <?/* <img src="/skins/common/images/<?php  echo $i_picto ?>.gif" align="absbottom" /> */ ?>
    	<i class="fa fa-<?php echo $i_picto ?>"></i>
    <?php  echo $i_boxtitle ?></h3>  
    </div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
          <td align="left" width="3">
            <img src="/skins/common/images/empty.gif" align="absmiddle" width="12" height="2" />
          </td>
          <td>
            <div class="menubgperfor"></div>
          </td>
    </tr>
    </table>
    
  </td></tr>
   
  <tr>
  <td align="left" width="3">
    <img src="/skins/common/images/empty.gif" align="absmiddle" width="1" height="<?php  echo $i_HEIGHT ?>" />
  </td>
  <td align="left" valign="top">
  
<?php 
}

function drawRMenuFooter()
{
  ?>
 </div>
  <?php 
}

function MenuCalcul($i_boxtitle = "Box title",$i_action="FILTER",$i_HEIGHT="100")
{
global $root_path;
  
  if ($i_action == 'CALC') { $i_picto = "picto_stats"; }
  echo drawRMenuHeader($i_boxtitle,$i_action,$i_HEIGHT);

  

}

function drawRMenuAction($i_boxtitle, $i_array, $i_showcontainer=true)
{
  global $skin_path;
  
  if ($i_showcontainer) {
    if (count($i_array)<4) { 
      echo drawRMenuHeader($i_boxtitle,"ACTION","50");
    } else {
      echo drawRMenuHeader($i_boxtitle,"ACTION");
    } 
  }
  
  $explanation = "<table cellpadding='2' cellspacing='1' border='0' width='98%'' align='center'>\n";
  for ($a=0;$a<count($i_array);$a++) {
    if (($i_array[$a][0]=="") || (SystemAccess::canAccess($i_array[$a][0]))) {
      $explanation .= "<tr>\n";
      $explanation .= "<td valign='top' align='center' width='20'><p><img class='iconAction' alt='". $i_array[$a][2] ."' src='". $skin_path ."images/picto/". $i_array[$a][1] .".gif'/></p></td>\n";
    	$explanation .= "<td><p><a href=\"".$i_array[$a][3]  ."\" ". (($i_array[$a][4]!="")?"class=\"". $i_array[$a][4] ."\"":"") ."> " . $i_array[$a][2] ."</a></p></td>\n";
    	$explanation .= "</tr>\n";
    }
  }
  $explanation .= "</table>\n";
  
  echo $explanation;
  
  if ($i_showcontainer) {
    echo drawRMenuFooter();
  }
}

function drawRMenuAction2($i_boxtitle, $i_array)
{
  global $skin_path;
  
  
  if (count($i_array)<4) { 
    echo drawRMenuHeader($i_boxtitle,"ACTION","50");
  } else {
    echo drawRMenuHeader($i_boxtitle,"ACTION");
  } 
  
  $explanation = "<table cellpadding='2' cellspacing='1' border='0' width='98%'' align='center'  class='test'>\n";
  for ($a=0;$a<count($i_array);$a++) {
    if (($i_array[$a]['access']=="") || (SystemAccess::canAccess($i_array[$a]['access']))) {
      $explanation .= "<tr>\n";
      $explanation .= "<td valign='top' align='center' width='40'><p><img class='iconAction' alt='". $i_array[$a]['name'] ."' src='". $skin_path ."images/picto/". $i_array[$a]['picto'] .".gif'/></p></td>\n";
      if ($i_array[$a]['popup']=='') {
         $explanation .= "<td><a href=\"".$i_array[$a]['url']  ."\">". $i_array[$a]['name'] ."</a></td>\n";
      } else { 
    	   $explanation .= "<td><p>" . popupLink($i_array[$a]['url'],$i_array[$a]['name'],$i_array[$a]['width'],$i_array[$a]['height'],$i_array[$a]['popup'], $i_array[$a]['function']) . $i_array[$a]['name'] ."</a></p></td>\n";
    	}
    	$explanation .= "</tr>\n";
    }
  }
  $explanation .= "</table>\n";
  
  echo $explanation;
  
  echo drawRMenuFooter();
}

function drawRMenuExplanation($i_boxtitle, $i_array, $type=false)
{
  global $skin_path;
  
  echo drawRMenuHeader($i_boxtitle,"EXPLANATION");
  if (!$type){
  $explanation = "<table cellpadding='2' cellspacing='1' border='0' width='98%'' align='center'>\n";
  for ($a=0;$a<count($i_array);$a++) {
    if (($i_array[$a][0]=="") || (SystemAccess::canAccess($i_array[$a][0]))) {
      $explanation .= "<tr>\n";
      $explanation .= "<td valign='top' align='center' width='20'><p><img class='". ($i_array[$a][1]!=""?$i_array[$a][1]:"iconDescr") ."' src='". $skin_path ."images/picto/". $i_array[$a][2] .".gif'/></p></td>\n";
    	$explanation .= "<td><p>". $i_array[$a][3] ."</p></td>\n";
    	$explanation .= "</tr>\n";
    }
  }
  $explanation .= "</table>\n";
  

  }elseif ($type =="txt" ){
      
        $explanation = "<table cellpadding='2' cellspacing='1' border='0' width='98%'' align='center'>\n";
  for ($a=0;$a<count($i_array);$a++) {

      $explanation .= "<tr>\n";
      if ( $i_array[$a][3]  == "" ){
      $explanation .= "<td valign='top' align='center' colspan='2'>
                            <p>". ($i_array[$a][1]!=""?$i_array[$a][1]:"iconDescr") ."</p>
                        </td>\n";
                        
       }else{
                 $explanation .= "<td valign='top' align='center'>
                            <p>". ($i_array[$a][1]!=""?$i_array[$a][1]:"iconDescr") ."</p>
                        </td>\n";
                         $explanation .= "<td><p>". $i_array[$a][3] ."</p></td>\n";
       }
       
        $explanation .= "</tr>\n";
    
  }
  $explanation .= "</table>\n";
  }
  echo $explanation;
  
  echo drawRMenuFooter();
}
?>