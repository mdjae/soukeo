<?php 
function getExpPdf($usercode,$tiers_code,$tiers_grp_id,$select_year,$select_month)
{
	global $corp_path;

	// R�cup�ration des lignes pour les NDF
	$query = "SELECT u.firstname,
					u.lastname,
					n.ea_date,
					n.distance,
					n.power,
					(n.distance * k.price) as km_ttc,
					(n.amount_tf + n.amount_vat1 + n.amount_vat2 +
						(n.distance * k.price)) as total,
					n.ea_type,
					reason
				FROM c_user u, c_ea n
					LEFT JOIN c_ea_km k
					ON k.power = n.power
						AND k.year = (CASE WHEN n.period >= 3 THEN n.year ELSE (n.year-1) END)
				WHERE u.usercode = n.usercode
					AND n.tiers_grp_id = " . $tiers_grp_id . "
					AND n.usercode = '" . $usercode . "'
					AND n.year = " . $select_year . "
					AND n.period = " . $select_month;
	
	//echo "<p>" . $query . "</p>";
	
	$res = dbQueryAll($query);
	
	// R�cup�tation de toutes les cat�gorie
	$query = "SELECT ea_type, description
				FROM c_ea_type";
	$res_cat = dbQueryAll($query);
	
	// Cr�ation du PDF et de la page
	$pdf = new FPDF('L','mm','A4');
	$pdf->SetDisplayMode('real');
	$pdf->SetSubject(sprintf(("NDF refacturables %s"), SystemParams::getCompanyParam('company*name')));
	$pdf->SetAuthor(SystemParams::getCompanyParam('company*name'));
	$pdf->SetCreator(SystemParams::getCompanyParam('company*name'));
	$pdf->SetTitle(sprintf(("NDF refacturables %s / %s / %s"), SystemParams::getCompanyParam('company*name'), $tiers_code, $usercode));
	$pdf->AddPage('L');
	
	$normal_size = 10;
	$normal_height = 3;
	
	// Entete de NDF
	$pdf->SetFont('Arial','B',12);
	$pdf->Image($corp_path . 'images/logo4doc.jpg',10,10,44);
	$pdf->Cell(100,12,'','');
	$pdf->SetFillColor(224,224,224);
	$pdf->Ln();
	$pdf->Cell(100,$normal_height * 2,SystemParams::getCompanyParam('company*name'),'');
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(90,$normal_height * 2,("FRAIS en euros"),'');
	$pdf->SetFont('Arial','B',12);
	$pdf->Ln();
	$pdf->Cell(100,$normal_height * 2,SystemParams::getCompanyParam('company*address'),'');
	$pdf->Cell(90,$normal_height * 2,sprintf(("Client : %s"), $tiers_code),'');
	$pdf->Ln();
	$pdf->Cell(100,$normal_height * 2,SystemParams::getCompanyParam('company*zipcode') . ' ' . SystemParams::getCompanyParam('company*city'),'');
	
	$pdf->Ln();
	$pdf->Ln();
	
	
	$pdf->Cell(100,$normal_height * 2,sprintf(("Consultant : %s"), $res[0]['firstname'] . ' ' . $res[0]['lastname']),1);
	$pdf->Cell(40,$normal_height * 2,'');
	$pdf->Cell(40,$normal_height * 2,sprintf(("MOIS : %s / %s"), $select_month, $select_year),1);
	
	$pdf->Ln();
	$pdf->Ln();
	
	$pdf->SetFont('Arial','B',10);
	
	$pdf->Cell(20,$normal_height * 2,('Date'),'TRL',0,'C');
	$pdf->Cell(20,$normal_height * 2,('Facturable'),'RLT',0,'C');
	
	// Boucle sur les cat�gories
	foreach($res_cat as $c)
	{
		$description = split(" / ",$c['description']);
		$pdf->Cell(20,$normal_height * 2,$description[0],'RLT',0,'C');
	}
	
	$pdf->Cell(80,$normal_height * 2,('Raison de la d�pense'),'RLT',0,'C');
	$pdf->Ln();
	$pdf->Cell(20,$normal_height * 2,'','BRL');
	$pdf->Cell(20,$normal_height * 2,('au total'),'BRL',0,'C');
	
	// Boucle sur les cat�gorie
	foreach($res_cat as $c)
	{
		$description = split(" / ",$c['description']);
		if (count($description) > 1) $pdf->Cell(20,$normal_height * 2,$description[1],'BRL',0,'C');
		else $pdf->Cell(20,$normal_height * 2,'','BRL',0,'C');
	}
	
	$pdf->Cell(80,$normal_height * 2,'','BRL',0,'C');
	
	$pdf->SetFont('Arial','',10);
	
	// Nombre de ligne du tableau
	$nb_lignes = 12;
	
	// Totaux
	$total_refac = 0.00;
	
	$total_parking_peage = 0;
	$total_cat = array();
	foreach($res_cat as $c)
	{
		if (!array_key_exists($c['ea_type'], $total_cat)) $total_cat[$c['ea_type']] = 0;
	}
	
	// Boucle sur les lignes
	for ($i = 0; $i < count($res); $i++)
	{
		// D�cr�ment du cpteur de ligne
		$nb_lignes -= 1;

		$pdf->Ln();
		$pdf->Cell(20,$normal_height * 2,writeNormalDate($res[$i]['ea_date']),1);
		$pdf->Cell(20,$normal_height * 2,number_format($res[$i]['total'],2,',',' '),1,0,'R');
		
		foreach($res_cat as $c)
		{
			// Est-ce que c'est la cat�gorie de notre colonne ?
			if ($c['ea_type'] == $res[$i]['ea_type'])
			{
				// On affiche le montant
				$pdf->Cell(20,$normal_height * 2,number_format($res[$i]['total'],2,',',' '),1,0,'R');
				
				// Totaux
				$total_refac += $res[$i]['total'];
				
				// Total de la cat�gorie
				$total_cat[$res[$i]['ea_type']] += $res[$i]['total'] ;
			}
			else
			{
				// On affiche rien
				$pdf->Cell(20,$normal_height * 2,'',1,0,'R');
			}
			

		}
		
		$pdf->Cell(80,$normal_height * 2,stripslashes($res[$i]['reason']),1);
	

	}
	
	// On ajoute les lignes vides
	for ($i = 0; $i < $nb_lignes; $i++)
	{
		$pdf->Ln();
		$pdf->Cell(20,$normal_height * 2,'',1);
		$pdf->Cell(20,$normal_height * 2,'',1);
		
		foreach($res_cat as $c)
		{
			$pdf->Cell(20,$normal_height * 2,'',1);
		}
		
		$pdf->Cell(80,$normal_height * 2,'',1);
	}
	
	// Total
	$pdf->Ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(20,$normal_height * 2,'','RT');
	$pdf->Cell(20,$normal_height * 2,number_format($total_refac,2,',',' '),1,0,'R');
	foreach($res_cat as $c)
	{
		$pdf->Cell(20,$normal_height * 2,number_format($total_cat[$c['ea_type']],2,',',' ') ,1,0,'R');
	}

	$pdf->Cell(80,$normal_height * 2,'','TL');
	
	
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	
	$pdf->Cell(100,$normal_height * 2,'',0);
	$pdf->Cell(30,$normal_height * 2,('TOTAL : '),1);
	$pdf->Cell(30,$normal_height * 2,number_format($total_refac,2,',',' '),1,0,'R');

	return $pdf;
}
?>