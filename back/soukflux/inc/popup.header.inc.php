<?php 
require_once $root_path .	'inc/offipse.inc.php';
SystemAccess::canAccessPage2();
SystemUserTracking::trackUser();
?>
<html>
<head>
	<title><?php echo ($top_body ? $top_body : _("Soukflux") ) ?></title>
	<?php /* <link rel="stylesheet" type="text/css" href="<?php  echo $root_path ?>/skins/base.css" /> */ ?>
    <link rel="stylesheet" type="text/css" href="<?php  echo $root_path ?>/skins/jquery-ui-1.9.2.custom-style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $root_path ?>/skins/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $root_path ?>/skins/style-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $root_path ?>/skins/font-awesome.min.css" />


   	<meta name="description" content="" />
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="Content-language" content="fr">
	<meta http-equiv="Window_target" content="_top">
	<meta http-equiv="expires" content="Tue, 01 Jan 2002 00:00:00 GMT">
	<script language="JavaScript" src="<?php  echo $root_path ?>inc/js/jquery.js" type='text/javascript'></script>
    <script language="JavaScript" src="<?php  echo $root_path ?>inc/js/jquery-ui-1.9.2.custom.js" type='text/javascript'></script>
    <!--<script type='text/javascript' src='/inc/js/xtcore.js'></script>-->

</head>
<body <?php  echo (isset($onload)?$onload:'') ?>>
<div id="parent container">

