
$.urlParam = function(name){
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
/*
 * Page suivante
 */
function nextPage(name, root_path) {
	commonUrl = "/common/grid/grid.service.php?path=" + root_path + "&";
	jQuery.ajax({
		type : 'get',
		url : commonUrl + "function=nextPage&name=" + name,
		beforeSend: function() {
				$('#grid_' + name +' table').empty();
				$('#grid_' + name +' table').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		},
		complete: function(){
		    	$('#prod_ecom_loader').hide();
		},
		success : function(data) {
			$('#grid_' + name).html(data);
		}
	})

}

/*
 * Derniére page
 */
function lastPage(name, root_path) {
	commonUrl = "/common/grid/grid.service.php?path=" + root_path + "&";
	jQuery.ajax({
		type : 'get',
		url : commonUrl + "function=lastPage&name=" + name,
		beforeSend: function() {
				$('#grid_' + name +' table').empty();
				$('#grid_' + name +' table').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		},
		complete: function(){
		    	$('#prod_ecom_loader').hide();
		},
		success : function(data) {
			$('#grid_' + name).html(data);
		}
	})
}

/*
 * Page précédente
 */
function previousPage(name, root_path) {
	commonUrl = "/common/grid/grid.service.php?path=" + root_path + "&";
	jQuery.ajax({
		type : 'get',
		url : commonUrl + "function=previousPage&name=" + name,
		beforeSend: function() {
				$('#grid_' + name +' table').empty();
				$('#grid_' + name +' table').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		},
		complete: function(){
		    	$('#prod_ecom_loader').hide();
		},
		success : function(data) {
			$('#grid_' + name).html(data);
		}
	})

}

/*
 * Premi�re page
 */
function firstPage(name, root_path) {
	commonUrl = "/common/grid/grid.service.php?path=" + root_path + "&";
	jQuery.ajax({
		type : 'get',
		url : commonUrl + "function=firstPage&name=" + name,
		beforeSend: function() {
				$('#grid_' + name +' table').empty();
				$('#grid_' + name +' table').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		},
		complete: function(){
		    	$('#prod_ecom_loader').hide();
		},
		success : function(data) {
			$('#grid_' + name).html(data);
		}
	})

}

function gridSort(name, col, sort, root_path) {
	jQuery.ajax({
		type : 'get',
		url : "/common/grid/grid.service.php?path=" + root_path + "&function=sort&name=" + name + "&col=" + col + "&sort=" + sort,
		beforeSend: function() {
				$('#grid_' + name +' table').empty();
				$('#grid_' + name +' table').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		},
		complete: function(){
		    	$('#prod_ecom_loader').hide();
		},
		success : function(data) {
			$('#grid_' + name).html(data);
		}
	})

}

function gridReload(name, root_path) {
	jQuery.ajax({
		type : 'get',
		url : "/common/grid/grid.service.php?path=" + root_path + "&function=reload&name=" + name,
		success : function(data) {
			$('#grid_' + name).html(data);
		},
		beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Rechargement de la liste...',
						closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						},
	    			});
		    	$loaddialog.dialog('open');
		},
		complete: function(){
			$loaddialog.dialog('close');
		}
	})
}

function confirmDelete(message, id1, id2, id3, grid_name, root_path) {
	if (confirm(message)) {
		jQuery.ajax({
			type : 'get',
			scriptCharset : "utf-8",
			contentType : "application/x-www-form-urlencoded; charset=UTF-8",
			url : "/common/grid/grid.service.php?path=" + root_path + '&function=delete&name=' + grid_name + '&id1=' + id1 + '&id2=' + id2 + '&id3=' + id3,
			success : function(data) {
				gridReload(grid_name, root_path);
			}
		});
	} else {
		// Rien
	}

}

function selectAll() {
	var aInput = document.getElementsByTagName('input');
	for (var i = 0; i < aInput.length; i++) {
		if (aInput[i].type == 'checkbox' && aInput[i].id != '') {
			aInput[i].checked = true;
		}
	}
}

function checkGrid (elm) {

	var tmp = elm.id.split("-");	
	var grid_id = "grid_"+tmp[1];
	
	$("#"+grid_id+' input:checkbox').prop('checked', elm.checked);
}


function setActifGrossiste () {
	
	var theData = {};
	
	theData['id'] = [];
	
	var id_grossiste = "";
	
	$("input:checked").each(function(){
		id_grossiste = this.id.split('_');
		id_grossiste = id_grossiste[1];
		
		theData.id.push(id_grossiste);
		$('#' + id_grossiste).attr('class', 'iconCircleCheck');
		$('#' + id_grossiste).attr('src', '/skins/common/images/picto/check.gif');
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/setactifgrossiste",
			data : theData,
			success : function(data) {
				$loaddialog.attr("id", "");
				$loaddialog.html(data);
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Activation des grossistes...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(){
				//$loaddialog.dialog('close');
			}
	});	
	
}

function setInactifGrossiste () {
	
	var theData = {};
	theData['id'] = [];
	var id_grossiste = "";
	
	$("input:checked").each(function(){
		id_grossiste = this.id.split('_');
		id_grossiste = id_grossiste[1];
		
		theData.id.push(id_grossiste);
		$('#' + id_grossiste).attr('class', 'iconCircleWarn');
		$('#' + id_grossiste).attr('src', '/skins/common/images/picto/denied.gif');
	});
	$.ajax({
			type : 'POST',
			url : "/app.php/setinactifgrossiste",
			data : theData,
			success : function(data) {
				$loaddialog.attr("id", "");
				$loaddialog.html(data);
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Désactivation des grossistes...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(){}
	});		
}

function popupNumComGrossiste () {
	
  	var $dialogRecap = $('<div></div>').html("<input id='numCommande' type='text'/>").dialog({
    			autoOpen: false,
    			width : 'auto',
    			title: 'Entrez numéro de commande :',
    			buttons:{
    			"OK":function (){
    					if($('#numCommande').val() !== ""){
    						assignCommGrossiste($('#numCommande').val());
    						$(this).dialog('destroy').remove();
    							
    					}else{
    						alert('Entrez un numéro de commande');
    						$(this).dialog('destroy').remove();
    					}
    				}
    			},
	      		"Annuler": function () {
		        	$(this).dialog('destroy').remove()
		        	return false;
	      		}});
	$dialogRecap.dialog('open');
}

function popupNumAnnonceRecep(){

  	var $dialogRecap = $('<div></div>').html("<input id='numAnnonceRep' type='text'/>").dialog({
    			autoOpen: false,
    			width : 'auto',
    			title: 'Entrez numéro annonce réception :',
    			buttons:{
    			"OK":function (){
    					if($('#numAnnonceRep').val() !== ""){
    						assignAvisRecept($('#numAnnonceRep').val());
    						$(this).dialog('destroy').remove();
    							
    					}else{
    						alert('Entrez un numéro d\'annonce de réception');
    						$(this).dialog('destroy').remove();
    					}
    				}
    			},
	      		"Annuler": function () {
		        	$(this).dialog('destroy').remove()
		        	return false;
	      		}});
	$dialogRecap.dialog('open');	
}


function verifAssignCommGrossiste () {
  
	if($('#inputNumCom').val() !== ""){
		assignCommGrossiste($('#inputNumCom').val(), true);
	}else{
		alert('Entrez un numéro de commande');
	}
}

function assignCommGrossiste(num, delete_row) {
	
	var code_gr = num;
	var theData = {} ;
	theData['po'] = [] ;
	theData['item'] = [] ;
	theData['external_code_grossiste']= code_gr;
	 
	$("input:checked").each(function(){
		var tmp = $(this).attr("id");
		var tmp = tmp.split('_');
		
		if(tmp[0] == "item"){
			theData.item.push(tmp[1]);
		}else if(tmp[0] == "po"){
			theData.po.push(tmp[1]);
		}
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/assigncommgrossiste",
			data : theData,
			
			success : function(data) {
				$loaddialog.dialog('destroy').remove();
				var info = jQuery.parseJSON(data);
				
				if ( info.po != null ){
					$.each( info.po, function( key, value ) {
	  					$('#com_'+value+'_po').text("commande complete");
					});
				};
				
				$.each( info.item, function( key, value ) {
  					$('#item_'+value+'_cmd').html('commandé : <a href="/app.php/grossistes/commande_grossiste?id='+info.external_code+'" target="_blank">'
									+info.external_code+'</a>');
					if(delete_row){
							$("#item_"+value).closest(" tr ").remove();
						}
				});		
				
				if(delete_row){
					$('#alert-recipient')
						.html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>'+info.item.length+' produit(s) ajouté(s) ! <br/>Commande : </strong> <a href="/app.php/grossistes/commande_grossiste?id='+info.external_code+'" target="_blank"><strong>'
									+info.external_code+'</strong> (Cliquez pour voir cette commande grossiste)</a> </div>');
				}	
							
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Mise en commande...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
	});		
}

function assignItemComGr (id_item, delete_row) {
	
	
	var code_gr = $('#popoverinput_'+id_item).val();
	var theData = {} ;
	theData['po'] = [] ;
	theData['item'] = [] ;
	theData['external_code_grossiste']= code_gr;
	 
	theData.item.push(id_item);
	
	if(code_gr != ""){
		$.ajax({
			type : 'POST',
			url : "/app.php/assigncommgrossiste",
			data : theData,
			
			success : function(data) {
				$loaddialog.dialog('destroy').remove();
				$('#popover_'+id_item).popover('hide');
				var info = jQuery.parseJSON(data);
				
				if ( info.po != null ){
					$.each( info.po, function( key, value ) {
	  					$('#com_'+value+'_po').text("commande complete");
					});
				};
				
				$.each( info.item, function( key, value ) {
					$('#item_'+value+'_cmd').html('commandé : <a href="/app.php/grossistes/commande_grossiste?id='+info.external_code+'" target="_blank">'
									+info.external_code+'</a>');
				});		
				
				if(delete_row){
					$("#litigeit_"+id_item).closest(" tr ").remove();
					$('#alert-recipient')
						.html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Produit ajouté ! <br/>Commande : </strong> <a href="/app.php/grossistes/commande_grossiste?id='+info.external_code+'" target="_blank"><strong>'
									+info.external_code+'</strong> (Cliquez pour voir cette commande grossiste)</a> </div>');
				}	
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Mise en commande...',
		    			closeOnEscape: false,
						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
		});	
	}
}

function assignAvisRecept(num, delete_row) {
	
	var code_recep = num;
	var theData = {} ;
	theData['ordergr'] = [] ;
	theData['item'] = [] ;
	theData['code_recep']= code_recep;
	 
	$("input:checked").each(function(){
		var tmp = $(this).attr("id");
		var tmp = tmp.split('_');
		
		if(tmp[0] == "itemcomgr"){
			theData.item.push(tmp[1]);
		}else if(tmp[0] == "comgr"){
			theData.ordergr.push(tmp[1]);
		}
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/assignavisreception",
			data : theData,
			
			success : function(data) {
				$loaddialog.dialog('destroy').remove();
				var info = jQuery.parseJSON(data);
				
				if ( info.ordergr != null ){
					$.each( info.ordergr, function( key, value ) {
	  					$('#recep_'+value+'_ordergr').text("Réception complete");
					});
				};
				
				$.each( info.item, function( key, value ) {
					$('#item_'+value+'_reception').html('A.R. : <a href="/app.php/grossistes/annonce_reception?id='+info.code_recep+'" target="_blank">'+info.code_recep+'</a>');
				});		

							
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Avis de réception...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
	});		
}

function assignItemAvisRecept (id_item, delete_row) {
	var code_recep = $('#popoverinput_'+id_item).val();
	var theData = {} ;
	theData['order_gr'] = [] ;
	theData['item'] = [] ;
	theData['code_recep']= code_recep;
	 
	theData.item.push(id_item);
	
	if(code_recep != ""){
		$.ajax({
			type : 'POST',
			url : "/app.php/assignavisreception",
			data : theData,
			
			success : function(data) {
				$loaddialog.dialog('destroy').remove();
				$('#popover_'+id_item).popover('hide');
				var info = jQuery.parseJSON(data);
				
				if ( info.order_gr != null ){
					$.each( info.order_gr, function( key, value ) {
	  					$('#recep_'+value+'_ordergr').text("Réceptionné");
					});
				};
				
				$.each( info.item, function( key, value ) {
					$('#item_'+value+'_reception').html('A.R. : <a href="/app.php/grossistes/annonce_reception?id='+info.code_recep+'" target="_blank">'+info.code_recep+'</a>');
				});		

			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Ajout avis reception...',
		    			closeOnEscape: false,
						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
		});	
	}
}

function assignPoComGr (id_po) {
	
	
	var code_gr = $('#popoverinput_'+id_po).val();
	var theData = {} ;
	theData['po'] = [] ;
	theData['item'] = [] ;
	theData['external_code_grossiste']= code_gr;
	 
	theData.po.push(id_po);
	
	if(code_gr != ""){
		$.ajax({
			type : 'POST',
			url : "/app.php/assigncommgrossiste",
			data : theData,
			
			success : function(data) {
				$loaddialog.dialog('destroy').remove();
				$('#popover_'+id_po).popover('hide');
				var info = jQuery.parseJSON(data);
				
				if ( info.po != null ){
					$.each( info.po, function( key, value ) {
	  					$('#com_'+value+'_po').text("commande complete");
					});
				};
				
				$.each( info.item, function( key, value ) {
					$('#item_'+value+'_cmd').html('commandé : <a href="/app.php/grossistes/commande_grossiste?id='+info.external_code+'" target="_blank">'
									+info.external_code+'</a>');
				});			
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Mise en commande...',
		    			closeOnEscape: false,
						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
		});	
	}
}


function downloadAllFact() {
	
	var theData = {} ;
	theData['facture'] = [] ;
	 
	$("input:checked").each(function(){
		var tmp = $(this).attr("id");
		var tmp = tmp.split('_');
		
		if(tmp[0] == "facture"){
			theData.facture.push(tmp[1]);
		}
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/downloadAllFact",
			data : theData,
			
			success : function(data) {
				//$loaddialog.dialog('destroy').remove();	
				$('#prod_ecom_loader').attr('id', "");
				$loaddialog.html(data);
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Création de l\'archive...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
	});		
}


function exportOpti (num) {
	
	var id_commande = num.split('_');
	id_commande = id_commande[1];
	
	var theData = {
		order_id_grossiste : id_commande
	}; 
	
	$.ajax({
			type : 'POST',
			url : "/app.php/exportOpti",
			data : theData,
			
			success : function(data) {
				/*$loaddialog = $('<div></div>')
		    		.html(data)
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Télécharger le fichier...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');*/
			}
	});	
}


function deleteIt (num) {
	
	var tmp = num.split('_');
	
	var type = tmp[0];
	var theData = {
	   		type : type,
	   		num  : tmp[1]
 	} ;
 	
    var $myDialog = $('<div></div>')
    		.html('Êtes vous sûr de vouloir supprimer cette ligne ?<br/><br/>Ok pour confirmer.  Annuler pour annuler.')
    		.dialog({
    			autoOpen: false,
    			title: 'Suppression de ligne',
    			buttons: {
				      	"OK": function () {
				      			$.ajax({
									type : 'POST',
									url : "/app.php/deleteIt",
									data : theData,
									
									success : function(data) {
										$("#"+tmp[0]+"_"+tmp[1]).closest(" tr ").remove();
										if($("#ticket_list_client").length > 0){
											$("#"+tmp[0]+"_"+tmp[1]+"_cli").closest(" tr ").remove();
										}
										
										if($('#'+type+'_messagebox').length > 0){
											$('#'+type+'_messagebox')
											.html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+type+' <strong>'+data+'</strong> a été supprimé !</a> </div>');
										}
									}
								});	
				      		$(this).dialog("destroy");
						    return true;
				      		},
			      		"Cancel": function () {
				        	$(this).dialog("destroy");
				        	return false;
			      		}
    				}
  			});
		$myDialog.dialog('open');
}


function addDateRelance (ticket_id, relance_id) {

	if($("#datepickerRelance_"+ticket_id).val() != ""){
		var theData = {
			date : $("#datepickerRelance_"+ticket_id).val(),
			type_relance_id : $('#select_RelanceType').val(),
		   	ticket_id  : ticket_id,
		   	relance_id : relance_id
	 	} ;
	 	
	 	$.ajax({
	 		type : "POST",
	 		url : "/app.php/addRelanceTicket",
	 		data : theData,
	 		
	 		success : function (data){
	 			$("#modalAddRelance").remove();
	 			
	 			if($("#ticket_container").length > 0){
	 				refreshMiniRelanceTicketList(ticket_id);	
	 			}else{
	 				refreshRelanceTicketList();	
	 			}
	 			
	 			
	 		}
	 	});		
	}
	else{
		alert("Veuillez renseigner une date de relance");
	}

}

function showTicketViewRelances () {
	
	if($('#relances').hasClass("closed")){
		$('#relances').show("fast");
		$('#relances').removeClass("closed");
	}else{
		$('#relances').hide("fast");
		$('#relances').addClass("closed");
	}
}

function deleteRelance (relance_id) {

	var result = confirm("Confirmer la suppression de la relance ?");
	
	if(result == true){
		var theData = {
		   	relance_id  : relance_id
	 	} ;
	 	
	 	$.ajax({
	 		type : "POST",
	 		url : "/app.php/supprRelance",
	 		data : theData,
	 		
	 		success : function (data){
	 			if($("#ticket_container").length > 0){
	 				refreshMiniRelanceTicketList(ticket_id);	
	 			}else{
	 				refreshRelanceTicketList();	
	 			}
	 		}
	 	});		
	}
}

function setRelanceTermine(relance_id, action){
	
	var theData = {
		relance_id : relance_id,
		action : action
	} ;
	    
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/setRelanceTermine",
		data : theData,
		success : function(data) {
			
			$( "#relanceaction_"+relance_id+" .dropdown-toggle").html(action+' <span class="caret"></span>') ;
			
			if(action == "Terminé"){
				$( "#relanceaction_"+relance_id+" .dropdown-toggle").attr("class", "btn btn-success dropdown-toggle")
			}
			if(action == "En cours"){
				$( "#relanceaction_"+relance_id+" .dropdown-toggle").attr("class", "btn btn-info dropdown-toggle")
			}
			return false;					
		}
	});		
}

function autoAssocActif(grossiste_id) {
	
	var theData = {} ;
	theData['categ'] = [] ;
	theData['grossiste_id'] = grossiste_id;
	 
	$("input:checked").each(function(){
		var text = $(this).parent().parent().find(".firstChild").text();
		theData.categ.push(text);
		$(this).parent().parent().find(".iconCircleWarn").attr("class", "iconCircleCheck");
		$(this).parent().parent().find(".iconCircleCheck").attr('src', '/skins/common/images/picto/check.gif');
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/setautoassoc",
			data : theData,
			
			success : function(data) {
				$loaddialog.attr("id", "");
				$loaddialog.html(data);
			},
			
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Activation association automatique...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(){}
	});		
	
}


function autoAssocInactif(grossiste_id) {
	
	var theData = {} ;
	theData['categ'] = [] ;
	theData['grossiste_id'] = grossiste_id;
	 
	$("input:checked").each(function(){
		var text = $(this).parent().parent().find(".firstChild").text();
		theData.categ.push(text);
		$(this).parent().parent().find(".iconCircleCheck").attr("class", "iconCircleWarn");
		$(this).parent().parent().find(".iconCircleWarn").attr('src', '/skins/common/images/picto/denied.gif');
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/cancelautoassoc",
			data : theData,
			
			success : function(data) {
				$loaddialog.attr("id", "");
				$loaddialog.html(data);
			},
			
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Désactivation association automatique...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(){}
	});			
}


function savePoidsCat (elm, grossiste_id) {
	
	var poids = $(elm).parent().parent().find(".poids").val().replace(',', '.').replace(' ', '');
	var cat   = $(elm).parent().parent().find(".firstChild").text();

	var theData = {
  		grossiste_id : grossiste_id,
		poids : poids,
		cat : cat
	};
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/valideditpoidscat",
		data : theData,
		success : function(data) {

			var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
			.dialog({
			    height: 140,
			    modal: false,
			    title: "Sauvegarde...",
			    open: function(event, ui){
			     setTimeout("$('#acknowledged-dialog').dialog('close')",700);
			    },
			    close: function(event, ui){
					$(this).dialog("destroy");
					$(this).remove();
					$("#dialogAv").dialog("close");
				}
			});
			dial.dialog('open');
		}
	}); 
	
}

function getFile (className) {
	
	var theData = {
		className : className
	};
	
	$.ajax({
		type : 'POST',
		url : "/app.php/getfile",
		data : theData,
		success : function(data) {
		
		},
		beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Création du fichier...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(data){
				$loaddialog.dialog('close');
			}
	});
}


/*
 * Permet de selectionner tout dans une grille (Classe Grid)
 * et de garder en session les éléments sélectionné malgré qu'on
 * fasse de la pagination
 */
function selectAllSession(){
	
	var aInput = document.getElementsByTagName('input');
	for (var i = 0; i < aInput.length; i++) {
		if (aInput[i].type == 'checkbox' && aInput[i].id != '') {
			aInput[i].checked = true;
		}
	}
	
	var theData = {};
	
	$.ajax({
			type : 'POST',
			url : "/app.php/selectallventesav",
			data : theData,
			success : function(data) {
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Selection des produits...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(){
				$loaddialog.dialog('close');
			}
	});
}

/*
 * Selection individuelle sur les Grilles (Grid)
 * Permet de sélectionner un élement et de mettre en session son ID
 */
function selectOneSession (elm) {
	
	var tmp = elm.id.split( '_' );
	
  	var theData ={id : tmp[1]};		
	
	if($(elm).is(':checked')){
  		$.ajax({
			type : 'POST',
			url : "/app.php/selectoneventesav",
			data : theData,
			success : function(data) {
			}
		});
  	}
  	else {
  		$.ajax({
			type : 'POST',
			url : "/app.php/selectoneventesav?a=undo",
			data : theData,
			success : function(data) {
			}
		});
  	}
}

/*
 * Action sur les grilles (Grid.class) permettant de déselectionner
 * tous les produits qui étaient en Session (vide la session)
 */
function deSelectAllSession() {
	var aInput = document.getElementsByTagName('input');
	for (var i = 0; i < aInput.length; i++) {
		if (aInput[i].type == 'checkbox' && aInput[i].id != '') {
			aInput[i].checked = false;
		}
	}
	var theData = {};
	
	$.ajax({
			type : 'POST',
			url : "/app.php/selectallventesav?a=undo",
			data : theData,
			success : function(data) {
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Selection des produits...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			complete: function(){
				$loaddialog.dialog('close');
			}
		});
}

function deSelectAll() {
	var aInput = document.getElementsByTagName('input');
	for (var i = 0; i < aInput.length; i++) {
		if (aInput[i].type == 'checkbox' && aInput[i].id != '') {
			aInput[i].checked = false;
		}
	}

}

jQuery(document).ready(function($) {
      $(".clickableRow").click(function() {
            window.document.location = $(this).data("rowurl");
      });
});

/*
 * Permet de placer en vente avahis tous les produits qu'on a selectionné sur les
 * grilles des grossistes (boutton trouvé dan qualification/venteavahis/menu.php)
 */
function sellSelectionVenteAv () {
	$.ajax({
		type : 'GET',
		url : '/app.php/sellselectionventeav',
		success : function(data){
			
			$( "input[type='checkbox']:checked" ).each(function(){
				var id = $(this).attr('id').split('_');
				id = id[1];
				
				var urlImg = $("#"+id+"_icnvente").attr("src");
				var classImg = $("#"+id+"_icnvente").attr("class");
				
				classImg = classImg.replace('Warn', 'Check')
				urlImg = urlImg.replace('denied', "check");
				
				$("#"+id+"_icnvente").attr("src", urlImg);
				$("#"+id+"_icnvente").attr("class", classImg);
			});
			var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
			.dialog({
			    height: "auto",
			    modal: false,
			    open: function(event, ui){
			    },
			    close: function(event, ui){
					$(this).dialog("destroy");
					$(this).remove();
				}
			});
			dial.dialog('open');
					
		},
		beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Mise en vente des produits...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
		complete: function(){
			$loaddialog.dialog("close");
		}
	})
}


/*
 * Permet de ne plu vendre tous les produits qu'on a selectionné sur les
 * grilles des grossistes (boutton trouvé dan qualification/venteavahis/menu.php)
 */
function cancelSellSelectionVenteAv () {
	$.ajax({
		type : 'GET',
		url : '/app.php/sellselectionventeav?a=undo',
		success : function(data){
			
			$( "input[type='checkbox']:checked" ).each(function(){
				var id = $(this).attr('id').split('_');
				id = id[1];
				
				var urlImg = $("#"+id+"_icnvente").attr("src");
				var classImg = $("#"+id+"_icnvente").attr("class");
				
				classImg = classImg.replace('Check', 'Warn')
				urlImg = urlImg.replace('check', "denied");
				
				$("#"+id+"_icnvente").attr("src", urlImg);
				$("#"+id+"_icnvente").attr("class", classImg);
			});
			var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
			.dialog({
			    height: "auto",
			    modal: false,
			    open: function(event, ui){
			    },
			    close: function(event, ui){
					$(this).dialog("destroy");
					$(this).remove();
				}
			});
			dial.dialog('open');
		},
		beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Annulation de la vente des produits...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
		complete: function(){
			$loaddialog.dialog("close");
		}
	})
}

function majCalcul(elm, fixedParams, aerien)  {
	


  var id = elm.id.split('_');
  id = id[0];
  
  var prxAchat = $("#"+id+'_prxachat').text().replace(',', '.').replace(' ', '');
  var percentOctrmer = $("#"+id+'_prctoctroimer').val().replace(',', '.').replace(' ', ''); 
  var percentmarge = $("#"+id+'_prctmarge').val().replace(',', '.').replace(' ', '');
  var percentTVA = $("#"+id+'_prctTVA').val().replace(',', '.').replace(' ', '');
  
  
  var octroimer = ( parseFloat(percentOctrmer) / 100) * ( parseFloat(prxAchat) + parseFloat(aerien) ) ;
  
  
  var prixRevient = parseFloat(prxAchat) + parseFloat(fixedParams) + parseFloat(aerien) + parseFloat(octroimer);
  
  $('#'+id+'_prxrevient').html(prixRevient.toFixed(2).replace('.', ','));
   
  var margeNette = prixRevient * parseFloat(percentmarge) / 100 ;
  
  $('#'+id+'_margenet').html(margeNette.toFixed(2).replace('.', ','));
  
  var TVA = ( parseFloat(prixRevient) + parseFloat(margeNette) ) * parseFloat(percentTVA) / 100;
  
  var prixVenteLivre = parseFloat(prixRevient) + parseFloat(margeNette) + parseFloat(TVA) ;
  
  var coef = prixVenteLivre / parseFloat(prxAchat) ;
  
  $('#'+id+'_coef').html(coef.toFixed(2).replace('.', ','));
  
  
  $('#'+id+'_prxlivre').html(prixVenteLivre.toFixed(2).replace('.', ','));
  
}

function calculPopupFactu () {
	
	var total_HT = 0;
	var total_comm_HT = 0;
	var total_fraisb_HT = 0;
	
	$( "input[type='checkbox']:checked" ).each(function(){
		var prix = $(this).closest(" tr ").find(".line_prix").text().replace(" €", "").replace(',', '.').replace(' ', '');
		if(prix != "") prix = parseFloat(prix);
		else prix = 0;
		var comm = $(this).closest(" tr ").find(".line_comm").text().replace(" €", "").replace(',', '.').replace(' ', '');
		if(comm != "") comm = parseFloat(comm)
		else comm = 0;
		var fraisb = $(this).closest(" tr ").find(".line_fraisb").text().replace(" €", "").replace(',', '.').replace(' ', '');
		if(fraisb != "")fraisb = parseFloat(fraisb);
		else fraisb = 0;
		
		total_HT += prix;
		total_comm_HT += comm;
		total_fraisb_HT += fraisb;
		
	});
	total_TTC = total_HT * 1.085;
	total_comm_TTC = total_comm_HT * 1.085;
	total_fraisb_TTC = total_fraisb_HT * 1.085;
	
	$('#total_HT').text(total_HT.toFixed(2).replace('.', ',') + " €");
	$('#total_comm_HT').text(total_comm_HT.toFixed(2).replace('.', ',') + " €");
	$('#total_fraisb_HT').text(total_fraisb_HT.toFixed(2).replace('.', ',') + " €");
	
	$('#total_TTC').text(total_TTC.toFixed(2).replace('.', ',') + " €");
	$('#total_comm_TTC').text(total_comm_TTC.toFixed(2).replace('.', ',') + " €");
	$('#total_fraisb_TTC').text(total_fraisb_TTC.toFixed(2).replace('.', ',') + " €");
}

function addLineFactu () {
	var desi = $('#addligne_designation').val();
	var prix = $('#addligne_prix').val();
	
	if(desi != "" && prix != "" && !isNaN(prix)){
		prix = parseFloat(prix);
		prix = prix.toFixed(2).replace('.', ',');
		$("#lines tr:last").after("<tr><td></td><td class='line_desig'>"+desi+"</td><td></td><td class='line_prix'>"+prix+" €</td><td></td><td></td><td><input class='addedLine' type='checkbox' checked onChange='calculPopupFactu()'></td></tr>");	
		calculPopupFactu();	
	}	
}

function saveFactuCommercant (vendor_id) {
	
	var theData = { item_id : [],
					added_lines : [],
					vendor_id : vendor_id}
	

	$( "input[type='checkbox']:checked" ).each(function(){
		
		if(this.id != ""){
			
			id = this.id.split('_');
			id = id[1];
			
			theData.item_id.push(id);
		}else if($(this).hasClass("addedLine")){
			var desig = $(this).closest(" tr ").find(".line_desig").text();
			var prix = $(this).closest(" tr ").find(".line_prix").text().replace(" €", "").replace(',', '.').replace(' ', '');
			if(prix != "") prix = parseFloat(prix);
			else prix = 0;
			theData.added_lines.push({
		    	'desig' : desig,
		    	'prix'  : prix
			});
		}
		
	});
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/savefactucommercant",
		data : theData,
		success : function(data) {
			
			var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
			.dialog({
			    height: 'auto',
			    modal: false,
			    title: "Sauvegarde...",
			    open: function(event, ui){
			     
			    },
			    close: function(event, ui){
					$(this).dialog("destroy");
					$(this).remove();
					$("#dialogAv").dialog("close");
				}
			});
			dial.dialog('open');
			$('input[type="submit"]').click();
		}
	});
	//$('input[type="submit"]').click();
}

function saveFactuAllCommercant () {
	
	var theData = { vendor_id : [] }
					
	$( "input[type='checkbox']:checked" ).each(function(){
		
		if(this.id != ""){
			
			id = this.id.split('_');
			if(id[0] == "vendor"){
				id = id[1];
				theData.vendor_id.push(id);	
			}
			
		}		
	});
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/saveFactuAllCommercants",
		data : theData,
		success : function(data) {
			$('input[type="submit"]').click();
		},
		beforeSend: function() {
			$loaddialog = $('<div id="prod_ecom_loader"></div>')
	    		.html('')
	    		.dialog({
	    			autoOpen: false,
	    			modal : true,
	    			draggable: false,
	    			closeText: "hide",
	    			title : 'Chargement..',
					closeOnEscape: false,
					open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
					close: function(event, ui){
						$(this).dialog("destroy");
						$(this).remove();
					},
    			});
	    	$loaddialog.dialog('open');
		},
		complete: function(){
			$loaddialog.dialog('close');
			$('input[type="submit"]').click();
		}
	});
}

function saveModifsVenteAv (elm) {
	
	var id = elm.id.split('_');
	id = id[0];

	var percentOctrmer = $("#"+id+'_prctoctroimer').val().replace(',', '.'); 
  	var percentmarge = $("#"+id+'_prctmarge').val().replace(',', '.');
  	var percentTVA = $("#"+id+'_prctTVA').val().replace(',', '.');
  	
  	var theData = {
  		id : id,
		percentOctrmer : percentOctrmer,
		percentmarge : percentmarge,
		percentTVA : percentTVA
	};
	
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/savemodifventeav",
		data : theData,
		success : function(data) {
			
			var dial = $("<div id ='acknowledged-dialog'> </div>").html('Ligne enregistrée !')
			.dialog({
			    height: 140,
			    modal: false,
			    open: function(event, ui){
			     setTimeout("$('#acknowledged-dialog').dialog('close')",1000);
			    },
			    close: function(event, ui){
					$(this).dialog("destroy");
					$(this).remove();
				}
			});
			dial.dialog('open');
		}
	})
	
}


function saveAllModifsVenteAv () {

	var theData = { prod : []}
	
	$( "input[type='checkbox']" ).each(function(){
		var id = $(this).attr('id').split('_');
		id = id[1];
		
		var percentOctrmer = $("#"+id+'_prctoctroimer').val().replace(',', '.'); 
	  	var percentmarge = $("#"+id+'_prctmarge').val().replace(',', '.');
	  	var percentTVA = $("#"+id+'_prctTVA').val().replace(',', '.');
	  	
		theData.prod.push({
		    'id' : id,
		    'percentOctrmer' : percentOctrmer,
			'percentmarge' : percentmarge,
			'percentTVA' : percentTVA
		});

	});
	
	jQuery.ajax({
	type : 'POST',
	url : "/app.php/saveallmodifventeav",
	data : theData,
	success : function(data) {
		var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
		.dialog({
			title : "Sauvegarde réussie !",
		    height: "auto",
		    width: "auto",
		    modal: false,
		    buttons: {
				"OK": function () {
					$(this).dialog('close');
				}
			},
		    close: function(event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
		});
		dial.dialog('open');
	}
})
}


function sendPassword(form) {

	var theData = {
		act : 'sendPassword',
		login : $('#your_login').value
	};

	jQuery.ajax({
		type : 'POST',
		url : "dispatcher.php",
		data : theData,
		success : function(data) {
			$('#dialog').html(data);
		}
	})

}


function updateGmap3() {
	var latitude = $("#SITE_LATITUDE").val().replace(',', ".");
	$("#SITE_LATITUDE").val(latitude);
	var longitude = $("#SITE_LONGITUDE").val().replace(',', ".");
	$("#SITE_LONGITUDE").val(longitude);
	$('#gmapsite').gmap3({
		action : 'clear',
		name : 'marker'
	}, {
		action : 'addMarker',
		latLng : [$("#SITE_LATITUDE").val(), $("#SITE_LONGITUDE").val()],
		map : {
			center : true
		},
		marker : {
			options : {
				draggable : true
			},
			events : {
				dragend : function(marker, event, data) {
					var myLatLng = event.latLng;
					var lat = myLatLng.lat();
					var lng = myLatLng.lng();
					$("#SITE_LATITUDE").val(lat);
					$("#SITE_LONGITUDE").val(lng);
				}
			}
		}
	});
}


/*
 * Permet de charger la catégorie suivante en ajax pour
 * l'arbre de catégorie Avahis
 */
function loadnextcat(elm) {
	
	var classelm = $(elm).attr('class');
	tmp = classelm.split(' ');
	tmp = tmp[0].split( '_' );
	

	
	var theData = {
		catid : elm.id,
		level : tmp[1]
	};
	if ($(elm).hasClass('open')) {
		$(elm).next().html('');
		$(elm).addClass('close');
		$(elm).removeClass('open');
		$(elm).removeClass('expanded');
		$(elm).prev().removeClass('tree-expander-expanded');
	} else {
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/showcat",
			data : theData,
			success : function(info) {
				$('.open.'+classelm).each(function(){
					$(this).removeClass('expanded');
					$(this).prev().removeClass('tree-expander-expanded');
					$(this).removeClass('open');
					$(this).next().html('');
				});
				
				/*$('.tree-expander-expanded').each(function(){
					$(this).removeClass('tree-expander-expanded');
				});*/
				
				if(info != ""){
					$(elm).prev().addClass('tree-expander-expanded');
				}
				$(elm).addClass('open');
				$(elm).removeClass('close');
				$(elm).addClass('expanded');
				$(elm).after(info);

			}
		});
	}
}
/*
 * Permet de charger la catégorie suivante en ajax pour
 * l'arbre de catégorie Avahis CP
 */
function loadnextcatGP(elm) {
	
	var classelm = $(elm).attr('class');
	tmp = classelm.split(' ');
	tmp = tmp[0].split( '_' );
	

	
	var theData = {
		catid : elm.id,
		level : tmp[1]
	};
	if ($(elm).hasClass('open')) {
		$(elm).next().html('');
		$(elm).addClass('close');
		$(elm).removeClass('open');
		$(elm).removeClass('expanded');
		$(elm).prev().removeClass('tree-expander-expanded');
	} else {
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/showcatGP",
			data : theData,
			success : function(info) {
				$('.open.'+classelm).each(function(){
					$(this).removeClass('expanded');
					$(this).prev().removeClass('tree-expander-expanded');
					$(this).removeClass('open');
					$(this).next().html('');
				});
				
				/*$('.tree-expander-expanded').each(function(){
					$(this).removeClass('tree-expander-expanded');
				});*/
				
				if(info != ""){
					$(elm).prev().addClass('tree-expander-expanded');
				}
				$(elm).addClass('open');
				$(elm).removeClass('close');
				$(elm).addClass('expanded');
				$(elm).after(info);

			}
		});
	}
}
function expandnextcat (elm) {
	
	var classelm = $(elm).next().attr('class');
	tmp = classelm.split(' ');
	tmp = tmp[0].split( '_' );	
	
	var cat_id = $(elm).next().attr('id');
	
	var theData = {
		catid : cat_id,
		level : tmp[1]
	};
	
	if ($(elm).next().hasClass('open')) {
		$(elm).next().next().html('');
		$(elm).next().addClass('close');
		$(elm).next().removeClass('open');
		$(elm).next().removeClass('expanded');
		$(elm).removeClass('tree-expander-expanded');
	} else {

		jQuery.ajax({
			type : 'POST',
			url : "/app.php/showcat",
			data : theData,
			beforeSend: function() {
				$(elm).addClass('tree-expander-load');
			},
			complete: function(){
				$(elm).removeClass('tree-expander-load');
			},
			success : function(info) {
				$('.open.lev_'+tmp[1]).each(function(){
					$(this).removeClass('expanded');
					$(this).prev().removeClass('tree-expander-expanded');
					$(this).removeClass('open');
					$(this).next().html('');
					
				});
				if(info != ""){
					$(elm).addClass('tree-expander-expanded');
				}
				$(elm).next().addClass('open');
				$(elm).next().removeClass('close');
				$(elm).next().addClass('expanded');
				$(elm).next().after(info);

			}
		});
	}
}
/**
 *permet d'étendre la catégorie pour GP
 *  
 */
function expandnextcatGP (elm) {
	
	var classelm = $(elm).next().attr('class');
	tmp = classelm.split(' ');
	tmp = tmp[0].split( '_' );	
	
	var cat_id = $(elm).next().attr('id');
	
	var theData = {
		catid : cat_id,
		level : tmp[1]
	};
	
	if ($(elm).next().hasClass('open')) {
		$(elm).next().next().html('');
		$(elm).next().addClass('close');
		$(elm).next().removeClass('open');
		$(elm).next().removeClass('expanded');
		$(elm).removeClass('tree-expander-expanded');
	} else {

		jQuery.ajax({
			type : 'POST',
			url : "/app.php/showcatGP",
			data : theData,
			beforeSend: function() {
				$(elm).addClass('tree-expander-load');
			},
			complete: function(){
				$(elm).removeClass('tree-expander-load');
			},
			success : function(info) {
				$('.open.lev_'+tmp[1]).each(function(){
					$(this).removeClass('expanded');
					$(this).prev().removeClass('tree-expander-expanded');
					$(this).removeClass('open');
					$(this).next().html('');
					
				});
				if(info != ""){
					$(elm).addClass('tree-expander-expanded');
				}
				$(elm).next().addClass('open');
				$(elm).next().removeClass('close');
				$(elm).next().addClass('expanded');
				$(elm).next().after(info);

			}
		});
	}
}

function recursiveCat (catString, level) {
	
	var tabId = catString.split("|");
	tabId.reverse();
	
	if(level < tabId.length){
		
		var catid = tabId[level];
		
		var theData = {
			catid : catid,
			level: level
		};
		
		$.ajax({
			type : 'POST',
			url  : '/app.php/showcat',
			data : theData,
			success : function (data) {
				$("#treeview_avahis #" + catid).addClass('open');
				$("#treeview_avahis #" + catid).removeClass('close');
				$("#treeview_avahis #" + catid).addClass('expanded');
				if(data != "")$("#treeview_avahis #" + catid).prev().addClass('tree-expander-expanded');
				$("#treeview_avahis #" + catid).after(data);
				level = level + 1 ;
				recursiveCat(catString, level);
			}
		});		
	}
}

function recursiveCatGP (catString, level) {
	
	var tabId = catString.split("|");
	tabId.reverse();
	
	if(level < tabId.length){
		
		var catid = tabId[level];
		
		var theData = {
			catid : catid,
			level: level
		};
		
		$.ajax({
			type : 'POST',
			url  : '/app.php/showcatGP',
			data : theData,
			success : function (data) {
				$("#treeview_avahis #" + catid).addClass('open');
				$("#treeview_avahis #" + catid).removeClass('close');
				$("#treeview_avahis #" + catid).addClass('expanded');
				if(data != "")$("#treeview_avahis #" + catid).prev().addClass('tree-expander-expanded');
				$("#treeview_avahis #" + catid).after(data);
				level = level + 1 ;
				recursiveCatGP(catString, level);
			}
		});		
	}
}


function recursiveCatCheckBox (catString, level) {
	
	var tabId = catString.split("|");
	tabId.reverse();
	
	if(level < tabId.length){
		
		var catid = tabId[level];
		
		var theData = {
			catid : catid,
			level: level
		};
		
		$.ajax({
			type : 'POST',
			url  : '/app.php/showtreecheckbox',
			data : theData,
			success : function (data) {
				$("#checkboxtree #p" + catid).removeClass('close');
				$("#checkboxtree #p" + catid).addClass('open');
				$("#checkboxtree #p" + catid).prop('checked', true);
				$("#checkboxtree #p" + catid).next().addClass('expanded');
				$("#checkboxtree #p" + catid).next().after(data);
				level = level + 1 ;
				recursiveCatCheckBox(catString, level);
			}
		});		
	}
}


function recursiveCatEcom (catString, level) {
	
	var tabId = catString.split(">");

	var count = 0;
	var finalIdCat ="";
	while(level < tabId.length && count <= level){
		if(count == 0){
			finalIdCat = tabId[count].trim();
		}else{
			finalIdCat = finalIdCat + " > " + tabId[count].trim();
		};
		count = count + 1;
	}
	
	if(level < tabId.length){
		showSubCat($("[id='"+finalIdCat+"']"));
		level = level + 1 ;
		recursiveCatEcom (catString, level);
	}
}


/*
 * Simuler clic sur une catégorie dans l'arbre de catégorie
 * pour les rechargements et garder l'arborescence
 */
function loadcat(idCat) {

	$('#treeview_avahis .open').each(function(){
		$(this).removeClass('expanded');
		$(this).removeClass('open');
		$(this).next().html('');
		$(this).prev().removeClass('tree-expander-expanded');
	});
	
	//tabId est un array contenant les id à la suite de chaque catégories à ouvrir
	var tabId = idCat.split("|");
	tabId.reverse();
	var level = 0;
	
	//Fonction récursive permettant d'ouvrir toutes les catégories
	recursiveCat(idCat, level);
		
	var theData2 = {
    	category : tabId[tabId.length -1],
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showenteteavahis",
			data : theData2,
			success : function(data) {
				$('#catalogue_avahis .inform_action').empty();
				$('#catalogue_avahis .inform_action').html(data);
			}
	});
    
    loadProdListFilteredAv("grid");
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showdynafilterav",
			data : theData2,
			success : function(data) {
				$('#catalogue_avahis .dyna_filters').empty();
				$('#catalogue_avahis .dyna_filters').append(data);
			}
	}); 
	
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/showbrandfilterav",
		data : theData2,
		success : function(data) {
			$('#catalogue_avahis .brand_filters').html(data);
		}
	}); 
	
}

/*
 * Simuler clic sur une catégorie dans l'arbre de catégorie
 * pour les rechargements et garder l'arborescence GP
 */
function loadcatGP(idCat) {
	
	$('#treeview_avahis .open').each(function(){
		$(this).removeClass('expanded');
		$(this).removeClass('open');
		$(this).next().html('');
		$(this).prev().removeClass('tree-expander-expanded');
	});
	
	//tabId est un array contenant les id à la suite de chaque catégories à ouvrir
	var tabId = idCat.split("|");
	tabId.reverse();
	var level = 0;
	
	//Fonction récursive permettant d'ouvrir toutes les catégories
	recursiveCatGP(idCat, level);
		
	var theData2 = {
    	category : tabId[tabId.length -1],
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showenteteavahisGP",
			data : theData2,
			success : function(data) {
				$('#catalogue_avahis .inform_action').empty();
				$('#catalogue_avahis .inform_action').html(data);
			}
	});
    
    loadProdListFilteredGP("grid");
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showdynafilterav",
			data : theData2,
			success : function(data) {
				$('#catalogue_avahis .dyna_filters').empty();
				$('#catalogue_avahis .dyna_filters').append(data);
			}
	}); 
	
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/showbrandfilteravGP",
		data : theData2,
		success : function(data) {
			$('#catalogue_avahis .brand_filters').html(data);
		}
	}); 
	
}


function loadcheckboxcat (idCat) {
	$('#checkboxtree .open').each(function(){
		$(this).next().removeClass('expanded');
		$(this).removeClass('open');
		$(this).prop('checked', false);
		$(this).next().next().html('')
	});
	
	//tabId est un array contenant les id à la suite de chaque catégories à ouvrir
	var tabId = idCat.split("|");
	tabId.reverse();
	var level = 0;
	
	//Fonction récursive permettant d'ouvrir toutes les catégories
	recursiveCatCheckBox(idCat, level);
	
}

function majNbProdCatTree (context) {
	//var cat = $(elm).prev().attr('id');
	
	$('span.countProdContext').each(function(){
		var cat = $(this).prev().attr('id');
		var obj = $(this);
		theData = {
			context : context,
			cat_id : cat
		};
		
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/countprodcontext",
			data : theData,
			beforeSend: function() {
				$(obj).html('(   )');
				$(obj).addClass("tree-expander-load");
		  	},
		  	complete: function(){
		    	$(obj).removeClass("tree-expander-load");
		  	},
			success : function(data) {
				$(obj).html("("+data+")");
			}
		}); 
	}); 
}

function showSubCat (elm) {
	
	if($(elm).hasClass('open')){
		$(elm).parent().siblings().children('.expanded').removeClass('open');
		$(elm).parent().siblings().children('.expanded').removeClass('expanded');
		$(elm).parent().siblings().children('.tree-expander-expanded').removeClass('tree-expander-expanded');
		$(elm).parent().siblings().next('ul').hide("fast");
		$(elm).parent().next("ul").hide("fast");
  		$(elm).removeClass('open');
  		$(elm).removeClass('expanded');
  		$(elm).prev().removeClass('tree-expander-expanded');
  	}
  	else{
  		$(elm).parent().siblings().children('.expanded').removeClass('open');
  		$(elm).parent().siblings().children('.expanded').removeClass('expanded');
  		$(elm).parent().siblings().children('.tree-expander-expanded').removeClass('tree-expander-expanded');
  		$(elm).parent().siblings().next('ul').hide("fast");
  		$(elm).parent().next("ul").show("fast");
  		$(elm).addClass('open');
  		$(elm).addClass('expanded');
  		$(elm).prev().addClass('tree-expander-expanded');	
  	}
}

function showSubCatIcon (elm) {
	
	if($(elm).next().hasClass('open')){
		$(elm).parent().siblings().children('.expanded').removeClass('open');
		$(elm).parent().siblings().children('.expanded').removeClass('expanded');
		$(elm).parent().siblings().children('.tree-expander-expanded').removeClass('tree-expander-expanded');
		$(elm).parent().siblings().next('ul').hide("fast");
		$(elm).parent().next("ul").hide("fast");
		$(elm).next().removeClass('open');
  		$(elm).next().removeClass('expanded');
  		$(elm).removeClass('tree-expander-expanded');
  	}
  	else{
  		$(elm).parent().siblings().children('.expanded').removeClass('open');
  		$(elm).parent().siblings().children('.expanded').removeClass('expanded');
  		$(elm).parent().siblings().children('.tree-expander-expanded').removeClass('tree-expander-expanded');
  		$(elm).parent().siblings().next('ul').hide("fast");
  		$(elm).parent().next("ul").show("fast");
  		$(elm).next().addClass('open');
  		$(elm).next().addClass('expanded');
  		$(elm).addClass('tree-expander-expanded');	
  	}
}

function testValidSelect2(){
	
	var theData = {
		cat_id : $("#selectbox-o").val()
	} ;
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validselect2",
			data : theData,
			success : function(data) {
				loadcat(data);
			}
	}); 
}

function testValidSelect2GP(){
	
	var theData = {
		cat_id : $("#selectbox-o").val()
	} ;
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validselect2",
			data : theData,
			success : function(data) {
				loadcatGP(data);
			}
	}); 
}


function validSelect2Popup(){
	
	var theData = {
		cat_id : $("#selectbox-popup").val()
	} ;
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validselect2",
			data : theData,
			success : function(data) {
				loadcheckboxcat(data);
			}
	}); 
}


/*
 * Permet de charger la catégorie suivante en ajax pour
 * l'arbre de catégorie Avahis avec checkbox de selection
 */
function loadnextcatcheckbox(elm) {
	
	var classelm = $(elm).attr('class');
	tmp = classelm.split(' ');
	tmp = tmp[0].split( '_' );
	
	var id=elm.id;
	id = id.substr(1, id.length);
	
	var theData = {
		catid : id,
		level : tmp[1]
	};
	
	if ($(elm).hasClass('open')) {
		$(elm).next().next().html('');
		$(elm).addClass('close');
		$(elm).removeClass('open');
		$(elm).next().removeClass('expanded');
		
		
	} else {
				
		var id = elm.id;
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/showtreecheckbox",
			data : theData,
			success : function(info) {
				
				$('#checkboxtree .open.'+classelm).each(function(){
					$(this).next().removeClass('expanded');
					$(this).removeClass('open');
					$(this).next().next().html('');
				});
				
				$(elm).addClass('open');
				$(elm).next().addClass('expanded');
				$(elm).next().after(info);
				
			}
		});
		
	}
}

//permet de savoir si on est en train de visionner les produits associés refusés à traiter ou tous
function changeContext (context, elm) {
   
   $('#context').text(context);
   var theData ={
   		context:context	
   }
   
   //var_dump(theData);
   jQuery.ajax({
			type : 'POST',
			url : "/app.php/changecontext",
			data : theData,
			success : function(data) {
				
			}
	});
   
}



/*
 * Fonction qui charge la liste des produits Ecommercant avec les
 * bonnes préférences de vue grid ou list
 */
function loadProductList (view) {
    
    var context = $('#context').text();
    
    if(context == "selected" ){
    	context = "todo";
		$('#context').text("todo");
    };
    
    var theData = {
    	t : $('#tech').text(),
    	id : $('#id').text(),
    	view : view,
    	id_cat_assoc : $('#id_cat_assoc').text(),
    	context : context
    } ;
    
    var typeVendor = $("#typeVendor").text();
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showprodlist",
			data : theData,
			beforeSend: function() {
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant #prods_ecommercant').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_ecom_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_commercant .brand_filters').show();
				$('#catalogue_commercant .dyna_filters').show();
				$('#catalogue_commercant .filter .filters .button').show();
				$('#catalogue_commercant .filter .filters .views').show();
				$('#catalogue_commercant .filter .actions').hide();
				$('#catalogue_commercant #prods_ecommercant').attr('class', view+'view');
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant #prods_ecommercant').append(data);
				$( "#treeview_commercant ul span:not(.tree-expander):not(.tree-expander-empty):not(.countProdContext)" ).each( function() {
  																	$(this).attr( "onClick", "loadEnteteEcommercant(this); loadProductList('"+view+"'); loadFilter"+typeVendor+"(this); loadBrandFilter(this);showSubCat(this)" );
																	});
				loadNavigation($('#cat_ecom').text()); 
				$('#btnAddAllCat').hide();
				if ($('#context').text() == "todo"){
					$('#btnAddAllCat').show();
				}
			}
	});
	
}

/*
 * Fonction permettant de charger les filtres dynamiques
 * des attributs de la catégorie ecommercant en cours
 */
function loadFilter (elm) {

    var theData = {
    	category : elm.id,
    	t : $('#tech').text(),
    	id : $('#id').text()
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showdynafilter",
			data : theData,
			success : function(data) {
				$('#catalogue_commercant .dyna_filters').empty();
				$('#catalogue_commercant .dyna_filters').append(data);
			}
	}); 
}

/*
 * Fonction permettant de charger les filtres dynamiques
 * des attributs de la catégorie ecommercant en cours
 */
function loadFilterGrossiste (elm) {

    var theData = {
    	category : elm.id,
    	t : $('#tech').text(),
    	id : $('#id').text()
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showdynafilter?type=grossiste",
			data : theData,
			success : function(data) {
				$('#catalogue_commercant .dyna_filters').empty();
				$('#catalogue_commercant .dyna_filters').append(data);
			}
	}); 
}

/*
 * Fonction permettant de charger les filtres dynamiques
 * des attributs de la catégorie Avahis en cours
 */
function loadFilterAv (elm) {

    var theData = {
    	category : elm.id,
    	t : $('#tech').text(),
    	id : $('#id').text()
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showdynafilterav",
			data : theData,
			success : function(data) {
				$('#catalogue_avahis .dyna_filters').empty();
				$('#catalogue_avahis .dyna_filters').append(data);
			}
	}); 
}


/*
 * Fonction permettant de charger la liste des marques pour la catégorie
 * ecommercant.
 */
function loadBrandFilter (elm) {

    var theData = {
    	category : elm.id,
    	t : $('#tech').text(),
    	id : $('#id').text()
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showbrandfilter",
			data : theData,
			success : function(data) {
				$('#catalogue_commercant .brand_filters').empty();
				$('#catalogue_commercant .brand_filters').append(data);
			}
	}); 
}


/*
 * Fonction permettant de charger la liste des marques pour la catégorie
 * Avahis.
 */
function loadBrandFilterAv (elm) {

    var theData = {
    	category : elm.id
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showbrandfilterav",
			data : theData,
			success : function(data) {
				$('#catalogue_avahis .brand_filters').html(data);
			}
	}); 
}
/*
 * Fonction permettant de charger la liste des marques pour la catégorie
 * Avahis.
 */
function loadBrandFilterAvGP (elm) {

    var theData = {
    	category : elm.id
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showbrandfilteravGP",
			data : theData,
			success : function(data) {

				$('#catalogue_avahis .brand_filters').html(data);
			}
	}); 
}

/*
 * Fonction permettant d'afficher les informations nécessaires pour l'entete
 * ecommercant
 */
function loadEnteteEcommercant (elm) {
    
    var theData = {
    	category : elm.id,
    	t : $('#tech').text(),
    	id : $('#id').text()
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showentetecommercant",
			data : theData,
			success : function(data) {
				$('#catalogue_commercant .inform_action').empty();
				$('#catalogue_commercant .inform_action').append(data);
				$(elm).addClass('selected');
			}
	}); 
	
}

function loadNavigation (id) {
	
	var theData = {
    	category : id
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/shownavigation",
			data : theData,
			success : function(data) {
				$('#catalogue_commercant .navigation').empty();
				$('#catalogue_commercant .navigation').append(data);
				showActiveOng();
			}
	});
	
	
}
function showActiveOng () {
	if($('#context').text() == "all"){
		$('#ongAll').addClass('active');
	}
	if($('#context').text() == "assoc"){
		$('#ongAssoc').addClass('active');
	}
	if($('#context').text() == "refused"){
		$('#ongRefused').addClass('active');
	}
	if($('#context').text() == "todo"){
		$('#ongTodo').addClass('active');
	}
	if($('#context').text() == "selected"){
		$('#ongSelected').addClass('active');
	}
	if($('#context').text() == "deleted"){
		$('#ongDeleted').addClass('active');
	}
}

/*
 * Fonction permettant d'afficher les informations nécessaires pour l'entete
 * avahis
 */
function loadEnteteAvahis (elm) {
    var theData = {
    	category : elm.id,
    } ;
    //var_dump(theData);
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showenteteavahis",
			data : theData,
			success : function(data) {
				$('#catalogue_avahis .inform_action').empty();
				$('#catalogue_avahis .inform_action').html(data);
			}
	}); 
}
/*
 * Fonction permettant d'afficher les informations nécessaires pour l'entete
 * avahis
 */
function loadEnteteAvahisGP (elm) {
    var theData = {
    	category : elm.id,
    } ;

    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showenteteavahisGP",
			data : theData,
			success : function(data) {
				$('#catalogue_avahis .inform_action').empty();
				$('#catalogue_avahis .inform_action').html(data);
			}
	}); 
}

/*
 * Fonction qui charge la liste des produits Avahis avec les
 * bonnes préférences de vue grid ou list
 */
function loadProductListAv (view) {

    var theData = {   	
    	view  : view
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showprodlistav",
			data : theData,
			beforeSend: function() {
				$('#catalogue_avahis #prods_avahis').empty();
				$('#catalogue_avahis #prods_avahis').append('<div id="prod_av_loader"></div>');
		     	$('#prod_av_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_av_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_avahis #prods_avahis').attr('class', view+'view');
				$('#catalogue_avahis #prods_avahis').empty();
				$('#catalogue_avahis #prods_avahis').append('<div id="prod_av_loader"></div>');
				$('#catalogue_avahis #prods_avahis').append(data);
				$('#treeview_avahis ul span:not(.tree-expander)' ).each( function() {
  					$(this).attr( "onClick", "loadnextcat(this);loadEnteteAvahis(this);loadProductListAv('"+view+"');loadFilterAv(this);loadBrandFilterAv(this);" );
																	});
			}
	}); 
}
/*
 * Fonction qui charge la liste des produits Avahis GP avec les
 * bonnes préférences de vue grid ou list
 */
function loadProductListAvGP (view) {
	var context = $('#context').text();
	if(context == "selected" ){
    	context = "all";
		$('#context').text("all");
    };
    var theData = {   	
    	view  : view,
    	context : context
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showprodlistavGP",
			data : theData,
			beforeSend: function() {
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append('<div id="prod_av_loader"></div>');
		     	$('#prod_av_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_av_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_avahis .brand_filters').show();
				$('#catalogue_avahis .dyna_filters').show();
				$('#catalogue_avahis .filter .filters .button').show();
				$('#catalogue_avahis .filter .filters .views').show();
				$('#catalogue_avahis .filter .actions').hide();
				$('#catalogue_avahis #prods_avahisGP').attr('class', view+'view');
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append('<div id="prod_av_loader"></div>');
				$('#catalogue_avahis #prods_avahisGP').append(data);
				$('#treeview_avahis ul span:not(.tree-expander)' ).each( function() {
  				$(this).attr( "onClick", "loadnextcatGP(this);loadEnteteAvahisGP(this);loadProductListAvGP('"+view+"');loadFilterAv(this);loadBrandFilterAvGP(this);" );
																	});
				loadNavigation($('#catalogue_avahis').text()); 
				$('#btnsupAll').hide();
				if ($('#context').text() == "deleted"){
					$('#btnsupAll').show();
				}
				$('.navigation_gp .active').each(function(){
					$(this).removeClass('active');
				});
				showActiveOng();
						
				$(".embed").each(function(){
					$(this).hide();
				});

				$('#main-content-area').show();
				$('#ongAll').addClass("active");											
			}
	}); 
}
/*
 * Fonction gérant l'affichage avec prise en compte grid ou list des produits
 * unqiuement séléctionnés par l'utilisateur pour l'écran grestion/produit
 */
function loadSelectedProductListAvGP (view) {
    
    var theData = {
    	view : view
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showselectedprodlistAvGP",
			data : theData,
						beforeSend: function() {
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append('<div id="prod_av_loader"></div>');
		     	$('#prod_av_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_av_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_avahis #prods_avahisGP').attr('class', view+'view');
				$('#catalogue_avahis #prods_avahisGP').empty();
				
				$('#catalogue_avahis .filter .actions').show();
				$('#catalogue_avahis .filter .filters .views').hide();
				$('#catalogue_avahis .brand_filters').hide();
				$('#catalogue_avahis .dyna_filters').hide();
				$('#catalogue_avahis .filter .filters .button').hide();
				
				$('#catalogue_avahis #prods_avahisGP').append('<div id="prod_av_loader"></div>');
				$('#catalogue_avahis #prods_avahisGP').append(data);
				$('#treeview_avahis ul span:not(.tree-expander)' ).each( function() {
  					$(this).attr( "onClick", "loadnextcatGP(this);loadEnteteAvahisGP(this);loadProductListAvGP('"+view+"');loadFilterAv(this);loadBrandFilterAvGP(this);" );
																	});

				$('.navigation_gp .active').each(function(){
					$(this).removeClass('active');
				});
				showActiveOng();			
			}
	}); 
	
}



/*
 * Fonction permettant d'afficher la liste des produits de la catégorie avec prise en compte
 * des filtres de recherche renseignés et de la vue en cours grid ou list pour
 * les produits Ecommercant
 */
function loadProdListFilteredEcom(view, order) {
	
	var b = $('#catalogue_commercant .brand_filters select option:selected').val();
	var attr = { "attr1" : {"id" : $('#catalogue_commercant .dyna_filters .attr1').attr('id'),  "val" : $('#catalogue_commercant .dyna_filters .attr1 option:selected').val()},
				 "attr2" : {"id" : $('#catalogue_commercant .dyna_filters .attr2').attr('id'),  "val" : $('#catalogue_commercant .dyna_filters .attr2 option:selected').val()},
				 "attr3" : {"id" : $('#catalogue_commercant .dyna_filters .attr3').attr('id'),  "val" : $('#catalogue_commercant .dyna_filters .attr3 option:selected').val()}};
	var priceMin = $('#catalogue_commercant .brand_filters .min_price').val();
	var priceMax = $('#catalogue_commercant .brand_filters .max_price').val();
	var search   = $('#catalogue_commercant .brand_filters .search').val();

	var theData = {
    	
    	brand  : b,
    	attr  : attr,
    	pmin : priceMin,
    	pmax : priceMax,
    	search : search,
    	order : order,
    	context : $('#context').text()
    	
    } ;
    
    var typeVendor = $('#typeVendor').text();
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showprodlistfiltered",
			data : theData,
			beforeSend: function() {
				$('#catalogue_commercant .button').attr('disabled', true);
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant #prods_ecommercant').append('<div id="prod_ecom_loader"></div>');
		     	$('#prod_ecom_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_ecom_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant #prods_ecommercant').append(data);
				$( "#treeview_commercant ul span:not(.tree-expander):not(.tree-expander-empty):not(.countProdContext)" ).each( function() {
					$(this).attr( "onClick", "loadEnteteEcommercant(this); loadProductList('"+view+"'); loadFilter"+typeVendor+"(this); loadBrandFilter(this);showSubCat(this);" );
					});
				loadNavigation($('#cat_ecom').text()); 
				$("#btnAddAllCat").hide();
				if ($('#context').text() == "todo"){
					$('#btnAddAllCat').show();
				}
			}
	}); 
}



/*
 * Fonction permettant d'afficher la liste des produits de la catégorie avec prise en compte
 * des filtres de recherche renseignés et de la vue en cours grid ou list pour
 * les produits Avahis
 */
function loadProdListFilteredAv(view, order) {

	var b = $('#catalogue_avahis .brand_filters #selectManufac').val();
	var searchVendor = $('#catalogue_avahis .brand_filters #selectVendor').val();
	var attr = { "attr1" : {"id" : $('#catalogue_avahis .dyna_filters .attr1').attr('id'),  "val" : $('#catalogue_avahis .dyna_filters .attr1 option:selected').val()},
				 "attr2" : {"id" : $('#catalogue_avahis .dyna_filters .attr2').attr('id'),  "val" : $('#catalogue_avahis .dyna_filters .attr2 option:selected').val()},
				 "attr3" : {"id" : $('#catalogue_avahis .dyna_filters .attr3').attr('id'),  "val" : $('#catalogue_avahis .dyna_filters .attr3 option:selected').val()}};
	var priceMin 		= $('#catalogue_avahis .brand_filters .min_price').val();
	var priceMax 		= $('#catalogue_avahis .brand_filters .max_price').val();
	var search   		= $('#catalogue_avahis .brand_filters .search').val();

	var allProd   		=$('#cB_all_Prods').prop('checked') ? 1 : 0;
	var allAsso   		=$('#cB_all_Assoc').prop('checked') ? 1 : 0;
	var allPoid   		=$('#cB_all_zero').prop('checked') ? 1 : 0;
	var allVendu   		=$('#cB_all_vendu').prop('checked') ? 1 : 0;

	var theData = {
    	
    	brand  : b,
    	attr  : attr,
    	pmin : priceMin,
    	pmax : priceMax,
    	search : search,
    	order : order,
    	allProd : allProd,
    	allAsso : allAsso,
    	allPoid : allPoid,
    	allVendu : allVendu,
    	searchVendor : searchVendor
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showprodlistfilteredav",
			data : theData,
			beforeSend: function() {
				$('#catalogue_avahis #prods_avahis').empty();
				$('#catalogue_avahis #prods_avahis').append('<div id="prod_av_loader"></div>');
		     	$('#prod_av_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_av_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_avahis #prods_avahis').empty();
				$('#catalogue_avahis #prods_avahis').append(data);
				$( "#treeview_avahis ul span:not(.tree-expander)" ).each( function() {
					$(this).attr( "onClick", "loadnextcat(this);loadEnteteAvahis(this);loadProductListAv('"+view+"');loadFilterAv(this);loadBrandFilterAv(this);" );
					});
			}
	}); 

}
/*
 * Fonction permettant d'afficher la liste des produits de la catégorie avec prise en compte
 * des filtres de recherche renseignés et de la vue en cours grid ou list pour
 * les produits Avahis GP
 */
function loadProdListFilteredGP(view, order) {
	

	var b = $('#catalogue_avahis .brand_filters #selectManufac').val();
	var searchVendor = $('#catalogue_avahis .brand_filters #selectVendor').val();
	var attr = { "attr1" : {"id" : $('#catalogue_avahis .dyna_filters .attr1').attr('id'),  "val" : $('#catalogue_avahis .dyna_filters .attr1 option:selected').val()},
				 "attr2" : {"id" : $('#catalogue_avahis .dyna_filters .attr2').attr('id'),  "val" : $('#catalogue_avahis .dyna_filters .attr2 option:selected').val()},
				 "attr3" : {"id" : $('#catalogue_avahis .dyna_filters .attr3').attr('id'),  "val" : $('#catalogue_avahis .dyna_filters .attr3 option:selected').val()}};
				 
	var priceMin 		= $('#catalogue_avahis .brand_filters .min_price').val();
	var priceMax 		= $('#catalogue_avahis .brand_filters .max_price').val();
	var search   		= $('#catalogue_avahis .brand_filters .search').val();
	
	var allProd   		= $('#cB_all_Prods').prop('checked') ? 1 : 0;
	var allAsso   		= $('#cB_all_Assoc').prop('checked') ? 1 : 0;
	var allPoid   		= $('#cB_all_zero').prop('checked') ? 1 : 0;
	var allVendu   		= $('#cB_all_vendu').prop('checked') ? 1 : 0;
	var allActif   		= $('#cB_all_Actif').prop('checked') ? 1 : 0;

	var theData = {
    	
    	brand  : b,
    	attr  : attr,
    	pmin : priceMin,
    	pmax : priceMax,
    	search : search,
    	order : order,
    	allProd : allProd,
    	allAsso : allAsso,
    	allPoid : allPoid,
    	allVendu : allVendu,
    	allActif : allActif,
    	searchVendor : searchVendor
    } ;

    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showprodlistfilteredavGP",
			data : theData,
			beforeSend: function() {
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append('<div id="prod_av_loader"></div>');
		     	$('#prod_av_loader').show();
		  	},
		  	complete: function(){
		    	$('#prod_av_loader').hide();
		  	},
			success : function(data) {
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append(data);
				$( "#treeview_avahis ul span:not(.tree-expander)" ).each( function() {
					$(this).attr( "onClick", "loadnextcatGP(this);loadEnteteAvahisGP(this);loadProductListAvGP('"+view+"');loadFilterAv(this);loadBrandFilterAvGP(this);" );
					});
			}
	}); 
}


/*
 * Fonction permettant d'afficher l'arbre de catégories pour l'ecommercant
 */
function loadCatEcom() {
	
	var theData = {
		context : $('#context').text()
	};
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/showcatCli",
		data : theData,
		beforeSend: function() {

			$('#treeview_commercant .well').empty();
			$('#treeview_commercant .well').append('<div id="cat_ecom_loader"></div>');
		
	  	},
	  	complete: function(){

	  	},
		success : function(data) {
			var obj = jQuery.parseJSON(data);
			$('#treeview_commercant .well ul').remove();
			$('#treeview_commercant .well').append(obj.treeview);
			recursiveCatEcom (obj.cat, 0);
		}
	});
	
}


/*
 * Fonction permettant d'appeller en Ajax les actions nécessaires à l'ajout en selection
 * d'un produit. Avec gestion de l'affichage et changement de couleur des fiches produit
 * déja selectionnées
 */
function selectProduct (elm) {
	
	if ($(elm).text() == "Sélection"){
		var prodId = $(elm).parent().parent().parent().parent().attr('id');

		var theData = {
    	id : prodId,
    	} ;
    
   	 	jQuery.ajax({
			type : 'POST',
			url : "/app.php/selectproduct",
			data : theData,
			success : function(data) {
				$(elm).parent().parent().parent().parent().addClass('selected');
				$(elm).text('Désélect.');
				$('#prod_selected').text('Sélection (' +data+ ')');
			}
		}); 
	};
	
	if ($(elm).text() == "Désélect."){

		var prodId = $(elm).parent().parent().parent().parent().attr('id');
		var theData = {
    	id : prodId,
    	} ;
    
   	 	jQuery.ajax({
			type : 'POST',
			url : "/app.php/deselectproduct",
			data : theData,
			success : function(data) {
				$(elm).parent().parent().parent().parent().removeClass('selected');
				$(elm).text('Sélection');
				$('#prod_selected').text('Sélection (' +data+ ')');
			}
		}); 
	};	
}


function selectAllProducts (elm) {
	
	var theData = {};
	
	if( $(elm).text() == "Select all"){
		
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/selectallproduct",
				data : theData,
				success : function(data) {
					$('#prods_ecommercant .produit').each(function(){
						$(this).addClass('selected');
						$('#prod_selected').text('Sélection (' +data+ ')');
						$('#'+this.id+' .actions a:first').text('Désélect.');
					});
				}
			});
	}
	else if ( $(elm).text() == "Deselect all"){
		
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/selectallproduct?a=undo",
				data : theData,
				success : function(data) {
					$('#prods_ecommercant .produit').each(function(){
						$(this).removeClass('selected');
						$('#prod_selected').text('Sélection (' +data+ ')');
						$('#'+this.id+' .actions a:first').text('Sélection');
					});
					$(elm).text('Select all');
				}
			});
	}
}


/**
 *Version uniquement deselection pour l'onglet produits séléctionnés 
 */
function deSelectAllProducts (elm) {
	var theData = {};
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/selectallproduct?a=undo",
			data : theData,
			success : function(data) {
				$('#prods_ecommercant .produit').each(function(){
					$(this).removeClass('selected');
					$('#prod_selected').text('Sélection (' +data+ ')');
					$('#'+this.id+' .actions a:first').text('Sélection');
				});
				$('#btnSelectAll').text('Select all');
			}
		});	
}


/**
 *selection tous les produit avahis de GESTION/produit 
 */
function selectAllProductsAvGP (elm) {
	
	var theData = {};
	
	if( $(elm).text() == "Select all"){
		
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/selectallproductAvGP",
				data : theData,
				success : function(data) {
					$('#prods_avahisGP .produit').each(function(){
						$(this).addClass('selected');
						$('#prod_selected').text('Sélection (' +data+ ')');
						$('#'+this.id+' .actions a:first').text('Désélect.');
					});
					$(elm).text('Desele all');
	
				}
			});
	}
	else if ( $(elm).text() == "Desele all"){
		
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/selectallproductAvGP?a=undo",
				data : theData,
				success : function(data) {
					$('#prods_avahisGP .produit').each(function(){
						$(this).removeClass('selected');
						$('#prod_selected').text('Sélection (' +data+ ')');
						$('#'+this.id+' .actions a:first').text('Sélection');
					});
					$(elm).text('Select all');
				}
			});
	}
}


/**
 *Version uniquement deselection pour l'onglet produits séléctionnés GP
 */
function deSelectAllProductsAvGP (elm) {
	var theData = {};
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/selectallproductAvGP?a=undo",
			data : theData,
			success : function(data) {
				$('#prods_avahisGP .produit').each(function(){
					$(this).removeClass('selected');
					$('#prod_selected').text('Sélection (' +data+ ')');
					$('#'+this.id+' .actions a:first').text('Sélection');
				});
			}
		});	
}


/*
 * Fonction gérant l'affichage avec prise en compte grid ou list des produits
 * unqiuement séléctionnés par l'utilisateur
 */
function loadSelectedProductList (view) {
    
    var theData = {
    	t : $('#tech').text(),
    	id : $('#id').text(),
    	view : view
    } ;
    
    jQuery.ajax({
			type : 'POST',
			url : "/app.php/showselectedprodlist",
			data : theData,
			success : function(data) {
				$('#catalogue_commercant #prods_ecommercant').attr('class', view+'view');
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant .filter .actions').show();
				$('#catalogue_commercant .filter .filters .views').hide();
				$('#catalogue_commercant .brand_filters').hide();
				$('#catalogue_commercant .dyna_filters').hide();
				$('#catalogue_commercant .filter .filters .button').hide();
				$('#catalogue_commercant #prods_ecommercant').append(data);
				$( "#treeview_commercant ul span:not(.tree-expander):not(.tree-expander-empty):not(.countProdContext)" ).each( function() {
  																	$(this).attr( "onClick", "loadEnteteEcommercant(this); loadProductList('"+view+"'); loadFilter(this); loadBrandFilter(this);showSubCat(this)" );
																	});
				$('.navigation .active').each(function(){
					$(this).removeClass('active');
				});
				showActiveOng();			
			}
	}); 
	
}


/*
 * Fonction permettant de flag un produit en tant que refusé
 */
function refuseProduct (elm) {
	
	if($(elm).text() == "Refuser"){
		var prodId = $(elm).parent().parent().attr('id');
		
		var theData = {
			t : $('#tech').text(),
	    	vendor : $('#id').text(),
	    	id : prodId,
	    } ;
	    
	    var $myDialog = $('<div></div>')
		    		.html('Êtes vous sûr de vouloir refuser ce produit ?<br/><br/>Ok pour confirmer.  Annuler pour annuler.')
		    		.dialog({
		    			autoOpen: false,
		    			title: 'Refus de fiche produit',
		    			buttons: {
							      	"OK": function () {
							      		jQuery.ajax({
											type : 'POST',
											url : "/app.php/refuseproduct",
											data : theData,
											success : function(data) {
												$(elm).parent().parent().addClass('refused');
							      				$(elm).siblings(':not(.btnSelect)').hide();
							      				$(elm).text("Annuler refus");
												
											}
										});
							      		loadNavigation($('#cat_ecom').text());
										loadCatEcom()
							      		$(this).dialog("destroy");
							      		
									    return true;
							      		},
						      		"Cancel": function () {
							        	$(this).dialog("destroy");
							        	return false;
						      		}
		    				}
		  			});
		$myDialog.dialog('open');
	}
	
	if($(elm).text() == "Annuler refus"){
		var prodId = $(elm).parent().parent().attr('id');
		
		var theData = {
			t : $('#tech').text(),
	    	vendor : $('#id').text(),
	    	id : prodId,
	    } ;
	    
	    var $myDialog = $('<div></div>')
		    		.html('De nouveau autoriser ce produit ?<br/><br/>Ok pour confirmer.  Annuler pour annuler.')
		    		.dialog({
		    			autoOpen: false,
		    			title: 'Autorisation fiche produit',
		    			buttons: {
							      	"OK": function () {
							      		jQuery.ajax({
											type : 'POST',
											url : "/app.php/refuseproduct?a=cancel",
											data : theData,
											success : function(data) {

												$(elm).parent().parent().removeClass('refused');
							      				$(elm).siblings().show();
							      				$(elm).text("Refuser");
							      				loadNavigation($('#cat_ecom').text());
												loadCatEcom();
												
											}
										});
							      		
							      		$(this).dialog("close");
									    return true;
							      		},
						      		"Cancel": function () {
							        	$(this).dialog("close");
							        	return false;
						      		}
		    				}
		  			});
		$myDialog.dialog('open');
	}
}


/*
 * Fonction permettant de flag un produit en tant que supprimé
 */
function supProduct (elm) {

	if($(elm).text() == "Supprimer"){
		var prodId = $(elm).parent().parent().attr('id');
		
		var theData = {
	    	id : prodId
	    } ;
		
	    var $myDialog = $('<div></div>')
		    		.html('Êtes vous sûr de vouloir supprimer ce produit ?<br/><br/>Ok pour confirmer.  Annuler pour annuler.')
		    		.dialog({
		    			autoOpen: false,
		    			title: 'Suppréssion de fiche produit',
		    			buttons: {
							      	"OK": function () {
							      		jQuery.ajax({
											type : 'POST',
											url : "/app.php/supproduct",
											data : theData,
											success : function(data) {
												$(elm).parent().parent().addClass('deleted');
							      				$(elm).siblings(':not(.btnSelect)').hide();
							      				$(elm).text("Annuler suppression");
											}
										});
							      		loadNavigation($('#cat_ecom').text());
										loadCatEcom()
							      		$(this).dialog("destroy");
							      		
									    return true;
							      		},
						      		"Cancel": function () {
							        	$(this).dialog("destroy");
							        	return false;
						      		}
		    				}
		  			});
		$myDialog.dialog('open');
	}
	
	if($(elm).text() == "Annuler suppression"){
		var prodId = $(elm).parent().parent().attr('id');
		
		var theData = {
			t : $('#tech').text(),
	    	vendor : $('#id').text(),
	    	id : prodId,
	    } ;
	    
	    var $myDialog = $('<div></div>')
		    		.html('De nouveau autoriser ce produit ?<br/><br/>Ok pour confirmer.  Annuler pour annuler.')
		    		.dialog({
		    			autoOpen: false,
		    			title: 'Autorisation fiche produit',
		    			buttons: {
							      	"OK": function () {
							      		jQuery.ajax({
											type : 'POST',
											url : "/app.php/supproduct?a=cancel",
											data : theData,
											success : function(data) {

												$(elm).parent().parent().removeClass('deleted');
							      				$(elm).siblings().show();
							      				$(elm).text("Supprimer");
							      				loadNavigation($('#cat_ecom').text());
												loadCatEcom();
												
											}
										});
							      		
							      		$(this).dialog("close");
									    return true;
							      		},
						      		"Cancel": function () {
							        	$(this).dialog("close");
							        	return false;
						      		}
		    				}
		  			});
		$myDialog.dialog('open');
	}
}


/**
 * Supprime tous les produits sélectionnés
 *  
 */
function supAllSelectedProds (elm) {

	if($(elm).text() == "Delete All"){
		if($('#nbProdAv').text() != "" && $('#nbProdAv').text() != "0" ){
			var $myDialog = $('<div></div>')
		    		.html('Êtes vous sûr de vouloir supprimer ces <b>'+$('#nbProdAv').text()+'</b> produits ?<br/><br/>Ok pour confirmer.')
		    		.dialog({
		    			autoOpen: false,
		    			title: 'Validation de la suppression',
		    			buttons: {
									"OK": function () {
										theData = {
										};
										jQuery.ajax({
												type : 'POST',
												url : "/app.php/supallselectedproduct",
												data : theData,
												success : function(data) {
													
													loadNavigation($('#cat_ecom').text());
										    		loadSelectedProductListAvGP("grid");
													$myDialog.dialog("close");
								    				return true;
													//loadCatEcom()
													//var info = jQuery.parseJSON(data);
													
												}
										}); 
										 
										
							      	},
						      		"Annuler": function () {
							        	$(this).dialog("close");
							        	return false;
						      		}
		    				}
		  			});
			$myDialog.dialog('open');
	}
	else{
		alert('Aucun produit supprimé !');
	}
	}
	if($(elm).text() == "Undelete All"){
		if($('#nbProdAv').text() != "" && $('#nbProdAv').text() != "0" ){
		var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir dé-supprimer ces <b>'+$('#nbProdAv').text()+'</b> produits ?<br/><br/>Ok pour confirmer.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de la désuppression',
	    			buttons: {
								"OK": function () {
									theData = {
									};
									jQuery.ajax({
											type : 'POST',
											url : "/app.php/unsupallselectedproduct",
											data : theData,
											success : function(data) {
												loadNavigation($('#cat_ecom').text());
									    		loadSelectedProductListAvGP("grid");
												$myDialog.dialog("close");
							    				return true;
											}
									}); 
						      	},
					      		"Annuler": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
			$myDialog.dialog('open');
		}
		else{
			alert('Aucun produit dé-supprimé !');
		}
	}	  		
}


/*
 * Fonction permettant d'associer un produit Ecommercant et gérer l'affichage de son nom
 * dans la partie associer de la fenetre, de plus l'appel ajax permet de placer en session'
 * ce produit à associer
 */
function associateProduct (elm) {

	
	if ($(elm).text() == "Associer"){
		var prodId = $(elm).parent().parent().parent().parent().attr('id');
	
		var theData = {
			t : $('#tech').text(),
    		vendor : $('#id').text(),
    		id : prodId,
    	} ;
   		
   		jQuery.ajax({
			type : 'POST',
			url : "/app.php/associateproduct",
			data : theData,
			success : function(data) {
				$('#prodAssocEcom').empty();
				$('#prodAssocEcom').append(prodId);
				$('#catalogue_avahis #assoc1 .product_name').empty();
				$('#catalogue_avahis #assoc1 .product_name').append(data);
				$('#catalogue_avahis #assoc1 ').addClass("alert");
				$(elm).parent().parent().parent().parent().addClass('associated');
				$(elm).text("Annuler");
			}
		}); 
	}
	
	if ($(elm).text() == "Annuler"){
		var prodId = $(elm).parent().parent().attr('id');
		
		var theData = {
			t : $('#tech').text(),
    		vendor : $('#id').text(),
    		id : prodId,
    	} ;
   
   		jQuery.ajax({
			type : 'POST',
			url : "/app.php/associateproduct?a=remove",
			data : theData,
			success : function(data) {
				$('#prodAssocEcom').empty();
				$('#catalogue_avahis #assoc1 .product_name').empty();
				$('#catalogue_avahis #assoc1 .product_name').append(data);
				$(elm).parent().parent().parent().parent().removeClass('associated');
				$('#catalogue_avahis #assoc1 ').removeClass("alert");
				$(elm).text("Associer");
			}
		});
    }
    if ($(elm).text() == "Désassocier"){
		var prodId = $(elm).parent().parent().parent().parent().attr('id');
		
		var theData = {
			t : $('#tech').text(),
    		vendor : $('#id').text(),
    		id : prodId,
    	} ;
   		//Dialog ERREUR
    	var $myConfirm = $('<div></div>').html('Êtes-vous sûr de vouloir désassocier ce produit ?')
    	.dialog({
    			autoOpen: false,
    			title: 'Erreur',
    			buttons: {
				      "OK": function () {
					      	jQuery.ajax({
								type : 'POST',
								url : "/app.php/associateproduct?a=unassoc",
								data : theData,
								success : function(data) {
									$('#prodAssocEcom').empty();
									$('#catalogue_avahis #assoc1 product_name').empty();
									$(elm).parent().parent().parent().parent().removeClass('associated');
									$(elm).text("Associer");
									$(elm).siblings().show();
									loadNavigation($('#cat_ecom').text());
									loadCatEcom()
								}
							});
					      	$(this).dialog("close");
					        return true;
				      },
				      "Cancel": function () {
				        	$(this).dialog("close");
				        	return false;
				      }
    			}
    	});
		$myConfirm.dialog('open');
    }
}


/*
 * Fonction permettant d'associer un produit Avahis et gérer l'affichage de son nom
 * dans la partie associer de la fenetre, de plus l'appel ajax permet de placer en session'
 * ce produit à associer
 */
function associateProductAv (elm) {
	
	if ($(elm).text() == "Associer"){
		var prodId = $(elm).parent().parent().parent().attr('id');
		
		var theData = {
	    		id : prodId,
	    } ;
	   
	   	jQuery.ajax({
				type : 'POST',
				url : "/app.php/associateproductav",
				data : theData,
				success : function(data) {
					$('#prodAssocAv').empty();
					$('#prodAssocAv').append(prodId);
					$('#catalogue_avahis #assoc2 .product_name').empty();
					$('#catalogue_avahis #assoc2 .product_name').append(data);
					$('#catalogue_avahis #assoc2 ').addClass("alert");
					$(elm).parent().parent().parent().addClass('associated');
					$(elm).text("Annuler");
				}
		}); 
	}  
	
	if ($(elm).text() == "Annuler"){
		var prodId = $(elm).parent().parent().parent().attr('id');
		
		var theData = {
    		id : prodId
    	} ;
   
   		jQuery.ajax({
			type : 'POST',
			url : "/app.php/associateproductav?a=remove",
			data : theData,
			success : function(data) {
				$('#prodAssocAv').empty();
				$('#catalogue_avahis #assoc2 product_name').empty();
				$('#catalogue_avahis #assoc2 product_name').append(data);
				$('#catalogue_avahis #assoc2 ').removeClass("alert");
				$(elm).parent().parent().parent().removeClass('associated');
				$(elm).text("Associer");
			}
		});
    } 
}


/*
 * Fonction permettant l'affichage d'un popup de confirmation pour l'association d'un produit
 * Avahis avec un produit ecommercant. Si confirmation, il y a appel en ajax pour le traitement
 * de l'association 
 */
function validAssociation () {
	var prodEcomId = $('#prodAssocEcom').text();
	var prodAvId   = $('#prodAssocAv').text();
	
	var theData = {
    		idEcom : prodEcomId,
    		idAv   : prodAvId
    } ;
    
    //Dialog ERREUR
    var $myError = $('<div></div>').html('Vous devez choisir 2 porduits à associer avant de valider')
    	.dialog({
    			autoOpen: false,
    			title: 'Erreur',
    			buttons: {
					      "OK": function () {
					      	$(this).dialog("close");
					        return true;
					      }
    			}
    });
    
    //DIALOG NORMAL
 	var $myDialog = $('<div></div>')
    	.html('Êtes vous sûr de vouloir associer :<br/><br/><b> '+ $('#assoc1 .product_name').text() +
    		  '.</b><br/><br/>Avec <br/><br/><b>'+ $('#assoc2 .product_name').text() +
    		  '</b><br/><br/>Ok pour confirmer.  Cancel pour annuler.')
    	.dialog({
    			autoOpen: false,
    			position: {my: "right top", at: "right top", of: window},
    			title: 'Validation de l\'association',
    			buttons: {
					      "OK": function () {
					       
					        jQuery.ajax({
										type : 'POST',
										url : "/app.php/validassoc",
										data : theData,
										success : function(data) { 
											$('#prodAssocAv').empty();
											$('#prodAssocEcom').empty();
											$('#catalogue_avahis #assoc2 .product_name').empty();
											$('#catalogue_avahis #assoc1 .product_name').empty();
											$('#prods_ecommercant #'+prodEcomId).remove();
											$('#catalogue_avahis #prods_avahis .associated').each(function(){
													$(this).removeClass("associated");
													$(this).find('.assocButtonAv').text('Associer');
													
											});
											$('#catalogue_commercant #prods_ecommercant .produit .associated').each(function(){
													$(this).find(".assocButtonEcom").text('Associer');
													$(this).removeClass("associated");
													
											});
											loadNavigation($('#cat_ecom').text());
											loadCatEcom()
										}
							}); 
							$(this).dialog("close");
					        return true;
					      },
					      "Cancel": function () {
					        $(this).dialog("close");
					        return false;
					      }
    			}
  	});
  	if(prodEcomId != "" && prodAvId != "") {
  		$myDialog.dialog('open');	
  	}else{
  		$myError.dialog('open');
  	}	
}


/*
 *Fonction permettant d'envoyer les données nécessaires en Ajax permettant
 * de valider l'association et le mapping des champs pour une catégorie entiere
 * de produits ecommercants 
 */
function validPrefCateg(){
	
	var theData = {};
	
	theData['staticFields'] = [];
	theData['dynaFields'] = [];
	
	//WEIGHT ??
	theData.staticFields.push( {
		name : $('#name').val(),
		sku  :  $('#sku').val(),
		categories : $('#id_cat_assoc').text(),
		cat_ecom : $('#catEcom').text(),
		description : $('#description').val(),
		short_description : $('#short_description').val(),
		price : $('#price').val(),
		country_of_manufacture : $('#country_of_manufacture').val(),
		meta_description : $('#meta_description').val(),
		meta_keyword : $('#meta_keyword').val(),
		meta_title : $('#meta_title').val(),
		image : $('#image').val(),
		small_image : $('#small_image').val(),
		thumbnail : $('#thumbnail').val(),
		manufacturer : $('#manufacturer').val(),
		meta_title : $('#meta_title').val(),
		vendor_id : $('#idV').text()
		
	}) ;
	
	$('#attr_chosen select').each(function(){
		var id_attr_ecom = this.id.split("_");
		theData.dynaFields.push({
			id_attr_av : $(this).val(),
			id_attr_ecom : id_attr_ecom[1]
		});
	});
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validprefcateg",
			data : theData,
			success : function(data) {
				alert(data);
			}
	}); 
}


/*
 *Fonction permettant d'envoyer les données nécessaires en Ajax permettant
 * de modifier la catégorie du produit
 * 
 */
function validModifCateg(){
	
	var theData = {};

	theData['caracId'] = [];
	var idprod = $('#id_produit').text();
	theData.caracId.push( {
		idProduit : idprod
	}) ;
	
	//Permet de différencier grossiste de ecommercant habituel
	var typeVendor = $('#typeVendor').text();
	
 	$("input[type=checkbox]:checked").each(function() {
 		var id=$(this).attr('id');
		id = id.substr(1, id.length);
		theData.caracId.push( {
			catId : id
		}) ;
  		
	});

	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validmodifcateg",
			data : theData,
			success : function(data) {
				$('#dialog').html(data);
				alert("Modification effectuée");
				location.reload(true);  
			}
	}); 
}


/*
 *Fonction permettant d'envoyer les données nécessaires en Ajax permettant
 * de modifier la catégorie des produits selectionnés GP
 * 
 */
function validModifAllCateg(){
	
	var theData = {};

	
	theData['caracId'] = [];
	
	//Permet de différencier grossiste de ecommercant habituel
	var typeVendor = $('#typeVendor').text();
	
 	$("input[type=checkbox]:checked").each(function() {
 		var id=$(this).attr('id');
		id = id.substr(1, id.length);
		theData.caracId.push( {
			catId : id
		}) ;
  		
	});

	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validallmodifcateg",
			data : theData,
			success : function(data) {
				$('#dialog').html(data);
				alert("Modification effectuée");
				location.reload(true);  
			}
	}); 
}


/*
 *Fonction permettant d'envoyer les données nécessaires en Ajax permettant
 * de valider l'ajout et le mapping des champs pour un produit ecommercant
 * entrainant la création d'un produit Avahis avec tous les attributs voulus
 */
function validAddProduct () {
	var theData = {};
	
	theData['staticFields'] = [];
	theData['dynaFields'] = [];
	theData.staticFields.push( {
		name : $('#val_name').val(),
		sku  :  $('#val_sku').val(),
		categories : $('#cat_id_assoc').text(),
		description : $('#val_description').val(),
		short_description : $('#val_short_description').val(),
		price : $('#val_price').val(),
		country_of_manufacture : $('#val_country_of_manufacture').val(),
		meta_description : $('#val_meta_description').val(),
		meta_keyword : $('#val_meta_keyword').val(),
		meta_title : $('#val_meta_title').val(),
		image : $('#val_image').val(),
		small_image : $('#val_small_image').val(),
		thumbnail : $('#val_thumbnail').val(),
		manufacturer : $('#val_manufacturer').val(),
		meta_title : $('#val_meta_title').val(),
		vendor_id : $('#idV').text(),
		weight : $('#val_weight').val()
		
	}) ;
	
	$('#attr_chosen select').each(function(){
		theData.dynaFields.push({
			id_attr_av : $(this).val(),
			value : $(this).parent().next().text()
		});
	});
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validaddproduct",
			data : theData,
			success : function(data) {
				alert(data);
				var currProdId = $('#current_product').text();
				$('#prods_ecommercant #'+currProdId).remove();
				
				
				var theData2 = {
					cat_id_assoc : $('#cat_id_assoc').text()	
				};
				
				//on va chercher l'arborescence des id de la catégorie associée pour
				//actualiser l'écran avahis à droite
				$.ajax({
					type : 'POST',
					url  : "/app.php/getidcattree",
					data : theData2,
					success : function(data) {
						//réactualisation de l'écran de droite pour afficher les nouveaux produits
						loadcat(data);
					}
				});
				loadProdListFilteredEcom($('#viewTypeEcom').text());
				$('#dialogAddProduct').dialog('close');
				
			}
	});   
}



/*
 *Fonction permettant d'envoyer les données nécessaires en Ajax permettant
 * de valider l'ajout pour un produit ecommercant
 * entrainant la création d'un produit Avahis avec tout les attributs voulus
 */
function validAddProductNoAttr () {
	var theData = {};
	
	theData['staticFields'] = [];
	theData['dynaFields'] = [];
	theData.staticFields.push( {
		name : $('#val_name').val(),
		sku  :  $('#val_sku').val(),
		categories : $('#cat_id_assoc').text(),
		description : $('#val_description').val(),
		short_description : $('#val_short_description').val(),
		price : $('#val_price').val(),
		country_of_manufacture : $('#val_country_of_manufacture').val(),
		meta_description : $('#val_meta_description').val(),
		meta_keyword : $('#val_meta_keyword').val(),
		meta_title : $('#val_meta_title').val(),
		image : $('#val_image').val(),
		small_image : $('#val_small_image').val(),
		thumbnail : $('#val_thumbnail').val(),
		manufacturer : $('#val_manufacturer').val(),
		meta_title : $('#val_meta_title').val(),
		vendor_id : $('#idV').text(),
		weight : $('#val_weight').val()
		
	}) ;
	
	//a corriger 
	$('#no_attr select').each(function(){
		var id_attr_av = this.id.split("_");
		theData.dynaFields.push({
			id_attr_av : this.id,
			value : $(this).val()
		});
	});
	
	$('#no_attr input.newAttr').each(function(){
		theData.dynaFields.push({
			id_attr_av : this.id,
			value : $(this).val()
		});
	});
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validaddproduct",
			data : theData,
			success : function(data) {
				alert(data);
				var currProdId = $('#current_product').text();
				$('#prods_ecommercant #'+currProdId).remove();
				
				
				var theData2 = {
					cat_id_assoc : $('#cat_id_assoc').text()	
				};
				
				//on va chercher l'arborescence des id de la catégorie associée pour
				//actualiser l'écran avahis à droite
				$.ajax({
					type : 'POST',
					url  : "/app.php/getidcattree",
					data : theData2,
					success : function(data) {
						//réactualisation de l'écran de droite pour afficher les nouveaux produits
						loadcat(data);
					}
				});
				loadProdListFilteredEcom($('#viewTypeEcom').text());
				$('#dialogAddProduct').dialog('close');
			}
	});   
}

/*
 * Validation de l'ajout d'un produit qui ne possède aucun attribut et de plus
 * est rattaché à une catégorie Avahis ne possédant elle aussi aucun attribut
 */
function validAddProductNoAttrAv () {
	
	var theData = {};
	
	theData['staticFields'] = [];
	theData['dynaFields'] = [];
	theData.staticFields.push( {
		name : $('#val_name').val(),
		sku  :  $('#val_sku').val(),
		categories : $('#cat_id_assoc').text(),
		description : $('#val_description').val(),
		short_description : $('#val_short_description').val(),
		price : $('#val_price').val(),
		country_of_manufacture : $('#val_country_of_manufacture').val(),
		meta_description : $('#val_meta_description').val(),
		meta_keyword : $('#val_meta_keyword').val(),
		meta_title : $('#val_meta_title').val(),
		image : $('#val_image').val(),
		small_image : $('#val_small_image').val(),
		thumbnail : $('#val_thumbnail').val(),
		manufacturer : $('#val_manufacturer').val(),
		meta_title : $('#val_meta_title').val(),
		weight : $('#val_weight').val(),
		vendor_id : $('#idV').text()
		
	}) ;
	
	//a corriger 
	$('#no_attr input.newAttr').each(function(){
		theData.dynaFields.push({
			id_attr_av : this.id,
			value : $(this).val()
		});
	});
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validaddproduct",
			data : theData,
			success : function(data) {
				alert(data);
				var currProdId = $('#current_product').text();
				$('#prods_ecommercant #'+currProdId).remove();
				
				
				var theData2 = {
					cat_id_assoc : $('#cat_id_assoc').text()	
				};
				
				//on va chercher l'arborescence des id de la catégorie associée pour
				//actualiser l'écran avahis à droite
				$.ajax({
					type : 'POST',
					url  : "/app.php/getidcattree",
					data : theData2,
					success : function(data) {
						//réactualisation de l'écran de droite pour afficher les nouveaux produits
						loadcat(data);
					}
				});
				loadProdListFilteredEcom($('#viewTypeEcom').text());
				$('#dialogAddProduct').dialog('close');
			}
	});  
}


/*
 * Work in progres
 */
function infiniteScroll()
{
	var offset = 0;
 
    // on initialise ajaxready à true au premier chargement de la fonction
	$("#prods_ecommercant").data('ajaxready', true);
 
	$('#prods_ecommercant').append('<div id="loader"><img src="/img/ajax-loader.gif" alt="loader ajax"> TEEESSSTT</div>');
 
	var deviceAgent = navigator.userAgent.toLowerCase();
	var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
 
 
	$("#prods_ecommercant").scroll(function()
	{
		// On teste si ajaxready vaut false, auquel cas on stoppe la fonction
		if ($("#prods_ecommercant").data('ajaxready') == false) return;
 
		if(($("#prods_ecommercant").scrollTop() + $("#prods_ecommercant").height()) == $("#prods_ecommercant").height()
		   || agentID && ($(window).scrollTop() + $(window).height()) + 150 > $(document).height())
		{
            // lorsqu'on commence un traitement, on met ajaxready à false
			$("#prods_ecommercant").data('ajaxready', false);
 
			$('#catalogue_commercant #loader').fadeIn(400);
			var theData = {
		    	t : $('#tech').text(),
		    	id : $('#id').text(),
		    	
		    } ;
    
		    jQuery.ajax({
					type : 'POST',
					url : "/app.php/showprodlist?limit="+offset,
					data : theData,
					success : function(data) {
						if(data != ""){
							$('#catalogue_commercant #prods_ecommercant').append(data);
							$(window).data('ajaxready', true);
							offset+= 10;
						}
					}
			});
			$('#prods_ecommercant #loader').fadeOut(400);	
			
		}
	});
};


/*
 * Pagination des pages de produits dans soukflux 
 * que ce soit mode grille ou liste, Page suivante
 */
function nextPageProd (elm) {
	
	//Coté ecommercant
	if($(elm).parent().parent().attr("id") == "prods_ecommercant"){
		var theData = {
			nbprod : $('#nbProd').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/nextpageprod?type=ecom",
					data : theData,
					success : function(data) {

					}
		});
		loadSessionProdList('ecom');
	}
	//Coté Avahis
	if($(elm).parent().parent().attr("id") == "prods_avahis"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/nextpageprod?type=av",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdList('av');
	}
	//Coté Avahis GP
	if($(elm).parent().parent().attr("id") == "prods_avahisGP"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/nextpageprodGP?type=avaGP",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdListGP('avaGP');
	}
}


/*
 * Pagination des pages de produits dans soukflux 
 * que ce soit mode grille ou liste, Page précedente
 */
function prevPageProd (elm) {
	//Coté ecommercant
	if($(elm).parent().parent().attr("id") == "prods_ecommercant"){
		var theData = {};
		
	  	jQuery.ajax({
						type : 'POST',
						url : "/app.php/prevpageprod?type=ecom",
						data : theData,
						success : function(data) {
	
						}
		});
		loadSessionProdList('ecom');
	}
	//Coté Avahis
	if($(elm).parent().parent().attr("id") == "prods_avahis"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/prevpageprod?type=av",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdList('av');
	}
	//Coté Avahis GP
	if($(elm).parent().parent().attr("id") == "prods_avahisGP"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/prevpageprodGP?type=avaGP",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdListGP('avaGP');
	}
}


/*
 * Pagination des pages de produits dans soukflux 
 * que ce soit mode grille ou liste, Première page
 */
function firstPageProd (elm) {
  	//Coté ecommercant
	if($(elm).parent().parent().attr("id") == "prods_ecommercant"){
		var theData = {};
		
	  	jQuery.ajax({
						type : 'POST',
						url : "/app.php/firstpageprod?type=ecom",
						data : theData,
						success : function(data) {
	
						}
		});
		loadSessionProdList('ecom');
	}
	//Coté Avahis
	if($(elm).parent().parent().attr("id") == "prods_avahis"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/firstpageprod?type=av",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdList('av');
	}
	//Coté Avahis
	if($(elm).parent().parent().attr("id") == "prods_avahisGP"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/firstpageprodGP?type=avaGP",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdListGP('avaGP');
	}
	
}


/*
 * Pagination des pages de produits dans soukflux 
 * que ce soit mode grille ou liste, Dernière page
 */
function lastPageProd (elm) {
  	//Coté ecommercant
	if($(elm).parent().parent().attr("id") == "prods_ecommercant"){
		var theData = {
			nbprod : $('#nbProd').text()
		};
		
	  	jQuery.ajax({
						type : 'POST',
						url : "/app.php/lastpageprod?type=ecom",
						data : theData,
						success : function(data) {
	
						}
		});
		loadSessionProdList('ecom');
	}
	//Coté Avahis
	if($(elm).parent().parent().attr("id") == "prods_avahis"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/lastpageprod?type=av",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdList('av');
	}
	//Coté Avahis GP
	if($(elm).parent().parent().attr("id") == "prods_avahisGP"){
		var theData = {
			nbprod : $('#nbProdAv').text()
		};
	
  		jQuery.ajax({
					type : 'POST',
					url : "/app.php/lastpageprodGP?type=avaGP",
					data : theData,
					success : function(data) {
			
					}
		});
		loadSessionProdListGP('avaGP');
	}
}



function loadSessionProdList (type) {
  var theData = {};
  
  jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadessionprodlist?type="+type,
		data : theData,
		beforeSend: function() {
			if(type == 'av'){
				$('#catalogue_avahis #prods_avahis').empty();
				$('#catalogue_avahis #prods_avahis').append('<div id="prod_av_loader"></div>');
	     		$('#prod_av_loader').show();	
			}
			else if(type == 'ecom'){
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant #prods_ecommercant').append('<div id="prod_ecom_loader"></div>');
	     		$('#prod_ecom_loader').show();
			}
	  	},
	  	complete: function(){
	  		if(type == 'av'){
	  			$('#prod_av_loader').hide();	
	  		}
	    	else if(type == 'ecom'){
	    		$('#prod_ecom_loader').hide();
	    	}
	  	},
		success : function(data) {
			if(type=='ecom'){
				$('#catalogue_commercant #prods_ecommercant').empty();
				$('#catalogue_commercant #prods_ecommercant').append(data);
			}
			else if(type=='av'){
				$('#catalogue_avahis #prods_avahis').empty();
				$('#catalogue_avahis #prods_avahis').append(data);
			}
			
		}
  });
}


function loadSessionProdListGP (type) {
  var theData = {};

  jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadessionprodlistGP?type="+type,
		data : theData,
		beforeSend: function() {
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append('<div id="prod_av_loader"></div>');
	     		$('#prod_av_loader').show();	
	  	},
	  	complete: function(){
	  			$('#prod_av_loader').hide();	
	  	},
		success : function(data) {
				$('#catalogue_avahis #prods_avahisGP').empty();
				$('#catalogue_avahis #prods_avahisGP').append(data);
		}
  });
}


/*
 * Annule l'association de deux produits visuellement en vidant les champs
 */
function cancelAssociation() {
	$('#prodAssocAv').empty();
	$('#prodAssocEcom').empty();
	$('#catalogue_avahis #assoc2 .product_name ').empty();
	$('#catalogue_avahis #assoc1 .product_name ').empty();
	$('#catalogue_avahis #prods_avahis .associated').each(function(){
			$(this).find(".assocButtonAv").text('Associer');
			$(this).removeClass("associated");
	});
	$('#catalogue_commercant #prods_ecommercant .associated').each(function(){
			$(this).find(".assocButtonEcom").text('Associer');
			$(this).removeClass("associated");
			
	});
}


/*
 * Permet l'ajout d'un produit et l'ouverture d'une fenetre de dialogue
 */
function addProduct (elm) {
	var prodId = $(elm).parent().parent().attr('id');
	
	var theData = {
			t : $('#tech').text(),
    		vendor : $('#id').text(),
    		id : prodId,
    } ;
    
    $('#dialog').dialog();
}


function addAllProductsToAv () {
	
	//Si catégorie déja associée 
	var cat_assoc = $("#id_cat_assoc").text();
	
	if(cat_assoc != ""){
		var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir ajouter ces <b>'+$('#nbProd').text()+'</b> produits à:<br/><br/><b> '+ $('#assoc_cat b').text()+
	    		  '</b><br/><br/>Ok pour confirmer.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de l\'association',
	    			buttons: {
								"OK": function () {
									
									var cat_ecom = $('#cat_ecom').text() ;
									
									theData = {
										cat_id_assoc : cat_assoc,
										cat_ecom : cat_ecom  	
									};
									
									jQuery.ajax({
											type : 'POST',
											url : "/app.php/addallproducttoav",
											data : theData,
											success : function(data) {
												var info = jQuery.parseJSON(data);
												var $dialogRecap = $('<div></div>').html(info.message).dialog({
									    			autoOpen: false,
									    			width : 'auto',
									    			title: 'Récapitulatif :',
									    			buttons:{
									    			"OK":function (){
									    					$(this).dialog('close');
									    				}
									    			},
										      		"Annuler": function () {
											        	$(this).dialog("close");
											        	return false;
										      		}});
												$dialogRecap.dialog('open');
												
												$myDialog.dialog("close");
												loadProductList ("grid");
												$('#assoc_cat a').trigger('click');
								    			return true;
											}
									}); 
						      	},
					      		"Annuler": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
		$myDialog.dialog('open');	  		
		
	}else{
		alert('Associez d\'abord tous ces produits à une catégorie !');
		$('#assoc_cat').next().trigger('click');
	};	
}


/*
 * Permet d'ajouter tous le produits étant dans la selection (dont le ID sont en session)
 * en fiches avahis
 */
function addAllSelectedProductsToAv () {
	
	if($('#nbProd').text() != "" && $('#nbProd').text() != "0" ){
		var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir ajouter ces <b>'+$('#nbProd').text()+'</b> produits à leur catégories respectives ?<br/><br/>Ok pour confirmer.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de l\'association',
	    			buttons: {
								"OK": function () {
	
									theData = {
		
									};
									
									jQuery.ajax({
											type : 'POST',
											url : "/app.php/addallselectedproducttoav",
											data : theData,
											success : function(data) {
												var info = jQuery.parseJSON(data);
												var $dialogRecap = $('<div></div>').html(info.message).dialog({
									    			autoOpen: false,
									    			width : 'auto',
									    			title: 'Récapitulatif :',
									    			buttons:{
									    			"OK":function (){
									    					$(this).dialog('close');
									    				}
									    			},
										      		"Annuler": function () {
											        	$(this).dialog("close");
											        	return false;
										      		}});
												$dialogRecap.dialog('open');
												$myDialog.dialog("close");
												deSelectAllProducts();
									    		loadSelectedProductList ("grid");
							    				return true;
											}
									}); 
									 
									
						      	},
					      		"Annuler": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
		$myDialog.dialog('open');
	}
	else{
		alert('Aucun produit séléctionné !');
	}	  		
}


function refuseAllSelectedProds (elm) {
	if($('#nbProd').text() != "" && $('#nbProd').text() != "0" ){
		var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir refuser ces <b>'+$('#nbProd').text()+'</b> produits ?<br/><br/>Ok pour confirmer.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de l\'association',
	    			buttons: {
								"OK": function () {
	
									theData = {
		
									};
									
									jQuery.ajax({
											type : 'POST',
											url : "/app.php/refuseallselectedproduct",
											data : theData,
											success : function(data) {
												
												loadNavigation($('#cat_ecom').text());
												$myDialog.dialog("close");
									    		loadSelectedProductList ("grid");
							    				return true;
											}
									}); 
									 
									
						      	},
					      		"Annuler": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
		$myDialog.dialog('open');
	}
	else{
		alert('Aucun produit séléctionné !');
	}	  		
}



function addAllGrossisteProds () {
	//Si catégorie déja associée 
	var cat_assoc = $("#id_cat_assoc").text();
	
	if(cat_assoc != ""){
		var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir ajouter ces <b>'+$('#nbProd').text()+'</b> produits à:<br/><br/><b> '+ $('#assoc_cat b').text()+
	    		  '</b><br/><br/>Ok pour confirmer.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de l\'association',
	    			buttons: {
								"OK": function () {
									
									theData = {
										cat_id_assoc : cat_assoc,
											
									};
									
									jQuery.ajax({
											type : 'POST',
											url : "/app.php/addallproducttoav",
											data : theData,
											success : function(data) {
												
												var info = jQuery.parseJSON(data);
												var $dialogRecap = $('<div></div>').html(info.message).dialog({
									    			autoOpen: false,
									    			width : 'auto',
									    			title: 'Récapitulatif :',
									    			buttons:{
									    			"OK":function (){
									    					$(this).dialog('close');
									    				}
									    			},
										      		"Annuler": function () {
											        	$(this).dialog("close");
											        	return false;
										      		}
										      	});	
										      	$dialogRecap.dialog('open');
												$myDialog.dialog("close");
												loadProductList ("grid");
												loadcat(""+info.id+"");
								    			return true;
											}
									}); 

						      	},
					      		"Annuler": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
		$myDialog.dialog('open');	  		
		
	}else{
		alert('Associez d\'abord tous ces produits à une catégorie !');
		$('#assoc_cat').next().trigger('click');
	};  
}

/*
 * Remplit dynamiquement le champs correspondant au select sur lequel 
 * l'utilsateur a cliqué et dont il a changé la valeur. Permet visuellement
 * de voir directement les données liées à ce champ pour un produit grace à AJAX
 */
function loadField (elm) {
	
	var id = elm.id;
	
	var value = $("#"+id+" option:selected").val(); 
	
	var theData = {
    	field : value,
  	} ;
  	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadfield",
		data : theData,
		success : function(data) {
			$('#val_'+id).val(data);
		}
	})
}


/*
 * Validation de l'association d'une catégorie ecommercant avec une catégorie Avahis
 * en fonction du contexte : Depuis l'ajout d'un produit qu'on veux associer à autre chose
 * Ou encore ajout d'un produit sans aucun antécédent d'association pour la catégorie.
 * Ou enfin association simple de la catégorie entière avec une cat avahis
 */
function validCateg (context) {
	
	var theData = { 'cat_id[]' : []};
	
	//Permet de différencier grossiste de ecommercant habituel
	var typeVendor = $('#typeVendor').text();
	
 	$("input[type=checkbox]:checked").each(function() {
 		
 		var id=$(this).attr('id');
		id = id.substr(1, id.length);
  		theData['cat_id[]'].push(id);
	});
	
	//Association pour un produit
	if(context == 'prod'){	
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/assoccategprod",
			data : theData,
			success : function(data) {
				var categ = jQuery.parseJSON(data);
				
				var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir associer ce produit à:<br/><br/><b> '+ categ.cat_label +
	    		  '</b><br/><br/>Ok pour confirmer.  Cancel pour annuler.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de l\'association',
	    			buttons: {
						      "OK": function () {
						      		theData['insert'] = true;
						      		theData['id_vendor'] = $('#idV').text();
						      		theData['cat_ecom'] = $('#catEcom').text();
						        	jQuery.ajax({
											type : 'POST',
											url : "/app.php/assoccategprod",
											data : theData,
											success : function(data) {
												var infoData = jQuery.parseJSON(data);
												$('#popup_categ').html(categ.cat_label);
												//Si l'ajout produit a été fait sans catégorie au préalable il y a assoc
												//de la categ ecom avec categ avahis
												if(infoData.newcat=="newcat"){
													$('#assoc_cat').html('Associé à <b>'+categ.cat_label+'</b>  <a href="#">(Voir)</a>');
													$('#assoc_cat a').attr('onClick', 'loadcat("'+infoData.catIdTree+'")');
													$('#cat_id_assoc').text(categ.cat_id);
													$('#popup_categ').append('<br/><a id="openCatTree" href="#ancreTreeView" onclick="openCatTree();">Changer la catégorie pour ce produit</a>');
												}
												$('#cat_id_assoc').text(categ.cat_id);
												$('#id_cat_assoc').text(categ.cat_id);
												$('.treeviewcheck').hide();
												$('#attr_chosen').show();
												showAttrAvPopup(categ.cat_id);
											}
									}); 
									$(this).dialog("close");
								    return true;
								        
						      		},
					      		"Cancel": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
	  		
				$myDialog.dialog('open');
			}
		})
	}else if(context == 'cat'){
		
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/assoccateg",
			data : theData,
			success : function(data) {
				var categ = jQuery.parseJSON(data);
				var $myDialog = $('<div></div>')
	    		.html('Êtes vous sûr de vouloir associer cette catégorie:<br/><br/><b>'+$('#catEcom').text()+'</b><br/><br/>Avec<br/><br/><b> '+ categ.cat_label +
	    		  '</b><br/><br/>Ok pour confirmer.  Annuler pour annuler.')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Validation de l\'association',
	    			buttons: {
						      "OK": function () {
						      		theData['insert'] = true;
						      		theData['id_vendor'] = $('#idV').text();
						      		theData['cat_ecom'] = $('#catEcom').text();
						        	jQuery.ajax({
											type : 'POST',
											url : "/app.php/assoccateg",
											data : theData,
											success : function(data) {
												if(typeVendor == "Grossiste"){
													//Derrière le popup
													$('#assoc_cat').html('Associé à <b>'+categ.cat_label+'</b>');
													$('#cat_id_assoc').text(categ.cat_id);
													$('#id_cat_assoc').text(categ.cat_id);
													$('#dialogAssocCateg').dialog('close');
												}else{
													
													$('#success').html('<b style="color:green">Association réussie !</b><br/><br/><b>'+categ.cat_label+'</b>');
													$('.treeviewcheck').html('');												
													$('#popup_categ').html(categ.cat_label);												
													$('#oldAssoc').html('');
													$('#fixed_fields').show();
													fillFixedFields(theData['id_vendor'], categ.cat_id, theData['cat_ecom']);
													
													//Derrière le popup
													$('#assoc_cat').html('Associé à <b>'+categ.cat_label+'</b> <a  href="#">(Voir)</a>');
													$('#assoc_cat a').attr('onClick', 'loadcat("'+data+'")');
													$('#cat_id_assoc').text(categ.cat_id);
													$('#id_cat_assoc').text(categ.cat_id);
													showAttrAvPopup(categ.cat_id);	
												}
												
											}
									});
									$(this).dialog("close");
								    return true;
						      		},
					      		"Annuler": function () {
						        	$(this).dialog("close");
						        	return false;
					      		}
	    				}
	  			});
	  		
				$myDialog.dialog('open');
			}
		})
	}else{
		
	}	
}




/*
 * Fonction permettant de préselectionner le mapping des champs fixes
 * pour une catégorie ou un produit en fonction des préférences sauvegardées
 * par l'utilisateur
 */
function fillFixedFields (idVendor, catId, catEcom) {
	
	var theData = {
    	id_vendor : idVendor,
    	cat_id : catId,
    	cat_ecom : catEcom
  	} ;
  	
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getstaticfields",
		data : theData,
		success : function(data) {
				
				var obj = jQuery.parseJSON(data);
				$('#fixed_fields select').each(function(){
					var value = obj[this.id];
					$('#fixed_fields #'+this.id+' option').each(function(){

						if($(this).text() == value ){
							$(this).attr('selected', 'selected');
						}
					});
					loadField(this);
				});
				
			}
	}); 
}



/**
 *Va chercher les attributs pour tous les produits de la catégorie avahis choisie
 * ainsi que tous les attributs du produit en cours d'ajout, la vue e chargera de 
 * mettre le tout sous forme de tableau avec des select multiples  
 */
function showAttrAvPopup (catId, prodId) {

	
	var idProduct = prodId;
	if (prodId ==""){
		idProduct = $('#current_product').text();
	}
	
	if (catId == ""){
		catId = $('#cat_id_assoc').text();
	}

	var theData = {
		cat_id : catId,
		id_product : idProduct
	};
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/findattrav",
			data : theData,
			success : function(data) {
				$('#attr_chosen').show();
				$('#attr_chosen tbody').html(data);
			}
	});
}



/**
 * Permet d'ajouter un attribut non existant dans la catégorie Avahis dans le contexte du 
 * pop-up permettant d'ajouter un produit e-commercant 
 */
function addAttrPopUp (elm) {

	var $myDialog = $('<div></div>')
	    		.html('Entrez le nom de l\'attribut que vous voulez ajouter : <br/><input id="i'+elm.id+'" type="text" \>')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Ajout d\'un attribut',
	    			buttons: {
	    				"OK": function () {
						      		var newAttr = $('#i'+elm.id).val();
						      		$(elm).prev().append('<option value="new_'+newAttr+'" selected>'+newAttr+'</option>');
						      		$('#i'+elm.id).remove();
									$(this).dialog("destroy");
							    	return true;
						 },
					     "Annuler": function () {
					     			$('#i'+elm.id).remove();
						        	$(this).dialog("destroy");
						        	return false;
					     }
	    			}
	    		});
	$myDialog.dialog('open');
}


/*
 * Fonction permettant d'ajouteur une valeur d'attribut à une liste
 */
function addValPopUp (elm) {
	var $myDialog = $('<div></div>')
	    		.html('Entrez votre valeur : <br/><input id="i'+elm.id+'" type="text" \>')
	    		.dialog({
	    			autoOpen: false,
	    			title: 'Ajout d\'une valeur',
	    			buttons: {
	    				"OK": function () {
						      		var newVal = $('#i'+elm.id).val();
						      		
						      		$(elm).prev().append('<option selected>'+newVal+'</option>');
						      		$('#i'+elm.id).remove();
									$(this).dialog("destroy");
							    	return true;
						 },
					     "Annuler": function () {
						        	$(this).dialog("destroy");
						        	$('#i'+elm.id).remove();
						        	return false;
					     }
	    			}
	    		});
	$myDialog.dialog('open');  
}


/*
 * Fonction permettant de simuler un click sur une catégorie voulue dans l'arbre
 * de catégorie en fonction du lien sur leuquel on a cliqué dans le chemin
 */
function linkBreadcrumps(link){
	$("[id='"+link+"']").trigger('click');
	
}



function genFile(id) {
	var theData = {
		siteid : id
	};
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/genfile",
		data : theData,
		success : function(data) {

			window.open('/o_services/download/download.php?path=' + data + '&file=import' + id + '.xml')

		}
	})

}


function showDialog(url) {
	$("#modalIframeId").attr("src", url);
	$("#dialog").dialog('open');
	return false;
}



function showDialog2(data) {

	var obj = jQuery.parseJSON(data);

	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		success : function(data) {

			$('#dialog').html(data);
			
			$("#dialog").dialog("open");
			
		},
		beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Chargement..',
						closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						},
	    			});
		    	$loaddialog.dialog('open');
		},
		complete: function(){
			$loaddialog.dialog('close');
		}
	});

	return false;
}


function testLoader () {
  $loaddialog = $('<div></div>')
		    		.html('<div id="progress" style="width:500px;border:1px solid #ccc;"></div><div id ="information">Chargement...</div>')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			height: "auto",
	    				width: "auto",
		    			closeText: "hide",
		    			title : 'Chargement..',
						closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						},
	    			});
	$loaddialog.dialog('open');
}

function showDialogAddTicket(entity_id, type) {
	
	var theData = {
		entity_id : entity_id,
		type : type	
	} ;
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/addTicket",
		data : theData,
		success : function(data) {
			
			if( $('#myModalTicket').length == 0){
				if($('#content-client').length > 0)
					$('#content-client').append(data);
				else		
					$('#content').append(data);
			}
			$('#myModalTicket').modal({
				 keyboard: true
			});
		}
	});

	return false;
}

function showDialogAddRelance(ticket_id, relance_id) {
	
	var theData = {
		ticket_id : ticket_id,
		relance_id : relance_id
	} ;
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogRelance",
		data : theData,
		success : function(data) {
			
			if( $('#modalAddRelance').length == 0){
				$('#content').append(data);
				$('#ticket_container').append(data);
			}else{
				$('#modalAddRelance').remove();
				$('#content').append(data);
				$('#ticket_container').append(data);
			}
			$('#modalAddRelance').modal({
				 keyboard: true
			});
		}
	});

	return false;
}


function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

function addObserver (ticket_id) {
	
	theData = {
		ticket_id : ticket_id
	}
	
	jQuery.ajax({
		type : "POST", 
		url : "/app.php/addObserverTicket",
		data : theData,
		success : function(data){
			if(data == 1){
				$('#observerModal .modal-body').html("<div class='alert alert-success'>Vous observez maintenant ce ticket</div>");
				$('#observerModal').modal();
				
				window.location.reload();
			}else{
				$('#observerModal .modal-body').html("<div class='alert alert-block alert-error fade in'><h4 class='alert-heading'>Attention !</h4>Vous observez déjà ce ticket !</div>");
				$('#observerModal').modal();
				
			}
		}
	});
}


function validFormAddTicket () {
	
	var theData = {
		entity_id : $('#inputEntity_id').val(),
		ticket_title : $('#inputTitle').val(),
		client_id : $('#inputCustomer_id').val(),
		client_name : $('#inputName').val(),
		ticket_priority_id : $('#inputPriority_id').val(),
		ticket_type_id : $('#select_TicketType').val(),
		ticket_text : tinyMCE.activeEditor.getContent(),
		client_type : $('#inputClient_type_id').val(),
		assign_list : $('#listuser').select2('data'),
		assign_user_id : $('#listassignuser').val(),
		ticket_visibility : $('#ticket_visibility').val(),
	}; 
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/insertTicket",
		data : theData,
		success : function(data) {
			if($("#ticket_list_client").length > 0){
				
				$('#ticket_list_client').html("");
			
				var data = {
					entity_id : $('#inputEntity_id').val(),
					client_type : $('#inputClient_type_id').val()
				};
				jQuery.ajax({
					type : "POST",
					url : "/app.php/refreshTicketList",
					data : data,
					success : function(data) {
						$('#ticket_list_client').html(data);
					}
				});
			}
			else{
				location.reload();
			}
		}
	});
}


function filterTickets(entity_id, user_id) {
	
	if($("#ticket_list_client").length > 0){
		var $add_id = "_cli";
	}else{
		var $add_id = "";
	}

	var theData = {
		entity_id : entity_id,
		user_id : user_id,
		ticket_priority_id : $('#search_ticket_priority'+$add_id).val(),
		client_name : 		 $('#search_ticket_client_name'+$add_id).val().trim(),
		ticket_state_id : 	 $('#search_ticket_statut'+$add_id).val(),
	}; 
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/refreshTicketList?filter=yes",
		data : theData,
		success : function(data) {
			
			if($("#ticket_list_client").length > 0){
				$('#ticket_list_client').html("");
				$('#ticket_list_client').html(data);
			}
			else{
				$('#ticket_list').html("");
				$('#ticket_list').html(data);
			}
			
		}
	});
}

function filterTicketsRelance() {
	
	var theData = {
		relance_statut : $('#search_ticketrelance_statut').val(),
		client_name : 	 $('#search_ticketrelance_client_name').val().trim()
	}; 
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/refreshRelanceTicketList?filter=yes",
		data : theData,
		success : function(data) {
			$('#relance_ticket_list').html(data);
		}
	});
}
function searchCustomer(){

	var theData = {
		entity_id : $('#search_customer_id').val().trim(),
		name : 		 $('#search_customer_name').val().trim(),
	}; 
	
	if($('#search_customer_id').val().trim() == "" & $('#search_customer_name').val().trim() == ""){
		return false;
	}
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/refreshCustomerList",
		data : theData,
		success : function(data) {
			$('#customer_tab').html("");
			$('#customer_tab').html(data);
		}
	});
}

function searchVendor(){

	var theData = {
		entity_id : $('#search_vendor_id').val().trim(),
		name : 		 $('#search_vendor_name').val().trim(),
	}; 
	
	if($('#search_vendor_id').val().trim() == "" & $('#search_vendor_name').val().trim() == ""){
		return false;
	}
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/refreshVendorList",
		data : theData,
		success : function(data) {
			$('#vendor_tab').html("");
			$('#vendor_tab').html(data);
		}
	});	
}

function showDialogAddCustomer() {
	
	var theData = {};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/addCustomer",
		data : theData,
		success : function(data) {
			
			if( $('#myModal').length == 0){
				$('#content').append(data);	
			}
			$('#myModal').modal({
				 keyboard: true
			});
		}
	});
	return false;
}


function showDialogEditPackCredit(pack_id) {
	
	var theData = {
		pack_id : pack_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogEditPackCredit",
		data : theData,
		success : function(data) {
			
			if( $('#myModalCreditPack').length == 0){
				$('#content').append(data);	
			}
			$('#myModalCreditPack').modal({
				 keyboard: true
			});
		}
	});
	return false;
}

function showDialogEditLivraisonType(livraison_type_id) {
	
	var theData = {
		livraison_type_id : livraison_type_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogEditLivraisonType",
		data : theData,
		success : function(data) {
			
			if( $('#myModalLivraison').length == 0){
				$('#content').append(data);	
			}
			$('#myModalLivraison').modal({
				 keyboard: true
			});
		}
	});
	return false;
}


function showDialogSupprPackCredit(pack_id) {
	
	var theData = {
		pack_id : pack_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogSupprPackCredit",
		data : theData,
		success : function(data) {
			
			if( $('#myModalSupprCreditPack').length == 0){
				$('#content').append(data);	
			}
			$('#myModalSupprCreditPack').modal({
				 keyboard: true
			});
		}
	});
	return false;
}


function validAddCreditPack(){
	
	if($("#input_label_pack").val() == "" || $("#input_value_pack").val() == "" || $("#input_price_pack").val() == ""){
		
		$('#form_errors_creditspacks').html("Veuillez remplir tous les champs svp !");
		return false;	
	}
	
	var theData = {
		label : $("#input_label_pack").val(),
		value : $("#input_value_pack").val(),
		price : $("#input_price_pack").val()
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/addPackCredit",
		data : theData,
		success : function(data) {
			refreshReferentielCredits();
			$("#messagebox_creditspacks").html(data);
			$("#form_errors_creditspacks").html("");
			
			//reset fields
			$("#input_label_pack").val("");
			$("#input_value_pack").val("");
			$("#input_price_pack").val("");
		}
	});	
}


function validEditPack(pack_id){
	
	var theData = {
		pack_id : pack_id,
		label : $("#edit_label_pack").val(),
		value : $("#edit_value_pack").val(),
		price : $("#edit_price_pack").val()
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/editPackCredit",
		data : theData,
		success : function(data) {
			$("#messagebox_creditspacks").html(data);
			$('#myModalCreditPack').remove();
			refreshReferentielCredits();
			$("#form_errors_creditspacks").html("");
		}
	});
}

function validEditLivraisonType(shipping_id){
	
	var theData = {
		shipping_id : shipping_id,
		refund : $("#refund").val(),
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/editLivraisonType",
		data : theData,
		success : function(data) {
			$("#messagebox_livraisontypes").html(data);
			$('#myModalLivraison').remove();
			refreshLivraisonTypes();
			$("#form_errors_creditspacks").html("");
		}
	});
}

function refreshLivraisonTypes () {
	
	
  	jQuery.ajax({
		type : 'GET',
		url : "/app.php/getAllLivraisonTypes",
		
		success : function(data) {
			$('#ref_livraisontypes').html(data);
		}
	});	
}


function deleteCreditPack (pack_id) {
	
	var theData = {
		pack_id : pack_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/deletePackCredit",
		data : theData,
		success : function(data) {
			$('#myModalSupprCreditPack').remove();
			$("#messagebox_creditspacks").html(data);
			refreshReferentielCredits();
			$("#form_errors_creditspacks").html("");
		}
	});
}


function refreshReferentielCredits () {
	
	
  	jQuery.ajax({
		type : 'GET',
		url : "/app.php/getAllCreditsPack",
		
		success : function(data) {
			$('#ref_creditspack').html(data);
		}
	});	
}


/////////////////////////////////

function showDialogEditReferentiel(entity, entity_id) {
	
	var theData = {
		entity : entity,
		entity_id : entity_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogEditReferentiel",
		data : theData,
		success : function(data) {
			
			if( $('#myModal'+entity).length == 0){
				$('#content').append(data);	
			}
			$('#myModal'+entity).modal({
				 keyboard: true
			});
		}
	});
	return false;
}


function showDialogSupprReferentiel(entity, entity_id) {
	
	var theData = {
		entity : entity,
		entity_id : entity_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogSupprReferentiel",
		data : theData,
		success : function(data) {
			
			if( $('#myModalSuppr'+entity).length == 0){
				$('#content').append(data);	
			}
			$('#myModalSuppr'+entity).modal({
				 keyboard: true
			});
		}
	});
	return false;
}


function validAddReferentiel(entity){
	
	if($("#input_label_"+entity).val() == "" ){
		
		$('#form_errors_'+entity).html("Veuillez remplir tous les champs svp !");
		return false;	
	}
	
	var theData = {
		label : $("#input_label_"+entity).val(),
		entity : entity
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/addReferentiel",
		data : theData,
		success : function(data) {
			refreshReferentielEntity(entity);
			$("#messagebox_"+entity).html(data);
			$("#form_errors_"+entity).html("");
			
			//reset fields
			$("#input_label_"+entity).val("");
		}
	});	
}

//permet de savoir si on est en train de visionner les produits associés refusés à traiter ou tous GP
function changeContextGP (context, elm) {
   
   $('#context').text(context);
   var theData ={
   		context:context	
   }
   
   //var_dump(theData);
   jQuery.ajax({
		type : 'POST',
		url : "/app.php/changecontextGP",
		data : theData,
		success : function(data) {
			$('.navigation_gp .active').each(function(){
				$(this).removeClass('active');
			});
			
			$('.embed').each(function(){
				$(this).hide();
			});
			$('#main-content-area').show();
			History.pushState('ajax', "Soukflux : Catalogue produit", "/app.php/catalogue/produit");
		}
	});
   
}

function supprAssocGr (id_product_gr, id_produit){
	
	bootbox.confirm("Êtes vous sur de vouloir supprimer cette association ? ", function(result) {
		
		if(result === true){
			
			var theData = {
				id_product : id_product_gr,
				id_produit : id_produit		
			};
			
			jQuery.ajax({
				type : "POST",
				url : "/app.php/supprAssocGrossiste",
				data : theData,
				success : function (data) {
					$("#productgr_"+id_product_gr).remove();
				}
			})
			
		}
	});
}

function modeEdition (id_produit) {
	
	window.scrollTo(0, 0);
	
	var theData = {
		id_produit : id_produit
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/embed_fiche_produit/edit",
		data : theData,
		beforeSend : function(){
			$.blockUI({css: { color: '#fff', borderRadius: "15px" }, message: "<h1>Mode édition...</h1>"});
			//$('#ficheproduit-'+id_produit).html("");
		},
		success : function(data) {
			$.unblockUI();
			var obj = jQuery.parseJSON(data);

			$('#ficheproduit-'+id_produit).html(obj.html);
		}
	});  
}

function modeDuplicate (id_produit, elm) {
	
	$(elm).prop("disabled",true);
	window.scrollTo(0, 0);
	
	var theData = {
		id_produit : id_produit
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/embed_fiche_produit/duplicate",
		data : theData,
		beforeSend : function(){
			$.blockUI({css: { color: '#fff', borderRadius: "15px" }, message: "<h1>Mode édition...</h1>"});
			//$('#ficheproduit-'+id_produit).html("");
		},
		success : function(data) {
			$.unblockUI();
			var obj = jQuery.parseJSON(data);

			$('.navigation_gp li').each(function(){
				$(this).removeClass("active");
			});
			$('#ficheproduit-'+id_produit).hide();
			
			$('#catalogue_avahis').append('<div class="well embed" id="ficheproduit-d'+id_produit+'" style="display: block;">'+obj.html+'</div>');
			$('ul.navigation_gp').append("<li class='active tabprod' id='tabprod-d"+id_produit+"'><a href='#d"+id_produit+"' onClick='tabFiche(this); return false;'>"+obj.nom+"</a><button class='pull-right close-tab' id='close-d"+id_produit+"' onClick='closeThisTab(this); return false;'>x</button></li>");
			window.scrollTo(0, 0);
		}
	});  
}



function modeConsultation (id_produit, message) {
	
	window.scrollTo(0, 0);
	
	var theData = {
		id_produit : id_produit
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/embed_fiche_produit",
		data : theData,
		beforeSend : function(){
			$.blockUI({css: { color: '#fff', borderRadius: "15px" }, message: "<h1>Mode consultation...</h1>"});
			//$('#ficheproduit-'+id_produit).html("");
		},
		success : function(data) {
			$.unblockUI();
			var obj = jQuery.parseJSON(data);

			$('#ficheproduit-'+id_produit).html(obj.html);
			
			if(message != ""){
				$('#ficheproduit-'+id_produit).prepend(message);
			}
		}
	});  
}
    
function openFicheProduit (id_produit, message) {
	
	
	History.pushState('ajax', "Soukflux : Catalogue produit", "/app.php/catalogue/produit?id="+id_produit+"");
	if($('#ficheproduit-'+id_produit).length > 0 ){
		
		$('.embed').each(function(){
			$(this).hide();
		});
		$('.navigation_gp li').each(function(){
			$(this).removeClass("active");
		});
		$('#main-content-area').hide();		
		$('#tabprod-'+id_produit).addClass("active");
		$('#ficheproduit-'+id_produit).show();
	}else{
		var theData = {
			id_produit : id_produit
		};
		
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/embed_fiche_produit",
			data : theData,
			beforeSend : function(){
				$('#prods_avahisGP').block({css: { color: '#fff', borderRadius: "15px" }});
			},
			success : function(data) {
				$('#prods_avahisGP').unblock();
				var obj = jQuery.parseJSON(data);
				
				$('.navigation_gp li').each(function(){
					$(this).removeClass("active");
				});
				$('#main-content-area').hide();
				$('#catalogue_avahis').append('<div class="well embed" id="ficheproduit-'+id_produit+'" style="display: block;">'+obj.html+'</div>');
				$('ul.navigation_gp').append("<li class='active tabprod' id='tabprod-"+id_produit+"'><a href='#"+id_produit+"' onClick='tabFiche(this); return false;'>"+obj.nom+"</a><button class='pull-right close-tab' id='close-"+id_produit+"' onClick='closeThisTab(this); return false;'>x</button></li>");
				window.scrollTo(0, 0);
				
				if(message != ""){
					$('#ficheproduit-'+id_produit).prepend(message);
				}
			}
		});  		
	}
}

function tabFiche (elm) {
	
	var tmp = $(elm).parent().attr("id");
	
	var id_produit = tmp.split("-");
	
	id_produit = id_produit[1];
	
	History.pushState('ajax', "Soukflux : Catalogue produit", "/app.php/catalogue/produit?id="+id_produit+"");
	
	$('.navigation_gp li').each(function(){
		$(this).removeClass("active");
	});	
	
	$('#tabprod-'+id_produit).addClass("active");
	$('#main-content-area').hide();
	$('.embed').each(function(){
		$(this).hide();
	});
		
	$('#ficheproduit-'+id_produit).show();
}
 
function closeThisTab (elm) {

	var id = elm.id;
	
	var id_produit = id.split("-");
	
	id_produit = id_produit[1];
	
	$('.navigation_gp li').each(function(){
		$(this).removeClass("active");
	});
		
	$("#ficheproduit-"+id_produit).remove();
	$("#tabprod-"+id_produit).remove();
	
	$('#main-content-area').show();
	$('#ongAll').addClass("active");
	History.pushState('ajax', "Soukflux : Catalogue produit", "/app.php/catalogue/produit");
}

function cancelDuplicate(id_produit){
	
	$('.navigation_gp li').each(function(){
		$(this).removeClass("active");
	});
		
	$("#ficheproduit-d"+id_produit).remove();
	$("#tabprod-d"+id_produit).remove();
	
	$('#main-content-area').show();
	$('#ongAll').addClass("active");	
	$("#btnduplicate_"+id_produit).prop("disabled",false);
}

function saveEditFiche (id_produit) {
	
	if($("#sku_"+id_produit).val().trim() != ""){
		
		var iframe = document.getElementById('iframe_avahis_'+id_produit);
	    var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	    
		var rowattribut = innerDoc.getElementsByClassName('row-attribut');
		
		var product_attribut = [];
		
		
		for(var i = 0; i < rowattribut.length; ++i) {
			
		   var id_attribut  = rowattribut[i].getElementsByClassName('attribut')[0].id;
		   var value 		= rowattribut[i].getElementsByClassName('value')[0].innerText;
		   var newElement = {};
		   
		   id_attribut = id_attribut.split("-");
		   id_attribut = id_attribut[1];
		   
		   if(isNaN(id_attribut)){
				newElement['id_attribut'] = rowattribut[i].getElementsByClassName('attribut')[0].innerText;
				newElement['value'] = value; 
		   }else{
		   		newElement['id_attribut'] = id_attribut;
		   		newElement['value'] = value;
		   }
		   
		   product_attribut.push(newElement);
		};
	
		var theData = {
			id_produit : id_produit,
			price : $("#price_"+id_produit).val(),
			manufacturer : $("#manufacturer_"+id_produit).val(),
			sku : $("#sku_"+id_produit).val(),
			categories : $("#categories_"+id_produit).val(),
			country_of_manufacture : $("#country_of_manufacture_"+id_produit).val(),
			ean : $("#ean_"+id_produit).val(),
			dropship_vendor : $("#dropship_vendor_"+id_produit).val(),
			description : $("#description_"+id_produit).val(),
			short_description : $("#short_description_"+id_produit).val(),
			image : $("#image_"+id_produit).val(),
			small_image : $("#small_image_"+id_produit).val(),
			thumbnail : $("#thumbnail_"+id_produit).val(),
			local_image : $("#local_image_"+id_produit).val(),
			local_small_image : $("#local_small_image_"+id_produit).val(),
			local_thumbnail : $("#local_thumbnail_"+id_produit).val(),
			description_html : innerDoc.getElementById('product_tabs_description_tabbed_contentss').innerHTML,
			short_description : innerDoc.getElementById('iframe_short_description').innerHTML,
			name : innerDoc.getElementById('iframe_product_name').textContent,
			weight : $("#weight_"+id_produit).val(),
			product_attribut : product_attribut
			
		};
	
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/saveEditFiche",
			data : theData,
			beforeSend : function(){
				$.blockUI({css: { color: '#fff', borderRadius: "15px" }, message: "<h1>Sauvegarde...</h1>"});
			},
			success : function(data) {
				$.unblockUI();
				var success = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations ! <strong>'+theData.name+'</strong> a bien été modifié !</div>'
				modeConsultation(id_produit, success);
			}
		});		
	}
	else{
		bootbox.alert("Vous devez entrer un SKU ! ");
	}
	  
}

function saveDuplicateFiche (id_produit) {
	
	if($("#sku_d"+id_produit).val().trim() != ""){
		
		var iframe = document.getElementById('iframe_avahis_d'+id_produit);
	    var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

		var rowattribut = innerDoc.getElementsByClassName('row-attribut');
		
		var product_attribut = [];
		
		
		for(var i = 0; i < rowattribut.length; ++i) {
			
		   var id_attribut  = rowattribut[i].getElementsByClassName('attribut')[0].id;
		   var value 		= rowattribut[i].getElementsByClassName('value')[0].innerText;
		   var newElement = {};
		   
		   id_attribut = id_attribut.split("-");
		   id_attribut = id_attribut[1];
		   
		   if(isNaN(id_attribut)){
				newElement['id_attribut'] = rowattribut[i].getElementsByClassName('attribut')[0].innerText;
				newElement['value'] = value; 
		   }else{
		   		newElement['id_attribut'] = id_attribut;
		   		newElement['value'] = value;
		   }
		   
		   product_attribut.push(newElement);
		};
			
		var theData = {
			price : $("#price_d"+id_produit).val(),
			manufacturer : $("#manufacturer_d"+id_produit).val(),
			sku : $("#sku_d"+id_produit).val(),
			categories : $("#categories_d"+id_produit).val(),
			country_of_manufacture : $("#country_of_manufacture_d"+id_produit).val(),
			ean : $("#ean_d"+id_produit).val(),
			dropship_vendor : $("#dropship_vendor_d"+id_produit).val(),
			description : $("#description_d"+id_produit).val(),
			short_description : $("#short_description_d"+id_produit).val(),
			image : $("#image_d"+id_produit).val(),
			small_image : $("#small_image_d"+id_produit).val(),
			thumbnail : $("#thumbnail_d"+id_produit).val(),
			local_image : $("#local_image_d"+id_produit).val(),
			local_small_image : $("#local_small_image_d"+id_produit).val(),
			local_thumbnail : $("#local_thumbnail_d"+id_produit).val(),
			description_html : innerDoc.getElementById('product_tabs_description_tabbed_contentss').innerHTML,
			short_description : innerDoc.getElementById('iframe_short_description').innerHTML,
			name : innerDoc.getElementById('iframe_product_name').textContent,
			weight : $("#weight_d"+id_produit).val(),
			product_attribut : product_attribut
			
		};
	
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/saveDuplicateFiche",
			data : theData,
			beforeSend : function(){
				$.blockUI({css: { color: '#fff', borderRadius: "15px" }, message: "<h1>Sauvegarde...</h1>"});
			},
			success : function(data) {
				$.unblockUI();
				var success = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations la duplication <strong>'+theData.name+'</strong> a bien été créée !</div>'
				$('#close-d'+id_produit).trigger("click");
				openFicheProduit(data, success);
				$("#btnduplicate_"+id_produit).prop("disabled",false);
			}
		});		
	}
	else{
		bootbox.alert("Vous devez entrer un SKU ! ");
	}    
}

function resetTest (id_produit) {
  $("#message-sku-"+id_produit).html("");
}

function testSku (elm, id_produit) {
    
    var sku = $("#sku_"+id_produit).val();   
    sku=sku.replace(/^\s+|\s+$/,'');
	
    if(sku != ""){
        $(elm).prop("disabled", true);
        $("#message-sku-"+id_produit).html("<img src='/skins/soukeo/loading.gif' /> Vérfif...");  
        
        var theData = {
            sku : sku
        };
        
        jQuery.ajax({
            type : 'POST',
            url : "/app.php/checkSKU",
            data : theData,
            success : function(data) {
                
                if(data==0){
                    $("#message-sku-"+id_produit).html("<img border='0' alt='Ok' align='absmiddle' class='iconCircleCheck' src='/skins/common/images/picto/check.gif'><strong style='color:green'> Sku Disponible<strong>");
                    $(elm).prop("disabled", false);
                }else{
                    $("#message-sku-"+id_produit).html("<img border='0' alt='KO' align='absmiddle' class='iconCircleWarn' src='/skins/common/images/picto/denied.gif'><strong style='color:red'> Sku déja pris<strong>");
                    $(elm).prop("disabled", false);
                }
            }
        });  
    }
}

function testSku2 (elm) {
    
    var sku = $("#val_sku").val();   
    sku=sku.replace(/^\s+|\s+$/,'');
	
    if(sku != ""){
        $(elm).prop("disabled", true);
        $("#message-sku").html("<img src='/skins/soukeo/loading.gif' /> Vérfif...");  
        
        var theData = {
            sku : sku
        };
        
        jQuery.ajax({
            type : 'POST',
            url : "/app.php/checkSKU",
            data : theData,
            success : function(data) {
                
                if(data==0){
                    $("#message-sku").html("<img border='0' alt='Ok' align='absmiddle' class='iconCircleCheck' src='/skins/common/images/picto/check.gif'><strong style='color:green'> Sku Disponible<strong>");
                    $(elm).prop("disabled", false);
                }else{
                    $("#message-sku").html("<img border='0' alt='KO' align='absmiddle' class='iconCircleWarn' src='/skins/common/images/picto/denied.gif'><strong style='color:red'> Sku déja pris<strong>");
                    $(elm).prop("disabled", false);
                }
            }
        });  
    }else{
    	$("#message-sku").html("<img border='0' alt='KO' align='absmiddle' class='iconCircleWarn' src='/skins/common/images/picto/denied.gif'><strong style='color:red'> Sku vide<strong>");
    }
}


function validEditReferentiel(entity, entity_id){
	
	var theData = {
		entity : entity,
		entity_id : entity_id,
		label : $("#edit_label_"+entity).val()
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/editReferentiel",
		data : theData,
		success : function(data) {
			$("#messagebox_"+entity).html(data);
			$('#myModal'+entity).remove();
			refreshReferentielEntity(entity);
			$("#form_errors_"+entity).html("");
		}
	});
}


function deleteReferentiel (entity, entity_id) {
	
	var theData = {
		entity : entity,
		entity_id : entity_id,
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/deleteReferentiel",
		data : theData,
		success : function(data) {
			$('#myModalSuppr'+entity).remove();
			$("#messagebox_"+entity).html(data);
			refreshReferentielEntity(entity);
			$("#form_errors_"+entity).html("");
		}
	});
}


function refreshReferentielEntity (entity) {
	
	var theData = {
		entity : entity
	};
		
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getAllReferentielEntity",
		data : theData,
		success : function(data) {
			$('#ref_'+entity).html(data);
		}
	});	
}

///////////////////////////////////////////
function showDialogEditCredits(vendor_id) {
	
	var theData = {
		vendor_id : vendor_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/editVendorCredits",
		data : theData,
		success : function(data) {
			
			if( $('#myModalCredits').length == 0){
				
				if($('#content-client').length > 0){
					$('body').append(data);
				}else{
					$('#content').append(data);	
				}
					
			}
			$('#myModalCredits').modal({
				 keyboard: true
			});
		}
	});

	return false;
}

function showDialogEditLivraison(vendor_id) {
	
	var theData = {
		vendor_id : vendor_id
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/editVendorLivraison",
		data : theData,
		success : function(data) {
			
			if( $('#myModalLivraison').length == 0){
				
				if($('#content-client').length > 0){
					$('body').append(data);
				}else{
					$('#content').append(data);	
				}
					
			}
			$('#myModalLivraison').modal({
				 keyboard: true
			});
		}
	});

	return false;
}


function confirmModalDialog(message, title, yesCallback, noCallback) {
	
	var theData = {
		message : message,
		title : title
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getModalConfirmDialog",
		data : theData,
		success : function(data) {
			
			if( $('#myConfirmModalDialog').length == 0){

				$('body').append(data);
			}
			
			$('#myConfirmModalDialog').modal({
				 keyboard: true
			});
		}
	});
		
    var dialog = $('#modal_dialog_confirm').dialog();

    $('#btnYes').click(function() {
        $('#myConfirmModalDialog').remove();
        yesCallback();
    });
    $('#btnNo').click(function() {
        $('#myConfirmModalDialog').remove();
        noCallback();
    });
}

function validModifCredits(type, vendor_id){
	
	if( type == "custom"){
		var $val   = $('#inputCustomMt').val();
		var $label = $('#inputCustomLabel').val();
		
		if(!isNaN($val) && (parseFloat($val) == parseInt($val)) ){
			
			bootbox.confirm("Etes vous sûr de vouloir ajouter "+$val+" crédits à ce vendeur ?", function(result) {
				
				if(result==true){
					var theData = {
						vendor_id : vendor_id,
						custom_val : $val,
						custom_label : $label	
					};	
		
					jQuery.ajax({
							type : 'POST',
							url : "/app.php/editVendorCustomCredit",
							data : theData,
							success : function(data) {
								$('input[type="submit"]').click();
								$('#myModalCredits').remove();
								
								if($('#histo_credit_vendor').length > 0){
									refreshHistoCreditVendor(vendor_id);
								}
							}
						});	
				}
			}); 
			

		}else{
			alert("Entrez un entier svp!");
			return false;
		}		
	}
	else if( type == "select"){
		
		var $type_transact = $('#inputTransacType').val();
		var $payment_type_id = $('#inputPaymentType').val();
		var $vendor_id = vendor_id;
		
		theData = {
			vendor_id : vendor_id,
			pack_id : $type_transact,
			payment_type_id : $payment_type_id
		};	
		
		bootbox.confirm("Etes vous sûr de vouloir attribuer <strong>"+$( "#inputTransacType option:selected" ).text()+"</strong> au vendeur "+vendor_id+"? <br>Moyen de paiement : <strong>"+$('#inputPaymentType option:selected').text()+"</strong><br> Une <strong>facture</strong> sera générée et envoyée (sauf pour option offert)!", function(result) {
				
			if(result==true){
				jQuery.ajax({
						type : 'POST',
						url : "/app.php/editVendorSelectCredit",
						data : theData,
						success : function(data) {
							var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
							.dialog({
							    height: 'auto',
							    modal: false,
							    title: "Opération terminée !",
							    open: function(event, ui){
							     
							    },
							    close: function(event, ui){
									$(this).dialog("destroy");
									$(this).remove();
									$("#dialogAv").dialog("close");
								}
							});
							dial.dialog('open');
							
							if($('#histo_credit_vendor').length > 0){
								refreshHistoCreditVendor($vendor_id);
							}
						},
						beforeSend: function() {
								$loaddialog = $('<div id="prod_ecom_loader"></div>')
						    		.html('')
						    		.dialog({
						    			autoOpen: false,
						    			modal : true,
						    			draggable: false,
						    			closeText: "hide",
						    			title : 'Génération facture et mail...',
										closeOnEscape: false,
				   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
										close: function(event, ui){
											$(this).dialog("destroy");
											$(this).remove();
										},
					    			});
						    	$loaddialog.dialog('open');
						},
						complete: function(){
							$loaddialog.dialog('close');
							
							if($('#content-client').length > 0){
								
								refreshInfoVendor(vendor_id);
							}else{
								$('input[type="submit"]').click();	
							}
							$('#myModalCredits').remove();
						}
					});
			}
		}); 

	}	
}

function refreshHistoCreditVendor(vendor_id){
	
	var theData = {
		vendor_id : vendor_id
	}
	
	jQuery.ajax({
		type : 'POST',
		url : '/app.php/refreshHistoCreditVendor',
		data : theData,
		success : function(data) {
			if($('#histo_credit_vendor').length > 0){
				$('#histo_credit_vendor').html(data);
			}			
		}
	})
}


function showDialogAddVendor() {
	
	var theData = {};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/addVendor",
		data : theData,
		success : function(data) {
			
			if( $('#myModalVendor').length == 0){
				$('#content').append(data);	
			}
			$('#myModalVendor').modal({
				 keyboard: true
			});
		}
	});
	return false;
}

function validFormAddCustomer () {
	
	var theData = {
		lastname : $('#inputLastname').val(),
		company : $('#inputCompany').val(),
		firstname : $('#inputFirstname').val(),
		email : $('#inputEmail').val(),
		phone : $('#inputPhone').val(),
		phone2 : $('#inputPhone2').val(),
		birthd : $('#inputBirthd').val(),
		statut : $('#inputStatut_id').val(),
	}; 
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/insertCustomer",
		data : theData,
		success : function(data) {

			if(!isNaN(data)){
				
				var dial = $("<div id ='acknowledged-dialog'> </div>").html(theData.lastname + " " + theData.firstname + " a bien été ajouté !")
					.dialog({
					    height: 140,
					    modal: false,
					    title: "Opération réussite...",
					    open: function(event, ui){
					    setTimeout("$('#acknowledged-dialog').dialog('close')",1000);
					    },
					    close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
							$("#dialogAv").dialog("close");
						}
					});
				dial.dialog('open');

				$('input[type="submit"]').click();
				$('#myModal').remove();
				
				if($('#customer_tab').length > 0){
					$('#search_customer_id').val(data);
					searchCustomer();
				}
			}
			else{
				bootbox.alert("Une erreur s'est produite, veuillez contacter le service technique !");
			}

			
		}
	});
}

function validFormAddvendor () {
	
	var theData = {
		vendor_attn2 : $('#inputLastname').val(),
		vendor_attn : $('#inputFirstname').val(),
		vendor_nom : $('#inputName').val(),
		siret : $('#inputSiret').val(),
		email : $('#inputEmail').val(),
		telephone : $('#inputPhone').val(),
		street : $('#inputStreet').val(),
		city : $('#inputCity').val(),
		zip : $('#inputZip').val(),
		country_id : $('#inputCountryId').val(),
		statut : $('#inputStatut_id').val(),
		vendor_type : $('#inputType').val(),
		civilite : $('#inputCiv').val(),
		
	}; 
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/insertVendor",
		data : theData,
		success : function(data) {
			
			if(!isNaN(data)){
				
				var dial = $("<div id ='acknowledged-dialog'> </div>").html(theData.vendor_nom + " a bien été ajouté !")
					.dialog({
					    height: 140,
					    modal: false,
					    title: "Opération réussite...",
					    open: function(event, ui){
					    setTimeout("$('#acknowledged-dialog').dialog('close')",1000);
					    },
					    close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
							$("#dialogAv").dialog("close");
						}
					});
				dial.dialog('open');
				
				$('input[type="submit"]').click();
				$('#myModalVendor').remove();
				
				if($('#vendor_tab').length > 0){
					$('#search_vendor_id').val(data);
					searchVendor();
				}
			}
			else{
				bootbox.alert("Une erreur s'est produite, veuillez contacter le service technique !");
			}

		}
	});
}



function showDialogAddProduct(data) {

	var obj = jQuery.parseJSON(data);
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		success : function(data) {

			var $myDialog = $('<div id = "dialogAddProduct"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: true,
	    			height: "auto",
	    			width: 850,
	    			title: 'Ajout d\'un produit au catalogue Avahis',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
	       $('#checkboxtree').checkboxTree({
			    onCheck: {
			        ancestors: 'check',
			        descendants: 'uncheck',
			        others: 'uncheck'
			    },
			    onUncheck: {
			        descendants: 'uncheck'
			    }
			});
			
			$("#selectbox-popup").select2({
				minimumInputLength: 3,
				ajax: {
					url: "/app.php/getlistcateg",
					dataType: 'json',
					data: function (search, page) {
						return {
								label: search
								};
						},
						results: function (data, page) {
							return { results: data };
						}
				}
			});	
			
			$('.user_attr').change(function(){
										if( this.value ) {
											$('.av_attr').attr('disabled', true);
										}else if(this.value == ""){
											$('.av_attr').attr('disabled', false);
										}
			});
	    	$myDialog.dialog("open");
		}
	});

	return false;
}
function modifCat(data) {

	var prodId = $(data).parent().parent().parent().parent().attr('id');

	var theData = {
	   	url : '/catalogue/produit/modif_cat/index.php',
	   	id : prodId
    } ;
    
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : theData,
		success : function(data) {

			var $myDialog = $('<div id = "modifcat"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: true,
	    			height: "auto",
	    			width: 850,
	    			title: 'Modification de la catégories produit',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});

			        $('#checkboxtree').checkboxTree({
			            onCheck: {
			                ancestors: 'check',
			                descendants: 'uncheck',
			                others: 'uncheck'
			            },
			            onUncheck: {
			                descendants: 'uncheck'
			            }
			        });
        
					$("#selectbox-popup").select2({
						minimumInputLength: 3,
						ajax: {
							url: "/app.php/getlistcateg",
							dataType: 'json',
							data: function (search, page) {
								return {
										label: search
										};
								},
								results: function (data, page) {
									return { results: data };
								}
						}
					});	

	    			$myDialog.dialog("open");
		}
	});

	return false;
}

function modifCatOneProd(id_produit) {
	
	var theData = {
	   	url : '/catalogue/produit/modif_cat/index.php',
	   	id : id_produit
    } ;
    
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : theData,
		success : function(data) {

			var $myDialog = $('<div id = "modifcat"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: true,
	    			height: "auto",
	    			width: 850,
	    			title: 'Modification de la catégories produit',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});

			        $('#checkboxtree').checkboxTree({
			            onCheck: {
			                ancestors: 'check',
			                descendants: 'uncheck',
			                others: 'uncheck'
			            },
			            onUncheck: {
			                descendants: 'uncheck'
			            }
			        });
        
					$("#selectbox-popup").select2({
						minimumInputLength: 3,
						ajax: {
							url: "/app.php/getlistcateg",
							dataType: 'json',
							data: function (search, page) {
								return {
										label: search
										};
								},
								results: function (data, page) {
									return { results: data };
								}
						}
					});	

	    			$myDialog.dialog("open");
		}
	});

	return false;
}

function confirmSubmitTicket(){
	if( $('input[name=ticket_visibility]').is(':checked') ){
    	return bootbox.confirm("Êtes-vous sûr de vouloir envoyer ce commentaire au client également ?!")
	}else{
		return true;
	}
}

/*
 *Fonction permettant d'envoyer les données nécessaires en Ajax permettant
 * de valider l'association et le mapping des champs pour une catégorie entiere
 * de produits ecommercants 
 */
function validEditProd(){
	
	var theData = {};
	var prums = true ;
	theData['staticFields'] = [];
	theData['dynaFields'] = [];
	var nomChamp= '' ;
	var aInput =document.getElementsByTagName("input");
	var nbr = 0 ;

	var nbrAttribut  = aInput.length - 29 ;
	var posDernAttr = aInput.length - 1 ; 
	theData.dynaFields.push( {
		nbrAttribut : nbrAttribut
	}) ;
	// on commence par le 28 éme input
	for (var i = 30; i < posDernAttr ; i++)
	{
		if (aInput[i].type != 'submit'&& aInput[i].type == 'text'  && aInput[i].type != 'button' && aInput[i].id != '')
		{
			
			nomChamp ='';
			valeur = '';
			
			valeur = aInput[i].id+"|"+aInput[i].value;
			theData.dynaFields.push( {
				attribut : valeur
				}) ;
			
			
			nbr++;
		}	
	}
	

	theData.staticFields.push( {
		id_produit :  $('#id_produit').val(),
		sku  :  $('#sku').val(),
		categories : $('#categories').text(),
		categories_id : $('#categories_id').text(),
		name : $('#name').val(),
		description : $('#description').val(),
		description_html : $('#description_html').val(),
		short_description : $('#short_description').val(),
		price : $('#price').val(),
		weight : $('#weight').val(),
		country_of_manufacture : $('#country_of_manufacture').val(),
		ean : $('#ean').val(),
		meta_description : $('#meta_description').val(),
		meta_keyword : $('#meta_keyword').val(),
		meta_title : $('#meta_title').val(),
		image : $('#image').val(),
		small_image : $('#small_image').val(),
		thumbnail : $('#thumbnail').val(),
		local_image : $('#local_image').val(),
		local_small_image : $('#local_small_image').val(),
		local_thumbnail : $('#local_thumbnail').val(),
		manufacturer : $('#manufacturer').val(),
		dropship_vendor : $('#dropship_vendor').val(),
		attribute_set : $('#attribute_set').val()
	}) ;

	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validEditProd",
			data : theData,
			success : function(data) {

				var dial = $("<div id ='acknowledged-dialog'> </div>").html(data)
				.dialog({
				    height: 140,
				    modal: false,
				    title: "Sauvegarde...",
				    open: function(event, ui){
				    setTimeout("$('#acknowledged-dialog').dialog('close')",1000);
				    },
				    close: function(event, ui){
						$(this).dialog("destroy");
						$(this).remove();
						$("#dialogAv").dialog("close");
					}
				});
				dial.dialog('open');
			}
	}); 
}


/**
 * Appel pour affiché l'écran de sélection des catégories pour tous les produits selectionné. 
 */
function ModifCatAllSelectedProds (data) {
	
	var theData = {} ;
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/ModifCatAllSelectedProds",
		data : theData,
		success : function(data) {
			
			if( $('#myModalModifAllCat').length == 0){
				if($('#content-client').length > 0)
					$('#content-client').append(data);
				else		
					$('#content').append(data);
			}
			$('#myModalModifAllCat').modal({
				 keyboard: true
			});
		}
	});

	return false;
	/*
	var theData = {
	   	url : '/catalogue/produit/modif_all_cat/index.php'
	} ;

  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : theData,
		success : function(data) {
			var $myDialog = $('<div id = "modifcat"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: true,
	    			height: "auto",
	    			width: 850,
	    			title: 'Modification de la catégories des produits',
	    			close: function(event, ui){
					        // Remove the dialog elements
					        // Note: this will put the original div element in the dom
							$(this).dialog("destroy");
					        // Remove the left over element (the original div element)
							$(this).remove();
							//location.reload(true);  
						}
	    			});


	    			$myDialog.dialog("open");
		}
	});

	return false;
	*/
}


function showDialogAssocCateg(data) {

	var obj = jQuery.parseJSON(data);
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		success : function(data) {

			var $myDialog = $('<div id = "dialogAssocCateg"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: true,
	    			height: "auto",
	    			width: 850,
	    			title: 'Association des catégories',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});

			        $('#checkboxtree').checkboxTree({
			            onCheck: {
			                ancestors: 'check',
			                descendants: 'uncheck',
			                others: 'uncheck'
			            },
			            onUncheck: {
			                descendants: 'uncheck'
			            }
			        });
        
					$("#selectbox-popup").select2({
						minimumInputLength: 3,
						ajax: {
							url: "/app.php/getlistcateg",
							dataType: 'json',
							data: function (search, page) {
								return {
										label: search
										};
								},
								results: function (data, page) {
									return { results: data };
								}
						}
					});	

	    			$myDialog.dialog("open");
		}
	});

	return false;
}


function showDialogEcom(data) {

	var obj = jQuery.parseJSON(data);
	
	
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		success : function(data) {

			var $myDialog = $('<div id = "dialogEcom"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: false,
	    			height: "auto",
	    			width: 700,
	    			position : { my: "left", at: "left bottom", of: window },
	    			title: 'FICHE PRODUIT COMMERCANT',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
	    	$myDialog.dialog("open");
		}
	})

	return false;
}


function showDialogAv(data) {

	var obj = jQuery.parseJSON(data);
	
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		beforeSend: function() {
			$( "#dialogAv" ).dialog( "close" );
		},
		success : function(data) {

			var $myDialog = $('<div id = "dialogAv"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: false,
	    			height: 800,
	    			width: 815,
	    			position : { my: "right", at: "right bottom", of: window },
	    			title: 'FICHE PRODUIT AVAHIS',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
					}
	    			});
	    	$myDialog.dialog("open");
		}
	})

	return false;
}


function showDialogAvGP(data) {

	var obj = jQuery.parseJSON(data);
	
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		beforeSend: function() {
			$( "#dialogAv" ).dialog( "close" );
		},
		success : function(data) {

			var $myDialog = $('<div id = "dialogAv" class="well"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: false,
	    			height: 'auto',
	    			width: 815,
	    			title: '<i class="fa fa-file"></i> FICHE PRODUIT AVAHIS',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
					}
	    			});
	    	$myDialog.dialog("open");
		}
	})
	return false;
}

function showDialogFactureCommercant(data) {

	var obj = jQuery.parseJSON(data);
	
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/loadshowdialog",
		data : obj,
		beforeSend: function() {
			$( "#dialogAv" ).dialog( "close" );
		},
		success : function(data) {

			var $myDialog = $('<div id = "dialogFactureEcom_'+obj.vendor_id+'"></div>')
			.html(data)
			.dialog({
	    			autoOpen: false,
	    			modal: false,
	    			height: 'auto',
	    			width: 1080,
	    			height: 900,
	    			title: '<i class="fa fa-file"></i> Facture Commerçant',
	    			close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
					}
	    			});
	    	$myDialog.dialog("open");
		}
	})
	return false;
}

var timeout

var timeout = 500;
var closetimer = 0;
var ddmenuitem = 0;

function jsddm_open() {
	jsddm_canceltimer();
	jsddm_close();
	ddmenuitem = $(this).find('ul').css('visibility', 'visible');
}

function jsddm_close() {
	if (ddmenuitem)
		ddmenuitem.css('visibility', 'hidden');
}

function jsddm_timer() {
	closetimer = window.setTimeout(jsddm_close, timeout);
}

function jsddm_canceltimer() {
	if (closetimer) {
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

document.onclick = jsddm_close;

$(document).ready(function() {
	$('td:first-child').addClass("firstChild");
	$(".tab2 tr").each(function() {
		$(this).find('td:eq(1)').addClass("secondChild");
		$(this).find('td:eq(2)').addClass("thirdChild");
	});
	$(".tab tr").each(function() {
		$(this).find('td:eq(1)').addClass("secondChild");
		$(this).find('td:eq(2)').addClass("thirdChild");
	});

	$("#dialog").dialog({
		autoOpen : false,
		modal : true,
		height : 500,
		width : 800
	});

	$("#dialog2").dialog({
		autoOpen : false,
		modal : true,
		height : 500,
		width : 800
	});

	$("#err").dialog({
		autoOpen : false,
		modal : true,
		height : 300,
		width : 480
	});

	$('#jsddm > li').bind('mouseover', jsddm_open)
	$('#jsddm > li').bind('mouseout', jsddm_timer)

	jQuery.ajaxSetup({
		'beforeSend' : function(xhr) {
			xhr.overrideMimeType('text/html; charset=UTF-8');
		}
	});
	$('#contLoad').hide();// hide it initially
//	.ajaxStart(function() {
//		$(this).show();
//	}).ajaxStop(function() {
//		$(this).hide();
//	});
/*
	tinyMCE.init({
		mode : "textareas",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,image,code",
		theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,link,unlink,removeformat",
		theme_advanced_buttons3 : "",
		width : "280",
		height : "230"

		// content_css : "/inc/js/css/base.css"

	});*/

});


function loadOldData(elm) {
	$('input[id^="' + elm.id + '"][type="text"]:disabled').each(function() {
		$('#' + this.id.replace(/_O$/, "")).val($(this).val());
	})
	$('input[id^="' + elm.id + '"][type="checkbox"]:disabled').each(function() {
		if ( typeof ($(this).attr('checked') ) === 'undefined') {
			valck = false;
		} else {
			valck = $(this).attr('checked');
		}
		$('#' + this.id.replace(/_O$/, "")).attr('checked', valck);
	})
}

function delit(elm) {
	if (!$(elm).attr('checked')) {
		partid = elm.id.split('_');
		$('input[id^="' + partid[0] + '"][type="text"]:enabled').each(function() {
			$(this).val('');
		})
		$('input[id^="' + partid[0] + '"][type="checkbox"]:enabled').each(function() {
			$(this).attr('checked', false);
		})
	}
}



function inArray(needle, haystack) {
	var length = haystack.length;
	for (var i = 0; i < length; i++) {
		if (haystack[i] == needle)
			return true;
	}
	return false;
}

function sendAjaxForm(theData) {
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/save",
		data : theData,
		success : function(data) {
			$('#dialog').html(data);

		}
	})
}

function showChild(elm){
	

	//alert( $(elm).closest( "tr " ).children('td').length);
	if ($(elm).hasClass('closed')){
		var theData = {
		   		elm : elm.id
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadChild",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' )
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('+' );
	} 	
}

function showInfo (elm, entity_type) {
	
	elm.disabled=true;
	
	var theData = {
	   		elm : elm.id
 	} ;

	jQuery.ajax({
		type : 'POST',
		url : "/app.php/load"+entity_type+"Info",
		data : theData,
		success : function(data) {
			$( "#content" ).fadeOut( 400, "linear");
			$( "#menuRight" ).fadeOut( 400, "linear");
			$("#content").after("<div id='content-client' class='content navbar navbar-inverse' style='width:97%'>"+data+"</div>");
			$("#content-client").fadeIn(400, function (){
				elm.disabled=false;
				
			});
		}
	});
}


function closeContentClient(){
	$( "#content-client" ).fadeOut(400, "linear", function(){
		$( "#content-client" ).remove()
		$( "#content" ).fadeIn(400);
		$( "#menuRight" ).fadeIn(400);	
	});
	
}

function showComments (elm) {
	
	var tmp = elm.id.split('_');
	
	if ($(elm).hasClass('closed')){
		var theData = {
		   		type : tmp[1],
		   		id : tmp[2],
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadComments",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-');
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('?' );
	} 
}

function addComment (id,type) {
	
	var content = $("#inputContent_"+id).val();
	
	var theData = {
		id : id,
		type : type,
		content : content
	}
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/saveComment",
			data : theData,
			success : function(data) {
				var comm = jQuery.parseJSON(data);
				$("#comlist_"+id).prepend("<div class = 'well' id='comment_"+comm.id_comment+"' ><span class ='author'><strong>"+comm.author+"</strong></span> le <span class ='date'>"+comm.date_created+"</span><br/><br/><span class='comment_content'>"
				+comm.content+"</span><br/><br/><a href='#' onclick='editComment ("+comm.id_comment+"); return false;'>Editer</a>  -  <a href='#' onclick='supprComment ("+comm.id_comment+"); return false;'>Suppr.</a></div>");
				var count = $('#comment_'+type+'_'+id+" small").text();
				if(count == " " || count == "" )count = 0;
				$('#comment_'+type+'_'+id+" small").text(" "+(parseInt(count)+1));
			}
	});
}

function showFullText (elm) {
	
	if($(elm).text() == "(plus...)"){
		var $text = $(elm).next();
		$text.show("fast");
		elm.remove();
		$text.after(elm);
		$(elm).text("(... moins)");	
	}
	else if($(elm).text() == "(... moins)"){
		var $text = $(elm).prev();
		$text.hide("fast");
		elm.remove();
		$text.before(elm);
		$(elm).text("(plus...)");	
	}
	
}
function editComment (id_comment) {
	
	var content = $('#comment_'+id_comment+' .comment_content').text();
	$('#comment_'+id_comment).html('<textarea rows="3" id="inputEditContent_'+id_comment+'" placeholder="Entrez votre commentaire">'+content+'</textarea><button class="btn" onClick="validEditComment('+id_comment+')">Valider</button>'	);

}

function validEditComment (id_comment) {
	
	var content = $('#inputEditContent_'+id_comment).val();
	
	var theData = {
		id_comment : id_comment,
		content : content,
	}
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/editComment",
		data : theData,
		success : function(data) {
			var comm = jQuery.parseJSON(data);
			$("#comment_"+comm.id_comment).html("<span class ='author'><strong>"+comm.author+"</strong></span> le <span class ='date'>"+comm.date_created+"</span><em class='date_edit' style='color:red'> - Edité le "+comm.date_updated+"</em><br/><br/><span class='comment_content'>"
			+comm.content+"</span><br/><br/><a href='#' onclick='editComment ("+id_comment+"); return false;'>Editer</a>  -  <a href='#' onclick='supprComment ("+id_comment+"); return false;'>Suppr.</a>");
		}
	});
}

function supprComment (id_comment) {
	
	var theData = {
		id_comment : id_comment
	}
	
	var $myDialog = $('<div></div>')
    		.html('Êtes vous sûr de vouloir supprimer ce commentaire ?<br/><br/>Ok pour confirmer.  Annuler pour annuler.')
    		.dialog({
    			autoOpen: false,
    			title: 'Suppression de commentaire',
    			buttons: {
				      	"OK": function () {
				      			$.ajax({
									type : 'POST',
									url : "/app.php/deleteComment",
									data : theData,
									
									success : function(data) {
										$("#comment_"+id_comment).remove();
									}
								});	
				      		$(this).dialog("destroy");
						    return true;
				      		},
			      		"Cancel": function () {
				        	$(this).dialog("destroy");
				        	return false;
			      		}
    				}
  			});
		$myDialog.dialog('open');
}

function showOrderAvahisInfo (elm) {
	
	var tmp = elm.id.split('_');
	
	if ($(elm).hasClass('closed')){
		var theData = {
		   		po_id : tmp[1],
		   		order_id : tmp[2],
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadOrderAvahisInfo",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('+' );
	} 
}

function showAnnonceRepInfo (elm) {
	
	var tmp = elm.id.split('_');
	
	if ($(elm).hasClass('closed')){
		var theData = {
		   		ar_id : tmp[1]
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadAnnonceRepInfo",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('+' );
	} 
}

function showOrderVendorInfo (elm, next) {
	var tmp = elm.id.split('_');
	
	if ($(elm).hasClass('closed')){
		var theData = {
		   		vendor_id : tmp[1],
		   		next : next
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadOrderVendorInfo",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('+' );
	}  
}

function showPoVendorInfo (elm, next) {
	var tmp = elm.id.split('_');
	
	if ($(elm).hasClass('closed')){
		var theData = {
		   		po_id : tmp[1],
		   		next : next
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadPoVendorInfo",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('+' );
	}  
}

function showAbonnementVendorInfo (elm) {
	
	var tmp = elm.id.split('_');
	
	if ($(elm).hasClass('closed')){
		var theData = {
		   		vendor_id : tmp[1]
	 	} ;
	    
		jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadAbonnementVendorInfo",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				$('table.paginated').each(function() {
			        var currentPage = 0;
			        var numPerPage = 10;
			        var $table = $(this);
			        $table.bind('repaginate', function() {
			            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
			        });
			        $table.trigger('repaginate');
			        var numRows = $table.find('tbody tr').length;
			        var numPages = Math.ceil(numRows / numPerPage);
			        var $pager_wrapper = $('<div class="pagination pagination-centered"></div>');
			        var $pager = $('<ul></ul>');
			        for (var page = 0; page < numPages; page++) {
			            $('<li class="page-number" ></li>').html('<a href="#" onClick="return false;">'+(page + 1)+'</a>').bind('click', {
			                newPage: page
			            }, function(event) {
			                currentPage = event.data['newPage'];
			                $table.trigger('repaginate');
			                $(this).addClass('active').siblings().removeClass('active');
			            }).appendTo($pager).addClass('clickable');
			        };
			        $pager_wrapper.html($pager);
			        $pager_wrapper.insertBefore($table).find('li.page-number:first').addClass('active');
				});		
						
				
			}
		});
		$(elm).removeClass('closed');
		$(elm).addClass('opend' );
	}else{
		$(elm).closest( "tr" ).next().remove();
	
		$(elm).removeClass('opend' );
		$(elm).addClass('closed' );
		$(elm).val('+' );
	} 
}

function setProperty(elm, entity, property, type) {
	
	var $id = $(elm).parent().attr("id");
	
	var $val = $(elm).parent().text();
	
	
	$("#"+$id).html('<input class="input-mini" type="'+type+'" value="'+$val.trim()+'"/><button class="btn pull-right" onClick="validProperty(this, &#34;'+entity+'&#34;, &#34;'+property+'&#34;, &#34;'+type+'&#34;)">OK</button>');
}

function validProperty (elm, entity, property, type) {
	
	
	var $id = $(elm).parent().attr("id");
	
	var $val = $("#"+$id+ " input").val();
	
	$val = $val.trim();
	
	if(type == "number"){
		
		if(!isNaN($val)) 
			var $valid = true;
		else
			var $valid = false;
	}else{
		var $valid = true;
	}
	
	if($valid ){
		
		var $entity_id = $id.split('_');

		$entity_id = $entity_id[$entity_id.length - 1];
		
		var theData = {
		   		entity_id : $entity_id,
		   		val : $val
	 	} ;
	 	
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/edit"+entity+"?property="+property,
				data : theData,
				success : function(data) { 

					$("#"+entity+"_messagebox").html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations ! <strong>'+property+'</strong> a été modifié !</div>');
					$("#"+$id).html($val + ' <a class="pull-right" style="margin-right:5px" href="#" onClick="setProperty(this, &#34;'+entity+'&#34;, &#34;'+property+'&#34;, &#34;'+type+'&#34;)"><img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Edit" alt="Edit"></a> ');			
				}
			});
	}else{
		bootbox.alert("Mauvais format de données");
	}		
}

function editTypeVendor(entity_id){
	
	theData = {
		entity_id : entity_id,
		vendor_type : $('#select_type_vendor').val()
	};
	
	jQuery.ajax({
		type : 'POST',
		url : '/app.php/editTypeVendor',
		data : theData,
		success : function(data){
			$('#vendor_messagebox').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations ! <strong>Le type vendeur </strong> a été modifié !</div>')
		}
	});
}

function editStatutVendor(entity_id){
	
	theData = {
		entity_id : entity_id,
		statut : $('#select_statut_vendor').val()
	};
	
	jQuery.ajax({
		type : 'POST',
		url : '/app.php/editStatutVendor',
		data : theData,
		success : function(data){
			$('#vendor_messagebox').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations ! <strong>Le statut vendeur </strong> a été modifié !</div>')
		}
	});
}

function editStatutCustomer (entity_id) {
	theData = {
		entity_id : entity_id,
		statut : $('#select_statut_customer').val()
	};
	
	jQuery.ajax({
		type : 'POST',
		url : '/app.php/editStatutCustomer',
		data : theData,
		success : function(data){
			$('#customer_messagebox').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations ! <strong>Le statut acheteur </strong> a été modifié !</div>')
		}
	});
}

function showFormEditClient(entity_id){
	
	var theData = {
		entity_id : entity_id
	};
	 
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getFormEditCustomer",
		data : theData,
		beforesend : function(){
			$("#infoClient").html('<div id="prod_ecom_loader"></div>');
		},
		success : function(data) {
			$('#prod_ecom_loader').remove()
			$("#infoClient").html(data);		
		}
	});
}

function showFormEditVendor(entity_id){
	
	var theData = {
		entity_id : entity_id
	};
	 
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getFormEditVendor",
		data : theData,
		beforesend : function(){
			$("#infoClient").html('<div id="prod_ecom_loader"></div>');
		},
		success : function(data) {
			$('#prod_ecom_loader').remove()
			$("#infoClient").html(data);		
		}
	});
}


function exitFormEditCustomer(entity_id){
	
	var theData = {
		entity_id : entity_id
	};
	 
	jQuery.ajax({
				type : 'POST',
				url : "/app.php/exitFormEditCustomer",
				data : theData,
				beforesend : function(){
					$("#infoClient").html('<div id="prod_ecom_loader"></div>');
				},
				success : function(data) {
					$('#prod_ecom_loader').remove()
					$("#infoClient").html(data);		
				}
			});	
}

function exitFormEditVendor(entity_id){
	var theData = {
		entity_id : entity_id
	};
	 
	jQuery.ajax({
				type : 'POST',
				url : "/app.php/exitFormEditVendor",
				data : theData,
				beforesend : function(){
					$("#infoClient").html('<div id="prod_ecom_loader"></div>');
				},
				success : function(data) {
					$('#prod_ecom_loader').remove()
					$("#infoClient").html(data);		
				}
			});	
}

function validFormEditCustomer (entity_id){
	
	var theData = {
		entity_id : entity_id,
		statut : $("#edit_cust_statut").val(),
		email : $("#edit_cust_mail").val(),
		phone : $("#edit_cust_tel1").val(),
		phone2 : $("#edit_cust_tel2").val(),
		birthd : $("#edit_cust_birthd").val(),
		more_info : $("#edit_cust_more_info").val()
		
	};
	 
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/validFormEditCustomer",
		data : theData,
		beforesend : function(){
			$("#infoClient").html('<div id="prod_ecom_loader"></div>');
		},
		success : function(data) {
			exitFormEditCustomer(entity_id);		
		}
	});		
}

function validFormEditVendor (entity_id){
	
	var theData = {
		entity_id : entity_id,
		vendor_nom : $("#edit_vendor_nom").val(),
		siret : $("#edit_siret").val(),
		statut : $("#edit_statut").val(),
		vendor_type : $("#edit_vendor_type").val(),
		vendor_attn : $("#edit_vendor_attn").val(),
		vendor_attn2 : $("#edit_vendor_attn2").val(),
		street : $("#edit_street").val(),
		city : $("#edit_city").val(),
		zip : $("#edit_zip").val(),
		country_id : $("#edit_country_id").val(),
		email : $("#edit_email").val(),
		telephone : $("#edit_telephone").val(),
		telephone2 : $("#edit_telephone2").val(),
		birthd : $("#edit_birthd").val()
		
	};
	 
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/validFormEditVendor",
		data : theData,
		beforesend : function(){
			$("#infoClient").html('<div id="prod_ecom_loader"></div>');
		},
		success : function(data) {
			exitFormEditVendor(entity_id);		
		}
	});		
}

function refreshInfoVendor (vendor_id) {
	
	theData = {
		vendor_id : vendor_id
	};
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/refreshInfoVendor",
		data : theData,
		beforesend : function(){
			$("#infoClient").html('<div id="prod_ecom_loader"></div>');
		},
		success : function(data) {
			$('#prod_ecom_loader').remove();
			$('#infoClient').html(data);		
		}
	});	
}

function setProductLimit(elm) {
	
	var $id = $(elm).parent().attr("id");
	
	var $val = $(elm).parent().text();
	
	$("#"+$id).html('<input class="input-mini" type="number" step="1" value="'+$val.trim()+'"/><button class="btn" onClick="validProductLimit(this)">OK</button>');  
}

function validProductLimit (elm) {
	
	
	var $id = $(elm).parent().attr("id");
	
	var $val = $("#"+$id+ " input").val();
	
	$val = $val.trim();
	
	if(!isNaN($val) && (parseFloat($val) == parseInt($val)) ){
		
		var $vendor_id = $id.split('_');
		$vendor_id = $vendor_id[1];
		
		var theData = {
		   		vendor_id : $vendor_id,
		   		val : $val
	 	} ;
	 	
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/editVendorProductLimit",
				data : theData,
				success : function(data) {
					$("#"+$id).html($val + ' <a class="pull-right btn" style="margin-right:5px" href="#" onClick="setProductLimit(this)"><img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Assigner commande" alt="Assigner commande"></a> ');			
				}
			});
	}else{
		bootbox.alert("Entrez un entier positif");
	}		
}


function setCommCateg(elm) {
	
	var $id = $(elm).parent().attr("id");
	
	var $val = $(elm).parent().text();
	
	$("#"+$id).html('<input class="input-mini" type="float" step="1" value="'+$val.trim()+'"/><button class="btn" onClick="validCommCateg(this)">OK</button>');  
}

function validCommCateg (elm) {
	
	var $id = $(elm).parent().attr("id");
	
	var $val = $("#"+$id+ " input").val();
	
	$val = $val.trim();
	
	if(!isNaN($val) ){
		
		var $cat_id = $id.split('_');
		$cat_id = $cat_id[1];
		
		var theData = {
		   		cat_id : $cat_id,
		   		val : $val
	 	} ;
	 	
	 	var id_comm_to_open = $(elm).parents(".vendorpolist").find(".opend").attr('id');
		var tmp = id_comm_to_open.split("_");
		tmp = tmp[2];
		var id_vendor_to_open = "vendor_"+tmp+"_"+tmp;
					
		jQuery.ajax({
				type : 'POST',
				url : "/app.php/editCommCateg",
				data : theData,
				success : function(data) {
					
					$("#"+$id).html($val + ' <a style="margin-right:5px" href="#" onClick="setCommCateg(this); return false;"><img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/edit.gif" title="Modifier Taux" alt="Modifier Taux"></a> ');
					
					jQuery.ajax({
						type : 'get',
						url : "/common/grid/grid.service.php?path=&function=reload&name=facturationcommercants",
						success : function(data) {
							$('#grid_facturationcommercants').html(data);
		    				$(function(){
		    					$('#'+id_vendor_to_open).trigger("click");
							}); 
							
						},
						beforeSend: function() {
								$loaddialog = $('<div id="prod_ecom_loader"></div>')
						    		.html('')
						    		.dialog({
						    			autoOpen: false,
						    			modal : true,
						    			draggable: false,
						    			closeText: "hide",
						    			title : 'Rechargement de la liste...',
										closeOnEscape: false,
				   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
										close: function(event, ui){
											$(this).dialog("destroy");
											$(this).remove();
										},
					    			});
						    	$loaddialog.dialog('open');
						},
						complete: function(){
							$loaddialog.dialog('close');
							
						}
					});					
				}
			});
	}else{
		alert("Entrez nombre positif");
	}		
}

function litigeIt(num, action) {
		var tmp = num.split('_');
		
		var theData = {
		   		type : tmp[0],
		   		num  : tmp[1]
	 	};
	    
	    if(action == "1"){
		    jQuery.ajax({
				type : 'POST',
				url : "/app.php/litigeIt?action="+action,
				data : theData,
				success : function(data) {
					$( "#"+tmp[0]+ "_"+tmp[1]+"_etat").text("litige") ;		
					$( "#litigeit_"+tmp[1]).attr("onClick", "litigeIt('"+tmp[0]+"_"+tmp[1]+"', 0);return false;")
										   .html('<img border="0" alt="Annuler un litige " align="absmiddle" title="Annuler litige"  class="iconAction" src="/skins/common/images/picto/check.gif"></a>');			
				}
			});	
	    }
		else if(action == "0"){
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/litigeIt?action="+action,
				data : theData,
				success : function(data) {
					$( "#"+tmp[0]+ "_"+tmp[1]+"_etat").text("") ;
					$( "#litigeit_"+tmp[1]).attr("onClick", "litigeIt('"+tmp[0]+"_"+tmp[1]+"', 1);return false;")
					   .html('<img border="0" alt="Déclarer litige " align="absmiddle" title="Déclarer litige"  class="iconAction" src="/skins/common/images/picto/denied.gif"></a>');	
				}
			});	
		}
		
}

function payIt (num, action) {
		var tmp = num.split('_');
		
		var theData = {
		   		type : tmp[0],
		   		num  : tmp[1]
	 	} ;
	    
	    if(action == "1"){
		    jQuery.ajax({
				type : 'POST',
				url : "/app.php/payIt?action="+action,
				data : theData,
				success : function(data) {
					$( "#"+tmp[0]+ "_"+tmp[1]+"_payment").text("Payé") ;		
					$( "#payit_"+tmp[1]).attr("onClick", "payIt('"+tmp[0]+"_"+tmp[1]+"', 0);return false;")
										   .html('<img border="0" alt="Annuler un payment" align="absmiddle" title="Annuler payment"  class="iconAction" src="/skins/common/images/picto/undo.gif"></a>');			
				}
			});	
	    }
		else if(action == "0"){
			jQuery.ajax({
				type : 'POST',
				url : "/app.php/payIt?action="+action,
				data : theData,
				success : function(data) {
					$( "#"+tmp[0]+ "_"+tmp[1]+"_payment").text("") ;
					$( "#payit_"+tmp[1]).attr("onClick", "payIt('"+tmp[0]+"_"+tmp[1]+"', 1);return false;")
					   .html('<img border="0" alt="Déclarer payment" align="absmiddle" title="Déclarer payment"  class="iconAction" src="/skins/common/images/picto/dollar.gif"></a>');	
				}
			});	
		}
		  
}

function payFacture (facture_id){
	
	var theData = {
		facture_id : facture_id
	} ;
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/payFacture",
			data : theData,
			success : function(data) {
				
				var $info = jQuery.parseJSON(data);
				
				var $text = $info.payment + " ligne(s) payée(s) ";
				 
				if ( $info.litiges != null && $info.litiges != 0 ){
					$text += " et " +$info.litiges+" lignes en litige"; 
				}
					
				var $dialogRecap = $('<div></div>').html($text).dialog({
    				autoOpen: false,
    				width : 'auto',
    				title: 'Récapitulatif:',
    				buttons:{
    					"OK":function (){
    						$(this).dialog('destroy').remove();
    						$( "#payment_"+facture_id).text("") ;
							$( "#payment_"+facture_id).attr("class", "label label-success")
					   		.text('complete');	
    					},
	      				"Annuler": function () {
		        			$(this).dialog('destroy').remove()
		        			return false;
	      				}}});
				$dialogRecap.dialog('open');
			}
	});				
}

function payAllFacture(argument) {
	
	var theData = {} ;
	theData['facture'] = [] ;
	 
	$("input:checked").each(function(){
		var tmp = $(this).attr("id");
		var tmp = tmp.split('_');
		
		if(tmp[0] == "facture"){
			theData.facture.push(tmp[1]);
		}
	});
}

function sendMailFact (facture_id) {
	
	var theData = {
		facture_id : facture_id
	} ;
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/sendMailFacture",
			data : theData,
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Envoi de l\'email...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
			success : function(data) {
				
				$('#prod_ecom_loader').remove();
					
				var $dialogRecap = $('<div></div>').html(data).dialog({
    				autoOpen: false,
    				width : 'auto',
    				title: 'Récapitulatif:',
    				buttons:{
    					"OK":function (){
    						$(this).dialog('destroy').remove();
    						$('input[type="submit"]').click();
    					},
	      				"Annuler": function () {
		        			$(this).dialog('destroy').remove()
		        			return false;
	      				}}});
				$dialogRecap.dialog('open');
			}
	});	
}


function sendAllFactMail() {
	
	var theData = {} ;
	theData['facture'] = [] ;
	 
	$("input:checked").each(function(){
		var tmp = $(this).attr("id");
		var tmp = tmp.split('_');
		
		if(tmp[0] == "facture"){
			theData.facture.push(tmp[1]);
		}
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/sendAllFactMail",
			data : theData,
			
			success : function(data) {
				$('#prod_ecom_loader').remove();
					
				var $dialogRecap = $('<div></div>').html(data).dialog({
    				autoOpen: false,
    				width : 'auto',
    				title: 'Récapitulatif:',
    				buttons:{
    					"OK":function (){
    						$(this).dialog('destroy').remove();
    						$('input[type="submit"]').click();
    					},
	      				"Annuler": function () {
		        			$(this).dialog('destroy').remove()
		        			return false;
	      				}}});
				$dialogRecap.dialog('open');
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Envoi des e-mails...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
	});		
}


function supprAllFactCom() {
	
	var theData = {} ;
	theData['facture'] = [] ;
	 
	$("input:checked").each(function(){
		var tmp = $(this).attr("id");
		var tmp = tmp.split('_');
		
		if(tmp[0] == "facture"){
			theData.facture.push(tmp[1]);
		}
	});
	
	$.ajax({
			type : 'POST',
			url : "/app.php/supprAllFactCom",
			data : theData,
			
			success : function(data) {
				$('#prod_ecom_loader').remove();
					
				var $dialogRecap = $('<div></div>').html("Factures bien supprimées !").dialog({
    				autoOpen: false,
    				width : 'auto',
    				title: 'Récapitulatif:',
    				buttons:{
    					"OK":function (){
    						$(this).dialog('destroy').remove();
    						$('input[type="submit"]').click();
    					},
	      				"Annuler": function () {
		        			$(this).dialog('destroy').remove()
		        			return false;
	      				}}});
				$dialogRecap.dialog('open');
			},
			beforeSend: function() {
				$loaddialog = $('<div id="prod_ecom_loader"></div>')
		    		.html('')
		    		.dialog({
		    			autoOpen: false,
		    			modal : true,
		    			draggable: false,
		    			closeText: "hide",
		    			title : 'Suppression des factures...',
		    			closeOnEscape: false,
   						open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },
						close: function(event, ui){
							$(this).dialog("destroy");
							$(this).remove();
						}
	    			});
		    	$loaddialog.dialog('open');
			},
	});		
}

function changeStatusOrder (order_id, state) {
	
	var theData = {
		order_id : order_id,
		state : state
	} ;
	    
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/changeStatusOrderGrossiste",
		data : theData,
		success : function(data) {

			$( "#status_"+order_id+" .dropdown-toggle").html(state+' <span class="caret"></span>') ;
			if(state == "New"){
				$( "#status_"+order_id+" .dropdown-toggle").attr("class", "btn btn-info dropdown-toggle")
			}
			if(state == "Completed"){
				$( "#status_"+order_id+" .dropdown-toggle").attr("class", "btn btn-success dropdown-toggle")
			}
			if(state == "Canceled"){
				$( "#status_"+order_id+" .dropdown-toggle").attr("class", "btn btn-danger dropdown-toggle")
			}					
		}
	});	
}

function changeStateTicket (ticket_id, state) {
	
	var theData = {
		ticket_id : ticket_id,
		state : state
	} ;
	    
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/changeStateTicket",
		data : theData,
		success : function(data) {
			$( "#ticketstate_"+ticket_id+" .dropdown-toggle").html(state+' <span class="caret"></span>') ;
			if(state == "Nouveau"){
				$( "#ticketstate_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-inverse dropdown-toggle")
			}
			if(state == "En cours"){
				$( "#ticketstate_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-info dropdown-toggle")
			}
			if(state == "Terminé"){
				$( "#ticketstate_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-success dropdown-toggle")
			}
			if(state == "Annulé"){
				$( "#ticketstate_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-danger dropdown-toggle")
			}		
			if($('#relance_ticket_list').length > 0){
				
				refreshRelanceTicketList();
			}			
		}
	});	
}

function refreshRelanceTicketList(){

	var theData = {} ;
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/refreshRelanceTicketList",
		data : theData,
		success : function(data) {
			$('#relance_ticket_list').html(data);
		}
	});		
}

function refreshMiniRelanceTicketList(ticket_id){

	var theData = {
		ticket_id: ticket_id
	} ;
	
	jQuery.ajax({
		type : "POST",
		url : "/app.php/refreshMiniRelanceTicketList",
		data : theData,
		success : function(data) {
			$('#relances').html(data);
		}
	});		
}

function changePriorityTicket (ticket_id, priority) {
	
	var theData = {
		ticket_id : ticket_id,
		priority : priority
	} ;
	    
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/changePriorityTicket",
		data : theData,
		success : function(data) {
			$( "#ticketpriority_"+ticket_id+" .dropdown-toggle").html(priority+' <span class="caret"></span>') ;
			if(priority == "Basse"){
				$( "#ticketpriority_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-inverse dropdown-toggle")
			}
			if(priority == "Moyenne"){
				$( "#ticketpriority_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-warning dropdown-toggle")
			}
			if(priority == "Haute"){
				$( "#ticketpriority_"+ticket_id+" .dropdown-toggle").attr("class", "btn btn-danger dropdown-toggle")
			}
			return false;					
		}
	});	
}

function setActifSession (elm) {
	
	var $actif = $(elm).prop("checked");
	
	var theData = {
		actif : $actif
	}
	
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/setProductActifGp",
		data : theData,
		success : function(data) {
								
		}
	});	
}


function changeTypeAbo(vendor_id, abonnement_id) {

	var theData = {
		vendor_id : vendor_id,
		abonnement_id : abonnement_id
	} ;
	    
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/changeTypeAboVendor",
		data : theData,
		success : function(data) {
			var obj = jQuery.parseJSON(data);
			
			$( "#abo_"+vendor_id+" .dropdown-toggle").html(obj.html) ;

			$( "#abo_"+vendor_id+" .dropdown-toggle").attr("class", "btn btn-"+obj.class+" dropdown-toggle")
								
		}
	});	
	  
}

function changeStatusAR (ar_id, state) {
	
	var theData = {
		ar_id : ar_id,
		state : state
	} ;
	    
    jQuery.ajax({
		type : 'POST',
		url : "/app.php/changeStatusAnnonceReception",
		data : theData,
		success : function(data) {

			$( "#status_"+ar_id+" .dropdown-toggle").html(state+' <span class="caret"></span>') ;
			if(state == "New"){
				$( "#status_"+ar_id+" .dropdown-toggle").attr("class", "btn btn-info dropdown-toggle");
				$( "#"+ar_id+"_daterecep").text("");
			}
			if(state == "Receptionne"){
				$( "#status_"+ar_id+" .dropdown-toggle").attr("class", "btn btn-success dropdown-toggle")
				$( "#"+ar_id+"_daterecep").text(data);
			}
			if(state == "Transit"){
				$( "#status_"+ar_id+" .dropdown-toggle").attr("class", "btn btn-warning dropdown-toggle");
				$( "#"+ar_id+"_daterecep").text("");
			}
			if(state == "Canceled"){
				$( "#status_"+ar_id+" .dropdown-toggle").attr("class", "btn btn-danger dropdown-toggle");
				$( "#"+ar_id+"_daterecep").text("");
			}					
		}
	});	
}

function generateSelectUser (elm, ticket_id) {
  

	
  if($(elm).text() == "(Voir)"){
	  var theData = {
	  	ticket_id : ticket_id
	  } ;
  
	$(elm).parent().prepend('<input type="hidden" id="listuser_'+ticket_id+'" />');
	
	var multiselect = $('#listuser_'+ticket_id);
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getAllUsers",
		data : theData,
		success : function(data) {

			var obj = jQuery.parseJSON(data);
			var list = obj.data;
			var selected = obj.selected;
			
			$(multiselect).select2({
			    data:list,
			    multiple: true,
			    width: "250px"
			});
			$(multiselect).select2("val", selected);
			$(elm).text("(OK)");
		}
	});	
  }
  else if($(elm).text() == "(OK)"){
  	
  	var multiselect = $('#listuser_'+ticket_id);
  	
  	var theData = {
  		user_ids : $(multiselect).select2('data'),
  		ticket_id : ticket_id
  	};
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/assignTicketUser",
		data : theData,
		success : function(data) {
			$("#s2id_listuser_"+ticket_id).remove();
  		 	$(elm).text("(Voir)");
		}
		});	
  	}
}

function setVendorCateg (elm){
	
	var $id = $(elm).parent().attr("id");
	var $tmp = $id.split("_");
	var $entity_id = $tmp[1];
	
	var theData = {
		entity_id : $entity_id
	} ;
	
	$("#"+$id).html('<input type="hidden" id="list_vendorcateg" /><button class="btn pull-right" onClick="validVendorCateg(this, '+$entity_id+')">OK</button>');
	
	var multiselect = $('#list_vendorcateg');
	
	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getAllUnivers",
		data : theData,
		success : function(data) {
			
			var obj = jQuery.parseJSON(data);
			var list = obj.data;
			var selected = obj.selected;
			
			$(multiselect).select2({
			    data:list,
			    multiple: true,
			    width: "250px"
			});
			$(multiselect).select2("val", selected);
			$(elm).text("(OK)");
		}
	});		
}

function validVendorCateg (elm, entity_id) {
	
	var $id = $(elm).parent().attr("id");
	
  	var multiselect = $('#list_vendorcateg');
  	
  	var theData = {
  		cat_ids : $(multiselect).select2('data'),
  		entity_id : entity_id
  	};
  	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/assignVendorCateg",
		data : theData,
		success : function(data) {
			$("#"+$id).html(data+"<a class='pull-right' href='#' onClick='setVendorCateg(this); return false;' style='margin-right:5px'><img class='iconAction' border='0' align='absmiddle' src='/skins/common/images/picto/edit.gif' title='Editer' alt='Editer'></a>")
			$('#vendor_messagebox').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Félicitations ! <strong>Univers vendeur </strong> a été modifié !</div>')
		}
	});	
}


function filterPoCommercant (elm) {
	var tmp = elm.id.split("_");
	
	vendor_id = tmp[1];

	jQuery.ajax({
			type : 'POST',
			url : "/app.php/loadOrderVendorInfo?order=complete",
			data : theData,
			success : function(data) {
				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				
			}
		});
}

function sendformMenu (elm) {
	var tmp = elm.id;
//	alert(elm.id);
	var tt = elm.id.split('_');

	datamenu = $(elm).serialize();
	//alert(datamenu);
		var theData = {
  		id_menu: tmp,
  		data : datamenu
  	};
	
	
	jQuery.ajax({
			type : 'POST',
			url : "/app.php/saveeditmenu",
			data : theData,
			success : function(data) {
				alert('test');
				$('#c'+tt[1]).html(data);
/*				var tdl = $(elm).closest( "tr " ).children('td').length + 1;
				$(elm).closest( "tr" ).after('<tr><td colspan="'+tdl+'" >'+data+' </td></tr>');
				$(elm).val('-' );
				$('.popover-markup>.trigger').popover({
						    html: true,
						    title: function () {
						        return $(this).parent().find('.head').html();
						    },
						    content: function () {
						        return $(this).parent().find('.content').html();
						    },
						    placement : 'top'
						});
				*/
			}
		});
	
}






function  plierMenu(elm) {
	
	if($(elm).attr('src') == "/skins/common/images/picto/add.gif"){
    	
    	$( "#menuRight" ).animate({
			up: "+=50",
			height: "toggle"
  			}, 500, function() {
    				$("#content").animate({'width':'98%'}, 500, function(){
    					$(elm).attr('src', "/skins/common/images/picto/back.gif");				
    				});
  			});
	
			
	}
	else if($(elm).attr('src') == "/skins/common/images/picto/back.gif"){

		$("#content").animate({
			"width":'81%'
			}, 500, function() {
    			$( "#menuRight" ).animate({
					up: "+=50",
					height: "toggle"
  					}, 500, function(){
  						$(elm).attr('src', "/skins/common/images/picto/add.gif");	
  					});
  			});
		
	}
}

$('.popover-markup>.trigger').popover({
    html: true,
    title: function () {
        return $(this).parent().find('.head').html();
    },
    content: function () {
        return $(this).parent().find('.content').html();
    }
});


/**
 *affichie en alert le contenut d'un object
 * @param obj la variable object qu'on veut vérifier
 *  
 */
function var_dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);
   }
/*
 * Autocomplete Plugin for JQuery
 * Version :
 * Date :
 * Licence : GPL v3 : http://www.gnu.org/licenses/gpl.html
 * Author : DEMONTE Jean-Baptiste
 * Contact : jbdemonte@gmail.com
 * Web site :
 */

( function(f) {
		var c = ["enable", "disable", "flushCache", "trigger", "display", "close"], b = this, d = "autocomplete";
		var e = {
			ajax : {
				url : document.URL,
				type : "POST",
				dataType : "json",
				data : {}
			},
			cb : {
				populate : null,
				cast : null,
				process : null,
				preselect : null,
				select : null,
				unselect : null
			},
			width : "auto",
			delay : 250,
			name : null,
			minLength : 1,
			cache : true,
			once : false,
			filter : false,
			source : null,
			prefix : true,
			splitChr : null,
			autohide : false,
			loop : true,
			className : d
		};
		function g(h) {
			var i;
			if (f.isArray(h)) {
				i = [];
				f.each(h, function(j, k) {
					i.push(k)
				})
			} else {
				if ( typeof (h) === "object") {
					i = f.extend(true, {}, h)
				} else {
					i = h
				}
			}
			return i
		}

		function a(q) {
			var r = {}, j, m, o, i = -1, t = 0, s, h = {}, k = false, n = false, p = this, l = {
				key : function(u) {
					p.key.apply(p, [u])
				},
				focusout : function(u) {
					if (!f(this).data(d + "-focus")) {
						p.hide(true)
					}
				},
				dblclick : function() {
					if (!o) {
						p.updateTOComplete()
					}
				}
			};
			this.init = function(u) {
				r = f.extend(true, {}, e, u);
				if ( typeof (r.source) === "string") {
					r.source = this.splitData(r.source)
				}
				if (u && ( typeof (u) === "object") && ( typeof (u.once) !== "undefined") && u.once) {
					if ( typeof (u.filter) === "undefined") {
						r.filter = true
					}
				}
				q.attr("autocomplete", "off");
				this.bind()
			};
			this.splitData = function(u) {
				if (r.splitChr) {
					return u.split(r.splitChr)
				} else {
					return u.split(/\r\n|\r|\n/)
				}
			};
			this.getSource = function(u) {
				if ( typeof (u) === "function") {
					return this.getSource.apply(this, [u.apply(q, [q.val()])])
				} else {
					if ( typeof (u) === "string") {
						return this.splitData(u)
					}
				}
				return u
			};
			this.flush = function() {
				h = {}
			};
			this.bind = function() {
				if (!k) {
					q[f.browser.opera?"keypress":"keydown"](l.key);
					q.focusout(l.focusout);
					q.dblclick(l.dblclick);
					k = true
				}
			};
			this.unbind = function() {
				if (k) {
					q.unbind(f.browser.opera ? "keypress" : "keydown", l.key);
					q.unbind("focusout", l.focusout);
					q.unbind("dblclick", l.dblclick);
					k = false
				}
			};
			this.updateToAutoHide = function() {
				if (!r.autohide) {
					return
				}
				this.stopToAutoHide();
				m = setTimeout(function() {
					p.hide(p, [true])
				}, r.autohide)
			};
			this.stopToAutoHide = function() {
				if (m) {
					clearTimeout(m);
					m = null
				}
			};
			this.updateTOComplete = function(u) {
				var v = this;
				clearTimeout(j);
				j = setTimeout(function() {
					v.complete.apply(v, [])
				}, u ? 0 : r.delay)
			};
			this.hover = function(v, u) {
				var w = o ? f("li", o).eq(v) : null;
				if (w) {
					w[(u?"add":"remove")+"Class"]("hover");
					if (u) {
						this.scroll(w)
					}
				}
			};
			this.scroll = function(u) {
				var x = o.scrollTop(), v = o.innerHeight(), y = u.position().top, w = u.outerHeight();
				if (y < 0) {
					n = true;
					o.scrollTop(x + y)
				} else {
					if (y + w > v) {
						n = true;
						o.scrollTop(x + y - v + w)
					}
				}
			};
			this.getPageUpDownItem = function(u) {
				if (!o) {
					return false
				}
				var x = o.innerHeight(), v = 0, w = i;
				f("li", o).each(function(A, z) {
					var y = f(z);
					v += (y.position().top >= 0) && (y.position().top + y.outerHeight() <= x) ? 1 : 0
				});
				if (i < 0) {
					return ( u ? t : v) - 1
				}
				w += u ? -v : v;
				w = Math.max(0, w);
				w = Math.min(w, t - 1);
				if (r.loop && (w == i)) {
					w = w === 0 ? t - 1 : 0
				}
				return w
			};
			this.key = function(v) {
				var w = v.keyCode, u;
				if (w === 9) {
				} else {
					if (!o && (w !== 27) && (w !== 13)) {
						this.updateTOComplete()
					} else {
						if ((w === 38) || (w === 40)) {
							u = i + (w === 38 ? -1 : 1);
							if (r.loop) {
								if (u < 0) {
									u = t - 1
								} else {
									if (u > t - 1) {
										u = 0
									}
								}
							}
							u = Math.max(0, u);
							u = Math.min(u, t - 1);
							this.preselect(u);
							v.preventDefault()
						} else {
							if ((w === 33) || (w === 34)) {
								u = this.getPageUpDownItem(w === 33);
								if (u !== false) {
									this.preselect(u)
								}
								v.preventDefault()
							} else {
								if (w === 13) {
									if (i !== -1) {
										this.select(i, f("li", o).eq(i).text());
										v.preventDefault();
										v.stopImmediatePropagation()
									} else {
										this.hide(true)
									}
								} else {
									if (w === 27) {
										this.preselect(-1);
										this.hide(true)
									} else {
										this.updateTOComplete()
									}
								}
							}
						}
					}
				}
			};
			this.data = function() {
				var v, u = "value";
				if ( typeof (r.cb.populate) === "function") {
					v = f.extend(true, {}, r.ajax.data, r.cb.populate.apply(q, []))
				} else {
					v = f.extend(true, {}, r.ajax.data);
					if (r.name && r.name.length) {
						u = r.name
					} else {
						if (q.attr("name") && q.attr("name").length) {
							u = q.attr("name")
						} else {
							if (q.attr("id") && q.attr("id").length) {
								u = q.attr("id")
							}
						}
					}
					v[u] = q.val()
				}
				return v
			};
			this.complete = function() {
				var u = q.val();
				if (r.minLength && (r.minLength > u.length)) {
					if (this.hide(true)) {
						this.preselect(-1)
					}
					return
				}
				if (r.source) {
					this.completeSource()
				} else {
					this.completeAjax()
				}
			};
			this.filterData = function(y, u) {
				var w = new RegExp((r.prefix ? "^" : "") + q.val().replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"), "i"), x = [], v;
				for ( v = 0; v < y.length; v++) {
					if (w.test(u(y[v]))) {
						x.push(y[v])
					}
				}
				return x
			};
			this.completeSource = function() {
				this.show(this.getSource(r.source), true)
			};
			this.completeAjax = function() {
				var v = this, x = q.val(), u;
				if (h && ((r.once && !f.isEmptyObject(h)) || (r.cache && ( typeof (h[x]) !== "undefined")))) {
					var w = r.once ? g(h) : g(h[x]);
					if ( typeof (r.cb.process) === "function") {
						w = r.cb.process.apply(q, [w, r.once ? "once" : "cache"])
					}
					if ( typeof (w) === "string") {
						w = v.splitData(w)
					}
					this.show(w, r.filter);
					return
				}
				u = f.extend(true, {}, r.ajax);
				u.success = function(z, A, y) {
					if (r.once) {
						h = g(z)
					} else {
						if (r.cache) {
							h[x] = g(z)
						}
					}
					if ( typeof (r.cb.process) === "function") {
						z = r.cb.process.apply(q, [z, A, y])
					}
					if ( typeof (z) === "string") {
						z = v.splitData(z)
					}
					v.show.apply(v, [z, r.filter])
				};
				u.data = this.data();
				f.ajax(u)
			};
			this.preselect = function(u) {
				this.updateToAutoHide();
				if (i === u) {
					return
				}
				this.hover(i, false);
				i = u;
				this.hover(i, true);
				if ( typeof (r.cb.preselect) === "function") {
					r.cb.preselect.apply(q, [i === -1 ? null : s[i], i])
				}
			};
			this.select = function(u, v) {
				this.stopToAutoHide();
				if (v === null) {
					v = q.val()
				} else {
					q.val(v)
				}
				this.hide();
				if ( typeof (r.cb.select) === "function") {
					r.cb.select.apply(q, [s[u], u])
				}
			};
			this.show = function(z, w) {
				var y = this, v = q.position(), x = f.browser.msie ? q.outerWidth() : q.width(), u = r.cb.cast ||
				function(A) {
					return A
				};

				s = z;
				this.hide();
				if (!z || ( typeof (z) !== "object") || !z.length) {
					return
				}
				if (( typeof (w) === "undefined" && r.filter) || w) {
					z = this.filterData(z, u)
				}
				o = f('<ul class="' + r.className + '"></ul>').css("position", "absolute").css("left", v.left + "px").css("top", (v.top + q.outerHeight()) + "px").scroll(function() {
					n = false
				});
				if (r.width === "auto") {
					o.css(f.browser.msie ? "width" : "minWidth", x + "px")
				} else {
					if (r.width === false) {
						o.css("width", x + "px").css("overflow", "hidden")
					} else {
						o.css("width", r.width).css("overflow", "hidden")
					}
				}
				i = -1;
				t = z.length;
				f.each(z, function(A, C) {
					var D = f("<li></li>"), B = f("<a></a>");
					B.click(function() {
						y.select.apply(y, [A, u(C)])
					});
					D.hover(function() {
						if (!n) {
							y.preselect(A)
						}
					});
					o.append(D.append(B.append(u(C))))
				});
				o.hover(function() {
					q.data(d + "-focus", true);
					y.stopToAutoHide()
				}, function() {
					q.data(d + "-focus", false);
					y.updateToAutoHide();
					if (!q.is(":focus")) {
						q.trigger("focusout")
					}
				});
				q.after(o);
				if (f.browser.msie) {
					f.each("min max".split(" "), function(B, A) {
						f.each("Width Height".split(" "), function(E, C) {
							var D = parseInt(o.css(A + C));
							if (!isNaN(D) && ((o[C.toLowerCase()]() < D) ^ B)) {
								o.css(C.toLowerCase(), D + "px")
							}
						})
					})
				}
				this.updateToAutoHide()
			};
			this.reverse = function(v) {
				var u = null;
				f("li", o).each(function(x, w) {
					if ((u === null) && (f(w).text() === v)) {
						u = x
					}
				});
				return u
			};
			this.hide = function(v) {
				if (o) {
					if (v) {
						var w = q.val(), u = !w.length || (r.minLength && (r.minLength > w.length)) ? null : this.reverse(w);
						if (u !== null) {
							this.select(u);
							return
						} else {
							if ( typeof (r.cb.unselect) === "function") {
								r.cb.unselect.apply(q, [])
							}
						}
					}
					this.stopToAutoHide();
					o.remove();
					o = null;
					i = -1;
					return true
				}
				return false
			};
			this.isPublic = function(u) {
				for (var v = 0; v < c.length; v++) {
					if (c[v] === u) {
						return true
					}
				}
				return false
			};
			this.process = function() {
				var w = [];
				for (var u = 0; u < arguments.length; u++) {
					w.push(arguments[u])
				}
				if (w.length && typeof (w[0]) === "string") {
					var v = w.shift();
					if (this.isPublic(v)) {
						this[v].apply(this, w)
					}
				} else {
					this.init.apply(this, w)
				}
			}
		}


		a.prototype.flushCache = function() {
			this.flush()
		};
		a.prototype.enable = function() {
			this.bind()
		};
		a.prototype.disable = function() {
			this.unbind();
			this.preselect(-1);
			this.hide()
		};
		a.prototype.trigger = function() {
			this.updateTOComplete(true)
		};
		a.prototype.display = function(h, i) {
			this.show(this.getSource(h), i)
		};
		a.prototype.close = function() {
			this.hide()
		};
		f.fn.autocomplete = function() {
			var h = arguments;
			f.each(this, function() {
				var j = f(this), i = j.data(d);
				if (!i) {
					i = new a(j);
					j.data(d, i)
				}
				i.process.apply(i, h)
			});
			return this
		}
	}(jQuery));


	
	// Handler for .ready() called.
//});

$('td', 'table').each(function(i) {
    $(this).text(i+1);
});



$('table.paginated').each(function() {
    var currentPage = 0;
    var numPerPage = 10;
    var $table = $(this);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = $('<div class="pager"></div>');
    for (var page = 0; page < numPages; page++) {
        $('<span class="page-number"></span>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            $(this).addClass('active').siblings().removeClass('active');
        }).appendTo($pager).addClass('clickable');
    }
    $pager.insertBefore($table).find('span.page-number:first').addClass('active');
});
