$(document).ready(function() {
    $(window).scroll(function() {
        if($(this).scrollTop() > 250){
            $('#goTop').stop().animate({
                top: '500px'    
                }, 500);
        }
        else{
            $('#goTop').stop().animate({
               top: '-100px'    
            }, 500);
        }
    });
    $('#goTop').click(function() {
        $('html, body').stop().animate({
           scrollTop: 0
        }, 500, function() {
           $('#goTop').stop().animate({
               top: '-100px'    
           }, 500);
        });
    });
    
});    

$(document).ready(function(){
	var History = window.History,
		State = History.getState();	
});

var current_cat_id = "";
var current_cat_childs = "";
var current_tab_id = "";
var current_attr_id= "";

if($.urlParam("current_attr")){
	current_attr_id = $.urlParam("current_attr");
}else{
	current_attr_id = "";
}
	
	
var nb_items_page = 0;
if($.urlParam("nb_items_page")){
	nb_items_page = $.urlParam("nb_items_page");
}else{
	nb_items_page = 10;
}
	
var total_page =0;
var current_page = 0;
if($.urlParam("attr_page")){
	current_page = $.urlParam("attr_page");
}else{
	current_page = 0;
}
	
var sort_nb_prods = "";
var sort_code_attr = "";

var pagination_utils = [];


$(document).keyup(function (event) {
    if(event.keyCode == 27){
        
		if (current_attr_id && current_tab_id != "#ongAll") {
			var elm = document.getElementById("close-"+current_attr_id);
				closeThisTabAttr(elm);
			
		}
    }  
    else if(event.keyCode == 70) {
		if (current_attr_id && current_tab_id != "#ongAll") {
			if($("#goto_values_attr_"+current_attr_id).hasClass("active") && $("#myModalEditValAttr").length > 0 ){
				showDialogFusionValues(current_attr_id);
			}
		}
		else if(current_tab_id == "#ongAll"){
			showDialogFusionAttributs();
		}    	
    } 
});

$('#jstree_categories')
.on('select_node.jstree', function (e, data) {
    var i, j, r = [];
    for(i = 0, j = data.selected.length; i < j; i++) {
        r.push(data.instance.get_node(data.selected[i]).id);
    }
    $('#event_result').html('Selected: ' + r.join(', '));
    
    current_cat_id = r.join(', ');
    current_page = 0;
    if(current_attr_id != ""){
    	loadAttributsCat(current_cat_id);
    }else{
    	showListeAttribut();
    }
    
})
.jstree({
    'core' : {
        'data' : {
            'url' : function (node) {
              return '/app.php/getJsonUnivers';
            },
            'dataType ' : 'json',
            'data' : function (node) {
              
              return { 'id' : node.id.trim() };
            }
        }
    },
  "plugins" : [
    "contextmenu", "dnd", "search",
    "state", "types", "wholerow"
  ]
});

$("#items_per_page").change(function(){
	nb_items_page = $("#items_per_page").val();
	current_page = 0;
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
	loadAttributsCat(current_cat_id);
});

function loadAttributsCat (cat_id) {

    var theData = {
        cat_id : cat_id,
        current_page : current_page,
        nb_items_page : nb_items_page,
        sort_nb_prods : sort_nb_prods,
        sort_code_attr : sort_code_attr 
    };
    
    $.ajax({
        type : 'POST',
        url : '/app.php/getGridAttrCat',
        data : theData,
        beforeSend : function(){
            
            $('#table_attributs').block({css: { color: '#fff', borderRadius: "15px" }, message: "<h4>Chargement...</h4>" });
        },
        success : function(data) {
        	var obj = jQuery.parseJSON(data);
        	
        	total_page = parseInt(obj.total_page);
        	
        	if(isNaN(total_page)){
        		total_page = 0;
        		$("#curr_page_attr").text(current_page);
        	}else{
        		$("#curr_page_attr").text(parseInt(current_page) + 1);
        	}
        	$("#current_cat_label").text(obj.cat_label);
        	$("#total_attributs").text(obj.total_items);
        	$("#total_page_attr").text(total_page);
            $('#table_attributs').unblock();
            $('#table_attributs').html(obj.html);
            current_cat_childs = obj.current_cat_childs;
            
            if(current_attr_id != "")
            	openFicheAttribut(current_attr_id);
        }
    });
};

function refreshAttrList(){
	loadAttributsCat(current_cat_id);
}
   
function openFicheAttribut (id_attribut, message) {
	
	current_attr_id = id_attribut;
	if($('#ficheattribut-'+id_attribut).length > 0 ){
		
		$('.embed').each(function(){
			$(this).hide();
		});
		$('.navigation_gp li').each(function(){
			$(this).removeClass("active");
		});
		$('#main-content-area').hide();		
		$('#tabattribut-'+id_attribut).addClass("active");
		$('#ficheattribut-'+id_attribut).show();
		
		current_tab_id = '#tabattribut-'+id_attribut;
		
	}else{
		var theData = {
			id_attribut : id_attribut,
			cat_id : current_cat_id,
			current_cat_childs : current_cat_childs 
		};
		
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/embed_fiche_attribut",
			data : theData,
			beforeSend : function(){
				$('#catalogue_avahis').block({css: { color: '#fff', borderRadius: "15px" }});
			},
			success : function(data) {

				$('#catalogue_avahis').unblock();
				var obj = jQuery.parseJSON(data);
				
				$('.navigation_gp li').each(function(){
					$(this).removeClass("active");
				});
				$('#main-content-area').hide();
				$('#catalogue_avahis').append('<div class="well embed" id="ficheattribut-'+id_attribut+'" style="display: block;">'+obj.html+'</div>');
				$('ul.navigation_gp').append("<li class='active tabattribut' id='tabattribut-"+id_attribut+"'><a href='#"+id_attribut+"' onClick='tabFicheAttr(this); return false;'>"+obj.nom+"</a><button class='pull-right close-tab' id='close-"+id_attribut+"' onClick='closeThisTabAttr(this); return false;'>x</button></li>");
				current_tab_id = '#tabattribut-'+id_attribut;
				History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
				window.scrollTo(0, 0);
				
				if(message != ""){
					$('#ficheattribut-'+id_attribut).prepend(message);
				}
				
				pagination_utils[id_attribut] = {
					current_page : 0,
					total_page : parseInt(obj.total_page),
					current_page_values : 0,
					total_page_values : parseInt(obj.total_page_values),
					nb_items_page_values : 20,
					nb_items_page_prods : 20 
				}
			}
		});  		
	}
}

function tabFicheAttr (elm) {
	
	var tmp = $(elm).parent().attr("id");
	
	var id_attribut = tmp.split("-");
	
	id_attribut = id_attribut[1];
	
	$('.navigation_gp li').each(function(){
		$(this).removeClass("active");
	});	
	
	$('#tabattribut-'+id_attribut).addClass("active");
	$('#main-content-area').hide();
	$('.embed').each(function(){
		$(this).hide();
	});
		
	$('#ficheattribut-'+id_attribut).show();
	current_tab_id = '#tabattribut-'+id_attribut;
	current_attr_id = id_attribut;
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
}


function showOngAll() {
	
	$('.navigation_gp li').each(function(){
		$(this).removeClass("active");
	});
			
	$('.embed').each(function(){
		$(this).hide();
	});
	
	$('#ongAll').addClass("active");
	
	$('#main-content-area').show();
	current_attr_id = "";
	current_tab_id = "#ongAll";
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page);
}
     
 
function closeThisTabAttr (elm) {

	
	var id = elm.id;
	
	var id_attribut = id.split("-");
	
	id_attribut = id_attribut[1];
	
	var tab_to_close = $("#tabattribut-"+id_attribut);
	
	var tabs = $('.tabattribut');
	
	var previous_tab = tabs.eq(tabs.index(tab_to_close) - 1);
	
	if(current_tab_id != "#tabattribut-"+id_attribut ){
		$("#ficheattribut-"+id_attribut).remove();
		$("#tabattribut-"+id_attribut).remove();		
	}
	else{
		$('.navigation_gp li').each(function(){
			$(this).removeClass("active");
		});
		
		if($(previous_tab).attr("id") == "ongAll"){
			$("#ficheattribut-"+id_attribut).remove();
			$("#tabattribut-"+id_attribut).remove();
		
			$('#main-content-area').show();
			$('#ongAll').addClass("active");
			current_attr_id = "";
			History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
			//showListeAttribut();	
		}
		else{
			previous_tab.addClass("active");
			
			var previous_id = 	$(previous_tab).attr("id");
			
			current_tab_id = "#"+previous_id;
			 
			var previous_attribut_id = previous_id.split("-");
			
			previous_attribut_id = previous_attribut_id[1];
			
			$("#ficheattribut-"+id_attribut).remove();
			$("#tabattribut-"+id_attribut).remove();
			
			$('#ficheattribut-'+previous_attribut_id).show();
			current_attr_id = previous_attribut_id; 
			History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
		}		
	}

	
}

function showListeAttribut () {

	$('.embed').each(function(){
		$(this).hide();
	});
		
	$('.navigation_gp li').each(function(){
		$(this).removeClass("active");
	});
	
	$('#ongAll').addClass("active");
	
	loadAttributsCat(current_cat_id);	
	$('#main-content-area').show();
	
	current_tab_id = '#ongAll';
	current_attr_id = "";
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
	
}

function deleteAttribut(id_attribut, label_attr) {

	
	bootbox.confirm("<h4>Etes vous sûr de vouloir supprimer l'attribut <strong style='color:red'>'"+label_attr+"'</strong>?</h4>", function(result) {
		
		if(result==true){
			var theData = {
				id_attribut : id_attribut,
				cat_id : current_cat_id, 
				current_cat_childs : current_cat_childs
			};
			
			$.ajax({
				type : "POST",
				url : "/app.php/deleteAttribut",
				data : theData,
				success : function(data) {
					$("#messagebox-categories").html(data);
					loadAttributsCat(current_cat_id);
					
					if(current_tab_id == "#tabattribut-"+id_attribut ){		
						var elm = document.getElementById("close-"+id_attribut);
						closeThisTabAttr(elm);
					}
				}
			});
		}
	});
}

function deleteSelectedAttr() {
	
	var theData = {} ;
	theData.id_attributs = [] ;
	theData.cat_id = current_cat_id;
	theData.current_cat_childs = current_cat_childs;
	var count = 0;
	var atleastOne = false;
	
	$("#table_attributs input.selection:checked").each(function(){
		var tmp = $(this).attr("id");
		tmp = tmp.split('_');
		
		if(tmp[0] == "check-attr"){
			atleastOne = true;
			theData.id_attributs.push(tmp[1]);
			count++;
		}
	});
	
	if(atleastOne){
		bootbox.confirm("<h4>Etes vous sûr de vouloir supprimer ces <strong style='color:red'>"+count+" attributs </strong> ?</h4>", function(result) {
			
			if(result===true){
				
				$.ajax({
					type : "POST",
					url : "/app.php/deleteSelectedAttr",
					data : theData,
					success : function(data) {
						$("#messagebox-categories").html(data);
						loadAttributsCat(current_cat_id);
					}
				});
			}
		});		
	}
	else{
		bootbox.alert("<h4>Vous n'avez sélectionné aucun attribut !</h4>");
	}
}


function showDialogFusionAttributs() {
	
	var theData = {} ;
	theData.id_attributs = [] ;
	theData.cat_id = current_cat_id;
	theData.current_cat_childs = current_cat_childs;
	var count = 0;
	var atleastOne = false;
	
	$("#table_attributs input.selection:checked").each(function(){
		var tmp = $(this).attr("id");
		tmp = tmp.split('_');
		
		if(tmp[0] == "check-attr"){
			atleastOne = true;
			theData.id_attributs.push(tmp[1]);
			count++;
		}
	});
	
	if(atleastOne){
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/getDialogFusionAttributs",
			data : theData,
			success : function(data) {
				
				if( $('#myModalFusionAttr').length == 0){
					$('#content').append(data);	
				}
				else{
					$('#myModalFusionAttr').remove();
					$('#content').append(data);
				}
				$('#myModalFusionAttr').modal({
					 keyboard: true
				});
			}
		});
	}
	else{
		bootbox.alert("<h4>Vous n'avez sélectionné aucun attribut !</h4>");
	}
	
	return false;
}

function showDialogFusionValues(id_attribut) {

	var theData = {} ;
	theData.id_attribut = id_attribut;
	theData.values = [] ;
	var count = 0;
	var atleastOne = false;
	
	$("#values_list_"+id_attribut+" input.selection_value:checked").each(function(){
		
		var value = $(this).parent().parent().parent().find(".attr_value ").html();
		atleastOne = true;
		theData.values.push(value);
		count++;
		
	});
	
	if(atleastOne){
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/getDialogFusionValues",
			data : theData,
			success : function(data) {
				if( $('#myModalFusionvalues').length == 0){
					$('#content').append(data);	
				}
				else{
					$('#myModalFusionvalues').remove();
					$('#content').append(data);
				}
				$('#myModalFusionvalues').modal({
					 keyboard: true
				});
				
			}
		});
	}
	else{
		bootbox.alert("<h4>Vous n'avez sélectionné aucune valeur !</h4>");
	}
	
	return false;	
}

function validFusion() {
	
	if($("#label_attr_fusion").val().trim() != ""){
		var theData = {} ;
		theData.id_attributs = [] ;
		theData.cat_id = current_cat_id;
		theData.current_cat_childs = current_cat_childs;
		theData.label_attr_fusion = $("#label_attr_fusion").val().trim();
		theData.dominant_id_attribut = $("#dominant_id_attribut").val();
		
		var count = 0;
		var atleastOne = false;
		
		$("#table_attributs input.selection:checked").each(function(){
			var tmp = $(this).attr("id");
			tmp = tmp.split('_');
			
			if(tmp[0] == "check-attr"){
				atleastOne = true;
				theData.id_attributs.push(tmp[1]);
				count++;
			}
		});	
		
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validFusionAttributs",
			data : theData,
			success : function(data) {
				
				loadAttributsCat(current_cat_id); 
				$("#messagebox-categories").html(data);
			}
		});	
	}
	else{
		bootbox.alert("<h4>Entrez un nouveau non pour l'attribut fusionné !</h4>");
	}
	
	return false;
}

function validFusionValues(id_attribut) {

	if($("#label_attr_value_fusion").val().trim() != ""){
		var theData = {} ;
		theData.values = [] ;
		theData.id_attribut = id_attribut;
		theData.cat_id = current_cat_id;
		theData.current_cat_childs = current_cat_childs ;
		theData.label_attr_value_fusion = $("#label_attr_value_fusion").val().trim();
		var count = 0;
		var atleastOne = false;
		
		$("#values_list_"+id_attribut+" input.selection_value:checked").each(function(){
			
			var value = $(this).parent().parent().parent().find(".attr_value ").html();
			atleastOne = true;
			theData.values.push(value);
			count++;
			
		});
		
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/validFusionValues",
			data : theData,
			success : function(data) {
				changePageValuesAttr(id_attribut); 
				$("#messagebox-attribut_"+id_attribut).html(data);
			}
		});	
	}
	else{
		bootbox.alert("<h4>Entrez un nouveau non pour les valeurs fusionnées !</h4>");
	}

	return false;	
}

function setIsFilterAttrCat(elm, cat_id){
	
	var id = elm.id;
	id= id.split("_");
	
	var id_attribut = id[1];
	
	var theData = {
		id_attribut : id_attribut,
		cat_id : cat_id,
		value : $(elm).prop("checked") ? 1 : 0
	};

  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/setAttributIsFilter",
		data : theData,
		success : function(data) {
			$("#messagebox-categories").html(data);
		}
	});			
}

function getDialogValeurAttribut(elm, id_attribut) {
	
	var value = $(elm).parent().parent().find(".attr_value ").html();
	
	var theData = {
		value : value,
		id_attribut : id_attribut
	}
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getDialogValeurAttribut",
		data : theData,
		success : function(data) {
			
			if( $('#myModalEditValAttr').length == 0){
				$('#myModalEditValAttr').remove();
				$('#content').append(data);	
			}
			else{
				$('#myModalEditValAttr').remove();
				$('#content').append(data);
			}
			$('#myModalEditValAttr').modal({
				 keyboard: true
			});			
		}
	});	
}

function editValueAttr() {
	
	var old_value = $("#old_label_attr_value").val();
	var value = $("#label_attr_value").val();
	var id_attribut = $("#input_id_attribut").val();
	
	if(value.trim() != ""){
		var theData = {
			value : value,
			old_value : old_value,
			id_attribut : id_attribut
		}
		
	  	jQuery.ajax({
			type : 'POST',
			url : "/app.php/editAttributValue",
			data : theData,
			success : function(data) {
				changePageValuesAttr(id_attribut); 
				$("#messagebox-attribut_"+id_attribut).html(data);
			}
		});			
	}
	else{
		bootbox.alert("<h4>Veuillez entrer une nouvelle valeur d'attribut ! </h4>")
	}
}

function saveEditLabelAttribut (id_attribut) {
	
	var label_attr = $("#label_attr_"+id_attribut).val();
	
	var theData = {
		label_attr : label_attr,
		id_attribut : id_attribut
	}
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/saveEditLabelAttribut",
		data : theData,
		success : function(data) {
			$("#messagebox-attribut_"+id_attribut).html(data);
		}
	});		
}

/// PAGINATION PRODUITS CONCERNES PAR ATTRIBUT

function prevPageProdAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page--; 
	
	if(pagination_utils[id_attribut].current_page < 0){
		pagination_utils[id_attribut].current_page = 0;
	}
	else{
		changePageProdAttr(id_attribut);	
	}
	
}

function nextPageProdAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page++;
	
	if(pagination_utils[id_attribut].current_page >= pagination_utils[id_attribut].total_page){
		pagination_utils[id_attribut].current_page = pagination_utils[id_attribut].total_page -1;
	}
		
	changePageProdAttr(id_attribut); 
}

function firstPageProdAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page = 0;
	changePageProdAttr(id_attribut); 
}

function lastPageProdAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page = pagination_utils[id_attribut].total_page -1;
	
	changePageProdAttr(id_attribut); 
}

function setItemPerPageProds(id_attribut, elm) {
	
	pagination_utils[id_attribut].nb_items_page_prods = $(elm).val();
	pagination_utils[id_attribut].current_page = 0;
	changePageProdAttr(id_attribut);
}

function changePageProdAttr(id_attribut) {
	
	$("#curr_page_"+id_attribut).text((pagination_utils[id_attribut].current_page) + 1);
	
	var theData = {
		id_attribut : id_attribut,
		curr_page : pagination_utils[id_attribut].current_page,
		current_cat_id : current_cat_id,
		current_cat_childs : current_cat_childs,
		nb_items_page : pagination_utils[id_attribut].nb_items_page_prods,
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getCatalogProductAttribut",
		data : theData,
		beforeSend : function(){
			$('#prods_avahisGP_'+id_attribut).block({css: { color: '#fff', borderRadius: "15px" }});
		},
		success : function(data) {
			var obj = jQuery.parseJSON(data);
			
			$('#prods_avahisGP_'+id_attribut).unblock();
			
			$('#prods_avahisGP_'+id_attribut).html(obj.html);
			$('#total_page_prods_'+id_attribut).html(obj.total_pages);
			pagination_utils[id_attribut].total_page = obj.total_pages;
		}
	}); 	
}

//PAGINATION VALEURS DE L ATTRIBUT


function prevPageValuesAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page_values--; 
	
	if(pagination_utils[id_attribut].current_page_values < 0)
		pagination_utils[id_attribut].current_page_values = 0;
	changePageValuesAttr(id_attribut);
}

function nextPageValuesAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page_values++;
	
	if(pagination_utils[id_attribut].current_page_values >= pagination_utils[id_attribut].total_page_values){
		pagination_utils[id_attribut].current_page_values = pagination_utils[id_attribut].total_page_values -1;
	}
		
	changePageValuesAttr(id_attribut); 
}

function firstPageValuesAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page_values = 0;
	changePageValuesAttr(id_attribut); 
}

function lastPageValuesAttr (id_attribut) {
	
	pagination_utils[id_attribut].current_page_values = pagination_utils[id_attribut].total_page_values -1;
	
	changePageValuesAttr(id_attribut); 
}

function setItemPerPage(id_attribut, elm) {
	
	pagination_utils[id_attribut].nb_items_page_values = $(elm).val();
	pagination_utils[id_attribut].current_page_values = 0;
	changePageValuesAttr(id_attribut);
}


function changePageValuesAttr(id_attribut) {
	
	$("#curr_page_values_"+id_attribut).text((pagination_utils[id_attribut].current_page_values) + 1);
	
	var theData = {
		id_attribut : id_attribut,
		curr_page : pagination_utils[id_attribut].current_page_values,
		sort_nb_prods : pagination_utils[id_attribut].sort_nb_prods,
		nb_items_page : pagination_utils[id_attribut].nb_items_page_values,
		cat_id : current_cat_id,
		current_cat_childs : current_cat_childs,
	};
	
  	jQuery.ajax({
		type : 'POST',
		url : "/app.php/getTableAttributValues",
		data : theData,
		beforeSend : function(){
			$('#values_list_'+id_attribut).block({css: { color: '#fff', borderRadius: "15px" }});
		},
		success : function(data) {
			
			var obj = jQuery.parseJSON(data);
			
			$('#values_list_'+id_attribut).unblock();
			
			$('#values_list_'+id_attribut).html(obj.html);
			$('#total_page_'+id_attribut).html(obj.total_pages);
			pagination_utils[id_attribut].total_page_values = obj.total_pages;
			
		}
	}); 	
}

//PAGINATION PRINCIPALE DES ATTRIBUTS

function prevPageAttr () {
	
	current_page--; 
	if(current_page < 0)
		current_page = 0;
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
	loadAttributsCat(current_cat_id);
}

function nextPageAttr () {
	
	current_page++;
	if(current_page >= total_page){
		current_page = total_page -1;
	}
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
	loadAttributsCat(current_cat_id); 
}

function firstPageAttr () {
	
	current_page = 0;
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
	loadAttributsCat(current_cat_id); 
	
}

function lastPageAttr () {
	
	current_page = total_page -1;
	History.pushState('ajax', "Soukflux : Attributs", "/app.php/catalogue/attributs?nb_items_page="+nb_items_page+"&attr_page="+current_page+"&current_attr="+current_attr_id);
	loadAttributsCat(current_cat_id); 
	
}

function sortNbProds (elm) {
	
	if($(elm).hasClass("cmpAsc")){
		sort_nb_prods = "cmpDesc";
		
	}
	else if($(elm).hasClass("cmpDesc")){
		sort_nb_prods = "cmpAsc";
	}
	else{
		sort_nb_prods = "cmpDesc";
	}
	
	sort_code_attr = "";
	
	loadAttributsCat(current_cat_id);
}

function sortCodeAttr (elm) {
	
	if($(elm).hasClass("asc")){
		sort_code_attr = "desc";
		
	}
	else if($(elm).hasClass("desc")){
		sort_code_attr = "asc";
	}
	else{
		sort_code_attr = "asc";
	}
	sort_nb_prods = "";
	loadAttributsCat(current_cat_id);
}

function sortNbProdsValues (elm, id_attribut) {
	
	if($(elm).hasClass("cmpAsc")){
		 
		pagination_utils[id_attribut].sort_nb_prods = "cmpDesc";
		
	}
	else if($(elm).hasClass("cmpDesc")){
		pagination_utils[id_attribut].sort_nb_prods = "cmpAsc";
	}
	else{
		pagination_utils[id_attribut].sort_nb_prods = "cmpDesc";
	}
	changePageValuesAttr(id_attribut);
}

function highlightSelection (elm) {
	
	if($(elm).prop("checked")){
		$(elm).parent().parent().parent().addClass("selected");
	}else{
		$(elm).parent().parent().parent().removeClass("selected");
	}
}

