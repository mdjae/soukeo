/*
Script: ID Generator.js
	generate a random ID

License:
	MIT-style license.

Author:
	Copyright (c) 2008 Chris Esler, <http://www.chrisesler.com/mootools>

*/

var idGenerator = new Class({
	// most of this is from mooglets
	letters: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
	generateID: function(num){
		var instance = '';
		for (i=0; i < num; i++) {
			var numI = this.getRandomNum();
			while (this.checkPunc(numI)) { numI = this.getRandomNum(); }
			instance += String.fromCharCode(numI);
		}
		return this.getOneNum(true)+instance;
	},

	getOneNum: function(alpha){
		var rndNum = Math.random();
		rndNum = parseInt(rndNum * 10);
		var letters = this.letters.split('');
		if (alpha) return letters[rndNum];
		else return rndNum;
	},

	getRandomNum: function(){
		var rndNum = Math.random();
		rndNum = parseInt(rndNum * 1000);
		rndNum = (rndNum % 94) + 33;
		return rndNum;
	},
	

	checkPunc: function(num){
		if (((num >=33) && (num <=47)) || ((num >=58) && (num <=64)) || ((num >=91) && (num <=96)) || ((num >=123) && (num <=126))) return true;
		return false;
	}
});
