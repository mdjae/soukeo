﻿<?php 
// Menu 
$MENU_ITEMS_str = "<ul id=\"navmenu-h\" class=\"mainmenu\"> \n";

// Favoris / Quick links
// -- Quick links de l'utilisateur connecté 
$query = "SELECT distinct q.domain, q.catagory, s.page_path  
			FROM c_user_qlink q, c_group_security s 
			WHERE s.catagory = q.catagory
   				AND s.domain = q.domain
				AND s.function = '' AND s.special = ''
				AND s.group_id IN " . SystemAccess::userGroupToSql() . " 
			ORDER BY q.adddate DESC  
            LIMIT 0,3";
			//AND s.page_path = q.page_path

$res_qlink = dbQueryAll($query);
						
if (count($res_qlink)!=0)
{
	$MENU_ITEMS_str .= "<li>
							<a href=\"#\">
								<img src='". $root_path ."skins/common/images/favori.gif' align='absmiddle' border='0'/>&nbsp;&nbsp;". ("Favoris") ."
							</a> \n
                            <ul> \n";
                                
	for($i = 0; $i < count($res_qlink); $i++)
  	{
  		// Texte
  		//$text = "menu_" . $res_qlink[$i]['domain'];
  		$text2 = "link_" . $res_qlink[$i]['catagory'];
  				      
  	    if ($i!=0)
  	    {
        	$MENU_ITEMS_str .= " ";
  	    }       
           
  	    $MENU_ITEMS_str .= "<li><a href=\"". $root_path.$res_qlink[$i]['page_path'] ."\">". $$text2 ."</a></li> \n";
	}
  					  
  	$MENU_ITEMS_str .= " </ul> \n";
  	$MENU_ITEMS_str .= "</li> \n";
}
						
// Liste des domaines et catégories accessibles pour l'utilisateur
$query = "SELECT distinct s.seq, s.domain, s.catagory, s.page_path  
			FROM c_group_security s
			WHERE s.catagory NOT IN ('login','logout')
			AND s.function = '' AND s.special = ''
			AND s.group_id IN " . SystemAccess::userGroupToSql() . " 
		ORDER BY s.seq";

/* Domains order :
	- common
	- services
	- sales
	- hr
	- invoicing
	- reporting
	- interfaces
	- admin
*/

// Boucle sur les domaines
$res_domain = dbQueryAll($query);
$prev_domain = "";
						
for($i = 0; $i < count($res_domain); $i++)
{
	// Texte
	$text = "menu_" . $res_domain[$i]['domain'];
	$text2 = "link_" . $res_domain[$i]['catagory'];
					
	if ($res_domain[$i]['domain'] != $prev_domain)
	{            
    	if ($prev_domain!="")
    	{
        	$MENU_ITEMS_str .= "</ul></li> \n";
	    }
	               
		$MENU_ITEMS_str .= "<li>
								<a href=\"". $root_path . SystemAccess::getPagePathDomCat($res_domain[$i]['domain'], $res_domain[$i]['catagory']) ."\">". $$text ."</a> \n
                         		<ul> \n";
                  
        $prev_domain = $res_domain[$i]['domain'];
	}
	else
	{
		if ($prev_domain!="")
		{
			$MENU_ITEMS_str .= " ";
	    }
    }
              
    $MENU_ITEMS_str .= "<li><a href=\"". $root_path . SystemAccess::getPagePathDomCat($res_domain[$i]['domain'], $res_domain[$i]['catagory']) ."\">". $$text2 ."</a></li> \n";          
}
            
// fin de la boucle, on terme la chaîne 
$MENU_ITEMS_str .= "</ul></li>\n 
                            </ul> \n";
            
echo $MENU_ITEMS_str;
?>


