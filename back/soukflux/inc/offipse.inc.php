<?php
/********************************************/
/*** CONFIGURATION et CONNEXION A LA BASE ***/
/********************************************/
// GLOBAL Init ($root_path is defined in each page)

// GLOBAL Init ($root_path is defined in each page)
if (isset($main_path)) {
	$path =  $main_path . "/".$root_path ;
}else {
    $path = $root_path ;
	if (session_id() == "") session_start();
}


require_once $path .   "inc/libs/error.inc.php";
require_once $path .   'config/config.inc.php';
if (isset($APACHE_VHOST_CONFIG) && $APACHE_VHOST_CONFIG)
{
    $config_file = $root_path . '/config/' . $_SERVER["SERVER_NAME"] . '.config.inc.php';
    if (file_exists($config_file)) require_once $config_file;
    else die(("Impossible de charge le fichier de configuration " . $config_file));
}
require_once $path .   'inc/version.php';
require_once $path .   'inc/libs/db.mysql.inc.php';
require_once $path .   'thirdparty/swiftmail/lib/swift_required.php';
require_once $path .   'thirdparty/simplehtmldom_1_5/simple_html_dom.php';
//$db_conn = dbConnect();

//date_default_timezone_set('UTC');
date_default_timezone_set("Indian/Reunion");

// PATHS

$inc_path           = $path.'inc/';
$class_path         = $inc_path.'class/';
$thirdparty_path    = $path.'thirdparty/';
$skin_path          = '/skins/common/';
$corp_path          = $path.'skins/'.$CORPORATE_PORTAL.'/';

//(isset($real_path_pop) ? $real_path_pop:$root_path  )  .'
/**********************************/
/*** AUTOLOADER DES CLASSES PHP ***/
/**********************************/
function my_autoload($class_name)
{
    global $root_path, $inc_path, $thirdparty_path, $class_path;

    if (strpos($class_name,'MyCustom') !== false)
    {
        require_once $class_path . 'mycustom/' . $class_name . '.class.php';
    }
	else if (strpos($class_name,"PHPExcel") !== false)
    {
        require_once $thirdparty_path . 'Classes/PHPExcel.php';
    }
    else if (strpos($class_name,'ReportGrid') !== false)
    {
        require_once $class_path . 'report_grids/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Grid') !== false)
    {
        require_once $inc_path . 'class/grids/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Form') !== false)
    {
        require_once $inc_path . 'class/forms/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Batch') !== false)
    {
        require_once $class_path . 'batchs/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Report') !== false)
    {
        require_once $class_path . 'reports/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Filter') !== false)
    {
        require_once $class_path . 'filters/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Chart') !== false)
    {
        require_once $thirdparty_path . 'pChart/pChart/pData.class';
        require_once $thirdparty_path . 'pChart/pChart/pChart.class';
        require_once $thirdparty_path . 'pChart/pChart/pCache.class';
        require_once $class_path . 'charts/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'System') !== false)
    {
        require_once $class_path . 'system/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Widget') !== false)
    {
        require_once $class_path . 'widget/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'UI') !== false)
    {
        require_once $class_path . 'ui/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Print') !== false)
    {
        require_once $class_path . 'print/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Business') !== false)
    {
        require_once $class_path . 'business/' . $class_name . '.class.php';
    }
    else if (strpos($class_name,'Business') !== false)
    {
        require_once $class_path . 'business/' . $class_name . '.class.php';
    }
	else if (strpos($class_name,'Manager') !== false)
    {
        require_once $class_path . 'managers/' . $class_name . '.class.php';
    }
    else if ($class_name == "FPDF")
    {
        require_once $thirdparty_path . 'fpdf/fpdf.php';
    }
    else if ($class_name == "FPDF_TPL")
    {
        require_once $thirdparty_path . 'fpdi/fpdf_tpl.php';
    }
    else if ($class_name == "FPDI")
    {
        require_once $thirdparty_path . 'fpdi/fpdi.php';
    }
    else if ($class_name == "fpdi_pdf_parser")
    {
        require_once $thirdparty_path . 'fpdi/fpdi_pdf_parser.php';
    }
	else if ($class_name == "Logger")
    {
        require_once $thirdparty_path . 'log4php/Logger.php';
    }
    else
    {
        // Rien
    }
}

spl_autoload_register("my_autoload");

/************************************/
/*** INITIALISATION DE LA SESSION ***/
/************************************/
//ini_set('session.gc_probability', 50);
/*
ini_set('session.save_handler', 'user');

session_set_save_handler(array('SystemSession', 'open'),
                         array('SystemSession', 'close'),
                         array('SystemSession', 'read'),
                         array('SystemSession', 'write'),
                         array('SystemSession', 'destroy'),
                         array('SystemSession', 'gc')
                         );*/
//if (session_id() == "") session_start();

/****************************/
/*** GESTION DE LA LANGUE ***/
/****************************/
SystemLang::setLang();

setlocale(LC_ALL,  $_SESSION['lang']);
putenv('LC_ALL='   . $_SESSION['lang']);
//putenv('LANG='     . $_SESSION['lang']);
//putenv('LANGUAGE=' . $_SESSION['lang']);

$domain = "messages";

bindtextdomain($domain,$inc_path.'lang');
bind_textdomain_codeset($domain, "UTF-8");
textdomain($domain);

/***********************/
/*** AUTRES INCLUDES ***/
/***********************/
require_once $path .   'inc/libs/global.inc.php';
require_once $path .   'inc/libs/box.inc.php';
?>
