﻿<?php
setlocale(LC_ALL, 'fr_FR.UTF-8');

/**
 * Ce fichier sert de routeur du framework
 * en fonction de l'url envoyee, on va rediriger vers la bonne action
 */
$root_path = '';

require_once $root_path . "inc/offipse.inc.php";
require __DIR__ . '/vendor/autoload.php';

$test = true;

$mt1 = microTime();

$_SESSION['last_seen'] = $_SERVER['REQUEST_URI'];
$title = _("Soukeo Flux");
if (PHP_SAPI === 'cli') {
	$_SESSION['isauth'] = true;
	$_SERVER['PATH_INFO'] = "/save";
}

if ($_SESSION['isauth']) {

	if (PHP_SAPI === 'cli') {

		$user = BusinessUser::getClassUser('tpo');
	} else {
		$user = BusinessUser::getClassUser($_SESSION['smartlogin']);
		
		//var_dump($user->getUserId());
	}

	// ------- Dispatch switch Admin -----
	$view  = new BusinessView();

	$skf   = new BusinessSoukeoModel();
	
	$commentManager 			= new ManagerComment();
	$annonceReceptionManager 	= new ManagerAnnonceReception();
	$vendorManager 				= new ManagerVendor();
	$factureManager 			= new ManagerFacture();
	$factureAddressManager 		= new ManagerFactureAddress();
	$factureComManager 			= new ManagerFactureCom();
	$factureComLineManager 		= new ManagerFactureComLine();
    $abonnementManager          = new ManagerAbonnement();
    $groupManager               = new ManagerGroup();
    $categorieManager           = new ManagerCategorie();
    $ticketManager              = new ManagerTicket();
    $ticketStateManager         = new ManagerTicketState();
    $ticketPriorityManager      = new ManagerTicketPriority();
    $functionalityManager       = new ManagerDashboardFunctionality();
	$groupFunctionalityManager  = new ManagerGroupDashboardFunctionality();
    $entityCrmManager           = new ManagerEntityCrm();
    $entityTypeManager          = new ManagerEntityType();
    $customerManager            = new ManagerCustomer();
    $ticketUserManager          = new ManagerTicketUser();
    $clientTypeManager          = new ManagerClientType();
    $customerStatutManager      = new ManagerCustomerStatut();
    $vendorStatutManager        = new ManagerVendorStatut();
    $creditPackManager          = new ManagerCreditsPack();
    $creditHistoryManager       = new ManagerCreditsHistory();
    $factureCreditManager       = new ManagerFactureCredit();
    $factureCreditLineManager   = new ManagerFactureCreditLine();
    $paymentTypeManager         = new ManagerPaymentType();
    $vendorCategorieManager     = new ManagerVendorCategorie();
    $ticketHistoryManager       = new ManagerTicketHistory();
    $userManager                = new ManagerAppUser();
    $catalogProductManager      = new ManagerCatalogProduct();
    $relanceManager             = new ManagerRelance();
    $livraisonTypeManager       = new ManagerLivraisonType();
    $attributManager            = new ManagerAttribut();
    $productAttributManager     = new ManagerProductAttribut();
    $attributCategoryManager    = new ManagerAttributCategory();
	//	$data = new BusinessMpfModel();
	
	// break;
	if ($user -> getAdmin() === true) {
	
		if ($_SERVER['ORIG_PATH_INFO']) 
			$_SERVER['PATH_INFO'] = $_SERVER['ORIG_PATH_INFO'];
		
		if ($_SERVER['PATH_INFO'] == "")
			$_SERVER['PATH_INFO'] = "/o_admin";
	//	var_dump($_SERVER['PATH_INFO']);
		switch ($_SERVER['PATH_INFO']) {
			
	
			case '/loadshowdialog' :
							$dispatch = true ;	
				ob_start();
				include ("./".$_POST['url']);
				$out = ob_get_contents();
				ob_end_clean(); 
				$view -> assign("out", $out);
				$template = "rendered";
			break;
				
			case '/common/pwd/' :
				include ("./common/pwd/index.php");
				$view -> assign("out", $out);
				$template = "rendered";
				
			break;
				
			case '/o_admin/groups/' :
				include ("./o_admin/groups/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/groups/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;

			case '/o_admin/system/' :
				include ("./o_admin/system/index.php");
				$view -> assign("out", $out);
				$template = "rendered";
			break;

			case '/o_admin' :
				include ("./o_admin/index.php");
				//$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

				break;

			case '/o_admin/batch/' :
				include ("./o_admin/batch/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/batch/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

				break;

			case '/o_admin/users/' :
				include ("./o_admin/users/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

				//Col Right
				ob_start();
				include ("./o_admin/users/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case "/unlockUsers" :
				$dispatch = true;
				ob_start();
				include ("./o_admin/users/popup_unlock/index.php");
				$popup_unlock = ob_get_contents();
				ob_end_clean();
				$out = $popup_unlock ;
			break;

			case    "/catalogue/categorie" :
				$ACCESS_key = "catalogue.categorie..";

				$template = "listingAvahis";
				//CATEGORIES AVAHIS
				$data = $skf->getSsCatByParentId();
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);

			break;
			
			//Permet d'afficher l'arbre de catégorie déroulant
			//Chargeant les sous catégories en AJAX
			case '/showcat' : 
				$dispatch = true ;
				$data = ($skf->getSsCatByParentId($_POST['catid']));
				$ct = $_POST['level'] + 1;
				$out = $view->showcat($data, $ct);
				
			break;
			
			//Permet d'afficher l'arbre de catégorie déroulant
			//Chargeant les sous catégories en AJAX
			case '/showcatGP' : 
				$dispatch = true ;
				$data = ($skf->getSsCatByParentId($_POST['catid']));
				$ct = $_POST['level']+1;
				$out = $view->showcatGP($data, $ct);
				
			break;
			
			//Permet d'afficher l'arbre des catégories avec les checkboxess dans les popup
			//Les sous catégories sont rechargés ici en Ajax
			case '/showtreecheckbox' : 
				$dispatch = true ;
				$data = ($skf->getSsCatByParentId($_POST['catid']));
				$ct = $_POST['level']+1;
				$out = $view->showTreeCheckbox($data, false , $ct );
				
			break;
			
			
			//Route permettant juste de faire appel à la fonction du model 
			//getCatIdTree permettant d'obtenir la parenté entiere d'une catégorie sous forme de
			//chaine idCatGrandPere - idCatPere - idCatFils
			case '/getidcattree':
				$dispatch = true;
				if(isset($_POST['cat_id_assoc'])){
					$out = $skf -> getCatIdTree($_POST['cat_id_assoc']);
				}
			break;
				
				
			//association à une catégorie pour UN produit lorsqu'on est sur le pop up d'ajout
			case '/assoccategprod':
				$dispatch = true;
				
				//Cette page est appellée deux fois mais on n'insert que si on a reçu en POST
				//insert
				if($_POST['insert'] == true){
					//Si aucune catégorie n'est associée à ce moment la ...
					if(!$skf -> isCatAssoc($_POST['id_vendor'], $_POST['cat_ecom'])){
						//On profite de cette ajout produit pour aussi associer cet categ ecom à une categ Avahis
						$skf -> addAssocCatVendor($_POST['id_vendor'], $_POST['cat_ecom'], end($_POST['cat_id']));
						$data['catIdTree'] = $skf -> getCatIdTree(end($_POST['cat_id']));
						$data['newcat'] ="newcat";
						$data['newcat_id'] = end($_POST['cat_id']);
						$out = json_encode($data);
					}
					else{
						$data['catIdTree'] = $skf -> getCatIdTree(end($_POST['cat_id']));
						$data['newcat_id'] = end($_POST['cat_id']);
						$out = json_encode($data);
					}
				//Cas normal permettant d'envoye l'ID de la catégorie associée
				//Ainsi qu'une chaine de type Root > Cat > sCat etc à AJAX sous forme JSON
				}else{
					$tmp = "";
					foreach ($_POST['cat_id'] as $id) {
						$cat = $skf -> getCatbyId($id);
						
						$tmp .= $cat['cat_label']. " > ";
					}
					//Envoi de données en JSON
					$arr = array('cat_id' => end($_POST['cat_id']), 
								 'cat_label' => substr($tmp, 0, strlen($tmp)-2));
					$out = json_encode($arr);
				}
				
			break;
			
			
			//appelé en AJAX pour l'association entre catégorie ecommercant
			case '/assoccateg':
				$dispatch = true;
				

				//Cette page est appellée deux fois mais on n'insert que si on a reçu en POST
				//insert
				if($_POST['insert'] == true){
					// CAS DES GROSSISTES //////////////////////////////////////////////////////////////
					if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
						$db = new BusinessSoukeoGrossisteModel();
						
						$db -> addAssocCatVendor($_POST['id_vendor'], $_POST['cat_ecom'], end($_POST['cat_id']));
						//Pour la navigation dans cette catégorie
						$_SESSION['gr']['cat_id_assoc'] = end($_POST['cat_id']);
						$_SESSION['gr']['correspondance'] = $skf ->getStrCateg(end($_POST['cat_id']));
						
						//On renvoie la chaine d'id catégorie associée pour le lien (Voir)
						$out = $skf -> getCatIdTree(end($_POST['cat_id']));
					}
					// CAS DES ECOMMERCANTS ///////////////////////////////////////////////////////////
					else{
						$skf -> addAssocCatVendor($_POST['id_vendor'], $_POST['cat_ecom'], end($_POST['cat_id']));
						//On en profite pour mettre la chaine correspondante a la catégorie associée directement en Session
						//Pour la navigation dans cette catégorie
						$_SESSION['ecom']['cat_id_assoc'] = end($_POST['cat_id']);
						$_SESSION['ecom']['correspondance'] = $skf ->getStrCateg(end($_POST['cat_id']));	
						
						//On renvoie la chaine d'id catégorie associée pour le lien (Voir)
						$out = $skf -> getCatIdTree(end($_POST['cat_id']));
					}
					
					
				//Cas normal permettant d'envoyer l'ID de la catégorie associée
				//Ainsi qu'une chaine de type Root > Cat > sCat etc à AJAX sous forme JSON		
				}else{
					$tmp = "";
					foreach ($_POST['cat_id'] as $id) {
						$cat = $skf -> getCatbyId($id);
						
						$tmp .= $cat['cat_label']. " > ";
					}
					$arr = array('cat_id' => end($_POST['cat_id']), 
								 'cat_label' => substr($tmp, 0, strlen($tmp)-2));
					$out = json_encode($arr);
				}
			break;
			
			
			//Appellé en AJAX permet de récupérer les informations permettant d'afficher l'entete pour l'ecommercant
			//Vérification si la catégorie est associée et mise en session de la categ associée 
			case '/showentetecommercant' :
				  
				$dispatch = true ;

				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					//Vide la session
					unset($_SESSION['gr']['cat_id_assoc']);
					unset($_SESSION['gr']['correspondance']);
					unset($_SESSION['gr']['offset']);
				
					$db = new BusinessSoukeoGrossisteModel();
					
					$cat_id = $db -> isCatAssoc($_POST['id'], $_POST['category']);
				}
				else{
					//Vide la session
					unset($_SESSION['ecom']['cat_id_assoc']);
					unset($_SESSION['ecom']['correspondance']);
					unset($_SESSION['ecom']['offset']);
					//Si il y a association on a l'id sinon on obtient false
					$cat_id = $skf -> isCatAssoc ($_POST['id'], $_POST['category']);
				
				}
				
				
				if($cat_id != false && $skf -> catExists($cat_id)){
					
					if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
						//Récupération de la correspondance Avahis ou forme de chaine 
						$correspondance = $skf -> getStrCateg($cat_id);
						//Mise en session variables utiles
						$_SESSION['gr']['cat_id_assoc']= $cat_id;
						$_SESSION['gr']['correspondance']= $correspondance;
					}
					else{
						//Récupération de la correspondance Avahis ou forme de chaine 
						$correspondance = $skf -> getStrCateg($cat_id);
						//Mise en session variables utiles
						$_SESSION['ecom']['cat_id_assoc']= $cat_id;
						$_SESSION['ecom']['correspondance']= $correspondance;
					}
				} 
				//Appel de la vue
				$out = $view->showEnteteEcommercant($_POST['category'], $_POST['t'], $_POST['id'], $correspondance, $cat_id);
			break;
				
			
			//route permettant de mettre en session des listes de produits utiles et des comptes de produits	
			case '/shownavigation':
				$dispatch = true;
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					
					$dbClass = "BusinessSoukeoGrossisteModel";
					if(is_string($dbClass)) $db = new $dbClass();
					
					if(isset($db)){
						$listProdAssoc = $db -> getAllAssocProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);				
						$listProdRefused = $db -> getAllRefusedProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);	
					}
					
					$_SESSION['gr']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['gr']['listProdRefused'] = $listProdRefused ;
					
					$nbAssoc = count($_SESSION['gr']['listProdAssoc']) ;
					$nbRefused = count($_SESSION['gr']['listProdRefused']); 
					$nbSelected = count($_SESSION['gr']['selectedProds']); 
					
					$_SESSION['gr']['nbAssoc']=$nbAssoc;
					$_SESSION['gr']['nbRefused']=$nbAssoc;
					$_SESSION['gr']['nbSelected']=$nbSelected;
					
					$out = $view -> showNavigationGrossiste($nbAssoc, $nbRefused, $nbSelected);
				}
				
				/*
				 * cas pour l'écran gestion/produit a vérifier. doit chargé le nombre de produit supprimé le nombre de produit
				 * sélectionner
				 */ 
				elseif ($_SESSION['typeVendor'] == "avahisGP" && $GLOBALS['typeVendor'] == "avahisGP") {
						
					//if($_SESSION['gr']['tech']=="Carshop") $dbClass = "BusinessSoukeoGrossisteModel";
					if(is_string($dbClass)) $db = new $dbClass();
					
					if(isset($db)){
						$listProdAssoc = $db -> getAllAssocProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);				
						$listProdRefused = $db -> getAllRefusedProds($_SESSION['gr']['vendor'], $_SESSION['gr']['cat']);	
					}
					
					$_SESSION['gr']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['gr']['listProdRefused'] = $listProdRefused ;
					
					$nbAssoc = count($_SESSION['gr']['listProdAssoc']) ;
					$nbRefused = count($_SESSION['gr']['listProdRefused']); 
					$nbSelected = count($_SESSION['gr']['selectedProds']); 
					
					$_SESSION['gr']['nbAssoc']=$nbAssoc;
					$_SESSION['gr']['nbRefused']=$nbAssoc;
					$_SESSION['gr']['nbSelected']=$nbSelected;
					
					$out = $view -> showNavigationAvahis(78, 78, $nbSelected);	
					//$out = $view -> showNavigationAvahis($nbAssoc, $nbRefused, $nbSelected);	
					
					
				}
				else{
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					
					if(is_string($dbClass)) $db = new $dbClass();
					
					if(isset($db)){
						$listProdAssoc = $db -> getAllAssocProds($_SESSION['ecom']['vendor'], $_SESSION['ecom']['cat']);				
						$listProdRefused = $db -> getAllRefusedProds($_SESSION['ecom']['vendor'], $_SESSION['ecom']['cat']);	
					}
					
					$_SESSION['ecom']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['ecom']['listProdRefused'] = $listProdRefused ;
					
					$nbAssoc = count($_SESSION['ecom']['listProdAssoc']) ;
					$nbRefused = count($_SESSION['ecom']['listProdRefused']); 
					$nbSelected = count($_SESSION['ecom']['selectedProds']); 
					
					$_SESSION['ecom']['nbAssoc']=$nbAssoc;
					$_SESSION['ecom']['nbRefused']=$nbAssoc;
					$_SESSION['ecom']['nbSelected']=$nbSelected;
					
					$out = $view -> showNavigation($nbAssoc, $nbRefused, $nbSelected);					
				}
			break;	
			
			
			//PErmet de mettre à jour le compteur de produit dans l'arbre de catégories en fonction du 
			//Context de navigation dans lequel on se trouve
			case '/countprodcontext':
				$dispatch = true;
				
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					
					 
					$dbClass = "BusinessSoukeoGrossisteModel";
					$vendor = 	$_SESSION['gr']['vendor'] ;
					
				}else{
					
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					$vendor = 	$_SESSION['ecom']['vendor'] ;	
				}
				
				$db = new $dbClass();
				
				$count = $db -> countAllProductInCat($_POST['cat_id'], $vendor, $_POST['context']);
				$out = $count;
			break;
			
			
			//Récupère les données pour afficher la liste des produits de l'ECOMMERCANT
			//Prie en compte de la vue utiliée Grid ou List
			case '/showprodlist' :
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
			
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
								
				$dispatch = true ;
				
				//GROSSISTES
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){

					 
					$dbClass = "BusinessSoukeoGrossisteModel";
					$vendor  = $_SESSION['gr']['vendor'] ;
					$currCat = $_SESSION['gr']['cat'];
					//Si on a reçu en POST en vue (les autres fois, changement) on la met en session
					isset($_POST['view'])? $_SESSION['gr']['view'] = $_POST['view'] : $_SESSION['gr']['view'] = $_SESSION['gr']['view'];
					$viewType = $_SESSION['gr']['view'];
				}
					// AVAHIS GP
				elseif ($_SESSION['typeVendor'] == "avahisGP" && $GLOBALS['typeVendor'] == "avahisGP"){
					$dbClass = "BusinessSoukeoModel";
					$vendor  = $_SESSION['avaGP']['vendor'] ;
					$currCat = $_SESSION['avaGP']['cat'];
					//Si on a reçu en POST en vue (les autres fois, changement) on la met en session
					isset($_POST['view'])? $_SESSION['avaGP']['view'] = $_POST['view'] : $_SESSION['avaGP']['view'] = $_SESSION['avaGP']['view'];
					$viewType = $_SESSION['avaGP']['view'];	
				}
				//ECOMMERCANTS	
				else{
					
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					$vendor  = $_SESSION['ecom']['vendor'] ;
					$currCat = $_SESSION['ecom']['cat'];
					//Si on a reçu en POST en vue (les autres fois, changement) on la met en session
					isset($_POST['view'])? $_SESSION['ecom']['view'] = $_POST['view'] : $_SESSION['ecom']['view'] = $_SESSION['ecom']['view'];
					$viewType = $_SESSION['ecom']['view'];	
				}

				if(is_string($dbClass))$db = new $dbClass();
				
				//récupère le contexte pour savoir dans quel onglet on se trouve
				$context = $_POST['context'] ;
				
				
				//Si la catégorie est asssociée on obtient l'id 
				$id_cat_assoc = $skf -> isCatAssoc($vendor, $currCat);
				
				//Liste de tous les produits non associés pour la catégorie
				$listeProduits = $db -> getAllUnassocProducts($currCat, $vendor, "", "", "", "", $context);
				$totalProd = count($listeProduits);
				foreach ($listeProduits as &$prod) {
					if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
						/*if($skf -> productIsAssocGr($_SESSION['gr']['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}*/
					}else{
						if($skf -> productIsAssoc($_SESSION['ecom']['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}
				}
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					//$listProdAssoc = $db -> getAllAssocProds($vendor, $currCat);				
					//$listProdRefused = $db -> getAllRefusedProds($vendor, $currCat);
					
					
					//$_SESSION['gr']['listProdAssoc'] = $listProdAssoc ;
					//$_SESSION['gr']['listProdRefused'] = $listProdRefused ;
					
					//PAGINATION
					$_SESSION['gr']["currPage"] = 1;
					$_SESSION['gr']['offset'] = 0;
					$limit = $_SESSION['gr']['offset'];
					//Mise en session de la liste produits ecommercant
					$_SESSION['gr']['listProdEcom'] = $listeProduits;
				}
				else{
					$listProdAssoc = $db -> getAllAssocProds($vendor, $currCat);				
					$listProdRefused = $db -> getAllRefusedProds($vendor, $currCat);
					
					
					$_SESSION['ecom']['listProdAssoc'] = $listProdAssoc ;
					$_SESSION['ecom']['listProdRefused'] = $listProdRefused ;
					
					//PAGINATION
					$_SESSION['ecom']["currPage"] = 1;
					$_SESSION['ecom']['offset'] = 0;
					$limit = $_SESSION['ecom']['offset'];
					//Mise en session de la liste produits ecommercant
					$_SESSION['ecom']['listProdEcom'] = $listeProduits;
				}
				
				
				if(count($listeProduits)<1 ){
					$out ="";
				}else{
					$out = $view->showProdList(array_slice($listeProduits,$limit, $nbProdsByPage), $viewType, 'ecom', $id_cat_assoc, $totalProd);	
				}
			break;
			
			
			//Gère le clique sur next page pour pagination et fait la différence coté avahis ou ecom
			case '/nextpageprod':
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}   
				
				$nbProd = $_POST['nbprod'];
				if( ($_SESSION[$typev][$offset] + $nbProdsByPage) > $nbProd){
					 $_SESSION[$typev][$offset] = $nbProd - $nbProd % $nbProdsByPage ;	
				}
				elseif(($_SESSION[$typev][$offset] + $nbProdsByPage) == $nbProd){
					$_SESSION[$typev][$offset] = $nbProd - $nbProdsByPage ;
				}
				else{
					$_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] + $nbProdsByPage ;

				}
				
				$nbPages = (int)($nbProd / $nbProdsByPage) ;
				if(($nbProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ;
				if(($nbProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;  
				
				if($_SESSION[$typev][$currPage]<$nbPages){
					$_SESSION[$typev][$currPage] = $_SESSION[$typev][$currPage] +1;
				}
			break;
			
			
			//Gère le clique sur next page pour pagination et fait la différence coté avahis GP
			case '/nextpageprodGP':
				//echo 'test = ';
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis') ;
				$typev = "avaGP";
				
				
				$offset="offsetav";
				$currPage = "currPageAv";
				  
				
				$nbProd = $_POST['nbprod'];

				if( ($_SESSION[$typev][$offset] + $nbProdsByPage) > $nbProd){
					 $_SESSION[$typev][$offset] = $nbProd - $nbProd % $nbProdsByPage ;	
				}
				elseif(($_SESSION[$typev][$offset] + $nbProdsByPage) == $nbProd){
					$_SESSION[$typev][$offset] = $nbProd - $nbProdsByPage ;
				}
				else{
					$_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] + $nbProdsByPage ;

				}
				
				$nbPages = (int)($nbProd / $nbProdsByPage) ;
				if(($nbProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ;
				if(($nbProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;  
				//$nbPages = 7777;
				if($_SESSION[$typev][$currPage]<$nbPages){
					$_SESSION[$typev][$currPage] = $_SESSION[$typev][$currPage] +1;
				}
			break;
			
			
			//Gère le clique sur prev page pour pagination et fait la différence coté avahis ou ecom
			case '/prevpageprod':
			
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}   
				
				$_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] - $nbProdsByPage;
				$_SESSION[$typev][$currPage] = $_SESSION[$typev][$currPage] -1 ;
				
				if($_SESSION[$typev][$offset] < 0){
					$_SESSION[$typev][$offset]   = 0;
				} 
				if($_SESSION[$typev][$currPage] < 1){
					$_SESSION[$typev][$currPage] = 1;
				}
					
			break;
			
			
			//Gère le clique sur prev page pour pagination et fait la différence coté avahis ou ecom
			case '/prevpageprodGP':
			
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis');
				//Avahis GP
				$typev = "avaGP";
								   
				if(isset($_GET['type']) && $_GET['type'] == "avaGP") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}   
								
				$_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] - $nbProdsByPage;
				$_SESSION[$typev][$currPage] = $_SESSION[$typev][$currPage] -1 ;
				
				if($_SESSION[$typev][$offset] < 0){
					$_SESSION[$typev][$offset]   = 0;
				} 
				if($_SESSION[$typev][$currPage] < 1){
					$_SESSION[$typev][$currPage] = 1;
				}
					
			break;
			
			
			case '/firstpageprod':
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				//ECOMMERCANT	
				}else{
					$typev = "ecom";
				}
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}
				$_SESSION[$typev][$offset] = 0;
				$_SESSION[$typev][$currPage] = 1; 
			break;
			
			
			// premmiére page pour GP	
			case '/firstpageprodGP':
				$typev = "avaGP";
				$offset="offsetav";
				$currPage = "currPageAv";
				$_SESSION[$typev][$offset] = 0;
				$_SESSION[$typev][$currPage] = 1; 
			break;
			
			
			case '/lastpageprod':
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				if(isset($_GET['type']) && $_GET['type'] == "ecom") {
					$offset="offset";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av") {
					$offset="offsetav";
					$currPage = "currPageAv";
				}
				$nbProd = $_POST['nbprod'];
				
				$_SESSION[$typev][$offset] = $nbProd - $nbProd % $nbProdsByPage;	
				//Si modulo de 9 = 0 il n'y aura aucun produits sur la dernière page;
				if(($nbProd % $nbProdsByPage) == 0) $_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] - $nbProdsByPage;
				
				$nbPages = (int)($nbProd / $nbProdsByPage) ;
				if(($nbProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ;
				if(($nbProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;  
				
				$_SESSION[$typev][$currPage] = $nbPages;
				
			break;
			
			
			case '/lastpageprodGP':
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis');
				//Avahis GP
				$typev = "avaGP";
				$offset="offsetav";
				$currPage = "currPageAv";
				
				$nbProd = $_POST['nbprod'];
				
				$_SESSION[$typev][$offset] = $nbProd - $nbProd % $nbProdsByPage;	
				//Si modulo de 9 = 0 il n'y aura aucun produits sur la dernière page;
				if(($nbProd % $nbProdsByPage) == 0) $_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] - $nbProdsByPage;
				
				$nbPages = (int)($nbProd / $nbProdsByPage) ;
				if(($nbProd % $nbProdsByPage) != 0) $nbPages = $nbPages + 1 ;
				if(($nbProd % $nbProdsByPage) == 0) $nbPages = $nbPages ;  
				
				$_SESSION[$typev][$currPage] = $nbPages;
			break;
			
			
			//Gère l'affichage de la liste produit après clics sur pagination fait la différence entre coté avahis et ecom
			case '/loadessionprodlist':
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				//CHOIX DU TYPE coté ecommercant ou coté Avahis
				if(isset($_GET['type']) && $_GET['type'] == "ecom"){
					$offset="offset";
					$listeProd = $_SESSION[$typev]['listProdEcom'];
					$vue = $_SESSION[$typev]['view'];
					$type = "ecom";
					$currPage = "currPage";
				} 
				if(isset($_GET['type']) && $_GET['type'] == "av"){
					$offset="offsetav";
					$listeProd =$_SESSION[$typev]['listProdAv'];
					$vue = $_SESSION[$typev]['viewAv'];
					$type = "av";
					$currPage = "currPageAv";
				}
				if(isset($_GET['type']) && $_GET['type'] == "avGP"){
					$offset="offsetav";
					$listeProd =$_SESSION[$typev]['listProdAv'];
					$vue = $_SESSION[$typev]['viewAv'];
					$type = "av";
					$currPage = "currPageAv";
				}      
				
				$dispatch=true;
				$nbProd = count($listeProd);
				
				//GESTION PAGINATION
				isset($_SESSION[$typev][$offset]) ? $_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] : $_SESSION[$typev][$offset] = 0;
				isset($_SESSION[$typev][$currPage]) ? $_SESSION[$typev][$currPage] =$_SESSION[$typev][$currPage] : $_SESSION[$typev][$currPage] = 1;
				
				if($_SESSION[$typev][$offset] > $nbProd) {
					$limit = $nbProd - $nbProdsByPage;	
					$_SESSION[$typev][$offset] = $nbProd - $nbProdsByPage;
				}
				else {
					$limit = $_SESSION[$typev][$offset];
				}

				$out = $view->showProdList(array_slice($listeProd,$limit, $nbProdsByPage), $vue, $type, false, $nbProd, $_SESSION[$typev][$currPage]);
			break;
			
			
			//Gère l'affichage de la liste produit après clics sur pagination avahisGP
			case '/loadessionprodlistGP':
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis') ;
				
				//Avahis GP
				$typev = "avaGP";
										  
				//if(isset($_GET['type']) && $_GET['type'] == "avGP"){
				$offset="offsetav";
				$listeProd =$_SESSION[$typev]['listProdAv'];
				$vue = $_SESSION[$typev]['viewAv'];
				$type = "av";
				$currPage = "currPageAv";
				//}   
				
				$dispatch=true;
				$nbProd = count($listeProd);
				
				//GESTION PAGINATION
				isset($_SESSION[$typev][$offset]) ? $_SESSION[$typev][$offset] = $_SESSION[$typev][$offset] : $_SESSION[$typev][$offset] = 0;
				isset($_SESSION[$typev][$currPage]) ? $_SESSION[$typev][$currPage] =$_SESSION[$typev][$currPage] : $_SESSION[$typev][$currPage] = 1;
				
				if($_SESSION[$typev][$offset] > $nbProd) {
					$limit = $nbProd - $nbProdsByPage;	
					$_SESSION[$typev][$offset] = $nbProd - $nbProdsByPage;
				}
				else {
					$limit = $_SESSION[$typev][$offset];
				}
				

				$out = $view->showProdListGP(array_slice($listeProd,$limit, $nbProdsByPage), $vue, $type, false, $nbProd, $_SESSION[$typev][$currPage]);
			break;
			
					
			//Récupération des informations pour l'affichage des filtres dynamiquess d'attributs
			//Des produits de l'ecommercant
			case '/showdynafilter' :
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}	
				
				$dispatch = true;
				//GROSSISTES
				if($typev == "gr"){
					
					if($_POST['t'] == "FK"){
						$dbClass = "BusinessSoukeoCarshopModel";	
					}else{
						$dbClass = "BusinessSoukeoGrossisteModel";
					}
					
					$db = new $dbClass();
					
					$coupleAttrVal = $db -> getDistinctAttr($_POST['category']);
					//Mise en session du tableau
					$_SESSION['gr']['attrVal'] = $coupleAttrVal;
				
					$out = $view -> showDynaFilter($coupleAttrVal, "Grossiste");
				}
				//ECOMMERCANTS
				else{
					
					$dbClass = "BusinessSoukeo".$_POST['t']."Model";
					$db = new $dbClass();
					
					//Montre les 3 attributs les plus utilisés
					$listeAttr = $db -> getBestAttrFromVendorCateg($_POST['category'],$_POST['id']);
					
					$coupleAttrVal  = array();
					//Création d'un tableau de couple attribut => liste de valeurs
					foreach($listeAttr as $attr){
						
						$listVal = $db -> getAllDisctValueForAttrFromVendorCateg($_POST['category'],$_POST['id'], $attr['ID_ATTR']);
						$coupleAttrVal[] = array('attr' => $attr, 'val' => $listVal); 
					}
					//Mise en session du tableau
					$_SESSION['ecom']['attrVal'] = $coupleAttrVal;
				
					$out = $view -> showDynaFilter($coupleAttrVal);	
				}
			break;
			
			
			//Récupération des informations pour l'affichage des filtres dynamiquess d'attributs
			//Des produits Avahis
			case '/showdynafilterav':
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}	
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				$dispatch = true;
				
				//VERSION EN COMMENTAIRE EST LA VERSION Où on voit même les attributs pour les sur-catégorie ne possédant pas directement
				//de produits
				//$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				//$cat =  substr($cat, 0,-2);
				$listeAttr = $skf -> getBestAttrFromCateg($_POST['category']);
				//$listeAttr = $skf -> getBestAttrFromCateg($cat);
				
				$coupleAttrVal  = array();
				//Création d'un tableau de couple attribut => liste de valeurs
				foreach($listeAttr as $attr){
					$listVal = $skf -> getAllDisctValueForAttrFromCateg($_POST['category'], $attr['ID_ATTR']);
					//$listVal = $skf -> getAllDisctValueForAttrFromCateg($cat, $attr['ID_ATTR']);
					
					$coupleAttrVal[] = array('attr' => $attr, 'val' => $listVal); 
				}
				//Mise en session du tableau
				$_SESSION[$typev]['attrValAv'] = $coupleAttrVal;

				$out = $view -> showDynaFilter($coupleAttrVal);
			break;
			
			
			//Récupération des informations nécessaires à l'affichage des filtres de recherche par 
			//marque des produits ECOMMERCANT
			case '/showbrandfilter' :
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				
				$dbClass = "BusinessSoukeo".$_POST['t']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//Récupération de toute les marques uniques pour la catégorie
				$brands = $db -> getAllDistinctManufFromCatVendor($_POST['category'],$_POST['id']);
				$_SESSION[$typev]['brands']= $brands;
				
				$out = $view -> showBrandFilter($brands, "ecom");
			break;
			
						//Récupération des informations nécessaires à l'affichage des filtres de recherche par 
			//marque des produits ECOMMERCANT
			
			
			
			case '/o_admin/showeditmenu/' :
				$db = new BusinessSoukeoModel(); 
				$template = "rendered"; 				
		 		$out = "Edition menu Avahis";
				$headmenu = $db->getall("SELECT * FROM  `skf_headmenu` ");
				foreach($headmenu as $H ){
					//var_dump($H);	
					$out .= "<form id='test_".$H["id_menu"]."'>";	
					$out .= "<table><tr>".
							"<td>".$H["label_menu"]."</td>".
							"<td style='background:lime'>".$H["position_menu"]."</td></tr>".
							"<tr><td colspan='2'>".
							"<table class= 'conmenu' ><tr>";
					//Pour chaque col 
					for ($j=1; $j <= 5 ; $j++) {
						$content_menu = $db->getall("select * from skf_contentmenu where id_menu = '".$H['id_menu']."' and col_contentmenu ='".$j."'  " );
					
						for ($i=1; $i < 11 ; $i++) { 	
							$C = $content_menu[$i-1];
							if ( $C['type_contentmenu'] == "titre" ) {
								$add = "background:red";
								$ap = 'CHECKED';
							}else {
								$add = "";
								$ap = "";
							}
							//$j = numcol 
							//$i = classement
							
							$out_tt .= "<tr class='lmenu' style='$add'  ><td style='$add' >
										<input type='text' id='label_".$j."_$i' name='label_".$j."_".$i."_".$H['id_menu']."'  value='".htmlspecialchars($C['label_contentmenu'], ENT_QUOTES)."' />
										</td> <td>
										<input type='text' id='link_".$j."_$i' name='link_".$j."_".$i."_".$H['id_menu']."'  value='".$C["menu_link"]."' />
										</td><td>
										<input id='titre_".$j."_".$i."_".$H['id_menu']."'  name='titre_".$j."_".$i."_".$H['id_menu']."'   type='checkbox' $ap />
										</td></tr>"; 
						}
						$out .= "<td><table><tr><td>label</td><td>url</td><td>titre</td></tr>$out_tt</table></td>";
						$out_tt  = "";
					}
					$out .= "</tr></table></td></tr></tr></table></br></br>";
					$out .= "<input type='button' onclick='sendformMenu(this.form)' value = 'send'>";
					$out .= "</form>";
					$out .= "<div id='c".$H["id_menu"]."'></div>";
				}			
				$view -> assign("out", $out );
				
				
			break;
			
				
			case '/saveeditmenu' :
	//			var_dump(unserialize($_POST["data"]));
			$db = new BusinessSoukeoModel(); 
			
			$dispatch = true;
				$query = $_POST["data"];
			$data = array();
			foreach (explode('&', $query) as $chunk) {
    			$param = explode("=", $chunk);

    			if ($param) {
    				$data [urldecode($param[0])] = urldecode($param[1]);
        		//	printf("La valeur du paramètre \"%s\" est \"%s\"<br/>\n", urldecode($param[0]), urldecode($param[1]));
			    }
			}
			
			
			foreach ($data as $K => $V) {
				//echo $K."    ".$V ."<br/>" ;
				$sqlS = "select * from skf_contentmenu  where id_menu = '".$te[3]."' and col_contentmenu ='".$te[1]."' and position_contentmenu =  '".$te[2]."'";
				$res = $db->getOne($sqlS);
				var_dump($sqlS);
				var_dump($res);	
				$te = explode('_', $K);
				//label_".$j."_".$i."_".$H['id_menu'].
				$sql = "insert into skf_contentmenu set ".$te[0]."='".htmlentities($V)."'  where id_menu = '".$te[3]."' and col_contentmenu ='".$te[1]."' and position_contentmenu =  '".$te[2]."'";
				echo $sql."<br/>";
			}
			
			
				//Col Right
			//	ob_start();
			//	var_dump($data);
			//	$out = ob_get_contents();
			//	ob_end_clean();
			
			
			//var_dump($data);			
			break;
			//Récupération des informations nécessaires à l'affichage des filtres de recherche par 
			//marque des produits AVAHIS
			case '/showbrandfilterav' :
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				
				//Récupération de toute les marques uniques pour la catégorie
				$brands = $skf -> getAllDistinctBrandFromCat($_POST['category']);
				$_SESSION[$typev]['brandsAv']= $brands;
				
				$out = "";
				echo $view -> showBrandFilter($brands, "av");
			break;
			
			
			//Récupération des informations nécessaires à l'affichage des filtres de recherche par 
			//marque des produits AVAHIS
			case '/showbrandfilteravGP' :
				$dispatch = true;
				//Avahis GP
				$typev = "avaGP";
				//Récupération de toute les marques uniques pour la catégorie
				$brands = $skf -> getAllDistinctBrandFromCat($_POST['category']);
				$_SESSION[$typev]['brandsAv']= $brands;
				
				$out = "";
				echo $view -> showBrandFilterGP($brands, "av");
			break;
			
			
			//Appel de la vue pour afficher l'entete coté droit Avahis
			case '/showenteteavahis' :
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
				}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				} 
				
				unset($_SESSION[$typev]['offsetAv']);
				$dispatch = true ;
				$_SESSION[$typev]['catAv']= $_POST['category'] ;
				$catIdTree = $skf -> getCatIdTree($_POST['category']);
				$view -> assign('catIdTree', $catIdTree);
				$out = $view->showEnteteAvahis($_POST['category']);
			break;			
			
			
			//Appel de la vue pour afficher l'entete Avahis GP
			case '/showenteteavahisGP' :
				//echo "test : ";
				$typev = "avaGP";
				unset($_SESSION[$typev]['offsetAv']);
				$dispatch = true ;

				$_SESSION[$typev]['catAv']= $_POST['category'] ;
				$catIdTree = $skf -> getCatIdTree($_POST['category']);
				$view -> assign('catIdTree', $catIdTree);
				$out = $view->showEnteteAvahisGP($_POST['category']);
			break;
			
			
			
			//Liste des produits pour avahis
			case '/showprodlistav' :
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
					$typev = "gr";
					//Avahis GP
				}
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{
					$typev = "ecom";
				}
				
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true ;
				//Le $_POST['view'] est récupéré depui l'appel AJAX et mis en session
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//Récupération d'une liste de produit avahis avec champs renommés en Alias pour utilisation dans la vue

				$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				$cat =  substr($cat, 0,-2);
				if($_SESSION['ecom']['context'] =='todo'&&$_SESSION['ecom']['context']==''){
					$listeProduits = $skf -> getAllProdByCatIdAlias($cat, null,null,null,null,null,null,null, null, null, null, null);
				}
				elseif ($_SESSION['ecom']['context'] =='deleted'){
					$_SESSION['ecom']['catAv']== null?	
						$listeProduits = $skf -> getAllProdByCatIdAlias(null,null,null,null,null,null,null,'1', null, null, null, null):
						$listeProduits = $skf -> getAllProdByCatIdAlias($cat,null,null,null,null,null,null,'1', null, null, null, null)	;
					
				}
				else{
					$listeProduits = $skf -> getAllProdByCatIdAlias($cat, null,null,null,null,null,null,null, null, null, null, null);
				} 
				$nbProd = count($listeProduits);
				
				//Pagination RAZ
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				//Mise en session de la lite produit pour actualiation 
				$_SESSION[$typev]['listProdAv']=$listeProduits;
				//Si écran Gestion/produit
				if($_SESSION['typeVendor'] == "avahisGP" && $GLOBALS['typeVendor'] == "avahisGP"){
					$listProdDeleted= $skf -> getAllDeletedProds();

					
					$_SESSION['ecom']['listProdDeleted'] = $listProdDeleted;
				}	
						
				$out = $view->showProdList(array_slice($listeProduits,$limit, $nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);	
			break;
			
			
			//Liste des produits pour avahis GP
			case '/showprodlistavGP' :
				//Avahis GP
				$typev = "avaGP";
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis') ;
				
				$dispatch = true ;
				//Le $_POST['view'] est récupéré depui l'appel AJAX et mis en session
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//Récupération d'une liste de produit avahis avec champs renommés en Alias pour utilisation dans la vue
				
				
				/////////////////////////////////////// ATTENTION //////////////////////////////////////////
				//$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				//$cat =  substr($cat, 0,-2);
				////////////////////////////////////////////////////////////////////////////////////////////
				$cat = $_SESSION[$typev]['catAv'];
				$_SESSION['avaGP']['activeProducts'] == "true" ? $actif = true : $actif = false;
                
				if($_SESSION[$typev]['context'] =='todo'&&$_SESSION[$typev]['context']==''){
					$listeProduits = $skf -> getAllProdByCatIdAlias($cat,null,null,null,null,null,null,null,null,null,null, $actif);
				}
				elseif ($_SESSION[$typev]['context'] =='deleted'){
					$_SESSION[$typev]['catAv']== null?	
						$listeProduits = $skf -> getAllProdByCatIdAlias(null,null,null,null,null,null,null,null,null,null,null, $actif):
						$listeProduits = $skf -> getAllProdByCatIdAlias($cat,null,null,null,null,null,null,null,null,null,null, $actif)	;
					
				}
				else{
					$listeProduits = $skf -> getAllProdByCatIdAlias($cat,null,null,null,null,null,null,null,null,null,null, $actif);
				} 
				$nbProd = count($listeProduits);
				
				//Pagination RAZ
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				
				//Mise en session de la lite produit pour actualiation 
				$_SESSION[$typev]['listProdAv']=$listeProduits;
				
				//Si écran Gestion/produit
				if($_SESSION['typeVendor'] == "avahisGP"){
					$listProdDeleted= $skf -> getAllDeletedProds();

					$_SESSION[$typev]['listProdDeleted'] = $listProdDeleted;
				}	
		
				
				$out = $view->showProdListGP(array_slice($listeProduits,$limit, $nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);	
				
				
			break;
			
			
			
			//Récupération des informations et d'une liste produits Avahis filtrée en fonction de ce que l'utilisateur a entré
			case '/showprodlistfilteredav' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Récupération de la vue grid ou lit
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//GESTION DES FILTRES
				$brand = $_POST['brand'];
				$priceMin = (float)$_POST['pmin'];
				$priceMax = (float)$_POST['pmax'];
				$search = $_POST['search'];
				$order = $_POST['order'];
				$allProd = $_POST['allProd'];
				$allPoid = $_POST['allPoid'];
				$allassoc = $_POST['allAsso'];
				$allVendu = $_POST['allVendu'];
				$idVendor = $_POST['searchVendor'];

				//Recuperation de la liste produits avec filtres de recherche, la catégorie est en session
				$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				$cat =  substr($cat, 0,-2);
				
				if(isset($_SESSION['typeVendor']) && $GLOBALS['typeVendor'] == "avahisGP"){
					$allassoc ? $assoc = 1 : $assoc = null ;
					$allPoid ? $poid = 1 : $poid = null ;
					$allVendu ? $vendu = 1 : $vendu = null ;
					$allProd ?	$cat = null : $cat = $cat ;
					if($_SESSION['ecom']['context'] =='deleted'){
						$deleted = 1 ;
					}
					
				}
                $allProd ?  $cat = null : $cat = $cat ;
				$listeProduits = $skf -> getAllProdByCatIdAlias($cat, $brand, $priceMin, $priceMax, $search, $order, $idVendor, $deleted, $assoc, $poid, $vendu, false);
				//Attributs reçus en ajax, tableau 2 dimensions avec les 3 attributs 
				//Fonction AJAX peut etre rendu dynamique pour X attributs
				$attr = $_POST['attr'];
				
				//GESTION DES PRODUITS FILTRES PAR ATTRIBUTS DYNAMIQUES
				$listeProdFiltre = array();
				//Pour chaque produit on vérifie s'il correspond aux critères de recherche
				
				if(!empty($attr)){
					foreach ($listeProduits as $prod) {
					
						//Création d'un tableau de vérification il faut que toutes les valeurs soient vraies
						$verif = array();
						
						foreach ($attr as $a) {
							
							//Si on a bien entré ce critère et qu'il correspond
							if ($a['val'] != "--"){
								
								if ($skf -> verifAttrForProductSkf($prod['ID_PRODUCT'], $a['id'], $a['val'])){
									$verif[] = true;
								}else{
									$verif[] = false;
								}
							}		
						}
						
						if(!in_array(false, $verif)){
							//ON remplit le tableau des produits corrects si tout correspond
							$listeProdFiltre[] = $prod;
						}
					}
				}else{
					$listeProdFiltre = $listeProduits;
				}
				
				$_SESSION[$typev]['listProdAv']=$listeProdFiltre;
				
				//PAGINATION
				$nbProd = count($listeProdFiltre);
				//Variables session pour pagination limite de 9 produits par page
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				
				//Affichage 
				$out = $view->showProdList(array_slice($listeProdFiltre,$limit,$nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);
			break;	
            
            case '/supprAssocGrossiste':
            
	           $dispatch = true;
               
               if(!empty($_POST['id_product']) && !empty($_POST['id_produit'])){
                   
                   $skf -> supprAssocGr($_POST['id_product'], $_POST['id_produit']);     
               }
                
	        break;
                
			//Récupération des informations et d'une liste produits Avahis filtrée en fonction de ce que l'utilisateur a entré
			case '/showprodlistfilteredavGP' :
            
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis');
				
				$dispatch = true;
				//Avahis GP
				$typev = "avaGP";
				
				//Récupération de la vue grid ou lit
				isset($_POST['view'])? $_SESSION[$typev]['viewAv'] = $_POST['view'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				
				//GESTION DES FILTRES
				$brand = $_POST['brand'];
				$priceMin = (float)$_POST['pmin'];
				$priceMax = (float)$_POST['pmax'];
				$search = $_POST['search'];
				$order = $_POST['order'];
				$allProd = $_POST['allProd'];
				$allPoid = $_POST['allPoid'];
				$allassoc = $_POST['allAsso'];
				$allVendu = $_POST['allVendu'];
                $allActif = $_POST['allActif'];
				$idVendor = $_POST['searchVendor'];
				
				//Recuperation de la liste produits avec filtres de recherche, la catégorie est en session
				$cat = $skf -> getAllsScatId ($_SESSION[$typev]['catAv']);
				$cat =  substr($cat, 0,-2);
				
				//PROVISOIRE A EFFACER QUAND ILS NE VOUDRONT PLUS LA RECHERCHE PAR CATEGORIE
				$cat = $_SESSION[$typev]['catAv']; //PROVISOIRE
				//PROVISSOIRE 
				
				
				$allProd ?	$cat = null : $cat =$cat ;
				$allassoc ? $assoc = 1 : $assoc = null ;
				$allPoid ? $poid = 1 : $poid = null ;
				$allVendu ? $vendu = 1 : $vendu = null ;
                $allActif ? $actif = true : $actif = false ;
				if($_SESSION[$typev]['context'] =='deleted'){
					$deleted = 1 ;
				}

				
				$listeProduits = $skf -> getAllProdByCatIdAlias($cat, $brand, $priceMin, $priceMax, $search, $order, $idVendor, $deleted, $assoc, $poid, $vendu, $actif);
				//Attributs reçus en ajax, tableau 2 dimensions avec les 3 attributs 
				//Fonction AJAX peut etre rendu dynamique pour X attributs
				$attr = $_POST['attr'];
				
				//GESTION DES PRODUITS FILTRES PAR ATTRIBUTS DYNAMIQUES
				$listeProdFiltre = array();
				//Pour chaque produit on vérifie s'il correspond aux critères de recherche
				
				if(!empty($attr)){
					foreach ($listeProduits as $prod) {
					
						//Création d'un tableau de vérification il faut que toutes les valeurs soient vraies
						$verif = array();
						
						foreach ($attr as $a) {
							
							//Si on a bien entré ce critère et qu'il correspond
							if ($a['val'] != "--"){
								
								if ($skf -> verifAttrForProductSkf($prod['ID_PRODUCT'], $a['id'], $a['val'])){
									$verif[] = true;
								}else{
									$verif[] = false;
								}
							}		
						}
						
						if(!in_array(false, $verif)){
							//ON remplit le tableau des produits corrects si tout correspond
							$listeProdFiltre[] = $prod;
						}
					}
				}else{
					$listeProdFiltre = $listeProduits;
				}
				
				$_SESSION[$typev]['listProdAv'] = $listeProdFiltre;
				
				//PAGINATION
				$nbProd = count($listeProdFiltre);
				//Variables session pour pagination limite de 9 produits par page
				$_SESSION[$typev]['offsetav'] = 0;
				$_SESSION[$typev]["currPageAv"] = 1;
				$limit = $_SESSION[$typev]['offsetav'];
				
				
				//Affichage 
				$out = $view->showProdListGP(array_slice($listeProdFiltre,$limit,$nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $nbProd);				
			break;	
			
			
			//Récupération d'une liste de produit Ecommercant filtré en fonction des filtres de recherche entrés
			case '/showprodlistfiltered' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				isset($_POST['view'])? $_SESSION[$typev]['view'] = $_POST['view'] : $_SESSION[$typev]['view'] = $_SESSION[$typev]['view'];
				
				//GESTION DES FILTRES
				$brand = $_POST['brand'];
				$priceMin = (float)$_POST['pmin'];
				$priceMax = (float)$_POST['pmax'];
				$search = $_POST['search'];
				$order = $_POST['order'];
				
				//Choix du bon model par rapport à la tech en session
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				
				///////////////////////////////////////// GROSSISTE /////////////////////////////////////////////////
				if($typev == "gr"){
					
					$dbClass = "BusinessSoukeoGrossisteModel";
					$db = new $dbClass();
					
					$attr = $_POST['attr'];
					$listeProdFiltre = $db -> getAllUnassocProducts($_SESSION[$typev]['cat'], $_SESSION[$typev]['vendor'], $brand, $priceMin, $priceMax, $search, $_POST['context'], $order, $attr);
					
					foreach ($listeProdFiltre as &$prod) {
						if($skf -> productIsAssocGr($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}
				}
				///////////////////////////////////////// ECOMMERCANT ////////////////////////////////////////////////	
				else{
					
					$db = new $dbClass();
					
					//Récupération de la lite produits avec filtres de recherche, la catégorie est en session
					$listeProduits = $db -> getAllUnassocProducts($_SESSION[$typev]['cat'], $_SESSION[$typev]['vendor'], $brand, $priceMin, $priceMax, $search, $_POST['context'], $order);
					
					//Attributs reçus en ajax, tableau 2 dimensions avec les 3 attributs 
					//Fonction AJAX peut etre rendu dynamique pour X attributs
					$attr = $_POST['attr'];
					
					//GESTION DES PRODUITS FILTRES PAR ATTRIBUTS DYNAMIQUES
					$listeProdFiltre = array();
					
					if(empty($attr)){
						$listeProdFiltre = $listeProduits;
					}else{
						//Pour chaque produit on vérifie s'il correspond aux critères de recherche
						foreach ($listeProduits as $prod) {
						
							//Création d'un tableau de vérification il faut que toutes les valeurs soient vraies
							$verif = array();
							
							foreach ($attr as $a) {
								
								//Si on a bien entré ce critère et qu'il correspond
								if ($a['val'] != "--"){
									
									if ($db ->verifAttrForProduct($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'], $a['id'], $a['val'])){
										$verif[] = true;
									}else{
										$verif[] = false;
									}
								}		
							}
							
							if(!in_array(false, $verif)){
								//ON remplit le tableau des produits corrects si tout correspond
								$listeProdFiltre[] = $prod;
							}
						}	
					}
	
					foreach ($listeProdFiltre as &$prod) {

						if($skf -> productIsAssoc($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}						
					}					
				}

				/////////////////////////////////////////////// GENERIQUE //////////////////////////////////////////////////
				
				$listProdAssoc = $db -> getAllAssocProds($_SESSION[$typev]['vendor'], $_SESSION[$typev]['cat'], $brand, $priceMin, $priceMax, $search);				
				$listProdRefused = $db -> getAllRefusedProds($_SESSION[$typev]['vendor'], $_SESSION[$typev]['cat'], $brand, $priceMin, $priceMax, $search);
				$_SESSION[$typev]['listProdAssoc'] = $listProdAssoc ;
				$_SESSION[$typev]['listProdRefused'] = $listProdRefused ;
				
				
				//Mise en session de la liste produits ecommercant
				$_SESSION[$typev]['listProdEcom'] = $listeProdFiltre;
				
				$totalProd = count($listeProdFiltre);
				
				//PAGINATION
				$_SESSION[$typev]["currPage"] = 1;
				$_SESSION[$typev]['offset'] = 0;
				$limit = $_SESSION[$typev]['offset'];
				
				$_SESSION[$typev]['listProdEcom']=$listeProdFiltre;
				
				$out = $view->showProdList(array_slice($listeProdFiltre,$limit,$nbProdsByPage), $_SESSION[$typev]['view'], 'ecom', "", $totalProd);
				
			break;
			
			
			//Récupère les informations permettant l'affichage uniquement de la liste des produits selectionnés par l'utilisateur
			case '/showselectedprodlist' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page') ;
				
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				isset($_POST['view'])? $_SESSION[$typev]['view'] = $_POST['view'] : $_SESSION[$typev]['view'] = $_SESSION[$typev]['view'];
				isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
				
				//Choix model en fonction de la tech ecom
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				$selectProdList = array();
				
				//On possède en session la liste des ID des produits que l'utilisateur a selection
				//A partir de tous ces id on va créer un tableau de produits
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					$prod = $db -> getProduct($_SESSION[$typev]['vendor'], $id);
					$prod = $prod[0];
					
					if($typev == "gr"){
						if($skf -> productIsAssocGr($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}elseif($typev == "ecom"){
						if($skf -> productIsAssoc($_SESSION[$typev]['vendor'], $prod['ID_PRODUCT'])){
							$prod['assoc'] = 1;
						}else{
							$prod['assoc'] = 0;
						}
					}
					
					
					$selectProdList[] = $prod;
				}
				$totalProd = count($selectProdList);
				//PAGINATION
				$_SESSION[$typev]["currPage"] = 1;
				$_SESSION[$typev]['offset'] = 0;
				$limit = $_SESSION[$typev]['offset'];
				
				$_SESSION[$typev]['listProdEcom']=$selectProdList;
				
				$out = $view->showProdList(array_slice($selectProdList,$limit,$nbProdsByPage), $_SESSION[$typev]['view'], 'ecom', "", $totalProd);
				
			break;
			
			
			//Récupère les informations permettant l'affichage uniquement de la liste des produits selectionnés par l'utilisateur pour l'écran gestion/produit
			case '/showselectedprodlistAvGP' :
				//$nbProdsByPage = SystemParams::getParam('system*nb_prods_page');
				//echo"test de la page de sélection ";
				$nbProdsByPage = SystemParams::getParam('system*nb_prods_page_avahis');
				$dispatch = true;
				$typev = "avaGP";
				
				isset($_POST['viewAv'])? $_SESSION[$typev]['viewAv'] = $_POST['viewAv'] : $_SESSION[$typev]['viewAv'] = $_SESSION[$typev]['viewAv'];
				isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
				
				
				$dbClass = "BusinessSoukeoModel";
				$db = new $dbClass();
				
				$selectProdList = array();
				
				//On possède en session la liste des ID des produits que l'utilisateur a selection
				//A partir de tous ces id on va créer un tableau de produits
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
				    
					$prod = $db -> getProductAlias($id);
					$selectProdList[] = $prod;
				}
				
				$totalProd = count($selectProdList);
				//PAGINATION
				$_SESSION[$typev]["currPage"] = 1;
				$_SESSION[$typev]['offset'] = 0;
				$limit = $_SESSION[$typev]['offset'];
				
				$_SESSION[$typev]['listProdAv'] = $selectProdList;
				
				
				$out = $view->showProdListGP(array_slice($selectProdList,$limit,$nbProdsByPage), $_SESSION[$typev]['viewAv'], 'av', "", $totalProd);
			break;
			
			
			//Permet de traiter l'ajout d'un produit à la liste de produits selectionnés en vérifiant les doublons
			case '/selectproduct':
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Vérification de la présence de variable en session ou première fois
				isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
				 
				$dispatch = true;
				//Id du produit qu'on vient de recevoir à traiter
				$idProduct = $_POST['id'];
				//Vérifications si c'esst un doublon ou pas
				if(!in_array($idProduct, $_SESSION[$typev]['selectedProds'])){
					//Ajout en session de l'id du produit
					$_SESSION[$typev]['selectedProds'][]= $idProduct;	
				}
				//Renvoi pour affichage du nombre de produits selectionnés
				$out = count($_SESSION[$typev]['selectedProds']);
			break;
			
			
			//permet de déselectionner un produit de notre lite de selection
			case '/deselectproduct': 
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//ID du produit à deselectionner
				$idProduct = $_POST['id'];
				
				//Si l'id de ce produit existe bien on l'unset
				if(($key = array_search($idProduct, $_SESSION[$typev]['selectedProds'])) !== false) {
    				unset($_SESSION[$typev]['selectedProds'][$key]);
				}
				//Renvoi pour affichage du nombre de produits selectionnés
				$out = count($_SESSION[$typev]['selectedProds']);
			break;
			
			
			case '/selectallproduct':
				$dispatch = true;	
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//DESELECT ALL
				if(isset($_GET['a']) && $_GET['a'] == "undo"){
					unset($_SESSION[$typev]['selectedProds']);
					$out = count($_SESSION[$typev]['selectedProds'] = array());
				//SELECT ALL	
				}else{
					//Vérification de la présence de variable en session ou première fois
					isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
					
					if(!empty($_SESSION[$typev]['listProdEcom'])){
						//Vérifications si c'esst un doublon ou pas
						foreach ($_SESSION[$typev]['listProdEcom'] as $prod) {
							if(!in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
								//Ajout en session de l'id du produit
								$_SESSION[$typev]['selectedProds'][]= $prod['ID_PRODUCT'];	
							}	
						}
						//Renvoi pour affichage du nombre de produits selectionnés
						$out = count($_SESSION[$typev]['selectedProds']);
					}else{
						$out = 0;
					}	
				}
				
				
			break;
				case '/selectallproductAvGP':
				$dispatch = true;	
				//GROSSISTE
				$typev = "avaGP";
				
				//DESELECT ALL
				if(isset($_GET['a']) && $_GET['a'] == "undo"){
					unset($_SESSION[$typev]['selectedProds']);
					$out = count($_SESSION[$typev]['selectedProds'] = array());
				//SELECT ALL	
				}else{
					//Vérification de la présence de variable en session ou première fois
					isset($_SESSION[$typev]['selectedProds']) ? $_SESSION[$typev]['selectedProds'] = $_SESSION[$typev]['selectedProds'] : $_SESSION[$typev]['selectedProds'] = array();
					
					if(!empty($_SESSION[$typev]['listProdAv'])){
						//Vérifications si c'esst un doublon ou pas
						foreach ($_SESSION[$typev]['listProdAv'] as $prod) {
							if(!in_array($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])){
								//Ajout en session de l'id du produit
								$_SESSION[$typev]['selectedProds'][]= $prod['ID_PRODUCT'];	
							}	
						}
						
						//Renvoi pour affichage du nombre de produits selectionnés
						$out = count($_SESSION[$typev]['selectedProds']);
					}else{
						$out = 0;
					}	
				}	
			break;
			
			
			case '/selectallventesav':
				$dispatch = true;
				
				if(isset($_GET['a']) && $_GET['a'] == "undo" ){
					unset($_SESSION['selectedProdsGr']);
				}else{
					$grid = Grid::getObj('venteavahis');
					$rs = dbQueryall($grid->_getSqlQuery());
					foreach ($rs as $row) {
						if(!in_array($row['ID_PRODUCT'], $_SESSION['selectedProdsGr'])){
							//Ajout en session de l'id du produit
							$_SESSION['selectedProdsGr'][]= $row['ID_PRODUCT'];	
						}
					}
				}

			break;
			
			
			case '/selectoneventesav':
				$dispatch = true;
				
				if(isset($_GET['a']) && $_GET['a'] == "undo"){
					
					if(($key = array_search($_POST['id'], $_SESSION['selectedProdsGr'])) !== false) {
    					unset($_SESSION['selectedProdsGr'][$key]);
						//$out = $_POST['id']. " supprimé de la sélection !";
					}
				}
				else{
					if(!in_array($_POST['id'], $_SESSION['selectedProdsGr'])){
						//Ajout en session de l'id du produit
						$_SESSION['selectedProdsGr'][]= $_POST['id'];
						//$out = $_POST['id']." ajouté en sélection !";	
					}	
				}
			break;

						
			case '/savemodifventeav' :
				$dispatch = true;
				
				$db = new BusinessSoukeoGrossisteModel();
				
				$product = $db -> getPrixCalcul($_POST['id']);
				$product = $product[0];
				
				//CALCUL
				
				$prixAchatHT = $product['PRIX_ACHAT'];				
				//pas de hauteur largeur longueur
				$poidsVolum = $product['POIDS'];
							
				$prixKilo = $prixAchatHT / $poidsVolum;		
					
						
				$aerien = (float)SystemParams::getParam('grossiste*aerienchrono') * (float)$poidsVolum;
				
				$octroiMer = ( (float)$_POST['percentOctrmer'] / 100 ) * ($prixAchatHT + $aerien);

				$frais_livr = $product['FRAIS_LIVR'];
					 
				$prixRevient = $prixAchatHT 
							 + $frais_livr
							 + $aerien
							 + $octroiMer;
							 
				$margeNette = $prixRevient * ( (float)$_POST['percentmarge'] / 100 ) ; // 25% marge nette à paramétrer, ici valeur par défaut
				
				$TVA = ( $prixRevient + $margeNette ) * ((float)$_POST['percentTVA'] / 100); //TVA 8,50 % à paramétrer plus tard, ici valeur par défaut
				
				
				$prixVenteLivre = $prixRevient + $margeNette + $TVA ;
				
				$coeff = $prixVenteLivre / $prixAchatHT ;  
				
				$product['PRIX_LIVR'] = 		$prixRevient;
				$product['OM'] = 				$octroiMer;
				$product['PERCENT_MARGE'] = 	(float)$_POST['percentmarge'] / 100 ;
				$product['PERCENT_TVA'] = 		(float)$_POST['percentTVA'] / 100 ;
				$product['COEF'] = 				$coeff ;
				$product['PRIX_FINAL'] = 		$prixVenteLivre ;
				$product['COEF'] 			= 	$coeff ;
				$product['RATIO_RUN_FR'] 	= 	$prixVenteLivre / $product['PRIX_METRO'];
				$product['PRIX_LIVRAISON'] 	= 	$aerien + $frais_livr;
				$product['RATIO_TRANSPORT'] = 	$product['PRIX_LIVRAISON'] / $prixAchatHT;
				
				$product['QUANTITY'] = $db->getQuantityProdGr($_POST['id'])	;
							
				$db -> updateValCalculProductGrossiste ($product);
				
				
				//Vérifier si différent de la catégorie???
				if(((float)$_POST['percentOctrmer'] / 100) != 	(float)SystemParams::getParam('grossiste*percentOM')) $spec['OM_PROD'] = (float)$_POST['percentOctrmer'] / 100 ;
				if(((float)$_POST['percentTVA'] / 100) != 		(float)SystemParams::getParam('grossiste*percentTVA')) $spec['TVA_PROD'] =  (float)$_POST['percentTVA'] / 100 ;
				if(((float)$_POST['percentmarge'] / 100) != 	(float)SystemParams::getParam('grossiste*percentMarge')) $spec['MARGE_PROD'] = (float)$_POST['percentmarge'] / 100 ;
				$spec['ID_PRODUCT'] = $_POST['id'] ;
				$db -> setSpecificValProdGrossiste($spec);
				
				//MAJ SI PRODUIT EN VENTE
				if($sku_av = $db -> getProdAvahisAssocieGrossiste($product['ID_PRODUCT'])) {
					if($db -> getStockPriceEcom($sku_av['EAN'], 24)){
						$db -> addRowStockPrixProdVenteAv ($product);
					}	
				}

			break;

			
			case '/saveallmodifventeav':
				$dispatch = true;
				
				$db = new BusinessSoukeoGrossisteModel();
				
				$list = $_POST['prod'];

				$i = 0 ;
				
				foreach ($list as $key => $value) {
					
					//Vérification i chaine non vide
					$value['percentOctrmer'] == "" ? $value['percentOctrmer'] = 0 	: $value['percentOctrmer'] ;
					$value['percentTVA']	 == "" ? $value['percentTVA']     = 0 	: $value['percentTVA'] ;
					$value['percentmarge']   == "" ? $value['percentmarge']   = 0 	: $value['percentmarge'] ;
					
					//On récupère le produit grace à l'ID obtenu
					//$product = $db -> getProduct("", $value['id']);
					//$product = $product[0];
					$product = $db -> getPrixCalcul($value['id']);
					$product = $product[0];
					
					//Série de calculs
					$prixAchatHT = $product['PRIX_ACHAT'];				
					$poidsVolum = $product['POIDS'];
					$prixKilo = $prixAchatHT / $poidsVolum;			
													
					$aerien = (float)SystemParams::getParam('grossiste*aerienchrono') * (float)$poidsVolum;
					

					$frais_livr = $product['FRAIS_LIVR'];
									
					$octroiMer = ( (float)$value['percentOctrmer'] / 100 ) * ($prixAchatHT + $aerien);
					
					$prixRevient = $prixAchatHT 
							 + $frais_livr
							 + $aerien
							 + $octroiMer;
							 
								 
					$margeNette = $prixRevient * ( (float)$value['percentmarge'] / 100 ) ; 
					
					$TVA = ( $prixRevient + $margeNette ) * ((float)$value['percentTVA'] / 100);
					
					$prixVenteLivre = $prixRevient + $margeNette + $TVA ;
					$coeff = $prixVenteLivre / $prixAchatHT ;  
					
					$product['PRIX_LIVR'] = 		$prixRevient;
					$product['OM'] = 				$octroiMer;
					$product['PERCENT_MARGE'] = 	(float)$value['percentmarge'] / 100 ;
					$product['PERCENT_TVA'] = 		(float)$value['percentTVA'] / 100 ;
					$product['COEF'] = 				$coeff ;
					$product['PRIX_FINAL'] = 		$prixVenteLivre ;
					$product['COEF'] 			= 	$coeff ;
					$product['RATIO_RUN_FR'] 	= 	$prixVenteLivre / $product['PRIX_METRO'];
					$product['PRIX_LIVRAISON'] 	= 	$aerien + $frais_livr;
					$product['RATIO_TRANSPORT'] = 	$product['PRIX_LIVRAISON'] / $prixAchatHT;
					
					$product['QUANTITY'] = $db->getQuantityProdGr($value['id'])	;
					
					//Mise à jour dans la BDD				
					$db -> updateValCalculProductGrossiste ($product);
					
					if(((float)$value['percentOctrmer'] / 100) != 	(float)SystemParams::getParam('grossiste*percentOM')) $spec['OM_PROD'] = (float)$value['percentOctrmer'] / 100 ;
					if(((float)$value['percentTVA'] / 100) != 		(float)SystemParams::getParam('grossiste*percentTVA')) $spec['TVA_PROD'] =  (float)$value['percentTVA'] / 100 ;
					if(((float)$value['percentmarge'] / 100) != 	(float)SystemParams::getParam('grossiste*percentMarge')) $spec['MARGE_PROD'] = (float)$value['percentmarge'] / 100 ;
					$spec['ID_PRODUCT'] = $_POST['id'] ;
					$db -> setSpecificValProdGrossiste($spec);
					
					//MAJ SI PRODUIT EN VENTE
					if($sku_av = $db -> getProdAvahisAssocieGrossiste($product['ID_PRODUCT'])) {
						if($db -> getStockPriceEcom($sku_av['EAN'], 24)){
							$db -> addRowStockPrixProdVenteAv ($product);
						}	
					}
					$i++;
				}

				$out = $i . " produits mis à jour !";
			break;

			
			case '/getallselectedventesav':
				$dispatch = true;
				if(!empty($_SESSION['selectedProdsGr']) && $_SESSION['selectedProdsGr'] != null){
					$json = json_encode($_SESSION['selectedProdsGr']) ;
					$out = $json;	
				} 
			break;
            
            case '/getproduct':
	           $dispatch = true;
               
               $product = $catalogProductManager -> getBy(array("id_produit" => $_POST['id_produit']));
               
               $out = "<a href='/app.php/catalogue/produit/fiche?id=".$product->getId_produit()."'>".$product->getName()."</a>";
               //$out = $view -> getProductThumbnail($product); 
                
	        break;
			
			case '/sellselectionventeav':
				$db = new BusinessSoukeoGrossisteModel();
				$dispatch = true;
				if(!empty($_SESSION['selectedProdsGr']) && $_SESSION['selectedProdsGr'] != null){
					
					if($_GET['a'] == "undo"){
						
						foreach ($_SESSION['selectedProdsGr'] as $id) {
							
							$rs = $db -> getProduct("", $id);
							$prodToSell = $rs[0];
														
							if($prodToSell['ACTIVE'] == 1 && $prodToSell['AJOUTE'] == 1 && $prodToSell['refus'] == 0){
								//AddRow
								//$prodToSell['QUANTITY'] = 0;
								
								$log = $db ->deleteVenteAv($prodToSell);
								
								if($log == 1)
									$add++;
								elseif ($log == 2)
									$updt++;
								elseif($log == 0)
									$err++;
							}
						}
						$out .= "" . $add. " produit(s) sélectionné(s) ont été retirés de vos ventes !";
					}
					else{
						$add 	   = 0;
						$updt 	   = 0;
						$err 	   = 0;
						$errStock  = 0;
						$dejavendu = 0;
						$errPoids  = 0;
						$nonAssoc  = 0;
						
						foreach ($_SESSION['selectedProdsGr'] as $id) {
							
							$rs = $db -> getProduct("", $id);
							$prodToSell = $rs[0];
							
							$prixProd = $db ->getPrixCalcul($id);
							$prixProd = $prixProd[0];
							$prixProd['QUANTITY'] = $prodToSell['QUANTITY'];
							
							if($prodToSell['ACTIVE'] == 1 && $prodToSell['AJOUTE'] == 1 && $prodToSell['refus'] == 0){
								//AddRow
								if((float)$prodToSell['WEIGHT'] < 30 ){
									if($prodToSell['QUANTITY'] >= 5){
										$log = $db ->addRowStockPrixProdVenteAv($prixProd);
										if($log == 1)
											$add++;
										elseif ($log == 2)
											$updt++;
										elseif($log == 0)
											$err++;
										elseif($log == 3)
											$nonAssoc++;
										elseif($log == 4)
											$dejavendu++;
									}
									else{
										$errStock ++;
									}
								}else{
									$errPoids ++;
								}
							}
						}
						$out .= "<p>Sur " . count($_SESSION['selectedProdsGr']). " produit(s) sélectionné(s) : </p>"
								."<b>".$add."</b> ajouté(s) à vos ventes !<br/>"
								."<b>".$updt."</b> produit(s) déjà en vente mis à jour !<br/>"
								."<b>".$err."</b> erreur(s).<br/>"
								."<b>".$errStock."</b> produits avec stock inférieur à 5 non vendu !<br/>"
								."<b>".$nonAssoc."</b> produits non associés  !<br/>"
								."<b>".$errPoids."</b> produits avec poids supérieur à 30 !<br/>"
								."<b>".$dejavendu."</b> annulé car déja vendu par un commerçant<br/>";
					}
					
				}else{
					$out = "Vous n'avez séléctionné aucun produit";
				}	
			break;
			
			
			
			case '/setactifgrossiste':
				$dispatch = true;
				foreach($_POST['id'] as $id_grossiste){
					
					if( $skf -> setGrossisteActiveValue($id_grossiste, 1) ){
						$mess .= "Grossiste $id_grossiste Activé ! <br/>";	
						$mess .= $skf -> setProductGrossisteActiveValue($id_grossiste, 1) . " produits activés !<br/>";
						$mess .= "La remise en vente ne se fera qu'au prochain batch de mise en vente.";
					}else{
						$mess .= "Grossiste $id_grossiste déja actif ! <br/>";
					}
					
					$mess .= "<br/>";
				}
				
				$out = $mess;	
			break;
			
			
			case '/setinactifgrossiste':
				$dispatch = true;
				foreach($_POST['id'] as $id_grossiste){
					
					if( $skf -> setGrossisteActiveValue($id_grossiste, 0) ){
						$mess .= "Grossiste $id_grossiste désactivé ! <br/>";
						$mess .= $skf -> setProductGrossisteActiveValue($id_grossiste, 0) . " produits désactivés !<br/>";
						$mess .= $skf -> setQuantityProductGrossisteZero($id_grossiste) . " produits en ventes passés à quantité 0 !<br/>";
					}else{
						$mess .= "Grossiste $id_grossiste déja désactivé ! <br/>";
					}
					
					$mess .= "<br/>";
				}
				
				$out = $mess;	
			break;
			
			
			case '/setautoassoc':
				$dispatch = true;
				foreach($_POST['categ'] as $cat){
					
					if( true ){
						$mess .= "Catégorie $cat sera auto-associée ! <br/>";
						$skf -> setAutoAssoc($cat, $_POST['grossiste_id'], 1);
					}else{
						$mess .= "Catégorie $cat déja auto-associée ! <br/>";
					}
					$mess .= "<br/>";
				}
				
				$out = $mess;	
			break;
			
			
			case '/cancelautoassoc':
				$dispatch = true;
				foreach($_POST['categ'] as $cat){
					
					if( true ){
						$mess .= "Catégorie $cat ne sera plus auto-associée ! <br/>";
						$skf -> setAutoAssoc($cat, $_POST['grossiste_id'], 0);
					}else{
						$mess .= "Catégorie $cat n'est déja plus auto-associée ! <br/>";
					}
					$mess .= "<br/>";
				}
				$out = $mess;	
			break;
			
			
			case '/valideditpoidscat':
				$dispatch = true;
				
				$skf -> setPoidsForCat($_POST['grossiste_id'], $_POST['cat'], $_POST['poids']);	
			break;
			
			
			//Récupère information pour association d'un produit ecommercant
			case '/associateproduct' :
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix Model BDD
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//Id du produit à associer
				$idProduct = $_POST['id'];

				if($_GET['a']=="remove"){
					unset($_SESSION[$typev]['productToAssoc']);
					$out = "Nom du produit";
				}elseif($_GET['a']=="unassoc"){
					if($typev == "gr"){
						$skf -> removeAssocGr($_SESSION[$typev]['vendor'], $idProduct);
						$_SESSION[$typev]['nbAssoc'] --;
						$out = "Suppression de l'association effectuée.";
					}else{
						$skf -> removeAssoc($_SESSION[$typev]['vendor'], $idProduct);
						$_SESSION[$typev]['nbAssoc'] --;
						$out = "Suppression de l'association effectuée.";	
					}
					
				}else{		
					//Récupère les information entière du produit à associer
					$prod = $db -> getProduct($_SESSION[$typev]['vendor'], $idProduct);
					$prod = $prod[0];
					//Mise en session du produit à associer
					$_SESSION[$typev]['productToAssoc'] = $prod ;
					
					$out = $prod['NAME_PRODUCT'];
				}
			break;
			
			
			//Récupère information pour association ou bien désassiocation d'un produit avahis 
			case '/associateproductav' :
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Association ANNULEE
				if($_GET['a']=="remove"){
					unset($_SESSION[$typev]['productToAssocAv']);
					$out = "Nom du produit";
					
				//Association
				}else{
					//Id du produit à associer
					$idProduct = $_POST['id'];
					
					//Récupère les information entière du produit à associer
					$prod = $skf -> getProduct($idProduct);
					
					//Mise en session du produit à associer
					$_SESSION[$typev]['productToAssocAv'] = $prod ;
					$out = $prod['name'];
				}
			break;
			
			
			
			//Va placer un flag un refus sur un produit ou bien annule un refus de produit 
			//Il y a renvoi vers Ajax de la catégorie en cours pour mettre à jour l'affichage et les
			//Arbres de navigation qui affiche le nombre de prods refusés etc 
			case '/refuseproduct':
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix BDD
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr")$dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//On a reçu en POST l'id du produit
				$idProduct = $_POST['id'];
				
				//Si on veux annuler le refus on a recu une action en GET	
				if($_GET['a']=="cancel"){
					$db -> setRefusProduct($_SESSION[$typev]['vendor'], $idProduct, 0);
					$_SESSION[$typev]['nbRefused'] -- ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];
					
				//On veux refuser le produit
				}else{	
					//Récupère les information entière du produit à associer
					$db -> setRefusProduct($_SESSION[$typev]['vendor'], $idProduct, 1);
					$_SESSION[$typev]['nbRefused'] ++ ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];;
				}
			break;
			
			
			//Permet de refuser tous les produits
			case '/refuseallselectedproduct':
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix BDD
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr")$dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//on récupère tous les ids des produits séléctionnés pour récupèrer leur infos
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					
					$db -> setRefusProduct($_SESSION[$typev]['vendor'], $id, 1);
					$_SESSION[$typev]['nbRefused'] ++ ;

				} 
				
				//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
				$out = $_SESSION[$typev]['catAv'];;
				
			break;
			
			
			//Va placer un flag delete sur un produit ou bien annule une supprésion de produit 
			//Arbres de navigation qui affiche le nombre de prods supprimé etc 
			case '/supproduct':
				
				$dispatch = true;
				$typev = "avaGP";
				
				//Choix BDD
				$dbClass = "BusinessSoukeoModel";
				$db = new $dbClass();
				
				//On a reçu en POST l'id du produit
				$idProduct = $_POST['id'];
				
				//Si on veux annuler la suppresion on a recu une action en GET	
				if($_GET['a']=="cancel"){
					$db -> setSupProduct($idProduct, 0);
					$_SESSION[$typev]['nbDeleted'] -- ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];
					
				//On veux refuser le produit
				}else{	
					//Récupère les information entière du produit à associer
					$db -> setSupProduct($idProduct, 1);
					$_SESSION[$typev]['nbDeleted'] ++ ;
					
					//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod refusés
					$out = $_SESSION[$typev]['catAv'];;
				}
			break;	
			
			
			//Permet de supprimmer tous les produits sélectionnés
			//Permet de supprimmer tous les produits sélectionnés
			case '/supallselectedproduct':
				$dispatch = true;
				$typev = "avaGP";
				
				$db = new BusinessSoukeoModel();

				//on récupère tous les ids des produits séléctionnés
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
						
					$db -> setSupProduct($id, 1);
					$_SESSION[$typev]['nbDeleted'] ++ ;

				}
				
				//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod supprimés
				$out = $_SESSION[$typev]['catAv'];;	
			break;
			
			
			//Permet de dé-supprimmer tous les produits sélectionnés
			case '/unsupallselectedproduct':
				$dispatch = true;
				$typev = "avaGP";
				
				$db = new BusinessSoukeoModel();
				
				//on récupère tous les ids des produits séléctionnés 
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
						
					$db -> setSupProduct($id, 0);
					$_SESSION[$typev]['nbDeleted'] ++ ;

				}
				
				//Renvoie de la catégorie pour réaffichage de la liste cat avec MAJ du nombre de prod désupprimé
				$out = $_SESSION[$typev]['catAv'];;
			break;
			
			
			//Appel au model permettant de valider l'association entre un produit avahis et un produit ecommercant
			case '/validassoc':
			
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Requete d'insertion
				$skf -> assocProduct($_POST['idEcom'], $_POST['idAv'], $_SESSION[$typev]['vendor']) ;
				$_SESSION[$typev]['nbAssoc'] ++ ;
				$out = "Les produits ".$_POST['idEcom']." et ".$_POST['idAv']. " ont bien été associés !";	
			break;
			
			
			//Validation des préférences mapping pour les champs statiques
			case '/validmodifcateg': 
				$dispatch = true;
				
				$typev = "avaGP";
				$id_produit = $_POST['caracId'][0];

				//$longeurDeTable = strlen($_POST['caracId'][1]);
				$longeurDeTable = 0 ;
				foreach ($_POST['caracId'] as $caracts){
					$longeurDeTable ++;
					
				}
					
				if(isset($id_produit)&&$longeurDeTable>1){// si une catégorie a été coché 
					
					$cat_id =  array_pop ($_POST['caracId']);
										
					
					$cat_id = $cat_id['catId'];					
					$id_produit = $id_produit['idProduit'] ;
					$skf -> updateCatProduct($id_produit,$cat_id) ? $mess = "Catégorie id sauvegarder" : $mess = "requéte sql a échoué" ;
					//$mess = "Catégorie id sauvegarder";
					
					$product = $catalogProductManager -> getById(array("id_produit"=> $id_produit));
                    
                    $pa_list = $product -> getAllProductAttributes();
                    
                    foreach ($pa_list as $pa) {
                        
                        if($pa->getActive()){
                            
                            $old_attr = $attributManager -> getById(array("id_attribut" => $pa->getId_attribut()));
                            
                            if($attr = $attributManager -> getBy(array("code_attr" => $old_attr->getCode_attr(),
                                                                         "cat_id" => $cat_id)) ){
                                
                            }
                            else{
                                $attr = new BusinessAttribut(array("code_attr" => $old_attr->getCode_attr(),
                                                                   "cat_id" => $cat_id,
                                                                   "label_attr" => $old_attr->getLabel_attr(),
                                                                   "is_filterable" => 0));
                                                                   
                                $attributManager -> save($attr);
                            }
                            
                            $clone_pa = clone $pa;
                            
                            $clone_pa -> setId_attribut($attr->getId_attribut());
                            $productAttributManager -> save($clone_pa);
                            $productAttributManager -> delete($pa);
                            
                            //Nettoyage 
                            if($old_attr -> getCountProdsWithThis () == 0 ){
                                $attributManager -> delete($old_attr);
                            }
                        }
                    }
				}
				else{//sinon
					$mess = "Pas de catégorie sélectionné" ;
				} 
				echo $mess ;
				unset($_POST['caracId']);

			break;
			
			
			//Validation des préférences pour la modification de la catégorie GP
			case '/validallmodifcateg':
				$dispatch = true;
				
				$typev = "avaGP";
				$id_produits = $_SESSION[$typev]['selectedProds'] ;
				
				$cat_id =  array_pop ($_POST['caracId']);
				$cat_id = $cat_id['catId'];
								
				
				if(isset($_POST['caracId'])){// si une catégorie a été coché 
					foreach($id_produits as $id_produit){

						$skf -> updateCatProduct($id_produit,$cat_id) ? $mess = "Catégorie id sauvegarder" : $mess = "requéte sql a échoué" ;
                        
                        //gestion attributs
                        $product = $catalogProductManager -> getById(array("id_produit"=> $id_produit));
                    
                        $pa_list = $product -> getAllProductAttributes();
                        
                        foreach ($pa_list as $pa) {
                            
                            if($pa->getActive()){
                                
                                $old_attr = $attributManager -> getById(array("id_attribut" => $pa->getId_attribut()));
                                
                                if($attr = $attributManager -> getBy(array("code_attr" => $old_attr->getCode_attr(),
                                                                             "cat_id" => $cat_id)) ){
                                    
                                }
                                else{
                                    $attr = new BusinessAttribut(array("code_attr" => $old_attr->getCode_attr(),
                                                                       "cat_id" => $cat_id,
                                                                       "label_attr" => $old_attr->getLabel_attr(),
                                                                       "is_filterable" => 0 ));
                                                                       
                                    $attributManager -> save($attr);
                                }
                                $clone_pa = clone $pa;
                                
                                $clone_pa -> setId_attribut($attr->getId_attribut());
                                $productAttributManager -> save($clone_pa);
                                $productAttributManager -> delete($pa);
                                
                                //Nettoyage 
                                if($old_attr -> getCountProdsWithThis () == 0 ){
                                    $attributManager -> delete($old_attr);
                                }
                            }
                        }
					}
					$_SESSION[$typev]['selectedProds'] = null;
				}
				else{//sinon
					$mess = "Pas de catégorie sélectionné" ;
				} 
				echo $mess ;
				unset($_POST['caracId']);

			break;
			
						
			//Validation des préférences mapping pour les champs statiques
			case '/validprefcateg':
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				$cat_id = $_POST['staticFields'][0]['categories'];
				$vendor_id = $_POST['staticFields'][0]['vendor_id'];
				$cat_ecom = trim($_POST['staticFields'][0]['cat_ecom']);
				
				unset($_POST['vendor_id']);
				unset($_POST['categories']);
				unset($_POST['cat_ecom']);
				
				$skf -> mappingCategStaticField($vendor_id, $cat_id, $cat_ecom, $_POST['staticFields'][0]);
				
				//ATTRIBUTS DYNAMIQUES
				foreach($_POST['dynaFields'] as $attr){
					//Les id des attributs ajoutés par l'utilisateur sont de la forme new_labelattribut
					//Les id déja en base sont simplement des numéros
					$id_attr_av = explode("_", $attr['id_attr_av']);
					
					if($id_attr_av[0] != "new" && $id_attr_av[1] != "null"){
						
						//insert mapping attribut product
						$skf -> mappingCategDynaField($vendor_id, $cat_id, $cat_ecom, trim($attr['id_attr_av']), $attr['id_attr_ecom']);
						
					}elseif($id_attr_av[0] == "new"){
						$skf -> prepAttribut();
						//Si c'est un attribut ajouté pour le moment pas de solution car pour l'instant
						//Un attribut n'est pas directement lié à une catégorie. Un attribut est lié à un produit
						//Qui possède cet attribut et qui est dans une catégorie.
						$new_id_attr_av = $skf -> addAttribut(array('cat_id' => $cat_id, 'label_attr' => $id_attr_av[1], 'code_attr' => strtoupper($id_attr_av[1])));
						
						$skf -> mappingCategDynaField($vendor_id, $cat_id, $cat_ecom, $new_id_attr_av, $attr['id_attr_ecom']);
					}
				}
				
				$out = "Préférences pour ".$cat_ecom."  sauvegardées !";
			break;
			
			
			//Validation de l'ajout d'un produit ecommercant vers un produit Avahis
			case '/validaddproduct':
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				unset($_POST['staticFields']['vendor_id']);
				
				if($typev == "ecom"){
					$skf -> prepInsertSfkCp();
                    
					//l'ajout du produit nous renvoie le lastInsertId correspondant à l'ID du nouveau produit
					$id_product = $skf -> addRowCatProduct($_POST['staticFields'][0], $_SESSION[$typev]['vendor']);
					
					if($id_product == 0){
						$id_product = $skf -> getProdAvAssoc($_SESSION[$typev]['vendor'], $_SESSION[$typev]['prodToAdd']['ID_PRODUCT']);
						if($id_product == 0){
							$id_product = $skf -> getProdByName($_SESSION[$typev]['prodToAdd']['NAME_PRODUCT']);

						} 
					}
					
					if( !empty($_POST['dynaFields']) && $id_product) {
						//ATTRIBUTS DYNAMIQUES
						foreach($_POST['dynaFields'] as $attr){
							//Les id des attributs ajoutés par l'utilisateur sont de la forme new_labelattribut
							//Les id déja en base sont simplement des numéros
							$id = explode("_", $attr['id_attr_av']);
							
							if($id[0] != "new" && $id[1] != null && $attr['value'] != "--"){
								
								//insert association attribut product
								$skf -> addAttributProduct(trim($id[1]), $id_product, $attr['value']);
							}
							//Nouveaux attributs à entrer
							elseif($id[0] == "new"){
								
								//insert nouvel attribut
								$skf -> prepAttribut();
								$id_attr = $skf -> addAttribut($V = array ( 'cat_id' => $_POST['staticFields'][0]['categories'],
															   				'code_attr' => strtoupper($id[1]),
															   				'label_attr' => $id[1]));
								//insert association attribut product
								$skf -> addAttributProduct($id_attr, $id_product, $attr['value']);
								$tmp .= "Création nouvel attribut ".$id[1]."\n";
							}
							//Produit qui n'avait pas d'attribut avant et l'id n'est alors pas composé d'underscore
							elseif(!isset($id[1]) && $id != ""){
								//insert association attribut product
								$skf -> addAttributProduct(trim($attr['id_attr_av']), $id_product, $attr['value']);
							}
						}
						//On en profite pour associer directement le produit ecommercant avec le produit avahis qu'il vient de générer
						$skf -> assocProduct($_SESSION[$typev]['prodToAdd']['ID_PRODUCT'], $id_product, $_SESSION[$typev]['vendor']) ; 
						$out = "Produit ajouté ! ".$tmp;
					}
					elseif($id_product){
						$skf -> assocProduct($_SESSION[$typev]['prodToAdd']['ID_PRODUCT'], $id_product, $_SESSION[$typev]['vendor']) ; 
						$out = "Produit ajouté !";	
					}
					elseif(!$_POST['staticFields'][0]['sku']){
						$out = "Aucun SKU entré !";
					}
					else{
						$out = "SKU déja existant produit mis à jour!" . $tmp;
					}		
				}
				elseif ($typev == "gr") {
					$db = new BusinessSoukeoGrossisteModel();
					$skf -> prepInsertSfkCp();
					//l'ajout du produit nous renvoie le lastInsertId correspondant à l'ID du nouveau produit
					$id_product = $skf -> addRowCatProduct($_POST['staticFields'][0], 24);
					
					if(!empty($_POST['dynaFields']) && $id_product){
						//ATTRIBUTS DYNAMIQUES
						foreach($_POST['dynaFields'] as $attr){
							//Les id des attributs ajoutés par l'utilisateur sont de la forme new_labelattribut
							//Les id déja en base sont simplement des numéros
							$id = explode("_", $attr['id_attr_av']);
							    
							if($id[0] != "new" && $id[1] != null && $attr['value'] != "--"){
								
								//insert association attribut product
								$skf -> addAttributProduct(trim($id[1]), $id_product, $attr['value']);
								
							}
							//Nouveaux attributs à entrer
							elseif($id[0] == "new"){
								
								//insert nouvel attribut
								$skf -> prepAttribut();
								$id_attr = $skf -> addAttribut($V = array ( 'cat_id' => $_POST['staticFields'][0]['categories'],
															   				'code_attr' => strtoupper($id[1]),
															   				'label_attr' => $id[1]));
								//insert association attribut product
								$skf -> addAttributProduct($id_attr, $id_product, $attr['value']);
								$tmp .= "Création nouvel attribut ".$id[1]."\n";
							}
							//Produit qui n'avait pas d'attribut avant et l'id n'est alors pas composé d'underscore
							elseif(!isset($id[1]) && $id != ""){
								//insert association attribut product
								$skf -> addAttributProduct(trim($attr['id_attr_av']), $id_product, $attr['value']);
							}
						}
						$db -> setAjouteProduct($_SESSION[$typev]['vendor'], $_SESSION[$typev]['prodToAdd']['ID_PRODUCT'], 1);
						$db -> assocProductGrossiste($_SESSION[$typev]['prodToAdd']['ID_PRODUCT'], $id_product, $_SESSION[$typev]['vendor']) ; 
						$out = "Produit ajouté !\nAttention ! Cette fonction est temporaire (inachevée niveau attributs dynamiques) \nveuillez préferer ajouter tout ou bien la sélection de produits";	
					}
					elseif($id_product){
						$db -> assocProductGrossiste($_SESSION[$typev]['prodToAdd']['ID_PRODUCT'], $id_product, $_SESSION[$typev]['vendor']) ; 
						$out = "Produit ajouté !".$tmp;	
					}
					elseif(!$_POST['staticFields'][0]['sku']){
						$out = "Aucun SKU entré !";
					}
					else{
						$out = "SKU déja existant produit mis à jour!" . $tmp;
					}
				}
			break;
			
			
			
			/*
			 * réponse a l'appel ajax lors du click sur ajouter tous les produits dans le contexte des grossistes 
			 * ou bien celui des e-commercants habituels. Il y a création en masse de fiche produit avahis avec création
			 * d'attributs et relation attr-produits à la volée ou bien en fonction du mapping enregistré pour la catégorie
			 * du produit concerné
			 */
			case '/addallproducttoav':
				$dispatch = true;
				
				//Cas des grossistes
				//La fonction addRowCatProduct est redéfinie dans BusinessSoukeoGrossisteModel et s'occupe de l'insertion
				//Des attributs, le mapping est statiques et commun à tous les produits du grossiste (csv statique) 
				if($_SESSION['typeVendor'] == "Grossiste"){
					$dbClass = "BusinessSoukeoGrossisteModel";
					$db = new $dbClass ();
					
					$db -> prepInsertSfkCp();
					if(!empty($_POST['cat_id_assoc'])){
						$i = 0;
						foreach($_SESSION['gr']['listProdEcom'] as $prod){
							//Si c'et bien insertion d'un nouveau produit
							$count_sku = $skf->checkSkuUnique($prod['sku']);
							if( $count_sku != 1 && $idNewProduct = $db -> addRowCatProduct($prod, $_POST['cat_id_assoc'])){
								$i++;
								//Pour les autres grossistes insertion d'attributs
								if($prod['GROSSISTE_ID'] != '1'){
									//Récupération de tous les attributs pour ce produit
									$attrVal = $db -> getAttrValueGrossiste($prod['ID_PRODUCT']);
									//Pour chaque couple valeur/attribut
									$db -> prepAttribut();
									foreach ($attrVal as $couple) {
										$idAttrAvahis = $db -> getAttrByCatAndCode($couple['CODE_ATTR'], $_POST['cat_id_assoc']);
										//SI l'attribut n'existe pas dans avahis
										if($idAttrAvahis == false){
											//Création nouvel attribut avahis
											$idAttrAvahis = $db -> addAttribut($attr = array( "cat_id" => $_POST['cat_id_assoc'], 
																								"code_attr" => $couple['CODE_ATTR'], 
																								"label_attr" => str_replace("_", " ",strtolower($couple['CODE_ATTR']))));
										}
										//Ajout de la relation produit attribut avahis
										if($couple['VALUE'] != "" && $couple['VALUE'] != "NULL" && $couple['VALUE'] != "-")
											$db -> addAttributProduct($idAttrAvahis, $idNewProduct, $couple['VALUE']);
									}
								}				
							//Si ça ne fonctionne pas c'est que c'est un doublon déjà présent							
							}else{
								$nameCat = $skf -> getStrCateg($_POST['cat_id_assoc']);
								$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> SKU déjà présent OU bien aucun SKU n'est entré</b><br/>";

							}
						}
						$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés !";
						$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
						$json = array ('message' => $message, 'id' => $skf ->getCatIdTree($_POST['cat_id_assoc']));
						$out = json_encode($json);
					}
					
				}
				//Cas des ecommercants
				else{
					$dbClass = "BusinessSoukeo".$_SESSION['ecom']['tech']."Model";
					$db = new $dbClass ();				
					$i=0;
					$j=0;
					//Tous les produits de la session actuelle de produits à traiter
					foreach ($_SESSION['ecom']['listProdEcom'] as $prod) {
						
						//On va chercher la catégorie avahis associée pour la categ de CE produit
						if($cat_av_assoc = $skf -> isCatAssoc($_SESSION['ecom']['vendor'], $prod['CATEGORY'])){
							
							//On va maintenant chercher le mapping pour cette combinaison
							if($mapping = $skf -> getStaticFieldsForVendor($_SESSION['ecom']['vendor'], $cat_av_assoc, $prod['CATEGORY'])){
							
								$mappingStatic = array();
								
								foreach ($mapping as $field) {
									$mappingStatic[$field['field_avahis']] = $field['field_ecom'];
								}
								
								//Préparation requete insert fk_catalogue_product
								$skf ->prepInsertSfkCp();
								
                                $countSku = $skf->checkSkuUnique($prod[$mappingStatic['sku']]);
								//INSERTION NOUVEAU PRODUIT ET RECUP DE SON ID
								if( $countSku != 1 && $idNewProduct = $skf -> addProductFromMappingFields($mappingStatic, $prod, $_SESSION['ecom']['vendor'])){
									$skf -> assocProduct($prod['ID_PRODUCT'], $idNewProduct, $_SESSION['ecom']['vendor']);
									$i++;
									
									////////////////////////////////////////////////////////////////////////////////////////////////
									//LES ATTRIBUTS
									//Récupération de tout les attributs ecommercant de ce produit
									if($listeAttr = $db -> getAllAttrFromProduct($prod['ID_PRODUCT'], $_SESSION['ecom']['vendor'])){
										
										foreach ($listeAttr as $attr) {
											//On va chercher si un idAttribut avahis a été mappé pour cet attribut
											if($idAttrAvahisMapped = $skf -> getMappingForAttrEcom($attr['ID_ATTR'], $_SESSION['ecom']['vendor'], $prod['CATEGORY'], $mappingStatic['categories'])){
												$skf -> addAttributProduct($idAttrAvahisMapped, $idNewProduct, $attr['VALUE']);	
												$j++;
											}
											//SINON ON AJOUTE DES ATTRIBUTS A LA VOLEE...
											else{
												
											}											
										}					
									}
								}
								else{
									$nameCat = $skf -> getStrCateg($cat_av_assoc);
									$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> SKU déjà existant OU bien aucun SKU n'est entré</b><br/>";
								}				
							}
						}
						else{
							$erreurs .= "<b>".$prod['NAME_PRODUCT']."</b> non inséré car sa catégorie <b>".$prod['CATEGORY']."</b> n'est pas associée !<br/>";
						}
					}
					$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés ! Et <b>".$j."</b> relations produits-attributs insérés !";
					$erreurs != "" ? $message .="<p><h3>Erreurs : </h3><b>Non associé : </b><br>".$erreurs : $message = $message;
					$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
					$json = array ('message' => $message, 'id' => array());
					$out = json_encode($json);	
				}	
			break;
			
			
			/*
			 * Permet d'ajouter dans avahis toute une selection de produits. Il y a gestion du log des produits bien inérés
			 * Gestion des doublon donc update de produits 
			 * Et gestion des erreurs de produits non ajoutés car leur catégorie n'est pas encore associée
			 * On précise lesquels de ces produits ont lancé une erreur et quelles catégories doivent être ajoutées.
			 * */ 
			case '/addallselectedproducttoav':
				$dispatch = true;
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){
				    $typev = "gr";
				    $dbClass = "BusinessSoukeoGrossisteModel";
				}
				//ECOMMERCANT	
				else{
				     $typev = "ecom";
				     $dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				}
				
				
				$db = new $dbClass ();				
				$i=0; //Insertion de produits
				$j=0; //Insertion de relations produit-attribut
				
				//on récupère tous les ids des produits séléctionnés pour récupèrer leur infos
				foreach ($_SESSION[$typev]['selectedProds'] as $id) {
					
					$prod = $db -> getProduct($_SESSION[$typev]['vendor'], $id);
					
					//tableau des produits séléctionnés qu'on va utiliser enssuite
					$selectProdList[] = $prod[0];
				}
				
				if($typev == "ecom"){
					//Tous les produits de la session actuelle de produits à traiter
					foreach ($selectProdList as $prod) {
						
						//On va chercher la catégorie avahis associée pour la categ de CE produit
						if($cat_av_assoc = $skf -> isCatAssoc($_SESSION[$typev]['vendor'], $prod['CATEGORY'])){
	
							//On va maintenant chercher le mapping pour cette combinaison
							if($mapping = $skf -> getStaticFieldsForVendor($_SESSION[$typev]['vendor'], $cat_av_assoc, $prod['CATEGORY'])){
							
								$mappingStatic = array();
								
								foreach ($mapping as $field) {
									$mappingStatic[$field['field_avahis']] = $field['field_ecom'];
								}
								
								$skf ->prepInsertSfkCp();
								
								//INSERTION NOUVEAU PRODUIT ET RECUP DE SON ID
								if($idNewProduct = $skf -> addProductFromMappingFields($mappingStatic, $prod, $_SESSION[$typev]['vendor'])){
									$skf -> assocProduct($prod['ID_PRODUCT'], $idNewProduct, $_SESSION[$typev]['vendor']);
									$i++;
									//Si l'id de ce produit existe bien on l'unset
									if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
					    				unset($_SESSION[$typev]['selectedProds'][$key]);
									}
									////////////////////////////////////////////////////////////////////////////////////////////////
									//LES ATTRIBUTS
									//Récupération de tout les attributs ecommercant de ce produit
									if($listeAttr = $db -> getAllAttrFromProduct($prod['ID_PRODUCT'], $_SESSION[$typev]['vendor'])){
										
										foreach ($listeAttr as $attr) {
											//On va chercher si un idAttribut avahis a été mappé pour cet attribut
											if($idAttrAvahisMapped = $skf -> getMappingForAttrEcom($attr['ID_ATTR'], $_SESSION[$typev]['vendor'], $prod['CATEGORY'], $mappingStatic['categories'])){
												$skf -> addAttributProduct($idAttrAvahisMapped, $idNewProduct, $attr['VALUE']);	
												$j++;
											}
											//SINON ON AJOUTE DES ATTRIBUTS A LA VOLEE...
											else{
												
											}											
										}
									}	
								//Si ça ne fonctionne pas c'est que c'est un doublon déjà présent							
								}else{
									$nameCat = $skf -> getStrCateg($cat_av_assoc);
									$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> est déjà ajouté dans <b>".substr($nameCat, 0, strlen($nameCat)-2)."</b><br/>";
									//Si l'id de ce produit existe bien on l'unset
									if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
					    				unset($_SESSION[$typev]['selectedProds'][$key]);
									}
								}
							}
						//Produits avec catégorie non associée
						}else{
							$erreurs .= "<b>".$prod['NAME_PRODUCT']."</b> non inséré car sa catégorie <b>".$prod['CATEGORY']."</b> n'est pas associée !<br/>";
						}
					}
					$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés ! Et <b>".$j."</b> relations produits-attributs insérés !";
					$erreurs != "" ? $message .="<p><h3>Erreurs : </h3><b>Non associé : </b><br>".$erreurs : $message = $message;
					$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
					$json = array ('message' => $message, 'id' => array());
					$out = json_encode($json);
					
				}elseif($typev == "gr"){
					$db -> prepInsertSfkCp();
					foreach ($selectProdList as $prod) {
						//On va chercher la catégorie avahis associée pour la categ de CE produit
						if($cat_av_assoc = $skf -> isCatAssocGross($_SESSION[$typev]['vendor'], $prod['CATEGORY'])){
							
							//Si on a bien crée un nouveau produit on a un idNewProduct
							if($idNewProduct = $db -> addRowCatProduct($prod, $cat_av_assoc)){
								$i++;
								//Pour les autres grossistes insertion d'attributs
								if($prod['GROSSISTE_ID'] != '1'){
									//Récupération de tous les attributs pour ce produit
									$attrVal = $db -> getAttrValueGrossiste($prod['ID_PRODUCT']);
									//Pour chaque couple valeur/attribut
									$db -> prepAttribut();
									foreach ($attrVal as $couple) {
										$idAttrAvahis = $db -> getAttrByCatAndCode($couple['CODE_ATTR'], $cat_av_assoc);
										//SI l'attribut n'existe pas dans avahis
										if($idAttrAvahis == false){
											//Création nouvel attribut avahis
											$idAttrAvahis = $db -> addAttribut($attr = array( "cat_id" => $cat_av_assoc, 
																								"code_attr" => $couple['CODE_ATTR'], 
																								"label_attr" => str_replace("_", " ",strtolower($couple['CODE_ATTR']))));
										}
										//Ajout de la relation produit attribut avahis
										if($couple['VALUE'] != "" && $couple['VALUE'] != "NULL" && $couple['VALUE'] != "-")
											$db -> addAttributProduct($idAttrAvahis, $idNewProduct, $couple['VALUE']);
									}
								}					
								//Si l'id de ce produit existe bien en selectedProds on l'unset
								if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
				    				unset($_SESSION[$typev]['selectedProds'][$key]);
								}
									
							//Si ça ne fonctionne pas c'est que c'est un doublon déjà présent							
							}else{
								$nameCat = $skf -> getStrCateg($cat_av_assoc);
								$erreursExist .= "<b>".$prod['NAME_PRODUCT']."</b> est déjà ajouté dans <b>".substr($nameCat, 0, strlen($nameCat)-2)."</b><br/>Ou bien l'EAN est NULL<br/>";
								
								//Si l'id de ce produit existe bien on l'unset
								if(($key = array_search($prod['ID_PRODUCT'], $_SESSION[$typev]['selectedProds'])) !== false) {
				    				unset($_SESSION[$typev]['selectedProds'][$key]);
								}
							}
						}else{
							$erreurs .= "<b>".$prod['NAME_PRODUCT']."</b> non inséré car sa catégorie <b>".$prod['CATEGORY']."</b> n'est pas associée !<br/>";
						}
					}
					//Gestion des erreurs et du log renvoyé en JSON pour être traité par Javascript ensuite
					$message = "<h3>Résultats : </h3><b>".$i."</b> produits insérés !";
					$erreurs != "" ? $message .="<p><h3>Erreurs : </h3><b>Non associé : </b><br>".$erreurs : $message = $message;
					$erreursExist != "" ? $message .= "<br/><b>Existe déjà:</b><br>".$erreursExist."</p>" : $message .= "</p>";
					$json = array ('message' => $message, 'id' => array());
					$out = json_encode($json);
				}	
			break;
			
			
			//permet de récupérer le mapping des champs static pour un ecommercant et sa catégorie
			//Afin de renvoyer les données dans un JSON
			case '/getstaticfields':
				$dispatch = true;
				$result = $skf -> getStaticFieldsForVendor($_POST['id_vendor'], $_POST['cat_id'], trim($_POST['cat_ecom']));
				
				$fields = array();
				foreach ($result as $row) {
					$fields[$row['field_avahis']] = $row['field_ecom']; 
				}
				
				$json  = json_encode($fields);
				
				$out = $json;
				
			break;
			
			
			//route ajax permettant de récupérer un json contenant toutes les catégories par label et id
			case '/getlistcateg':
				$dispatch=true;
				//Récupération de tou les label et id de catégorie distinctes
				$catList = $skf -> getAllCatByLabel ($_GET['label']);
					
				if(!empty($catList)){
					
					foreach ($catList as $cat) {
						
						$data[] = array("id" => $cat['cat_id'], "text" => $cat['cat_label']);
					}		
				}
				else {
					// 0 résultat
					$data[] = array("id"=>"0","text"=>"Pas de résultat..");
				} 
				//on encode le tableau en json pour le réutiliser dans le javascript
				echo json_encode($data);
			break;
            
            case '/getSelectProduct':
                $dispatch = true;
                ob_start();
                include ("./clients/popup_select_product/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;                
            break;
            
            case '/getListProdSelect2':
                $dispatch=true;
                //Récupération de tou les label et id de catégorie distinctes
                $prodList = $skf -> getAllProdByName ($_GET['label']);
                    
                if(!empty($prodList)){
                    
                    foreach ($prodList as $prod) {
                        
                        $data[] = array("id" => $prod['id_produit'], "text" => $prod['name'], "image" => $prod['image'], "shortdesc" => $prod['short_description']);
                    }       
                }
                else {
                    // 0 résultat
                    $data[] = array("id"=>"0","text"=>"Pas de résultat..");
                } 
                //on encode le tableau en json pour le réutiliser dans le javascript
                echo json_encode($data);                
            break;
			
			
			//route utilisée par le plugin select2 permettant d'obtenir une chaine d'id de catégorie
			//sous forme 123-4561-12345 représentant les id des catégories et sous catégories pour une cat donnée
			case '/validselect2':
				$dispatch = true;
				
				//On transforme un id = 1234 (Cartes graphiques) par exemple en 
				//45-625-1234 représentant Informatique > Composant > Cartes graphique
				$tmp = $skf -> getCatIdTree($_POST['cat_id']);
				
				//On renvoie la chaine séparée par des tirets à Ajax
				$out = $tmp;
			break;
			
			
			
			//action dans le popup d'association permettant de charger dynamiquement en ajax
			//différentes information produit en fonction du champ selectionné 
			case '/loadfield':
				 $dispatch = true;
				
				 //GROSSISTE
				 if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				 //Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				 //ECOMMERCANT	
				 else{ $typev = "ecom";}
				
				 //produit en cours d'ajout
				 $prod = $_SESSION[$typev]['prodToAdd'];
				 //champ sélectionné par l'utilisateur
				 $field = $_POST['field'];
				 //Renvoi des information pour le champ selectionné
				 $out = $prod[$field];	
			break;
			
			
			//Permet de charger les attributs Avahis et du produits ecommercant pour la bonne catégorie
			case '/findattrav':
				$dispatch = true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//Choix de la base de données
				$dbClass = "BusinessSoukeo".$_SESSION[$typev]['tech']."Model";
				if($typev == "gr") $dbClass = "BusinessSoukeoGrossisteModel";
				$db = new $dbClass();
				
				//ID catégorie Avahis qu'on a selectionné
				$cat_id = $_POST['cat_id'];
				
				//Produit de l'ecommercant
				$prod_id = $_POST['id_product'];
				
				//récup attributs du produit
				//Si l'id du produit est fourni on va chercher les attributs par rapport au produit
				if ($prod_id != "" && $prod_id != null){
					$attrProd = $db->getAllAttrFromProduct($prod_id, $_SESSION[$typev]['vendor']);
				}
				//Sinon on récupère tous les attributs présents dans la catégorie du vendeur
				else{
					$attrProd = $db -> getAllAttrFromVendorCateg($_SESSION[$typev]['cat'], $_SESSION[$typev]['vendor']);
				}
				//récup attributs avahis pour la catégorie
				$attrAvahis = $skf -> getAllAttrByProdFromCat($cat_id);
				
				if(empty($attrAvahis) | $attrAvahis == null){
					$attrAvahis = $skf -> getAllAttrDistFromCat($cat_id);
				}
				//Appel à businessView pour la mise en HTML des attributs ecommercant et avahis associés
				$out = $view -> showAttributsEcomAvPopup($attrProd, $attrAvahis, $_SESSION[$typev]['vendor'], $_SESSION[$typev]['cat'], $_POST['cat_id']);
			break;
			
			
			//Permet de changer l'onglet en cours A traiter / refusés / associés etc...
			case '/changecontext':
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				//On place dans la bonne session le context choisis obtenu en POST
				$_SESSION[$typev]['context'] = $_POST['context'];
			break;
			
			
			//Permet de changer l'onglet en cours A traiter / refusés / associés etc... GP
			case '/changecontextGP':
				$typev = "avaGP";
				//On place dans la bonne session le context choisis obtenu en POST
				$_SESSION[$typev]['context'] = $_POST['context'];	
			break;	
			
			
			case    "/catalogue/produit" :

				// on vat changer tous les données session utilisé que par gestion/produit telle que ['avaGP']
				$template = "listingAvahis";
				unset($_SESSION['ecom']);
				unset($_SESSION['typeVendor']);
				$_SESSION['typeVendor'] = "avahisGP";
				
				$_SESSION['avaGP']['context'] ="todo"; // affiché tous
				$_SESSION['avaGP']['gestion'] = 'produit'; // définit un paramétre d'affichage ici écran gestion/produit
				$ACCESS_key = "catalogue.produit..";
				
				$listProdDeleted = $skf -> getAllDeletedProds(); // on recherche tous les produits delete
				$_SESSION['avaGP']['listProdDeleted'] = $listProdDeleted;
				
				$nbDeleted = count($_SESSION['avaGP']['listProdDeleted']);  // on compte le nombre de produit supprimé
				$_SESSION['avaGP']['nbDeleted'] = $nbDeleted;				

				$listVendorAss = $skf -> getAllVendorAssoc(); //on recherche les vendeurs associés
				$_SESSION['avaGP']['listVendorAss'] = $listVendorAss ;
				
				if(!isset($_SESSION['avaGP']['catAvIdTree'])){
					$_SESSION['avaGP']['catAvIdTree'] = $skf -> getOneCateg();
				}
								
				$data = $skf->getSsCatByParentId(); // récupére l'architecture des catégories - enfant parent

				$catAv = $view->showcatGP($data); //  affiche les catégories paramétrage des catégories
				$view -> assign("catavGP", $catAv);
				$view -> assign("listProdDeleted", $listProdDeleted);
			break;

            case    "/catalogue/attributs" :

                // on vat changer tous les données session utilisé que par gestion/produit telle que ['avaGP']
                $template = "attributs";
                
                $ACCESS_key = "catalogue.attributs..";

            break;
            
            case '/getJsonUnivers':
                
                $dispatch = true;
                
                if($_GET['id'] == "#"){
                    $univers_list = $categorieManager -> getAllUnivers();    
                }else{
                    $univers_list = $categorieManager -> getAll(array("parent_id" => trim($_GET['id']), "cat_active" => 1));
                }
                
                
                $json = array();
                
                $html = "";
                
                foreach ($univers_list as $cat) {
                    
                    if($cat->hasChild()){
                        $children = true;
                    }
                    else{
                        $children = false;
                    }
                    
                    $json [] = array("id" => $cat->getCat_id(),
                                     "text" => truncateStr($cat->getCat_label(), 58),
                                     "type" => "root", 
                                     "children" => $children);
                }

                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');                
                header('Content-Type: application/json;charset:utf-8'); 
                $out = json_encode($json, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);    
            break;
            
            case '/getGridAttrCat':
                
	           $dispatch = true;
               
               if(isset($_POST['nb_items_page']) && !empty($_POST['nb_items_page'])){
                   $item_per_page = $_POST['nb_items_page'];
               }else{
                   $item_per_page = 10;
               }
               
               if(isset($_POST['current_page']) && !empty($_POST['current_page'])){
                   $curr_page = $_POST['current_page'];
               }else{
                   $curr_page = '0';
               }

               if(isset($_POST['sort_code_attr']) && !empty($_POST['sort_code_attr'])){
                   $sort_code_attr = $_POST['sort_code_attr'];
               }else{
                   $sort_code_attr = '';
               }
               
               $categorie = $categorieManager -> getById(array("cat_id" => $_POST['cat_id']));
               
               /****************** RAJOUT ********************/
               $categorie_childs = substr($skf -> getAllCatChildId($categorie -> getCat_id()), 0, -2); 
               
               
               /**********************************************/                 
               //$data = $skf -> getAllAttrForCat($_POST['cat_id'], $sort_code_attr); // OLD
               $data = $skf -> getAllAttrInCat($categorie_childs, $sort_code_attr);;
               
               foreach ($data as $attribut) {
                   $attribut -> setCurrent_cat_id($_POST['cat_id']);
               }
               
               if(isset($_POST['sort_nb_prods']) && $_POST['sort_nb_prods'] != ""){
                   
                    //usort($data, $_POST['sort_nb_prods']); // OLD   
                    
                    /****************** RAJOUT ********************/
                    usort($data, function($a, $b) use ($categorie_childs){
                       if ($a->getCountProdsInListCatWithThis($categorie_childs) == $b->getCountProdsInListCatWithThis($categorie_childs)) {
                            return 0;
                       }
                       if($_POST['sort_nb_prods'] == "cmpAsc")
                            return ($a->getCountProdsInListCatWithThis($categorie_childs) < $b->getCountProdsInListCatWithThis($categorie_childs)) ? -1 : 1;
                       elseif($_POST['sort_nb_prods'] == "cmpDesc")
                            return ($a->getCountProdsInListCatWithThis($categorie_childs) > $b->getCountProdsInListCatWithThis($categorie_childs)) ? -1 : 1;
                    });
                    /**********************************************/
                    $sort_nb_prods_class = $_POST['sort_nb_prods'];
               }
                
               
               $items_total = count($data) ;
               
               if($items_total > 0){ 
                   $paginationCount = floor($items_total / $item_per_page);
                   $paginationModCount= $items_total % $item_per_page;
                   
                   if(!empty($paginationModCount)){
                       $paginationCount++;
                   }
               }
               
               $data = array_slice($data, $item_per_page * $curr_page, $item_per_page);
               
               $json = array("html"        => $view -> showAttributsTable($data, $sort_nb_prods_class, $categorie->getCat_id(), $sort_code_attr, $categorie_childs),
                             "total_page"  => $paginationCount,
                             "total_items" => $items_total,
                             "cat_label"   => $categorie -> getCat_id()." - ".$categorie -> getCat_html(),
                             "current_cat_childs" => $categorie_childs);
                             
               $out = json_encode($json);
                
	        break;

            case '/embed_fiche_attribut':
            
                $dispatch = true;
                $item_per_page = 20;
                
                if(isset($_POST['curr_page']) && !empty($_POST['curr_page'])){
                    $curr_page = $_POST['curr_page'];
                }else{
                    $curr_page = '0';
                }
                
                if(isset($_POST['id_attribut']) && $_POST['id_attribut'] != "" && $_POST["current_cat_childs"] != ""){
                    
                    $attribut = $attributManager -> getBy(array("id_attribut" => $_POST['id_attribut']));
                    
                    $attribut -> setCurrent_cat_id($_POST['cat_id']);
                    
                    /********************* RAJOUT ******************/
                    $categorie_childs = $_POST["current_cat_childs"]; 
                    /**********************************************/
                    
                    if($attribut){
                        
                        //$prod_attr_list = $skf -> getAllProductAttrForCat($_POST['cat_id'], $attribut->getId_attribut()); //OLD    
                        $prod_attr_list = $skf -> getAllProductAttrInCatList($categorie_childs, $attribut->getId_attribut());
                    }
                    
                    $view -> assign("attribut" , $attribut);
                    
                    $product_list = array();
                    
                    $json = array("nom" => truncateStr(htmlentities($attribut->getLabel_attr()), 20) );
                    
                    //PAGINATION liste produit
                    $items_total = count($prod_attr_list); 
                    $paginationCount = floor($items_total / $item_per_page);
                    $paginationModCount= $items_total % $item_per_page;
               
                    if(!empty($paginationModCount)){
                        $paginationCount++;
                    }
                    
                    $json["total_page"] = $paginationCount;
                    $view -> assign("total_page" , $paginationCount);
                    $view -> assign("curr_page" , $curr_page);
                    $view -> assign("total_prods" , $items_total);
                    
                                            
                    $prod_attr_list = array_slice($prod_attr_list, $item_per_page*$curr_page, $item_per_page);
                    
                    if($prod_attr_list){
                        
                        foreach ($prod_attr_list as $prod_attr) {
                            $product_list[] = $catalogProductManager -> getById(array("id_produit" => $prod_attr ->getId_produit()));    
                        }
                        
                        $product_list_html = $view -> showProdListAttributs($product_list, $_POST['id_attribut']);
                        $view -> assign("product_list" , $product_list_html);
                        
                        //LISTE DES VALEURS
                        
                        //$values_list = $attribut -> getDistinctValuesForCat($_POST['cat_id']);
                        $values_list = $attribut -> getDistinctValuesInCatList($categorie_childs);
                        //PAGINATION liste produit
                        $items_total = count($values_list); 
                        $view -> assign("total_values" , $items_total);
                        $paginationCount = floor($items_total / $item_per_page);
                        $paginationModCount= $items_total % $item_per_page;
                   
                        if(!empty($paginationModCount)){
                            $paginationCount++;
                        }
                        $json["total_page_values"] = $paginationCount;
                        $view -> assign("total_page_values" , $paginationCount);
                        $values_list = array_slice($values_list, $item_per_page*$curr_page, $item_per_page);
                        $values_list_html = $view -> showValuesListAttribut($values_list, $_POST['id_attribut']);
                        $view -> assign("list_values" , $values_list_html);
                    }
                    
                    $html = $view -> render("embedAttribut");
                    
                    $json["html"] = utf8_encode($html);
                    
                    //var_dump($json);
                        
                    $tmp = json_encode($json, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP );
                    
                    $error = json_last_error();
                    
                    $out = $tmp;
                }
                else{
                    $out = "Pas de fiche attribut";
                }
            break;
            
            case '/deleteAttribut':
                    
                   $dispatch = true;
                 
	               if(isset($_POST['id_attribut']) && $_POST['id_attribut'] != ""){
	                   
                       if($attribut = $attributManager -> getById(array("id_attribut" => $_POST['id_attribut']))){
                           
                           /********************* RAJOUT ******************/
                           //$categorie_childs = substr($skf -> getAllCatChildId($_POST['cat_id']), 0, -2); 
                           $categorie_childs = $_POST["current_cat_childs"];
                           /**********************************************/
                           
                           //$pa_list = $skf -> getAllProductAttrForCat($_POST['cat_id'], $attribut->getId_attribut());
                           $pa_list = $skf -> getAllProductAttrInCatList($categorie_childs, $attribut->getId_attribut());
                           
                           foreach ($pa_list as $pa) {
                               $productAttributManager -> delete($pa);
                           }
                           
                           if($attribut -> getCountProdsWithThis() == 0){
                               $attributManager -> delete($attribut);
                               $out = "<div class='alert alert-success'> 
                                       <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                       Félicitations ! Lattribut <strong>".htmlentities($attribut->getLabel_attr())."</strong> a bien été supprimé !
                                   </div>";
                           }
                           else{
                               $out = "<div class='alert alert-warning'> 
                                       <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                       Félicitations ! 
                                       Les relations produit-attributs ont bien été effacés pour cette catégorie néanmoins cet
                                       attribut <strong>".htmlentities($attribut->getLabel_attr())."</strong> est encore utilisé dans d'autres catégories !
                                   </div>";
                           }
                       }
                       else{
                           $out = "<div class='alert alert-danger'> 
                                       <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                       Attention ! Lattribut que vous essayez de supprimer n'existe pas !
                                   </div>";
                       }   
                   }
	       break;
           
           case '/deleteSelectedAttr':
               $dispatch = true;
               
               if(isset($_POST['id_attributs']) && !empty($_POST['id_attributs'])) {

                   $html = "<div class='alert alert-success'> 
                               <button type='button' class='close' data-dismiss='alert'>&times;</button>
                               Félicitations ! Supression des attributs : <br/>";                   
                   
                   /********************* RAJOUT ******************/
                   //$categorie_childs = substr($skf -> getAllCatChildId($_POST['cat_id']), 0, -2); 
                    $categorie_childs = $_POST["current_cat_childs"];
                   /**********************************************/
                           
                   foreach ($_POST['id_attributs'] as $id_attribut) {
                       
                       if($attribut = $attributManager -> getById(array("id_attribut" => $id_attribut))){
                           
                           //$pa_list = $skf -> getAllProductAttrForCat($_POST['cat_id'], $attribut->getId_attribut());
                           $pa_list = $skf -> getAllProductAttrInCatList($categorie_childs, $attribut->getId_attribut());
                           
                           foreach ($pa_list as $pa) {
                               $productAttributManager -> delete($pa);
                           }
                            
                           if($attribut -> getCountProdsWithThis() == 0){
                               //attribut non utilisé ailleurs, on peut le supprimer entièrement
                               $attributManager -> delete($attribut);
                               $html .= "<strong>-".$attribut -> getLabel_attr()."</strong><br/>";
                           }
                           else{
                                $html .= "<strong>-".$attribut -> getLabel_attr()."</strong> (Uniquement les valeurs ! Attribut utilisé dans d'autres catégories !)<br/>";
                           }
                       }
                   }
                   $html .= "</div>";
                   $out = $html;
               } 
           break;
           
            case '/getDialogFusionAttributs':
                $dispatch = true;
                
                if(isset($_POST['cat_id']) && !empty($_POST['cat_id'])){
                    
                    $categorie = $categorieManager -> getById(array("cat_id" => $_POST['cat_id']));    
                    /********************* RAJOUT ******************/
                    $categorie_childs = $_POST["current_cat_childs"];
                    /**********************************************/
                }

                if(isset($_POST['id_attributs']) && !empty($_POST['id_attributs'])){
                    
                    $attribut_list = array();
                    
                    foreach ($_POST['id_attributs'] as $id_attribut) {
                        
                        $attribut_list[] = $attributManager -> getById(array("id_attribut" => $id_attribut)); 
                    }                    
                }
                
                ob_start();
                include ("./catalogue/popup_fusion_attribut/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;                    
            break;

            case '/validFusionAttributs':
                $dispatch = true;
                
                if(isset($_POST['cat_id']) && !empty($_POST['cat_id'])){
                    $categorie = $categorieManager -> getById(array("cat_id" => $_POST['cat_id'])); 
                       
                }
                $categorie_childs = $_POST["current_cat_childs"];
                
                if(isset($_POST['id_attributs']) && !empty($_POST['id_attributs']) && !empty($_POST['label_attr_fusion'])){
                    $error = "";
                    $html = "<div class='alert alert-success'> 
                               <button type='button' class='close' data-dismiss='alert'>&times;</button>
                               Félicitations ! Fusion des attributs : <br/>";
                    
                    $unique_code_array = array();
                    
                    foreach ($_POST['id_attributs'] as $id_attribut) {
                        
                        $attribut = $attributManager -> getById(array("id_attribut" => $id_attribut));
                        $unique_code_array[] = $attribut -> getCode_attr(); 
                    }     
                    
                    $code_attr = str_replace(array(" ", "'"), "_", strtoupper(stripAccents(trim($_POST['label_attr_fusion']))));
                    
                    if(in_array($code_attr, $unique_code_array)){
                        $code_attr = "TMP_LABEL_BEFORE_SAVE_".$code_attr."_".mt_rand(1,100);
                    }

                    if($attr_test = $attributManager -> getBy(array("code_attr" => str_replace(array(" ", "'"), "_", strtoupper(stripAccents(trim($_POST['label_attr_fusion'])))),
                                                                    "cat_id" => $_POST['cat_id']))){
                        if($attr_test->getCountProdsWithThis() == 0)
                            $attributManager -> delete($attr_test);
                        else{
                            $pa_list_error = $productAttributManager-> getAll(array("id_attribut" => $attr_test->getId_attribut(), "active" => 1));
                            foreach ($pa_list_error as $pa_error) {
                                $prod_tmp = $catalogProductManager -> getById(array("id_produit" => $pa_error -> getId_produit()));
                                if($prod_tmp->getCategories() != $_POST['cat_id'])
                                    $error .= "<br/><strong>".truncateStr($prod_tmp->getName(), 55)."</strong> de la catégorie <strong>".$prod_tmp->getCategory()->getCat_label()."</strong> utilise l'attribut ".$attribut->getLabel_attr();
                            }
                        } 
                    }
                                                                                            
                    $new_attr = new BusinessAttribut(array("cat_id" => $_POST['cat_id'],
                                                           "code_attr" => $code_attr,
                                                           "label_attr" => trim($_POST['label_attr_fusion']),
                                                           "is_filterable" => 0));
                                                           
                    $attributManager -> save($new_attr);

                    if($new_attr -> getId_attribut()){
                        
                        $dominant_attribut = $attributManager -> getById(array("id_attribut" => $_POST["dominant_id_attribut"]));
                        
                        
                        foreach ($_POST['id_attributs'] as $id_attribut) {
                            
                            //On garde l'attribut dominant pour la fin pour qu'il écrase les valeurs
                            if($id_attribut != $dominant_attribut -> getId_attribut()){
                                    
                                $attribut = $attributManager -> getById(array("id_attribut" => $id_attribut));
                                
                                //$pa_list = $skf -> getAllProductAttrForCat($_POST['cat_id'], $attribut->getId_attribut());
                                $pa_list = $skf -> getAllProductAttrInCatList($categorie_childs, $attribut->getId_attribut());
                                
                                //$html .= "<strong>-".$attribut -> getCode_attr()." - ".$attribut -> getCountProdsInCatWithThis($categorie->getCat_id())." Produits</strong><br/>";
                                $html .= "<strong>-".$attribut -> getCode_attr()." - ".$attribut -> getCountProdsInListCatWithThis($categorie_childs)." Produits</strong><br/>";
                                //Pour chaque relation produit attributs on va créer un clone de la relation avec le nouvel ID de l'attribut fusion
                                foreach ($pa_list as $product_attribut) {
                                    
                                    $clone_pa = clone $product_attribut;
                                    //changement avec nouvel ID de fusion et sauvegarde
                                    $clone_pa -> setId_attribut($new_attr->getId_attribut());
                                    
                                    $productAttributManager -> save($clone_pa);
                                    $productAttributManager -> delete($product_attribut);
                                }
                                
                                
                                //Suppression attribut et valeurs    
                                if($attribut->getCountProdsWithThis() == 0){
                                    $attributManager -> delete($attribut);
                                }else{
                                    if($attribut->getCode_attr() == $code_attr){
                                        $pa_list_error = $productAttributManager-> getAll(array("id_attribut" => $attribut->getId_attribut(), "active" => 1));
                                        foreach ($pa_list_error as $pa_error) {
                                            $prod_tmp = $catalogProductManager -> getById(array("id_produit" => $pa_error -> getId_produit()));
                                            $error .= "<br/><strong>".truncateStr($prod_tmp->getName(), 55)."</strong> de la catégorie <strong>".$prod_tmp->getCategory()->getCat_label()."</strong> utilise l'attribut ".$attribut->getLabel_attr();
                                        }
                                    }

                                } 
                            }
                        }
                        //Attribut dominant en dernier
                        //$pa_list_dominant = $skf -> getAllProductAttrForCat($_POST['cat_id'], $dominant_attribut->getId_attribut());
                        $pa_list_dominant = $skf -> getAllProductAttrInCatList($categorie_childs, $dominant_attribut->getId_attribut());
                        
                        //$html .= "<strong>-".$dominant_attribut -> getCode_attr()." - ".$dominant_attribut -> getCountProdsInCatWithThis($categorie->getCat_id())." Produits</strong><br/>";
                        $html .= "<strong>-".$dominant_attribut -> getCode_attr()." - ".$dominant_attribut -> getCountProdsInListCatWithThis($categorie_childs)." Produits</strong><br/>";
                        foreach ($pa_list_dominant as $pa) {
                            
                               $clone_pa = clone $pa;
                               //changement avec nouvel ID de fusion et sauvegarde
                               $clone_pa -> setId_attribut($new_attr->getId_attribut());
                                    
                               $productAttributManager -> save($clone_pa); 
                               $productAttributManager -> delete($pa);
                        }
                        
                        //Suppression attribut dominant
                        if( $dominant_attribut -> getCountProdsWithThis() == 0 ){
                            $attributManager -> delete($dominant_attribut);    
                        }else{
                            if($attribut->getCode_attr() == $code_attr){    
                                $pa_list_error = $productAttributManager-> getAll(array("id_attribut" => $dominant_attribut->getId_attribut(), "active" => 1));
                                foreach ($pa_list_error as $pa_error) {
                                    $prod_tmp = $catalogProductManager -> getById(array("id_produit" => $pa_error -> getId_produit()));
                                    $error .= "<br/><strong>".truncateStr($prod_tmp->getName(), 55)."</strong> de la catégorie <strong>".$prod_tmp->getCategory()->getCat_label()."</strong> utilise l'attribut ".$attribut->getLabel_attr();
                                }
                            }
                        } 
                        
                        
                        
                        $new_attr = $attributManager -> getById(array("id_attribut" => $new_attr->getId_attribut()));
                        
                        if($attr_test = $attributManager -> getBy(array("code_attr" => str_replace(array(" ", "'"), "_", strtoupper(stripAccents(trim($_POST['label_attr_fusion'])))),
                                                                        "cat_id" => $_POST['cat_id']))){
                            if($attr_test->getCountProdsWithThis() == 0)
                                $attributManager -> delete($attr_test);
                        }
                        
                        $new_attr -> setCode_attr(str_replace(array(" ", "'"), "_", strtoupper(stripAccents(trim($_POST['label_attr_fusion'])))));    
                        $attributManager -> save($new_attr);              
                        
                        $new_attr = $attributManager -> getById(array("id_attribut" => $new_attr->getId_attribut()));
                        
                        //$html.="<hr/> Résultat : <strong>-".$new_attr -> getCode_attr()." - ".$new_attr -> getCountProdsInCatWithThis($categorie->getCat_id())." Produits</strong><br/>";
                        $html.="<hr/> Résultat : <strong>-".$new_attr -> getCode_attr()." - ".$new_attr -> getCountProdsInListCatWithThis($categorie_childs)." Produits</strong><br/>";
                        $html .= "</div>";
                        
                        
                        if(str_replace(array(" ", "'"), "_", strtoupper(stripAccents(trim($_POST['label_attr_fusion'])))) != $new_attr->getCode_attr()){
                            
                            $html.="<div class='alert' style='color: black;'> 
                               <button type='button' class='close' data-dismiss='alert'>&times;</button>
                               Attention ! Le nom temporaire <strong>".htmlentities($new_attr->getCode_attr())."</strong> a été choisi car <br/>
                               <strong>".str_replace(array(" ", "'"), "_", strtoupper(stripAccents(trim($_POST['label_attr_fusion']))))."</strong> existe déjà en base 
                               pour cette catégorie même s'il n'apparait pas (Il est utilisé sur des produits d'autres catégories et ne peux donc pas être remplacé).";
                            
                            $html .= "<br/>".$error."</div>";
                        }     
                    }
                    else{
                        $html = "<div class='alert alert-danger'> 
                               <button type='button' class='close' data-dismiss='alert'>&times;</button>
                               Erreur ! Le nom d'attribut <strong>".htmlentities($_POST['label_attr_fusion'])."</strong> existe déja en dehors de ceux sélectionnés pour fusion <br/>
                               Pensez à l'ajouter à la liste de fusion si le nom existe déjà dans la liste.</div>";
                               
                        if($error){
                            $html .= "<div class='alert'> 
                               <button type='button' class='close' data-dismiss='alert'>&times;</button>
                               Des produits d'autres catégories utilisent ces attributs :<br/>".$error."</div>";                            
                        }
       
                    }
                
                }
                $out = $html ;                    
            break;
            
            case '/setAttributIsFilter':
	           $dispatch = true;
                
               if($attribut = $attributManager -> getById(array("id_attribut" => $_POST['id_attribut']))){
                   /*
                    if($attribut_category = $attributCategoryManager -> getBy(array("id_attribut"=> $_POST['id_attribut'], "cat_id" => $_POST['cat_id']))){
                           
                        $attribut_category -> setIs_filter($_POST['value']);
                       
                        $attributCategoryManager -> save($attribut_category);
                      
                    }   
                    else {
                        $attribut_category = new BusinessAttributCategory(array("id_attribut" => $_POST['id_attribut'], 
                                                                               "cat_id" => $_POST['cat_id'],
                                                                               "is_filter" => $_POST['value']));
                                                                               
                        $attributCategoryManager -> save($attribut_category);
                       
                    }*/
                    $attribut -> setIs_filterable($_POST["value"]);
                    $attributManager -> save($attribut);
                   
                    $html = "<div class='alert alert-success'> 
                           <button type='button' class='close' data-dismiss='alert'>&times;</button>
                           L'attribut <strong>".htmlentities($attribut->getLabel_attr())."</strong>  est maintenant ";
                    
                    $html .= $_POST['value'] ? "<strong>filtrable ! </strong></div>" : "<strong>non filtrable ! </strong></div>";                    
               }
               else{
                    $html = "<div class='alert alert-danger'> 
                           <button type='button' class='close' data-dismiss='alert'>&times;</button>
                           Erreur ! Cet attribut ne semble plus exister (cliquez sur Refresh pour mettre à jour le tableau d'affichage)</div>";                    
               }
                
               $out = $html; 
                
	        break;
            
            case "/getCatalogProductAttribut":
                
                $dispatch = true;
                
                if(isset($_POST['curr_page']) && !empty($_POST['curr_page'])){
                    $curr_page = $_POST['curr_page'];
                }else{
                    $curr_page = '0';
                }
                if(isset($_POST['nb_items_page']) && !empty($_POST['nb_items_page'])){
                    $item_per_page = $_POST['nb_items_page'];
                }else{
                    $item_per_page = 20;
                }
                
                if(isset($_POST['id_attribut']) && $_POST['id_attribut'] != ""){
                    
                    $attribut = $attributManager -> getBy(array("id_attribut" => $_POST['id_attribut']));
                    
                    if($attribut){

                        //$prod_attr_list = $skf -> getAllProductAttrForCat($_POST['current_cat_id'], $attribut->getId_attribut());
                        $prod_attr_list = $skf -> getAllProductAttrInCatList($_POST['current_cat_childs'], $attribut->getId_attribut());
                    }
                    
                    $product_list = array();
                                        
                    //PAGINATION liste produit
                    $items_total = count($prod_attr_list); 
                    $paginationCount = floor($items_total / $item_per_page);
                    $paginationModCount= $items_total % $item_per_page;
               
                    if(!empty($paginationModCount)){
                        $paginationCount++;
                    }
                                            
                    $prod_attr_list = array_slice($prod_attr_list, $item_per_page*$curr_page, $item_per_page);
                    
                    if($prod_attr_list){
                        
                        foreach ($prod_attr_list as $prod_attr) {
                            $product_list[] = $catalogProductManager -> getById(array("id_produit" => $prod_attr ->getId_produit()));    
                        }
                        
                        $product_list_html = $view -> showProdListAttributs($product_list, $_POST['id_attribut']);

                    }

                    $arr = array ("html" => $product_list_html,
                                  "total_items" => $items_total,
                                  "total_pages" => $paginationCount);    
                                  
                    $out = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP );
                }
                else{
                    $out = "Aucun résultat";
                }
                
            break;

            case "/getTableAttributValues":
                
                $dispatch = true;
                
                if(isset($_POST['curr_page']) && !empty($_POST['curr_page'])){
                    $curr_page = $_POST['curr_page'];
                }else{
                    $curr_page = '0';
                }
                if(isset($_POST['nb_items_page']) && !empty($_POST['nb_items_page'])){
                    $item_per_page = $_POST['nb_items_page'];
                }else{
                    $item_per_page = 20;
                }
                
                if(isset($_POST['id_attribut']) && $_POST['id_attribut'] != ""){
                    
                    $attribut = $attributManager -> getBy(array("id_attribut" => $_POST['id_attribut']));
                    $attribut -> setCurrent_cat_id($_POST['cat_id']);
                    
                     //LISTE DES VALEURS
                    //$values_list = $attribut -> getDistinctValuesForCat($_POST['cat_id']); 
                    $values_list = $attribut -> getDistinctValuesInCatList($_POST['current_cat_childs']);
                    
                    
                    if(isset($_POST['sort_nb_prods']) && isset($_POST['sort_nb_prods']) != ""){
                   
                        usort($values_list, $_POST['sort_nb_prods']."Values");    
                        $sort_nb_prods_class = $_POST['sort_nb_prods'];
                    }
                    
                    //PAGINATION liste produit
                    $items_total = count($values_list); 
                    $paginationCount = floor($items_total / $item_per_page);
                    $paginationModCount= $items_total % $item_per_page;
               
                    if(!empty($paginationModCount)){
                        $paginationCount++;
                    }
                    $values_list = array_slice($values_list, $item_per_page*$curr_page, $item_per_page);
                    
                    $values_list_html = $view -> showValuesListAttribut($values_list, $_POST['id_attribut'], $sort_nb_prods_class);
                    
                    $arr = array ("html" => $values_list_html,
                                  "total_items" => $items_total,
                                  "total_pages" => $paginationCount);    
                                  
                    $out = json_encode($arr, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP );
                }
                else{
                    $out = "Aucun résultat";
                }
                
            break;

            case '/getDialogValeurAttribut':
                $dispatch = true;

                if(isset($_POST['id_attribut']) && !empty($_POST['id_attribut'])){
                    
                    $attribut = $attributManager -> getById(array("id_attribut" => $_POST["id_attribut"]));
                                    
                    ob_start();
                    include ("./catalogue/popup_edit_value_attribut/index.php");
                    $popup_actif = ob_get_contents();
                    ob_end_clean();
                    $out = $popup_actif ;
                }                    
            break;
                        
            case '/editAttributValue':
	            $dispatch = true;
                
                if(isset($_POST['id_attribut']) && !empty($_POST['id_attribut'])){
                    
                    $product_attribut_list = $productAttributManager -> getAll(array("value" => trim($_POST['old_value']), "id_attribut" => $_POST['id_attribut']));
                    
                    foreach ($product_attribut_list as $pa) {
                        
                        $pa -> setValue (trim($_POST['value']));
                        $pa -> setActive (1);
                        $productAttributManager -> save($pa);
                    }
                    
                    $out = "<div class='alert alert-success'> 
                           <button type='button' class='close' data-dismiss='alert'>&times;</button>
                           Toutes les ancinnes valeurs <strong>".htmlentities(trim($_POST['old_value']))."</strong>  sont maintenant nommées 
                           <br/><strong>".htmlentities(trim($_POST['value']))."</strong></div>"; 
                }
	        break;
            
            case '/getDialogFusionValues':
                $dispatch = true;


                if(isset($_POST['id_attribut']) && !empty($_POST['id_attribut']) && !empty($_POST['values'])){
                    
                    $attribut = $attributManager -> getById(array("id_attribut" => $_POST["id_attribut"]));
                    
                    ob_start();
                    include ("./catalogue/popup_fusion_values/index.php");
                    $popup_actif = ob_get_contents();
                    ob_end_clean();
                    $out = $popup_actif ;                
                }    
            break;
            
            case '/validFusionValues':
                $dispatch = true;
                
                if(isset($_POST['id_attribut']) && !empty($_POST['id_attribut'])){
                        
                    $html = "<div class='alert alert-success'> 
                               <button type='button' class='close' data-dismiss='alert'>&times;</button>
                               Félicitations ! Fusion des valeurs : <br/>";
                                  
                    foreach ($_POST['values'] as $value) {
                        
                        //$product_attribut_list = $skf -> getAllProductAttrForCatAndValue($_POST["cat_id"], $_POST['id_attribut'], trim($value));
                        $product_attribut_list = $skf -> getAllProductAttrInCatListAndValue($_POST["current_cat_childs"], $_POST['id_attribut'], trim($value));
                        
                        $html .= "-<strong>$value</strong><br/>";
                        
                        foreach ($product_attribut_list as $pa) {
                            
                            $pa -> setValue (trim($_POST['label_attr_value_fusion']));
                            $pa -> setActive (1);
                            $productAttributManager -> save($pa);
                        }                        
                    }

                    
                    $html .= "Toutes les anciennes valeurs de la catégorie ( et sous-catégories ) sont maintenant nommées : <strong>".trim(htmlentities($_POST['label_attr_value_fusion']))."</strong></div>";
                           
                    $out = $html; 
                }	
	        break;
            
            case '/saveEditLabelAttribut':
	            $dispatch = true;
                
                if($attribut = $attributManager -> getById(array("id_attribut" => $_POST["id_attribut"]))) {
                        
                    $attribut -> setLabel_attr($_POST["label_attr"]);
                    
                    $attributManager -> save($attribut);
                    
                    $out = "<div class='alert alert-success'> 
                           <button type='button' class='close' data-dismiss='alert'>&times;</button>
                           <strong>Félicitations ! </strong> Le nouveau label de votre attribut est ".htmlentities($attribut -> getLabel_attr())."</div>";
                               
                }
                else{
                    $out = "<div class='alert alert-danger'> 
                           <button type='button' class='close' data-dismiss='alert'>&times;</button>
                           <strong>Erreur ! </strong> L'attribut que vous essayez de modifier n'existe plus !</div>";                     
                }
                
	        break;
	                                
            case '/getJsonCat':
                
                $dispatch = true;
                
                $univers_list = $categorieManager -> getAll(array("parent_id" => trim($_GET['id']), "cat_active" => 1));
                
                $json = array();
                
                foreach ($univers_list as $cat) {
                    $json [] = array("id" => $cat->getCat_id(),
                                     "text" => $cat->getCat_label(),
                                     "type" => "default", 
                                     "children" => true,
                                     "cat_level" => $cat->getCat_level());
                    $html .= "<li id='".$cat->getCat_id()."' class='jstree-closed'>".$cat->getCat_label()."</li>";
                }
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json; charset=UTF-8');
                echo json_encode($json);
                $out = $html;    
            break;
                        			
            case '/setProductActifGp':
               $dispatch = true;  
	           $_SESSION['avaGP']['activeProducts'] = $_POST['actif']; 
            break;
            
            case '/catalogue/produit/fiche':
                    
                    if(!isset($_GET['id']) || $_GET['id'] == ""){
                        $_GET['id'] = mt_rand ( 20000 , 90000 );
                    }
                    $product = $catalogProductManager -> getBy(array("id_produit" => $_GET['id']));
                
                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }
                    
                    
                    $template = "product";
                    $view -> assign("product" , $product);
                    $view -> assign("prod_attr_list" , $prod_attr_list);
            break;	
            
            case '/embed_fiche_produit/edit':
            
                $dispatch = true;
                
                if(isset($_POST['id_produit']) && $_POST['id_produit'] != ""){
                    
                    $product = $catalogProductManager -> getBy(array("id_produit" => $_POST['id_produit']));
                    
                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }
                    
                    $view -> assign("product" , $product);
                    $view -> assign("prod_attr_list" , $prod_attr_list);
                    $view -> assign("embed" , true);
                    
                    $json = array("html" => $view -> render("embedProductEdit"),
                                  "nom" => htmlentities(truncateStr($product->getName(), 16) ));
                    $out = json_encode($json, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
                }
                else{
                    $out = "Pas de fiche produit";
                }

            break;

            case '/embed_fiche_produit/duplicate':
            
                $dispatch = true;
                
                if(isset($_POST['id_produit']) && $_POST['id_produit'] != ""){
                    
                    $product = $catalogProductManager -> getBy(array("id_produit" => $_POST['id_produit']));
                                        
                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }

                    
                    $product->setName("Duplicat. ".$product->getName());
                    
                    $view -> assign("product" , $product);
                    $view -> assign("prod_attr_list" , $prod_attr_list);
                    $view -> assign("embed" , true);
                    
                    $json = array("html" => $view -> render("embedProductEditDuplicate"),
                                  "nom" => truncateStr($product->getName(), 16) );
                    $out = json_encode($json);
                }
                else{
                    $out = "Pas de fiche produit";
                }

            break;
            
            case '/embed_fiche_produit':
            
                $dispatch = true;
                
                if(isset($_POST['id_produit']) && $_POST['id_produit'] != ""){
                    
                    $product = $catalogProductManager -> getBy(array("id_produit" => $_POST['id_produit']));
                    
                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }
                    
                    $view -> assign("product" , $product);
                    $view -> assign("prod_attr_list" , $prod_attr_list);
                    $view -> assign("embed" , true);
                    
                    $json = array("html" => $view -> render("embedProduct"),
                                  "nom" => htmlentities(truncateStr($product->getName(), 16)) );
                    $out = json_encode($json);
                }
                else{
                    $out = "Pas de fiche produit";
                }

            break;
            
            case '/saveEditFiche':
                
            	$dispatch = true;
                
                if(isset($_POST['id_produit']) && $_POST['id_produit'] != ""){
                    
                    if($product = $catalogProductManager -> getById(array("id_produit" => $_POST['id_produit']))){
                    
                        $product -> setName($_POST['name'])
                                 -> setPrice($_POST['price'])
                                 -> setManufacturer($_POST['manufacturer'])
                                 -> setCategories($_POST['categories'])
                                 -> setCountry_of_manufacture($_POST['country_of_manufacture'])
                                 -> setEan($_POST['ean'])
                                 -> setDropship_vendor($_POST['dropship_vendor'])
                                 -> setDescription($_POST['description'])
                                 -> setShort_description($_POST['short_description'])
                                 -> setImage($_POST['image'])
                                 -> setSmall_image($_POST['small_image'])
                                 -> setThumbnail($_POST['thumbnail'])
                                 -> setLocal_image($_POST['local_image'])
                                 -> setLocal_small_image($_POST['local_small_image'])
                                 -> setLocal_thumbnail($_POST['local_thumbnail'])
                                 -> setDescription_html($_POST['description_html'])
                                 -> setWeight($_POST['weight']);
                                 
                        $catalogProductManager -> save($product);
                        
                        if($product->getId_produit()){
                            $product_attribut_list = $product->getAllProductAttributes();
                            
                            if(!empty($product_attribut_list)){
                                foreach ($product_attribut_list as $pa) {
                                    
                                    $productAttributManager -> delete($pa);
                                }    
                            }                            
                        }

                        
                        foreach ($_POST['product_attribut'] as $pa) {
                            
                            //On ne crée pas de relation ni d'attributs si pas de valeur pour le couple produit_attribut
                            if(trim($pa['value']) !=  ""){
                                
                                $product_attr = new BusinessProductAttribut();
                                $product_attr -> setId_produit($product->getId_produit());
                                $product_attr -> setValue($pa['value']);
                                $product_attr -> setActive(1);
                                
                                //attribut exists
                                if(is_numeric(trim($pa['id_attribut']))){
                                    $product_attr -> setId_attribut($pa['id_attribut']);
                                    $productAttributManager -> save($product_attr);
                                }
                                else{ //attribut à créer
                                    if($attribut = $attributManager -> getBy(array("cat_id" => $product->getCategories(), 
                                                                                    "code_attr" => strtoupper(stripAccents(trim($pa['id_attribut'])))))){
                                                                                        
                                        $product_attr -> setId_attribut($attribut -> getId_attribut()); 
                                        $productAttributManager -> save($product_attr);
                                    }     
                                    else{
                                        $attribut = new BusinessAttribut(array("cat_id" => $product->getCategories(),
                                               "code_attr" => strtoupper(stripAccents(trim($pa['id_attribut']))),
                                               "label_attr" => trim($pa['id_attribut']),
                                               "is_filterable" => 0 
                                               ));
                                        $attributManager -> save($attribut);
                                        
                                        if($attribut -> getId_attribut()){
                                            $product_attr -> setId_attribut($attribut -> getId_attribut()); 
                                            $productAttributManager -> save($product_attr);
                                        }                                
                                    }
                                }
                            }
                        }
                    }
                }
                
            break;
            
            case '/saveDuplicateFiche':
                
	            $dispatch = true;
                
                $product = new BusinessCatalogProduct();

                $product -> setName($_POST['name'])
                         -> setPrice($_POST['price'])
                         -> setSku($_POST['sku'])
                         -> setManufacturer($_POST['manufacturer'])
                         -> setCategories($_POST['categories'])
                         -> setCountry_of_manufacture($_POST['country_of_manufacture'])
                         -> setEan($_POST['ean'])
                         -> setDropship_vendor($_POST['dropship_vendor'])
                         -> setDescription($_POST['description'])
                         -> setShort_description($_POST['short_description'])
                         -> setImage($_POST['image'])
                         -> setSmall_image($_POST['small_image'])
                         -> setThumbnail($_POST['thumbnail'])
                         -> setLocal_image($_POST['local_image'])
                         -> setLocal_small_image($_POST['local_small_image'])
                         -> setLocal_thumbnail($_POST['local_thumbnail'])
                         -> setDescription_html($_POST['description_html'])
                         -> setWeight($_POST['weight']);
                         
                $catalogProductManager -> save($product);
                
                $product_attribut_list = $product->getAllProductAttributes();
                
                if(!empty($product_attribut_list)){
                    foreach ($product_attribut_list as $pa) {
                        $productAttributManager -> delete($pa);
                    }    
                }
                
                foreach ($_POST['product_attribut'] as $pa) {
                    
                    //On ne crée pas de relation ni d'attributs si pas de valeur pour le couple produit_attribut
                    if(trim($pa['value']) !=  ""){
                        
                        $product_attr = new BusinessProductAttribut();
                        $product_attr -> setId_produit($product->getId_produit());
                        $product_attr -> setValue($pa['value']);
                        $product_attr -> setActive(1);
                        
                        //attribut exists
                        if(is_numeric(trim($pa['id_attribut']))){
                            $product_attr -> setId_attribut($pa['id_attribut']);
                            $productAttributManager -> save($product_attr);
                        }
                        else{ //attribut à créer
                            if($attribut = $attributManager -> getBy(array("cat_id" => $product->getCategories(), 
                                                                            "code_attr" => strtoupper(stripAccents(trim($pa['id_attribut'])))))){
                                                                                
                                $product_attr -> setId_attribut($attribut -> getId_attribut()); 
                                $productAttributManager -> save($product_attr);
                            }     
                            else{
                                $attribut = new BusinessAttribut(array("cat_id" => $product->getCategories(),
                                       "code_attr" => strtoupper(stripAccents(trim($pa['id_attribut']))),
                                       "label_attr" => trim($pa['id_attribut']),
                                       "is_filterable" => 0 
                                       ));
                                $attributManager -> save($attribut);
                                
                                if($attribut -> getId_attribut()){
                                    $product_attr -> setId_attribut($attribut -> getId_attribut()); 
                                    $productAttributManager -> save($product_attr);
                                }                                
                            }
                        }
                    }
                }                
                $out = $product->getId_produit();
                                    
                
            break;
                        
            case '/catalogue/produit/fiche/edit':
                
                    $product = $catalogProductManager -> getBy(array("id_produit" => $_GET['id']));
                    
                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }
                    
                    $template = "productEdit";
                    $view -> assign("product" , $product);
                    $view -> assign("prod_attr_list" , $prod_attr_list);
            break; 

            case '/catalogue/produit/fiche/create':
                
                    $template = "productCreate";
            break;
            
            case '/checkSKU':
                $dispatch = true;
                
                $out = $skf->checkSkuUnique($_POST['sku']);    
            break; 

            
            case '/catalogue/produit/fiche/create/valid':
                
                $product = new BusinessCatalogProduct();
                
                if($_POST['sku'] == "" || $_POST['name'] == "" || $_POST['categories'] == ""){
                    $error_message = '<div class="alert alert-danger"> 
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Veuillez remplir les champs obligatoires svp !</strong></div>';      
                    $template = "productCreate";
                    $view -> assign("error_message" , $error_message);
                }
                else{
                    $product -> setSku($_POST['sku'])
                             -> setName($_POST['name'])
                             -> setManufacturer($_POST['manufacturer'])
                             -> setWeight($_POST['weight'])
                             -> setCategories($_POST['categories'])
                             -> setCountry_of_manufacture($_POST['country_of_manufacture'])
                             -> setEan($_POST['ean'])
                             -> setDropship_vendor($_POST['dropship_vendor'])
                             -> setShort_description($_POST['short_description'])
                             -> setDescription($_POST['description'])
                             -> setDescription_html($_POST['description_html'])
                             -> setImage($_POST['image'])
                             -> setSmall_image($_POST['small_image'])
                             -> setThumbnail($_POST['thumbnail'])
                             -> setLocal_image($_POST['local_image'])
                             -> setLocal_small_image($_POST['local_small_image'])
                             -> setLocal_thumbnail($_POST['local_thumbnail']);
                             
                             
                    $catalogProductManager -> save($product);
                    
                    header('Location: /app.php/catalogue/produit/fiche?id='.$product->getId_produit());                    
                }

            break;
                                    
            case '/catalogue/produit/fiche/edit/valid':
                    
                    $product = $catalogProductManager -> getBy(array("id_produit" => $_POST['id_produit']));
                    
                    $product -> setName($_POST['name'])
                             -> setManufacturer($_POST['manufacturer'])
                             -> setWeight($_POST['weight'])
                             -> setCategories($_POST['categories'])
                             -> setCountry_of_manufacture($_POST['country_of_manufacture'])
                             -> setEan($_POST['ean'])
                             -> setDropship_vendor($_POST['dropship_vendor'])
                             -> setShort_description($_POST['short_description'])
                             -> setDescription($_POST['description'])
                             -> setDescription_html($_POST['description_html'])
                             -> setImage($_POST['image'])
                             -> setSmall_image($_POST['small_image'])
                             -> setThumbnail($_POST['thumbnail'])
                             -> setLocal_image($_POST['local_image'])
                             -> setLocal_small_image($_POST['local_small_image'])
                             -> setLocal_thumbnail($_POST['local_thumbnail']);
                             
                             
                    $catalogProductManager -> save($product);
                    
                    header('Location: /app.php/catalogue/produit/fiche?id='.$_POST['id_produit']);
                    //$template = "productEdit";
                    //$view -> assign("product" , $product);
                    //$view -> assign("prod_attr_list" , $prod_attr_list);
            break;
            				
			case '/validEditProd':
				$mess .= "<p>Sauvegarde effectuée !</p>";
				$dispatch = true;
				$attribut = array() ; // la table avec tous les attrinuts passé en POST
				$caract = $_POST['staticFields'][0]; // tous les caractéristiques récupéré en POST
				
				// on récupére maintenant les attributs dans $_POST['dynaFields']
				$nbrAttr = $_POST['dynaFields'][0]['nbrAttribut'] ; // le nombre d'attribut
				
				for ($i = 1 ; $i <= $nbrAttr; $i++){ // on regarde sur tous le tableau de $_POST['dynaFields']  
					// on récupére les valeurs 
					$rows = $_POST['dynaFields'][$i] ;
					$rows =  explode("|",$rows['attribut']); // on sépare l'id de la valeur
					$attribut[$rows[0]] = $rows[1];			// on ajoute a la table attribut
				}
					
				// ATTRIBUTS STATIQUE
				$skf-> prepareStmteditProd();
				$skf-> editRowProductAvahis($caract);
				//ATTRIBUTS DYNAMIQUES
				
				if ($nbrAttr>0){
					$nbdyna = 0 ;	
					foreach($attribut as $idAttr => $value){
						
						if($skf -> addAttributProduct($idAttr,$caract['id_produit'],$value) ){
							$nbdyna++ ;
						}
					}
				}
				$out = $mess;
				
			break;
	
	
			case    "/commercants/ecommercant" :
				$ACCESS_key = "commercants.ecommercant..";
				unset($_SESSION['gestion']);
				unset($_SESSION['avaGP']);
				unset($_SESSION['ecom']['listProdDeleted']);
				unset($_SESSION['ecom']['tech']);
				unset($_SESSION['ecom']['vendor']);
				unset($_SESSION['ecom']['cat']);
				unset($_SESSION['ecom']['listProdEcom']);
				unset($_SESSION['ecom']['listProdAv']);
				unset($_SESSION['ecom']['catAv']);
				unset($_SESSION['ecom']['viewAv']);
				unset($_SESSION['ecom']['view']);
				unset($_SESSION['ecom']['attrVal']);
				unset($_SESSION['ecom']['attrValAv']);
				unset($_SESSION['ecom']['selectedProds']);
				unset($_SESSION['ecom']["currPage"]);
				unset($_SESSION['ecom']['offset']);
				unset($_SESSION['ecom']["currPageAv"]);
				unset($_SESSION['ecom']['offsetAv']);
				unset($_SESSION['ecom']['brands']);
				unset($_SESSION['ecom']['brandsAv']);
				unset($_SESSION['ecom']['listProdAssoc']);
				unset($_SESSION['ecom']['listProdRefused']);
				unset($_SESSION['ecom']['nbAssoc']);
				unset($_SESSION['ecom']['nbRefused']);
				unset($_SESSION['ecom']['nbSelected']);
				$_SESSION['ecom']['context'] ="todo";
				
				//Création de la grille
				include("./commercants/ecommercant/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./commercants/ecommercant/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

			break;
			
			
			case "/grossistes/categoriesgrossiste" :
				$ACCESS_key ="grossistes.categoriesgrossiste..";
				$security_domain = "grossistes";
				$domain_categ = "categoriesgrossiste";
				$right_menu_visibility = false;
				
				$grid = new CategoriesGrossisteGrid($_GET['id']);
				// init step of SQL limit
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./grossistes/grossiste/categories/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
	
			
			/**
			 * Ecran de choix des produits grossistes que Avahis veux mettre en vente
			 */
			case    "/qualification/venteavahis" :
				$ACCESS_key = "qualification.venteavahis..";				
				$ACCESS_key ="qualification.venteavahis..";
				$security_domain = "qualification";
				$domain_categ = "venteavahis";
				$right_menu_visibility = true;
				
				$grid = new VenteAvahisGrid();
				// init step of SQL limit
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./qualification/venteavahis/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;

			/**
			 * Ecran de choix des produits grossistes que Avahis veux mettre en vente
			 */
			case    "/o_admin/filesexport/" :
				$ACCESS_key = "admin.filesexport..";				
				$ACCESS_key ="admin.filesexport..";
				$security_domain = "admin";
				$domain_categ = "filesexport";
				$right_menu_visibility = true;
				
				$grid = new FilesExportGrid();
				// init step of SQL limit
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./o_admin/filesexport/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;		
            
            case "/clients/modes_de_livraison/" :
                    
                $template = "test";
                
                var_dump($view);
                
                $typesLivraison = $livraisonTypeManager -> getAll();
                
                $view -> assign("typesLivraison", $typesLivraison);

            break;  
                        
            /**
             * Ecran de choix des produits grossistes que Avahis veux mettre en vente
             */
            case    "/o_admin/gestionreferentiels/" :
                $ACCESS_key = "admin.gestionreferentiels..";    
                $security_domain = "admin";
                $domain_categ = "gestionreferentiels";
                
                $template = "referentiels";
                
                $packList = $creditPackManager -> getAll();
                $paymentList = $paymentTypeManager -> getAll();
                $custStatusList = $customerStatutManager -> getAll();
                $vendorStatusList = $vendorStatutManager -> getAll();
                
                $view -> assign("packList", $packList);
                $view -> assign("paymentList", $paymentList);
                $view -> assign("custStatusList", $custStatusList); 
                $view -> assign("vendorStatusList", $vendorStatusList);  

            break;  

            /**
             * Ecran de choix des produits grossistes que Avahis veux mettre en vente
             */
            case    "/o_admin/statistiques/" :
                $ACCESS_key = "admin.statistiques..";    
                $security_domain = "admin";
                $domain_categ = "statistiques";
                
                $template = "statistiques";
                

            break;             	
						
			case '/catalogue/rechercheproduitgrossiste':
				$ACCESS_key = "catalogue.rechercheproduitgrossiste..";				
				$ACCESS_key ="catalogue.rechercheproduitgrossiste..";
				$security_domain = "catalogue";
				$domain_categ = "rechercheproduitgrossiste";
				$right_menu_visibility = true;
				
				$grid = new RechercheProduitGrossisteGrid();
				// init step of SQL limit
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./catalogue/rechercheproduitgrossiste/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case    "/commercants/tracking" :
				$ACCESS_key = "commercants.tracking..";
				unset($_SESSION['avaGP']);
				unset($_SESSION['ecom']);
				unset($_SESSION['gr']);
				
				
				//Création de la grille
				include("./commercants/tracking/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				//Col Right
				ob_start();
				include ("./commercants/tracking/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";

			break;
			
			
			case "/deleteTracking" :
				$dispatch = true;
				ob_start();
				include ("./commercants/tracking/popup_delete/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;

			break;
			
			
			case "/addTrackingVendor" :
				$dispatch = true;
				ob_start();
				include ("./commercants/tracking/popup_addUrl/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;

			break;
			
			
			case "/deleteVendor" :
				$dispatch = true;
				ob_start();
				include ("./commercants/ecommercant/popup_delete/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;
			break;
			
			
			case "/actifVendor" :
				$dispatch = true;
				ob_start();
				include ("./commercants/ecommercant/popup_actif/index.php");
				$popup_actif = ob_get_contents();
				ob_end_clean();
				$out = $popup_actif ;
			break;
			
			
			case "/passifVendor" :
				$dispatch = true;
				ob_start();
				include ("./commercants/ecommercant/popup_inactif/index.php");
				$popup_inactift = ob_get_contents();
				ob_end_clean();
				$out =$popup_inactift ;			break;
			
			
			case "/popup_infovend" :
				$ACCESS_key = "commercants.popup_infovend..";
				
				$dispatch = true;
				ob_start();
				include ("./commercants/ecommercant/popup_infovend/index.php");
				$popup_infovend = ob_get_contents();
				ob_end_clean();
				$out =$popup_infovend ;
			break;
				
				
			case    "/grossistes/grossiste" :
				$ACCESS_key = "grossistes.grossiste..";
				unset($_SESSION['avaGP']);
				unset($_SESSION['gr']);
				$_SESSION['gr']['context'] ="todo";

				
				$security_domain = "grossistes";
				
				$domain_categ = "grossiste";
				
				$right_menu_visibility = true;
				
				$step_limit = SystemParams::getParam('system*nlist_element');
				
				$grid = new GrossisteGrid($query);
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				
				//Col Right
				ob_start();
				include ("./grossistes/grossiste/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			
			case    "/commercants/ecommercant/listeProduits" :
				$ACCESS_key = "commercants.listeproduits..";
				include("./commercants/ecommercant/listeProduits/index.php");
				$view -> assign("out", $grid -> display());
				$template = "rendered";

			break;
			
			
			//Mise en forme de la page des ecommercant avec création des treemap pour le coté ecommercants
			//et le coté Avahis
			case    "/commercants/ecommercant/assoc" :
				$_SESSION['typeVendor'] = "Ecom";
				$ACCESS_key = "commercants.assoc..";
				$template = "assoc";
				
				$class = "BusinessSoukeo".$_GET['tech']."Model";
				$_SESSION['ecom']['tech']=$_GET['tech'];
				$db = new $class();
				
				$_SESSION['ecom']['vendor'] = $_GET['id'];
				$categs = $db -> getDistinctCat($_GET['id']);
				
				$nomEcom = $db -> getValidVendor($_GET['id']);
				$view -> assign('vendName', $nomEcom['VENDOR_NOM']);
				

				if(!isset($_SESSION['ecom']['context']))$_SESSION['ecom']['context'] = "todo";
				if(empty($_POST['context']))$_POST['context']=$_SESSION['ecom']['context'];
				
				
				!isset($_SESSION["ecom"]['view']) ? $_SESSION["ecom"]['view'] = "grid" : $_SESSION["ecom"]['view'] = $_SESSION["ecom"]['view'];
				
				
				$treemap = BusinessTreemap :: getInstance ("ecom", $_GET['id'], $_SESSION['ecom']['context'], $_GET['tech'], $_SESSION['ecom']['view']);
				//Renvoie des données à la vue
				$view -> assign('data', $data);
				$view -> assign('catcli', $treemap -> display());
				
				if(!isset($_SESSION['ecom']['catAvIdTree'])){
					$_SESSION['ecom']['catAvIdTree'] = $skf -> getOneCateg();
				}

				//CATEGORIES AVAHIS
				$data = $skf->getSsCatByParentId();
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);
				
				$listProdAssoc = $db -> getAllAssocProds($_SESSION['ecom']['vendor']);
				
				$listProdRefused = $db -> getAllRefusedProds($_SESSION['ecom']['vendor']);
				
				$view -> assign("listProdAssoc", $listProdAssoc);
				$view -> assign("listProdRefused", $listProdRefused);
			break;
			
			
			case '/showcatCli':
				
				$dispatch=true;
				
				//GROSSISTE
				if(isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "Grossiste"){$typev = "gr";}
				//Avahis GP
				elseif ( isset($_SESSION['typeVendor']) && $_SESSION['typeVendor'] == "avahisGP" ) {
					$typev = "avaGP";
				}
				//ECOMMERCANT	
				else{ $typev = "ecom";}
				
				!isset($_SESSION[$typev]['view']) ? $_SESSION[$typev]['view'] = "grid" : $_SESSION[$typev]['view'] = $_SESSION[$typev]['view'];
				if(empty($_POST['context']))$_POST['context']="all";
				
				$treemap = BusinessTreemap::getInstance ($typev, $_SESSION[$typev]['vendor'], $_POST['context'], $_SESSION[$typev]['tech'], $_SESSION[$typev]['view']); 
				
				$arr['treeview'] = $treemap -> display(); 
				if(isset($_SESSION[$typev]['cat'])){
					$arr['cat'] = $_SESSION[$typev]['cat'];
				};
				
				$out = json_encode($arr);
			break;			
			
			
			//Mise en forme de la page des grossistes avec création des treemap pour le coté grossiste
			//et le coté Avahis
			case    "/grossistes/grossiste/assocdealer" :
				$_SESSION['typeVendor'] = "Grossiste";
				$ACCESS_key = "grossistes.assocdealer..";
				$template = "assocGrossiste";
				$db = new BusinessSoukeoGrossisteModel();
				$_SESSION['gr']['tech'] = $_GET['tech'];
				
				
				$_SESSION['gr']['vendor'] = $_GET['id'];
				$categs = $db -> getDistinctCat($_GET['id']);
				
				$nomEcom = $db -> getValidGrossiste($_GET['id']);
				$view -> assign('vendName', $nomEcom['GROSSISTE_NOM']);
				
				if(!isset($_SESSION['gr']['context']))$_SESSION['gr']['context'] = "todo";
				if(empty($_POST['context']))$_POST['context']=$_SESSION['gr']['context'];
				!isset($_SESSION['gr']['view']) ? $_SESSION['gr']['view'] = "grid" : $_SESSION['gr']['view'] = $_SESSION['gr']['view'];
				
				$treemap = BusinessTreemap :: getInstance("gr", $_GET['id'], $_SESSION['gr']['context'], $_GET['tech'], $_SESSION['gr']['view']);

				$view -> assign('data', $data);
				$view -> assign('catcli', $treemap -> display());
				
				if(!isset($_SESSION['gr']['catAvIdTree'])){
					$_SESSION['gr']['catAvIdTree'] = $skf -> getOneCateg();
				}
				//CATEGORIES AVAHIS
				$data = $skf->getSsCatByParentId();
				$catAv = $view->showcat($data);
				$view -> assign("catav", $catAv);

				
				$view -> assign("listProdAssoc", $listProdAssoc);
				$view -> assign("listProdRefused", $listProdRefused);
			break;
			
			
			case "/commercants/ecommercant/listeProduits/prod" :
				//Mise à jours des données.
				$ACCESS_key = "commercants.ficheproduit..";
				$template = "qualificationProduit";


				if(isset($_GET['t'])){
					switch ($_GET['t']) {
						case 'TH':
							$skf_ecom = new BusinessSoukeoTheliaModel();
							break;
						case 'PS':
							$skf_ecom = new BusinessSoukeoPrestashopModel();
						default:
							
							break;
					}
				}
				$data = $skf_ecom -> getAllDataProductByVendor($_GET['vendor'], $_GET['prod']);
				$vendorData = $skf_ecom -> getValidVendor($_GET['vendor']);
				
				$view -> assign('skfModelProd', $data);
				foreach ($data['prod'] as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);

				}
				
				foreach ($vendorData as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);

				}				
			break;
			
			case '/getfile':
				$dispatch = true;
				
				$class = $_GET['class'];
				
				$csv = new $class();
				
				$csv -> makeContent();
				
				// Definition : generateCSV($delimiter, $withBOM = false, $streaming = false)
				$csv -> generateCSV(";", true, true);
				
				
			break;
			
			case '/getallfiles':
				$dispatch = true;
				global $root_path;
				
				$fileList = $skf -> getAllFileEntry();
				
				/*foreach ($fileList) {
					
				}*/
				
				$class = $_POST['className'];
				
				$csv = new $class();
				
				$fichier = $csv -> getPath() . $csv -> getFile_name();
				$path = $csv -> getPath();
				$file_name= $csv -> getFile_name();
				
				$html = "<p><a style='font-size:0.8em;text-align:center' 
								href='". $root_path . "/../../o_services/download/download.php?path=". urlencode($path) . "&file=" . urlencode($file_name) . "'\">
							<img border=\"0\" align=\"absmiddle\" alt=\"" . ("Téléchargement du Fichier") . 
							"\" class=\"iconAction\" title= \"" . ("Téléchargement du Fichier") . "\" src=\"" . $skin_path . "images/picto/download.gif\">
							Telecharger le fichier</a></p>";
				$out = $html;
				
			break;	
				

			case '/facturation/commande':
			    $ACCESS_key ="facturation.commande..";
				$security_domain = "facturation";
				$domain_categ = "commande";
				$right_menu_visibility = true;
			
				$grid = new OrdersGrid();				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./facturation/commande/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case '/avahis/commande_avahis':
			    $ACCESS_key ="avahis.commande_avahis..";
				$security_domain = "avahis";
				$domain_categ = "commande_avahis";
				$right_menu_visibility = true;
			
				$grid = new OrdersAvahisGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./avahis/commande_avahis/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;

			case '/grossistes/commande_grossiste':
			    $ACCESS_key ="grossistes.commande_grossiste..";
				$security_domain = "grossistes";
				$domain_categ = "commande_grossiste";
				$right_menu_visibility = true;

				$grid = new OrdersGrossisteGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./grossistes/commande_grossiste/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case '/commercants/commande_commercant':
			    $ACCESS_key ="commercants.commande_commercant..";
				$security_domain = "commercants";
				$domain_categ = "commande_commercant";
				$right_menu_visibility = true;

				$grid = new OrdersCommercantGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./commercants/commande_commercant/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case '/clients/listing_acheteurs':
			    $ACCESS_key ="clients.listing_acheteurs..";
				$security_domain = "clients";
				$domain_categ = "listing_acheteurs";
				$right_menu_visibility = true;

				$grid = new CustomersGrid("", $_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
			
				ob_start();
				include ("./clients/listing_acheteurs/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;

            case '/clients/listing_vendeurs':
                $ACCESS_key ="clients.listing_vendeurs..";
                $security_domain = "clients";
                $domain_categ = "listing_vendeurs";
                $right_menu_visibility = true;

                $grid = new VendorsGrid("", $_GET['id']);             
                $view -> assign("out", $grid -> display());
                $template = "rendered";
            
                ob_start();
                include ("./clients/listing_vendeurs/menu.php");
                $colright = ob_get_contents();
                ob_end_clean();
                
                $view -> assign("right", $colright);
                $tempCol = "colAdmin";
            break;

            case '/clients/listing_tickets':
                $ACCESS_key ="clients.listing_tickets..";
                $security_domain = "clients";
                $domain_categ = "listing_tickets";
                $right_menu_visibility = true;

                $grid = new TicketsGrid("", $_GET['id']);             
                $view -> assign("out", $grid -> display());
                $template = "rendered";
            
                ob_start();
                include ("./clients/listing_tickets/menu.php");
                $colright = ob_get_contents();
                ob_end_clean();
                
                $view -> assign("right", $colright);
                $tempCol = "colAdmin";
            break;
                        
            case '/clients/livraisons':
                
                $template = "modesDeLivraison";
                
                $typesLivraison = $livraisonTypeManager -> getAll();
                
                $view -> assign("typesLivraison", $typesLivraison);

            break;
            			
			case '/grossistes/annonce_reception':
			    $ACCESS_key ="grossistes.annonce_reception..";
				$security_domain = "grossistes";
				$domain_categ = "annonce_reception";
				$right_menu_visibility = true;

				$grid = new AnnonceReceptionGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./grossistes/annonce_reception/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case '/avahis/item_grossiste_to_order':
				$template = "prodGrossisteToOrder";
			    $ACCESS_key ="avahis.item_grossiste_to_order..";
				$security_domain = "avahis";
				$domain_categ = "item_grossiste_to_order";			
				$gridMAGI = new ProdGrossisteToOrderGrid(3, "MAGINEA");
				$gridLDLC = new ProdGrossisteToOrderGrid(2, "LDLC");							
				$gridFK = new ProdGrossisteToOrderGrid(1, "FK");	
								
				$view -> assign("gridMAGI", $gridMAGI -> display() );
				$view -> assign("gridLDLC", $gridLDLC -> display() );
				$view -> assign("gridFK",   $gridFK -> display() );
			break;

			case '/commercants/facturation':
			    $ACCESS_key ="commercants.facturation..";
				$security_domain = "commercants";
				$domain_categ = "facturation";
				$right_menu_visibility = true;

				$grid = new FacturationCommercantsGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./commercants/facturation/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
			
			case '/commercants/historique_factures_com':
				$ACCESS_key ="commercants.historique_factures_com..";
				$security_domain = "commercants";
				$domain_categ = "historique_factures_com";
				$right_menu_visibility = true;

				$grid = new HistoriqueFacturesComGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./commercants/historique_factures_com/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
															
			case '/loadChild':
				$dispatch = true;
				$right_menu_visibility = true;
				$temp = explode("_", $_POST['elm']);
				//var_dump($temp);
				switch ($temp[0]) {
					case 'com':
						$numCom = $temp[1];
						$grid = new PoGrid($numCom , true);			
						$out =  $grid -> display();	
					
						break;
					
					case 'po':
						$numCom = $temp[1];
						$grid = new PoGrid($numCom, true);			
						$out =  $grid -> display();	
					break;
					
					case 'item':
						$idPo = $temp[1];
						$vendor_id = $temp[2];
						$grid = new ItemGrid($idPo, $vendor_id, true);			
						$out =  $grid -> display();	
					
					break;
					
					case 'itemgr':
						$idOrderGrossiste = $temp[1];
						$vendor_id = $temp[2];
						$grid = new ItemGrossisteGrid($idOrderGrossiste, $vendor_id, true);			
						$out =  $grid -> display();	
					
					break;
					
					default:
						
						break;
				}
			break;	
			
			case '/litigeIt':
				$dispatch = true ;
				$num 	= $_POST['num'] ;
				$type 	= $_POST['type'] ;
				
				$action = $_GET['action'];
				
				switch ($type) {
					
					case 'com':
						$order = new BusinessOrder($num) ;	
						$order -> setLitiges($action);
						$order -> save();

						
						$listPo = $order -> getAllPo();
						foreach ($listPo as $po) {
							$po -> setLitiges($action);
							$po -> save();
							
							$listItems = $po -> getAllItems();
							foreach ($listItems as $item) {
								$item -> setLitiges($action);
								$item -> save();
							}	
						}
					
					break;
					
					case 'po':
						$po = new BusinessPo($num) ;
						$po -> setLitiges($action);
						$po -> save();
						
						$listItems = $po -> getAllItems();	
						foreach ($listItems as $item) {
								$item -> setLitiges($action);
								$item -> save();
						}
						
					break;
					
					case 'item':
						$item = new BusinessItem($num) ;
						$item -> setLitiges($action);
						$item -> save();
						
					break;
					
					default:
					break;
				}

			break;

			case '/payIt':
				$dispatch = true ;
				$num 	= $_POST['num'] ;
				$type 	= $_POST['type'] ;
				
				$action = $_GET['action'];
				switch ($type) {

					case 'po':
						$po = new BusinessPo($num) ;
						
						$itemList = $po -> getAllItems();
						
						$itemCount = count($itemList);
						$c = 0;
						
						foreach ($itemList as $item ) {
							if( ! $item -> getLitiges()){
								$item -> setPayment($action);					
								$item -> save();
								$c++;
							}		
						}
						if($action == "0"){
							$po -> setPayment("unpaid");
						}elseif($c > 0 && $c != $itemCount){
							$po -> setPayment("partiel");
						}elseif($c == $itemCount){
							$po -> setPayment("complete");
						}
						$po -> save();
					break;
					
					default:
					break;
				}

			break;
            
            case '/payFacture':
                $dispatch = true;
                $facture = $factureManager -> getFactureById($_POST['facture_id']);
                
                $itemList = $facture -> getAllItems();
                
                $line= 0;
                $litiges=0;
                foreach ($itemList as $item ) {
                    if( ! $item -> getLitiges()){
                        $item -> setPayment(1);                   
                        $item -> save();
                        $po = $item -> getPo();
                        $po -> checkPayment();
                        $po -> save();
                        
                        $line++;
                    }else{
                        $litiges++;
                    }       
                }
                
                $arr = array("payment" => $line, "litiges" => $litiges);
                
                $out = json_encode($arr);
                
                            
            break;
			
            case '/payAllFact':
                $dispatch = true;
                
                $line= 0;
                $litiges=0; 
                
                foreach ($_POST['facture'] as $facture_id) {
                   
                    $facture = $factureManager -> getFactureById($facture_id);
                   
                    $itemList = $facture -> getAllItems();
                

                   
                    foreach ($itemList as $item ) {
                        
                        if( ! $item -> getLitiges()){
                            $item -> setPayment(1);                   
                            $item -> save();
                            $item -> getPo();
                            $line++;
                        }else{
                            $litiges++;
                        }       
                    }
                }
                $arr = array("payment" => $line, "litiges" => $litiges);
                $out = json_encode($arr);

            break;
            
            			
			case '/assigncommgrossiste':
				$dispatch = true ;
					
				
				$external_code_grossiste= $_POST['external_code_grossiste'];
				
				if($order_id_grossiste = $skf -> getOrderIdGrossisteByCode($external_code_grossiste)){
					//var_dump($order_id_grossiste);
					$order_id_grossiste = $order_id_grossiste;
					
				}else{
					$order_id_grossiste = $skf -> insertOrderGrossiste(array("order_status" => "new",
																			 "order_external_code" => trim($external_code_grossiste),
																			 "grossiste_id" => "1",
																			 "estimated_price" => "0",
																			 "litiges" => "0",
																		));	
				}
				
				$orderGrossiste = new BusinessOrderGrossiste($order_id_grossiste);
				
				$itemIdArr = array();
				$estimatedPrice = 0;
				$estimatedWeight = 0;
				$arrJson = array();
				
				foreach ($_POST['po'] as $po_id) {
					$po = new BusinessPo($po_id);
					$listItems = $po -> getAllItems();
					
					foreach ($listItems as $item) {
						
						if(!in_array($item->getItem_id(), $itemIdArr)){
							$item -> setOrder_id_grossiste($order_id_grossiste);
							$item -> save();
							$itemIdArr[] = $item->getItem_id();	
						}	
					}
				}
				
				foreach ($_POST['item'] as $item_id) {
					
					if(!in_array($item_id, $itemIdArr)){
						$item = new BusinessItem($item_id);
						$item -> setOrder_id_grossiste($order_id_grossiste);
						$item -> save();
						$itemIdArr[] = $item->getItem_id();
					}
				}
				
				$allItems = $orderGrossiste -> getAllItems();
				foreach ($allItems as $item) {
					$estimatedPrice += $item -> getProductGrossiste() -> getPrice_product() * $item->getQty_ordered();
					$estimatedWeight += $item -> getProductGrossiste() -> getWeight() * $item->getQty_ordered();
				}
				$orderGrossiste -> setEstimated_price($estimatedPrice);
				$orderGrossiste -> setEstimated_weight($estimatedWeight);
				$orderGrossiste -> save();
				
				
				$arrJson ['po'] = $_POST['po'];
				$arrJson ['item'] = $itemIdArr; 
				$arrJson ['external_code'] = $external_code_grossiste;
				
				$out = json_encode($arrJson);
				
				
			break;
			
			case '/assignavisreception':
				
				$dispatch = true ;
				
				$annonce_reception_code = $_POST['code_recep'];

				if($annonceReception = $annonceReceptionManager -> getAnnonceReceptionByCode($annonce_reception_code)){
					$annonce_reception_id = $annonceReception -> getAnnonce_reception_id();
					
				}else{
					$annonceReception = new BusinessAnnonceReception(array(	"annonce_reception_code" => $annonce_reception_code,
																			"commentaires" => null ));
					$annonceReceptionManager -> save($annonceReception);
					$annonce_reception_id = $annonceReception -> getAnnonce_reception_id();					
				}	
				
				$itemIdArr = array();
				$arrJson = array();
				
				foreach ($_POST['ordergr'] as $order_grossiste_id) {
					$order_grossiste = new BusinessOrderGrossiste($order_grossiste_id);
					$listItems = $order_grossiste -> getAllItems();
					
					foreach ($listItems as $item) {
						
						if(!in_array($item->getItem_id(), $itemIdArr)){
							$item -> setAnnonce_reception_id($annonce_reception_id);
							$item -> save();
							$itemIdArr[] = $item->getItem_id();	
						}	
					}
				}
				
				foreach ($_POST['item'] as $item_id) {
					if(!in_array($item_id, $itemIdArr)){
						$item = new BusinessItem($item_id);
						$item -> setAnnonce_reception_id($annonce_reception_id);
						$item -> save();
						$itemIdArr[] = $item->getItem_id();
					}
				}
				
				$arrJson ['ordergr'] = $_POST['ordergr'];
				$arrJson ['item'] = $itemIdArr; 
				$arrJson ['code_recep'] = $annonce_reception_code;
				
				$out = json_encode($arrJson);
				
			break;
			
			case '/exportOpti':
				$dispatch = true;
				$orderGrossiste = new BusinessOrderGrossiste($_GET['order_id_grossiste']);
				
				$itemList = $orderGrossiste -> getAllItems();
				
				//Envoyer liste d'orders grossiste??? Plusieurs orders_gr sur un fichier opti?
				$excel = new BusinessExportOptiLog ();
				$excel -> makeContent($itemList, $orderGrossiste);
				
			break;
			
			
			case '/deleteIt':
				$dispatch = true;
				$num 	= $_POST['num'] ;
				$type 	= $_POST['type'] ;
				
				switch ($type) {
					
					case 'comgr':
						$orderGrossiste = new BusinessOrderGrossiste($num);
						$orderGrossiste -> delete();
					break;
					
					case 'po':

					break;
					
					case 'itemcomgr':
						
					$item = new BusinessItem($num);
					$item -> setOrder_id_grossiste(null);
					$item -> save();
						
					break;
					
					case 'ar':
						$annonceReception = $annonceReceptionManager -> getAnnonceReceptionById($num);
						$annonceReceptionManager -> delete($annonceReception);
					break;
                    
                    case 'ticket':
                        $ticket = $ticketManager -> getById(array("ticket_id" => $num));
                        $ticketManager -> delete($ticket);
                        $out = $ticket -> getTicket_code();
                    break;
					
					default:
					break;
				}
			break;
			
			case '/loadComments':
				$dispatch = true;
				$type 	= $_POST['type'];
				$id 	= $_POST['id'];
				
				$commentManager = new ManagerComment();
				
				switch ($type) {
					case 'po':
						$header = $view -> getCommentForm($type, $id);
						$comment_list = $commentManager -> getAllCommentsForObject($id, "skf_po");
						$content = $view -> showCommentList($comment_list, $type, $id);
					break;

					case 'item':
						$header = $view -> getCommentForm($type, $id);
						$comment_list = $commentManager -> getAllCommentsForObject($id, "skf_order_items");
						$content = $view -> showCommentList($comment_list, $type, $id);
					break;
					
					case 'order':
						$header = $view -> getCommentForm($type, $id);
						$comment_list = $commentManager -> getAllCommentsForObject($id, "skf_orders");
						$content = $view -> showCommentList($comment_list, $type, $id);
					break;

					case 'ordergr':
						$header = $view -> getCommentForm($type, $id);
						$comment_list = $commentManager -> getAllCommentsForObject($id, "skf_orders_grossiste");
						$content = $view -> showCommentList($comment_list, $type, $id);
					break;
										
					case 'cust':
						$header = $view -> getCommentForm($type, $id);
						$comment_list = $commentManager -> getAllCommentsForObject($id, "skf_customers");
						$content = $view -> showCommentList($comment_list, $type, $id);
					break;
					
					case 'vendor':
						$header = $view -> getCommentForm($type, $id);
						$comment_list = $commentManager -> getAllCommentsForObject($id, "sfk_vendor");
						$content = $view -> showCommentList($comment_list, $type, $id);
					break;
                    
                    case 'ticket':
                        $header = $view -> getCommentForm($type, $id);
                        $comment_list = $commentManager -> getAllCommentsForObject($id, "skf_tickets");
                        $content = $view -> showCommentList($comment_list, $type, $id);
                    break;
															
					default:
						
					break;
				}
				//$out = '<div id="myModal_'.$type.'_'.$id.'" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >'.$header.$content.'</div>';		
				$out = $header.$content;
			break;
			
			case '/saveComment':
			    $user_id = $user->getUserId();
				$dispatch = true;
				$commentManager = new ManagerComment();
				
				switch ($_POST['type']) {
					case 'po':
						$commented_object_table = "skf_po";
					break;

					case 'item':
						$commented_object_table = "skf_order_items";
					break;
					
					case 'order':
						$commented_object_table = "skf_orders";
					break;

					case 'ordergr':
						$commented_object_table = "skf_orders_grossiste";
					break;
										
					case 'cust':
						$commented_object_table = "skf_customers";
					break;

					case 'vendor':
						$commented_object_table = "sfk_vendor";
					break;
					
                    case 'ticket':
                        $commented_object_table = "skf_tickets";
                    break;
                    										
					default:
						
					break;
				}
				
				$comment = new BusinessComment($data = array("user_id" => 1, 
															 "content" => $_POST['content'],
															 "user_id" => $user_id,  
															 "commented_object_id" => $_POST['id'], 
															 "commented_object_table" => $commented_object_table ));
				$commentManager -> save($comment);
				
				
				$out = json_encode(array("author" => $comment->getAuthor(),
										 "content"=> $comment->getContent(),
										 "date_created"=> smartdate($comment -> getDate_created()),
										 "id_comment"  => $comment -> getId_comment() ));
			break;
			
			case '/editComment':
				$dispatch = true;
				
				$comment = $commentManager -> getCommentById($_POST['id_comment']);
				$comment -> setContent($_POST['content']);
				
				$commentManager -> save($comment);

				$out = json_encode(array("author" => $comment->getAuthor(),
										 "content"=> $comment->getContent(),
										 "date_created"=> smartdate($comment -> getDate_created()),
										 "id_comment"  => $comment -> getId_comment(),
										 "date_updated" => smartdate($comment -> getDate_updated())));
			break;
			
			case '/deleteComment':
				$dispatch = true;
				
				$comment = $commentManager -> getCommentById($_POST['id_comment']);
				$commentManager -> delete($comment);
					
			break;
			
            case '/clients/client':
                
                $template = "client";
                
                if($_GET['type'] == "customer"){
                    
                    $customer = $customerManager -> getBy(array("entity_id" =>$_GET['id']));
                    
                    if($customer -> getCustomer_id() > 40000){
                        $orderList = $customer -> getAllOrdersByMail();
                    }
                    elseif(!$customer -> getCustomer_id()){
                        $orderList = $customer -> getAllOrdersByMail();
                    }
                    else{
                        $orderList = $customer -> getAllOrders();
                    }
                    
                    $view -> assign("clientInfo", $view -> showCustomerInfo($customer, $orderList));                    
                }
                elseif($_GET['type'] == "vendor"){
                    
                    $vendor = $vendorManager -> getBy(array("entity_id" =>$_GET['id']));
                    
                    if($vendor -> getVendor_id()){
                        $orderList = $vendor -> getAllPo();
                    }
                    else{
                        $orderList = null;
                    }
                    
                    $view -> assign("clientInfo", $view -> showVendorInfo($vendor, $orderList));        
                }

            break;
            
            case '/refreshHistoCreditVendor':
                $dispatch = true;
                $vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']));
                $creditsHistory = $vendor -> getCreditsHistory();
                
                $out = $view ->getHistoCreditsVendor($vendor, $creditsHistory);
                
            break;
            
            case '/ticket_edit':
                
                var_dump($_POST);
                if(!empty($_POST)){
               
                    //Nouveaux observateurs ou pas
                    $ticketUsers = $ticketUserManager -> getAll(array("ticket_id" => $_POST['id']));    
                    $observers = array();
                    foreach ($ticketUsers as $ticketUser) {
                        $observers[] = $ticketUser -> getUser_id();
                    }
                   
                    $list_obs = explode(",",$_POST["listobs"]);
                   
                    sort($observers);            
                    sort($list_obs);     
    
                    if($observers != $list_obs){
                       
                        foreach ($ticketUsers as $ticketUser) {
                            $ticketUserManager -> delete($ticketUser);
                        }
                       
                        foreach ($list_obs as $obs_id) {
                            if($obs_id){
                                $ticketUser = new BusinessTicketUser(array("user_id" => $obs_id, "ticket_id" => $_POST['id'], "seen" => 0));
                                $ticketUserManager -> save($ticketUser);
                            }
                        }
                   }
                   
                   $_GET['id'] = $_POST['id'];
                   
                   $ticket = $ticketManager -> getById(array("ticket_id" => $_GET['id']));
       
                   $atleastOneUpdate = false;
               
                   if($_POST['select_state'] != $ticket -> getTicket_state_id()){
                       $atleastOneUpdate = true;
                   }
                   if($_POST['select_priority'] != $ticket -> getTicket_priority_id()){
                       $atleastOneUpdate = true;
                   }
                   
                   if($_POST['listuser'] == 0) $_POST['listuser'] = null;
                   if($_POST['listuser'] != $ticket -> getAssigned_user_id()){
                       $atleastOneUpdate = true;
                   }
                   if(!empty($_POST['commentaire']) && trim($_POST['commentaire']) != ""){
                       $atleastOneUpdate = true;
                   }
                   
                   
                   if($atleastOneUpdate){
                       $ticketHistory = new BusinessTicketHistory(array("ticket_id" => $_POST['id'],
                                                                        "creator_user_id" => $user -> getUserId(),
                                                                        "user_name" => $user -> getFirstname()));
                       
                       if($_POST['select_state'] != $ticket -> getTicket_state_id()){
                           $ticketHistory -> setState_id_old($ticket -> getTicket_state_id());
                           $ticketHistory -> setState_id_change($_POST['select_state']);
                       }
                       
                       if($_POST['select_priority'] != $ticket -> getTicket_priority_id()){
                           $ticketHistory -> setPriority_id_old($ticket -> getTicket_priority_id());
                           $ticketHistory -> setPriority_id_change($_POST['select_priority']);
                       }
                                              
                       if($_POST['listuser'] != $ticket -> getAssigned_user_id()){
                           $ticketHistory -> setTicket_assign_old($ticket -> getAssigned_user_id());
                           $ticketHistory -> setTicket_assign_change($_POST['listuser']);
                       }
                       
                       if(trim($_POST['commentaire']) != ""){
                           $ticketHistory -> setCommentaire($_POST['commentaire']);
                           $_POST['public'] ? $visibility = 1 : $visibility = 0;
                           $ticketHistory -> setTicket_history_visibility($visibility);
                       }                       
                       
                       
                       
                       $ticketHistoryManager -> save($ticketHistory);
                       
                       $ticket -> sendMail("<p>Le ticket N° ".$ticket->getTicket_code()." a été mis à jour par ".$user->getName()."</p>".$ticketHistory->getChanges_html());
                       
                       
                       if($ticket->getTicket_visibility() && $_POST['public'] && $_POST['commentaire'] != ""){
                            $ticket->sendMailClient($ticketHistory->getUser_name());
                       }
                   }
                    
                   if($_POST['select_state'] != $ticket -> getTicket_state_id()){
                       $ticket -> setTicket_state_id($_POST['select_state']);
                   }
                   
                   if($_POST['select_priority'] != $ticket -> getTicket_priority_id()){
                       $ticket -> setTicket_priority_id($_POST['select_priority']);
                   }
                   
                   if($_POST['listuser'] != $ticket -> getAssigned_user_id()){
                       $ticket -> setAssigned_user_id($_POST['listuser']);
                   }

                   if($_POST['select_TicketType'] != $ticket -> getTicket_type_id()){
                       $ticket -> setTicket_type_id($_POST['select_TicketType']);
                   }
                                                         
                   $ticketManager->save($ticket);
                   
                   header('Location: ticket?id='.$_POST['id']);
                       
                   
               }
            break;
            
            case '/ticket':
	           
               $template = "ticket";
               
               if($_GET['id']){
                   
                   if(!$ticket = $ticketManager -> getById(array("ticket_id" => $_GET['id']))){
                       header('Location: app.php');
                   }
                   
                   if($ticket -> getTicket_entity_id()){
                      $entity = $entityCrmManager -> getById(array("entity_id" => $ticket -> getTicket_entity_id()));
                      $type = $entityTypeManager -> getById(array("type_entity_id" => $entity -> getType_entity_id()));
                    
                      if($type -> getLabel() == "customer"){
                         $customer = $customerManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                         $ticketClient = "<a href='clients/client?type=customer&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($customer->getName())."</a> (A)";
                      }elseif($type -> getLabel() == "vendor"){
                         $vendor = $vendorManager ->getById(array("entity_id" => $entity-> getEntity_id()));
                         $ticketClient = "<a href='clients/client?type=vendor&id=".$entity-> getEntity_id()."' id='".$entity-> getEntity_id()."'>".htmlentities($vendor->getVendor_nom())."</a> (V)"; 
                      }
                    
                   }else{
                      $ticketClient = "<em> Global </em>";
                   } 
                   
                   $ticketRelances = $relanceManager -> getAll(array("ticket_id" => $_GET['id']));
                   
                   $ticketHistory = $ticketHistoryManager -> getAll(array("ticket_id" => $_GET['id']), " ORDER BY date_created ASC ");
                    
                   if($creator = $userManager -> getById(array("user_id" => $ticket -> getTicket_creator_id()))){
                       $creatorName = $creator->getName();
                   }else{
                       $creatorName = "Client";
                   }
                   
                   $assigned_user = $userManager -> getById(array("user_id" => $ticket -> getAssigned_user_id()));
                   
                   $listTicketUser = $ticketUserManager -> getAll(array("ticket_id" => $_GET['id'] ));
                   
                   $listUser = array();
                   
                   $priorityList = $ticketPriorityManager -> getAll();
                   $stateList = $ticketStateManager -> getAll();
                   
                   $res = dbQueryAll("SELECT DISTINCT user_id as id, CONCAT(firstname, ' ', lastname) as text FROM c_user");
                
                   $selected = array();
                   $obsselected = array();
                    
                   $selected[] = $ticket -> getAssigned_user_id();
                   
                   foreach ($listTicketUser as $ticketUser) {
                       $obsselected[] = $ticketUser -> getUser_id();
                       
                       if($ticketUser -> getUser_id() == $user ->getUserId()){
                           $ticketUser -> setSeen(1);
                           $ticketUserManager -> save($ticketUser);
                       }
                   }
                   
                   foreach ($listTicketUser as $ticketUser) {
                        $listObservateur[] = $userManager -> getById(array("user_id" => $ticketUser-> getUser_id()));
                   }
                    
                   $data_to_send = array("data" => $res, "selected" => $selected, "obsselected" => $obsselected);
                   $json = json_encode($data_to_send);

                   $view -> assign("ticket", $ticket);
                   $view -> assign("ticketHistory", $ticketHistory);  
                   $view -> assign("ticketRelances", $ticketRelances);                 
                   $view -> assign("creatorName", $creatorName);
                   $view -> assign("listObservateur", $listObservateur);
                   $view -> assign("json", $json);
                   $view -> assign("priorityList", $priorityList);
                   $view -> assign("stateList", $stateList);
                   $view -> assign("assigned_user", $assigned_user);
                   $view -> assign("ticketClient", $ticketClient);
               }
               
	        break;
            
            case '/refreshMiniRelanceTicketList':
                
               $dispatch = true;
	           if($ticket = $ticketManager -> getBy(array("ticket_id" => $_POST['ticket_id']))){
	              
                  if($relance_list = $relanceManager -> getAll(array("ticket_id"=> $_POST['ticket_id']))){
                      $out = $view ->getMiniRelanceListTable ($relance_list, $ticket);
                  } else {
                      $out = "Aucune relance";
                  }
	           }
	        break;
            
			case '/loadCustomersInfo':
				$dispatch = true;

				$customer = $customerManager -> getBy(array("entity_id" =>$_POST['elm']));
                
                if($customer -> getCustomer_id() > 40000){
                    $orderList = $customer -> getAllOrdersByMail();
                }
                elseif(!$customer -> getCustomer_id()){
                    $orderList = $customer -> getAllOrdersByMail();
                }
                else{
                    $orderList = $customer -> getAllOrders();
                }
				$out = $view -> showCustomerInfo($customer, $orderList);

			break;	

            case '/loadVendorsInfo':
                $dispatch = true;

                $vendor = $vendorManager -> getBy(array("entity_id" =>$_POST['elm']));
                
                if($vendor -> getVendor_id()){
                    $orderList = $vendor -> getAllPo();
                }
                else{
                    $orderList = null;
                }
                $out = $view -> showVendorInfo($vendor, $orderList);

            break;
             
			case '/loadOrderAvahisInfo':
				$dispatch = true;
				
				$po = new BusinessPo ($_POST['po_id']);
				$order = new BusinessOrder ($_POST['order_id']);
				$itemList = $po -> getAllItems();
				
				$out = $view -> showOrderAvahisInfo($po, $order, $itemList);

			break;	
			
			case '/loadAnnonceRepInfo':
				$dispatch = true;
				
				$annonceReception = $annonceReceptionManager -> getAnnonceReceptionById($_POST['ar_id']);
				
				$itemList = $annonceReception -> getAllItems();
				
				$out = $view -> showAnnonceReceptionInfo($annonceReception, $itemList);
			break;
			
			case '/loadOrderVendorInfo':
				$dispatch = true;
				
				if($_POST['next'] == "next"){
					$date = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
            		$date = $date['MIN(date_value)']; 
					$poList = $skf -> getAllPoByVendorAfterDate($_POST['vendor_id'], $date);
					$out = $view -> showVendorPoList($poList, $_POST['vendor_id'],$date);
					
				}else{
					$poList = $skf -> getAllPoByVendor($_POST['vendor_id']);
					$out = $view -> showVendorPoList($poList, $_POST['vendor_id']);					
				}
			break;
			
			case '/loadPoVendorInfo':
				$dispatch = true;
				
				$po = new BusinessPo($_POST['po_id']);
				
				$itemList = $po -> getAllItems();
				
				$out = $view -> showVendorPoItemList($itemList, $po);

			break;
					
            case '/loadAbonnementVendorInfo':
                $dispatch = true;
                 
                $vendor = $vendorManager->getBy(array("vendor_id" => $_POST['vendor_id']));
                
                $abonnement = $abonnementManager -> getById(array("abonnement_id" => $vendor->getAbonnement()));
                
                $nbProductsSold = $skf -> getCountProductsInSaleByVendor($_POST['vendor_id']); 

                
                $out = $view -> showAbonnementVendorInfo($vendor, $abonnement, (int)$nbProductsSold);
                
                
            break;
            
            case '/editVendorProductLimit':
                $dispatch = true;
                
                $vendor = $vendorManager->getBy(array("vendor_id" => $_POST['vendor_id']));
                
                $vendor -> setProduct_limit($_POST['val']);
                
                $vendorManager->save($vendor);

            break;
            
            case '/editTypeVendor':
                $vendor = $vendorManager->getBy(array("entity_id" => $_POST['entity_id']));
                
                $vendor -> setVendor_type($_POST['vendor_type']);
                
                $vendorManager -> save($vendor);
            break;

            case '/editStatutVendor':
                $vendor = $vendorManager->getBy(array("entity_id" => $_POST['entity_id']));
                
                $vendor -> setStatut($_POST['statut']);
                
                $vendorManager -> save($vendor);
            break;
            
            case '/editStatutCustomer':
                $customer = $customerManager->getBy(array("entity_id" => $_POST['entity_id']));
                
                $customer -> setStatut($_POST['statut']);
                
                $customerManager -> save($customer);
            break;
                        
            case '/editCommCateg':
                $dispatch = true;

                
                $categ = $categorieManager->getCategorieByCategorieId($_POST['cat_id']);
            
                $categ -> setComm($_POST['val']);
                
                $categorieManager->save($categ);
                
                
            break;
            
            case '/editcustomer':
                $dispatch = true;
                
                $customer = $customerManager -> getById(array("entity_id" => $_POST['entity_id']));
                
                $func = "set".ucfirst($_GET['property']);
                
                $customer -> $func($_POST['val']);
                
                $customerManager -> save($customer);
                
            break;
            
            case '/editvendor':
                $dispatch = true;
                
                $vendor = $vendorManager -> getById(array("entity_id" => $_POST['entity_id']));
                
                $func = "set".ucfirst($_GET['property']);
                
                $vendor -> $func($_POST['val']);
                
                $vendorManager -> save($vendor);
                
                if($vendor->getVendor_id()){
                    $vendorManager -> saveDbAvahis($vendor);
                }
                
            break;
            
			case '/changeStatusOrderGrossiste':
				
				$dispatch = true;
				$orderGrossiste = new BusinessOrderGrossiste($_POST['order_id']);
				
				$orderGrossiste -> setOrder_status($_POST['state']);	
				
				$orderGrossiste -> save();

			break;
            
            case '/changeStateTicket':
                $dispatch = true;
                $ticketState = $ticketStateManager -> getBy(array("label" => trim(strtolower($_POST['state']))));

                $ticket = $ticketManager -> getById(array("ticket_id" => $_POST['ticket_id']));

                $ticket -> setTicket_state_id($ticketState -> getState_id());    
                $ticketManager -> save($ticket);
            break;
            
            case '/changePriorityTicket':
                $dispatch = true;
                $ticketPriority = $ticketPriorityManager -> getBy(array("label" => trim(strtolower($_POST['priority']))));
                
                 $ticket = $ticketManager -> getById(array("ticket_id" => $_POST['ticket_id']));

                $ticket -> setTicket_priority_id($ticketPriority -> getPriority_id());    
                $ticketManager -> save($ticket);
            break;

            case '/changeTypeAboVendor':
                
                $dispatch = true;
                $vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']));
                
                $vendor -> setAbonnement($_POST['abonnement_id']);    
                
                $vendorManager -> save($vendor);
                
                $abo = $abonnementManager -> getBy(array("abonnement_id" => $_POST['abonnement_id']));
                
                $html = ucfirst($abo->getLabel()).'<span class="caret"></span>';
                
                switch ($_POST['abonnement_id']) {
                    case '1':
                        $class="brass";
                    break;
                    
                    case '2':
                        $class="silver";
                    break;
                        
                    case '3':
                        $class="gold";    
                    break;
                    default:
                    break;
                } 
                
                $json = array("html" => $html, "class" => $class);
                
                $out = json_encode($json);

            break;
            
			case '/changeStatusAnnonceReception':
				
				$dispatch = true;
				$annonceReception = $annonceReceptionManager -> getAnnonceReceptionById($_POST['ar_id']);
				
				$annonceReception -> setStatus($_POST['state']);	
				
				if($_POST['state'] == "Receptionne"){
					$annonceReception -> setDate_reception(date('Y-m-d H:i:s'));
					$date = str_replace("&agrave;", "à",smartdate(date('Y-m-d H:i:s')));
				}else{
					$annonceReception -> setDate_reception(null);
				}
				
				$annonceReceptionManager -> save($annonceReception);
				
				if($date) $out = $date;

			break;
                
            case '/getAllUsers':
	           $dispatch = true;
                $res = dbQueryAll("SELECT DISTINCT user_id as id, CONCAT(firstname, ' ', lastname) as text FROM c_user");
                
                $listUser = $ticketUserManager -> getAll(array("ticket_id" => $_POST['ticket_id']));
                $selected = array();
                
                foreach ($listUser as $ticketUser) {
                    $selected[] = $ticketUser -> getUser_id();
                }
                
                $data_to_send = array("data" => $res, "selected" => $selected);
                $json = json_encode($data_to_send);
                $out = $json;
	        break;
              
            case '/getAllUnivers':
                $dispatch = true;
                $res = dbQueryAll("SELECT cat_id as id, cat_label as text FROM sfk_categorie WHERE cat_active = 1 AND cat_level = 2");

                $listVendorCateg = $vendorCategorieManager -> getAll(array("entity_id" => $_POST['entity_id']));                
                $selected = array();
                
                foreach ($listVendorCateg as $cat) {
                    $selected[] = $cat -> getCat_id();
                }
                
                $data_to_send = array("data" => $res, "selected" => $selected);
                $json = json_encode($data_to_send);
                $out = $json;
            break; 
                        
            case "/addCustomer" :
                $dispatch = true;
                
                $statusList = $customerStatutManager -> getAll();
                
                ob_start();
                include ("./clients/popup_new_customer/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;

            case "/getDialogEditPackCredit" :
                $dispatch = true;
                
                $pack_credit = $creditPackManager -> getById(array("pack_id" => $_POST['pack_id']));
                
                ob_start();
                include ("./o_admin/popup_edit_creditpack/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;

            case "/getDialogEditLivraisonType" :
                $dispatch = true;
                
                $livraisonType = $livraisonTypeManager -> getById(array("shipping_id" => $_POST['livraison_type_id']));
                
                ob_start();
                include ("./clients/popup_edit_livraison/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;
            

                        
            case '/editLivraisonType':
                $dispatch = true;
                
                if($livraisonType = $livraisonTypeManager -> getById(array("shipping_id" => $_POST['shipping_id']))){
                    $livraisonType -> setRefund($_POST['refund']);
                    
                    $livraisonTypeManager -> save($livraisonType);
                    
                    $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>'.$livraisonType->getShipping_code(). ' a bien été modifié ! </strong></div>';
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Une erreur est survenue. </strong><br/>Ce mode de livraison n existe plus !</div>';
                }    
            break;
                        
            case '/getModalConfirmDialog':
                $dispatch = true;
                
                ob_start();
                include ("./o_admin/popup_confirm_modal/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;
            
            case '/getDialogSupprPackCredit':
                $dispatch = true;
                
                $pack_credit = $creditPackManager -> getById(array("pack_id" => $_POST['pack_id']));
                
                ob_start();
                include ("./o_admin/popup_suppr_creditpack/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;                
            break;
            
            case '/editPackCredit':
                $dispatch = true;
                
                if($pack_credit = $creditPackManager -> getById(array("pack_id" => $_POST['pack_id']))){
                    
                    $pack_credit -> setLabel($_POST['label']);
                    $pack_credit -> setValue($_POST['value']);
                    $pack_credit -> setPrice($_POST['price']);
                
                    $creditPackManager -> save($pack_credit);
                    $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Le pack N° '.$_POST['pack_id']. ' a bien été modifié ! </strong></div>';    
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Une erreur est survenue. </strong><br/>Ce pack n existe pas !</div>';
                }
            break;
            
            case '/addPackCredit':
                $dispatch = true;
                
                $pack_credit = new BusinessCreditsPack(array("label" => $_POST['label'],
                                                             "value" => $_POST['value'],
                                                             "price" => $_POST['price']));
                
                $creditPackManager -> save($pack_credit);
                
                if($pack_credit -> getPack_id()){
                $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Le pack N° '.$pack_credit -> getPack_id(). ' a bien été crée ! </strong></div>';    
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erreur ! </strong><br/> Le pack de crédit n\'a pas été créé</div>';
                }
                
            break;
            
            case '/deletePackCredit':
                $dispatch = true;
                
                $pack_credit = $creditPackManager -> getById(array("pack_id" => $_POST['pack_id']));
                
                if($creditPackManager -> delete($pack_credit)){
                    $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Le pack N° '.$_POST['pack_id']. ' a bien été supprimé ! </strong></div>';    
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Une erreur est survenue. </strong><br/>Ce pack n existe pas !</div>';
                }                    
            break;
            
            case '/getAllCreditsPack':
                $dispatch = true;
                
                $packList = $creditPackManager -> getAll();
                
                $out = $view -> showCreditsPackTable($packList);
            break;
            
            case '/getAllLivraisonTypes':
                $dispatch = true;
                
                $livraisonTypeList = $livraisonTypeManager -> getAll();
                
                $out = $view -> showLivraisonTypesTable($livraisonTypeList);
            break;

            
            case "/getDialogEditReferentiel" :
                $dispatch = true;
                
                $class= "Manager". trim($_POST['entity']);
                $manager = new $class();
                
                $id = $manager->getId();
                $id = array_keys($id);
                $id = $id[0];
                $entity_to_edit = $manager -> getById(array($id => $_POST['entity_id']));
                
                ob_start();
                include ("./o_admin/popup_edit_referentiel/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;
            
            case '/editReferentiel':
                $dispatch = true;
                $class= "Manager". trim($_POST['entity']);
                $manager = new $class();
                
                $id = $manager->getId();
                $id = array_keys($id);
                $id = $id[0];
                
                if( $entity_to_edit = $manager -> getById(array($id => $_POST['entity_id']))){
                    
                    $entity_to_edit -> setLabel($_POST['label']);
                
                    $manager -> save($entity_to_edit);
                    $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>N° '.$_POST['entity_id']. ' a bien été modifié ! </strong></div>';    
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Une erreur est survenue. </strong><br/>Cette ligne n\'existe pas !</div>';
                }
            break;
            
            case '/addReferentiel':
                $dispatch = true;
                
                $class= "Manager". trim($_POST['entity']);
                $manager = new $class();
                
                $id = $manager->getId();
                $id = array_keys($id);
                $id = $id[0];
                $func_id = "get".ucfirst($id);
                
                $class= "Business". trim($_POST['entity']);
                $businessClass = new $class();
                
                $entity_to_add = new $businessClass(array("label" => $_POST['label']));
                
                $manager -> save($entity_to_add);
                
                if($entity_to_add -> $func_id()){
                $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Le '.trim($_POST['entity']).' N° '.$entity_to_add -> $func_id(). ' a bien été crée ! </strong></div>';    
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Erreur ! </strong><br/> La ligne n\'a pas été créé</div>';
                }
                
            break;   
                     
            case '/getAllReferentielEntity':
                $dispatch = true;
                
                $out = BusinessReferentielUtils::showReferentielsTable(trim($_POST['entity']));	
	        break;
            
            case '/getDialogSupprReferentiel':
                
                $dispatch = true;
                
                $class= "Manager". trim($_POST['entity']);
                $manager = new $class();
                
                $id = $manager->getId();
                $id = array_keys($id);
                $id = $id[0];
                
                $entity_to_delete = $manager -> getById(array($id => $_POST['entity_id']));
                
                ob_start();
                include ("./o_admin/popup_suppr_referentiel/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;                
            break;
            
            case '/deleteReferentiel':
                $dispatch = true;
                $class= "Manager". trim($_POST['entity']);
                $manager = new $class();
                
                $id = $manager->getId();
                $id = array_keys($id);
                $id = $id[0];
                               
                $entity_to_delete = $manager -> getById(array($id => trim($_POST['entity_id'])));
                
                if($manager -> delete($entity_to_delete)){
                    $out = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Ligne N° '.$_POST['entity_id']. ' a bien été supprimé ! </strong></div>';    
                }else{
                    $out = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Une erreur est survenue. </strong><br/>Cette ligne n existe pas !</div>';
                }   
            break;
            
                                              
            case "/editVendorCredits" :
                $dispatch = true;
                
                $vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']));
                
                $transactionTypes = $creditPackManager -> getAll();
                
                $paymentTypes = $paymentTypeManager -> getAll();
                
                ob_start();
                include ("./commercants/popup_edit_credits/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;
            
            case '/editVendorCustomCredit':
                
                $dispatch = true;
                
                if($_POST['custom_val'] > 0 ){
                    $type = "credit";
                }else{
                    $type = "debit";
                }
                
                if($vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']))){
                    
                    $creditHistory = new BusinessCreditsHistory(array("vendor_id" => $_POST['vendor_id'],
                                                                  "label" => $_POST['custom_label'],
                                                                  "amount" => $_POST['custom_val'],
                                                                  "type" => $type));
                                                                  
                    $creditHistoryManager -> save ($creditHistory);
                    
                    $vendor -> setSolde_credit( (int)($vendor->getSolde_credit() + (int)$_POST['custom_val']));
                    
                    $vendorManager -> save($vendor);    
                }
            break;
                                    
            case '/editVendorCustomCredit':
                
                $dispatch = true;
                
                if($_POST['custom_val'] > 0 ){
                    $type = "credit";
                }else{
                    $type = "debit";
                }
                
                if($vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']))){
                    
                    $creditHistory = new BusinessCreditsHistory(array("vendor_id" => $_POST['vendor_id'],
                                                                  "label" => $_POST['custom_label'],
                                                                  "amount" => $_POST['custom_val'],
                                                                  "type" => $type));
                                                                  
                    $creditHistoryManager -> save ($creditHistory);
                    
                    $vendor -> setSolde_credit( (int)($vendor->getSolde_credit() + (int)$_POST['custom_val']));
                    
                    $vendorManager -> save($vendor);    
                }
            break;
 
            case '/editVendorSelectCredit':
                $dispatch = true;
                
                if($vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']))){
                    
                    $creditsPack = $creditPackManager -> getById(array("pack_id" => $_POST['pack_id']));
                    
                    if($creditsPack-> getValue() > 0){
                        $type = "credit";
                    }else{
                        $type = "debit";
                    }
                    
                    $creditHistory = new BusinessCreditsHistory(array("vendor_id" => $_POST['vendor_id'],
                                                                  "label" => $creditsPack -> getLabel(),
                                                                  "amount" => $creditsPack -> getValue(),
                                                                  "type" => $type,
                                                                  "pack_id" => $creditsPack -> getPack_id(),
                                                                  "facture_id" => null));
                                                                  
                    $creditHistoryManager -> save ($creditHistory);
                    
                    $vendor -> setSolde_credit( (int)$vendor->getSolde_credit() + (int)$creditsPack -> getValue() );
                    
                    $vendorManager -> save($vendor);    
                    
                    
                    $addr_emeteur = new BusinessFactureAddress(array(   'name'     => $vendor ->getVendor_nom(),
                                                        'siret'    => SystemParams::getParam("marketplace*siret"),
                                                        'street'   => SystemParams::getParam("marketplace*street"),
                                                        'city'     => SystemParams::getParam("marketplace*city"),
                                                        'zip_code' => SystemParams::getParam("marketplace*zip"),
                                                        'country'  => SystemParams::getParam("marketplace*country")));
                                                                    
                    $addr_recipient = new BusinessFactureAddress(array( 'name'     => $vendor ->getVendor_nom(),
                                                                        'siret'    => $vendor ->getSiret(),
                                                                        'street'   => $vendor ->getStreet(),
                                                                        'city'     => $vendor ->getCity(),
                                                                        'zip_code' => $vendor ->getZip(),
                                                                        'country'  => $vendor ->getCountry_id()));
                    $factureAddressManager -> save($addr_recipient);
                    $factureAddressManager -> save($addr_emeteur);
                    
                    if($addr_recipient->getFacture_address_id()){
                        //Création Facture et Facture spécifique commercant
                        $facture = new BusinessFacture(array('emeteur_addr_id' => $addr_emeteur -> getFacture_address_id(),
                                                             'recipient_addr_id' => $addr_recipient -> getFacture_address_id()));
                        $factureManager -> save($facture);
                        
                        $facture -> setFacture_code('FSKOCOM'.date('Ymd').zerofill($facture -> getFacture_id()));
                        $factureManager -> save($facture);
                        $factureCredit = new BusinessFactureCredit(array('facture_id' => $facture -> getFacture_id(),
                                                                   'vendor_id'  => $_POST['vendor_id'],
                                                                   'total_prix' => $creditsPack -> getPrice(),
                                                                   'grand_total' => ($creditsPack -> getPrice() * 1.085),
                                                                   'email' => SystemParams::getParam("marketplace*mailVendeur"),
                                                                   'tel'   => SystemParams::getParam("marketplace*tel"),
                                                                   'email_send' => 0,
                                                                   'payment_type_id' => $_POST['payment_type_id'] ));
                                                                   
                        $factureCreditManager -> save($factureCredit);
                        
                        $creditHistory -> setFacture_id($facture -> getFacture_id());
                        
                        $creditHistoryManager -> save ($creditHistory);
                        
                        $factureCreditLine = new BusinessFactureCreditLine(array("line_id" => 1, 
                                                                                 "facture_credits_id" => $factureCredit -> getFacture_credit_id(),
                                                                                 "pack_id" => $creditsPack -> getPack_id(),
                                                                                 "qty" => 1,
                                                                                 "designation" => $creditsPack -> getLabel(),
                                                                                 "price_ht" => $creditsPack -> getPrice(),
                                                                                 "price_ttc" => $creditsPack -> getPrice()*1.085)
                                                                                 );
                                                                                 
                        $factureCreditLineManager -> save($factureCreditLine);            
                        
                        $tmp .= "<a href='/app.php/commercants/downloadfactCredit?id=".$facture -> getFacture_id()."' >Download PDF</a>";
                        
                        $mailFact = new BusinessFactureMail($facture -> getFacture_id(), "credit");
                   
                        $mailFact -> fillBodyCredit();
                        
                        $mailFact -> attachFacture();
                        
                        if($_POST['pack_id'] != "6" ){
                            if($mailFact -> sendMail()){
                               
                               $factureCredit -> setEmail_send(1);
                               
                               $factureCreditManager -> save($factureCredit);
                               
                               $tmp .= "<br/>Mail bien envoyé à ".$mailFact -> getVendor() -> getVendor_nom().", addresse <b>" . $mailFact -> getMail_client();
                            }else{
                               $tmp .= "<br/>Echec de l'envoi du mail à ".$mailFact -> getVendor() -> getVendor_nom().", addresse <b> " . $mailFact -> getMail_client() . "</b> ! <br/>" ;
                            }                            
                        }else{
                            $tmp .= "<br/>Facture crée mais pas envoyée au client. ";
                        }

                        $out = $tmp;                         
                    }else{
                        $out = "<strong style='color:red'>Erreur Facturation : Adresse du client non renseignée !<strong>";
                    }

                }
            break;
                        
                            
            case '/commercants/downloadfactCredit':
                
                $facture = $factureManager -> getFactureById($_GET['id']);
                
                $emeteur   = $factureAddressManager -> getFactureAddressById($facture -> getEmeteur_addr_id());
                $recipient = $factureAddressManager -> getFactureAddressById($facture -> getRecipient_addr_id());
                
                $factureCredit = $factureCreditManager -> getBy(array("facture_id" => $_GET['id']));
                
                $paymentType = $paymentTypeManager -> getById(array("payment_type_id" => $factureCredit -> getPayment_type_id()));
                
                $vendor = $vendorManager -> getBy(array("vendor_id" => $factureCredit -> getVendor_id()));
                
                $lines = $factureCredit -> getAllLines();
                
                $dispatch = true;
                ob_get_clean();
                ob_start();
                include(dirname(__FILE__).'/commercants/facturation/factucredit.php');
                $content = ob_get_clean();
                
                // convert in PDF
                require_once(dirname(__FILE__).'/thirdparty/html2pdf_v4.03/html2pdf.class.php');
                try
                {
                    $html2pdf = new HTML2PDF('P', 'A4', 'fr');
                    $html2pdf->setDefaultFont('Arial');
                    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
                    
                    $html2pdf->Output($facture -> getFacture_code().'.pdf');
                    
                }   
                catch(HTML2PDF_exception $e) {
                    echo $e;
                    exit;
                } 
            break;
                   
                        
            case "/addVendor" :
                $dispatch = true;
                
                $statusList = $vendorStatutManager -> getAll();
                
                ob_start();
                include ("./clients/popup_new_vendor/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;
                        
                        
            case '/insertCustomer':
                $dispatch = true;
                $entityType = $entityTypeManager -> getBy(array("label" => "customer"));

                $birthdate = DateTime::createFromFormat('Y-m-d', $_POST['birthd']);
                
                if($birthdate){
                    $birthd = $birthdate->format('Y-m-d');
                }else{
                    $birthd = null;
                }

                //Creation d'une entité
                $entity = new BusinessEntityCrm(array("type_entity_id" => $entityType -> getType_entity_id()));
                
                $entityCrmManager -> save($entity);
                
                $customer = new BusinessCustomer(array( "entity_id" => $entity->getEntity_id(),
                                                        "customer_id" => null,
                                                        "name" =>       $_POST['firstname']." ".$_POST['lastname'],
                                                        "company" =>    $_POST['company'],
                                                        "firstname" =>  $_POST['firstname'],
                                                        "lastname" =>   $_POST['lastname'],
                                                        "email" =>      $_POST['email'],
                                                        "phone" =>      $_POST['phone'],
                                                        "phone2" =>     $_POST['phone2'],
                                                        "birthd" =>     $birthd,
                                                        "statut" =>     $_POST['statut']));
           
                $customerManager -> save($customer);
                
                $out = $customer -> getEntity_id();
	        break;
            
            case '/getDescProd':
	           $dispatch = true;
               if($product = $catalogProductManager->getById(array("id_produit" => $_GET['id']))){
                   
                   ob_start();
                    //include("./catalogue/produit_avahis/description_html.php");
                    include("./catalogue/produit_avahis/description_html_new_design.php");
                    $content = ob_get_contents();
                    ob_end_clean();
                    $out = $content ;
               }
	        break;    
            
            case '/getAvahisView':
	           $dispatch = true;
               if($product = $catalogProductManager->getById(array("id_produit" => $_GET['id']))){
                   
                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }
                    
                    $univers_id = $skf -> getUniversForCat($product -> getCategories());
                    
                    $categorie_list = $skf -> getAllCatChildId($univers_id);
                    $categorie_list = substr($categorie_list, 0, -2);
                    
                    $results = $skf -> getAllAttrInCat($categorie_list);
                    
                    $tmp = array();
                    
                    if(!empty($results)){
                        foreach ($results as $attr) {
                            
                            $tmp[] = array("id" => $attr -> getId_attribut(), "text" => $attr -> getLabel_attr());        
                        }                    
                    }

                    $json = array("results" =>  $tmp);
                
                    $json = json_encode($json);
                    
                    ob_start();
                    //include("./catalogue/produit_avahis/index.php");
                    include("./catalogue/produit_avahis/index_new_design.php");
                    $content = ob_get_contents();
                    ob_end_clean();
                    $out = $content ;
               }
	        break;
            
            case '/getAttrCategorie' :
            
                $dispatch = true;

                $results = $skf -> getAllAttrByCategoriesProduct($_GET['id']);
                
                $json = array();
                
                if(!empty($results)){
                    foreach ($results as $rs) {
                        
                        $json[] = array("id" => $rs['id_attribut'], "text" => $rs['label_attr']);        
                    }                    
                }

                $tmp = array("results" =>  $json);
                
                $out = json_encode($tmp);
                //$out = $_POST['json'];
            break;
            
            case '/getAvahisViewComplete':
               $dispatch = true;
               if($product = $catalogProductManager->getById(array("id_produit" => $_GET['id']))){

                    if($product){
                        $prod_attr_list = $product -> getAllProductAttributes();    
                    }
                                   
                    ob_start();
                    //include("./catalogue/produit_avahis/indexcomplete.php");
                    include("./catalogue/produit_avahis/indexcomplete_new_design.php");
                    $content = ob_get_contents();
                    ob_end_clean();
                    $out = $content ;
               }                
            break;
                    
            case '/insertVendor':
            	$dispatch = true;
                $entityType = $entityTypeManager -> getBy(array("label" => "vendor"));
                
                //Creation d'une entité
                $entity = new BusinessEntityCrm(array("type_entity_id" => $entityType -> getType_entity_id()));
                
                $entityCrmManager -> save($entity);
                
                $vendor = new BusinessVendor(array(     "entity_id"     => $entity->getEntity_id(),
                                                        "vendor_id"     => null,
                                                        "vendor_nom"    => $_POST['vendor_nom'],
                                                        "vendor_attn2"  => $_POST['vendor_attn2'],
                                                        "vendor_attn"   => $_POST['vendor_attn'],
                                                        "siret"         => $_POST['siret'],
                                                        "email"         => $_POST['email'],
                                                        "telephone"     => $_POST['telephone'],
                                                        "street"        => $_POST['street'],
                                                        "city"          => $_POST['city'],
                                                        "zip"           => $_POST['zip'],
                                                        "country_id"    => $_POST['country_id'],
                                                        "product_limit" => 0 ,
                                                        "solde_credit"  => 0,
                                                        "statut"        => $_POST['statut'],
                                                        "vendor_type"   => $_POST['vendor_type'],
                                                        "civilite"      => $_POST['civilite'],
                                                        "new" => 1));
                
                                                        
                $vendorManager -> save($vendor);
                
                $out = $vendor -> getEntity_id();
            break;    
            
            case "/addTicket" :
                $dispatch = true;
                $res = dbQueryAll("SELECT DISTINCT user_id as id, CONCAT(firstname, ' ', lastname) as text FROM c_user");
                $jsondata = json_encode($res);
                
                $typeList = $clientTypeManager -> getAll();
                $priorityList = $ticketPriorityManager -> getAll();
                
                if($_POST['type'] == "customer"){
                    $customer = $customerManager -> getById(array("entity_id" => $_POST['entity_id']));
                    $thisClientType = $clientTypeManager -> getBy(array("label" => "customer"));
                }
                elseif($_POST['type'] == "vendor"){
                    
                    $vendor = $vendorManager -> getById(array("entity_id" => $_POST['entity_id']));
                    $thisClientType = $clientTypeManager -> getBy(array("label" => "vendor"));
                }
                ob_start();
                include ("./clients/popup_new_ticket/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;

            break;
            
            case '/ModifCatAllSelectedProds': 
            	$dispatch = true;
                ob_start();
                include ("./catalogue/produit/modif_all_cat/index.php");
                $popup_actif = ob_get_contents();
                ob_end_clean();
                $out = $popup_actif ;
            break;
            
            case '/getDialogRelance':
	           $dispatch = true;
               
               if($_POST['relance_id']){
                   
                   $relance = $relanceManager->getBy(array("relance_id" => $_POST['relance_id']));
                   $out = $view -> getPopUpDateRelance($_POST['ticket_id'], $relance);
               }else{
                   $out = $view -> getPopUpDateRelance($_POST['ticket_id']);    
               } 
               
                
	        break;
            
            case '/insertTicket':
	            $dispatch = true; 
                $ticketToken = uniqid(mt_rand(), true);
                
                $ticket = new BusinessTicket(array( "ticket_code" => ($_POST['entity_id'] + 100 ),
                                                    "ticket_creator_id" => $user ->getUserId(),
                                                    "ticket_state_id" => 1,
                                                    "ticket_entity_id" => $_POST['entity_id'],
                                                    "ticket_title" => $_POST['ticket_title'], 
                                                    "client_id" => $_POST['client_id'], 
                                                    "client_name" => $_POST['client_name'], 
                                                    "ticket_priority_id" => $_POST['ticket_priority_id'],
                                                    "ticket_type_id" => $_POST['ticket_type_id'], 
                                                    "ticket_text" => $_POST['ticket_text'], 
                                                    "client_type" => $_POST['client_type'],
                                                    "ticket_visibility" => $_POST['ticket_visibility'] ? $_POST['ticket_visibility'] : 0,
                                                    "ticket_token" => $ticketToken,
                                                    "assigned_user_id" => $_POST['assign_user_id'] ? $_POST['assign_user_id'] : null ));
               $ticketManager -> save($ticket);
               
               
               $ticket -> setTicket_code($ticket->getTicket_id() + 100);
               //var_dump($ticket);
               $ticketManager -> save($ticket);
               
               if($_POST['assign_list']){
                   foreach ($_POST['assign_list'] as $rs) {
                        $ticketUser = new BusinessTicketUser(array("user_id" => $rs['id'], "ticket_id" => $ticket->getTicket_id(), "seen" => 0));
                        $ticketUserManager -> save($ticketUser);
                        //NOTIFY USERS
                   }
               }
               $ticket -> sendMail();
               
               if($ticket->getTicket_visibility()){
                   $ticket -> sendMailClientCreation();
               }
	        break;
            
            case '/addObserverTicket':
                $dispatch = true;
                
                if(!$ticketUserManager -> getBy(array("ticket_id" => $_POST['ticket_id'],"user_id" => $user->getUserId()))){
                    
                    $ticketUser = new BusinessTicketUser(array("ticket_id" => $_POST['ticket_id'], "user_id" => $user->getUserId(), "seen" => 0));
                    
                    $ticketUserManager -> save($ticketUser);
                    
                    $out = 1;
                }
                else{
                    $out = 0;
                }
            break;
            
            case '/supprRelance':
                $dispatch = true;
                
                if($relance = $relanceManager -> getById(array("relance_id" => $_POST['relance_id'])) ){
                    $relanceManager->delete($relance);
                }	
	        break;
            
            case '/addRelanceTicket':
                $dispatch = true;
                
                if(isset($_POST['relance_id']) && !empty($_POST['relance_id'])){
                    if($relance = $relanceManager->getBy(array("relance_id"=>$_POST['relance_id']))){
                        
                        if($date_relance = DateTime::createFromFormat('d-m-Y', $_POST['date'])){
                            
                            $relance->setType_relance_id($_POST['type_relance_id']);
                            
                            $relance->setDate_relance($date_relance -> format("Y-m-d"));
                            
                            $relanceManager->save($relance);
                                                    
                        }else{
                            $out = "Erreur, format de date entré invalide !";
                        }    

                    }else{
                        $out = "Erreur relance inexistante !";
                    }    
                }else{
                    if($date_relance = DateTime::createFromFormat('d-m-Y', $_POST['date'])){
                    
                        $_POST['type_relance_id'] ? $type = $_POST['type_relance_id']: $type = null ;
                            
                        
                        $relance = new BusinessRelance(array("date_relance" => $date_relance -> format("Y-m-d"),
                                                             "type_relance_id" => $type,
                                                             "ticket_id" => $_POST['ticket_id']));
                        $relanceManager->save($relance);
                        
                        if($relance->getRelance_id()){
                            $out = "Relance bien sauvegardée";
                        }else{
                            $out = "Erreur lors de la création de la relance";
                        }                     
                    }
                    else{
                        $out = "Erreur, format de date entré invalide !";
                    }                    
                }
	
	        break;
            
            case '/setRelanceTermine':
                $dispatch = true;
                
                if($relance = $relanceManager -> getById(array("relance_id" => $_POST['relance_id'])) ){
                        
                    if($_POST['action'] == "Terminé"){
                        $relance->setDate_termine(" NOW() ");    
                        $relanceManager->save($relance);
                    }
                    elseif($_POST['action'] == "En cours"){
                        $relance->setDate_termine(null);
                        $relanceManager->save($relance);
                    }
                }   	           
	        break;
            
            case '/getFormEditCustomer':
	            $dispatch = true;
                //$typeList = $clientTypeManager -> getAll();
                $statutList = $customerStatutManager -> getAll();
                if($_POST['entity_id']){
                    $customer = $customerManager -> getById(array("entity_id" => $_POST['entity_id']));
                    $out = $view -> getFormEditCustomer($customer, $statutList);
                }
	        break;

	        case '/getFormEditVendor':
                $dispatch = true;
                //$typeList = $clientTypeManager -> getAll();
                //$statutList = $customerStatutManager -> getAll();
                if($_POST['entity_id']){
                    $vendor = $vendorManager -> getById(array("entity_id" => $_POST['entity_id']));
                    $out = $view -> getFormEditVendor($vendor);
                }
            break;
                        
            case '/exitFormEditCustomer':
                $dispatch = true;
                //$typeList = $clientTypeManager -> getAll();
                $statutList = $customerStatutManager -> getAll();
                if($_POST['entity_id']){
                    $customer = $customerManager -> getById(array("entity_id" => $_POST['entity_id']));
                       
                    if($customer -> getCustomer_id() && $customer -> getCustomer_id() < 400000){
                        $orderList = $customer -> getAllOrders();
                    }
                    else{
                        $orderList = $customer -> getAllOrdersByMail();
                    }
                    
                    $out = $view -> ficheInfoClient($customer, $orderList);
                }
            break;

            case '/exitFormEditVendor':
                $dispatch = true;

                if($_POST['entity_id']){
                    $vendor = $vendorManager -> getBy(array("entity_id" =>$_POST['entity_id']));
                
                    if($vendor -> getVendor_id()){
                        $orderList = $vendor -> getAllPo();
                    }
                    else{
                        $orderList = null;
                    }
                    $out = $view -> ficheInfoClientVendor($vendor, $orderList);
                }
            break;
                                    
            case '/validFormEditCustomer':
                
                $dispatch = true;
                 if($_POST['entity_id']){
                    $customer = $customerManager -> getById(array("entity_id" => $_POST['entity_id']));
                    
                    
                    $customer -> setStatut($_POST['statut'])
                              -> setEmail($_POST['email'])
                              -> setPhone($_POST['phone'])
                              -> setPhone2($_POST['phone2'])
                              -> setBirthd($_POST['birthd'])
                              -> setMore_info($_POST['more_info']);
                    
                    $customerManager -> save($customer);
                 }    
            break;
            
            case '/validFormEditVendor':
                $dispatch = true;
                 if($_POST['entity_id']){
                    $vendor = $vendorManager -> getById(array("entity_id" => $_POST['entity_id']));
                    
                    
                    $vendor -> setVendor_nom($_POST['vendor_nom']);
                    $vendor -> setSiret($_POST['siret']);
                    $vendor -> setStatut($_POST['statut']);
                    $vendor -> setVendor_type($_POST['vendor_type']);
                    $vendor -> setVendor_attn($_POST['vendor_attn']);
                    $vendor -> setVendor_attn2($_POST['vendor_attn2']);
                    $vendor -> setStreet($_POST['street']);
                    $vendor -> setCity($_POST['city']);
                    $vendor -> setZip($_POST['zip']);
                    $vendor -> setCountry_id($_POST['country_id']);
                    $vendor -> setEmail($_POST['email']);
                    $vendor -> setTelephone($_POST['telephone']);
                    $vendor -> setTelephone2($_POST['telephone2']);
                    $vendor -> setBirthd($_POST['birthd']);

                    $vendorManager -> save($vendor);
                 }                 
            break;
            
            case '/refreshInfoVendor':
                $dispatch = true;
                
                if($_POST['vendor_id']){
                    $vendor = $vendorManager -> getBy(array("vendor_id" =>$_POST['vendor_id']));
                
                    if($vendor -> getVendor_id()){
                        $orderList = $vendor -> getAllPo();
                    }
                    else{
                        $orderList = null;
                    }
                    $out = $view -> ficheInfoClientVendor($vendor, $orderList);
                }                
            break;
                        
            case '/assignTicketUser':
                $dispatch = true;
                
                $selected = array(); 
                
                foreach ($_POST['user_ids'] as $rs) {
                    $ticketUser = new BusinessTicketUser(array("user_id" => $rs['id'], "ticket_id" => $_POST['ticket_id'], "seen" => 0));
                    $ticketUserManager -> save($ticketUser);
                    $selected[] = $rs['id'];
                }
                
                //VERIFICATION DES MANQUANTS DE LA SELECTION ET SUPRESSION
                $listUser = $ticketUserManager -> getAll(array("ticket_id" => $_POST['ticket_id']));
                $assigned = array();
                foreach ($listUser as $ticketUser) {
                    $assigned[] = $ticketUser -> getUser_id();
                }
                
                //var_dump($assigned);
                
                foreach ($assigned as $id) {
                    if(!in_array($id, $selected)){
                        $ticketUser = new BusinessTicketUser(array("user_id" => $id, "ticket_id" => $_POST['ticket_id']));
                        $ticketUserManager -> delete($ticketUser);
                    }
                }    
            break;  
            
            case '/assignVendorCateg':
                $dispatch = true;
                
                $selected = array(); 
                
                foreach ($_POST['cat_ids'] as $rs) {
                    $vendorCateg = new BusinessVendorCategorie(array("cat_id" => $rs['id'], "entity_id" => $_POST['entity_id']));
                    $vendorCategorieManager -> save($vendorCateg);
                    $selected[] = $rs['id'];
                }
                
                //VERIFICATION DES MANQUANTS DE LA SELECTION ET SUPRESSION
                $listCatVendor = $vendorCategorieManager -> getAll(array("entity_id" => $_POST['entity_id']));
                $assigned = array();
                foreach ($listCatVendor as $vendorCateg) {
                    $assigned[] = $vendorCateg -> getCat_id();
                }
                
                //var_dump($assigned);
                
                foreach ($assigned as $id) {
                    if(!in_array($id, $selected)){
                        $vendorCateg = new BusinessVendorCategorie(array("cat_id" => $id, "entity_id" => $_POST['entity_id']));
                        $vendorCategorieManager -> delete($vendorCateg);
                    }
                }	
                
                $vendor_univers_list = $vendorCategorieManager -> getAll(array("entity_id" => $_POST['entity_id']));
                $str_univerlist = "";
                foreach ($vendor_univers_list as $vend_cat) {
                    $cat = $categorieManager -> getById(array("cat_id" => $vend_cat -> getCat_id()));
                    $str_univerlist .= " - ". $cat -> getCat_label();
                }
                
                $out = $str_univerlist;           
	        break;
			
            case '/refreshTicketList' :
                $dispatch = true;
                
                if($_GET['filter'] == "yes"){
                    $ticketList = $ticketManager -> getAllFilter(array( "ticket_entity_id" => $_POST['entity_id'],
                                                                        "ticket_code" => $_POST['ticket_code'],
                                                                        "client_name" => $_POST['client_name'], 
                                                                        "ticket_priority_id" => $_POST['ticket_priority_id'], 
                                                                        "ticket_state_id" => $_POST['ticket_state_id'],  ) );
                    if($_POST['user_id']){
                        $ticketUserList = $ticketUserManager -> getAll(array("user_id" => $_POST['user_id']));
                        $ticket_id_array = array();
                        foreach ($ticketUserList as $ticketUser) {
                            $ticket_id_array[] = $ticketUser -> getTicket_id();
                        }
                        $filteredTicketList = array();
                        foreach ($ticketList as $ticket) {
                            if(in_array($ticket -> getTicket_id(), $ticket_id_array)){
                                $filteredTicketList[] = $ticket;
                            }        
                        }
                        
                        $out = $view -> getTicketsListTable($filteredTicketList, $_POST['entity_id'], $_POST['user_id'], $_POST['client_name'], $_POST['ticket_priority_id'], $_POST['ticket_state_id']);
                    }else{
                        $out =  $view -> getTicketsListTable($ticketList, $_POST['entity_id'], $_POST['user_id'], $_POST['client_name'], $_POST['ticket_priority_id'], $_POST['ticket_state_id']);    
                    }
                }
                else {
                    $ticketList = $ticketManager -> getAll(array("ticket_entity_id" => $_POST['entity_id']) );
                    $out =  $view -> getTicketsListTable($ticketList, $_POST['entity_id']);    
                }
            break;
            
            case '/refreshRelanceTicketList':
                $dispatch = true;
                
                if($_GET['filter'] == "yes"){
                    $relanceTicketList = $user -> getAllRelanceTicketsFilter($_POST);
                    $out = $view -> getRelanceTicketsListTable($relanceTicketList, $_POST['client_name'], $_POST['relance_statut']);
                }else{
                    $relanceTicketList = $user -> getAllRelanceTickets();
                    $out = $view -> getRelanceTicketsListTable($relanceTicketList);    
                }

	        break;
            
            case '/refreshCustomerList':
	           $dispatch = true;
               
               $customerList = $customerManager -> getAllByFilters($_POST['entity_id'], $_POST['name']); 
               
               $out =  $view -> getMiniCustomersListTable($customerList, $_POST['entity_id'], $_POST['name']) ;
                
	        break;	
            
            case '/refreshVendorList':
               $dispatch = true;
               
               $vendorList = $vendorManager -> getAllByFilters($_POST['entity_id'], $_POST['name']); 
               
               $out =  $view -> getMiniVendorsListTable($vendorList, $_POST['entity_id'], $_POST['name']) ;	
	        break;
                
			case '/savefactucommercant':
				$dispatch = true;
				$req = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
				
				$date = new DateTime($req['MIN(date_value)']);
				$date -> modify('-15 day');
								
 				$vendor = $vendorManager -> getBy(array("vendor_id" => $_POST['vendor_id']));
				
				//Création des deux objets addresse commercant et Soukeo
				$addr_emeteur = new BusinessFactureAddress(array(	'name'     => $vendor ->getVendor_nom(),
																	'siret'    => SystemParams::getParam("marketplace*siret"),
																	'street'   => SystemParams::getParam("marketplace*street"),
																	'city'     => SystemParams::getParam("marketplace*city"),
																	'zip_code' => SystemParams::getParam("marketplace*zip"),
																	'country'  => SystemParams::getParam("marketplace*country")));
																	
				$addr_recipient = new BusinessFactureAddress(array(	'name'     => $vendor ->getVendor_nom(),
																	'siret'    => $vendor ->getSiret(),
																	'street'   => $vendor ->getStreet(),
																	'city'     => $vendor ->getCity(),
																	'zip_code' => $vendor ->getZip(),
																	'country'  => $vendor ->getCountry_id()));
				$factureAddressManager -> save($addr_recipient);
				$factureAddressManager -> save($addr_emeteur);
				
				//Création Facture et Facture spécifique commercant
				$facture = new BusinessFacture(array('emeteur_addr_id' => $addr_emeteur -> getFacture_address_id(),
													 'recipient_addr_id' => $addr_recipient -> getFacture_address_id()));
				$factureManager -> save($facture);
				$facture -> setFacture_code('FSKOCOM'.date('Ymd').zerofill($facture -> getFacture_id()));
				$factureManager -> save($facture);
				$factureCom = new BusinessFactureCom(array('facture_id' => $facture -> getFacture_id(),
														   'vendor_id'  => $_POST['vendor_id'],
														   'date_limit' => $date -> format('Y-m-d H:i:s'),
														   'email' => SystemParams::getParam("marketplace*mailVendeur"),
														   'tel'   => SystemParams::getParam("marketplace*tel")));
				$factureComManager -> save($factureCom);

				//Récupération de tous les PO à payer du vendeur
				$poList = $vendor -> getAllPoToPay(); 
				
				$total_prix = 0;
				$total_comm = 0;
				$total_frais_b = 0;
				$grand_total = 0;
				$countLine = 1;
				foreach ($poList as $po) {
					$order = new BusinessOrder($po->getOrder_id());
					$itemList = $po -> getAllItems();
					//Pour chaque item on va créer une ligne de facture
					foreach ($itemList as $item) {
						$comm = dbQueryOne('SELECT COMM FROM sfk_categorie WHERE cat_id = '.$item -> getCategory_id());
						$comm = $comm['COMM'];
						
						if(!$item -> getLitiges() && !$item -> getPayment() && in_array($item -> getItem_id(), $_POST['item_id']) ){
							
							$line = new BusinessFactureComLine(array("line_id" => $countLine,
																	 "facture_com_id" => $factureCom -> getFacture_com_id(),
																	 "item_id" => $item -> getItem_id(),
																	 "order_increment_id" => $order -> getIncrement_id(),
																	 "qty" => $item -> getQty_ordered(),
																	 "designation" => $item -> getName(),
																	 "price_ttc" => $item -> getBase_row_total_incl_tax(),
																	 "category_id" => $item -> getCategory_id(),
																	 "percent_comm" => $comm,
																	 "mt_comm" => $item -> getBase_row_total_incl_tax() * $comm,
																	 "mt_frais_banq" => $item -> getBase_row_total_incl_tax() * 0.005,
																	 "litiges" 		=> $item -> getLitiges()
																));
							$factureComLineManager -> save($line);
							
							$total_prix += $item -> getBase_row_total_incl_tax();
							$total_comm += $item -> getBase_row_total_incl_tax() * $comm;
							$total_frais_b += $item -> getBase_row_total_incl_tax() * 0.005;
							$grand_total += ($item -> getBase_row_total_incl_tax() * $comm) + ($item -> getBase_row_total_incl_tax() * 0.005);
							
							$item -> setFacture_id($facture -> getFacture_id());
							$item -> setPayment(1);
							$item -> save();
							
							$countLine ++ ;
							
						}elseif($item -> getLitiges() && !$item -> getPayment() && in_array($item -> getItem_id(), $_POST['item_id'])){
							$line = new BusinessFactureComLine(array("line_id" => $countLine,
																	 "facture_com_id" => $factureCom -> getFacture_com_id(),
																	 "item_id" => $item -> getItem_id(),
																	 "order_increment_id" => $order -> getIncrement_id(),
																	 "qty" => $item -> getQty_ordered(),
																	 "designation" => $item -> getName(),
																	 "price_ttc" => 0,
																	 "category_id" => $item -> getCategory_id(),
																	 "percent_comm" => $comm,
																	 "mt_comm" => 0,
																	 "mt_frais_banq" => 0,
																	 "litiges" 		=> $item -> getLitiges()
																));
							$factureComLineManager -> save($line);
							$item -> setFacture_id($facture -> getFacture_id());
							$item -> save();
							$countLine ++ ;
						}
					}
                    //Remboursement frais de port
                    if($po->isShippingRefund() &&  (float)$po->getBase_shipping_amount() > 0){
                        
                            $line = new BusinessFactureComLine(array("line_id" => $countLine,
                                                                     "facture_com_id" => $factureCom -> getFacture_com_id(),
                                                                     "item_id" => 0,
                                                                     "order_increment_id" => $order -> getIncrement_id(),
                                                                     "qty" => 1,
                                                                     "designation" => "Remboursement frais d'envoi ".numberByLang($po->getBase_shipping_amount())." €",
                                                                     "price_ttc" => $po->getBase_shipping_amount(),
                                                                     "category_id" => 0,
                                                                     "raison_litige" => "frais_envoi",
                                                                     "percent_comm" => 0,
                                                                     "mt_comm" => 0,
                                                                     "mt_frais_banq" => 0,
                                                                     "litiges"      => 0
                                                                ));
                            $factureComLineManager -> save($line);
                            
                            $total_prix += (float)$po->getBase_shipping_amount();

                            $countLine ++ ;
                    }
                    
                    //Après avoir passé sur tous les items on vérifie le paiement total/partiel du PO
                    $po->checkPayment();
                    $po->save();
				}
				//Création des lignes de facture pour les objets ajoutés en JS
				foreach ($_POST['added_lines'] as $added_line) {
					
					$line = new BusinessFactureComLine(array("line_id" => $countLine,
																	 "facture_com_id" => $factureCom -> getFacture_com_id(),
																	 "item_id" => 0,
																	 "order_increment_id" => "-",
																	 "qty" => 1,
																	 "designation" => $added_line['desig'],
																	 "price_ttc" => $added_line['prix'],
																	 "category_id" => 0,
																	 "percent_comm" => 0,
																	 "mt_comm" => 0,
																	 "mt_frais_banq" => 0,
																	 "litiges" => 0
																));
					$factureComLineManager -> save($line);
					
					$total_prix += $added_line['prix'];
					
					$countLine ++ ;
				}
				
				$factureCom -> setTotal_prix($total_prix);
				$factureCom -> setTotal_comm($total_comm);
				$factureCom -> setTotal_frais_banq($total_frais_b);
				$factureCom -> setGrand_total($grand_total);
				$factureCom -> setMt_a_transferer($total_prix  - ($grand_total * 1.085));
				
				$factureComManager -> save($factureCom);
				
				$tmp .= "<a href='". $root_path . "downloadfact?id=".$facture -> getFacture_id()."' >Download PDF</a>";
				
				$out = $tmp; 
			break;
							
			case '/commercants/downloadfact':
				$facture = $factureManager -> getFactureById($_GET['id']);
				
				$emeteur = $factureAddressManager -> getFactureAddressById($facture -> getEmeteur_addr_id());
				$recipient = $factureAddressManager -> getFactureAddressById($facture -> getRecipient_addr_id());
				
				$factureCom = $factureComManager -> getFactureComByFactureId($_GET['id']);
				
				$vendor = $vendorManager -> getBy(array("vendor_id" => $factureCom -> getVendor_id()));
				
				$lines = $factureComLineManager -> getAllFactureComLineForFactureCom($factureCom -> getFacture_com_id());
				
				
				$dispatch = true;
				ob_get_clean();
				ob_start();
    			include(dirname(__FILE__).'/commercants/facturation/factu.php');
    			$content = ob_get_clean();
				
    			// convert in PDF
    			require_once(dirname(__FILE__).'/thirdparty/html2pdf_v4.03/html2pdf.class.php');
    			try
    			{
        			$html2pdf = new HTML2PDF('P', 'A4', 'fr');
        			$html2pdf->setDefaultFont('Arial');
        			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
					
        			$html2pdf->Output($facture -> getFacture_code().'.pdf');
					
    			}	
    			catch(HTML2PDF_exception $e) {
        			echo $e;
        			exit;
    			} 
			break;
			
			case '/downloadAllFact' :
				$dispatch = true;
				
				$path = '/opt/soukeo/tmp';
				$zip_name = "Factures_com_".date('d-m-Y').'.zip';
				$zip = new ZipArchive;
				$res = $zip->open($path."/".$zip_name, ZipArchive::CREATE);
						
				foreach ($_POST['facture'] as $facture_id) {
						 
					$facture 	= $factureManager -> getFactureById($facture_id);				
					$emeteur 	= $factureAddressManager -> getFactureAddressById($facture -> getEmeteur_addr_id());
					$recipient 	= $factureAddressManager -> getFactureAddressById($facture -> getRecipient_addr_id());
					$factureCom = $factureComManager -> getFactureComByFactureId($facture_id);
					$vendor 	= $vendorManager -> getBy(array("vendor_id" => $factureCom -> getVendor_id()));
					$lines 		= $factureComLineManager -> getAllFactureComLineForFactureCom($factureCom -> getFacture_com_id());
					
					
					$dispatch = true;
					ob_get_clean();
					ob_start();
	    			include(dirname(__FILE__).'/commercants/facturation/factu.php');
	    			$content = ob_get_clean();
					
	    			// convert in PDF
	    			require_once(dirname(__FILE__).'/thirdparty/html2pdf_v4.03/html2pdf.class.php');
	    			try
	    			{
	        			$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	        			$html2pdf->setDefaultFont('Arial');
	        			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
						
						$content_PDF = $html2pdf->Output('', 'S');
						
						if ($res === TRUE) {
						    $zip->addFromString($facture -> getFacture_code().'.pdf', $content_PDF);
						}
					}	
	    			catch(HTML2PDF_exception $e) {
	        			echo $e;
	        			exit;
	    			}
				}
 				$zip->close();
				
				$path_url = urlencode($path);
				$file_url = urlencode($zip_name);
				$out = '<h4 class="well" text-align="center" ><a "="" href="../../../o_services/download/download.php?path='.$path_url.'&file='.$file_url.'">
							<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/download.gif" title="Téléchargement de la facture" alt="Téléchargement de la facture">
							 Telecharger 
							<img class="iconAction" border="0" align="absmiddle" src="/skins/common/images/picto/download.gif" title="Téléchargement de la facture" alt="Téléchargement de la facture">
						</a></h4>';

			break;
            
            case '/o_admin/statistiques/download_stats':
	           $dispatch = true;
               
               $csv = new BusinessExportStatistiques();
               $csv -> makeContent(); 
               $csv -> generateCSV(";", true, true);
                
	        break;
			
			case '/commercants/showLastFacture':
				$dispatch = true;
				
				$facture = $factureManager -> getFactureById($_GET['id']);
				
				$emeteur = $factureAddressManager -> getFactureAddressById($facture -> getEmeteur_addr_id());
				$recipient = $factureAddressManager -> getFactureAddressById($facture -> getRecipient_addr_id());
				
				$factureCom = $factureComManager -> getFactureComByFactureId($_GET['id']);
				
				$vendor = $vendorManager -> getBy(array("vendor_id" => $factureCom -> getVendor_id()));
				
				$lines = $factureComLineManager -> getAllFactureComLineForFactureCom($factureCom -> getFacture_com_id());
				
				ob_get_clean();
				ob_start();
    			include(dirname(__FILE__).'/commercants/facturation/factu.php');
    			$content = ob_get_clean();
				
    			// convert in PDF
    			require_once(dirname(__FILE__).'/thirdparty/html2pdf_v4.03/html2pdf.class.php');
    			try
    			{
        			$html2pdf = new HTML2PDF('P', 'A4', 'fr');
			//      $html2pdf->setModeDebug();
        			$html2pdf->setDefaultFont('Arial');
        			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        			$html2pdf->Output($facture -> getFacture_code().'.pdf');
    			}	
    			catch(HTML2PDF_exception $e) {
        			echo $e;
        			exit;
    			} 
			break;
			
            case '/sendMailFacture':
	           $dispatch = true;
               //$out = var_dump("construct"); 
               $mailFact = new BusinessFactureMail($_POST['facture_id']);
               
               $mailFact -> fillBody();
                
               $mailFact -> attachFacture();
               
               if($mailFact -> sendMail()){
                   
                   $factureCom = $factureComManager -> getFactureComByFactureId($_POST['facture_id']);
                   
                   $factureCom -> setEmail_send(1);
                   
                   $factureComManager -> save($factureCom);
                   
                   $out = "Mail bien envoyé à ".$mailFact -> getVendor() -> getVendor_nom().", addresse <b>" . $mailFact -> getMail_client();
               }else{
                   $out = "Echec de l'envoi du mail à ".$mailFact -> getVendor() -> getVendor_nom().", addresse <b> " . $mailFact -> getMail_client() . "</b> ! <br/>" ;
               }
	        break;
            
            
            case '/sendAllFactMail':
               $dispatch = true;
                
               foreach ($_POST['facture'] as $facture_id) {
                   
                   $mailFact = new BusinessFactureMail($facture_id);
                   
                   $mailFact -> fillBody();
                   
                   $mailFact -> attachFacture();
                   
                   if($mailFact -> sendMail()){
                       
                       $factureCom = $factureComManager -> getFactureComByFactureId($facture_id);
                       
                       $factureCom -> setEmail_send(1);
                       
                       $factureComManager -> save($factureCom);
                       
                       $tmp .= "Mail bien envoyé à ".$mailFact -> getVendor() -> getVendor_nom().", addresse <b>" . $mailFact -> getMail_client() . "</b> <br/>";
                   }else{
                       $tmp .= "Echec de l'envoi du mail à ".$mailFact -> getVendor() -> getVendor_nom().", addresse <b> " . $mailFact -> getMail_client() . "</b> ! <br/>" ;
                   }                   
               }
               $out = $tmp;

            break;
                        
            case '/supprAllFactCom':
               $dispatch = true;
                
               foreach ($_POST['facture'] as $facture_id) {
                   
                   
                   $facture = $factureManager -> getFactureById($facture_id);
                   
                   $items = $facture -> getAllItems();
                   
                   foreach ($items as $item) {
                       $item -> setPayment(0);
                       $item -> save();
                       $po = $item -> getPo();
                       $po -> checkPayment();
                       $po -> save();
                   }                
                   $factureManager -> delete($facture);
               }
               

            break;
                                    
			case '/saveFactuAllCommercants':
				$dispatch = true;
				
				$req = dbQueryOne('SELECT MIN(date_value) FROM skf_calendar WHERE date_type_id = 1 AND date_value > (NOW() - INTERVAL 5 DAY)');
				
				$date = new DateTime($req['MIN(date_value)']);
				$date -> modify('-15 day');
				
				
				foreach ($_POST['vendor_id'] as $vendor_id) {
					
					$vendor = $vendorManager -> getBy(array("vendor_id" => $vendor_id));
				
					//Création des deux objets addresse commercant et Soukeo
					$addr_emeteur = new BusinessFactureAddress(array(	'name'     => $vendor ->getVendor_nom(),
																		'siret'    => SystemParams::getParam("marketplace*siret"),
																		'street'   => SystemParams::getParam("marketplace*street"),
																		'city'     => SystemParams::getParam("marketplace*city"),
																		'zip_code' => SystemParams::getParam("marketplace*zip"),
																		'country'  => SystemParams::getParam("marketplace*country")));
																		
					$addr_recipient = new BusinessFactureAddress(array(	'name'     => $vendor ->getVendor_nom(),
																		'siret'    => $vendor ->getSiret(),
																		'street'   => $vendor ->getStreet(),
																		'city'     => $vendor ->getCity(),
																		'zip_code' => $vendor ->getZip(),
																		'country'  => $vendor ->getCountry_id()));
					$factureAddressManager -> save($addr_recipient);
					$factureAddressManager -> save($addr_emeteur);
					
					//Création Facture et Facture spécifique commercant
					$facture = new BusinessFacture(array('emeteur_addr_id' => $addr_emeteur -> getFacture_address_id(),
														 'recipient_addr_id' => $addr_recipient -> getFacture_address_id()));
					$factureManager -> save($facture);
					$facture -> setFacture_code('FSKOCOM'.date('Ymd').zerofill($facture -> getFacture_id()));
					$factureManager -> save($facture);
					$factureCom = new BusinessFactureCom(array('facture_id' => $facture -> getFacture_id(),
															   'vendor_id'  => $vendor_id,
															   'date_limit' => $date -> format('Y-m-d H:i:s'),
														   		'email' => SystemParams::getParam("marketplace*mailVendeur"),
														   		'tel'   => SystemParams::getParam("marketplace*tel")));
					$factureComManager -> save($factureCom);
	
					//Récupération de tous les PO à payer du vendeur
					$poList = $vendor -> getAllPoToPay(); 
					
					$total_prix = 0;
					$total_comm = 0;
					$total_frais_b = 0;
					$grand_total = 0;
					$countLine = 1;
					
					foreach ($poList as $po) {
						$order = new BusinessOrder($po->getOrder_id());
						$itemList = $po -> getAllItems();
						//Pour chaque item on va créer une ligne de facture
						foreach ($itemList as $item) {
							$comm = dbQueryOne('SELECT COMM FROM sfk_categorie WHERE cat_id = '.$item -> getCategory_id());
							$comm = $comm['COMM'];
							
							if(!$item -> getLitiges() && !$item -> getPayment()  ){
								
								$line = new BusinessFactureComLine(array("line_id" => $countLine,
																		 "facture_com_id" => $factureCom -> getFacture_com_id(),
																		 "item_id" => $item -> getItem_id(),
																		 "order_increment_id" => $order -> getIncrement_id(),
																		 "qty" => $item -> getQty_ordered(),
																		 "designation" => $item -> getName(),
																		 "price_ttc" => $item -> getBase_row_total_incl_tax(),
																		 "category_id" => $item -> getCategory_id(),
																		 "percent_comm" => $comm,
																		 "mt_comm" => $item -> getBase_row_total_incl_tax() * $comm,
																		 "mt_frais_banq" => $item -> getBase_row_total_incl_tax() * 0.005,
																		 "litiges" 		=> $item -> getLitiges()
																	));
								$factureComLineManager -> save($line);
								
								$total_prix += $item -> getBase_row_total_incl_tax();
								$total_comm += $item -> getBase_row_total_incl_tax() * $comm;
								$total_frais_b += $item -> getBase_row_total_incl_tax() * 0.005;
								$grand_total += ($item -> getBase_row_total_incl_tax() * $comm) + ($item -> getBase_row_total_incl_tax() * 0.005);
								
								$item -> setFacture_id($facture -> getFacture_id());
								$item -> setPayment(1);
								$item -> save();
								
								$countLine ++ ;
								
							}elseif($item -> getLitiges() && !$item -> getPayment()){
								$line = new BusinessFactureComLine(array("line_id" => $countLine,
																		 "facture_com_id" => $factureCom -> getFacture_com_id(),
																		 "item_id" => $item -> getItem_id(),
																		 "order_increment_id" => $order -> getIncrement_id(),
																		 "qty" => $item -> getQty_ordered(),
																		 "designation" => $item -> getName(),
																		 "price_ttc" => 0,
																		 "category_id" => $item -> getCategory_id(),
																		 "percent_comm" => $comm,
																		 "mt_comm" => 0,
																		 "mt_frais_banq" => 0,
																		 "litiges" 		=> $item -> getLitiges()
																	));
								$factureComLineManager -> save($line);
								$item -> setFacture_id($facture -> getFacture_id());
								$item -> save();
								$countLine ++ ;
							}
						}
                        //Remboursement frais de port
                        if($po->isShippingRefund() &&  (float)$po->getBase_shipping_amount() > 0){
                            
                                $line = new BusinessFactureComLine(array("line_id" => $countLine,
                                                                         "facture_com_id" => $factureCom -> getFacture_com_id(),
                                                                         "item_id" => 0,
                                                                         "order_increment_id" => $order -> getIncrement_id(),
                                                                         "qty" => 1,
                                                                         "designation" => "Remboursement frais d'envoi ".numberByLang($po->getBase_shipping_amount())." €",
                                                                         "price_ttc" => $po->getBase_shipping_amount(),
                                                                         "category_id" => 0,
                                                                         "raison_litige" => "frais_envoi",
                                                                         "percent_comm" => 0,
                                                                         "mt_comm" => 0,
                                                                         "mt_frais_banq" => 0,
                                                                         "litiges"      => 0
                                                                    ));
                                $factureComLineManager -> save($line);
                                
                                $total_prix += (float)$po->getBase_shipping_amount();
    
                                $countLine ++ ;
                        }
                        
                        //Après avoir passé sur tous les items on vérifie le paiement total/partiel du PO
                        $po->checkPayment();
                        $po->save();
					}
					
					$factureCom -> setTotal_prix($total_prix);
					$factureCom -> setTotal_comm($total_comm);
					$factureCom -> setTotal_frais_banq($total_frais_b);
					$factureCom -> setGrand_total($grand_total);
					$factureCom -> setMt_a_transferer($total_prix  - ($grand_total * 1.085));
					
					$factureComManager -> save($factureCom);
				}
			break;
			
			case '/commercants/abonnements':
				$ACCESS_key ="commercants.abonnements..";
				$security_domain = "commercants";
				$domain_categ = "abonnements";
				$right_menu_visibility = true;

				$grid = new AbonnementsComGrid($_GET['id']);				
				$view -> assign("out", $grid -> display());
				$template = "rendered";
				
				ob_start();
				include ("./commercants/abonnements/menu.php");
				$colright = ob_get_contents();
				ob_end_clean();
				
				$view -> assign("right", $colright);
				$tempCol = "colAdmin";
			break;
            
            case '/dashboard':
                //$dispatch = true;
                $group_id = $user -> getGroup();
                $group_id = $group_id[0];
                $group_id = $group_id['group_id'];

                $group = $groupManager -> getGroupById ($group_id);
                
                switch ($group -> getName() ) {
                        
                    case 'ADMIN':
                        $template = "dashboardServiceClient";
                        $recentTicketList = $ticketManager -> getAllRecentTickets();
                        
                        
                        if($ticketList = $user -> getAllTickets()){
                            $view -> assign('ticketList', $ticketList);    
                        }
                        
                        if($relanceTicketList = $user -> getAllRelanceTickets()) {
                            $view -> assign('relanceTicketList', $relanceTicketList);
                        }
                        
                        $view -> assign('recentTicketList', $recentTicketList );
                        $view -> assign('user', $user);                 
                    break;

                    case 'SERVICE CLIENT':
                        
                        $template = "dashboardServiceClient";
                        $recentTicketList = $ticketManager -> getAllRecentTickets();
                        
                        
                        if($ticketList = $user -> getAllTickets()){
                            $view -> assign('ticketList', $ticketList);    
                        }
                        
                        if($relanceTicketList = $user -> getAllRelanceTickets()) {
                            $view -> assign('relanceTicketList', $relanceTicketList);
                        }
                        
                        $view -> assign('recentTicketList', $recentTicketList );
                        $view -> assign('user', $user);         
                    break;

                    case 'STAGIAIRE':
                        
                        $template = "dashboardServiceClient";
                        
                        $recentTicketList = $ticketManager -> getAllRecentTickets();
                        
                        if($ticketList = $user -> getAllTickets()){
                            $view -> assign('ticketList', $ticketList);    
                        }
                        
                        $view -> assign('recentTicketList', $recentTicketList );
                        $view -> assign('user', $user);        
                    break;
                                                    
                    case 'GESTION AVAHIS':
                        $template = "dashboardGestionAvahis";
                        $poList = $skf -> getAllPoAvahisToOrder();
                        $orderGrList = $skf -> getAllOrdersGrossisteByStatus('new');
                        $countProdToQualify = $skf -> getCountProductGrossisteToQualify();
                        $itemListToOrder = $skf -> getAllItemsToOrder();
                        
                        if($ticketList = $user -> getAllTickets()){
                            $view -> assign('ticketList', $ticketList);    
                        }
                        
                        $view -> assign('poList', $poList );
                        $view -> assign('orderGrList', $orderGrList );
                        $view -> assign('countProdGr', $countProdToQualify );
                        $view -> assign('itemListToOrder', $itemListToOrder);
                        $view -> assign('user', $user);  
                    break;
            
                    case 'STAGIAIRE':
                        $template = "dashboardGestionAvahis";
                        
                    break;
                        
                    default:
                    break;
                }
                
                        
            break;
			
			default :
				//   header("location:
				// http://".$_SERVER["SERVER_ADDR"]."/common/login/index.php");
			break;
		}

	} elseif ($user -> getAdmin() === false) {

		switch ($_SERVER['PATH_INFO']) {
			case '/errForm' :
				$dispatch = true;
				$r = $data -> getLibCarac($_POST['code']);
				$out = $view -> getFormError($r, $_POST["tarif"], $_POST['code2']);
				break;
			case '/save' :
				$dispatch = true;
				// donnees modifiees par gestionnaire
				if ($_POST['act'] == 'saveForm') {
					$out = $data -> savedataGest($_POST["dataForm"], $_POST["siteid"]);

					$out .= _("Donnèes sauvegardées avec succès") . "<br/>";
					$out .= _("Vos données sont enregistrées provisoirement mais ne sont pas transmises à") . '.<br/>' . _("N'oubliez de revenir pour les compléter et les envoyer");

				} elseif ($_POST['act'] == 'sendForm') {
					$xt_page = "Envoi_MAJ::" . strtoupper(SystemLang::getUrlCampLang());

					$out = $data -> senddataGest($_POST["dataForm"], $_POST["siteid"]);

					// Il est possible de desactiver l'export en mettant cette entree a N dans la table c_params
					if (SystemParams::getParam("system*gen_xml") == 'Y') {
						$exXml = new BusinessExportXML( array( array("ID" => $_POST["siteid"])));
						$exXml -> exportToSite();
					}
					if (!$test) {
						$myMail = new BusinessMail();
						$myMail -> sendmailGest($data -> getMailGest($_POST["siteid"]));
					}
					$out .= _("Donnèes envoyées avec succès");
				}
				break;

			case '/gest' :
				//ACCEUIL
				$xt_page = "Tableau_de_bord::" . strtoupper(SystemLang::getUrlCampLang());
				$ACCESS_key = "gest.acc..";
				$var = $data -> getDataAccGest($user -> getUserId());
				$template = 'gestAcceuil';
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
			break;
            
			case '/valid' :
				$xt_page = "ENVOI::" . strtoupper(SystemLang::getUrlCampLang());
				$ACCESS_key = "gest.acc..";
				$var = $data -> getDataAccGest($user -> getUserId());
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				$template = 'gestAcceuil';
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
			break;
            
			case '/gest_maj' :
				//Mise à jours des données.
				$xt_page = "MAJ::" . strtoupper(SystemLang::getUrlCampLang());
				$ACCESS_key = "gest.maj..";
				$template = "gestMaj";
				$var = $data -> getDataMajGest($user -> getUserId());
				// On envoi toutes les donnees a la vue
				$view -> assign('mpfModelGest', $var);
				foreach ($var as $K => $V) {
					if (($V != '(null)'))
						$view -> assign($K, $V);
				}
				$tempCol = "colRight";
			break;

			default :
				$ACCESS_key = "FALSE";
				//   header("location:
				// http://".$_SERVER["SERVER_ADDR"]."/common/login/index.php");
			break;
		}

	}
	if (!$dispatch) {
		//  SystemAccess::canAccessPage2();
		//  SystemUserTracking::trackUser();

		require $root_path . 'inc/header.inc.php';
		echo $view -> render($template);
		if ($tempCol) {
			if ($tempCol == "colRight") {
				$info1 = $data -> getCol1content();
				$view -> assign('bloc1', $info1);
			}
			echo $view -> render($tempCol);
		}

		require $inc_path . 'footer.inc.php';

	} else {
		echo $out;
	}
} else {
    
	header("location: http://" . $_SERVER["SERVER_ADDR"].":".$_SERVER["SERVER_PORT"]. "/common/login/index.php");
}
