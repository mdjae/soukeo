﻿<?php 
$root_path = "../../";
require_once $root_path .	"inc/libs/error.inc.php";
require_once $root_path .	'config/config.inc.php';
require_once $root_path .	'inc/version.php';
require_once $root_path .	'inc/libs/db.mysql.inc.php';
$db_conn = dbConnect();
require_once $root_path . 'inc/class/ui/UIBox.class.php';

//date_default_timezone_set('UTC');
date_default_timezone_set('Europe/Paris');

// PATHS
$inc_path = $root_path.'inc/';
$skin_path = $root_path.'skins/common/';
$corp_path = $root_path.'skins/'.$CORPORATE_PORTAL.'/';
?>
<html>
	<head>
		<title><?php  echo ("Gestionnaire - Installation de la base de données") ?></title>
		<link rel="shortcut icon" type="image/ico" href="<?php  echo $root_path ?>favicon.ico">
		<script language="JavaScript1.2" src="<?php  echo $root_path ?>inc/js/mootools-1.2-core-yc.js" type='text/javascript'></script>
  		<script language="JavaScript1.2" src="<?php  echo $root_path ?>inc/js/mootools-1.2-more.js" type='text/javascript'></script>
		<!--link href="<?php  echo $root_path ?>skins/common/offipse.css" rel="stylesheet" type="text/css" /-->
  		<style type="text/css">
			<?php  require_once $root_path."skins/common/offipse.css"; ?>
  		</style>
		<link href="<?php  echo $root_path ?>skins/common/menu_styles.css" rel="stylesheet" type="text/css" />
		<!--link href="<?php  echo $corp_path ?>more.css" rel="stylesheet" type="text/css" /-->
		<style type="text/css">
			<?php  require_once $corp_path."more.css"; ?>
  		</style>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000px">
			<tr>
				<td class="content" valign="top">
					<img src="<?php  echo $root_path ?>skins/common/images/empty.gif" width="1" height="10"/>
					<!-- content -->


<?php 
	// Controle de la présence de tables dans la base
	$mybox = new UIBox("db_box",("Installation de la base de données"),"tools","NoBg"); 
	$mybox->drawBoxHeaderStatic();
	
?>
				<table bgcolor="#DDDDDD" cellpadding="1" cellspacing="0" border="0" align="center" width="70%">
				<tr><td>
				<table bgcolor="#EEEEEE" cellpadding="8" cellspacing="1" border="0" width="100%" align="center">
					<tr bgcolor="#DDDDDD">
						<td><p align="center"><?php  echo ("Opération") ?></p></td>
						<td><p align="center"><?php  echo ("Statut") ?></p></td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td><p><?php  echo ("Procédez au chargement manuel du dump de base fourni avec l'application") ?></p></td>
					</tr>
				</table>
				</td></tr>
				</table>
<?php 
	$mybox->drawBoxFooter();  
?>
				</td>
			</tr>
		</table>
	</body>
</html>