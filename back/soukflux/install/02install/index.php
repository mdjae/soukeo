<?php 
$root_path = "../../";
$inc_path = $root_path.'inc/';
$skin_path = $root_path.'skins/common/';
$corp_path = $root_path.'skins/PADOC/';
require_once $root_path . 'inc/class/ui/UIBox.class.php';
require_once $root_path . 'inc/class/system/SystemConfig.class.php';

// Doit-on mettres � jour les param�tres ?
if (array_key_exists('submitted',$_POST))
{
	if (SystemConfig::updateConfigFile())
	{
		// On file vers la page d'installation de la base de donn�es
		header ("location: ../03dbinstall/");
	}
}
?>
<html>
	<head>
		<title><?php  echo ("Padoc - Configuration de base") ?></title>
		<link rel="shortcut icon" type="image/ico" href="<?php  echo $root_path ?>favicon.ico">
		<!--link href="<?php  echo $root_path ?>skins/common/offipse.css" rel="stylesheet" type="text/css" /-->
  		<style type="text/css">
			<?php  require_once $root_path."skins/common/offipse.css"; ?>
  		</style>
		<link href="<?php  echo $root_path ?>skins/common/menu_styles.css" rel="stylesheet" type="text/css" />
		<script language="JavaScript1.2" src="<?php  echo $root_path ?>inc/js/mootools-1.2-core-yc.js" type='text/javascript'></script>
  		<script language="JavaScript1.2" src="<?php  echo $root_path ?>inc/js/mootools-1.2-more.js" type='text/javascript'></script>
		<!--link href="<?php  echo $corp_path ?>more.css" rel="stylesheet" type="text/css" /-->
		<style type="text/css">
			<?php  require_once $corp_path."more.css"; ?>
  		</style>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000px">
			<tr>
				<td class="content" valign="top">
					<img src="<?php  echo $root_path ?>skins/common/images/empty.gif" width="1" height="10"/>
					<!-- content -->
					<?php  echo SystemConfig::displayConfigSetting(); ?>
				</td>
			</tr>
		</table>
	</body>
</html>