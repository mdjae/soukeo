﻿<?php 
$root_path = "../../";
$inc_path = $root_path.'inc/';
$skin_path = $root_path.'skins/common/';
$corp_path = $root_path.'skins/PADOC/';
require_once $root_path . 'inc/class/ui/UIBox.class.php';
require_once $root_path . 'inc/class/system/SystemServerConfig.class.php';
	
if (!function_exists("bindtextdomain"))
{
	echo "Activer le support getext dans vos module PHP pour commencer!";
	exit;
}

// Compteur des trucs à controller
$total = 0;
?>
<html>
	<head>
		<title><?php  echo ("Gestionnaire - Contrôle du serveur") ?></title>
		<link rel="shortcut icon" type="image/ico" href="<?php  echo $root_path ?>favicon.ico">
		<!--link href="<?php  echo $root_path ?>skins/common/offipse.css" rel="stylesheet" type="text/css" /-->
  		<style type="text/css">
			<?php  require_once $root_path."skins/common/offipse.css"; ?>
  		</style>
  		<script language="JavaScript1.2" src="<?php  echo $root_path ?>inc/js/mootools-1.2-core-yc.js" type='text/javascript'></script>
  		<script language="JavaScript1.2" src="<?php  echo $root_path ?>inc/js/mootools-1.2-more.js" type='text/javascript'></script>
		<link href="<?php  echo $root_path ?>skins/common/menu_styles.css" rel="stylesheet" type="text/css" />
		<!--link href="<?php  echo $corp_path ?>more.css" rel="stylesheet" type="text/css" /-->
		<style type="text/css">
			<?php  require_once $corp_path."more.css"; ?>
  		</style>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" align="center" width="1000px">
			<tr>
				<td class="content" valign="top">
					<img src="<?php  echo $root_path ?>skins/common/images/empty.gif" width="1" height="10"/>
					<!-- content -->
					<?php  echo SystemServerConfig::displayServerConfig(); ?>
					<span id="installation" style="display: none"><h3 align="center"><?php  echo ("Votre installation permet d'utiliser Gestionnaire de terrain") ?></h3><p align="center"><input type="button" onClick="location.href='../02install'" value="<?php  echo ("Passer à la configuration") ?> >>"/></p></span>
					<script>var total = <?php  echo $total ?></script>
				</td>
			</tr>
		</table>
	</body>
</html>